# About

This is my personal knowledge management system, including not only tech-related areas but basically anything.

Feel free to ask me questions.

**I strongly encourage pull requests or any other form of collaboration.**

# Content guidelines

I mostly follow the [Simple Ontology for Knowledge Bases](https://gitlab.com/lsbenitezpereira/simple-ontology-for-knowledge-bases/-/blob/main/README.md) syntax.

For historical reasons, some differences arised:
* the files are written in English, Portuguese and sometimes Spanish. 
* Docx and ODT are used alongside markdown.

Folder structure:
* Folder "General Subjects": knowledge from basic sciences, like mathematics or physics
* Folder "Engineering": knowledge from the area of electronics/computing

Sometimes math topics are covered under engineering, like statistics being in the data science folder.

# Graphical version
Using the tool [Knowledge Base Analytics](https://gitlab.com/lsbenitezpereira/Knowledge-Base-Analytics), I created a graphical version of this knowledge base. 
The content is exactly the same as the gitlab version.

It is available at: https://benitezknowledgebase.z6.web.core.windows.net/