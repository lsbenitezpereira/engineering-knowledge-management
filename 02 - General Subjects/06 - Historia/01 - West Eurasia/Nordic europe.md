[TOC]

Scandinavia: viking stuff

Fennoscandian: scandinavia + finland and estonia + parts of russia

# Nordic and baltics

## Estonia

* **middle age**
  * The Hanseatic League controlled trade on the Baltic Sea, and Estonia was in 
* **Russia-Sweden-Russia**
  * 1558, Russia occupy Estonia (Tsar Ivan the Terrible)
  * Sweden react, and wins in 1583
  * 1600, Polish-Swedish War. Sweden gaining Livonia
  * 1700, Great Northern War. conquered by the Russian Empire
* **Independence**
  * 1919, Estonian Constituent Assembly 
  * 2 years of war to that
* **Russia occupation**
  * 1940, Estonia was annexed by the Soviet Union
  * Germany occupy , not gently
  * Germany loses. Soviet occupation
* **re independence**
  * 1991
  * political parties
    *  Popular Front of Estonia. moderate wing in the independence movement
    *  Estonian National Independence Party, more radical independence

* 

## Finland

* tai *Suomen historia*
* **Pre history**
  * unabitated until 10.000 BCE (end of the last ice age)
  * First ceramic potter: 5200 BCE
  * Start of agriculture: 3000 BCE
  * Broze age: 1500 BCE
  * Pre roman iron age: 500 BCE
  * During the Viking Age, those regions were inhabitated by Finnic Tribes
* **Part of the kingdom of Sweden**

  * Gradually became part of Sweden during the 13th century, through the forced expantion of the Catholic Church (series of wars)
  * arround 1240: Second Swedish Crusade, led by Birger Jarl
  * the term *Österland* (Eastland) was used to describe the area until the 15th, when it was replaced by Suomi (Finland)
  * Swedish was the nobility and administration language 
  * Main defesive fortress: Turku, Hämeenlinna, and Viipuri
  * 1590: helsinki is stabished, but it remained a mere fishing village for over two centuries
  * 1591: translation of the New Testament in Finnish
  * 1640: first university in Finland, Åbo (later moved to Helsinki, after and fire)
  * The Large Wrath (1713~1721): russian occupation of finland (during Great Northen War)
  * The Lesser Wrath (1741~1743): russian occupation again
* **The Finnish War (1808~1809)**
  * From February 1808 to September 1809
  * The nobility wanted to join Russia/leave Sweden 
  * 1806: incorporated to the Russian Empire, as an independent region 
* **19th century urbanization**

  * The population in Finland grew rapidly, and the country became urbanized
  * In 1841 the Finnish language became a subject in schools
  * The Finns culture and indentity become stronger, and wanted more independence
  * 1906: adopted the Universal Suffrage
* 
* **Independence (1917)**
  * 1905: after revoutionary movement, Russia limits the Finnish autonomy
  * 1907: first parlamentary elections in finland (with universal sufrage)
  * 1908: Russia increase the repression
  * Finland can’t have an army, but hey begin to organize (the so called *jaegers*)
  * Primeira guerra mundial (1914): permaneceu neutro
  * 1917: declared it’s independence (after the Russian Revolution)
  * A civil wars followed that
    * was formed the Civil Guard Organization (government, pro independence). They keep acting after the war as an independent mainteiner of the finnish soberanity (+-40k in 1918)
    * Red guard: pro russia (+-30k in 1918)
    * 
  * After a brief but bitter civil war between Social Democrats and the non-Socialist Parliament, won by the latter, Finland was announced a democratic republic
  * Becomes an Presidential Republic 
* **Second World War**
  * Winter war (1939)
    * russia invaded finland
    * 4 moths later, peace is signed and Finand losted some territories 
    * 25k finnish causalities, 165k russian causualities 
  * Continuation was (1941~1944)
    * again agaist Russia, “togeder” with Germany
    * After lose the war, had to pay a lot of reparations
    * lost some areas in the Finnish Karelia
    * Germans helped on the north (220k german soldiers in the north)
    * In the end of the war, Finns atacked the German that were in the north (Lappish war). Germans burnt everything while retreating (scorched earth technique), including Rovaniemi
  * Years after war
    * Called *year of danger*, because of fear of becoming like the easter europe
    * 1948: agreement of mutual assistence with URSS (but just in case that Finland was directly affected)
    * era absolutamente essencial para os finlandeses manter boas relações com o lado soviético. Em 1948, os dois países se puseram de acordo sobre uma solução de compromisso pela qual a Finlândia abria mão de se associar a qualquer tipo de aliança voltada contra a URSS.
* **Åland**
  * arquipélago
  * Localizado a meio caminho entre a Finlândia e a Suécia, no centro do mar Báltico
  * constitui uma região autônoma, vinculada à república finlandesa
  * População de cerca de 28 mil habitantes que falam sueco.
  * Possui bandeira, parlamento e legislação próprios.

## Lithuania

* Vilnius key dates
  * 1323: grand duchy of lithunia realocates the capital from Trakai to Vilnius (vilnius was founded for that?)
  * 1387: grand duchy of lithunia accepts christinity
  * 1795: vilnius becpme part of russia empire
  * 1918-02-16: recovers independence 
  * 2015: joins the euro zone


# Escandinávia

* Todos foram rapidamente convertidos ao cristianismo entre os séculos X e XII e, trezentos anos mais tarde, adotaram uniformemente o protestantismo de confissão luterana.
* A cristianização da Escandinávia gerou um tipo de arquitetura religiosa singular: a stavkirke, igreja inteiramente construída em madeira, entre os anos 1000 e 1300, no molde cristão, mas com adornos tipicamente herdados da arte pagã dos vikings (dragões, serpentes, entalhes simétricos e intricados).
* 
* Por sua localização geográfica periférica e limitações em terras férteis, a região escandinava não conheceu o feudalismo, em sua plenitude, como no restante da Europa.
* os camponeses possuíam as terras que cultivavam ou então trabalhavam em uma grande propriedade sem, contudo, “pertencerem” ao senhorio.
* 
* últimos duzentos anos (after napoleonic war) de intensa cooperação regional.
* 
* Todos permaneceram neutros durante a Primeira Guerra Mundial
* **Vikings**
  * Por volta do ano 880 os vikings atingiram a embocadura do Sena e, em pouco tempo, adentraram mais de 150 km de rio, devastando cidades e arrasando as frágeis defesas militares francesas, então abaladas por lutas intestinas após o desmoronamento do Império Carolíngio.
  * As tribos lideradas por Rolão passaram a ocupar o que hoje constitui a região da Normandia (Norr-man, “terra dos homens do norte”)
  * Durante os séculos IX e X, o chefe Rurik e, em seguida, seu filho Igor, governaram o primeiro domínio permanente na estepe ocupada pelos povos eslavos. De suas terras, eles se lançaram ainda mais rumo ao leste em expedições de conquista territorial. Ficaram conhecidos como os rus, termo do qual deriva o gentílico “russo”.

## Sweden

* **Swedish state consolidation**
  * from the 6th to 16th century
  * Kalmar Union (1397 to 1523)
    * joined under a single monarch of the three kingdoms of Denmark, Sweden (then including most of Finland), and Norway,
    * there were several short interruptions
    * Each sub kingdom remained quite independent
    * 1523: Gustav Vasa was electing king in sweden
* **Colonies**
  * 1630s: New Sweden (america), Swedish Gold Coast (Ghana)
  * 1784-1878: Saint Barthélemy (caribe). Bought and then sold
  * Slaves were freed in 1847
* **Napoleonic war**
  * Denmark-Norway on France side
  * Sweden on Russian side
  * Swedish–Norwegian War: Battles of 1814
  * France (and norway) lose
  * Norway becomes part of sweden (denmark remain independent). Norway had a lot of freedom, with even an own constitution)
* **Great Northern War (1700–1721)**
  * Russian (alied with Denmark-Norway and Polish–Lithuanian) vs Sweden
  * Russia conquered Estonia, Livonia, Ingria, and Karelia
* **WW2**
  * Neutral, with slight cooperation with German (much like Norway, but without being invaded)
* **Cold war**
  * Neutral, with slight cooperation with USA
* **Swedish Social Democratic Party**
  * Built the welfare-state of modern Sweden
  * In power 1932-1976 and 1982-1986
  * left-wing
  * Olof Palme
    *  famoso primeiro-ministro da Suécia
    *  Fucking popular, even today
    *  1969-1986: led the Party
    *  Prime minister:  1969-1976, 1982-1986
    *  killed in 1986
    *  assassinado quando saía do cinema Grand, em Estocolmo
    *  foi muito crítico da ação militar norte-americana no Vietnã.

## Norway

* 1914 (napoleotic war) til 1905 (independence): part of Swede
* **Independence**
  * Managed to get full independence in 1905. 
  * By referendum , choose monarchy and elect an danish prince as king

* **ww1**
  * Neutral in ww1

* **Ww2**
  * Invaded by Germany in April 1940
  * beyond the strategic location, the Nazi thought of Norwegians as "asleep aryan"
  * 62 days of campaing
  * Norwegian strategic position: cut off Russian-UK communications

## Denmark

* Copenhagen (/coupenheigen/) = Emergent harbor
* Was founded Just after Viking age
* Denmark and Norway were against hanseatic league 
* Sweden don't want alliance, so they had 300years of wars between them 
* Ww2: Germany just invaded, Denmark want able to do much

## Islândia

* inuits
  * povo originário
  * antes denominados esquimós
