[TOC]

By AD 43 the Roman emperor Claudius had
conquered England, but not Scotland

after AD 411 the Roman Empire gave up on
England

When the Western Roman Empire
collapsed in the fifth century AD , Britain suffered
the most complete decline. England had slipped back into
poverty and political chaos

The rise of inclusive institutions and the
subsequent industrial growth in England did not
follow as a direct legacy of Roman (or earlier)
institutions.

Neither slavery
nor the severe economic restrictions of the feudal
medieval period, such as serfdom, existed in
England at the beginning of the seventeenth
century

* **Weak, thus pluralistic**
  * As the Spanish began their conquest of the Americas in the 1490s, England was a minor European power recovering from the devastating effects of a civil war, the Wars of the Roses. She was in no state to take advantage of the scramble for loot and gold and the opportunity to exploit the indigenous peoples of the Americas
  * Elizabeth I and her successors could not monopolize the trade with the Americas. Other European monarchs could. So while in England, Atlantic trade and colonization started creating a large group of wealthy traders with few links to the Crown, this was not the case in Spain or France
  * The English state aggressively promoted mercantile activities and worked to promote domestic industry
  * Because in 1600 the grip of the Crown was weaker in England than in France and Spain, Atlantic trade opened the way to the creation of new institutions with greater pluralism in England, while strengthening the French and Spanish monarchs
  * the profits of the slave trade helped to enrich those who opposed absolutism (while in Africa they helped to create and strengthen absolutism)
  *  The same forces were at work in France, Spain, and Portugal. But there the kings were much more able to control trade and its profits
  *  compared with Spain and Portugal, England was a latecomer to Atlantic trade, she allowed for relatively broad- based participation in trading and colonial opportunities. What filled the Crown’s coffers in Spain enriched the newly emerging merchant class in England



* War of the Roses
  * long duel between the Houses of Lancaster and York, two families with contenders to be king. The winners were the Lancastrians, whose candidate for king, Henry Tudor, became Henry VII in 1485
* 1642 Civil War
  * between Charles and Parliament 
  * Under the leadership of Oliver Cromwell, the Parliamentarians—known as the Roundheads after the style in which their hair was cropped— defeated the royalists, known as Cavaliers. Charles was tried and executed in 1649
  * monarchy was replaced by the dictatorship of Oliver Cromwell. Following Cromwell’s death, the monarchy was restored in 1660
* Glorious Revolution
  * forged a broad and powerful coalition able to place durable constraints on the power of the monarchy and the executive
  * After victory in the Glorious Revolution, Parliament and William negotiated a new constitution
  * these changes in political institutions represented the triumph of Parliament over the king, and thus the end of absolutism in England and subsequently Great Britain
  *  involved the emergence of a new regime based on constitutional rule and pluralism.

* Act of Union in 1707
  * England and Scotland were united
  * Great Britain (or just Britain) is called the union of England, Wales, and Scotland


members of Parliament were elected, but less than 2 percent of the population could vote


abolition of domestic monopolies, which had been achieved by 1640,

creation of the Bank of England in 1694, as a source of funds for industry. paved the way for a much more extensive
“financial revolution,”
