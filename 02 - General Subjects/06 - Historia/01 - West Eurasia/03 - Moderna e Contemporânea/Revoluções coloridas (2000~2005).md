[TOC]

# Revoluções coloridas

* Derrubada de governos considerados anti americanas
* Principalmente na área de influência da extinta uniao soviética, e depois no oriente médio
* ==de 2000 a 2005?==

## Revolução Bulldozer (serbia, 2000)

* derrubada de sloboda milosevic
* rápida (efetivamente 1 dia), pouco violenta (uns 2 mortos, no maximo)
* alá servia: movida a máfia e conspiração
* ==serbia=Iusguslávia??==
* Antecedentes
  * final da guerra do kosovo, pois kosovo era a “jerusalém da servia
  * Greve na mina de carvão de Kolubara
* Kosovo, 1999
  * A maior parte da população do Kosovo é de origem albanesa
  * Tentou declarar independencia da servia em 1991, mas falhou
  * Guerrilha da ELK (exercio de liberação do kosovo) vs governo servio (na época ainda estava junto com montenegro e se chamava Iusguslávia)
  * 1999: OTAN bombardeia e “liberta” o kosovo
  * Forma-se um governo provisorio
  * 2008: declara sua independência (de forma unilateral) da Sérvia
* Desenrolar
  * Ataque à radio tevelevisão estatal (que depois ficou conhecida como “tv bastilha”)

## Revolução Rosa (Geórgia, 2004)

* Geórgia fica do lado da Turquia, caso você não se lembr


## Revolução laranja (ukrain)

* Ucrania, 2004 a 2005

* Pacifica

* Após frauda nas eleições

* O líder da oposição Viktor Yushchenko foi declarado presidente, derrotando Viktor Yanukovych.


## Revoução rosa (georgia)

* Geogia, 2003



## Belarus: não aconteceu

* ou *bielorrúsia*
* Pais independente desde 1991 (fim da união soviética)
* Revolução laranja na Ucrania
* Ditador Alexander Lukashenko (since 1994)
* ultima ditadura da europa (existe eleições)

## No oriente médio

### Revolução roxa (Iraque, 2005)

### Revolução dos Cedros (Líbano, 2005)

retirada das tropas sírias da regiao. Acho que Bush saiu fortalecido

* na sequência do assassínio do ex-primeiro-ministro Rafik Hariri
* exigiam a retirada completa das forças armadas da Síria do Líbano
* o presidente sírio, Bashar al-Assad decidiu retirar as tropas sírias (2,5 meses após o assassinato)

### Revolução das tulipas (Quirguistão, 2005)

* Ou *Revolução Cor de Rosa*
* Violenta pakas