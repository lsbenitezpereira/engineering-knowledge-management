Balkans: Albania, Bosnia and Herzegovina, Bulgaria, Croatia (approximately the southern half), Greece, Kosovo, Montenegro, North Macedonia, Romania (the Dobrudja region), Serbia, Slovenia (the coastal section), and Turkey (East Thrace)

## Russia

* **Initial occupations**
  * the Slavic tribes		

  * establishment of the Rus' state in the north in 862 ruled by Vikin

  * name 
    * Rus - ia = land of Rus (Latin) 

    * Rus = Vikings who came here

    * curiosity: Baltic countries give different names to Russia, related to the Slavic tribes with witch they bordered 
* **Centralized state**
  * adopted Christianity from the Byzantine Empire in 988

  * disintegrated as a state due to the Mongol invasions in 1237–1240

  * returned as and great duchy centralized in Moscow
* **Tsardom** 
  * 1547
  * Benin with Ivan the Terrible
* **Russian empire**
  * 1721
  * Peter the great
  * the state is reformed to the Europeans model
  * founded St petersburg, to europenize the state

## Ukrain

### 2014 conflic

* Fevereiro de 2014

* Rebeldes derrubam o governo ucraniano

* Putin avança militarmente para garantir a anexação

* Ucrânia reclama, ocidente reclama, mas a região pertence atualmente ao território Russo

* A região da Criméia (de maioria populacional russa e historicamente ligada à ela) fez um referendo favorável à se juntar à Rússia.

* [Why is Ukraine the West's Fault? Featuring John Mearsheimer](https://www.youtube.com/watch?v=JrMiSQAGOS4)

* **Contexto histórico**

  * A Criméia foi cedida à Ucrânia pelos russos em 1954
  * É uma província autônoma deste 1992.
  * NATO’s final declaration in bucharest Sumit (2008): “NATO welcomes ukraine’s and georgia’s euro atlantic aspirations for membership in NATO. We agreed today that these countries will become members of NATO”
  * a little after that declaration, russia invaded georgia

* **strategical position of ukrain**

  * “buffer” between russia and europe (specially germany)

  * natural gas

    ![image-20220226120111727](./Images - Eastern europe and russia/image-20220226120111727.png)

### 2022 conflic

* Reasons
  * Explanation 1
    * the goal was not to conquer it
    * too few soldiers (190k) for that
    * the goal was block the NATO entry
    * similarly, EU-extension is interpreted by russia as preceeding NATO expansion (therefore, equally undesirable)
    * https://youtu.be/r4wLXNydzeY?si=-dvEG8167ABhHZ-S&t=2536
  * Explanation 2
    * the goal was to annexate ukrain
* Preludes
  * Minsk agreements, 2014 and 2015: cease fire in Donetsk and Luhansk rebelions (that were fueled by Russia); In the second agreement, France, Germany, Russia and Ukraine were involved
  * Article - On the Historical Unity of Russians and Ukrainians
    * Written by putin
    * 12 July 2021
    * http://en.kremlin.ru/events/president/news/66181
  * Russia invaded in February 2022



## Geogia

Independence in 1991

### 2008 conflict



or *Russo-Georgian War*

russia invaded 

supported by self-proclaimed republics of South Ossetia and Abkhazia

mian reason: NATO acknowledges georgia pretention to be part of NATO

prelude: april 2008 NATO summit in  Bucharest



On 17 August, Medvedev announced that Russian military would start to pull out of Georgia 

## Poland

Gdynia for centuries remained a small fishing village. By the 20th-century it attracted visitors as a seaside resort town. In 1926, Gdynia was granted city rights after which it enjoyed demographic and urban development, with a modernist cityscape. It became a major seaport city of Poland

Following ww1, city of Gdańsk (Danzig) and surrounding area was declared a free city and put under the League of Nations, though Poland was given economic liberties and requisitioned for matters of foreign representation.

The decision to build a major seaport at Gdynia village was made by the Polish government in winter 1920, as they realized the need for a port city it was in complete control of, economically and politically.

In 1938 Gdynia was the largest and most modern seaport on the Baltic Sea, as well as the tenth biggest in Europe

## Czech republic

* Prague is the historical capital of Bohemia (former bohemian kingdom). founded in c. 1306 BC
* The first masonry under Prague Castle dates from the year 885
* The Přemyslid fort Vyšehrad was founded in the 900
* 
* 1526, the habsburg started ruling this region
  * Habsburg capitla was kinda rotating: officially was Vienna, but the king was often traveling through the empire
* 1999, velvet revolution
* Slovaks wanted to separate. They felt overshadowed by the more rich a d populous Czechs. It was mostly a political, no popular (there were no referendum)

## Hungary

- 2800BC, begin of bronze age

- 800BC. Begin of iron age. Scythians tribes (from Iran?)

- 500BC to 100AC, this area was mainly inhabited by Celtics tribes. La Tène civillization

- Roman conquest of Transdanubia (100s?)

- 200s, marcommanic wars of Roman's against east tribes.

- 350s first hun tribes crossed Volga river (modern russia).

- 453, death of Hun King Attila

- Since 500s influx of tribes from east, notably Avar tribes

- 800s, these area was the fringes of Carolinian empire. Mosaburg was the strongest city (whose location is debated, probably modern Austria or slovenia)

- 800s, hungarian tribes (aka Megyer tribe) settled in this region, coming from east 

- 895, conques of carpatian basis (+- modern hungary)

- 907, battle of brezalauspurc, Bavarian army is defeated by huns

- Hungarian state was established in the late 9th century

- Arpad kings: 1000-1200s

- 1235-1270, cuman tribes from east settled here fleeing from mongol-tartar and were well incorporated in the kingdom

- 1300s anjou kings. As consessions while coming to power, many economic liberal actions were made; growth in trade

- 1400s, sigismund and Janos hunyadi kings

- 1526, battle of mohacs, hungarian defeat to ottomans

- 1541, ottomans conquered Buda

- 1568, peace of adrianople, consolidated ottoman occupation. The northern territory joined habsburg

- 1683-1699, expulsion of turks by habsburg

- 1915, In the napoleonian wars, hubgary was against france

- 1848, hungary had a war trying to sepsrstr itselg from habsburg, and lost

- Mongols invaded in 1241. Didn't stay.

- In 1886, became part of the Austrian empire

- 

- Buda castle

  - Royal place since ~1300
  - 1541, Buda city captured by ottomans
  - Buda city was recaptured from otomans.from 1686 to 1860

- 

- ww1

  - peace treaty was very desfavorable to hungarians

- ww2

  - allied to germany
  - In marcha 1944, germany officially occupied hungary.
  - The German embassador Edmund Vessenmeyer was appointed ruler of the country

  - Germans invaded budapest in march 1944

- Soviet conquered budapest in January 1945

- 10 years after the end of WW2, Hungary tried to became independent for USSR.

- Failed

- 1956 Hungarian revolution

- Soon after austria regain independence

- 200k people fled, and 26k were imprisoned 

- After the revolution, the dictatorship became "lighter"

- ...

- In total, about 400k were deported to gulag and similar camps

- 200k more german-hungarians were deported to fermany

- 100k more indigenous were expelled

- During the Nazi occupation, the party was the Arrow Cross

- It's rank were directly incorporated into the comunist party

* 

## Serbia

* Before Romans, a lot of tribes were here, forming small cities and working metal and coins, including celts and scordiscs. Had influence from greece in many aspects.
* Roman occupied this territory in 1st century
* Slavic tribes invaded in ~600ac
* 1808, One of the first big uprising against ottomans
  * Creation of the first university
* 1868, Liberation from ottomans...?
* Balkan wars (1912-1913)
* * 
* 1918, Kingdom of Yugoslavia
  * Meaning "South slavs"
  * Architecture during the kingdom was highly jnslired by romans
  * 1934: Yuguslavian king was assassinated by selaratists. Kingdom shaked but remained one
  * Art in yuguslavua was consirably aligned with west, not sticking to socialist realism
* Socialist
  * Instead of becoming part of USSR, yuguslavia became an independent communist state (but with strong ties to USSR).
  * Yuguslavia was considered "the most democratic socialist state" at the time.
  * Woman had considerable rights, votes for constitution, etc
  * Tito
    * President that declared republican
    * was member of the communist party, and led the uprising against the uuguslavian kingdom
    * Ruled until 80s whe  he died
* Kosovo war
  * US started supporting Kosovo at some point, arguiblt to improve its image with muslin countries
  * Some European countries saw serbia as allied to Russia, and thus were automatically against serbia (thus pro kosovo)
* 2000: President lsot election, didn't accept, was overthrown
* Discussions about joining EU, but just superficially
* 2012, current party came to power.
* Stops discussions about joining EU, aligns more with Russia and China
* 
* 
* Xemum neighbourhood of belgrade
  * was never ottoman, it had belong to  Austro-Hungarian until end of ww1, when it became part of yuguslavia.
  * Many had already serbians fled to that territory during ottoman, specially novigrad, so assimilation was not difficult
  * Other than that small xemum, Belgrade was not populated to the north of Sava

## Slovenia

1917, corfu declaration, consolidated yuguslavia

According to Slovenians, "Serbs used violence to disseminate their ideology of a Greater Serbia, which understood the idea of a future Yuguslavia merely as an amplified Serbia" museum

Slovenians took ww1 as the opportunity to break from habsurg

Serbia was on the winning side of ww1, while Croatia and slovenia on the losing, so they felt they deserved more power

Carinthian plebiscite 1920, North of slovenia and litoral voted to be outside yuguslavia. We're part of italy and Austria.

"Belgrade replaced vienna"1

1941, occupied by germans

Even here they love Tito

1990, blebicite with 88% of slovenians wanting independence
1991, declaration of independence
10 days of war, endd with the Brioni Declaration

2004, joined EU and nato. First ex socialist to do so.



## Greece

- 1818, Macedonian revolt against turks
- 	Many small rebellions untill 1856
- 	
- 	1877, russo-turikish war. Bulgaria was separated from ottoman empire and because a country, but Macedonia remained ottoman.
- 	
- 	1897, greco-turkish war. Greece lost.
- 	Bulgaria started claiming Greek territory
- 	
- 	1908, turkey had a coup against the sultan. "Young Turk revolution"
- 	
- 	1913, bulgaria-greece war (second Balkan war)
