[TOC]

The first English attempt to plant a colony, at
Roanoke, in North Carolina, between 1585 and
1587, was a complete failure. In 1607 they tried
again. Shortly before the end of 1606, three
vessels, Susan Constant, Godspeed, and Discovery,
under the command of Captain Christopher
Newport, set off for Virginia

* Colonization model
  * they realized that the model of colonization that had worked so well for Cortés and Pizarro simply would not work in North America
  * They could not overcome the indians, and even if they could the indians were distributed aong several tribes instead of having a central king that could be captured
  * if there were going to be a viable colony, it was the colonists who would have to work; 
  * The colonists received incentives to invest and to work hard.
* Independence
  * By the 1720s, all the thirteen colonies of what was to become the United States had similar structures of government. In all cases there was a governor, and an assembly based on a franchise of male  property  holders.  They  were  not democracies; women, slaves, and the propertyless could not vote. But political rights were very broad compared with contemporary societies elsewhere. It was these assemblies and their leaders that coalesced to form the First Continental Congress in 1774
  * The Constitution of the United States did not create a democracy by modern standards. Who could vote in elections was left up to the individual states to determine.
  * The first president of the United States, George Washington, was also a successful general in the War of Independence

Mexican-American War of 1846–1848
Gadsden Purchase of 1853: US annexe a great area of mexico

In the 1890s, large trusts began to emerge
in nearly every sector of the economy, and many
of them controlled more than 70 percent of the
market in their sector.


The southern political institutions, both before
the Civil War and after, had a clear economic
logic, not too different from the South African
Apartheid regime: to secure cheap labor for the
plantations



* Rosa parks case
  * Rosa Parks’s offense was to sit in a section of the Cleveland Avenue bus reserved for whites, a crime under Alabama’s Jim Crow laws
  * She was secretary of the Montgomery chapter of the National Association for the Advancement of Colored People
  * Her arrest triggered a mass movement

The extractive institutions in the
southern United States institutions started to
crumble only after the Second World War and then
truly after the civil rights movement destroyed the
political basis of the system



nowadays the gov is divided between the Congress, the executive, and the courts
congress have 2 houses







In 1846–48 the United States invaded Mexico, and for the price of 13,000 dead American soldiers, it got California, Nevada, Utah, Arizona, New Mexico and parts of Colorado, Kansas, Wyoming and Oklahoma

# US contemporary culture

## Sports

* **NBA - National Basketball Association**
  * 30 teams (29 in the United States and 1 in Canada)
* **Super Bowl**
  * Annual championship game of the National Football League (NFL)
  * played between mid-January and early February

# Commercial war with china

* 2022 october - US impose sanction on the usage of Chinese semiconductors
  * basically, US companies can’t use chinese chips
  * it was company-wise (you can’t buy from specific chinese companies, like Yangtze)
  * is applies only to high technology chips?
  * https://www.telegraph.co.uk/business/2022/10/17/apple-forced-cancel-plans-buy-memory-chips-china-bidens-crackdown/
  *  
  * Also, they are blocking US citizens from working in chinese semiconductors companies
  * Also, block exports to China of American-made manufacturing equipment needed to produce advanced chips