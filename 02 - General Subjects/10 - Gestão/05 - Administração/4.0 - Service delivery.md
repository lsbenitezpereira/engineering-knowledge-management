 Matt Philips proposes that knowledge work consists of two
dimensions where internal and external viewpoints are on one dimension and product
and service delivery are on the other

![image-20231007123207407](/home/benitez/Documents/engineering-knowledge-management/02 - General Subjects/10 - Gestão/05 - Administração/Images - 4.0 - Service delivery/image-20231007123207407.png)



# meetings

* retrospectives

  * People do not usually stop to reflect during work, especially when they are busy.
    Contemplation is not a natural activity, which is why it’s so essential to formalize a
    behavior and make it a ritual

  * Methods and tools to ease retrospective

    * starfish exercise

      ![image-20231007123423816](/home/benitez/Documents/engineering-knowledge-management/02 - General Subjects/10 - Gestão/05 - Administração/Images - 4.0 - Service delivery/image-20231007123423816.png)

    * premortem: imagine your project has failed; place a sticky note on a whiteboard or wall that includes every problem with even a remote chance of occurring that derailed your project; Spend the final 30 minutes creating proactive solutions or backup plans for the three problems. Finally, assign actions and responsibility for them to team members.

  * ?

    * A service delivery review meeting should be a regular assembly for the analytics team
      to discuss with internal customers how well they are meeting their service delivery
      expectations; weekly, biweekly, or monthly

* health check

  * Team health check assessments are monthly or quarterly
    workshops that enable teams to discuss and assess their current situation based on
    many attributes
  * hour-long
  * popularized by Spotify’s Squad Health Check
