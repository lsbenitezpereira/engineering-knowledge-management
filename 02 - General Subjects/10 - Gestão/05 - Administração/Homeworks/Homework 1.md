#  Ray Kurzweil

* About the person
  * 1948
  * American
  * Inventor/engenheiro, empreendedor e escritor
  
* About the professional
  * Tehnical: speech recognition and artificial itnelligence
  * several companies
  *  Google since 2012, where he is a "director of engineering". 
  
* Results
  * received the 1999 National Medal of Technology and Innovation, the United States' highest honor in technology
  * He was the recipient of the $500,000 Lemelson-MIT Prize for 2001
  * has received 21 honorary doctorates, and honors from three U.S. presidents
  * 2005 book The Singularity Is Near was a New York Times bestseller, and has been the #1 book on Amazon in both science and philosophy
  * several others
  
* Uma das coisas que ele fala: crescimento exponencial e singularidade

  * lei dos retornos acelerados

  * Exemplo conhecido: lei de moore

    ![image-20200217232224150](Images - work 1/image-20200217232224150.png)

  * Generalização para outras áreas: 

    ![image-20200217232307978](Images - work 1/image-20200217232307978.png)

    ![image-20200217232324625](Images - work 1/image-20200217232324625.png)

# Giuliano perri

* About the person
  * Empreendedor
* About the professional
  * Graduação em administração de empresas pela FAAP e MBA pela FIPE-USP
  * De 2011 a 2015, foi o responsável pelo Private Banking da XP Investimentos (sua estrutura B2C)
  * fundador e diretor da gp3o Investimentos
* Por que ele é foda?
  * autor de Pra cima deles
  * disseminador de conhecimento
  * dá pra falar um pouco de… sei lá, sociedade do conhecimento popularização do mundo financeiro…