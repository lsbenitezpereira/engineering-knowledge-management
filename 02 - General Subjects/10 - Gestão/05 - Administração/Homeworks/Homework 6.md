### Que estilo e caracteristicas deve ter um lider eficaz?

Para mim um líder deve **inspirar uma visão compartilhada**, isto é: um futuro positivo que arregimente as pessoas para um objetivo comum. Porém cada pessoa é “fisgada” por uma visão diferente, então esse objetivo comum deve ser construído sob medida para cada ambiente, não existe uma formula que funcione para todas as organizações.

![image-20200414222126638](Images - Homework 6/image-20200414222126638.png)

> [Fonte](https://www.modernservantleader.com/servant-leadership/bad-boss-vs-good-leader-image/)