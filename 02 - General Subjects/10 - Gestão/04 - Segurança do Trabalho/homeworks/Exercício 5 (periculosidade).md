# Caso 1

Como a atuação limitava-se somente aos circuitos de controle e automação, **não** caracteriza insalubridade nem periculosidade.



# Caso 2

O trabalhador atuava com as unidades geradoras da empresa, o que caracteriza periculosidade mesmo que as tais estejam desenergizadas (devido à possibilidade de energização acidental) segundo o item 4.2.

# Caso 3

O trabalho _habitual_ com o gerador caracterizam periculosidade. 

Após maio de 2008, é evidente que as atividades do Autor caracterizavam trabalho habitual, portanto este deveria receber um adicional de periculosidade.

Antes de maio de 2008, a situação requer uma análise mais cuidadosa. Quão frequentes eram as faltas de energia elétrica? Dependendo da frequência o pode ou não ser considerado habitual.

# Caso 4

A situação exige uma análise adicional para avaliar a exposição do trabalhador ao gás refrigerante: concentração do gás, uso de EPI, e frequência de exposição.

Existe jurisprudencia indicando decições favoráveis ao trabalhador nessas condições, como por exemplo em [1]: *“A juíza Martha Franco de Azevedo, em exercício na 21ª Vara do Trabalho  de Brasília, baseou sua decisão em laudo pericial que confirmou que os  produtos químicos usados na limpeza de ar condicionado pelo trabalhador, sem o comprovado uso de Equipamentos de Proteção Individual (EPIs),  oferecem riscos à saúde.”*

Apesar do trabalho estar relacionado à energia elétrica, o trabalho não caracteriza atuação junto ao Sistema Elétrico de Potência e portanto não caracteriza periculosidade (vide também [1]).

# Caso 5

Vamos considerar que - entre os vários percursos de buscar/levar equipamentos, buscar peças, etc - o trabalhador acabe indo todos os dias à sala de caldeiras, por 10 minutos cada vez. 

Vamos considerar também que o trabalhador realiza uma atividade física pesada. A depender do IBUGT do local de trabalho, o trabalhador pode precisar descansar fora do local de trabalho, mas sem caracterizar uma situação insalubre ou periculosa.

# Caso 6

A atividade inclui fiscalização habitual de cabinas de distribuições de energia, sendo então enquadrado no item 4.2 do anexo 4 da NR16 e caracterizando periculosidade.

# Caso 7

A menos que a empresa forneça provas do contrário, a atividade parece ser habitual. Conforme NR16, anexo 2, artigo 2, item IV, alínea A [2], a atividade descrita é periculosa:

```
Arrumação de vasilhames ou quaisquer outras atividades executadas dentro do prédio de armazenamento de inflamáveis ou em recintos abertos e com vasilhames cheios de inflamáveis ou vazios não desgaseificados ou decantados.
```

# Caso 8

As situações descritas não fazem parte do SEP e portanto não são passíveis de periculosidade, desde que os materiais e equipamentos elétricos estejam em conformidade com as normas técnicas oficiais estabelecidas pelos órgãos competentes.

# Referências

[1] - http://www.granadeiro.adv.br/clipping/tribunais/2015/06/02/mecanico-de-refrigeracao-exposto-a-produtos-quimicos-tem-direito-a-adicional-de-insalubridade

[2] - http://www.guiatrabalhista.com.br/legislacao/nr/Nr16-anexo2.htm