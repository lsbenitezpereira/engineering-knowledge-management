# Logística

-   Transporte, estoque, distribuição de uma mercadoria

-   **Principais termos**

    -   Embarcador → quem tem a mercadoria

    -   Transportador → quem transporta e assume os riscos

    -   Mercadoria → item a ser transportado. Confere o caráter mercantil.

    -   Frete → pagamento ao transportador

## Modais de transporte

-   Como a mercadoria será transporta

-   Os modais podem ser do tipo terrestre, marítimo ou aéreo

-   ANTT → Agência Nacional de Transportes Terrestres

-   **Classificação do transporte segundo o número de modais**

    -   Unimodal → uma modalidade só em todo o transporte

    -   Intermodal → várias modalidades, sendo um contrato para cada trecho

    -   Multimodal → várias modalidades, sendo feito um único contrato

    -   Sucessivo → transportado por vários veículos da mesma modalidade (1 contrato)

    -   Segmentado → vários contratos para vários modais

-   **Transporte rodoviários **

    -   Terrestre

    -   (+) baixo custo inicial

    -   (-) maior custo operacional

    -   (-) muito poluente

    -   RNTRC → Registro Nacional de Transportadores Rodoviários de carga

    -   Caminhão fixo → caminhãozinho simples

    -   Carreta → cavalo mecânico + semirreboque

    -   Cegonheira → transporta carros

    -   Bitrem → composto por 3 partes

-   **Ferroviário**

    -   Terrestre

    -   (-) lento (geralmente)

    -   (-) só pode ser descarregado em terminais

    -   Opera em horários rígidos

    -   No brasil concentra-se no sul e sudeste, predominantemente para transporte de cargas de baixo valor agregado e em grande quantidade (minério, fertilizantes, derivados de petróleo, etc)

    -   Cada carro transporta umas 100T

-   **Aquático**

    -   ANTAQ → Agência Nacional de Transporte Aquaviário

    -   No brasil, corresponde a 90% do comercio internacional

    -   (-) maior exigência de embalagem

    -   (-) lento

    -   (-) congestionamento terrestre nos portos

    -   (-) muito influenciado pelas condições climáticas

    -   Classificação

        -   Marítimo → cabotagem ou longo curso

        -   Interior → fluvial ou lacustre

    -   Tipos de navios

        -   Porta container → containers movimentados por equipamentos a
            bordo ou em terra

        -   RO-RO (roll on roll off) → transporta veículos. Possui rampas.

        -   Cargueiros → cargas em geral

        -   Graneleiros → porões sem divisão. Cantos arredondados.

-   **Dutoviário**

    -   Terrestre ou aquático

    -   No brasil, representa só 4% da matriz (sendo o maior o gasoduto brasil-bolivia, com 2000km)

    -   Exige bombeamento e estações

    -   (+) baixo custo do operação e manutenção

    -   (+) confiabilidade e segurança

    -   (-) exige mão de obra especializada e equipamentos específicos

    -   (-) movimentação lenta

-   **Aéreo**

    -   ANAC → Agência Nacional de Aviação Civil

    -   TECA → Terminal Alfandegário para carga (o Brasil tem 32)

    -   (+) rápido, eficiente e confiável

    -   (+) baixo custo de seguro

    -   (-) alto custo de operação e manutenção

    -   (-) frete elevado

    -   (-) baixa capacidade de carga

    -   (-) restrição com cargas perigosas

    -   (-) limita a dimensão da carga

## Almoxarifado

-   Recebe, protege, entrega quando requisitado e mantem os registros atualizados

-   **Atividades**

    -   Análise de estoque

    -   Compras

    -   Controle de recebimento

    -   Estocagem

    -   Confiabilidade de estoque

    -   Controle de saída do material

-   **Objetivos**

    -   Máxima utilização do espaço e dos recursos

    -   Satisfação das necessidades do cliente

    -   Pronto acesso a todos os itens

    -   Proteção dos itens estocados

    -   Boa organização

-   **Anotações importantes**

    -   Analisar frequência de utilização, peso e volume dos itens

    -   Corredores devem ser de fácil acesso

    -   Portas de acesso devem ser bem dimensionadas

    -   Prateleiras adequadas em peso e altura

    -   Piso deve ser resistente o suficiente

    -   Gases e líquidos devem ser manipulados em recipientes adequados

    -   Deve ser feita a contagem periódica dos itens e apuração do seu valor
