[TOC]

# Conceptualization

* Process to solve problems by approaching it from the customer's view

## Phases

- **Immersion**
  - empathise with the user
  - Define what they need
  - intervies, surveys, meetings, etc
  - Main outcome: problem statement
  - what makes a good problem statement? human-centered, do not specify technologies, specific enough to lead to solutions,
  - tool - empathy map ([good video](https://www.youtube.com/watch?v=QwF9a56WFWA)); most of it we derive from what the clients explain
  - tool - double diamond; identify problems and then solutions, always first diverging and then converging
- **Ideation**
  - ?
  - Main outcome: acceptance criteria (ideally should not constrain the possible solutions, and should be defined before solutions been suggested)
- **implementation**
  - Prototype and test solutions
  - Real users should try the solution
  - Main outcome: first working solution

# Other annotaions

- **What is a Use Case?** A Use Case is in a simple way a problem description composed of user needs, requirements, and constraints. When collecting a Use Case we ask questions, understand and describe the user's needs and desired results in detail. Afterward, starts the exploration phase, whose goal is to open the possibilities of solving the problem, this is the point where we get creative and gather different ideas together.
- **What is a solution?** The solution is the implementation of one idea from the exploration phase, which really solves the problem.
- **What is a component?** The component is part of a solution and this is what allows reusability. The more modular a solution architecture is created, the more reusable its components are going to be.

* Template for the firsts iterations:

  ```
  ### Use Case
  
  <Describe the Use Case in General>
  
  ### Users Needs
  
  **I as a** <role>  
  **want to** <requirement>
  **so that** <the reason>
  
  (write several views, like manager, IT user, etc)
  
  ### Problem Statement
  
  <Describe the problem we are aiming to solve through this user story>
  
  ### Acceptance criteria
  
  - [ ]  <What will demonstrate that this is done?>
  
  ### Proposed Solution
  ...
  ```

  