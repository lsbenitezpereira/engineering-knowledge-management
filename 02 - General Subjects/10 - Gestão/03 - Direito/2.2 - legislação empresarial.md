[TOC]

# Burocracia empresarial

*  **Processo de abertura de empresa**
   *  Na Junta Comercial você vai gastar entre R\$71 e R\$351. Já na prefeitura, para emitir o alvará, os custos em geral variam entre R\$120 e R\$753.
   *  costuma levar entre 30 e 45 dias
*  **Razão social**
   *  nome devidamente registrado sob o qual uma pessoa jurídica se individualiza e exerce suas atividades
   *  No caso de MEI, a razão Social obedece uma regra fixa: Nome próprio do empreendedor individual seguido de seu CPF.
*  **Nome Fantasia**
   *  Usa para divulgar sua empresa
   *  Não precisa ser igual à Razão Social.
*  **Instituto Nacional de Propriedade Industrial (INPI)**
   *  órgão de marcas e patentes
   *  Marca registrada (o que inclui o nome fantasia) pode ser indicado com ®
*  **Como tirar dinheiro da empresa**
   *  <u>pro labore</u>
      *  precisa pagar o INSS, dá uns 130 reais por mês
      *  não é obrigatório ter pro labore nenhum, tudo pode ser distribuido como lucro

   *  <u>distriubição de lucro</u>
      *  Isente de IR (hoje, 2022)
*  **share capital**: amount of money the owners of a company have invested in the business. Different from shareholders’ equity because it does not include the right to receive earnings
*  **Capital distribution**: when a solvent company is closed, and there are more assets than debts, the assets are sold and are distributed to the shareholders; distributed capital is taxed for the person as income
*  Usual Corporate activities
   Tax Reporting: lodge company and sales tax information on a quarterly basis
   Business Licenses: ?
   Share Certificates: documents that state who owns which percentage of the company, what the terms are for operating that company and so on.

## Regime de tributação

* define a cobrança de impostos
* **Lucro Real**
* **Lucro Presumido**
  * mais simples que o lucro real

* **Simples Nacional**
  * ver abaixo

### Simples Nacional

*  Engloba MEI e ME
*  Você emite nota fiscal
*  precisa de contador
*  Impostos
   *  DAS: guia única simplificada que reúne os seguintes impostos: IRPJ, CSLL, COFINS, PIS/Pasep, CPP, ISS.
   *  percentual do faturamento 
   *  4.5% ~ 15.5% (de acordo com a atividade)
   *  pago até o dia 20 do mês subsequênte (ex: ganhos de março paga até 20 de abril)
   *  ==Para cada Nota emitida tenho que pagar quase 17% do valor no DAS?==
   *  poderá gerar um boleto via Portal do Empreendedor
   *   
   *   
   *  Imposto de Renda da Pessoa Jurídica (IRPJ);
   *  Contribuição Social sobre o Lucro Líquido (CSLL);
   *  Programa de Integração Social (PIS);
   *  Programa de Formação do Patrimônio do Servidor Público (Pasep);
   *  Contribuição para o Financiamento da Seguridade Social (Cofins); 
   *  Contribuição Previdenciária Patronal (CPP).
   *  Se a empresa realizar deslocamentos entre estados, há o Imposto sobre Circulação de Mercadorias e Serviços (ICMS). 
   *  Se for prestadoras de serviço, há o tributo municipal Imposto Sobre Serviços (ISS)
   *  Se for indústria, paga Imposto sobre Produtos Industrializados (IPI).
*  regras
   *  ==O sócio pode residir no exterior?==


#### Modalidades de empresa

*  Essas modalidades são só pro Simples?
*  são definições baseadas no porte e faturamento e não no tipo jurídico
*  
*  Quando é MEI não possui nenhuma dessas modalidades?
*  **Micro Empresa (ME)**
   *  até 360k por ano
   *  Se encaixa no simples nacional
   *  o serviço de contabilidade mensal é exigido por lei...?
*  **Empresa de Pequeno Porte (EPP)**
   *  até 4800k por ano
   *  Se encaixa no simples nacional
   *  o serviço de contabilidade mensal é exigido por lei...?
*  **...?**

## Natureza juridica da empresa

*  Normas e exigências que devem ser seguidas pelos sócios de um negócio
*  composição do quadro de sócios e administradores
*  MEI, ME e EPP são natureza jurídica?
*  **MEI**
   *  ver abaixo
*  **Empresário Individual - EI**
   *  Código: 213-5
   *  minimo 1k de capital social 
   *  sem restrição entre os bens da pessoa física e juridica
   *  Se encaixa no simples nacional
   *  1 só pessoa 
*  **Eireli**
   *  Empresa Individual de Responsabilidade Limitada
   *  Capital social minimo: 100 salários minimos
   *  1 só pessoa 
   *  Se encaixa no simples nacional
   *  foi extinda em 2021, e as empresas passaram a ser SLU
*  **Sociedades Limitadas Unipessoais (SLU)**
   *  Ver abaixo

*  **LTDA**
*  **Sociedade Anônima (SA)**

### MEI

*  Desde 2009
*  Se encaixa no simples nacional, com vantagens 
*  Natureza Jurídica: Empresário Individual
*  **Regras**
   *  até 81k por ano (caso você abriu no meio, o valor máxima é proporcional)
   *  não permite que você tenha várias empresas ao mesmo tempo
   *  a lista de atiidades permitadas é bem restrita 
   *  [Tabela de Atividades Permitidas no MEI](https://www.contabilizei.com.br/contabilidade-online/atividades-mei-tabela/?utm_medium=social&utm_source=youtube+&utm_campaign=youtube-serie-abertura-cnaes-mei+)
   *  Dev enquandra como MEI? Não (ainda), mas profissões similares sim (ex: consertar notebook)…
   *  Máximo 1 funcionário
*  **Befenificios**
   *  uma unica taxa mensal 
   *  tem cnpj, emite nota fiscal
   *  abre pela internet
   *  Não precisa contador
   *  pode ter um emprega- do que recebe um salário mínimo ou o piso da categoria
   *  acesso a benefícios como auxílio-maternidade, auxílio-doença e aposentado- ria, por exemplo. 
   *  isento de tributos federais (Imposto de Renda, PIS, Cofins, IPI e CSLL)
*  **Impostos**
   *  +- 60 reais (já incui INSS)
   *  IR?
*  **Nota fiscal**: O MEI é obrigado a emitir nota fiscal nas vendas e nas prestações de  serviços para pessoas jurídicas, inclusive órgãos públicos, sendo  opcional a emissão de nota fiscal para pessoa física
*  **Certificado de Condição de MEI (CCM)**
   *  ?
*  **DAS**: pago mensalmente no Portal do Empreendedor
*  **Declaração Anual do Simples Nacional (DASN-SIMEI)**
   *  renda bruta do ano anterior
   *  entre o mês de janeiro até o último dia de maio (31/05) de cada ano
   *  Não precisa dos 12 Relatório de Receitas Brutas Mensais, é só colocar o valor total
*  O que pode ser deduzido do imposto de renda
   *  uma quinta parte (20%) dos gastos residenciais quando a sua atividade esteja sendo exercida no endereço do autônomo 
   *  custeio: agua, luz, materiais de consumo, etc
   *  
   *  Há uma parcela do lucro que é autoamticamente isenta. Para serviços, 32% do lucro bruto é isenta de tributação. Calculada sobre o bruto - gastos 
   *  Quando não há contador, você pode encaixar como despeza apenas o que estiver dentor do "lucro presumido"?
   *  as depezas devem ser registradas no "Livro caixa"


### SLU

*  Sociedades Limitadas Unipessoais
*  Se encaixa no simples nacional
*  não exige capital social mínimo
*  possui separação jurídico-pessoal
*   
*  Encargos e responsabilidades (se estiver no Simples)
   *  Ter um contador
   *  Pagar o DAS mensal
      *  o valor depende das receitas do mês anterior, que tem que ser decladadas pra gerar a guia DAS
      *  depende também do cnae
      *  +- 6% dos lucros, geralmente
   *  Alvará
      *  com a prefeitura
      *  pra serviços de TI dá pra abrir como “empresa não estabelecida” (sem acesso de público?), que custa uns 400 reais por ano
      *  renovado anualmente
   *  Registro livros contábeis
      *  Todo ano você deve registrar os livros contábeis na junta comercial do município
      *  custa uns 19 reais por livro
      *  pode atrasar isso vários anos, não dá problema, só precisa estar em dia quando for fechar a empresa ou algo similar
   *  Certificado Digital?
   *  INSS
      *  Se você retirar dinheiro como pró labore, precisa pagar INSS
   *  

## Plataformas de administração

* Contabilizei
  *  deconto: https://www.contabilizei.com.br/programa-de-indicacao/?ref=95d6af7bc415725764de602a3fbb68f0&utm_source=plataforma&utm_campaign=MGM&utm_source=plataforma
* MarketUp
* Agilize
* MEIfacil
* asaas: conta em análise
* Qipu: não em floripa
* Aplicativo NFSE: nao fpolis
* sebrae SP: pedia um cadastro estadual que eu nao tenho
* ContaAzul (online): precisa de certificado digital; dá pra comprar
* Emitte: ridiculamente ruim; nao emite nota fiscal de serviço
* contabilivre: serviço horrpivel (referencia carol)

## International stuff

* **offshore company**
  * ?
* international (trust) company
  * ?
* 
* registration vs incorporation
  * are two different bussiness strucutres
  * Incorporation
    * creates a separate legal entity
    * protects the business owner from losing personal assets if the  company is sued
    * can often be achieved without residency
    * Usually is registered in the Chamber of Commerce of the country

  * registraation
    * registration is not a form of incorporation, and it doesn’t often provide any protection of personal assets
    * you receive a license to operate legally, but not much more than that

* .

# Impostos e declarações

## Notas fiscais

* ==emite a nota fiscal, depois presta o serviço==
* Individual para cada atividade realizada
* em Santa Catarina, a emissão de notas de prestação de serviço é de responsabilidade da prefeitura da cidade. No  caso de comércio ou indústria, é dever do Governo do Estado legislar e  prestar informações sobre o assunto.
* **Nota fiscal avulsa**
  * ==NFA-e?==
  * o armazenamento da cópia da nota fiscal por cinco anos é obrigatório
  * precisa se registrar no CCM e no ==simei== estadual
  * Em Santa Catarina, a emissão de NF de prestação de serviços
    é de responsabilidade da prefeitura
  *  fácil e gratuita: na hora pela internet
  * modelos de nota fiscal avulsa
    * Modelo 55: é o mesmo da NF-e e, portanto, tem emissão e armazenamento  exclusivamente eletrônicos, sendo a nota enviada ao remetente através de um arquivo XML;
    * Modelo 1-A: a nota é gerada eletronicamente, mas a sua versão impressa é que vale como documento fiscal.
    * Modelo 1: ?
* **Nota fiscal eletronica**
  * NF-e
  
  * emitido gratuitamente a partir de um sistema disponibilizado pela própria prefeitura
  
  * Você deve ter um certificado digital; MEI não precisa
  
  * Como obter a identidade digital
  
    * procurar uma Autoridade Certificadora
    * preencher o formulário com seus dados, escolher uma das opções de pagamento e se apresentar em uma Autoridade de Registro, levando os documentos pessoais necessários para a autenticação. 
    * A emissão de certificado para Pessoa Jurídica requer a apresentação dos seguintes documentos: registro comercial; ato constitutivo, estatuto ou contrato social; CNPJ e documentos pessoais do responsável.
* **Nota Fiscal de Bloco**
  * Em papel
  * quase não é mais usada
  * Você deverá ir até a Secretaria da Fazenda estadual ou municipal e solicitar uma “Autorização de Impressão de Nota Fiscal”. Depois disso, é preciso imprimir o talão de notas fiscais em uma gráfica ou empresa especializada. Com o bloco em mãos, as notas fiscais devem ser preenchidas manualmente e entregues aos clientes ou empresas que compraram produtos ou serviços do MEI.
* **RPA**: ver em *regime de contrataçao autonomo*
* 

## Invoice

* usado na cobrança de valores em qualquer moeda estrangeira
* tem a mesma importância da nota fiscal emitida em transações dentro do Brasil
* opcional para transações abaixo de 3k USD
* não existe um modelo oficial para o documento
* Qualquer pessoa, seja física ou jurídica, deve emitir uma invoice ao fazer transações comerciais com o exterior
* ==não tem valor fiscal, então eu só gero e fodase?==
* depois que o valor for pago, você deve gerar uma nota fiscal só pra deixar registrado
* O valor recebido vai ser declarado normalmente para Receita brasileira
*  
* you will need somewhere to send your invoices from. That location is where you officially make the money you earn
* When sending invoices for the services you render, it is important that you include certain data (like company registration number, VAT number, legal address) on your invoices in order for them to be legally valid in the country you pay tax to. 
* 
* Typos
  * Commercial invoice
  * Invoice para exportação
  * Proforma invoice
* Plataformas para gerar invoice
  * https://agilize.com.br
  * [Remessa online](https://www.remessaonline.com.br/blog/emissao-de-invoice-da-remessa-online-mais-uma-vantagem-para-voce/)
  * Freshbooks.com
  * Moneybird.nl, an invoicing system for the Netherlands and Germany
* 
* Em transações internacionais há isenção de impostos como o ISS e o ICMS, mas você ainda precisará pagar alguns tributos, como o Imposto Sobre Operações Financeiras (IOF) e o Imposto de Renda.

## Alvarás e etc

*  Consulta de Viabilidade para Instalação: emitida na criação do MEI
* Atestado de Vistoria do Corpo de Bombeiros para Funcionamento: necessário para o Alvará Definitivo
* **Alvará**
  * te habilita a atuar em uma determinada localidade
* **o quê da vigilância sanitária?**
  * ?

## Inscrições

* **Inscrição municipal**
  * cadastrado feito no município em que a empresa atuará
  * diretamente relacionada ao Imposto sobre Serviço de Qualquer Natureza (ISS)
* **Inscrição Estadual**
  * número de Inscrição Estadual (DECA)

## IRPF

* Leão: animal escolhido na década de 70 como mascote do IR
* pago anualmente, geralmente até final de abril 
* descreve os ganhos do ano anterior 
* pagamento pode ser feito por boleto bancário ou débito automático
* 
* No caso de universitários com até 24 anos, a lei permite que seus pais os declarem como dependentes legais. Uma possível bolsa do universitário deve ser somado à renda tributável anual do declarante.
* basta acessar o site receita.fazenda.gov.br, fazer o download e a instalação do programa da Receita Federal e informar os dados solicitados
* abaixo de 28k/ano não precisa declarar
* 
* Alguns gastos podem ser deduzidos, como gastos médicos
* você pode retificar a declaração até 5 anos depois
* malha fina: cruzamento de dados para checagem das informações disponibilizadas
* Mesmo ganhos isentos de IR devem ser informados
* 
* Simplificado: 
* Completo: 

## IRPJ

* Declarar o Imposto de Renda de Pessoa Jurídica (IRPJ) é obrigatório para todas as empresas, à exceção das micro e pequenas empresas que se enquadram no Simples Nacional.
* Lucro presimido: pagamento anual ou trimestral
* Lucro Real: pagamento anual (último dia de dezembro) ou trimestral.

## ISS

* Imposto sobre Serviço de Qualquer Natureza
* Lei Complementar nº 116/2003
* Esfera: municipal
* http://portal.pmf.sc.gov.br/entidades/fazenda/?cms=declaracao+eletronica+do+iss
* Como pagar o ISS: autonomo
  * o autônomo emite nota fiscal avulsa (er em seção própria) na prefeitura e já recolhe no mesmo momento o valor devido a título de Imposto Sobre Serviço
* Como pagar o ISS: Simples Nacional (MEI e ME)
  * já vai junto com a guia unica DAS
  * Se for ME (não MEI): ==Para realizar registrar o valor a ser pago no DAS, é necessário acessar o site da Receita Federal e ir até o PGDAS-D==
* Como pagar o ISS: empresa
  * ?

## Guia de Informação Fiscal – GIF?

## ICMS

* Imposto sobre Operações relativas à Circulação de Mercadorias e Prestação de Serviços de Transporte Interestadual e Intermunicipal e de Comunicação
* Lei Complementar 87/1996
* Esfera: estadual
* ==se paga icms nao paga ISS==


## VAT

* Value Added Tax
* In most countries, you are required to charge a sales tax on the services or products you sell
* Depending on where the you incorporate, your company will get a VAT number, which needs to be printed on your invoices. If your client is a company, you will be required to print his VAT number on the invoice as well.
* Officially, you have to get a VAT registration in each country you do business with (meaning in each country
  you have clients). A way around this (for services or products delivered outside of the EU to EU businesses) is to create invoices without VAT, stating that the VAT is deferred. This means, the client needs to register the VAT over their purchase, which they can then deduct again
* MwSt in Germany
* IVA in Spain
* TVA in France
* BTW in the Netherlands

# Funcionários e contratações

* Ferramentas de gestão
  * https://www.papayaglobal.com: to ay employees globaly

# Contratos

* **NDA**
* **Contrato de prestação de serviços**
  * Se for para uma empresa internacional, em que idioma deve estar? Há algum problema se for só em ingles? 

# Opening companies in other countries

## Denmark

high company taxes (22%)
you need a work permit in order to run a business in Denmark

## Ireland

work visa to self-employed and high skill work employees: "D visa"

## Norway 

the whole process of setting up a new company is entirely online

CIT is, in general, assessed at a rate of 22%. Certain companies within the financial sector are assessed at a CIT rate of 25%
The taxes can be high, indeed, but they are transparent, and the costs are balanced out by low investment risks

## British Virgin Island

Part of UK
Not EU
Online process

## Georgia

Corporate taxes: 15% (0% for IT-companies
Online process
Not yet part of EU, and not Nato

## Singapure

?

## Andorra

Not EU, not Nato
Is not part of the Schengen zone?
Good for be done online

## Cyprus

EU member, not Nato
12.5% of Corporate tax
Good for be done online

## Malta

EU member, not nato
Good for be done online
Corporate taxes can go down to 5%

## Slovakia

Good for be done online?

## Norway

Good for be done online
Not member of EU (but with unrestricted access to the EU market as a member of the European Economic Area)

## Poland

If the company’s turnover does not exceed 1 million 200 thousand euros, the income tax rate will be 9%

## Estonia

* Can be done with the E-residency
* 
* Corporate tax: 20% (paid at the moment of profit distribution)
* **Forms of business**
  * Limited Liability Partnership or Private Limited Company (OÜ)
    * most common type
    * Minimum capital share of 2500 euros
    * Can not be listed on stock
    * equivalent designation of LLC in the US or Ltd in the UK
    *  
    * you do not need an Estonian bank account to confirm that you paid the share capital of your company
    
  * Public Limited Company (AS)
    * can be traded in stock

  * General Partnership (TÜ)
  * Limited Partnership (UÜ)
  * Commercial Association
  * Sole Proprietor (FIE)
  * non-profit organization (MTÜ)
  * there is also the option to register a branch of an existing foreign company to sell goods or services in Estonia, but this would not be a separate legal entity — the foreign enterprise remains liable for all obligations created in Estonia, and the branch office must adhere to certain criteria set by the Estonian tax board
* **accounting companies**
  * <u>Xolo</u>
    * formerly LeapIN
    * For company registration, there is a one-time fee of €290
    * Go
      * send invoices using Xolo’s legal entity; you don’t need to own a company or even an e-Residency. Deducts VAT and fee of 5.9% from every invoice, then this is sent to your account.
      * you enter a partner agreement wiht them: contract: https://app.xolo.io/terms/partnership-agreement/current
      * service contract: https://app.xolo.io/terms/service-contract/current
    * Leap starter plan: 59e/m, but what is lacking?
    * Leap basic plan: €89 per month, everything included
  * list of others: https://marketplace.e-resident.gov.ee/en/service-providers/
    

## Lithuania

* If your business participates in R&D activities in Lithuania, expenses will be deducted by triple the original amount
* accounting companies:
  * https://tet.lt/
  * https://www.creada.lt/lithuanian-e-residency/
  * https://patikimabuhaltere.lt/en

## Neatherlands

* Wealth tax: ~1.2% tax on the value of your house (minus the amount of her mortgage) yearly
* Income Tax: monthly rent received is tax-free
* Personal income tax: 33.5% -52%, including social security payments.
* Corporate tax: 20.0% for the first €275,000 and above that a corporate tax rate of 25.5%

## Belgium

* Income tax: 25-50%.
* Corporate tax: 34%.

## Spain

* Personal income tax: 20-49%
* Corporate tax will be reduced to 25% in 2016. There is a lower tax rate for newly formed companies: 15% for the first two years in which the company obtains taxable profit
* Profits for freelancers (‘autonomo’) up to €20,000 a year are not taxed. The first tax bracket above that amount is 20%. However much or little you make, monthly payments to social security remain at the same relatively high level: €264 per month.
