[TOC]

# Conceituação

* Inflação
  * aumento continuado e generalizado dos preços dos bens e serviços	



## Fluxo de caixa

* conjunto de entradas e saídas de dinheiro ao longo do tempo
* Se o período do fluxo de caixa não coincidade com a o período de capitalização dos juros, geralmente se converte o período do fluxo de caixa para coincidir com o de juros
* Caixa: dinheiro ( de empresas, de investimentos, de projetos, de operações financeiras, etc)
* Fluxo de caixa líquido: já somando algebricamente as entradas e saídas de cada período
* 
* PV = present value; capital inicial; ou principal, ou capex (capital expenditure): quantidade de dinheiro necessária para inicar um negócio;
* FV = Future value, or final value, ou montante: quantidade total de dinheiro de uma data futura
* 
* 
* os valores monetários só podem ser colocados no início (série antecipativa) ou no final (serie postergativa) de cada período, dependendo da convenção adotada. Geralmente é postergativa
* 
* **Representação por diagrama**
  * ou *Diagrama Padrão do Fluxo de Caixa*
  * representação esquemática
  * os intervalos de tempo de todos os períodos são iguais
  * Periodic PayMenT (PMT, ou PGTO): pagamento regular no tempo
  * ![image-20211023184723751](Images - Matemática financeira/image-20211023184723751.png)
* **represetnação por tabela**
  * uma linha por período
  * uma coluna por tipo de fluxo (entradas, rendimentos, custo de manutenção, etc)
  * uma coluna final com o fluxo líquido
* **Séries uniformes**
  * pagamentos ou recebimntos constantes ao longo do horizonte de análise
  * uma parcela por período
  * Consideraremos sempre juros compostos
  * PMT: valor das parcelas
  * 
  * $FV = PMT [(1 + i)^n - 1] / i$ – Dado PMT achar FV
  * $PMT = FV*i / [(1 + i)^n - 1]$ – Dado FV achar PMT
  * $PV = PMT[(1 + i)^n - 1] / [ i (1 + i)^n]$ – Dado PMT achar PV
  * $PMT = PV[i(1 + i)^n ] / [(1 + i)^n - 1]$ – Dado PV achar PMT
  * <u>série uniforme infinita</u>
    * n tendendo à infinito
    * muito usado para aposentadorias
    * $PV=PMT/i$

### Comparação de fluxos

* 
* descontar um valor futuro FV para o ponto zero: $FV/(1 + n*i)$
* 
* **Valor presente**: soma algébrica de todas as suas parcelas futuras, descontadas para o ponto zero por uma taxa i (denominada taxa de desconto).
* **Valor presente líquido (VPL)**: também inclui na soma algébrica a parcela colocada no ponto zero (valor principal), respeitada a convenção de sinais.
* **Fluxos de caixa equivalentes**
  * seus valores presentes são iguais
  * data focal: data utilizada para comparar fluxos de caixa; utilizaremos sempre a data zero
  * teorema: se dois fluxos são equivalente para uma data focal, então eles são equivalentes para quaquer data focal

* **Taxa interna de retorno (TIR)**
  * or Internal Return Rate (IRR) ,ou *rendabilidade*
  * taxa de desconto i que iguala o Valor Presente Líquido de um fluxo de caixa a zero
  * é uma taxa hipotética
  * quanto maior, melhor
  * se a TIR for maior do que a taxa de desconto atual (da inflação, por exemplo), então o projeto é um bom investimento
  
  * <u>comparação com períodos irregulares</u>
    * convertemos para a menor base (provavelmente dia)
    * convertemos a taxa de juros para essa base (utiliznado a conversão real, via "conversão entre períodos de capitalização")
    * os períodos sem lucro ficarão zerados
    * fazemos o cálculo normalente, que dará a TIR na base calculada
    * convertemos os juros para a base desejada (provavelmente mês)
* **Considerando a inflação**

  * Taxa de Inflação (“ti”): é a taxa que mede a variação do índice teórico
  *  Taxa de Juros Real (“i”): é a taxa de juros utilizada nos fluxos de caixa
    expressos em moeda a preços constantes, sem inflação
  * Taxa de Juros Nominal (“tn”) é a taxa de juros utilizada nos fluxos de caixa
    expressos em $ a preços correntes das respectivas datas em que ocorrem, e que
    incorporam a inflação da moeda
  *  
  * quando fazemos a correção de valores passados pela inflação também dizemos que estamos "trazendo à valor corrente"

## Juros

* Desconto por fora $D$
  
  * expresso em \$
  * $D_f = FV - PV$
  
* taxa de desconto por fora (d)

  * 
  * ?
  * expresso em percentual
  * aplicada sobre o valor futuro FV para produzir o valor presente PV; 
  * praticamente não é utilizada, ainda mais c
  
* 

* desconto por dentro $D_d$

  * ou racional
  * expresso em \$
  * $D_d = FV - PV$
  * ==igual ao desconto do fora?==
  
* taxa de desconto por dentro (i), ou taxa de rentabilidade, ou juros

  * define o valor dos juros
  * expresso em percentual
  * aplicada sobre o valor presente PV para produzir o valor futuro FV
  * 
  
* 

* 

* 

* período de capitabilização: periodicidade com a qual os juros são somado ao capital inicial

* 

* **Regime Simple**
  
  * Juros de cada período são calculados sobre o capital inicial aplicado, ou seja, os juros não
    rendem juros;
  * Crescimento linear (progressão aritmética)
  * `montante = capital_inicial + periods*capital_inicial*juros`
  * $D = (montante\_final/capital\_inicial - 1)/periods$
  * $D_d = (1 - capital\_inicial/montante\_final)/periods$
  * juros proporcionais: períodos de capitalização diferentes, mas levam ao mesmo montante
  
* **Regime composto**

  * Juros de cada período são calculados sobre o capital do início do período, ou seja, os juros rendem juros;
  * Crescimento exponencial (progressão geométrica).
  * os juros devem ser indicados na mesma unidade do período de capitalização (ex: 1%aa, período anual)
  * 
  * caso o resgate não for multíplo do perído de capitalização, calcula-se o proporcional do período incompleto
  * `montante = capital_inicial*(1+juros)**(periods)`
  * $D_d = FV - PV = FV\left(\frac{(1+i)^n - 1}{(1+i)^n}\right)$, 
  * $PV = FV (1-d)^n$
  * <u>taxa de juros efeitiva:</u> concide com o período de capitalização
  * <u>taxa de juros nominal:</u> não é a real rentabilidade, ex: 12%aa, quando na verdade o período é mensal com taxa 1%am
  * <u>taxa de juros equivalentes</u>: períodos de capitalização diferentes, mas levam ao mesmo montante; para calcular, precisa fazer a conversão
  * <u>conversão entre períodos de capitalização</u>
    * exemplo - conversão entre ao ano e ao mês: $(1 + i_a) = (1 + i_m)^{12}$
  * <u>comentários práticos</u>
    * a poupança brasileira tem capitalização mensal
  
* **Juros capitalizados continuamente**

  * Quando menor for o período de cálculo dos juros compostos, mais você ganha

  * Calculando o limite da função genérica quando essa capitalização diminui e aplicando um limite fundamental, chegamos nessa expressão:
    $$
    M=Ce^{i*t}
    $$

  * O mesma dedução pode ser obtida com muito mais facilidade usando uma EDO

* **Outras anotações**

* 
  * 
  
  * 
  * 
  * 
  * valores de datas diferentes são grandezas que só podem ser comparadas e so-
    madas algebricamente após serem movimentadas para uma mesma data, com
    a correta aplicação de uma taxa de juros.

## Financiamentos

* amortização: parte do que é pago que realmente reduz a dívida (ou seja, acima dos juros)
* os custo de pagamento dos juros é sempre calculado sobre a dívida atual (considerando o que já foi pago)
* **Sistema PRICE**
  * ou *sistema frances*
  * prestações constantes
  * $PRESTACAO = PMT = PV[i(1 + i)^n ] / [(1 + i)^n - 1]$ 
  * todas as amortizações são equivalentes quando descontadas para o valor presente
* **Sistema SAC**
  * ou *sistema de hamburgo*
  * amortizações constantes
  * o valor das prestações diminui com o tempo
  * muito usado para financiamentos longos
  * AMORTIZAÇÃO = VALOR FINANCIADO / PRAZO DO FINANCIAMENTO
  * PRESTAÇÃO = JUROS + AMORTIZAÇÃO

# Análise de investimentos

* investimentos em geral
  *  sacrifício hoje em prol da obtenção de uma série de benefícios futuros
  * Investimento Financeiro: compra de títulos financeiros.
  - Investimento de Capital: compra de ATIVOS para o processo produtivo.
  - 

* Alternativa de referencia	
  * Investimento "de conforto", baseline
  * já é investido ou que é de fácil acesso
  * será usada para comparar com investimentos melhores
  * 

* taxa minima de atratividade (TMA)
  * TIR da Alternativa de referencia	

* **Risco**

  * Risco significa estar exposto à possibilidade de um mau resultado (Dan Borge)
  * Risco é a expectativa de perda/insucesso em um intervalo de tempo ou atividade
    (www2.anac.gov.br)
  * Risco é um evento ou condição incerta que, se ocorrer, em um efeito positivo ou
    negativo sobre ao menos um dos objetivos do projeto (Project Management Inst.)
  * Expressão da combinação de severidade do efeito e da probabilidade de
    ocorrência de perda/insucesso (www2.anac.gov.br)


## Métricas de desempenho

* **valor presente liquido**
  * muito utilizado
  * o investimento vale apena se o seu $VPL$ é positivo à taxa $i=TIR_{alternativa}$
  * (+) fácil de ser calculada
  * considera que os recebimentos intermediários são reaplicados com base na taxa de juros, o que é a
    situação mais realista.
  * Ponto de Equilíbrio de Fischer: comparando dois investimentos, é a taxa de juros tal que 
* **TIR**
  * muito utilizado
  * o investimento vale apena se o seu $TIR$ for maior do que $TIR_{alternativa}$
  * (+) fácil de interpretar (percentual)
  * (-) difícil de ser calculada
  * (-) não pode ser utilizada na análise de investimentos com durações diferentes
  * considera que os recebimentos intermediários (quando existirem) são reaplicados com base na TIR
  * <u>Método da TIR Modificada (TIRM)</u>
    * jogar todas as parcelas para a data final (corrigidas pela taxa de juros) e calciular a TIR dessa nova séries
    * exemplo: na série `[-1000, 500, 500, 500]` com `i=1.1`, o novo valor final será `FV = 500*(1.1)^2 + 500*(1.1)^1 + 500,00*(1.1)^0 = 1655` e a TIRM será `18.29%`
* **tempo de recupração do capital**

  * ou *tempo de payback*
  * tempo necessário para recuperar o investimento inicial
  * mede a LIQUIDEZ de uma alternativa de investimento
  * quanto menor o tempo de payback, melhor
  * quanto maior o payback, maior o risco e a incert
  * (+) fácil de interpretar
  * 
  * pode ser calculado pela soma algebrica de cada recebimento ou pela soma _descontada_ para o valor presente
  * Deve-se descontar pelo $TIR_{alternativa}$
  * 
  * payback simples
    * .
  * playback descontado
    * .
* **indice de lucratividade**
  * lucro / investimento?
* **valor anual uniforme equivalente**

## Análise de sensibilidade

* variamos um parâmetro P, vemos quão atrativos os investimentos ficam
* P = taxa de juros, custos operacionais, receitas, etc
* 
* um dos investimentos vai deixar de valer apenas antes do outro, portanto é mais arriscado
* um dos investimentos (não necessariamente o mesmo do de cima) varia mais com a variação dos juros, portanto é mais sensível (tanto para melhor quanto para pior)
*  
* quantificado por meio do coeficiente de inclinação da reta

## Análise de cenários

* especislistas definem vários cenários
* calcula a media e std de cada índice de desempenho (TIR, tempo de payback, etc)
* coeficiente de variação: std / mean; “risco por unidade de retorno”
* ponderar pela probabilidade de ocorrência de cada cenário?

# Imposto de renda

* deprecisão pdoe ser descontada do IR
* se a compra do financiamento foi via financiamentos, os JUROS do financiamento podem ser descontados do IR (não a amortização)
