[TOC]

# Open Banking

* conjunto de padrões e leis que tornam o sistema bancario mais aberto
* Banking as a Service-concept
*  
* compartilhamento de informações bancarias entre bancos
* o dado pertence ao usuário, não ao banco
* open APIs (both from banks and government) to finantial services



# Payment systems

## modern ones

* **Paypal**
  * send and receive money securely
  * linked with your “source” (bank account, debit, or credit card)
  *  intermediary between you and a bank
  * when sending money, you inform the receiver email (paypal account) and the amount will be deducted from your linked source ==or from your paypal balance==
  * They have a feature to generate invoices
* **Echtzeitüberweisung**
  * Or *SEPA Instant Payment*
  * https://de.wikipedia.org/wiki/Echtzeit%C3%BCberweisung
  * just germany?

## National/domestic ones

* Some countries have their own payment systems, which allow for fast payments between different banks
* In the Netherlands there is iDEAL
* In the EU most countries use SWIFT?
* India has Net Banking
* Germany has a system called Giropay
* US offers Secure Vault Payments
* **PIX**
  * ?
* **Philippines’ BSP National QR Code Standard (QRPh)**
  * 

* **Indonesia’s Quick Response Code Indonesia Standard (QRIS)**
  * ?

* **Thailand's PromptPay**
  * develop by Bank of Thailand, but not exclusive to it
  * implemented in 2017
  * some other countries integrate with it (allow payment using the same qr code), like malesia and hongkong
* **US domestic system?**
  * ABA routing numbers: equivalent to IBAN?


# Other Annotaitons

* **SEPA**
  * Single Euro Payments Area
  * standardize cashless payments 
* **global-standard processing networks**
  * VISA, mastercarf, eetc
* **SWIFT**
  * Society for Worldwide Interbank Financial Telecommunication
  * payment system
  * you can receive Swift payments in 17 currencies, including: GBP, USD, EUR, CAD, AUD, NZD, SGD, SEK, NOK, DKK, PLN*, HKD, CHF, CZK, JPY, HUF, BGN
  * established in 1973
  * based in Belgium
  * 
  * <u>what they do</u>
    * define the protocol/standard for the messages
    * host the backend to process the messages (including their IT infra)
    * also sells software and services to financial institutions, mostly for use on its proprietary "SWIFTNet"
    * also assigns ISO 9362 Business Identifier Codes (BICs), popularly known as "Swift codes"
    *  
    * For bank A to send a message to bank B with a copy or authorization involving institution C, it formats the message according to standards and securely sends it to SWIFT. SWIFT guarantees its secure and reliable delivery to B after the appropriate action by C
  * <u>How it works</u>
    * 
    * messaging network through which international payments are initiated
    * carrier of the "messages containing the payment instructions between financial institutions involved in a transaction"
    * After a payment has been initiated, it must be settled through a payment system, such as TARGET2 in Europe
  * <u>Related standards</u>
    * ISO 9362: 1994 Banking – Banking telecommunication messages – Bank identifier codes
    * ISO 10383: 2003 Securities and related financial instruments – Codes for exchanges and market identification (MIC)
    * ISO 13616: 2003 IBAN Registry
    * ISO 15022: 1999 Securities – Scheme for messages (Data Field Dictionary) (replaces ISO 7775)
    * ISO 20022-1: 2004 and ISO 20022-2:2007 Financial services – Universal Financial Industry message scheme
    * in RFC 3615 urn:swift: was defined as Uniform Resource Names (URNs) for SWIFT FIN
  * <u>other annotations</u>
    * US don't use it domestically
    * used in 200 countries
    * most active members get the most voice irrespective of geography
* **CIPS**: by China, for RMB related deals. 1467 financial institutions in 111 countries and regions have connected to the system. The actual business covers more than 4,200 banking institutions in 182 countries and regions around the world.
* **SFMS**: by India
* **SPFS**: by Russian