from finantial_math import *
import math

################
# Questão 1: Os fluxos de caixa a seguir representam dois investimentos mutuamente exclusivos que
# estão sendo analisados por um investidor.
# Considerando que a TMA do investidor é de 40 % a.a.:
# a) Determinar, para cada investimento, os valores de capital inicial (PVA e PVB) que resultam em um
# desempenho econômico semelhante ao da alternativa de referência.
# b) Determinar o capital inicial para que o investimento A apresente uma TIR igual a 51,7 % a.a..
# c) Determinar o capital inicial para que o investimento B apresente uma TIR igual a 46,8 % a.a..
print('\n\n-------------------------------------------')
print('# Questão 1)')

i = 0.4
r = 11500/(1+i)**2
assert_almost_equal(i, serie_arbitraria_get_TIR([-r, 0, 11500])[1])
print(f'a) O investimento A deve ter PV = {r}')

i = 0.4
r = 14000/(1+i)**2
assert_almost_equal(i, serie_arbitraria_get_TIR([-r, 0, 14000])[1])
print(f'   O investimento B deve ter PV = {r}')

i = 0.517
r = 11500/(1+i)**2
assert_almost_equal(i, serie_arbitraria_get_TIR([-r, 0, 11500])[1])
print(f'c) O investimento A deve ter PV = {r}')

i = 0.468
r = 14000/(1+i)**2
assert_almost_equal(i, serie_arbitraria_get_TIR([-r, 0, 14000])[1])
print(f'c) O investimento B deve ter PV = {r}')



#############
#Questão 2: Calcular o VPL dos investimentos mutuamente exclusivos, representados pelos fluxos de
#caixa a seguir, considerando uma taxa mínima de atratividade de 8% a.a. e indicar qual deles deve ser
#escolhido pelo investidor.
print('\n\n-------------------------------------------')
print('# Questão 2)')
r1 = serie_arbitraria_get_VPL([-20000, 7500, 7500, 7500, 7500, 7500], 0.08)
print(f'VPL do investimento A: {r1}')

r2 = serie_arbitraria_get_VPL([-30000, 8500, 8500, 8500, 8500, 8500], 0.08)
print(f'VPL do investimento B: {r2}')

if r1>r2:
    print('O investimento 1 é melhor')
else:
    print('O investimento 2 é melhor')

#Questão 3: Uma empresa está analisando a troca de um equipamento e dois novos modelos estão
#sendo analisados. O modelo A custa $ 15.000,00, tem vida útil de 5 anos e proporcionará uma economia
#anual de $ 12.000,00. O modelo B custa $ 25.000,00, tem vida útil de 5 anos e proporcionará uma
#economia anual de $ 18.000,00. Sabendo-se que o novo modelo adquirido será necessário por 10 anos e
#que o custo de capital (TMA) da empresa é de 5 % a.a., escolha qual dos 2 novos modelos deve ser
#adquirido, utilizando para isso, o método do VPL.
print('\n\n-------------------------------------------')
print('# Questão 3)')

r1 = serie_arbitraria_get_VPL([-15000, 12000, 12000, 12000, 12000, 12000 - 15000, 12000, 12000, 12000, 12000, 12000], 0.05)
print(f'VPL do equipamento A: {r1}')


r2 = serie_arbitraria_get_VPL([-25000, 18000, 18000, 18000, 18000, 18000 - 25000, 18000, 18000, 18000, 18000, 18000], 0.05)
print(f'VPL do equipamento B: {r2}')

print(r1, r2)
if r1>r2:
    print('O equipamento 1 é melhor')
else:
    print('O equipamento 2 é melhor')



#Questão 4: Os fluxos de caixa a seguir representam dois investimentos mutuamente exclusivos que
#estão sendo analisados por um investidor.
# Considerando que a TMA do investidor é de 20 % a.a.:
print('\n\n-------------------------------------------')
print('# Questão 4)')
inv_A = [-15000, 0, 25500]
inv_B = [-16500, 0, 30000]
TMA = 0.2
convergiu, TIR_A = serie_arbitraria_get_TIR(inv_A)
assert convergiu
if TIR_A>TMA:
    print(f'Investimento A é viável (TIR={TIR_A})')
else:
    print(f'Investimento A NÃO é viável (TIR={TIR_A})')

convergiu, TIR_B = serie_arbitraria_get_TIR(inv_B)
assert convergiu
if TIR_B>TMA:
    print(f'Investimento B é viável (TIR={TIR_B})')
else:
    print(f'Investimento B NÃO é viável (TIR={TIR_B})')


if TIR_A>TIR_B:
    print(f'Investimento A é melhor')
else:
    print(f'Investimento B é melhor')

r = get_fisher_point(inv_A, inv_B)
print(f'Fonto de fisher = {r}')
print(f'VPL_A = {serie_arbitraria_get_VPL(inv_A, r)}')
print(f'VPL_B = {serie_arbitraria_get_VPL(inv_B, r)}')


###########
# Questão 5: A tabela a seguir representa o fluxo de caixa referente à compra de um equipamento que
# tem vida útil de 4 anos, com 100 % de recursos próprios (sem financiamento).
print('\n\n-------------------------------------------')
print('# Questão 5)')
df = analisar_investimento_IR(
    investimento_inicial = 180000,
    valor_residual = 30000,
    receitas = [60000]*4,
    custos = [-10000]*4,
    IR = 0.3,
    #financiamento_perc=2/3,
    #financiamento_i=0.12,
    #financiamento_f=financiamento_sac,
)


print('**SEM financiamento:**')
df['FluxoSemIR'] = df['Fluxo'] - df['IR']
print(f"VPL sem IR: {serie_arbitraria_get_VPL(df['FluxoSemIR'].to_list(), i=0.03)}")
print(f"VPL com IR: {serie_arbitraria_get_VPL(df['Fluxo'].to_list(), i=0.03)}")
print(df.to_markdown())

print('\n\n**COM financiamento:**')
df = analisar_investimento_IR(
    investimento_inicial = 180000,
    valor_residual = 30000,
    receitas = [60000]*4,
    custos = [-10000]*4,
    IR = 0.3,
    financiamento_perc=1/2,
    financiamento_i=0.08,
    financiamento_f=financiamento_price,
)
print(df.to_markdown())
print(f"\n\nPrimeira parcela do IR: R$ {-df.loc[1, 'IR']}")
