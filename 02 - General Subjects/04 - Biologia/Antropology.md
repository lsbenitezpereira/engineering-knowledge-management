[TOC]

# Conceptualization

* Branch of social sciences that studies culture and society. 
* **History**

* * they believed that science showed that Europeans and Americans were biologically, mentally, and socially superior to others. 
  * “The savage is morally and mentally an unfit instrument for the spread of civilization except when, like the higher mammalia, he is reduced to a state of slavery,” wrote August Lane Fox Pitt-Rivers, a British army colony and Cannibal Club member
  * “all societies run through the same stages in their evolution… from simpler forms of organization—families, brotherhoods, tribes—to modern, complex nation states.” Lewis Henry Morgan, a businessman and part-time scholar from Rochester
  * 
  * Victorian “anthropologists” assumed they needed to study culture, biological evolution, and archeology together to understand humans. By the late twentieth century, however, anthropologists no longer believed that biology was destiny, and the study of culture and society had become a discipline that was (mostly) separate from the study of human biology and evolution; the former was called “social” or “cultural” anthropology;VI the latter “physical” anthropology. The boundary was not (and is not) rigid; 

# Social Anthropology

* defined less by a single theory than its distinctive way of looking at the world
* Symbols and rituals are powerful precisely because they reflect and reinforce cultural patterns of which we are (at best) only dimly aware. 
* **Ethnography**
  * method that anthropologists typically employ to study people (open-ended, intense, face-to-face observation)
  * true “participant observation” was hard to achieve, since the mere presence of a researcher in a society tends to change what is being studied
* **Other annotations**
  * concept of habitus developed by Bourdieu, the French anthropologist.28 This theory argues that the way humans organize space reflects the mental and cultural “maps” we inherit from our surroundings—but as we move about that space, with familiar habits, these actions reinforce these shared mental maps, and make them seeming so natural and inevitable that we don’t notice them at all. We are creatures of our environment in a social, mental, and physical sense, and these aspects intensify one another (hence the reason “habit” and “habitat” have the same linguistic root in English)
  * “Humans are symbolizing, conceptualizing, meaning-seeking animals,” Clifford Geertz—who 
  * anthropologists are increasingly studying networks, talking to people in different places, who were not a single physical community, but nonetheless connected. 


# Physical anthropology

* Human history, as something separate from the history of animals, began there about 7 million years ago

* 4 million years ago: upright posture

* Australopithecus 		→ 3 milhões de anos

* 2.5 million years ago: Stone tools became common

* Homo habilis 		→ 2,4 e 1.5 milhões: is not a direct anscentor of Sapiens, and both branched out of Australopithecus: coexisted in time with sapiens (but probably not in space and have not interbreed)???

* Homo erectus 		→ 8 milhões até 300.000 aC

* 500k years ago: use of fire? or earlier?

* Homem de Neandertal 	→ 230.000 até 30.000aC;  competed with Homo sapiens from 130,000 to 40,000

* Homo Sapiens 		→ 120.000; evolved (branched out) from homo erectus (coexisted for a while, for probably not in the same region)

* 

* 50k years ago: Great Leap Forward

  * People termed Cro-Magnons
  * Begun probably in East Africa, then they expanded to the rest of the world (sustituing the poulations in Eurasia and colonizing the others)
  * More advanced tools
  * Art
  * Language
  * 40k BC: Lion-man sculture (first known fictional entity);  Aurignacian culture of the Upper Paleolithic; germany
  * 15k BC: animal-human paintings in the Grotte de Gabillou (france)

* 13k BC: first inital sedentary lifestyle, in some areas of the world

* 11k B.C: Americas were first settled around (but maybe sooner)

  * what was sea at that time? there was a corridor of dry land connecting Asia to Australia? https://en.m.wikipedia.org/wiki/Sundaland
  * :man_scientist: new findings? https://www.nature.com/articles/d41586-020-02137-3 sugests about 20kBC

* The spread of humans around the world:

  ![image-20191208134100292](Images - Antropology/image-20191208134100292.png)

  ![image-20200920002052294](Images - Antropology/image-20200920002052294.png)

* ?:

    ![image-20200906174648687](Images - Antropology/image-20200906174648687.png)

## dating techniques

* **Obsidian hydration**
  * Once mined, the water content
  falls at a known rate, allowing archaeologists to
  calculate the date a piece of obsidian was mined
* **radiocarbon dating**
  * a powerful technique used by archaeologists to date
  the age of organic remains
  * gives an estimate of when the tree was cut down
* **Arcada dentária**
  *  Em
    muitos átomos, o spin resultante de todos os elétrons é zero (a quantidade de elétrons
    “spin para cima” é igual à de elétrons “spin para baixo”). Se esses átomos forem
    ionizados e perderem um elétron, no entanto, o spin resultante do íon que permanece
    será não nulo. Isso acontece naturalmente no esmalte do dente, onde a ionização é
    causada pela radioatividade no ambiente. Quanto mais tempo um dente for exposto,
    mais íons estarão presentes. Para determinar a idade dos dentes fósseis, como os
    encontrados neste crânio de Homo neanderthalensis, uma amostra do esmalte é
    colocada em um campo magnético forte. Os spins do íon se alinham em sentidos
    opostos a seu campo (tornando-se “spin para baixo”). A amostra, então, é iluminada
    com fótons de micro-ondas com a energia exata para inverter os spins para a
    configuração de energia mais alta alinhada com o campo (“spin para cima”). A
    quantidade de energia de micro-ondas absorvida nesse processo (chamada
    ressonância de spin do elétron) indica o número de íons presentes e, portanto, a
    idade do esmalte.

## Other annotaitons

Homo sapiens sapiens: aquele que sabe que sabe (ou seja, mais do que técnica, o homem pensa, reflete, compreende). ==isso tem algum fundamento????==