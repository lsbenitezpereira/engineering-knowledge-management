Autotróficos e heterotróficos
=============================

-   Produz seu próprio alimento, ou come os outros?

<!-- -->

-   **Autotróficos**

    -   Produzem o próprio alimento

    -   **Quimiossintetizantes**

        -   Sobrevivem da energia liberada pela oxidação de matérias
            inorgânicas.

        -   Bactérias e arqueobactérias

    -   **Fotossintetizantes**

        -   Detalhes em "*citologia -- obtenção e armazenamento de
            energia"*

-   **Heterotróficos**

    -   Consumidores

    -   **Carnívoros** (carne: leão, tigre, onça)

    -   **Herbívoros** (vegetais: cavalo, cabra, vaca)

    -   **Onívoros** (ambos: humanos, urso, suricato)

    -   **Hematófagos** (sangue: mosquito, pernilongo, piolho.
        Geralmente são parasitas).

    -   **Ictiófagos** (peixe: águia pescadora, leão marinho)

    -   **Coprófagos** (fezes: algumas espécies de mosca e besouro)

    -   **Ornitófagos (**carne de ave: falcão peregrino)

    -   **Insitífagoros** (insetos: sapo)

    -   **Detritívoros** (detritos animais e vegetais: abutre, hiena,
        urubu)

    -   **Planctófagos** (plâncton: flamingo e arraia).

Fluxo de energia
================

-   A fotossíntese utiliza apenas uma pequena parcela (1 a 2%) da
    energia total que alcança a superfície (que já é só metade do que
    chega ao planeta)

-   Essa energia é suficiente para sustentar todas os ciclos que se
    seguem

![http://image.slidesharecdn.com/aula52012ecossistemas-140505024115-phpapp02/95/aula5-2012-ecossistemas-18-638.jpg?cb=1399257764](./Images/11 - Fluxo de energia/media/image1.jpeg){width="5.430660542432196in"
height="3.641509186351706in"}

-   Obviamente, as relações naturais são mais complexas que a cadeia
    alimentar

-   A teia alimentar representa menor como essas relações de fluxo de
    energia se dão:

![](./Images/11 - Fluxo de energia/media/image2.png){width="7.263888888888889in"
height="6.084722222222222in"}

-   A energia diminui de um nível trópico para o outro grande parte da
    energia (+- 90%) sempre é perdida na forma de calor

-   Quanto maior a diferença de energia entre um nível outro, menor a
    eficiência energética.

-   Ex: uma plantação de arroz de 40000m2 pode alimentar, durante todo o
    ano, 24 pessoas. Se esse arroz for usado para alimentar gado e só
    depois o homem, a carne produzida alimenta apenas 1 pessoa.

![http://www.sobiologia.com.br/conteudos/figuras/bio\_ecologia/piramide\_energia.jpg](./Images/11 - Fluxo de energia/media/image3.gif){width="4.989211504811898in"
height="2.009433508311461in"}

-   Sendo a energia menor, a quantidade também diminui:

![Resultado de imagem para pirâmide
ecologica](./Images/11 - Fluxo de energia/media/image4.gif){width="2.849057305336833in"
height="1.4331681977252844in"}

-   **Produtividade**

    -   Produtividade primária bruta (PPB)

        -   Energia total assimilada na fotossíntese

        -   Proporcional à área e ao tempo.

        -   Regiões tropicais mais luz, temperatura e umidade PPB muito
            maior

    -   Produtividade primária liquida (PPL)

        -   Energia efetivamente acumulada na biomassa

        -   $PPB\ –\ energia\ perdida\ pela\ planta\ (basicamente\ pela\ respiração)$

    -   Produtividade líquida da comunidade (PLC)

        -   $PPL\ –\ energia\ perdida\ pelos\ herbívoros$

![http://www.sobiologia.com.br/conteudos/figuras/bio\_ecologia/produtividade\_ecossistema.jpg](./Images/11 - Fluxo de energia/media/image5.jpeg){width="4.0in"
height="1.7517432195975504in"}

![](./Images/11 - Fluxo de energia/media/image6.png){width="4.863438320209974in"
height="3.6415102799650043in"}

<http://oficinacientifica.com.br/downloads/Energia%20no%20ecossistema.pdf>
