Sucessão biológica
==================

-   Processo de instalação e desenvolvimento de uma nova comunidade

-   É provável que, no começo, as condições sejam muito adversas

-   Após alcançar a estabilidade (climax), esse ambiente terá uma
    biodiversidade maior e mais complexa

![Mapa Mental: Sucessao
Ecologica](./Images/31 - Dinâmica das populações/media/image1.png){width="7.268055555555556in"
height="5.908333333333333in"}

-   Utilização do fogo para estimular a sucessão

    -   <http://www.sobiologia.com.br/conteudos/bio_ecologia/ecologia25.php>

Curva populacional
==================

-   O quanto a população cresce

-   **Potencial biótico **

    -   Capacidade teórica de uma espécie se reproduzir, em ambiente
        favorável

    -   Influenciado pela taxa de reprodução; habilidade para migração
        ou dispersão; mecanismos de defesa; habilidade para suportar
        condições adversas, etc

-   **Resistência do meio**

    -   Porém o ambiente sempre freia o crescimento

    -   Falta de água ou de alimentos; falta de espaço ou hábitat
        conveniente; presença de predadores, parasitas; doenças;
        competição, disponibilidade de luz, etc

-   Conceitos aprofundados em *geografia humana -- conceitos
    demográficos*

![https://thinkbio.files.wordpress.com/2012/03/f3-3.jpg?w=300&h=196](./Images/31 - Dinâmica das populações/media/image2.jpeg){width="3.7301246719160104in"
height="2.452173009623797in"}![](./Images/31 - Dinâmica das populações/media/image3.png){width="3.460870516185477in"
height="3.2659142607174103in"}

Dinâmicas ecológicas
====================

-   Como uma coisa afeta a outra

-   **Dinâmicas de predatismo **

    -   Quando sobra presa, aumentam os predadores

    -   Se tem muito predador, diminuem as presas

![Predatismo  A relação predador- presa demonstra o acompanhamento do
\...](./Images/31 - Dinâmica das populações/media/image4.jpeg){width="3.741833989501312in"
height="3.0525021872265965in"}

-   **Dinâmicas de competição**

    -   Uma das espécies tende a desaparecer

-   **Dinâmicas de interferência humana**

    -   Sempre fodem tudo

> ![](./Images/31 - Dinâmica das populações/media/image5.png){width="6.478507217847769in"
> height="6.46875in"}
