Conceituações
=============

-   **Efluente** → qualquer líquido ou gás gerado nas diversas
    atividades humanas e que são descartados na natureza

-   **Resíduos sólidos** → lixo

-   **Chorume** → líquido proveniente do lixo

-   **Gás metano**

    -   Proveniente da degradação anaeróbia (por bactérias) de matéria
        orgânica

    -   Biodigestor aproveita o gás metano (pode ser acoplado a uma
        composteira)

<!-- -->

-   **Magnificação trófica**

    -   Ou *Bioacumulação*

    -   Acumulação de metais através da cadeia alimentar

    -   Absorção e retenção de substâncias tóxicas no organismo de um
        ser vivo

    -   Quanto maior o nível trófico, maior a acumulação

    -   Exemplo: inseticida DDT, mercúrio, etc

![Resultado de imagem para
bioacumulação](./Images/40 - Resíduos sólidos/media/image1.jpeg){width="4.3125in"
height="4.750317147856518in"}

-   **Biorremediação**

    -   Pode ser utilizado para resíduos sólidos, líquidos e gasosos.

    -   Utilização de microrganismo/plantas para limpeza ou
        descontaminação

    -   Normalmente são geneticamente modificados

    -   Transformam o poluente em algo menos danoso

    -   Pode ser usado para tratar rios poluídos

        -   <http://hypescience.com/biorremediacao/>

    -   Cuidado: os locais tratados devem estar preparados para suportar
        esse microrganismo.

Classificações 
===============

-   **Quanto à origem**

    -   Domiciliar

    -   Hospitalar

    -   Comercial

    -   Agrícola

    -   Industrial

    -   Outros

-   **Quanto ao tipo**

    -   Reciclável

    -   Não reciclável

-   **Quanto à periculosidade **

    -   Classe I (perigosos)

        -   Não perigosos

    -   Classe II a

        -   Inertes

    -   Classe II b

        -   Não inertes

Soluções coletivas
==================

-   Devem ser implementadas em grande escala

-   **Lixão**

    -   Céu aberto

    -   Sem planejamento, controle nem monitoramento

    -   Segundo o IBGE, 50,8% dos municípios brasileiros usam
        lixões (2008)

![Lixo](./Images/40 - Resíduos sólidos/media/image2.jpeg){width="6.670138888888889in"
height="2.4243055555555557in"}

-   **Aterro controlado**

    -   Controle do lixo que entra (evitando produtos tóxicos)

    -   Monitoramento

    -   Compactação do lixo

    -   Solo coberto

-   **Aterro sanitário**

    -   Impermeabilização do terreno

    -   Chorume e gazes poluentes coletados e tratados

    -   Além de tudo o que foi implementado no aterro controlado

![Resultado de imagem para aterro
sanitario](./Images/40 - Resíduos sólidos/media/image3.gif){width="6.329861111111111in"
height="3.866832895888014in"}

Soluções individuais
====================

-   Podem ser implementadas em pequena escala

-   **Separação do lixo reciclável **

<!-- -->

-   **Compostagem **

    -   Reaproveitamento de resíduos orgânicos

    -   Produz fertilizantes

    -   [Procedimento]{.underline}

        -   Deve ser revirado para homogeneizar

        -   Deve ser umedecido diariamente para permitir a ação dos
            microorganismos

        -   Intercalar lixo com capim/grama/folhas

Bônus
=====

-   **Desastre de Mariana **

    -   Rompimento de uma barragem de rejeitos de mineração da Samarco

    -   Novembro de 2015

    -   17 mortos e 2 desaparecidos

    -   A lama atingiu o Rio Doce

    -   Sua foz era um dos maiores pontos de desova de peixes marinhos
        do mundo.

<!-- -->

-   Lixo eletrônico
