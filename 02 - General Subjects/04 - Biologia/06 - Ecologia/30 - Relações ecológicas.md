Conceituação
============

-   [Intra]{.underline}específicas mesma espécie

-   [Inter]{.underline}específicas espécies diferentes

-   Harmônica sem prejuízo para nenhuma das partes

-   Desarmônica uma das partes se fode

Intraespecífica harmônica
=========================

-   Indivíduos da mesma espécie se ajudando

-   **Colônias**

    -   Profundo grau de interdependência

    -   Pode ou não haver divisão de trabalho (uns especializados na
        reprodução e outros no deslocamento, etc)

    -   Corais, esponjas, bactérias, cracas, etc

![Resultado de imagem para colonias
biologia](./Images/30 - Relações ecológicas/media/image1.jpeg){width="3.4811318897637795in"
height="2.318520341207349in"}

-   **Sociedades**

    -   Podem viver separados, mas é mais vantajoso viver juntos.

    -   Geralmente ocorre divisão de trabalho

![Resultado de imagem para formigueiro
biologia](./Images/30 - Relações ecológicas/media/image2.jpeg){width="4.732937445319335in"
height="3.160378390201225in"}

Intraespecíficas desarmônicas
=============================

-   Mesma espécie se pegando no pau

-   Geralmente acontece em disputas por território, alimento, etc

-   Diretamente relacionada com a densidade populacional

-   **Canibalismo**

    -   Mata e come

    -   Aranhas, escorpiões, peixes, roedores, planárias, humanos, etc.

Interespecífica harmônica
=========================

-   Espécies diferentes se ajudando

-   **Comensalismo**

    -   Uma das espécies (comensal) se alimenta dos restos da outra sem
        prejudica-la

    -   Significado: "aquele que come à mesa do outro"

    -   Ex: Rêmora é um peixe que gruda no tubarão e come seus restos

    -   Ex: Entamoeba coli é bactéria do nosso intestino e come o que
        viraria cocô.

![Resultado de imagem para
rêmora](./Images/30 - Relações ecológicas/media/image3.png){width="3.0938724846894137in"
height="2.1509437882764653in"}

-   **Inquilinismo**

    -   Uma das espécies se abriga na outra sem prejudica-la

    -   Ex: o peixe agulha se abriga no pepino-do-mar

    -   Ex: orquídeas e bromélias (epifitismo)

![Resultado de imagem para
inquilinismo](./Images/30 - Relações ecológicas/media/image4.jpeg){width="3.754861111111111in"
height="2.48125in"}

-   **Mutualismo**

    -   Ambas são beneficiadas

    -   Profunda interdependência um só vive na presença do outro

    -   Bactérias/fungos e raízes de plantas

        -   A primeira fornece nitrogênio

        -   A segunda matéria orgânica

    -   Cupins e protozoários

        -   Os cupins comem madeira, porém não tem celulase (enzima que
            digere)

        -   Os protozoários que existem em seu intestino fazem essa
            digestão

    -   Ruminantes e microrganismos

        -   Mesma coisa que o caso anterior

-   **Protocooperação**

    -   Ambas são beneficiadas

    -   Podem viver separadas

    -   Pássaros que promovem a dispersão das plantas

    -   Pássaros que comem carrapatos e bois.

Interespecífica desarmônica
===========================

-   Espécies diferentes competindo entre si

-   Geralmente quando possuem o mesmo nicho ecológico, ou seja, vivem na
    mesma região, competem pela mesma comida, etc.

-   **Antibiose**

    -   Ou *amensalismo*

    -   Uma espécie bloqueia o crescimento ou a reprodução de outra
        (amensal), através de substâncias tóxicas

    -   Ex: antibióticos mantando bactérias

    -   Ex: algumas raízes liberam substâncias tóxicas e impedem o
        crescimento de outras plantas.

-   **Parasitismo**

    -   Uma espécie (parasita) se instala no corpo da outra
        (hospedeiro), sugando nutrientes

    -   Causa desde incômodos até a morte

-   **Predatismo**

    -   Um come o outro

    -   Forma a espécie predada a se adaptar

    -   Mimetismo

        -   O animal se parece com outro

        -   Ex: cobra coral e falsa coral

    -   Camuflagem

        -   A espécie confunde-se com o ambiente

        -   Ex: cor da pelagem.

    -   Aposemantismo

        -   O animal apresenta cores de advertência

        -   Dão a entender que o animal é venenoso ou tem gosto muito
            desagradável

![Mapa Mental: Relacoes
Ecologicas](./Images/30 - Relações ecológicas/media/image5.png){width="7.268055555555556in"
height="5.608615485564305in"}
