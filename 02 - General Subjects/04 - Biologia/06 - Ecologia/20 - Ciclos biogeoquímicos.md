Ciclo do nitrogênio
===================

-   O nitrogênio é fundamental para a vida composição e controle
    metabólico

-   78% da atmosfera é constituída de nitrogênio

-   Porém, a maioria dos organismos não consegue utilizá-lo na forma
    gasosa.

-   **Fixação**

    -   Transformação do gás N~2~ em substâncias aproveitáveis (nitrato
        NO~3~ e amônia NH~3~^+^)

    -   Realizado por bactérias nitrificantes

-   **Amonificação**

    -   Amônia originada da decomposição de matéria orgânica
        morta/excretada

    -   Realizada por fungos e bactérias (decompositores)

-   **Nitrificação**

    -   Transforma a amônia em nitrato

    -   Paralela à amonificação

-   **Assimilação**

    -   Absorção do nitrato e da amônia pelos seres vivos

    -   Introdução do nitrogênio na cadeia alimentar

-   **Desnitrificação**

    -   Transformação (por bactérias desnitrificantes) do nitrato em
        nitrogênio molecular

    -   Nitrogênio volta a atmosfera, fechando o ciclo

![http://www.sobiologia.com.br/conteudos/figuras/bio\_ecologia/ciclo\_nitrogenio.jpg](./Images/20 - Ciclos biogeoquímicos/media/image1.jpeg){width="4.880571959755031in"
height="3.1226410761154857in"}

-   **Curiosidade**

    -   A plantação por rotação de culturas ajuda a manter o solo rico
        em nitrogênio.

    -   Ex: intercalar a cana (retira o nitrogênio do solo) com
        leguminosas, como o feijão (devolvem o nitrogênio para o solo).

Ciclo da água
=============

-   Detalhes em *geografia física -- hidrografia*

-   **Drenagem**

    -   Na superfície a água pode: escorrer, infiltrar-se no solo ou
        evaporar.

    -   Micro drenagem → água dos telhados/edificações até os corpos
        recepotores

    -   Macro drenagem → grandes corpos receptores (rios, diques, redes
        de esgoto, etc)

![Resultado de imagem para ciclo da
água](./Images/20 - Ciclos biogeoquímicos/media/image2.jpeg){width="5.886805555555555in"
height="4.094444444444444in"}![http://www.os5elementos.com/wp-content/uploads/2012/11/hidrografia.png](./Images/20 - Ciclos biogeoquímicos/media/image3.png){width="4.183214129483814in"
height="2.594339457567804in"}

Ciclo do carbono e oxigênio
===========================

-   Podem ser estudados como ciclos separados

-   Porém estão intimamente ligados

-   Fotossíntese

    -   Retira CO2 da atmosfera

    -   Libera O2

-   Respiração celular e queima/combustão

    -   Consome O2

    -   Libera CO2 ou CO

-   Demais processos

    -   Estratosfera O2 é transformado - pela radiação - em O3 (ozônio)

    -   O ciclo do carbono está diretamente relacionado ao efeito estufa
        (ver detalhes em *geografia física -- fenômenos climáticos)*

![Resultado de imagem para ciclo do
oxigênio](./Images/20 - Ciclos biogeoquímicos/media/image4.jpeg){width="5.607884951881015in"
height="4.0in"}

![http://www.sobiologia.com.br/conteudos/figuras/bio\_ecologia/ciclo\_carbono.jpg](./Images/20 - Ciclos biogeoquímicos/media/image5.jpeg){width="4.5675612423447065in"
height="4.320754593175853in"}

Ciclo do Fósforo
================

-   Fósforo material genético e molécula de ATP

-   Não passa pela atmosfera

-   Ciclo [eco]{.underline}lógico curto recicla-se entre os consumidores
    e decompositores

-   Ciclo [geo]{.underline}lógico longo formação e sedimentação das
    rochas

![http://www.sobiologia.com.br/conteudos/figuras/bio\_ecologia/ciclo\_fosforo.jpg](./Images/20 - Ciclos biogeoquímicos/media/image6.jpeg){width="3.754861111111111in"
height="3.236111111111111in"}

Ciclo do Cálcio
===============

-   Cálcio ossos, conchas, coagulação do sangue, entre outros.

-   Formação e desgaste das rochas

![Resultado de imagem para ciclo do
cálcio](./Images/20 - Ciclos biogeoquímicos/media/image7.jpeg){width="5.603550962379702in"
height="3.8677537182852144in"}

Ciclo do Enxofre
================

-   Enxofre produção de ácido sulfúrico, fertilizantes, corantes e
    explosivos

-   Provem de vulcões e demais fenômenos geológicos

-   Depositam-se principalmente nas rochas

-   É um dos causadores da chuva ácida (detalhes em *geografia física --
    fenômenos climáticos)*

![Resultado de imagem para ciclo do
enxofre](./Images/20 - Ciclos biogeoquímicos/media/image8.png){width="7.268055555555556in"
height="3.7243044619422574in"}
