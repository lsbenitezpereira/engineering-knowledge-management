Conceitos ecológicos
====================

-   Ecologia Grego Oikós (casa) e logos *(*estudo) estudo do habitat

-   **Espécie**

    -   Conjunto de indivíduos semelhantes

    -   Estruturalmente, funcionalmente e bioquimicamente

    -   Se reproduzem naturalmente, originando descendentes férteis.

    -   Ex.: Homo sapiens

-   **População**

    -   Conjunto de indivíduos de uma **mesma espécie** em uma mesma
        região e tempo.

    -   Ex: 10 mil pessoas vivendo na cidade X em 1996

-   **Comunidade**

    -   Ou *Biocenose*

    -   Conjunto de populações, de **diversas espécies**, habitando uma
        mesma região e tempo.

    -   Ex: conjunto de seres de uma floresta

-   **Habitat**

    -   Onde a espécie pode ser encontrada

    -   Ex: leão pode ser encontrado nas savanas africanas

    -   Ex: Uma planta pode ser o habitat de um inseto etc.

-   **Biótipo**

    -   Fatores físicos do local onde uma comunidade vive

    -   Ex: Solo, água, ar, luz solar, minerais, etc

-   **Nicho ecológico**

    -   Todo o modo de vida de um organismo

    -   O que come, como se reproduz

    -   Quais espécies vivem próximas, de que maneira eles se
        relacionam.

-   **Ecossistema**

    -   Conjunto formado pelo Biótipo e pela comunidade

    -   Ex: mangue, mata atlântica, etc.

    -   Ex: aquário, uma única planta (considerando todos os seres que
        vivem nela e as relações entre eles), etc.

-   **Bioma**

    -   Grandes unidades de classificação

    -   Ex: bioma amazônico.

-   **Biodiversidade**

    -   Diversidade de ecossistemas, espécies e até diversidade genética

-   **Biosfera**

    -   Soma de todos os ecossistemas do planeta

![Mapa Mental: Niveis de Organizacao em
Biologia](./Images/10 - Conceitos ecológicos /media/image1.png){width="7.268055555555556in"
height="5.232129265091864in"}

Legislação básica
=================

-   **Unidade de conservação**

    -   Área delimitada pelo poder público para preservação

    -   Unidade de proteção integral → não pode extrair nada

    -   Unidade de uso sustentável → alia à proteção do local a práticas
        de manejo

    -   Podem ou não serem abertas à visitação

-   **Corredores ecológicos**

    -   Unem fragmentos florestais ou unidades de conservação

    -   Permite o transito de animais e sementes

    -   Ex: tuneis que passam por baixo de uma rodovia

    -   Ex: reconexão de duas pequenas florestas

-   **Lei das águas\#\#**
