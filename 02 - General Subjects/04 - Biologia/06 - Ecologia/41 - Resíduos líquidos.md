Conceituação
============

-   **Esgoto**

    -   99,92% líquido residual

    -   No Brasil, 47,8% dos municípios não têm sistema de esgoto
        (IBGE, 2000)

-   **Tipos de esgoto**

    -   Sanitário → uso higiênico normal

    -   Pluvial → escoamento da chuva "limpa"

    -   Hospitalar → fodido pra caralho

    -   Industrial → altamente tóxico

-   **Tipos de poluição da água**

    -   Química → altera a composição

    -   Física → não altera a composição

    -   Biológica → presença de microrganismos danosos

Soluções Coletivas
==================

-   Devem ser implementadas em grande escala

-   **Rede de esgoto**

    -   Coletora → sob a rua, a 1,5m de profundidade e com declividade

    -   Tronco → confluência das coletoras

    -   Emissário → manda pra ETE

    -   Vista → poço para acesso e manutenção

-   **Estação de Tratamento de Esgoto (ETE)**

    -   Trata até um nível aceitável

    -   Descarta no mar/rio

-   **Lagos**

-   **Soluções Alternativas**

Soluções individuais
====================

-   Tratamento do esgoto sanitário no local de geração

-   **Fossa Séptica**

    -   Decomposição anaeróbica de dejetos orgânicos líquidos

    -   A matéria orgânica resultante pode ser usada para adubo,
        biodigestão ou simplesmente descartada.

    -   A água resultante deve ir para um sumidouro, vala ou filtro.

![](./Images/41 - Resíduos líquidos/media/image1.png){width="6.313194444444444in"
height="1.773611111111111in"}

-   **Sumidouro** → infiltra o esgoto já tratado no solo

-   **Vala de Infiltração** → infiltra o esgoto já tratado através de
    uma vala longitudinal

-   **Filtro anaeróbico** → tratamento complementar à fosse séptica

Eutrofização
============

-   Despejo de esgoto doméstico

-   Matéria orgânica em excesso proliferação de microalgas

-   Muita alga pouca luz entra as algas de baixo começam a morrer

-   As algas mortas são decompostas por bactérias

-   Essa decomposição consome oxigênio, fodendo com as outras formas de
    vida.

![](./Images/41 - Resíduos líquidos/media/image2.png){width="6.081912729658793in"
height="3.6458333333333335in"}
