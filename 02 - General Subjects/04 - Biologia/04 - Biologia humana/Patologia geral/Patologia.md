# Cânceres

## Quimioterapia

 Um método de tratamento do
câncer ósseo é injetar um isótopo
radioativo de estrôncio ( 89Sr) na veia do
paciente. O estrôncio é quimicamente
semelhante ao cálcio, pois, nos átomos
dos dois elementos, os dois elétrons
mais externos estão em um estado s. Então o estrôncio é rapidamente
absorvido pelos tumores, onde a
reposição de cálcio é mais rápida que no
osso saudável. A radiação do estrôncio
ajuda a destruir os tumores.