## Conceptualization

## Indicators

 reproduction number (the average number of
secondary cases each case generates), R

## Interventions

* non-pharmaceutical interventions (NPIs)
  * reduce transmission by reducing contact rates
  * Close schools, etc

## Strategies

* **estrategia no control**
  * 80% pega (dada as características desse corona), letalidade dobra pela falta de tratamento (~2%, pro covid19)
* **mitigação**
  * focuses on slowing but not necessarily
    stopping epidemic spread
  * parar escola, trabalho, etc
  * R>1
  * May include more severe intervention to high risc individuals
* **supressão**
  * aims to reverse epidemic growth. ninguém sai de casa
  * transmission can quickly rebound when interventions are relaxed.
  * R<1

# Epidemias virais

zika virus: parecido com a denge

denge: 4 tipos diferentes 

#  covid-19

* most serious seen in a respiratory virus since the 1918 H1N1 influenza pandemic
* aparaetemente só pega uma vez
* letalidade real de 1% ou menos
*  incubation period of 5.1 days
* $R_0=2.4$
* doubling time of 3~5 days
* people stay in hospital care for a very long time, up to an estimated 3 weeks.
* China informations
  * Wuhan
    * capital of Hubei province
    * population: 11Bi
* China and south corea cases
  * supression
  * worked for the “first round”, we dont know about the future
  * China: large scale supression (cities)
  * South Corea: micro scale supression (individuals). Only possible when testing… almost everyone
* **Previsão - US and Gran Britain (2020/03/20)**
  * unmitigated epidemic, we would predict approximately 510,000 deaths in GB and 2.2 million in the
    US
  *  “optimal” mitigation scenario would still result in an 8-fold higher peak demand on
    critical care beds over and above the available surge capacity in both GB and the US.
* **Previsão - Brasil (2020/03/20)**
  * mais letalidade por causa das condições sociais, menos telidade pela demografia. Eu imagino que no final será basicamente a mesma
  * cenário de mitigração
    * mortalidade +-2%
    * reduce peak healthcare demand by 2/3 and deaths by half
    * 1 milhão de mortos até agosto
  * cenáro de supressão
    * alguns milhares de mortos. Foi o que a china fez
* **Possibilidade de resurgir**
  * :man_scientist: “Impact of non-pharmaceutical interventions (NPIs) to reduce COVID-
    19 mortality and healthcare demand” dá bons modelos do comportamneto a longo prazo da doença





Bielorrúsia mantem apenas uma leve mitigração até hoje (2020/03/31)