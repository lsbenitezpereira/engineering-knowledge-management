# Pergunta 1

Ferro α: ferro puro com estrutura cristalina CCC [1]

Ferrita: mesma coisa que ferro α [1,2]

Perlita: tipo de vidro vulcânico de composição riolítica, com 70 à 75% de $SiO_2$ [3]

Martensita: solução sólida de carbono e ferro com um formato centro-estrutural cristalino cúbico

Martensita revenida: microestrutura muito duro de aço, formada pelo resfriamento rápido da Austenita [4]

Cementita: microestrutura ortorrômbica de aço (6,67% de carbono e 93,33% de ferro), muito dura e quebradiça [5]

Austenita: ferro puro com estrutura CFC [2]

# Pergunta 2

![image-20200329190517855](Images - Atividade Complementar 5 - Leonardo Benitez/image-20200329190517855.png)

# Pergunta 3

**Solda de Estanho-chumbo** [6]

* Composição: estanho e chumbo em proporções diversas, geralmente 70% de estanho e 30% de chumbo 
* Utilização: união de componentes eletrônicos e fios
* Baixo ponto de fusão
* Alta condutividade

**latão** [7]

* Composição: cobre e zinco
* Utilização: espaçadores
* Maciez
* Ductilidade
* Facilidade de trabalho

**Alumínio 1050** [8]

* Composição: alumínio (99,50%) com diversos outros metais
* Utilização: dissipador de calor
* Alta resistência à corrosão
* Boa conformabilidade e soldabilidade
* Baixa resistência mecânica
* Apropriada para anodização decorativa

![image-20200329184407857](Images - Atividade Complementar 5 - Leonardo Benitez/image-20200329184407857.png)





# Referêncas

[1] - https://pt.wikipedia.org/wiki/Ferrita

[2] - https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&ved=2ahUKEwjHhM3M1MDoAhURK7kGHT_yDvQQFjAIegQICRAB&url=https%3A%2F%2Festudomec.info%2Ffiles%2FMCM1_Aulas_1-4_LS.pdf&usg=AOvVaw3u8SB90dbWFfULTCMrcLGl

[3] - https://pt.wikipedia.org/wiki/Perlite_(geologia)

[4] - https://en.wikipedia.org/wiki/Martensite

[5] - https://pt.wikipedia.org/wiki/Cementita

[6] - https://pt.wikipedia.org/wiki/Solda_de_estanho

[7] - Notas de aulas - Slide 5 - Fernando Miranda

[8] - https://shockmetais.com.br/tabelas/aluminio/plig