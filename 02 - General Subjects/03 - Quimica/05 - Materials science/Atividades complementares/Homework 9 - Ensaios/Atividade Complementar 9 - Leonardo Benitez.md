#### 1) Quais são as propriedades mecânicas dos materiais que podem ser obtidas com a realização do ensaio de **tração**? Pesquise e represente uma curva contendo essas propriedades para um material a sua escolha.

Materiais apresentam deformação quando submetidos à tensão, geralmente seguindo a seguinte curva [1]:

![image-20200731124618336](Images - Atividade Complementar 9 - Leonardo Benitez/image-20200731124618336.png)

onde a região elástica refere-se à faixa de tensão onde o material retorna ao formato original após deformado, e a região plástica à faixa onde este não retorna 100%.

Com a aplicação de uma tensão crescente, é possível determinar a curva característica do material, como por exemplo a do cobre [2], muito usado na fabricação de fios condutores de alta tensão:

<img src="Images - Atividade Complementar 9 - Leonardo Benitez/image-20200731130133369.png" alt="image-20200731130133369" style="zoom:60%;" />

![CONTROLE E MONITORAMENTO DE TEMPERATURA DE CONDUTORES EM LINHAS ...](Images - Atividade Complementar 9 - Leonardo Benitez/images)

#### 2) Descreva com maiores detalhes em qual contexto encontramos a escala Brinell

E como avaliar a dureza de um material? 

Uma opção é utilizar o ensaio de Brinell, que consiste em comprimir uma esfera de aço e medir o diâmetro da calota impressa (que será proporcional à deformação do material). É um teste fácil de ser realizado e amplamente utilizado na indústria para analisar metais. 

**Provocação**: o comportamento elástico (deforma e volta) não afeta a medição? Sim, pois o diâmetro da calota diminui com a retirada da esfera [3]:

![image-20200731125259323](Images - Atividade Complementar 9 - Leonardo Benitez/image-20200731125259323.png)

#### 3) Descreva com maiores detalhes o **ensaio Charpy** e quais informações podemos obter em sua realização.

Se um carro bate, o quanto da “porrada” é absorvida pela carroceria e quanto é transmitido para os passageiros? Parte da resposta está nos materiais que compõe a sua estrutura: se eles se deformarem com  muita facilidade o impacto não é amortecido, e se eles se deformarem muito pouco então o momento será transferido diretamente aos passageiros. 

Como medir a energia absorvida por um corpo durante a fratura? Ensaio Charpy!!

Este ensaio consiste em utilizar um corpo de prova com dimensões normatizadas para avaliar quanta energia ele irá absorver de um pêndulo quando o pêndulo impactar contra ele [4]

![1 INTRODUÇÃO](Images - Atividade Complementar 9 - Leonardo Benitez/Fri, 31 Jul 2020 134845.jpeg)



#### Referências

[1] - R. C. Hibbeler. Resistência dos Materiais, editora Pearson Education, São Paulo, 2004.

[2] - Luan Bezerra Silva, Sheila Medeiros de Carvalho. ESTUDO DO COMPORTAMENTO MECÂNICO EM TRAÇÃO DO COBRE ELETROLÍTICO C11000 E DO COBRE CROMO C18200 ENSAIADOS EM ALTA TEMPERATURA. 70º Congresso Anual da ABM, RJ, 2015.
[3] - https://www.youtube.com/watch?v=n4kT3wNJpx4

[4] - https://pt.wikipedia.org/wiki/P%C3%AAndulo_Charpy