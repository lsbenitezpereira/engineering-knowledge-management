## Imperfeições cristalinas em CIs

Imperfeições cristalinas afetam as propriedades dos materiais, podendo, por exemplo, tornar-lo mais eletropositivo ou eletronegativo. Essas dois efeitos citados são amplamente utilizados na indústria eletrônica para criar duas variações do silício puro: uma com maior tendência a recebe elétrons (mais eletronegativa) e outra com maior tendência a doar elétrons (menos eletronegativo), de forma de uma junção de ambos os materiais conduza apenas em um sentido.

Um dos processos para provocar imperfeições de forma controlada é o doping substitucional, onde se substitui um átomo da estrutura cristalina de silício por um outro átomo causando uma imperfeição pontual. O doping pode ser do tipo P ou do tipo N.

## Doping P

Um átomo de silício é substituído por um átomo com 5 elétrons na camada de valência (como o fósforo), de forma que se tenha um elétron livre.

![image-20200329195325666](Images - Atividade Complementar 4 - Leonardo Benitez/image-20200329195325666.png)

## Doping N

Um átomo de silício é substituído por um átomo com 3 elétrons na camada de valência (como o boro), de forma que se tenha uma lacuna.

![image-20200329195401590](Images - Atividade Complementar 4 - Leonardo Benitez/image-20200329195401590.png)

# Referências

SEDRA, A. S., & SMITH, K. C. (2015). *Microelectronic circuits*. New York, Oxford University Press.