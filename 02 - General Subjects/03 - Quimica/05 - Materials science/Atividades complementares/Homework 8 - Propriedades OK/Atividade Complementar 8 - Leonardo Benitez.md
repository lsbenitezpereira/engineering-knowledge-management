## 1) Apassivação

* Os metais corroem por que esse é um estado de menor energia, onde possuem maior estabilidade eletrônica:

<img src="Images - Atividade Complementar 8 - Leonardo Benitez/image-20200426203125480.png" alt="image-20200426203125480" style="zoom:80%;" />

Entretanto alguns metais formam uma película protetora após um período inicial de corrosão, chamada *camada apassivadora*, diminuindo drasticamente a taxa de corrosão. Esse é o caso do aço inoxidável, cujo película protetora consegue suportar até um alto potencial antes da sua ruptura:

<img src="Images - Atividade Complementar 8 - Leonardo Benitez/image-20200426204240948.png" alt="image-20200426204240948" style="zoom:80%;" />



## 2) Mobilidade eletrônica

Sílicio intrínseco: 1450 $cm^2/{Vs}$

Silício dopado P (com boro): 470 $cm^2/{Vs}$

Silicio dopado N (com fósforo): 1414 $cm^2/{Vs}$

Como podemos observar, toda adição de impurezas diminui a mobilidade pois aumenta a probabilidade de colisões na movimentação do elétron:

![image-20200426221032170](Images - Atividade Complementar 8 - Leonardo Benitez/image-20200426221032170.png)

## 3) Efeito fotovoltaico

Elétrons liberado por efeito fotoelétrico em uma junção pn são repelidos em direção aos eletrodos por causa do campo elétrico presente na junção. Com a associação de vários dispositivos semicondutores podemos gerar tensões e correntes consideráveis apenas com a radiação incidente, formando uma placa fotovoltaica.

## Referências

Notas de aula de do professor Chostak, disciplina de Química Geral 1, IFSC.

Microelectronic Circuits (Adel S. Sedra, Kenneth C. Smith), Oxford UniversityPress, 2004

Bart J. Van Zeghbroeck. Electrical, Computer & Energy Engineering from University of Colorado 1996. Disponível em https://ecee.colorado.edu/~bart/book/mobility.htm 











