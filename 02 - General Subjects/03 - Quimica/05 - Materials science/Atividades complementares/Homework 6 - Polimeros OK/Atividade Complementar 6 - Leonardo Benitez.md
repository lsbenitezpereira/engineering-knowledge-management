## 1)

**A – Alcano**
Algumas características: insolúveis em água e solúveis em solventes pouco polares
Exemplo de aplicação: combustíveis
Fonte: [1]

![image-20200410164304691](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164304691.png)

**B – Naftaleno**
Algumas características: volátil, com um odor degradante e que arde
Exemplo de aplicação: agente anti-traça
Fonte: [2]

![image-20200410164324584](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164324584.png)

**C – Álcool etílico**
Algumas características: volátil e inflamável
Exemplo de aplicação: bebidas alcoólicas
Fonte: [3]

![image-20200410164335123](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164335123.png)

**D – Poliamida**
Algumas características: Resistência ao envelhecimento e a temperaturas elevadas durante longos períodos de tempo
Exemplo de aplicação:  carpetes, airbags, patins,relógios, calçados esportivos, entre outros
Fonte: [4]

![image-20200410164344096](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164344096.png)

**E – Polisulfona**
Algumas características: estabilidade em altas temperaturas
Exemplo de aplicação: retardador de chamas
Fonte: [5]

![image-20200410164354052](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164354052.png)

**F – PEEK**
Algumas características: grande resistência química e mecânica
Exemplo de aplicação: isolamento de cabos elétricos
Fonte: [6]

![image-20200410164405084](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410164405084.png)


## 2) 

a) Para substituirmos 8% dos átomos de hidrogênio do polietileno com átomos de cloro devemos devemos utilizar 5,6% de cloro (percentual da massa total de polietileno)

==Errata: acho que a resposta certa é a seguinte:==

Se a cada 1000 moléculas de $C_2H_4$ teremos $h=1000*\frac{4}{6}$ átomos de hidrogênio, dos quais $c=h*0.08$ serão substituidos, então precisamos de $c$ átomos de cloro

Esse átomos de cloro pesam $c*70u$, o que corresponde a $\frac{c*70u}{1000*28u}$ da massa atual de $C_2H_2$  

Ou seja, o percentual de massa que devemos adicionar é $\frac{1000*\frac{4}{6}*0.08*70}{1000*28}=13.3\%$





b) O polietileno clorado difere do polietileno pois o primeiro “permite a criação de centros reactivos que permitem a vulcanização, isto é, obtemos as características de um material susceptível de ser vulcanizado – uma borracha.” [7]. Dessa forma podemos fazer um material mais resistência à ignição, ao calor, à baixas temperaturas e à abrasão [8].



## 3) 

Fibra de poliéster: usado para a fabricação de placas de circuito impresso, que devem possuir boa resistência mecânica e baixa condutividade elétrica. Em comparação com outros materiais, PCIs de políester podem ser utilizadas para circuitos flexíveis como o mostrado na figura abaixo [9]

![image-20200410154401009](Images - Atividade Complementar 6 - Leonardo Benitez/image-20200410154401009.png)

Epóxi: utilizado no encapsulamento de Circuitos Integrados, deve possuir grande resistência mecânica, alto ponto de fusão e suportarem ambientes agressívos em termos de humidade, agentes químicos, impáctos físicos e variações de temperatura [10]

## Referências

[1] - Wipedia, 2020. Disponível em:  https://pt.wikipedia.org/wiki/Alcano

[2] - Wipedia, 2020. Disponível em:  https://pt.wikipedia.org/wiki/Naftalina

[3] - Wipedia, 2020. Disponível em:  https://pt.wikipedia.org/wiki/Etanol

[4] - Wipedia, 2020. Disponível em:  https://pt.wikipedia.org/wiki/Poliamida

[5] - Wipedia, 2020. Disponível em:  https://en.wikipedia.org/wiki/Polysulfone

[6] - Wipedia, 2020. Disponível em:  https://en.wikipedia.org/wiki/Polyether_ether_ketone

[7] - Mário Caetano. CT borracha, 2019. Disponível em:  https://www.ctborracha.com/borracha-sintese-historica/materias-primas/borrachas/borrachas-sinteticas/borrachas-de-polietileno-clorado-cm-epolietileno-cloro-sulfonado-csm/

[8] Siezzo Compostos de Borracha, 2019. Disponível em: http://siezzo.com.br/capacidade/polimeros-de-composto-de-borracha/ce/

[9] Leandro Schwarz e Mayara de Sousa. Notas de aula de *DTE60403 Desenho Técnico*, 2017.

[10] - Priscila Custódio de Matos e Valesca Alves Correa. Estudo preliminar sobre testes de screening e qualificação em circuitos integrados de encapsulament plástico em aplicações especiais. III Congresso Internacional de Ciência, Tecnologia e Desenvolvimento, 2014.