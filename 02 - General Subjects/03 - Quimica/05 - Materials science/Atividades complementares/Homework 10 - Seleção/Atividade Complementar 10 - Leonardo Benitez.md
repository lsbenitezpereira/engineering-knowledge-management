# Material para o gabinete

Após buscar no mercado nacional por gabinetes similares ao desejado para este projeto, identificou-se algumas opções e os materiais utilizados:

* [Opção 1](https://produto.mercadolivre.com.br/MLB-1331099268-uno-r3-case-gabinete-novo-brilho-transparente-acrilico-caixa-_JM?quantity=1#position=14&type=item&tracking_id=8b2d7e65-7172-4ee7-b889-dcd3752444bb): acrílico e policarbonato

* [Opção 2](http://www.patola.com.br/index.php?route=product/product&product_id=36&search=TRANSPARENTE&description=true&page=2): ABS

* [Opção 3](https://www.eletrogate.com/case-para-arduino-uno-em-acrilico-transparente?utm_source=Site&utm_medium=GoogleMerchant&utm_campaign=GoogleMerchant&gclid=EAIaIQobChMIyImR1-y-6wIVk4aRCh23tgK6EAQYASABEgLu9vD_BwE): acrílico

* [Opção 4](https://produto.mercadolivre.com.br/MLB-1610495869-caixa-plastica-pvc-tampa-transparente-280mmx180mmx140mm-_JM?quantity=1#position=9&type=pad&tracking_id=d4992aaa-2892-4416-bbf7-5f4cb271e91a&is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=12&ad_click_id=NTc4NTBmMzktM2NkMS00M2YyLWIwOWQtZWE0Mjg3OTg5NDUx): PVC

Todos esses materiais são moldáveis e podem ser fabricados de forma transparente. O material ABS ainda é comumente vendido em filamentos para utilização em impressão 3D, o que permitiria mais uma opção para a produção do gabinete.

Para permitir uma comparação quantitativa dos materiais, utilizou-se o seguinte Índice de Mérito, levando em consideração que desejamos um material resistente e leve:

$$
IM = \frac{\text{Resistência à tração à 20ºC}}{\text{Densidade à 20ºC}}
$$

* IM Acrílico = $\frac{80 MPa}{1.22 g/cm^3}=65.57$ [1]
* IM Policarbonato = $\frac{50 MPa}{ 1.24 g/cm^3}=40.32$ [2]
* IM ABS = $\frac{37 MPa}{1.03 g/cm^3}=35.92$ [3]
* IM PVC = $\frac{16.5 MPa}{1.35 g/cm^3}=12.22$ [4]

O acrílico (Polymethyl methacrylate) foi o material com melhor Índice de Mérito e, por se tratar de um material facilmente disponível no mercado nacional, trata-se de um material adequada para a produção do gabinete. 


# Referêncas

[1] - https://matmatch.com/materials/mbas087-polymethylmethyacrylimide-pmmi-

[2] - https://matmatch.com/materials/mbas009-polycarbonate-pc-

[3] - https://matmatch.com/materials/greco101-abs-547

[4] - https://matmatch.com/materials/mbas021-plasticized-polyvinyl-chloride-plasticized-ppvc-
