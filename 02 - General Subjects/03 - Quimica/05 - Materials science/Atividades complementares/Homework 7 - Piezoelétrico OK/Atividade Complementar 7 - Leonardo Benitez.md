## Efeito Piezoelétrico

O efeito piezoelétrico foi descoberto em meados da década de 1880 pelos irmãos e
físicos franceses Pierre e Paul-Jacques Currie, que perceberam a capacidade de alguns cristais gerarem tensão elétrica por resposta a uma pressão mecânica. O efeito é bidirecional, e aplicando uma tesão elétrica é possível observar uma deformação do material (expansão ou contração)

> Pressão -> Tensão

> Tensão -> deformação

![image-20200415203556506](Images - Efeito Piezoelétrico/image-20200415203556506.png)

A explicação para o fenômeno está na estrutura cristalina dos materiais que apresentam essa propriedade, a qual possui uma célula unitária com distribuição assimétrica das cargas elétricas. Abaixo vemos a estrutura do quartzo, um material com essa propriedade, e como a aplicação de tensão elétrica provoca uma expansão do material e, de forma análoga, uma compressão gera tensão.

![image-20200415203954912](Images - Efeito Piezoelétrico/image-20200415203954912.png)

## Materiais

Qualquer cristal com a característica descrita acima apresentará o efeito piezoelétrico, mas cabe ressaltar que essa característica é rara. Além de cristais naturais como o quartzo, podemos sintetizar cerâmicas com essa assimetria na estrutura cristalina a partir de Titanato de Bário e Titanato Zirconato de Chumbo (PZT), mostrados abaixo.

![image-20200415204220201](Images - Efeito Piezoelétrico/image-20200415204220201.png)

> Célula unitária do Titanato de Bário

![image-20200415204151337](Images - Efeito Piezoelétrico/image-20200415204151337.png)

> Célula unitária do Titanato Zirconato de Chumbo

## Exemplo de aplicação: transdutor ultrassônico

Para gerar uma onda mecânica podemos fazer oscilar uma superfície, o que pode ser implementado com um cristal piezoelétrico sendo estimulado eletricamente na frequência desejada. Como o efeito piezoelétrico é bidirecional, podemos usar outro cristal que, ao ser deformado por uma onda mecânica, conseguiremos ler a tensão elétrica resultante. Combinando os descritor atuador e sensor, podemos implementar um sensor de distância, que funciona emitindo pulsos ultrassônicos periodicamente e utilizando um temporizador eletrônico para determinar quanto tempo um pulso leva para refletir em um objeto e retornar.

![image-20200415204309205](Images - Efeito Piezoelétrico/image-20200415204309205.png)

## Referências

WEBSTER, J. G. Measumerement, Instrumentation and Sensor Handbook. 1st edition. Florida: CRC Press; 1998.

Notas de aula do professor Jony Silveira, na disciplina de Instrumentação Eletrônica.