# Características

Gráficos plotados com o software Colaboratory utilizando a linguagem Python. Código disponível em https://colab.research.google.com/drive/1XN8cIxoBs_hNQU5aT2WU_C8PxeHpEZlD

<img src="graph.png" alt="img" style="width: 90%;" />

# Aplicações

## Titânio

É um metal resistente porém com baixa densidade (leve), sendo assim um excelente material para estruturas automotivas e aeroespaciais. Também é um material raramente rejeitado pelo corpo humano, sendo muito usado em implantes e próteses [1].

Sua baixa resistividade elétrica o tornaria um bom material para fios rígidos e conectores elétricos, não fosse pelo seu alto custo.

**Referências**

[1] - https://matmatch.com/materials/mama00000121-titanium

## Crômio

É um metal brilhante e com boa resistência à oxidação, motivo pelo qual é muito usado para recobrir peças metálicas [2]. Apesar de possuir densidade similar ao titânio, possui resistência mecânica menor e maior condutividade térmica, sendo portanto pouco utilizado em estruturas. Apesar de possuir resistividade elétrica maior que o titânio, esta ainda é muito baixa para que o material seja utilizado como isolante elétrico (diferentemente do Isopreno).

**Referências**

[2] - https://matmatch.com/materials/good00089-chromium

## Carbeto de silício

É um material cerâmico muito duro e com uma temperatura de fusão excepcionalmente elevada, sendo assim muito utilizado como abrasivo [3, 4]. 

Sua alta resistividade elétrica e alta condutividade térmica faz com que também seja muito utilizado em Resistências elétricas para aquecimento [6].

**Referências**

[3] - https://accuratus.com/silicar.html

[4] - https://en.wikipedia.org/wiki/Silicon_carbide

[5] - http://www.morgantechnicalceramics.com/en-gb/materials/silicon-carbide-sic/

[6] - https://www.macea.com.br/materiais/carbeto-de-silicio

## Isopreno

É um composto orgânico muito usado para a fabricação de borracha sintética (poliisopreno) [9], possuindo essa diversas aplicações, como: pneus, vedações, mangueiras, entre outras.

É o único material desta série líquido à temperatura ambiente (ponto de fusão -146 e ponto de ebuliçãão 34ºC). Sua alta resistividade elétrica torna-o adequado para a sua utilização como isolante elétrico.

[7] - https://pubchem.ncbi.nlm.nih.gov/compound/Isoprene#section=Other-Experimental-Properties

[8] - https://books.google.com.br/books?id=DFo1sZBwdNgC&pg=PA170&lpg=PA170&dq=isoprene+thermal+conductivity&source=bl&ots=YONloAv8ou&sig=ACfU3U1ARhme8DSUpX3vyQhY-pGoyGRbOw&hl=en&sa=X&ved=2ahUKEwib14GVgfPnAhUsCrkGHS8mBDIQ6AEwA3oECAwQAQ#v=onepage&q=isoprene%20thermal%20conductivity&f=false

[9] - https://pt.wikipedia.org/wiki/Isopreno