**1) Utilizando os conhecimentos adquiridos em sala de aula, descreva a**
**relação existente entre os elementos químicos apresentados na coluna**
**do grupo IV A na tabela periódica com os dispositivos semicondutores**
**da área de eletrônica. Explicar as suas estruturas atômicas, tipo de**
**ligações e possíveis combinações com outros elementos químicos não**
**presentes neste grupo.**

Todos os elementos do grupo IVA possuem 4 elétrons na última camada, como podemos observar pelas suas distribuições eletrônicas: 

* Carbono:

![carbono](Images - Atividade Complementar 2 - Leonardo Benitez/carbono(2).jpg)

* Silício:

![silício](Images - Atividade Complementar 2 - Leonardo Benitez/silicio.jpg)

* Germânio:

![germânio](Images - Atividade Complementar 2 - Leonardo Benitez/germanio.jpg)

* Estanho:

![estanho](Images - Atividade Complementar 2 - Leonardo Benitez/estanho.jpg)

* Chumbo:

![chumbo](Images - Atividade Complementar 2 - Leonardo Benitez/chumbo(1).jpg)

* Fleróvio:

![fleróvio](Images - Atividade Complementar 2 - Leonardo Benitez/flerovio.jpg)



Dessa forma, todos eles precisam de mais 4 elétrons para ficarem estáveis. Esses 4 elétrons podem ser obtidos por vários tipos de ligação diferentes, como ligações metálicas (exemplo: um átomo de silício compartilhando elétrons com outros 4 átomos de silício) ou ligações covalente (exemplo: um átomo de carbono fazendo duas ligações duplas com átomos de oxigénio, formando CO2). Diferentes ligações formarão materiais com diferentes propriedades que podem ser usados em diferentes aplicações. Algumas aplicações de cada material: 

* Carbono: utilizado na produção do aço e de sistemas de filtração, etc;
* Silício: utilizado como semicondutor em peças eletrônicas, na composição do vidro, da cerâmica, produtos de silicone, etc;
* Germânio: utilizado na produção de fibra óptica e lentes para microscópios, etc;
* Estanho: utilizado em soldas, na formação de ligas metálicas e na proteção de metais contra a corrosão;
* Chumbo: utilizado na proteção contra raios X, produção de baterias, etc.





**2) O Carbono é um elemento químico pode se apresentar duas formas**
**alotrópicas: em grafite e em diamante. Levando em consideração a**
**configuração eletrônica deste elemento, explique o fenômeno de**
**hibridização para estas duas formas do Carbono.**

O carbono possui 6 elétrons e, portanto, sua distriuição eletrônica é conforme segue:

![image-20200410173434304](Images - Atividade Complementar 2 - Leonardo Benitez/image-20200410173434304.png)

Ou seja, possui 2 orbitais incompleto. Visto que apenas orbitais incompletos podem participar de ligações covalentes, sejam elas do tipo pi ou sigma, então o carbono só poderia fazer 2 ligações covalente com essa configuração eletrônica. Para permitir a realização de mais ligações (e portanto completar a camada de valência), é preciso que ocorra uma **hibridização** dos orbitais, isto é, uma reorganização dos orbitais para aumentar o número de ligações possíveis:

![image-20200410173434304](Images - Atividade Complementar 2 - Leonardo Benitez/image-20200410173434304 (copy).png)

Esse orbitais hibridos podem assumir diferentes geometrias no átomo e realizar números diferentes de ligações sigma e pi, portanto levando a formação de diferentes **estruturas cristalinas**, como por exemplo do grafite (hibridização sp²) e ou diamante (hibridização sp³):

![Universo da Química: INICIAÇÃO À QUÍMICA ORGÂNICA](Images - Atividade Complementar 2 - Leonardo Benitez/New Picture (1).png)

## Referências

https://mundoeducacao.bol.uol.com.br/quimica/familia-carbono.htm

https://www.quimicasuprema.com/2013/09/hibridizacao-de-orbitais.html