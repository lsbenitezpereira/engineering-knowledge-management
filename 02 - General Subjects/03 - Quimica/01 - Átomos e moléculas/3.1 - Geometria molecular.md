Geometria molecular

-   Forma espacial das moléculas

-   **Teoria da repulsão dos pares eletrônicos**

    -   Os pares eletrônicos do átomo central se repelem

    -   Logo, tendem a manter a maior distância possível um do outro

-   O formato depende do número de átomos e o número de pares
    eletrônicos livres do [átomo central.]{.ul}

![http://s3.amazonaws.com/magoo/ABAAAfKXoAL-0.jpg](./Images/3.1 - Geometria molecular/media/image1.jpeg){width="7.25in"
height="6.40625in"}

Seis átomos (5 pares) → bipirâmide

![Geometria octaédrica para molécula com sete
átomos](./Images/3.1 - Geometria molecular/media/image2.jpeg){width="2.8125in"
height="2.8125in"}

Sete átomos (6 pares) → octaédrica

Tem algum migé sobre os orbitais (S, P, D ou F) que entram na ligação,
que os p são perpendiculares aos S, dai a ligação é sempre angulada
entre eles. Pesquise.
