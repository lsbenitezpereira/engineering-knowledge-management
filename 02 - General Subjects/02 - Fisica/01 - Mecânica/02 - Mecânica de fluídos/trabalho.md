

**1)  através de que fórmulas se calcula a velocidade média na saída do tubo  (a partir da coluna inclinada de álcool ligada ao tubo de Pitot) e a  vazão a partir dela e por quê (note que não são utilizadas no cálculo da  velocidade média nem a velocidade na parede nem a velocidade no centro  do tubo);**

​	Em regime permanente e longe da região de entrada, o perfil de velocidade será:

![1561220988536](Images - Mecânica de Fluídos/1561220988536.png)

​	Tendo a velocidade média $V_{avg}$, a vazão será dada por $\dot \forall = V_{avg}A_c$, onde $A_c$ é a área da seção. Portanto, o cálculo da vazão geralmente é feito medindo a velocidade do fluído, visto que a área normalmente é conhecida.

​	Para calcular a velocidade média, precisamos realizar a integração da velocidade em cada ponto da tubulação:

![1561220977919](Images - Mecânica de Fluídos/1561220977919.png)

​	No caso de termos medições discretas (ao invés de uma função contínua), faremos um somatório ao invés de uma integral. Esse é o caso do experimento realizado, na qual a velocidade do fluído foi medida à diversas distância do centro utilizando um tubo de pitot. 

​	Em algumas situações, é possível utilizar um método heurístico que se demonstrou muito preciso, apesar da sua simplicidade. Este consiste em medir a velocidade apenas em algumas distâncias específicas, de forma de que a vazão será calculada pela seguinte fórmula:
$$
\dot \forall = \frac{\sum_{i=0}^n V_i}{n}A
$$
**2) como  se calcula a vazão a partir das leituras do tubo em U contendo água  ligado às tomadas de pressão do tubo de Venturi  (mostre o cálculo de, pelo menos uma das vazões, consulte a seção 8-8 do livro de Çengel e Cimbala).**

​	O tubo de Venturi é um dispositivo de obstrução que permite medir a velocidade média $V_{avg}$ de um fluído. Tal medição é realizada comparando a pressão antes e durante o ponto de constricção.

![1561848442305](Images - trabalho/1561848442305.png)

​	Aplicando a equação da continuidade ($A_1V_1=A_2V_2$), a equação de bernouli (![1561848518094](Images - trabalho/1561848518094.png)) e isolando a equação em função de $V_2$, obtemos que:
$$
V_2=\sqrt{\frac{2(P_1-P_2)}{\rho (1-\beta^4)}}
$$

​	onde $\beta=d/D$ é a razão entre os diâmetros. As pressões podem ser obtidas utilizando um manômetro diferencial (tubo em U), medindo a diferença de altura nas colunas de fluído e aplicando a equação de Stevin. Sabendo a velocidade, a vazão pode ser facilmente determinada por $\dot \forall = A_2 V_2$.

​	Se quisermos levar em consideração a perda de energia devido a não idealidades experimentais, podemos adicionar um fator correção $C_d$ (discharge coefficient):

![1561849039301](Images - trabalho/1561849039301.png)

**3) o que é e para que serve um túnel de vento;**

É um equipamento que permite simular a passagem de ar por um objeto posicionado no túnel. Dessa forma podemos visualizar e estudar o fluxo de fluídos ao redor do objeto, possibilitando analisar situações complexas (turbulências, geometrias complexas, etc) e/ou validar hipóteses teóricas.

Para, por exemplo, estudar o comportamento de uma bola de baseball girando, podemos utilizar um tunel de vento e visualizar o fluxo com fumaça:

![1561225628381](Images - Mecânica de Fluídos/1561225628381.png)



**4) o que é o experimento de Reynolds;**

O experimento de Reynolds permite caracterizar o escoamento de um fluído por um duto de vidro, vizualizando as linhas de fluxo utilizando tinta. Ou seja, permite identificar se o escoamento é laminar ou turbulento.

Em velocidade suficientemente baixas, a tinta segue uma trajetória suave e retilínea, caracterizando assim um escoamento laminar. 

![1561260532520](Images - trabalho/1561260532520.png)

Quando a velocidade é suficientemente alta, percebe que a tinta mistura-se caoticamente, caracterizando assim um escoamento turbulento. 

![1561260552119](Images - trabalho/1561260552119.png)

Em velocidades intermediárias, entretanto, é possível observar uma intermitência entre regiões de escoamento turbulento e laminar, caracterizando assim um escoamento transisional. 

![1561847365446](Images - trabalho/1561847365446.png)

**5) o que é e o que causa a perda de carga.** 

É de grande interesse da engenharia determinar a perda de energia mecânica ao longo de um sistema, que causa uma diminuição no potencial energético (energy grade line, EGL). Na figura abaixo podemos observar como a EGL diminui monotonicamente ao longo da tubulação: 

![1561227198908](Images - trabalho/1561227198908.png)

A perda de energia $h_L$ (de *head loss*) quantifica essa diminuição, pois representa a diminuição no potencial manométrico do fluído. Ou seja, $h_L$ pode ser entendida como a altura adicional que precisaríamos elevar a bomba para superar as perdas friccionais ao longo do sistema.





