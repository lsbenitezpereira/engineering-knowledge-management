* pressão -> tensão

* tensão -> expansão

  ![image-20200415203556506](Images - Efeito Piezoelétrico/image-20200415203556506.png)

* mais comuns em cristais (especialmente quartzo), ou algumas cerâmicas artificiais

* When you apply
  an ac voltage across them, they vibrate at the frequency of the applied voltage.

* **História**

  * Meados da década de 1880.
  *  irmãos e
    físicos franceses Pierre e Paul-Jacques Currie

* **explicação física**

  * Distribuição assimétrica das cargas elétricas na estrutura cristalina do material

  * Exemplo do quartzo:

    ![image-20200415203954912](Images - Efeito Piezoelétrico/image-20200415203954912.png)

* **Cutting the cristal**

  * The natural shape of a quartz crystal is a hexagonal prism with pyramids at the
    ends

## Materiais

* Quartzo

  

* Turmalina

* Cerâmicas sintéticas a base de Titanato de Bário

  ![image-20200415204220201](Images - Efeito Piezoelétrico/image-20200415204220201.png)

* Cerâmicas sintéticas a base de Titanato Zirconato de Chumbo (PZT)

  * ?

    ![image-20200415204151337](Images - Efeito Piezoelétrico/image-20200415204151337.png)

* 

## aplicações

* **Oscilador**
  
  * joga uma onda, ele devolve filtrada na frequencia natural/ressonante desse cristal
  
  * filtro putamente seletivo, muito melhor que qualquer circuito eletronico
  
  * 
  
  * a frequencia se dá pela caraacteristica mecânica do cristal (definida com muita precisão): joga um sinal elétrico alternado, o cristal vibra mas só ressona em uma frequencia muito específica
  
    ![image-20200415204450750](Images - Efeito Piezoelétrico/image-20200415204450750.png)
  
* **ecografia**
  
  * o cristal é um puta sensor pra vibrações 
  
* **microbomba**
  
  * deflete pra um lado, enxe a câmara, deflete pro outro
  
  * valvulas unidirecionais
  
  * mesmo principio do coração humano
  
    ![image-20200415204330856](Images - Efeito Piezoelétrico/image-20200415204330856.png)
  
* **Sensor ultrassônico**

  resistência quando submetidos a
  um esforço mecânico![image-20200415204309205](Images - Efeito Piezoelétrico/image-20200415204309205.png)

# Efeito piezoresistivo

* resistência quando submetidos a
  um esforço mecânico
* Materiais: silício ou germânio