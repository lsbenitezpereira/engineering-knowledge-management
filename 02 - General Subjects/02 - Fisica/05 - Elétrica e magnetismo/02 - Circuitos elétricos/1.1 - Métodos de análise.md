[TOC]

Método das malhas
=================

-   Baseada nas Leis de Kichhoff, porém simplifica os cálculos através
    do [artifício matemático]{.ul} das "Correntes de Malha"

-   Elas circulam na malha inteira e as correntes reais são calculadas a
    partir da soma/subtração algébrica de todas as correntes de malha
    que circulam em um mesmo ramo

-   O artifício é perfeitamente válido, visto que as correntes são
    combinações umas das outras

-   O número de equações no mínimo igual ao número de janelas (podemos
    contar ou calcular pelo Teoremas da topologia de redes), sendo que
    cada componente deve estar representado ao menos em uma equação

-   **Desenvolvimento padrão**

    -   Arbitre as correntes de malha como achar conveniente

    -   Monte a equação de cada malha separadamente, desenvolvendo e
        simplificando o que for possível

    -   Cada malha deve ser equacionada da seguinte forma:

$$\text{LKT}_{A}\therefore\sum_{\circlearrowright A}^{}{V = 0}\ \ \ \ \  \Rightarrow \ \ \ \ \ \ \ aI_{A} + bI_{B} = TI$$

-   Depois de equacionar todas, monte o Sistema Linear e resolva (por
    substituição, crammer ou gauss jordan, preferencialmente)

-   Depois de calcular as correntes de malha, determine as correntes
    reais

-   **E se tiver uma fonte de corrente?**

    -   Calma jovem, para tudo existe uma gambiarra

    -   Podemos escolher as malhas de forma que apenas uma malha passe
        pelo ramo da fonte de corrente

    -   Daí não equacionaremos essa malha e sua corrente será obviamente
        a corrente da fonte

    -   Jogamos esses valores na próxima equação e deu

-   **Método da supermalha**

    -   Alternativa àquela gambiarra da fonte de corrente

    -   Afinal, nem sempre poderemos fazer aquilo (uma fonte
        compartilhada por duas malhas)

    -   [Desenvolvimento padrão]{.ul}

        -   Definimos as correntes de malha

        -   Substituímos a fonte por um circuito aberto

        -   Essa janela grande que se formará será a supermalha

        -   Equacionaremos a supermalha levando em consideração [as
            correntes definidas anteriormente]{.ul}

        -   A equação que falta para completar o sistema vem do ramo
            eliminado no começo

Método dos Nós
==============

-   Ou *Análise Nodal*

-   Baseado na Lei de Kirchhoff das Correntes

-   Equacionamos correntes para achar as tensões dos nós

-   Fontes de corrente facilitam o serviço

-   Nº de equações = nº de nós -- 1 (pois um dos nós será referência)

-   **Desenvolvimento padrão**

    -   Determine um nó de referência

    -   Arbitre o sentido e o nome das correntes

    -   Equacione o valor das correntes

    -   Cada nó deve ser equacionado da seguinte forma:

$$\text{LKC}_{A}\therefore\sum_{A}^{}{I = 0}\ \ \ \ \  \Rightarrow \ \ \ \ \ \ \ I_{A} = \frac{V_{a} - V_{b}}{R}$$

-   Equacione os nós (LKC)

-   Resolva o sistema

-   **E se tiver uma fonte de tensão sem resistores?**

    -   Se um dos nós for referência, blz, até facilita

    -   Senão, não teremos como resolver

-   **Método do Supernó**

    -   Consideramos ambos os nós como um só (curto-circuito)

    -   Equacionamos todo esse supernó junto, mas mantemos as tensões
        definidas anteriormente

    -   A equação que falta virá do ramo retirado
