[TOC]

# Resposta de frequência

* variação do comportamento ao mudar a frequência do sinal

* Numeros complexos aparecem quando tentamos colocar a fase no meio do equacionamento

* Como respostas em frequência alteram a fase, invariavelmente cairemos em números complexos (se não quisermos equacionar fase e amplitude separadamente

* Se estivermos trabalhando com circuitos elétricos e fasores, ai sim numeros complexos tem tudo a ver

* **Relembrando fundamentos matemáticos**

  * Números complexos

  * Para tirar j do denominador, multiplique pelo conjugado

  * Impedâncias
    * $R$
    * $jwL$
    * $\frac{1}{jwC}$

  * Logaritmos

  ![1544906275873](Análise em frequência - Images/1544906275873.png)

  * Outras coisas

    * $(a+b)(a-b)=a^2-b^2$

### Escala Bel

* Ganho expresso na forma logarítmica  

* Multiplicador usual: decibel

* [Great simple explanation](https://www.youtube.com/watch?v=jNpRPX3FjtY&list=PL75kaTo_bJqmw0wJYw3Jw5_4MWBd-32IG&index=3)

* 0db 	→ transferência unitária

* 3db		→ transferência 2x

* Década	→ intervalo entre duas frequências com uma proporção 10
  
* A definição é em termos de potência; quando for tensão ou corrente (proporcionais ao quadrado da potência), o quadrado multiplica a equação por 2
  $$
  {G}_{db} = 10 log_{10}\frac{P_2}{P_1}
  $$

  $$
  {G}_{db} = 20 log_{10}\frac{V_2}{V_1}
  $$

  $$
  {G}_{db} = 20 log_{10}\frac{I_2}{I_1}
  $$

  $$
  {G}_{db} = 20 log_{10}H
  $$





### Função de transferência

* Relaciona entrada e saída 

* relação entre L{saída} e L{entrada} quando as condições iniciais são nulas

* Não considera as condições iniciais da EDO: é uma representação genérica

* Dá a resposta apenas para REGIME PERMANENTE

* Apenas para sistemas lineares?

  

* A transfer function may be defined only for a linear, stationary (constant
  parameter) system. A nonstationary system, often called a time-varying system,
  has one or more time-varying parameters, and the Laplace transformation may not
  be utilized. Furthermore, a transfer function is an input–output description of the
  behavior of a system

* Trabalhamos sempre em função da frequência 

  ![1544906722261](Análise em frequência - Images/1544906722261.png)

  ![1544906714533](Análise em frequência - Images/1544906714533.png)

*  Zeros	→ frequências de transferência zero (raízes do numerador)

* Polos	→ frequência de transferência infinita (raízes do denominador)

* Separando a parte complexa e a parte imaginária de H:
  $$
  \ln (H_{(\omega)}) = ln |H_{(\omega)}| + j\phi_{(\omega)}
  $$

* **Forma padrão**

  * Essa função de transferência pode ser uma equação qualquer
  * Entretando, para facilitar a análise, a expressamos em uma forma padrão
  * Já separamos zeros/polos pela sua “característica” (na origem, simples, quadráticos, etc)
  * Já juntamos $\omega j$, para facilitar a transformação para o domínio da frequência, onde faremos a substituição $s=\omega j$

  ![1544906986593](Análise em frequência - Images/1544906986593.png)

* **Outras anotações**
  
  * em todo sistema realizável, o denominador possui grau maior que o  numerador, pois o mundo real sempre atual +- como um filtro passa-baixa

### Diagramas de Bode

* Traçam a amplitude e a fase da função de transferência ($H(jw)$) em escala semilog 

*  Ou seja, $20log_{10}|H(jw)|$ e $\angle H(jw)$ vs $log_{10}(w)$

* Bode assintótico → aproximação por retas padronizadas (é o que usaremos aqui)

* if the response h(t) is real, then $|H(Jw )|$ is an even function of w and $\angle H(jw)$ is
  an odd function of w. Because of this, the plots for negative w are superfluous and can
  be obtained immediately from the plots for positive w

* $w=0$ não aparece no gráfico, pois $log (0)=\infty$

* :keyboard: scilab:

  ```matlab
  s= %s;
  num=100;
  den= s*(s+10)*(s+1);
  G= syslin('c',num,den);
  clf();
  bode(G,0.01,1000,'rad');
  bode_asymp(G,0.01,1000);
  ```

  

* **Análise dos termos**

  *  Os expoentes geralmente são inteiros 
  *  $K$
     *  Termo constante
     *  Se K>0, phi=180º
  *  $jw^{\pm N}$
     *  polo/zero na origem (1 na escala Bel). 
     *  ou derivador/integrador
     *  N geralmente é 1, para circuitos
  *  $1+\frac{jw}{z_1}$
     *  polo/zero simples
     *  Quando $w=z_1, w=$frequência de corte
     *  Nesse momento o comportamento chaveia.
     *  Nesse momento o erro da aproximação assintótica é máximo e igual a 3dB
        para a magnitude. 
  *  $1+\frac{2jw\xi}{w_k} + \left(\frac{jw}{w_K} \right)^2 $
     *  polo/zero quadrático
     *  xi \ksi\ ($\xi$) é o fator de amortecimento
     *  $w_k$ é a frequênciade corte.
     *  A amplitude possui um erro bem grande em $w_{k}$, que depende do fator de amortecimento. 

* **Traçado manual**

  *  Traçamos cada componente separadamente 
  *  Sobrepomos todas elas
  *  Podemos fazer isso pois os termos padrões da função de transferência estão se multiplicando e, ao passar o log, irão se somar

  ![1544908448780](Análise em frequência - Images/1544908448780.png)

* **Interpretação**

  * Zeros aumentam a inclinação da amplitude, enquanto polos diminuem
  * a transformada inversa de la place não inclui o transitório… o que exatamente isso implica?

Ressonancia
============

-   Troca de energia entre capacitor e indutor

-   Vai ter muita corrente circulando o você não necessariamente
    perceberá externamente

-   Acontece em uma frequência determinada, quando a impedância
    capacitiva anula a indutiva

-   A curva de amplitude apresentará um pico agudo

-   Acontece em qualquer sistema que tenha um par conjugado complexo de
    pólos. Ou seja, que tenham pelo menos um capacitor e um indutor.

-   As tensões no indutor e no capacitor podem ser muito maiores que a
    tensão da fonte

-   Importante: esses valores de frequência são sempre em rad/s. Para
    converter para Hz, divida por 2pi

-   **Frequência de ressonância** $\left( w_{0} \right)$

    -   Pico de amplitude

    -   $w_{1}$ e $w_{2}$ → frequências de meia potência (ou seja,
        $\frac{1}{\sqrt{2}}$ da amplitude)

> $\omega_{0} = \frac{1}{\sqrt{\text{LC}}}\frac{\text{rad}}{s}$

$w_{1}ew_{2} \cong w_{0} \pm \frac{B}{2}$

$w_{1}ew_{2} = \pm \frac{w_{0}}{2Q} + w_{0}\sqrt{1 + \left( \frac{1}{2Q} \right)^{2}}$

-   **Bandwidth** (B)

    -   Faixa em que dissipamos mais da metade da potência máxima

    -   Y será a amplitude da corrente (para circuitos paralelos) ou
        tensão (série).

![](./Images/3.3 - Ressonancia/media/image1.png){width="3.0in"
height="2.3in"}

-   Como a impedância capacitiva não é linear, o "empalhamento" da faixa
    passante será assimétrico.

-   Entretanto, quando o fator de qualidade Q for maior do que 10,
    podemos fazer a seguinte aproximação:

![](./Images/3.3 - Ressonancia/media/image2.png){width="1.3229166666666667in"
height="0.28125in"}

-   **Fator de qualidade (Q)**

    -   Seletividade do filtro

    -   Adimencional

    -   razão entre a potência reativa do indutor/capacitor e a potência
        média do resistor na frequência de ressonância

        $Q = \frac{w_{0}}{B}$

![](./Images/3.3 - Ressonancia/media/image3.png){width="2.151388888888889in"
height="0.7555555555555555in"}

![](./Images/3.3 - Ressonancia/media/image4.png){width="4.5625in"
height="0.9895833333333334in"}

![](./Images/3.3 - Ressonancia/media/image5.png){width="3.6770833333333335in"
height="4.53125in"}

![](./Images/3.3 - Ressonancia/media/image6.png){width="7.267361111111111in"
height="2.4881944444444444in"}

![](./Images/3.3 - Ressonancia/media/image7.png){width="7.267361111111111in"
height="3.115972222222222in"}

## Ressonância de circuitos série

-   Toda tensão da fonte parece ficar no resistor

-   Impedância será mínima na ressonância

![](./Images/3.3 - Ressonancia/media/image8.png){width="3.46875in"
height="1.6354166666666667in"}

![](./Images/3.3 - Ressonancia/media/image9.png){width="3.8541666666666665in"
height="2.696527777777778in"}

![](./Images/3.3 - Ressonancia/media/image10.png){width="2.5347222222222223in"
height="0.6395833333333333in"}

![](./Images/3.3 - Ressonancia/media/image11.png){width="1.1625in"
height="0.58125in"}

![](./Images/3.3 - Ressonancia/media/image12.png){width="1.40625in"
height="0.3541666666666667in"}

$B = w_{0}^{2}\text{CR} = \frac{1}{\text{CR}} = \frac{R}{L}$

-   Outras equações (reformulações das anteriores)

$w_{1}\text{ou}w_{2} = \pm \frac{R}{2L} + \sqrt{\left( \frac{R}{2L} \right)^{2} + \frac{1}{\text{LC}}}$

![](./Images/3.3 - Ressonancia/media/image13.png){width="4.645833333333333in"
height="1.1458333333333333in"}

![](./Images/3.3 - Ressonancia/media/image14.png){width="1.8854166666666667in"
height="1.0833333333333333in"}



## Ressonância paralela

-   Toda a corrente da fonte parece passar pelo resistor

-   Impedância será máxima na ressonância, e a admitância mínima

-   *Dica:* calcule pelo equivalente das admitâncias (que se somam)

-   Deduzimos as tretas pela admitância

![](./Images/3.3 - Ressonancia/media/image15.png){width="3.15625in"
height="1.0625in"}

![](./Images/3.3 - Ressonancia/media/image16.png){width="1.9305555555555556in"
height="0.8840277777777777in"}

$\left| I_{L} \right| = \left| I_{C} \right| = \text{QI}_{m}$

$B = \frac{1}{\text{RC}} = w_{0}^{2}\frac{L}{R}$

-   Outras equações (reformulações das anteriores):

    $w_{1}\text{ou}w_{2} = \pm \frac{1}{\text{RC}} + \sqrt{\left( \frac{1}{2\text{RC}} \right)^{2} + \frac{1}{\text{LC}}}$

![](./Images/3.3 - Ressonancia/media/image17.png){width="3.6354166666666665in"
height="1.1604166666666667in"}



Filtros
===============

-   Circuito projetado para deixar passar sinais com frequências
    desejadas e rejeitar/atenuar os outros

-   Consideraremos a frequência de corte como sendo a frequência de meia
    potência

> $\omega = \omega_{0} \leftrightarrow \frac{P_{\text{out}}}{P_{\text{in}}} = \frac{1}{2},\text{ou}\text{seja},\frac{V_{\text{out}}}{V_{\text{in}}} = \frac{1}{\sqrt{2}}$

-   **Tipos de filtros**

    -   Passa baixas, altas, faixa ou rejeita faixa
    -   notch: apenas um rejeita faixa de alta ordem; geralmente usado em aplicações tipo eliminar 60Hz; costuma ser estudado separadamente, por suas peculiaridades

![](./Images/3.4 - Filtros/media/image1.png){width="5.4375in"
height="2.4166666666666665in"}

![image-20201116231831222](Images - 3.4 - Filtros/image-20201116231831222.png)

![image-20201116231902562](Images - 3.4 - Filtros/image-20201116231902562.png)

![image-20201116231914498](Images - 3.4 - Filtros/image-20201116231914498.png)

![image-20201116231925448](Images - 3.4 - Filtros/image-20201116231925448.png)

-   **Ordem do filtro**
    -   o nú1nero de pólos existentes na função de
            transferência do mesmo
    -   1º → atenua até 20dB/dec fora de $w_{c}$
    -   2º → atenua até 40dB/dec fora de $w_{c}$
    -   3º → atenua até 60dB/dec fora de $w_{c}$
-   **Especificação de projeto**
    -   Ripple na banda passante
    -   Ripple na baixa rejeitada
    -   largura da banda de transição
    -   etc
    -   as especificações podem ser relativas (em dB, em relação ao sinal não filtrado) ou absolutas (em magnitude real, em volts)

## **Aproximações**

-   ==Usado para projetar?==

-   idependente da impementação (ex: ampop) e de suas não idealidades

-   **Butterworth**

    -   Ou *resposta plana* (por ser monotônica)

    -   (+) não possui ripple

    -   (+) fase mais linear que o chebchev

    -   Função de transferencia para um filtro passa baixa:

        ![image-20201116233617184](Images - 3.4 - Filtros/image-20201116233617184.png)

        > Kra = ganho do filtro PB quando a freqüência w é nula
        >
        > $w_c=2\pi f_c$ é a freqüência de corte
        >
        > n é a orden do filtro.

-   **Chebyshev**

    -   melhor definição nas vizinhanças de $w_c$

    -   apresenta ondulações (ripples) na faixa de passagem
        
    -   (+) atenua mais que o butterworth, para a mesma ordem
        
    -   (-) possui ripple
        
    -   Função de transferencia para um filtro passa baixa:
    
        ![image-20201116234245106](Images - 3.4 - Filtros/image-20201116234245106.png)
    
        >  Kpa é o ganho do filtro PB para freqüência nula (w = O)
        >
        > Wc é a freqüência de corte
        >
        > E é uma constante que define a amplitude (PR) dos ripples presentes na faixa de passagem
        >
        > $C_0n$ = polinômio de Chebyshev
    
    -   O núrnero de ripples presentes na faixa de passage1n é igual à ordern do filtro.
    
    -   para n ímpar, os ripples apresentam em w = O seu valor máximo e,
        para n par, os ripples apresentam em w=O o seu valor minirno
    
    -   quanto maior a a,nplitude do ripple, maior será a atenuação obtida na faixa de transição.
    
    -   polinômio de Chebyshev é dado por:
    
        ![image-20201116234520513](Images - 3.4 - Filtros/image-20201116234520513.png)
    
        > Ou ainda:
    
        ![image-20201116234539103](Images - 3.4 - Filtros/image-20201116234539103.png)
    
    -   (+) melhor resposta de amplitude que um buterworth da mesma ordem
    
    -   (-) resposta de fase menos linear que o butterworth
    
    -   <u>Variação - chebchev 2:</u> o ripple está na faixa de rejeição
    
-   **Cauer**

    -   Ou *eliptica*
    -   (-) complexos
    -   (+) precisa de uma ordem menor, para o mesmo resultado
    -   presentam ripples tanto na faixa de passagem
        como na faixa de corte
    -   cauer é muito seletivo, e consegue atuar mais ainda para algumas  frequências mais altas que a frequencia de corte
    -   bastante usado para  sinais biomédicos 

## Filtros passivos

-   Geralmente tentamos utilizar apenas resistores e capacitores (por
    custo, espaço, disponibilidade, etc)

-   Filtros passivos possuem ganho sempre menor que 1

-   Não costuman ser muito bons abaixo de 300Hz

### Topologias usuais

-   Podemos melhorar adicionando mais componentes passivos para melhorar
    o fator de qualidade, adicionar componentes ativos, etc

-   *Dica*: olhe sempre para os extremos, ao tentar determinar o tipo de
    filtro

-   **Passa altas**
-   $w_{c} = \frac{1}{\text{RC}}$

![](./Images/3.4 - Filtros/media/image2.png){width="3.78125in"
height="2.6979166666666665in"}

-   **Passa baixas**

    -   $w_{c} = \frac{1}{\text{RC}}$

        ![](./Images/3.4 - Filtros/media/image3.png){width="4.072916666666667in"
        height="2.4895833333333335in"}

-   **Rejeita faixa**

    -   $w_{0} = \frac{1}{\sqrt{\text{LC}}}$

![](./Images/3.4 - Filtros/media/image4.png){width="5.052083333333333in"
height="2.0729166666666665in"}

-   **Passa faixa**
-   $w_{0} = \frac{1}{\sqrt{\text{LC}}}$

![](./Images/3.4 - Filtros/media/image5.png){width="4.927083333333333in"
height="2.3645833333333335in"}

## Filtros ativos

-   (+) Desacoplamento (filtro e carga, entre estágios, etc)
-   (-) Menos confiaveis e menos estáveis
-   (-) Nao costuman operar bem acima de 100k Hz
-   **Associações**
    -   Associando em cascata filtros PB e PF de primeira e segunda ordens, podemos obter
        os filtros de ordem superior à segunda.
    -   o calculo de um filtro de 2º orderem (por exemplo) varia se ele está inserido em um circuito total de 6º ou 8º ordem (exemplos); Dont worry, tem tudo tabelado
    -   geralmente o ultimo estágio é o de ordem menor, e fica principalmente  responsável por diminuir os ripples dos estágios anteriores
    -   Associação em cascata de filtros PF e RF não é tão simples
    -   opção simples para conseguir um bom PF
        -   Associar um PB com um PA
        -   os filtros devem ter a mesma ordem
        -    a orde1n do filtro PF obtido seja o dobro da ordem de cada um dos filtros 
        -   (-) baixa precisão
-   **Ordens maiores sem associações**
    -   Dá pra implementar filtros com ordem maior que 2 com um único ampop
    -   o design/equacionamento é muito, muito mais complexo; geralmente apenas métodos iterativos, e muito pesados
    -   na prática, não se faz

### Topologias usuais

-    uma mesma estrutura pode ser utilizada para itnplementar diferentes aproximações
    
-   podem implementar qualquer um dos tipos de filtros (pass baixa, alta, etc)

-   numa mesma associação em cascata não devemos utilizar estruturas distintas.

-   **Realimentação múltipla (MFB)**

    -   Ou *multiple feedback*
    -   ganho negativo
    -   :books: Passa baixa, antonio pg 184
    -   :books: Passa faixa, antonio pg 193

-   **Fonte de tensão controlada por tensão**

    -   Ou *Sallen e Key*
    -   Ou FTCT ou VCVS (VCVS: voltage-controlled voltage source)
    -   :books: Passa baixa, antonio pg 182
    -   :books: Rejeita faixa, antonio pg 197 ( só possibilita ganho unitário.)

-   

-   ==essas topologias abaixo recebem algum nome??==

-   Passa baixa primeira ordem

    -   esse mesmo circuito também atua como passa baixa
    -   ![](./Images/3.4 - Filtros/media/image6.png){width="2.3125in"
    height="2.1354166666666665in"}
    -   ![](./Images/3.4 - Filtros/media/image7.png){width="0.9479166666666666in"
        height="0.4895833333333333in"}

-   Passa alta primeira ordem

    ![](./Images/3.4 - Filtros/media/image8.png){width="2.375in"
    height="1.78125in"}

-   ![](./Images/3.4 - Filtros/media/image9.png){width="1.03125in"
    height="0.4583333333333333in"}

-   Passa faixa

-   ![](./Images/3.4 - Filtros/media/image10.png){width="5.333333333333333in"
    height="2.8020833333333335in"}

-   ![](./Images/3.4 - Filtros/media/image11.png){width="3.0208333333333335in"
    height="1.7395833333333333in"}

-   Rejeita faixa

    -   ou *filtro notch*

    -   Passa alta em paralelo com passa baixa, depois um amplificador

        ![](./Images/3.4 - Filtros/media/image12.png){width="4.21875in"
        height="2.78125in"}

### **Dicas práticas**

-   Um filtro ativo não tem sua performance alterada quando multiplicamos (ou
    dividimos) os valores dos resistores por um fator m > 1, desde que os valores dos
    capacitores sejam divididos (ou multiplicados) pelo mesmo fator.
-   Um filtro PA pode ser obtido a partir da estrutura de um filtro PB, bastando, para tanto,
    tàzer a permutação dos resistores por capacitores e dos capacitores por resistores.
-   o valor do fator $Q_0$ (fator de qualidade??) não deve ser superior a 10.
-   O ganho K não deve ultrapassar $K<2Q_0^2$
-   **Heurística para o capacitores**: C=10/f, onde, para fc dado em Hertz, se obtén C em microfarad.
-   Ao escolher valores comerciais, recalcule os valores para cada componente escolhido e escolha os próximos baseados nos novos calculos

## Análise como sistema

-   THE MAGNITUDE-PHASE REPRESENTATION
    OF THE FREQUENCY RESPONSE OF LTI SYSTEMS:

    Y(jw) = H(jw)X(jw),
    
- H(jw) is the frequency response of the system-i.e., the Fourier transform of the
  system's impulse response

- A variável é complexa, podemos dividir em amplutde e fase 

-   
    
-   
    
-   Podemos analisar o tipo de filtro pela resposta ao imulso h(t) e
    degrau s(t)

-   passa baixa

    -   the step responses overshoot their long-term final values and
        exhibit oscillatory behavior, frequently referred to as ringing.

    -   Ex:

        ![](./Images/3.4 - Filtros/media/image13.png){width="6.768055555555556in"
        height="2.7430555555555554in"}

    -   ![](./Images/3.4 - Filtros/media/image14.png){width="6.768055555555556in"
        height="2.9555555555555557in"}