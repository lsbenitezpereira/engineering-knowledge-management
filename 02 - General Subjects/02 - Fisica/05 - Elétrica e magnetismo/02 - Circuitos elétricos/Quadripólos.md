# 	Quadripolos

* Se o circuito for linear, podemos trabalhar só em função das entradas e saídas
* Bipolos: uma equação é suficiente para relacionar a tensão e corrente (uma fonte e uma impedância)
* Para quadipólos, são necessárias duas tensões e duas correntes (duas fontes e duas impedâncias)
* Ou seja, caímos em um sistema 
* Como os circuitos internos podem ser ativos, o balanço de potência indicará a potencia fornecida/recebido ao circuito 
* As correntes são padronizadas entrando sempre pelo positivo
* Podemos analisar escolhendo diversos conjuntos de parâmetros diferentes (um para cada combinação possível de entradas e saídas)
* Podemos converter um conjunto em outro, manipulando as equações (essa conversão é tabelada), o que pode ser util quando tivermos limitações no ensaio prático 
* **associação de quadilopos** 
  * Série: soma impedancias
  * Paralelo: soma admitâncias
  * Cascata: produto das matrizes de transmissão 
* **Método de resolução**
	* Matamos uma coluna de cada vez
	* Para tensao damos um curto, para corrente abrimos o circuito 

### **Parâmetros de impedância**

* Zeramos uma corrente de cada vez, ou seja, abrimos uma das portas 

   ![img](Images - quadripólos/1543340428543.png)

   ![img](Images - quadripólos/1543340468545.png)

* $z_{11}$=impedância de entrada

* $z_{22}$ = impedância de saída 

* $z_{12}$ e $z_{21}$ = impedância de transferência 

* Podemos remodelar o circuito da seguinte forma:

   ![img](Images - quadripólos/1543334949689.png)

* Se $z_{12}=z_{21}$ (circuitos passivos geralmente são simétricos), podemos simplicar como um circuito Y: 

   ![img](Images - quadripólos/1543334973821.png)

### Parâmetros de admitância
* Zeramos uma tensão de cada vez, ou seja, damos um curto 

	![img](Images - quadripólos/1543340529904.png)

	![img](Images - quadripólos/1543340541014.png)

* Modelo:

	![img](Images - quadripólos/1543340564458.png)

* Se $y_{12}=y_{21}$, então podemos simplificar como um modelo delta:

	![img](Images - quadripólos/1543340573497.png)
### Parâmetros hibridos 1 

* Misturamos impedância e admitância, fazendo aparecer uns ganhos no meio

* Entrada em aberto, saída em curto 

	![img](Images - quadripólos/1543340595956.png)

	![img](Images - quadripólos/1543340695870.png)

* Modelo: 

	![img](Images - quadripólos/1543340730326.png)

### Parâmetros hibridos 2

* Mesma lógica, mas escolhemos um conjunto de variáveis diferente

*  frequentemente usados para modelar transistores de efeito de campo (FETs)

	![img](Images - quadripólos/1543340786165.png)

	![img](Images - quadripólos/1543340792981.png)

* Modelo:

	![img](Images - quadripólos/1543340805434.png)
### Parâmetros de transmissão 1

* Olhamos ora só pra entrada, hora só pra saída 

	![img](Images - quadripólos/1543340957270.png)

	![img](Images - quadripólos/1543340971535.png)

### Parâmetros de transmissão 2
* ou *transmissão inversa* 

	![img](Images - quadripólos/1543341027198.png)

	![img](Images - quadripólos/1543341038187.png)

	==tem mais coisa== 