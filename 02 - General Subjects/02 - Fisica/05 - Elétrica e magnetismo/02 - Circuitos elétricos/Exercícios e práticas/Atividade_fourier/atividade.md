# Exercício 36

Sendo $$V_s=10(1-t)$$ uma função periódica de período $T=1​$, temos que:

$$
V_s(t)=a_0+\sum_{n=1}^\infty\left(a_ncos(n\omega_0t)+b_nsen(n\omega_0t)\right)
$$
Onde:
$$
a_0=\frac{2}{1}\int_0^1 10(1-t)cos(n 2\pi t)dt\\ a_0=5
$$

$$
a_n=\frac{2}{1}\int_0^1 10(1-t)cos(n2\pi t)dt=\frac{5-5cos(2\pi n)}{\pi² n^2}\\
a_n=0, \forall n \in \Z
$$

$$
b_n=\frac{2}{1}\int_0^1 10(1-t)sen(n2\pi t)dt=\frac{10\pi n-5sen(2\pi n)}{\pi^2n^2}\\ \frac{10}{\pi n}, \forall n \in \Z
$$

Portanto, podemos reescrever a função como:
$$
V_s=5+\sum_{n=1}^\infty \frac{10}{\pi n}sen(n 2\pi t)
$$
Tomando o limite superior como um numero real finito, obtemos os seguintes gráficos (obtidos pela plataforma online [Desmos](https://www.desmos.com/calculator/u6cozhnnkk)):

![1543366949668](/home/benitez/.config/Typora/typora-user-images/1543366949668.png)

------

![1543366982457](/home/benitez/.config/Typora/typora-user-images/1543366982457.png)

------

![1543366997006](/home/benitez/.config/Typora/typora-user-images/1543366997006.png)

