# Difussion mass transfer

* Very similar to conduction heat transfer

* Due to a concentration gradient

* occurs more readily in gases than in liquids and more readily in liquids than in solids

* Microsciopic vision

  * Modecular motion are random
  * If there is more molecules of one type in some part to the space, so more of them will “go” than other types of molecules “come”

* Fick’s law

  * rate equation for mass diffusion

  *  in a binary mixture of A and B:

  ![1554918998496](Images - Others/1554918998496.png)

  * $D_{AB}$ = binary mass diffusivity

  * $\vec{J}_i$ = mass flux of species i (Kg m^2/s)

  * $\rho$ = mixture mass density
    $$
    \rho = \sum \rho_i
    $$

  * The flux may also be evaluated on a molar basis: 

    ![1554919350108](Images - Others/1554919350108.png) 

  * $\vec{J}_i^*$ = diffusive molar flux of species i (kmol m^2/s)

  * C = molar concentration of the mixture

  * $x_A = C_A/C$

$$
C = \sum C_i
$$

* Mass Diffusivity
  * diffusion coefficient
  *  increase with increasing temperature.
  * experimentally determinated