%a) Um elétron com energia cinética inicial igual a
%32 eV colide com uma barreira quadrada com 41 eV de altura e
%0,25 nm de largura. Qual é a probabilidade do tunelamento do
%elétron através dessa barreira? (b) Um próton com a mesma ener-
%gia cinética colide com a mesma barreira. Qual é a probabilidade
%de tunelamento do próton através dessa barreira?
E = 32;
U0 = 41;
L = 0.25e-9;
G = 16*E/U0*(1 - E/U0);
K = sqrt(2*m_eletron*(U0-E)*ev2joule)*2*pi/h;
T = G*e^(-2*K*L)


K = sqrt(2*m_proton*(U0-E)*ev2joule)*2*pi/h;
T = G*e^(-2*K*L)
%R:0.27 (a) 0,0013 (b) 10^-143
