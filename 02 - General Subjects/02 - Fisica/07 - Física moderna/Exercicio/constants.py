import numpy as np

pi = 3.141592653589793
e = 2.71828182845904523536
h = 6.62e-34 #J*s
h_cortado = h/(2*pi)
m_eletron = 9.109e-31 #Kg
m_proton = 1.67e-27 # kilograms
c = 300e6 #m/s
e0 = 8.8541878128e-12 #F⋅m^−1

charge_eletron = -1.6e-19 #C
charge_proton = 1.6e-19 #C


mu_B = np.abs(charge_eletron) * h_cortado/(2*m_eletron) #magneton de bohr


# Convertion factors
# to convert from <a> to <b>, multiply by "<a>2<b>"
ev2joule = 1.60218e-19
joule2ev = 1/ev2joule
year2second = 265*24*60*60

rad2degree = 180/pi
degree2rad = 1/rad2degree

# Multipliers
nano   = 1e-9
micro  = 1e-6
mili     = 1e-3
kilo     = 1e3
mega  = 2e6
