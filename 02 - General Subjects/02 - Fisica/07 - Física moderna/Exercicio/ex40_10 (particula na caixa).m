% Um próton está em uma caixa de largura L. Qual deve
%ser a largura da caixa para que a energia do nível fundamental
%seja igual a 5,0 MeV, valor típico para a energia de ligação de
%partículas no interior de um núcleo? Compare o resultado com o
%tamanho de um núcleo, que é da ordem de 10–14 m.
h = 4.13*10^-15 %eV*s
m_proton = 1.67*10^-27 % kilograms
E = 5*10^6
n=1
L = sqrt((n^2*h^2)/(8*E*m_proton))