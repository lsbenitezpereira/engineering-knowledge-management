h = 6.62*10^-34 %J*s
c = 300*10^6 %m/s

duracao = 12e-9
lambda = 193e-9

% a)
% A radiação está na faixa ultravioleta

% b)
E_foton = h*c/lambda %J

% c)
P = 1.5e-3 %w
Epulso = P*duracao %J
n_foton = Epulso/E_foton