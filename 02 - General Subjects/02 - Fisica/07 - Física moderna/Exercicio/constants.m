h = 6.62*10^-34 %J*s
h_cortado = h/(2*pi)
m_eletron = 10^-30 %Kg
m_proton = 1.67*10^-27 % kilograms
c = 300*10^6 %m/s

charge_eletron = 1.6e-19 %C
charge_proton = 1.6e-19 %C

% Convertion factors
% to convert from <a> to <b>, multiply by "<a>2<b>"
ev2joule = 1.60218e-19
joule2ev = 1/ev2joule
year2second = 265*24*60*60

% Multipliers
nano   = 1e-9
micro  = 1e-6
mili     = 1e-3
kilo     = 1e3
mega  = 2e6