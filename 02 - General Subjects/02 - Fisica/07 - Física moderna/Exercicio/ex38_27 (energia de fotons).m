calor_especifico = 4190 %J/(kg K)
calor_vaporizacao = 2.256e6 %J/kg

m= 2*1e-6*1e-3 %kg de sangue
T_final = 100
T_inicial = 33
lambda = 585e-9

% (a)Quanta energia cada pulso deve transferir para a mancha? 
energia_aquecimento  = m*calor_especifico*(T_final - T_inicial)
energia_vaporizacao = m*calor_vaporizacao
energia_total = energia_aquecimento + energia_vaporizacao

% (b) Qual deve ser a potência de saída desse laser? 
P = energia_total/t % W

%(c) Quantos fótons cada pulso deve transferir para a mancha?
n = energia_total/(h*c/lambda)