% (a) Que potencial de aceleração é necessário para
%produzir elétrons com comprimentos de onda de 5,00 nm? (b)
%Qual seria a energia dos fótons com o mesmo comprimento de
%onda desses elétrons? (c) Qual seria o componente dos fótons que
%possuem a mesma energia dos elétrons no item (a)?
lambda = 5e-9

Vab = (h/lambda)^2/(2*m_eletron*charge_eletron)
E = h*c/lambda*joule2ev
