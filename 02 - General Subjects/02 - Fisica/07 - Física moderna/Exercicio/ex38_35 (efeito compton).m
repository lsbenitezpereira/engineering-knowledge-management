%Um fóton de comprimento de onda igual a 0,1100 nm
%colide com um elétron livre que está inicialmente em repouso.
%Depois da colisão, o comprimento de onda passa a ser 0,1132 nm.
%(a) Qual é a energia cinética do elétron após a colisão? Qual é
%sua velocidade? (b) Caso o elétron seja subitamente contido (por
%exemplo, usando-se um alvo sólido), toda a sua energia cinética
%é empregada na criação de um fóton. Qual é o comprimento de
%onda do fóton?
h = 6.62*10^-34 %J*s
m_eletron = 10^-30 %Kg
c = 300*10^6 %m/s

lambda_in = 0.11*10^-9
lambda_out = 0.1132*10^-9

f_in = c/lambda_in
f_out = c/lambda_out

%a)
delta_E = h*(f_in - f_out) %energia do elétron em J
v_eletron = sqrt(delta_E*2/m_eletron)

%b)
lambda_photon = c/(delta_E/h)