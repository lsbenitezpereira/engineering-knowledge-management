% Bolas de bilhar no nível fundamental. (a) Determine
%o nível de energia mais baixo para a partícula em uma caixa,
%supondo que a partícula seja uma bola de bilhar (m = 0,20 kg)
%e que a largura da caixa seja 1,3 m, o tamanho de uma mesa de
%bilhar. (Suponha que a bola deslize sem atrito em vez de rolar.
%Ou seja, despreze a energia cinética da rotação.) (b) Como a
%energia do item (a) é totalmente dada pela energia cinética, a
%que velocidade isso corresponde? Com essa velocidade, quanto
%tempo a bola levaria para ir de uma extremidade da mesa até a
%outra? (c) Qual é a diferença de energia entre os níveis n = 2 e
%n = 1? (d) Os efeitos da mecânica quântica são importantes para
%o jogo de bilhar?

m = 0.2
L = 1.3
n=1
E1 = n^2*h^2/(8*m*L^2)

v = sqrt(E1*2/m)