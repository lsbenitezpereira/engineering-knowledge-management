h = 6.62*10^-34 %J*s
h_cortado = h/(2*pi)
m_eletron = 10^-30 %Kg
c = 300*10^6 %m/s

L = 0.125e-9

%a
n=4
E4 = n^2*pi^2*h_cortado^2/(2*m_eletron*L^2)
n=1
E1 = n^2*pi^2*h_cortado^2/(2*m_eletron*L^2)
delta_E = E4-E1 %J
printf("a) A diferença de energia é de %f J\n", delta_E)

%b
lambda =  h*c/(delta_E) %nm
printf("b) O comprimento de onda do foton deve ser de %f nm\n", lambda)