E_inicial = 1e6
lambda_inicial = h*c/E_inicial
lambda_final = 500e-9

espalhamentos = 1e26
%a)
delta_lambda_por_espalhamento = abs((lambda_final - lambda_inicial)/espalhamentos)
%b)
phi_por_espalhamento = sqrt(delta_lambda_por_espalhamento*m_eletron*c/h*2) %radianos

%c)
t = 10e6*year2second
d_total = c*t
d_por_espalhamento = d_total/espalhamentos