%Um próton com energia cinética inicial igual a 50,0 eV
%encontra uma barreira de 70,0 eV de altura. Qual é a largura da
%barreira se a probabilidade de tunelamento é 8,0  10–3? Como
%isso se compara com a largura da barreira para um elétron com a
%mesma energia tunelando por uma barreira de mesma altura com
%a mesma probabilidade?


E = 50;
U0 = 70;
G = 16*E/U0*(1 - E/U0);
K = sqrt(2*m_proton*(U0-E)*ev2joule)*2*pi/h;
T = 8e-3
L = log(T/G)/(-2*K)


