the idea that positively influencing the
longterm future is a key moral priority of our time





* metaphor 1
  * imprudent teenager. Most of a
    teenager’s life is still ahead of them, and their decisions can have lifelong
    impacts. In choosing how much to study, what career to pursue, or which
    risks are too risky, they should think not just about short-term thrills but
    also about the whole course of the life ahead of them.
* Metaphor 2
  * molten glass
  * At present, society is still
    malleable and can be blown into many shapes. But at some point, the glass
    might cool, set, and become much harder to change. The resulting shape
    could be beautiful or deformed, or the glass could shatter altogether,
    depending on what happens while the glass is still hot
* Metaphor 3
  *  path towards longterm impact as a risky
    expedition into uncharted terrain. In trying to make the future better, we
    don’t know exactly what threats we will face or even exactly where we are
    trying to go; but, nonetheless, we can prepare ourselves. We can scout out
    the landscape ahead of us, ensure the expedition is well resourced and well coordinated, and, despite uncertainty, guard against those threats we are
    aware of.





* value lock-in
  * an event that causes a single value system, or set of value systems, to persist for an extremely long time
  * While the lock-in of Nazism and Stalinism would have been nightmarish, the lock-in of the values of any time or place would be terrible in many respects
  * What we should aim to do is build a morally exploratory world: one structured so that, over time, the norms and institutions that are morally better are more likely to win out, leading us, over time, to converge on the best possible society





* Problems of stagnation
  * if society stagnates technologically, it could remain stuck in a period of high catastrophic risk for such a long time that extinction or collapse would be
    all but inevitable
  *  consider what would have happened if we had
    plateaued at 1920s technology. We would have been stuck relying on fossil
    fuels. Without innovations in green technology, we would have kept
    emitting an enormous amount of carbon dioxide. Not only would we have
    been unable to stop climate change, but we would also have simply run out
    of coal, oil, and gas eventually. The 1920s’ level of technological
    advancement was unsustainable
  *  as long as growth restarts at some point,
    then a period of stagnation is not close in importance to extinction or the
    lock-in of bad values. Just as a growth slowdown might delay us by a
    decade, a period of stagnation might delay us by a thousand years. But, so
    the argument goes, whether the delay is ten years or a thousand years, it’s
    pretty minor compared to the millions, billions, or trillions of years ahead of
    us

# Population ethics

* Repugnant Conclusion
  * The wellbeing from
    enough lives that have slightly positive wellbeing can add up to more than
    the wellbeing of ten billion people that are extremely well-off.
  * If you want to reject the Repugnant Conclusion, therefore, then you’ve
    got to reject one of the premises that this argument was based on