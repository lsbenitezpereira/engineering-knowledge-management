**[Adam Smith]{.underline}** (1723-1790)

-   O estado deveria interferir o mínimo possível na
    [economia]{.underline}.

-   Ver mais em *"geografia -- geopolítica -- economia"*

**[Edmundo Burke ]{.underline}**(1729 \~ 1797)

-   "O Estado é um fato social e uma realidade histórica, não uma
    manifestação formal de vontades apuradas num dado momento: ele
    reflete a alma popular, o espírito da raça"

-   Condenou a Revolução Francesa pela noção abstrata e absoluta dos
    direitos do homem.

**[Benjamin Constant]{.underline}** (1767-1830)

**[Karl Marx]{.underline}** (1818 1883)

-   O Estado é um instrumento de [dominação]{.underline}.

-   O fator econômico determina os fenômenos sociais

**[Max Weber]{.underline}** (1864-1920)

-   O Estado detém o monopólio do uso legítimo da força

-   **Funções sociais básicas**

    -   Manter a lei e a ordem

    -   Cobrar impostos

    -   Registro a população

    -   Defesa nacional

    -   Cuidar do bem-estar da população (saúde, educação, etc)

**[Antonio Gramsci]{.underline}** (1891 \~ 1937)

-   Mudanças políticas deveriam vir acompanhadas de [mudanças na
    mentalidade]{.underline}.

-   Esta mudança nasceria nas instituições sociais, principalmente das
    [escolas]{.underline}

-   Capacitar os alunos para serem mais do que simples operários.

-   **Hegemonia**

    -   Dominação de uma classe social sobre a sociedade.

    -   Exercida pelas instituições políticas

    -   Mais do que a violência (Maquiavel), a ideologia e a alienação
        > também são instrumentos do poder.

-   *Frase bônus:*

"Contra o pessimismo da razão, o otimismo da prática"

**[T. H. Marshall]{.underline}**
([1893](https://pt.wikipedia.org/wiki/1893) \~
[1981](https://pt.wikipedia.org/wiki/1981))

-   Consolida a ideia de [direto social]{.underline} (que se soma aos
    direitos civis e políticos)

-   **Direito civil**

    -   Revolução Francesa (sec XVIII)

    -   Direto à propriedade, ir e vir, liberdade, vida, etc

-   **Direitos políticos**

    -   Sec. XIX (qual contexto?)

    -   Votar e ser votado, existência de partidos, etc

-   **Diretos sociais**

    -   1950, pós 2º Guerra

    -   Estado deve garantir o bem-estar básico

    -   Saúde e educação pública, direitos trabalhistas, aposentadoria,
        seguro desemprego, etc.

**[Jürgen Habermas]{.underline}** (1929-hoje)

-   [Esfera pública]{.underline}

-   Ver detalhes em *"escola de Frankfurt"*

Other annotations
=================

Monarchy: Si no hay reí \[para hacer la representation unificada\], hay
que haber presidente. But king are prepared all life, and are way more
reliable than politics

giving people political rights is good, because the soldiers and workers
of

democratic countries perform better than those of dictatorships.
Allegedly, granting people political

rights increases their motivation and their initiative, which is useful
both on the battlefield and in the factory.
