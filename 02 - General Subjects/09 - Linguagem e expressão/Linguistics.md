# Conceptualization

* cognitive linguistics
  * a blanket term covering a wide variety of research activities
  * in order to understand language change, it is necessary to
    distinguish between two approaches to linguistic theory: formalist and functionalist
  * Functionalist approaches investigate why people use language and come up with answers that
    involve not only communicating messages (either by replicating existing structures perfectly
    or by exploiting them in an innovative way), but also functions such as promoting social bonds.

# Phnology

* phonemes: ?; language-specific categories; few dozen
* allophones: different sounds of the same letter
* 
* contrastive sound: ?
* voiced sound: ?
*  The features [high] and [back] relate to the position of the tongue body in the oral
  cavity
* 
* consonant chart of the International Phonetic Alphabet (IPA)
  * provides symbols for all sounds that are contrastive in at least one language
  * https://www.internationalphoneticalphabet.org/ipa-sounds/ipa-chart-with-sounds/
  * The axes of this chart are for place of articulation (horizontal), the location in the oral
    cavity of the primary constriction, and manner of articulation (vertical),
  * Many cells of the chart contain two consonants, one voiced and the
    other unvoiced

## Repreenatation

* word boundary is usually symbolized by #
* The finite-state approaches emphasize the temporal (or left-to-right) ordering of phono-
  logical representations. In contrast, attribute-value models emphasize the hierarchical na-
  ture of phonological representations.
* autosegmental phonology
  * phonological representation
  * going eight +1 or -2, but not +2 (therefore you can't go up after going down)
* SPE-style phonological rules could be modelled using finite-state methods
* Sound Pattern of English (SPE)
  * Early model of generative phonology
* Optimality Theory
  * ?
  * has many variants. Two notable ones are Harmonic Grammar
    (Potts et al. 2008) and Harmonic Serialism (McCarthy 2008b)

# Morphology

* study of the structure of words and how words are formed by combining
  smaller units of linguistic information called morphemes
* languages with rich morphology: Hungarian, Finnish, Turkish, Arabic, etc
* Morphological analysis is the process of decomposing words into their constituent
  morphemes to uncover linguistic information
* morphological generation is the process of synthesizing words from the available linguistic information
*  
* free morphemes can occur by themselves as a word
* bound morphemes are not words in their own right but have to be attached in some way to a free morpheme
*  
* **morphological processes to build the words**
  * Inflectional morphology: do not change the part-of-speech, but
    add information like person and number agreement, case, definiteness, tense, aspect,
    etc.
  * Derivational morphology produces a new word usually (but not necessarily) of a
    different part-of-speech
  * Compounding is the concatenation of two or more free morphemes—usually nouns— to form a new word generally with no or very minor changes in the words involved. Example german: Lebensversicherungsgesellschaftsangesteller (Life insurance company employee’)
* **Morphemes making up words**
  * Prefixation refers to a concatenative combination where the bound morpheme is affixed to
    the beginning of the free morpheme or a stem
  * suffixation refers to a concatenative
    combination where the bound morpheme is affixed to the end of the free morpheme or
    a stem
  * Infixation
  * circumfixation, part of the attached morpheme comes before the stem while another part goes after the stem; in
    German, e.g., the past participle of a verb such as machen (to make) is indicated by <u>ge</u>mach<u>t</u>
* **vowel harmony**
  * requires that vowels in (most) affixed morphemes agree in various phonological features with the vowels in the
    root or the preceding morphemes
* **Finite state morphology**
  * formalism for describing morphographemics and morphotactics
  * morphographemics transducer maps from surface strings to
    lexical strings
* **Other stuff**
  * Morpho Challenge competitions: for morphological analysers
  * English has a rule that states that when a suffix starting with a (phonological) vowel is
    affixed to a root word ending in a consonant and that has stress in the last syllable, then
    the last consonant of the root word is geminated, that is, repeated in the orthography. For
    instance, refer is one such root word and when it is affixed the +ed past tense suffix, we get
    referred. Similarly, one gets zipping from zip.

#  lexicon

* words of a language
* type: ?
* hapaxes: ?
* **What is a word**
  * i. A unique spelling form (a type). In this sense, the word swam is the same word, how-
    ever often it is used.
  * ii. A single occurrence (a token) of a lexical type—that is, one particular use of the lexical
    type by one particular writer or speaker on one particular occasion. The twelfth, twen-
    tieth, and twenty-sixth token of the preceding sentence (italicized) are three separate
    tokens of the type one.
  * iii. All the forms (base form and inflected forms collectively) that go to make up a lemma
    or lexeme. In English, the number of types in a lemma is always small: typically, just
    two types for noun lemmas (e.g. thing, things), four for weak verbs (e.g. hope, hopes,
    hoping, hoped), and five for strong verbs (e.g. take, takes, taking, taken, took). In a 
  * highly inflected language such as Czech, a lemma typically consists of a much larger
    number of types.
  * iv. A phraseme or multiword expression, which has a particular meaning (e.g. celery fly,
    forest fire, false acacia).
  * v. A lexical entry (in a dictionary or in a person’s mental lexicon), including lexemes,
    phrasemes, and some partial items (affixes and suffixes such as anti-, -oholic, and
    -gate).
  * vi. Any of (i)–(iii) above including (or excluding) proper names.
* 
* 
* Some word counts treat different parts of speech as separate types (e.g. to/PREP and to/
  INF may be regarded as two separate types); others regard a type as simply a string. 
* some lemmatizers treat phrasemes such as in_spite_of and of_course as a single type
* From a philosophical point of view, however, a contrast is made between names (which
  denote individuals) and words (which, supposedly, denote classes)
*  
* German characteristically splits its verbs into affix + base
  and appends ge- as a prefix to past participles
* The task of lexicographers can be seen as being to identify every established word (lexical
  item) of the language
* in any large corpus of natural texts, approximately 50%
  of the types are hapaxes. This finding falsifies the intuitively plausible expectation
  that the proportion of hapaxes will steadily decrease as corpus sizes increase
* For practical purposes, the lexicon of general English, with sufficient coverage for most
  natural-language processing (NLP) applications, consists of fewer than 6,000 verbs, around
  which clauses are built up by the addition of nouns and adjectives in particular clause
  roles: subject, object, and prepositional object.
*  
* Czech makes productive use of prefixes to form verbs, where English typically uses two words: phrasal verbs consisting of a base verb and a particle
*  
* **Selectional Restrictions**
  * The account of the lexicon in Chomsky (1965) assumes that NecessaryAndSufficientConditions for syntactic well-formedness can be stipulated

# Syntax

*  characterize the relation between semantic predicate–argument relations
*  Japanese is a language that uses explicit
  marking to indicate how the words of a sentence map onto the predicate–argument relations
  it expresses.
* The contiguous sequences of related words
  thus group together to form phrases or constituents, as indicated by the grouping brackets
  in “[The man from the school] visited [the young girl]”
*   
*  Subject-Verb-Object (SVO) languages (such as English, French, and Indonesian)
*  SOV languages (such as Turkish, Korean, and Japanese)
*  
* **noun phrase (NP)**
  * consist of a noun grouped with its associated modifiers
  * entire NPs, not just simple nouns, can be the arguments of the predicate
* **agreement**
  * or *concord*
  * the features of one word or phrase
    must be consistent with the features of other words that they combine with. English requires
    agreement between the subject noun phrase and the verb of a finite clause and between the
    determiner and the noun of a noun phrase
  * is an example of a syntactic dependency
* **embedded clause**
  * A sentence contained as a part of another sentence
  * Example: “John believes that Mary likes Bill”, `believe(John, like(Mary, Bill))`

## Representation

* should be expressive enough to allow for all the variations that exist across all the languages of the world

* parsing (or recognition): finding the predicate–argument relations for a given sentence

* generation: finding the sentences that express a given meaning

* 

* **regular grammars** 

  * sentence can be described in the
    notation of regular grammars and implemented computationally as the equivalent finite-
    state machines.
  * The transitions leaving a state determine exactly what the next set of words can be
  *  it is generally acknowledged that they can give
    only an approximate characterization of many syntactic phenomena

* **context-free grammar**

  * The phrase structure
    can be represented as a tree that records how categories are rewritten into strings of
    other categories.

  * differ from finite-state machines in that an un-
    limited number of dependencies of this type can be encoded with only a finite expansion
    of the grammar

    ![image-20230114214741039](Images - Linguistics/image-20230114214741039.png)

* **Transformational Grammar**

  * that combines a context free phrase structure grammar with another component of transformations that specify how trees of a given form can be transformed into other trees in a systematic way
  * provided reasonable descriptions of many syntactic phenomena
  * could not easily be used in practical language analysis systems
  * <u>deep structure</u>
    * starting point for a sequence of transformations, each of
      which takes the tree from a previous step as input and produces a modified tree as its output.
  * <u>surface structure</u>
    * The tree produced by the last transformation

* **Augmented Transition Networks**

  * computationally motivated 
  *  set of finite-state transition networks, one for each of the phrasal
    categories (NP, S, etc.) that can appear in the surface structure of a sentence.

* **dependency grammar**

  * attractive for computational work

  * focus on the relations between the individual words

    ![image-20230114220053375](Images - Linguistics/image-20230114220053375.png)

# Semantics

* how meanings are assigned to words, phrases, and sentences
* **Tense**
  * compact linear representation
  * represented by applying temporal operators to basic, present-tense sentences
* **Aspect**
  * dynamics of events and indicates whether they are ongoing, whether they have starting and end points, how they develop, and how they are viewed.
* **anaphoric expressions**
  * the meaning is commonly determined with respect to a prior antecedent expression
  * like bound variables in logic
  * anaphora resolution The task of determining the antecedent of an anaphor.
* **veridicality**
  * if O is veridical, then O(φ) entails φ
  * Example: “it’s undoubtedly the case that” is veridical
  * Example: “it is doubtful whether” is non-veridical
* **monotonicity**
  * entailment is monotonic if adding premises doesn’t invalidate conclusions

## Represetation

* Deep methods are those which represent the meaning of texts using a linguistically motivated techniques, parsing
  each sentence, identifying its logical or semantic structure, and interpreting it compositionally

* shallow methods that represent the meaning of texts in terms of surface-level features of text, such as n-grams, the presence or absence of keywords, sentence length, punctuation and capitalization, and rough document structure

* 

* **Discourse Representation Theory (DRT)**

  * standardized in Kamp and Reyle (1993) and implemented in the
    wide-coverage semantic parser Boxer

  * ‘box notation’ with the discourse referents on top, and the conditions below,

  * Can be converted to a  compact linear representation

  * Example:

    ![image-20230114220531259](Images - Linguistics/image-20230114220531259.png)

# Discourse

# Pragmatics

* the way in which contextual factors and speaker intentions affect meaning and inference

# Dialogue