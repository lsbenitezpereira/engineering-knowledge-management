[TOC]

# basic gramatics

* No gender
* No article
* No preposition (as a alone word): they are represented by word endings
* The sentences follows the order of SVO (subject-verb-object), just as in english
* vartalo = stem = raiz da palavra
* **Negation**

  * The axiliary verb *ei* is placed before the main verb, no negate its meaning
  * The verb *ei* is conjugated by person: en, et, ei, emme, ette, eivät
  * the main verb is’nt conjugated (because the personal ending goes to the negation verb), but it lost it’s last letter
  
  * Ex: Sinä *et* voi tulla = You cannot come.
  
  * Ex: Hän *ei* juo kahvia = He doesn't drink coffee.
  
  * <u>Negative imperative</u>
  
    * The imperative for the negative verb is älä in singular.
    * The main verb is without any ending
    * In plural it's *älkää* (only in formal speaking)
* **Questions**
  * Yes/no questions are formed by *-ko* suffix (in a noun, pronoun or verb). 
  * The verb with the question suffix starts the sentence (because it is the topic of the sentence)
  * the verb comes last.
  * Or by using questions words (equivalent to wh questions), like *miksi*
* **Country and nationality**
  * Ethnicities and names of the inhabitants (gentile) are formed with -lainen suffix (brasilialainen = brazilian)
  * When you want to say where you are from, where you come from, use the ending -sta (brasiliasta = from brazil)
  * Names of the languages are the same as the names of the country.
  * Names of languages and gentile begins with small letter
* Verb conjugation
  * The verbs are devided into types according to the infinitive marker and the way the personal endings are attached to the verb.
  * To conjugate, take away the infinite marker and add the right suffix 
* Expressing necessity (I have to, I must)
  * This is a special Finnish sentence type expressing necessity, what  someone must/has to do. The subject is in the genitive form, the main  verb **täytyy** is always in the same form (3rd person singular – no personal endings!) and the second verb is in the infinitive (basic) form.
  * In the negative form the main verb is **ei tarvitse**.

## genitive construction

* to express a relation between two nouns such as the possession of one by another 
* Includes the genitive case (ex: talon), the possessive pronoun (or it’s just the personal pronoun in genitive case?) (ex: minun) and the possessive sufix (taloni)

## Practical stuffs

* **Country and nationality**
  * Ethnicities and names of the inhabitants (gentile) are formed with -lainen suffix (brasilialainen = brazilian)
  * When you want to say where you are from, where you come from, use the ending -sta (brasiliasta = from brazil)
  * Names of the languages are the same as the names of the country.
  * Names of languages and gentile begins with small letter

# Lexical classifications

## Verb

* Conjugation in person:

  * 1st person singular: -n 

  * 1st person plural: -mme 

  * 2nd person singular: -t 

  * 2nd person plural: -tte 

  * 3rd person singular: -V or -ø 

  * 3rd person plural: -vat or -vät
* Others
  * In spoken finnish, the 3s is ussualy used for 3p

## Adverb

* täällä = *(over) here*
* siellä = *(over) there* (where we are not at the moment)
* tuolla = *(over) there* (where you can point to)

## Pronoum

### **Personal**

| Person |Peronal Pronoun |
| -- |------- |
| 1s | minä, mä |
| 2s | sinä, sä |
| 3s | hän, se (to things) |
| 1p | me     |
| 2p | te (also used as formal 2s) |
| 3p | he, se |
* **genitive form of a personal** 

  * Indicates possetion
  
  * There are no possessive pronouns in Finnish. so e.g. minun means both *my* and *mine*.
  
  * (minä) minun    Minun nimi on Juan
  
    (sinä) sinun       Mikä sinun nimi on?
  
    (hän) hänen      Hänen nimi on Kaire.
  
    (me) meidän     Meidän kotimaa on Ranska.
  
    (te) teidän         Mikä teidän kotimaa on?
  
    (he) heidän       Heidän kotimaa on Italia.
  
* **possessive suffix**

  * attached to a noun to indicate its possessor
  * to identify the ownership of something, you add a possessive suffix to the object
  * it's different from the genitive case
  * It can be translated as ~ "my, your, his, ours, their" (possessive adjectives), but embedded in the noun
  * The number of <u>possessors</u> can be distinguished for the singular and plural (the number of possessed objects is not morphologic explicit)

  | Personal pronouns | Possessive pronouns | Possessive suffix |
  | ----------------- | ------------------- | ----------------- |
  | 1s: minä          | minun               | -ni               |
  | 2s: sinä          | sinun               | -si               |
  | 3s: hän           | hänen               | -nsa              |
  | 1p: me            | meidän              | -mme              |
  | 2p: te            | teidän              | -nne              |
  | 3p: he            | heidän              | -nsa              |

  * <u>Examples</u>

    * Minun kotimaani / nimeni           

      Sinun kotimaasi / nimesi            

      Hänen kotimaansa / nimensä     

      Meidän kotimaamme / nimemme

      Teidän kotimaanne / nimenne

      Heidän kotimaansa / nimensä

  * <u>Suffix Exeptions</u>	

    * words ending in -i: the -i changes into -e
    * Words ending is -si: the -si changes to -te
    * Words ending in -nen: the -nen changes to -se
    * Words ending in -e: -e becomes -ee
    * Words ending in -us, -ys, -os, -ös, -is, -es: remove the -s and add -kse
    * Words ending in -as or -äs: remove -s and double the vowel.
    * Words ending in -in: -in becomes -ime-
    * Loanwords ending in a consonant: +i

### Demonstrative

* When inflected in inessive case, they have exceptional forms

  * tämä->tässä

  * tuo->tuossa

  * se->siinä

  * Ex: Minä en asu tässä talossa

  * Ex: Karl asuu siinä huoneessa.     

    *I don´t live in this house.*

    *Karl lives in that room.*

| Finnish  | English    |
| -------- | ---------- |
| Singular |            |
| *tämä*   | this       |
| *tuo*    | that       |
| *se*     | it/that    |
| Plural   |            |
| *nämä*   | these      |
| *nuo*    | those      |
| *ne*     | they/those |

# grammatical information

* Or *grammatical categories*
* Gender, person, number, tense, aspect, mood, voice, case
* I’ll try to study they all exhaustively (because I’m still learing finnish), even that some of them are very basic
* **Comparative grammar**
  * not all languages inflect case (==and a verb never inflect in case==)
  * In some languages the verb just inflect in a few of those grammatical informations

## Gramatical pearson

* verbs are inflected for person and number
* **Zero person**
  * or *fourth pearson*
  * in addition to passive voice may serve to leave the subject-referent open
  * Can be translated to english as the third pearson "one" and "ones"
* **1º person**
  * speaker
  * minä, mä (singular)
  * me (pural)
* **2º person**
  * Addresse
  * Sinä, sä (singular)
  * te (plural)
* **3º person**
  * Others
  * se (singular. he-she-it)
  * he (plural)

| Finnish      | English |
| ------------ | ------- |
| **Singular** |         |
| *minä*       | I       |
| *sinä*       | you     |
| *hän*        | he, she |
| **Plural**   |         |
| *me*         | we      |
| *te*         | you     |
| *he*         | they    |
| **Polite**   |         |
| *Te*         | you     |

## Grammatical mood

* Or *modality*
* Indicates degree of necessity, obligation, probability, ability, etc
* http://users.jyu.fi/~pamakine/kieli/suomi/verbit/tapaluokaten.html
* **Indicative**
  * can be used with any one of the four tenses
* **Conditional**
  * Only with present and past tense
* **Imperative**
  * can only be used with the present tense
  * The imperative singular form of every verb type is formed from the 1st person (*minä*-form) by simply leaving out the personal ending -n.
  * The imperative form is not necessarily very commanding in Finnish, particularly if you don´t shout. We use the phrase ole hyvä with it
* **Potential**
  * Only with present and past tense

## Grammatical Voices

* **Passive voice**
  * It can also be said that in the Finnish passive the agent is always 
    human and never mentioned. A sentence such as 'the tree was blown down' 
    would translate poorly into Finnish if the passive were used, since it 
    would suggest the image of a group of people trying to blow the tree 
    down.
  * Is much more used than in english
* **Active voice**
  * Normal

## Verb forms

* Principal parts of the verb: those forms that a student must memorize in order to be able to conjugate the verb
* Infinitive form?
* 1st person singular present form?
* 3rd person plural present form?
* **Participle**

  * When a verb act as adjective/adverb
  * https://en.wikipedia.org/wiki/Participle#Finnish
  * Since participles are actually adjectives, they are declinated alike
  * <u>present participle</u>
    * va-participle
  * <u>Past participle</u>
    * nut-participle
    * Sufix -*nut* or -*nyt* or *-neet* (plural)
    * http://venla.info/grammar-nut-participle.php
    * Examples
      * juopu*nut* mies (a drunken man)

# Verb tenses

* Verbs also decline according to four tenses
* **present tense**
  * simple present
* **Present perfect**
* you use *olla*-verb in the appropriate form (*olla* = to be, to have) and nut-participle.
  
* Ex: Sinä olet opiskellut (You have studied)
  
* Ex: Me olemme eronneet. (We have separated)
* **Imperfekti past**
  * simpled past
* **Perfekti past**
  * ?
* **Past perfect tense**
  * ?
* **Future**
  * No morphological future (its just say when an action will happen)
  * aikoa = to be going to
  * Aiomme pitää juhlat  = We are going to have a party
  * When we talk about the future activities we might add an adverb such as e.g. huomenna *´tomorrow´* into the sentence.
