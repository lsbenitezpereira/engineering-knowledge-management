[TOC]

# Pronuntiation 

* stress is always in the first syllable
* Double words: prolong (to vowels) or inhale (to consonants)
* Finnish have a higly fonetics writing system, so the words are written exactly as they as pronounced, and the letters have (almost) always the same sound within words
* double dot letters: open the mouth (better projection) and contract the glote 

# Alphabet

* [a, e, i, o, u] are the same as portuguese
* [k] sound like “c” as in *cat*, or “k” in *cake*
* [j] sound like “j” as in *yes*, /i/
* [h] like *hand* (/r/, almost silent)
* [a^º] âu (almost like /óu/)
* [ä] sound like “ae” as in *that*. Open sound
* [ö] sound like “ea” in *learn*. Closed and gutural sound, like ôô
* [y] is like /úhgh/. Similar to guarani, but no so gutural 
* Double vowel: larger sound
* **Tip**
  * the most dificult (to an english) are j, ä, ö, y, u, r
  * phrase tip (remember of herFinland childhood, with cow):
  * you can learn to *muu*, tracy
* **Difference from latin alphabet**
  * *å*: swedish o. Just used in names
  * *ä*
  *  *ö*

# Vowel harmony

* no native noncompound word can contain vowels from the group {*a*, *o*, *u*} together with vowels from the group {*ä*, *ö*, *y*}
* If the word does not have any of the vowels a, o or u the ending or the suffix cannot have those vowels either. Therefore we have two sets of endings and suffixes.
* Espanja +sta.   Juan on Espanjasta.
* espanja +lainen.   Hän on espanjalainen.
* Berliini +stä.   Rolf on Berliinistä.
* Berliini +läinen.   Hän on berliiniläinen.

# KPT rule

Or *consonant gradation*

last sybable inflection

strong -> weak

# **Why Finnish sounds it is nice**

* high proportion of vowels make it sound “softer”
* Regular: stress always on first silable
* Vowel harmony: a word should contain just similar sounding vowels
* Gradation: regular alternation between open and closed syllable