# General informations

* bordering the Baltic Sea, Gulf of Bothnia, and Gulf of Finland, between Norway to the north, Sweden to the northwest, and Russia to the east
* The capital and largest city is Helsinki
* Finland's population is 5.52 million
* Gentilício: finns
* Language: finnish
* 
* 
* Hollidays: https://www.officeholidays.com/countries/finland/2019 

# Geography

* Political organization
  * parliamentary republic
  * God save the Welfare State
* Political divisions and terminology
  * Coutry > region > municipality (city)
  * Nordic countries: Northern Europa (Denmark, Finland, Iceland, Norway, Sweden and Greenland)
  * Scandinavia: northern Europe (Denmark, Norway and Sweden). Germanic linguistic branch
  * Karelia: region between Russia and Finland
  * Lapland: north of Finland (over the Arctic Polar Circle)
* Usage of land
  * The forests cover some 77% of the land
  * cultivated land takes up 10%
* Geomorphology
  * Mountains in the north, hills in the center, flat in the south/west
  * few hills and fewer mountains, because of the long ice age
  * Several lakes and rivers

## Climate

* The snow season in northern Finland begins in November and lasts at least until May.
* Snow covers the ground in southern Finland from December to April, and northern Finland is snowbound from October to April
* **Climate in septem-december**
  * Average temperature from september to december: 10 6, 0, -4ºC
  * Average rainfall: 70mm, 17 days per month.
  * The rainiest periods are summer and autumn.
  * Fall (autumn): from September 1 to November 30
  * In the inland regions of southern and central Finland, the first snow 
    falls at the beginning of December and melts during late March
* **Aurora borealis**
  * nights are dark enough for Northern Lights viewing from late August to April.
  * Finland is one of the best places to spot the Aurorae
  * A couple houres before and after midnight
  * the best time for seeing auroras in Northern Finland is in March and early April, because of the weather
  * http://aurorasnow.fmi.fi/public_service/
  * <u>Probabilities</u>
    * The statistical probability for seeing auroras during a dark and cloudless night is approximately...
    * At Kilpisjärvi: 75 % of nights
    * Lapland (e.g. ski resorts Ylläs, Levi, Saariselkä): roughly 50 % of nights
    * In the central part of Finland (e.g. Oulu, Kuusamo): roughly 25 % of nights
* **Midnight sun**
  * Or kaamos, in finnish
  * In the northernmost areas of the country the sun never rises above the horizon for about 2 months
  * So, we also have continuous daylight during part of the summer
  * Southern Finland: 19 hours of daylight a day in midsummer, 6 hours of daylight in the midwinter
  * Hours of daylight: http://en.biginfinland.com/hours-of-daylight-finland-lapland/

## Economy

* **Internal economy**
  * Services (72%)
  * Manufacturing and refining (31%)
  * Primary production (2.9%)
* **Industries**
  *  electronics (21.6 percent)
  *  machinery, vehicles and other engineered metal products (21.1 percent)
  *  forest industry (13.1 percent)
  *  and chemicals (10.9 percent)
* **Energy**
  * nuclear power - 26%
  * net imports - 20%
  * hydroelectric power - 16%
  * combined production district heat - 18%
  * combined production industry - 13%
  * condensing power - 6%

# Culture

## Daily things

* GReeting: handshake (both men and women)
* You never wear shoes at somebody’s home
* Cans and bottles are always, always, taken back to the store and recycled there.
* Most homes are separating at least regular trash, biodegradable, cans, and bottles.
* Many Finns are sensitive to perfumes. Wear fragrance lightly
* In Finland, we have a concept called ‘Everyman’s right’ *(jokamiehen oikeus in Finnish)*. It allows everyone to roam freely in nature, eat and pick berries and mushrooms anywhere in forests. 
* Event OPM (Oma Pullo Mukaan) = Bring Your Own Bottle (Most parties are)
* In Finland, if you get a traffic ticket, it will be according to your yearly salary.
* **Dating rules** 
  * Finns date only one person at a time. 
  * In fact, in Finland, *dating* means that you are *in a relationship*. 
  * If you are going on dates, you are 100% NOT dating, you are single. 
* **Coffe break**
  * Everyone stops to go drink coffee
  * It’s dam sacret
  * Even in factories they stop to get everyone togheder
  * Sometimes they talk a lot, sometimes they just… drink in silence
  * Remember: silence in not awkward to Finns 
* Funnily enough, asking a Finn for a cup of coffee or a pint is more 
  ‘personal’ in Finland than in many other countries. A Finn may feel more
  comfortable around you if you guys have an activity to do. 
* For a Finn, being silent is being polite, as you are not disturbing the other person
* ‘Only ugly people need to dress up’ *(Vain rumat ne vaatteilla koreilee, in Finnish)*
* Avoid compliments. It makes us Finns a bit uncomfortable.
* Most Finnish households remove their shoes at the front door 
* A recent [Eurobarometer study](https://yle.fi/uutiset/osasto/news/survey_finland_ranks_number_one_in_citizen_trust/10270981) found that Finns’ trust in fellow citizens is higher than anywhere else in Europe
* [EU figures](https://ec.europa.eu/eurostat/web/products-eurostat-news/-/DDN-20170302-1)
  suggest Finns do the most exercise of all Europeans and are more likely
  to work out outdoors than other nationalities, despite the often harsh 
  climate.
* **Tiping**
  * Tipping in Finland is not required, and if you want to tip at all, 
    simply round up the bill to the nearest 5 or 10 Euro amount or put 
    something in the tip cup
* **Working**
  * Working Hours Act, passed in 1996, which gives most staff the right to adjust the typical daily hours of their workplace by starting or finishing up to three hours earlier or later.
  * ust 4% of employees regularly work 50 hours a week or more, well below the average across the western world, according to [OECD figures](https://www.oecd.org/statistics/Better-Life-Initiative-country-note-Finland.pdf).

## Food

* The foods are also sasonal, rsrs
* Fresh and simple
* Leipajuusto 
  * like cheese
* Vispipuur
  * like yogurt
* Lohikeitto
  * salmon soup
* Mustikkapiirakka
  * Blueberry pie
* Reindeer
  * deer meet
* Kaalilaatikko
  * Cabbage soup
* Kaalilaatikko
  * fish pie
* Riispuuro
  * Rice porridge
* Ruisleipä = sanduish
* **Normal Breakfast**
  * rye bread (with chease, but and maybe tomato)
  * Kaurapuuro = oatmeal porridge (mingau de aveia)
  * karelian pie (rye crust and thick rice porridge inside. Its good, but little tasteless)
* Meat
  * Their minced meat (in the market) is… wierd: to fine grinded, with a non-meat taste
  * And they do some kind of sauce with their minced meat that is really wierd, taste lake tuna with tomato extract
  * Makkara: sausage in the fire pit
* Viili
  * traditional heirloom yogurt from Finland
  * smily, a litle sour 
  * http://fermup.com/blog/how-to-make-viili-at-home/
* Salmiakki
  * Salty candy
  * candy = Karkki
  * Wierd as hell
  * is not entire bad after you get used to it, the in the begining is veeery wierd
  * they also do something similar (salty) with icecream
* Ohra
  * local product Similar to rice
  * Is being gradually substituted in the traditional cousine, because rice is cheaper
* Kotikalja
  * (juice beer)
  * Is that one that they served in Sodexo, HAMK

## Music

* **Traditional folk**
  * or *Kalevala rune singing style*
  * Follows the Kalevala's metrics
  * mainly from the region of Karelian
  * myths and beliefs
  * Have some contemporary band: Joose Keskitalo (terrible, not folk at all), Kuusumun Profeetta (very nice, I feel like half in a tribe an half in a lounge), Paavoharju

* **Early Contemporary**
  * Erik Bergman and Joonas Kokkonen 
  * Einojuhani Rautavaara and Usko Meriläinen
* **Rock**
  * Suomirock = Finnish rock
  * Pekka Streng
  * Hurriganes
  * Hector, Juice Leskinen, Dave Lindholm
  * happoradio: very good melodic rock, I love it

* **Pop**
  *  Maija Vilkkumaa, Sanni Kurkisuo, 
  * Jenni Vartiainen: sexy voice, with a powerfull rithm
  * nelli matula, Anssi Kela, Irina
  * Chisu: a lot of falsete, with a commom pop rithm
  * Anna Puu: hight pitch, with simple songs, but somewhat intimist and original
  * Anna Eriksson, Hanna Pakarinen and Antti Tuisku



## Religion

* Most are evangelical lutheran
* Spiritual world
  * 34% are spiritual 
  * 8.5% say that had contact with an spirit
  * 54% say that had experieced something non-normal
* Old paganism
  * conservation of luck: to my neughbour have he need to take from me, so I have to protect my luch
  * The bear was sacred 
* Curiosity: Britain
  * Ghosts are something of a national obsession

## Others

While Finnish men are obliged by law to do national military service, women can do it voluntarily