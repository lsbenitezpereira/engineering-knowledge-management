[TOC]

# Basic words

* kyllä = niin (agreement, “quite so”) = joo (informal) = juu (informal) = yes

* ei (conjugation: en, et, ei, emme, ette, eivat) = no

* suomi = finlandes (lingua)

* Verb olla: mean both *to have* and *to be*

* **Greetings (Tervehdykset)**
  
  * moi = moika = hei = terve = hello
  * Hyvää huomenta = hyvää aamupäivää = good morning
  * Hyvää iltaa = good evening
  * Hyvää yötä = Good night 
  * Päivää (Informal)
  * tervetuloa = welcome (terve can be used as *hello*, and terve=healthy and tuloa=comming)
  * mita kuuluu? (normal) = miten menee? (slang)= how are you going?  (that is a question)
  * kuinka voit? = miten voit = how are you?
  
* **Farewell**
  
  * Hyvää matkaa! = bon voyage
  * Moi moi = hei hei = bye
  * näkemiin = goodbye (more formal)
  * Kivaa päivää! (casual) = Hyvää päivän jatkoa = Have a nice day 
  * Nähdään huomenna! = see you tomorow
  
* **Be Educated!**

  * olkaa hyvä (formal) = ole hyvä (informal) = you’re welcome = please (polite request), or when giving something to someone
  * kiitos = kiiti (informal) = thanks (agradecimento)
  * anteksi = anteeks (informal) = i’m sorry = excuse me (also If you are not sure what someone says)
  * there's no word for 'please' in Finnish (to ask for something). In deed there is a word for asking things (like please), but is just used bu kids, in childish asks
  * Hauska tavata = hauska tutustua = nice to meet you
  * asking things (may I have)
    * sanko = polite way to say ‘get to me’
    * Saisinko is More potential than sanko
  
* **Simple exclamations**
* Ahaa!    *I see!*
  * Ai!     *Oh!; I see!*
  * No niin
  * yeah, sure, well	
  
* **Vai vs Tai**
  * both mean *or*
  * http://randomfinnishlesson.blogspot.com/2012/09/tai-vai.html
  
* **numbers**

  * The partitive case is the most used, for numbers bigger than 1
  * https://en.wikipedia.org/wiki/Finnish_numerals
| Number | Nominative| Genitive |Partitive | Illative |
| ------ | ------------------------------------------------------ | -------------------------------------------------- | --------------------------------------------------------- | ------------------------------------------------------- |
| 0      | nolla                                                  | nollan                                             | nollaa                                                    | nollaan                                                 |
| 1      | yksi                                                   | yhden                                              | yhtä                                                      | yhteen                                                  |
| 2      | kaksi                                                  | kahden                                             | kahta                                                     | kahteen                                                 |
| 3      | kolme                                                  | kolmen                                             | kolmea                                                    | kolmeen                                                 |
| 4      | neljä                                                  | neljän                                             | neljää                                                    | neljään                                                 |
| 5      | viisi                                                  | viiden                                             | viittä                                                    | viiteen                                                 |
| 6      | kuusi                                                  | kuuden                                             | kuutta                                                    | kuuteen                                                 |
| 7      | seitsemän = *seiska*                                   | seitsemän                                          | seitsemää = *seitsentä*                                   | seitsemään                                              |
| 8      | kahdeksan = *kasi*                                     | kahdeksan                                          | kahdeksaa                                                 | kahdeksaan                                              |
| 9      | yhdeksän = *ysi*                                       | yhdeksän                                           | yhdeksää                                                  | yhdeksään                                               |
| 10     | kymmenen                                               | kymmenen                                           | kymmentä                                                  | kymmeneen                                               |

* **Easy to confuse**
  * kuusi (six) vs kusi (urine, piss)
  * mikroaaltouuni = microware (mikro=tiny, aalto=wave, uuni=oven)
  * Computer = tietokone (tieto=knowledge, kone=machine)
* **Others**
  * Ei kesta = No problem
  * Missa on vessa? = Where is the bathroom?
  * minut on ryösttetty = I have been robbed
  * missa on <someting> = where is the <something>
  * minäkin = me too
  * Sehän on mielenkiintoista = that’s interesting
  * saudade
    * kaipuu
    * Maybe also ikävä
    * there are other words, see in https://glosbe.com/pt/fi/saudade
    * Brazilian trying to explain it in finnish: https://mielenihmeet.fi/saudade-mita-tunnetta-tama-portugalinkielinen-sana-ilmaisee/
  * Kuusi palaa
    * ( different meaning)
    * https://www.puhutaan-suomea.net/kuusi-palaa-9-meanings-in-finnish/

# Per topic
* **Introducing myself**
  * Mikä sinun nimesi on? = What is your name?
  * Minun nimeni on <name> = My name is <name>
  * Olen kaksikymmentäkaksi vuotta vanha = I'm 22 years old.
* **Nacionality**
  * Where are you from? = Mistä olet kotoisin?
  * Minkämaalainen sinä olet? = *What is your nationality?*
  * kansalaisuus = *nationality, citizenship*
  * maa = *country, land*
  * Brazil = Brasilia
  * Brazilian = Brasilialainen
  * I’m brazilian = minä olen brasilialainen
  * I’m from brazil = minä Olen kotoisin Brasiliasta
* **Language**
  * Mitä kieliä sinä puhut? = *What languages do you speak?*
  * Puhutko englantia? = Do you speak English?
  * Voitko toistaa englanniksi? = can you repet in english
  * suoeksi = in finnish (sufix -ksi)
  * kieltä = language, tonge
  * äidinkieli = mother language (aiti = mother)
* **Profession**
  * olen opiskelija = I’m a student
  * tieteellinen tutkimus = scientific research
  * tiede = science
  * tutkimus = research
* **Alimentation**
  * Hyvää ruokahalua! = Bon appetit
  * kippis = saúde (brinde)
  * lounas = lunch
  * voileipä = sanduich
* **About music**
  * Who is your favorite artist? = Kuka on sinun suosikkiartistisi?
  * Do you listen to Finnish music? = Kuunteletko suomalaista musiikkia?
  * kuunnella = to listen
  * kuuntelet = listen (2s)
* **Airport**
  * portti = gate
  * terminaali = terminal
  * olet hukannut matkatavarani = i’ve lost my lugagge (tavarani = my stuffs)
  * passia = passport
