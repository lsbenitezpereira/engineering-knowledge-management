[TOC]

# Language learning

## Language levels

### CERF

Levels: https://www.coe.int/en/web/common-european-framework-reference-languages/table-1-cefr-3.3-common-reference-levels-global-scale

Vocabulary per level ([source](https://languagelearning.stackexchange.com/questions/3061/what-are-estimates-of-vocabulary-size-for-each-cefr-level)):

![image-20231118185655575](./Images - Educação e aprendizado/image-20231118185655575.png)

| **CEFR Level** | **Hours of Audio Exposure** |
| -------------- | --------------------------- |
| **A1**         | 50-100 hours                |
| **A2**         | 100-200 hours               |
| **B1**         | 200-400 hours               |
| **B2**         | 400-600 hours               |
| **C1**         | 600-1,000 hours             |
| **C2**         | 1,000+ hours                |

# Educational systems

* **PhD**
  * Doctor of Philosophy (philosophiae doctor)
  * entre 4 e 8 anos
  * Alguns programas de doutorado admitem estudantes que tenham apenas a graduação, enquanto outros exigem que seja feito um mestrado antes
  * <u>Tips</u>
    * “Provavelmente a parte mais importante na sua admissão para um doutorado é conseguir os contatos com seus possíveis orientadores e convencê-los de que você é bom”
  * <u>PhD vs doutorado</u>
    * Ambos são o 3º ciclo do ensino superior
    * enquanto no Brasil o doutorado ainda é praticamente uma garantia de que o pesquisador continuará na vida acadêmica, a Europa vem buscando orientar seus programas de PhD para aproximá-los do mercado de trabalho e dos problemas da sociedade.

## Germany

* applications are usually due in the end of the year
* while in grad school, there are changes to be a teaching assistant (or graduate student instructor)
* **Types of institutions**
  * Hochschule is the generic term in German for institutions of higher education, corresponding to universities and colleges 
  * Universität = university = institutions with the right to confer doctorates.
  * Technische Universität = ?
  * Fachhochschule = University of Applied Science
  *  
  * Semester fees per semester are usually between 80 euros (simple) to 400 (usually with very nice train stuff and alike)
  * Private universities are thousands per semester
* **about work while doing a master**
  * https://www.toytowngermany.com/forum/topic/349234-possibility-of-pursuing-a-masters-degree-while-working-full-time/
  * “I know from talking to my students that some of them work part-time and do the full-time Master's program”
  *  
  * depending on you visa type (eg, student), you may have a limit on number o wrk hours
  * Non-EU/EEA students are also able to work in Germany alongside their studies, for 120 full days or 240 half days per year. If you take a job as a student assistant or research assistant at your university, this is usually not counted in your limit.
  *  non-EU students are *not* permitted to work in a self-employed or freelance capacity.
  *  
  * https://www.hochschulkompass.de/en/degree-programmes/study-in-germany-search/studying-part-time-while-employed.html
* **PhD**
  * you cannot start a PhD program without a master's degree in Germany
  * M.Eng still allows you to write the PHD
  * can only be done at universities, not UAS
  * “if you already plan on pursuing a Ph.D. after your master’s our recommendation is to primarily look for master’s degrees at universities”
    * https://www.mygermanuniversity.com/articles/universities-of-applied-sciences-and-the-phd
  * <u>Do a PhD at UAS</u>
    * In some German states (German: *Bundesland*) some reforms have been made so that now, there is a handful of UAS in Germany that do (under certain conditions) have the right to award doctorate degrees (in certain subjects)
    * this is still very, very uncommon
  * <u>cooperative doctoral studies.</u>
    * A different model of PhD
    *  students join a UAS and conduct their research at a UAS under the supervision of UAS staff. On top of that, they have an additional supervisor at a research university which is able to award the doctoral degree.
* **Costs**
  * “Often, Ph.D. candidates fund their studies by working at a university”
  * if you graduate from a UAS in Germany with a master’s degree you qualify for doctoral studies
  * [excelente video about master’s costs](https://www.youtube.com/watch?v=jqXwGRCmyjk)
  * <u>stipend</u>
    * bolsa de PhD
    * on average about 20k USD per year
  * <u>personal story (random dude)</u>
    * German PhD here. Work 30h/Week and get ~2000€ *after* taxes and health insurance, which is definitely lower than industry  average but still enough to live comfortably with. (36k/year before  taxes)
    * My only job is research. Some of my colleagues also have to help with teaching.
    * Note, however, that the pay is fixed. You cannot get a increase in pay by  working hard or being Einstein, as your pay is determined by the TV-L  list, same as other professions paid by the state.
    * Also PhD pay in Europe varies considerably. From what I've heard France does barely pay PhD candidates anything.
    * [source](https://www.reddit.com/r/MachineLearning/comments/lej57x/d_yet_another_rant_on_phd_applications/gmexukg?utm_source=share&utm_medium=web2x&context=3)

## US

* Academic names for the students such as “freshman,” “sophomore,” “junior,” and “senior”
* Elementary school
* ?
* **secondary school**
  * Lower secondary education
    * American middle school ==or junior high?==
    * around 12-15 years old
    * grades 5-8
  * more subject-oriented curriculum
    
  * Upper secondary education
    * American high school
    * aged 16–18
    * grades 7-12
    * increased range of subject options and streams.
    * 12th grade = last year = senior year
* college ~= university?

* Study time
  
  * 180 days in the standard school year in the United States, as compared with 220
    in South Korea, about 230 in Germany, and 243 in Japan
  * total time devoted to studies (in and out of the classroom) is about 20 hours a week.
    Japanese fifth-graders average 33 hours a week
* 
* 
* 
* Masters degree in America is different than in Germany, in the sense that bachelors are typically longer and the Master and PhD are somewhat parallel in something called 'graduate school'.
* **Costs**
  * junior data engineering salary: https://www.reddit.com/r/dataengineering/comments/r4kvde/junior_data_engineer_salary/
  * <u>Personal story (luu)</u>
    * 4 yrs in state cost 21k
    * out of state 48k
    * my undergrads is 54k
    * if you don't have a scholarship-- you have to pay the bills
    * 54k is w/o room and board
    * w/ room and board its 70k...
    * mit your beloved school, cost 73k
      the scholarships usually just "not pay university", without any help for housing/food
    * some scholarships will allow you to do whatever you want with the money, some will only transfer directly to school

##  Finland

* **PhD**
  * No tuition fees
  * 4 years
  * You’ll genearlly apply to a program, and not to a project
* **Personal story (Henrique, master in Wireless Communications at Oulu University)**
  * No meu caso, meu orientador do TCC me indicou para uma vaga de emprego  como pesquisador assistente. 
  * Então consegui a oportunidade de emprego e  mestrado juntos.
  * O processo normal para aplicar para o mestrado aqui é fazer a aplicacão em dezembro/janeiro. E se aceito, tu comecaria o  mestrado em setembro. 
  * O valor da tuition fee é 10000 euros por ano, mas  quase todo mundo consegue bolsa da universade então é só 2500 euros. 
  * Se tu tiver notas bem boas, talvez consigas estudar de graca e talvez até  um emprego na univeridade enquanto estudas (se tu quiseres estudar na  minha áreas que é wireless communications)

## Austria

* Austria is signatary in the Bologna Process 
* higher = post-secondary
* ECTS
  * Based on workload
  * 1 ECTS = 25 hrs of work (usually just half is the a classroom)
  * 30 ECTS = 1 semester 
  * 1 SWS = 1h per week of classroom (in Austria) 
* Levels
  * bachelor
    * 180 ECTS (6 semesters)
    * undergraduate
  * Master
    * graduate?
    * 120ECTS (4 semesters)
  * Doctoral
    * equivalent to the american Ph.D.
* 
* Universities
  * universities
  * University of aplied sciences (Fachhochshulen)
  * University colleges of education
  * Institute of Science and Technology
  * University of philosophy and theology
* Non-universities
  * Academies for midwifery
  * Clinical technical academies
  * Milaty academies
  * School of international studies (diplomatische akademien)
  * training institute for psychoterapiest and conservatories

## Italy

* The first modern University in Europe was founded in Bologna in 1088
* Have now 61 public universities, 30 private universities and 11 public  research organizations
* **Tratado de Bolonha**
  * 1999
  * padronizou todo o sistema de ensino europeu
  * Três Ciclos de Formação
* **Institutions**
  * Universities (public and privates). Include the Polytechnics.
  * Higher Technical Institutes (ITS – acronym for Istituti Tecnici Superiori)
  * The Higher Education for Fine Arts, Music and Dance sector (AFAM- acronym for Alta Formazione Artistica e Musicale)
* **Requisitos**
  * Complete secondary education (12 yeas)
* **Levels**
  * I will register here just to the university, but is almost the same thing to AFAM
  * ITS gives an Diploma di Tecnico Superiore (2/3 years). EQF level 5
  * <u>Undergraduate Degree: Laurea</u>
    * Or laurea triennale or corso di laurea di primo livello
    * equivalent to a Bachelor of Science in the European university system
    * 3 years (180 credits)
  * <u>Graduate Degree: Laurea Magistrale</u>
    * equivalent to Master of Science
    * 2 years (120 credits)
    * Lauree Magistrali a Ciclo Unico (undergraduate + graduate) are usually offered in the following fields of study: Medicine,  Veterinary Medicine, Pharmacy, Architecture, Law.
  * <u>Postgraduate Degree (PhD): Dottorato di Ricerca</u>
    *  requires  that candidates pass a comprehensive examination and complete original  research leading to a dissertation
    *  300 or 360  credits
  * <u>Specializing Masters and Continuing Education Programs</u>
    * after 1º and 2º level
* **CFU Credits**
  * *Crediti Formativi  Universitari*
  * measure the total workload (teaching hours + studing)
  * 1 credit = 25 hs
  * 1 year = 60 credits = 1500 hours
* **Others**
  * Exams are graded using a grading  scale of 30, where 18 is the minimum passing grade and 30 cum laude the  highest grade. 

### Recognition of qualifications

To  apply to a Laurea (equivalent to bachelor degree) program, you must  hold a secondary school qualification released after a minimum of 12  years of schooling (overall).

It verifies the validity of it and can allow access to first cycle programs if:

* It  is the official final secondary school leaving certificate/ the first  and second cycle university qualifications from the relevant foreign  system;
* qualification  that allows entry to comparable first cycle program (ex. academic) in  the relevant foreign system; qualification that allows entry to  comparable second or third cycle program in the relevant foreign higher  education syste
* qualification obtained after an itinerary of at least 12 years of schooling;

Universities and Higher Education Institutions are entitled to officially recognize international qualifications.

Please remember that this procedure does not transform a foreign secondary school diploma into an Italian qualification.

For further information on the recognition of qualification follow the link below:

<http://www.cimea.it/en/servizi/procedure-di-riconoscimento-dei-titoli/procedure-di-riconoscimento-accademico-dei-titoli.aspx>

 

**Students with less than 12 years of schooling**

Qualifications  obtained after 11 or 10 years of global schooling do not allow access  to Italian universities, unless you satisfy one of the following  additional conditions: 

* you  attended one (in case of 11 years) or two (in case of 10 years) years  of university studies and took all the exams foreseen by the study  program;
* you obtained a post-secondary title to compensate for any missing years of secondary education; 
* you  attended a preparatory program (foundation) at an Italian or foreign  institution, to compensate the missing years of schooling and to acquire  the competences and skills necessary to be accepted to the degree  program you wish to access at University. 
* **Students who have attended multiple secondary education institutions:**
* Secondary  school qualifications will be considered valid if achieved after at  least 2 years of attendance of the same education system.

 

**PREPARE YOUR DOCUMENTS**

In  order to complete the enrollment procedures you need to prepare the  following documents in the country where the educational system of the  school issuing your diploma belongs to. The original documents must be  delivered upon official enrollment. 

1. **Final diploma of secondary studies (at least 12 years)****:** accompanied by a translation in Italian language officially legalized by the Italian competent Consulate.
2. **"Declaration of value” (Dichiarazione di Valore**) issued  by the Italian Diplomatic Representative related to the high school  degree. The Declaration of Value is a document, released by the Italian  competent Authority in the Country to which the educational system of  your school refers, attesting the validity of your Diploma and in  particular attesting you are entitled to enter University.
    	For more information visit the following websites:
        	 
        	[Academic recognition](http://www.cimea.it/en/servizi/procedure-di-riconoscimento-dei-titoli/procedure-di-riconoscimento-accademico-dei-titoli.aspx)
        	[Recognition and equivalence of foreign qualifications](https://www.esteri.it/mae/en/politica_estera/cultura/universita/riconoscimento_titoli_studio/)
3. **Certificate proving the completion of 1 or 2 years of academic studies or a post-secondary title**  for those countries where the educational system lasts only 11 or 10  years. A transcript of the exams translated into Italian legalized by  the Italian competent Consulate if not issued by the university directly  in English, French or Spanish.
4. **Certificate attesting that the University entrance examination in the country of origin has been passed** if required by law (e.g. *Vestibular* for Brazil, *Selectividad* for Spain, *Prova de Aferição* or *Prova Geral de Acesso ao Ensino Superior* for Portugal, etc.). It is necessary for Chinese students to take the GAO KAO examination in order to apply
5. **Italian Language Certificate for programs held in Italian and English Certificate for programs held in English**

Documents  at point 1, 3 and 4 must be duly apostilled (only if the country where  the diploma has been achieved is part of the Hague Convention abolishing  requirement for legalization of foreign public documents). Apostilles  authenticate the seals and signatures of officials on public documents,  so that they can be recognized in foreign countries that are parties of  the Convention. Please view the list of countries that have signed the  Convention (1)  and the authority in charge of Apostille (2).

1. <https://www.hcch.net/en/states/hcch-members> 
2. <https://www.hcch.net/en/instruments/conventions/authorities1/?cid=41> 

If  your country is not part of the list, documents at point 1, 3, and 4  must be legalized by the Italian Diplomatic representatives.

 

**SPECIFIC ADMISSION REQUIREMENTS ARE FORESEEN FOR THE FOLLOWING SECONDARY SCHOOL DIPLOMAS**


**American High School Diploma**

Students  holding an American High School Diploma must have passed at least three  "Advanced Placements" (APs) with a score of 3 to 5 in order to access a  Laurea (equivalent to Bachelor of Science) program. 

An  AP in Italian will be compulsorily required to students with foreign  citizenship who apply to a program held in Italian language. The AP in  Italian cannot be counted among the 3 Aps in case of students with  Italian citizenship or in the case of enrollment in courses delivered  entirely in English. 

In the absence of the required APs, you can access a Laurea program if: 

* You have completed one full year of college and have taken four Aps 
* You have completed two full years of college and can be admitted to the third year 

 

**British qualifications**

Students  holding a British High School Diploma must have passed at least three A  levels in order to access a Laurea (equivalent to Bachelor of Science)  program. 

An  A level in Italian will be compulsorily required to students with  foreign citizenship who apply to a program held in Italian language. The  A level in Italian cannot be counted among the 3 A levels in case of  students with Italian citizenship or in the case of enrollment in  courses delivered entirely in English. 

 

**International Baccalaureate**

The  International Baccalaureate (IB) Diploma will be accepted for  enrollment  without any additional documentation only if issued from one  of the schools present in this list (<http://www.studiare-in-italia.it/studentistranieri/moduli/2017/Allegato2.pdf>).  In case the diploma was issued by a school not present in the list, you  will have to provide the usual set of documents required for  enrollment, prepared by the Italian Embassy in Geneva. 

If  you hold and IB and are a Non EU citizens, you can be admitted to the  test outside of the specific places reserved for non-EU candidates  residing abroad, only if you have passed an Italian language course in  your curriculum.

<http://www.studiare-in-italia.it/studentistranieri/moduli/2017/Circolare2017_2018En.pdf>

 

**MARCO POLO –  TURANDOT: Project for Chinese students**

The  Marco Polo- Turandot program is designed for Chinese students residing  in their home country who wish to attend a Laurea (equivalent to a  bachelor degree) program at an Italian University. Students are required  to complete an Italian language course of 10/11 months in Italy before  entering the University of their choice. Application to the Marco Polo  program takes place between April and August, at the Italian embassy in  China, where students fill out a pre-application, deciding both the  University program they wish to enroll in and the school or University  where to study Italian. For detailed information about the application  process and deadlines please consult the Italian Ministry of University  and Research website (or directly contact the Italian embassy): 

<http://www.studiare-in-italia.it/studentistranieri>

## Spain

* começa normalmente na última semana de Setembro e acaba na primeira semana de Junho
* bacharelato = ensino médio
* Para os estudantes bolseiros, com famílias numerosas ou com resultados excepcionais, têm direito à matrícula gratuita.
* A matricula é paga uma vez por ano
* **prova de acesso à universidade**
  * PAU
  * Nem todas as faculdades usam
  * Pode ser feita à distância
  * Tem uma fase geral obrigatória e uma específica optativa 
  * Logo vai desaparecer 
* 
* O que os alunos brasileiros devem obrigatoriamente fazer é homologar seu estudos

# University structure

* **Example: IFSC**

  * <u>CEPE</u>
    * Colegiado de Ensino, Pesquisa e Extensão
    * do IFSC como um todo
    * normativo e consultivo

  * <u>Consup</u>
    * Conselho Superior
    * reitor = presidente

# Educação 4.0

* responde às necessidades da Indústria 4.0

* o que falta atualmente
  * apreder a estudar
  * trabalho em equipe
  * desafios

	## Metodologias

* STEM
  * Science, Technology, Engineering, Math
  * 
  
* Problem based learning

  * Learning by doing

* 

## Ferramentas tecnologicas

* App inventor
  * easy
  * integra bem com lego mindstorm (via bluetooh)
  * INtegra bem com arduino (via bluetooh)
  * acessa os recursos do celular
* Lego mindstorm