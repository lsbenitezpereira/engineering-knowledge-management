[TOC]

# Prnounce

* umlaut: two dots; 
* ä is like /ê/

# Word classes

## Nouns

* writes compound nouns without spaces (e.g., Computerlinguistik – “computational linguistics”; Lebensversicherungsgesellschaftsangestellter – “life insurance company employee”)
*  you capitalize all nouns

## Pronouns

* The
  * der: masculine
  * die: feminine
  * das: neuter 
* personal
  * Ich: I
  * du: you singular
  * er: he
  * sie: she
  * es: it
  * wir: we
  * ihr: you plural
  * sie: they
* demonstrative
  * das: that

## Verbs

* To be
  * bin
  * bist
  * ist
  *  
  * sind
  * seid
  * sind
* Modal verbs
  * Only 6: können, ...?
  * 1º and 3º person are cojugated the same way
  * are followed by another verb (that acts as dependant; it comes in the end of the sentence and is always infinitive)


## Pronoums

* plurals
  * +er or +en? 
  * the pronoum is always die

# Noun cases

* Same as *declension*?
* Nominative
  * subject of a sentence
* Dative
  * Same as *indirect object*
  * receiver of the direct object
  * mir – to/for me
  * the policeman gives the ticket <u>to the driver</u>
  * responde a pergunta "para quem?", "to whom?"
* Accusative
  * Same as *direct object*, or *objective case*
  * receiver of the action of a transitive verb
  * mich – me
  * response a pergunta "o que?", "what?"
* Genitive
  * meiner – of me

# Vocab

## Greetings

* guten = good (gute, iffeminine)
* Guten morgen: good morning
* Guten tag: good day (10 am)
* Guten Abend: good evening
* Gute Nacht /guté narrt/: good night
* Schalf schön: sleep well
*  
* auf wiedersehen: bye (formal)
* Tschuss: bye (informal)
* bis später /bish spâtã/: see you later

