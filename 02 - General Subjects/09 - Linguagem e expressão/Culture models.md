# Lewis model

* categorizes cultures into three primary types based on their communication preferences and behaviors: Linear-Active, Multi-Active, and Reactive

  ![ ](./Images - Culture models/lewis-model.jpg)