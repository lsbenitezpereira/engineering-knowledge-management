Javier Vazquez-Corral
inspired by how the human brain relate concepts (name for colors)

11 buckets of colors that are widely used in industry

tasks:
* pallete-based editing: edit the image to follow a pallete
  * multi image variaton: edit many images to satisfy the same palette
* color harmonization: shift the collors to satisfy a "template" (progression of colors that is usually pleasant)
* style mimic (no predefined palete or prompt, just learn to mimic the style of an specific photo retoucher)


thier method:
find prototype palette
cluster pixel images
decide target pallete
recolour to palette
>>>> only recolour if the color transformatjin stay in the same of the 11 buckets
dataset generated on blender (combinations of colors and shapes; disentangles concepts to change them independently)
use colors tokens (added 11 new tokens, made short finetunnign just to learn these concepts); netowrk can interpolate between colors that are similar (example> blue and red, but not red and grenI), ebcause I think they don't pass the probabilistic output through a softmax in the end, so if the token of the generated patch is 0.7C1 and 0.3C2 they just render the blend of both


another method (they built on top of this, is a past work): saliency-based palete grouping
find one pallete for bakcground areas and another for foreground
assigment of an area is done by both saliency and the domint palette
