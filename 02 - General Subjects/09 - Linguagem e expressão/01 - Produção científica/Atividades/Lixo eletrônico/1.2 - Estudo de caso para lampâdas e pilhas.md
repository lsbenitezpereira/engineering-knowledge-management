Informações preliminares
========================

-   100%

-   17 páginas

**Referência:** VIEIRA, Karina Nascimento; SOARES, Thereza Olívia
Rodrigues; SOARES, Laíla Rodrigues. A LOGÍSTICA REVERSA DO LIXO
TECNOLÓGICO: UM ESTUDO SOBRE O PROJETO DE COLETA DE LÂMPADAS, PILHAS E
BATERIAS DA BRASKEM. **Revista de Gestão Social e Ambiental**, Salvador,
v. 3, n. 3, p.120-136, nov. 2009.

**Citação com autor incluído no texto:** Vieira, Soares e Soares (2009)

**Citação com autor não incluído no texto:** (VIEIRA; SOARES; SOARES,
2009)

REEs
====

Estima-se que a cada ano em todo mundo são fabricados 40 milhões de
toneladas de

lixo tecnológico. Se esta quantidade de lixo fosse colocada em vagões de
trem, o comboio

formado teria comprimento equivalente a uma volta em torno da terra

Definindo LR
============

Diversas medidas podem ser adotadas pelas organizações que desejam ser
responsáveis.

Entre elas está a logística reversa, um importante instrumento de estudo
cujo objetivo

principal é acompanhar o produto desde sua venda até o retorno ao seu
ponto de origem (os

fabricantes), que serão responsáveis pela destinação ambientalmente
adequada. Outra medida

é o retorno ao ciclo produtivo pela reciclagem, na qual os componentes e
matérias-primas das

mercadorias descartadas podem utilizadas na fabricação de novos
produtos.

A logística reversa, que durante sua ampliação veio abrangendo outros
aspectos, pode

ser considerada também como:

\[\...\] a área da logística empresarial que planeja, opera e controla o
fluxo e as informações

logísticas correspondentes, do retorno dos bens de pós-venda e
pós-consumo ao ciclo de

negócios ou ciclo produtivo por meio dos canais de distribuição
reversos, agregando-lhes

valor de diversas naturezas: econômico, ecológico, legal, logístico, de
imagem

corporativa entre outros. (Leite, 2007, p. 116-117).

Este conceito mostra de forma geral o verdadeiro papel da logística
reversa, que é de

facilitar o retorno do produto ao ciclo produtivo ou remanufatura,
reduzindo desta forma a

poluição da natureza e o desperdício de insumos

dificuldades
============

Existem muitas dificuldades para adoção do sistema de logística reversa.
Para Leite

(2007), o motivo desse pouco interesse pelo estudo dos canais de
distribuição reversos é sua

pouca importância econômica quando comparada aos canais de distribuição
diretos, como

transporte, depósito, armazenagem, estoques, ou seja, tudo que facilite
a distribuição do

produto por da empresa até a chegada para o cliente, de forma rápida e
sem prejuízos.

Vale ressaltar

que a empresa que adotar a logística reversa deverá também utilizar
mecanismos de apoio da

logística empresarial, devendo, sobretudo após o recolhimento do
produto, armazená-los em

local adequando, assim como transportá-los de forma segura.

.

O fósforo, aplicado como revestimento do interior das telas de vidro dos

monitores, contém metais pesados como cádmio, zinco e vanádio, assim, a
desmontagem de

um monitor de forma inadequada expõe a pessoa ao contato com esses
elementos perigosos.

quando se trata de um projeto de coleta de

eletrônicos, percebe-se que estabelecer pontos de coleta é a parte mais
fácil, o complicado é o

alto custo para a destinação adequada de produtos que vêm apresentando
pouca pós-utilidade.
