+-----------------------------------+-----------------------------------+
| ![Marca IFSC com                  | INSTITUTO FEDERAL DE EDUCAÇÃO,    |
| texto](./Images/Citacoes e Refere | CIÊNCIA E TECNOLOGIA DE SANTA     |
| ncias - exercicios/media/image1.p | CATARINA                          |
| ng){width="1.3965277777777778in"  |                                   |
| height="0.7756944444444445in"}    | DEPARTAMENTO ACADÊMICO DE         |
|                                   | ELETRÔNICA                        |
|                                   |                                   |
|                                   | UNIDADE CURRICULAR: MEP           |
|                                   | PROFESSORA: CLÁUDIA R. SILVEIRA   |
|                                   |                                   |
|                                   | ACADÊMICO:                        |
|                                   | ­­­­­­­­­­­­­­­­­­­­­­­­­­­\_\_\_ |
|                                   | \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\ |
|                                   | _\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ |
|                                   | \_\_\_\_                          |
|                                   | TURMA: 3ª FASE                    |
+-----------------------------------+-----------------------------------+

**CITAÇÕES E REFERÊNCIAS**

Produza um capítulo de um artigo científico sobre o tema "**O lixo
eletrônico e as questões ambientais**". Para isso, utilize todos os três
artigos disponibilizados pela professora (caso queira efetuar outras
pesquisas, além das fontes dadas, é possível).

Observação: este capítulo deverá conter:

a\) Uma citação direta com menos de 3 linhas, com a indicação do autor no
início da sentença.

b\) Uma citação direta com menos de 3 linhas, com a indicação do autor no
final da sentença.

c\) Uma citação direta com mais de 3 linhas.

d\) Uma citação indireta, com a indicação do autor no início da sentença.

e\) Uma citação indireta, com a indicação do autor no final da sentença.

f\) Uma citação com apud.

g\) Faça a referência de todos os artigos, em uma seção própria para
"Referências"

**Estrutura de seu trabalho: **

\- Cabeçalho

\- texto: **1 TÍTULO** (abra quantos títulos, subtítulos preferir)

**- REFERÊNCIAS **

**Formatação: **

\- espaço 1,5

\- fonte 12

\- texto alinhado

\- recuo do parágrafo: 2cm

\- formatação da página: esquerda e superior: 3cm; direita e inferior:
2cm

**Observações:**

1: Use o número de linhas necessário para um texto claro e completo.

2\. O texto que for considerado plágio receberá nota zero.

3\. O trabalho será avaliado a partir dos seguintes critérios:

\- acentuação gráfica

\- ortografia

\- conexão entre frases e parágrafos

\- capacidade de construir um texto a partir de um artigo científico

\- capacidade de efetivar uma pesquisa

\- normas da ABNT para citações

\- normas da ABNT para referências

\- normas da ABNT para trabalhos acadêmicos -- formatação

**Data da entrega: até o dia 22/03/2018 -- para o email:**

[**claudiasilveiramep\@gmail.com**](mailto:claudiasilveiramep@gmail.com)

**O trabalho deverá ser entregue em 2 documentos: PDF e Word ou Libre
officer (quem não encaminhar nesses 2 formatos não terá seu texto
corrigido).**
