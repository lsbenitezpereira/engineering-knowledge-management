Informações preliminares
========================

-   100%

-   9 páginas

-   **Citações**

    -   Segundo Natume e Sant'anna (2011, p. xx)

    -   (NATUME; SANT'ANNA, 2011, p. xx)

-   **Referencias **

    -   NATUME, R.Y.; SANT'ANNA, F. S. P. **Resíduos
        Eletroeletrônicos:** Um Desafio para o Desenvolvimento
        Sustentável e a nova Lei da Política Nacional de Resíduos
        Sólidos. São Paulo, 2011.

LR
==

PNRS\[\...\]estabelece a

"responsabilidade compartilhada" entre governo, indústria, comércio e
consumidor

final no gerenciamento e na gestão dos resíduos sólidos.

REE
===

O crescimento na geração dos resíduos eletroeletrônicos é decorrente
principalmente

da revolução tecnológica dos últimos anos que produz equipamentos em
larga escala, com

variadas utilidades.

Estes resíduos eletroeletrônicos são considerados perigosos pela sua
composição diversificada, principalmente de metais pesados que causam
sérios problemas ao ser humano, animais, vegetais, lençóis freáticos
dentre outros.

Doyle (2007) apresenta dados que indicam a crescente produção de REEs no

mundo, como os detritos elétricos e eletrônicos estão entre as
categorias de lixo de

mais alto crescimento no mundo, e em breve devem atingir a marca dos 40
milhões

de toneladas anuais

Os problemas ambientais causados pelo descarte inapropriado de resíduos
eletroeletrônico são extremamente graves, visto seu potencial de
contaminação do meio ambiente. Os danos causados podem ou não serem
permanentes, porém é necessário ressaltar que sua intensidade vem
crescendo proporcionalmente ao aumento da quantidade de material
descartado.
