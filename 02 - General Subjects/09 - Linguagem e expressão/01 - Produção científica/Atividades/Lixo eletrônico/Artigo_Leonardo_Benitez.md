Introdução
==========

Relevância do tema, objetivos e metodologia

1 Fundamentação teorica 
========================

1.1 Eficiência energética
-------------------------

Por que é importante, como podemos aumenta-la e os efeitos sobre o
consumo de conhecer claramente o que se gasta (que é o que o nosso PGEN
fornece)

1.2 Smart grids
---------------

Conceituação, principais linhas de pesquisa

1.3 Sistemas de GEN
-------------------

2 DEsenvolvimento
=================

2.1 Opções comerciais
---------------------

Foco apenas no brasil? Com que profundidade analiso cada uma das opções?
Com que metodologia vou realizar essa análise comercial?

2.2 Linhas de pesquisa
----------------------

Novamente, foco apenas no brasil? Nosso projeto pode ser citado como
desenvolvimento de um produto de baixo custo, ou é melhor nem citar?

2.3 Estado da arte
------------------
