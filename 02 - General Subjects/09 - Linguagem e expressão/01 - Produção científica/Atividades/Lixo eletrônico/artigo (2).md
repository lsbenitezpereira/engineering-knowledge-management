2 Logística reversa de Resíduos EletroEletrônicos
-------------------------------------------------

A logística reversa é conjunto de medidas que visam o retorno dos
resíduos de negócios e empreendimentos ao setor empresarial (NATUME;
SANT'ANNA, 2011), diminuindo assim o descarte inadequado desses
resíduos. Tais medidas são amplamente apoiado pela Política Nacional de
Resíduos Sólidos (Lei nº 12.305/2010), pois esta "estabelece a
'responsabilidade compartilhada' entre governo, indústria, comércio e
consumidor final no gerenciamento e na gestão dos resíduos sólidos"
(NATUME; SANT'ANNA, 2011, p. 6).

O princípio da logística reversa faz-se ainda mais importante no caso
dos Resíduos Eletroeletrônicos (REEs), visto que os dados causados por
estes são extremamente graves, como aponta Natume e Sant'anna (2011, p.
7):

Os problemas ambientais causados pelo descarte inapropriado de resíduos
eletroeletrônico são extremamente graves, visto seu potencial de
contaminação do meio ambiente. Os danos causados podem ou não serem
permanentes, porém é necessário ressaltar que sua intensidade vem
crescendo proporcionalmente ao aumento

da quantidade de material descartado.

### 2.1 Dificuldades de implementar uma logística reversa para REEs
