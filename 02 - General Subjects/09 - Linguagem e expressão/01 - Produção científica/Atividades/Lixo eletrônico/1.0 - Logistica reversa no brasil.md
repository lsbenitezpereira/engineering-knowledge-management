Informações preliminares
========================

-   100%

-   12 páginas

-   **Citações**

    -   Segundo Xavier et. al. (2017, p. xx)

    -   (XAVIER et. al., 2011, p. xx).

-   **Referência**

    -   XAVIER, Lúcia Helena et al. **Gestão de resíduos
        eletroeletrônicos:** mapeamento da logística reversa de
        computadores e componentes no brasil. Recife, 2017.

Principais citações
===================

Por outro

lado, computadores e componentes pós-consumo, apesar de representarem
uma categoria específica de

resíduos eletroeletrônicos, também podem contribuir para ações como o
desenvolvimento socioambiental, por

meio da recuperação de materiais e equipamentos pós-consumo, bem como
pela inclusão digital.

dificuldades
============

Dificuldades em LR de computadores:

¾ curtos ciclos de vida;

¾ grande número de produtos;

¾ baixa previsibilidade da demanda;

¾ grande variabilidade do mercado;

¾ grande customização de atendimento;

¾ muitos canais de fornecimento.

Ainda de acordo com esses autores, a inovação estabelece o tempo de vida
útil dos computadores em função da

substituição de produtos antigos por outros mais modernos,
independentemente do fato de estar ou não

funcionando

Estudos recentes têm evidenciado que locais próximos à áreas de
destinação de resíduos eletroeletrônicos na

China apresentaram teores elevadores de contaminação por metais pesados
como chumbo e cádmio

Nesse contexto, Miguez *et al* (2007) afirmam que:

"é possível aplicar a Logística Reversa no processo produtivo, obtendo
benefícios ambientais,

sociais e também econômicos para a empresa. Os benefícios ambientais
podem ser percebidos

pela economia na utilização de recursos minerais; pela redução de
materiais nos aterros

sanitários; pela diminuição de processos químicos que agridem o meio
ambiente e; pela opção

dada, para outras empresas, em relação ao destino de seus produtos e
equipamentos após o uso."

O processo da Logística Reversa de resíduos eletroeletrônicos ainda é um
assunto pouco abordado no Brasil.
