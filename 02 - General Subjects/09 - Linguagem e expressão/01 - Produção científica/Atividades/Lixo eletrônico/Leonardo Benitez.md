1 Logística reversa de resíduos sólidos 
----------------------------------------

A logística reversa é conjunto de medidas que visam ao retorno dos
resíduos de negócios e empreendimentos ao setor empresarial (NATUME;
SANT'ANNA, 2011), diminuindo, assim, o descarte inadequado desses
resíduos. De forma mais ampla, pode ser definida como:

\[\...\] a área da logística empresarial que planeja, opera e controla o
fluxo e as informações logísticas correspondentes, do retorno dos bens
de pós-venda e pós-consumo ao ciclo de negócios ou ciclo produtivo por
meio dos canais de distribuição reversos, agregando-lhes valor de
diversas naturezas: econômico, ecológico, legal, logístico, de imagem
corporativa entre outros. (LEITE, 2007, apud VIEIRA; SOARES; SOARES,
2009, p. 5).

Tais medidas são amplamente apoiadas pela Política Nacional de Resíduos
Sólidos - PNRS (Lei nº 12.305/2010), que, segundo Natume e Sant'anna
(2011), estabelece uma responsabilidade compartilhada pelos resíduos
sólidos, envolvendo o governo, indústria, comércio e consumidor final.

A PNRS também deixa claro que a logística reversa é um instrumento de
desenvolvimento social e econômico, pois permite "facilitar o retorno do
produto ao ciclo produtivo ou remanufatura, reduzindo desta forma a
poluição da natureza e o desperdício de insumos." (VIEIRA; SOARES;
SOARES, 2009, p. 5).

### 1.1 Logística reversa aplicada a Resíduos Eletroeletrônicos

O princípio da logística reversa faz-se, ainda, mais importante no caso
dos Resíduos Eletroeletrônicos (REEs), visto que os dados causados por
estes são extremamente graves, -

Os problemas ambientais causados pelo descarte inapropriado de resíduos
eletroeletrônico são extremamente graves, visto seu potencial de
contaminação do meio ambiente. Os danos causados podem ou não serem
permanentes, porém é necessário ressaltar que sua intensidade vem
crescendo proporcionalmente ao aumento

da quantidade de material descartado.(NATUME; SANT'ANNA, 2001, p. 7)

Ademais de evitar os problemas causados pelo descarte inadequado de
REEs, a logística reversa aplicada a esse tipo de resíduo possui uma
grande possibilidade de retorno econômicos, pois, como apontam Xavier
et. al. (2017, p. 3), "esses dispositivos eletrônicos contem metais
nobres como ouro, prata e cobre que podem ser reaproveitados."

Apesar desses fatores, Xavier et. al. (2017) indicam que a logística
reversa de REEs ainda é pouco debatida no Brasil. Para Leite (2007 apud
VIEIRA; SOARES; SOARES, 2009, p. 6), esse desinteresse é motivado pela
"pouca importância econômica quando comparada aos canais de distribuição
diretos". Vieira, Soares e Soares (2009) acrescentam, ainda, que existem
muitas dificuldades -- técnicas e econômicas -- em fornecer uma
destinação adequada aos produtos eletrônicos.

Referências
-----------

NATUME, R.Y.; SANT'ANNA, F. S. P. **Resíduos Eletroeletrônicos:** Um
Desafio para o Desenvolvimento Sustentável e a nova Lei da Política
Nacional de Resíduos Sólidos. São Paulo, 2011. incorreto

VIEIRA, Karina Nascimento; SOARES, Thereza Olívia Rodrigues; SOARES,
Laíla Rodrigues. A logística reversa do lixo tecnológico: um estudo
sobre o projeto de coleta de lâmpadas, pilhas e baterias da Braskem.
**Revista de Gestão Social e Ambiental**, Salvador, v. 3, n. 3,
p.120-136, nov. 2009. Ótimo!

XAVIER, Lúcia Helena et al. **Gestão de resíduos eletroeletrônicos:**
mapeamento da logística reversa de computadores e componentes no brasil.
Recife, 2017. incorreto

Texto excelente, Leonardo!

Formatação, ok

Pecou um pouquinho nas referências (e nas vírgulas! Rsrs)

Nota: 9,0

Gabarito das Ref.:

![](./Images/Leonardo Benitez/media/image1.png){width="5.905555555555556in"
height="0.5158508311461067in"}

NATUME, R.Y.; SANT'ANNA, F.S.P. Resíduos Eletroeletrônicos: um Desafio
Para o Desenvolvimento Sustentável e a Nova Lei da Política Nacional de
Resíduos Sólidos. In: INTERNATIONAL WORKSHOP ADVANCES IN CLEANER
PRODUCTION, 3, 2011, São Paulo. **Anais Eletrônicos**\... São Paulo,
mai. 18-20, 2011. Disponível em:
\<http://www.advancesincleanerproduction.net/third/files/sessoes/5b/6/natume\_ry%20-%20paper%20-%205b6.pdf\>.
Acesso em: 22 mar. 2018.

VIEIRA; Karina N.; SOARES, Thereza O. R.; SOARES, Laíla R. A logística
reversa do lixo tecnológico: um estudo sobre o projeto de coleta de
lâmpadas, pilhas e baterias da Braskem. **RGSA -- Revista de Gestão
Social e Ambiental**. V.3, n. 3, p. 120-136, set./dez. 2009. Disponível
em: \<https://rgsa.emnuvens.com.br/rgsa/article/view/180\>. Acesso em:
22 mar. 2018.

XAVIER, Lúcia H. et al. Gestão de resíduos eletroeletrônicos: mapeamento
da logística reversa de computadores e componentes no Brasil. 3º
SIMPOSIO IBEROAMERICANO DE INGENIERÍA DE RESIDUOS, 2º SEMINÁRIO DA
REGIÃO NORDESTE SOBRE RESÍDUOS SÓLIDOS, 2010, João Pessoa - PB. **Anais
Eletrônicos**\... REDISA -- Red de Ingeniería de Saneamiento Ambiental,
ABES -- Associação Brasileira de Engenharia Sanitária e Ambiental. 2010,
12p. Disponível em:
\<http://www.redisa.net/doc/artSim2010/Gestao/Gest%C3%A3o%20de%20residuos%20electroelectr%C3%B4nicos\_mapeamento%20da%20log%C3%ADstica%20reversa%20de%20computadores%20e%20componenetes%20no%20Brasil.pdf\>.
Acesso em: 22 mar. 2018.

### 
