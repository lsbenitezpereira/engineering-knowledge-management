[TOC]

Conceituação
============

-   Preposição “a” + artigo “a”

-   Logo, apenas palavras femininas te crase

-   Verbo não tem artigo, logo, *não existe crase antes de verbo*

Exceções
========

-   **Crase facultativa**
-   Antes de nomes próprio femininos e pronomes possessivos
        femininos
    
-   Why? Because esses termos podem ou não ter um artigo antes
    
-   Ex: Falei à/a Natália
    
-   Ex: Falei à/a sua classe
    
-   **Cidade em países**

    -   NÃO VAI CRASE

    -   Ex: vou à academia

    -   Ex: Vou a Curitiba

Bizus
=============================

-   **Bizu 1**

    -   “Se vou a, e volto da: crase há!”

    -   “Se vou a, e volto de: crase pra quê?”

-   **Bizu 2**

    -   Colocar o termo feminino no masculino: se vira “ao”, tem crase!

    -   Ex: Refiro-me à aluna → refiro-me ao aluno

    -   Ex: Conheço a candidata → conheço o candidato

![img](./Images - 2.2 - Crase/media/image1.png){width="5.698611111111111in"
height="3.1979166666666665in"}

![img](./Images - 2.2 - Crase/media/image2.png){width="5.699305555555555in"
height="2.6319444444444446in"}
