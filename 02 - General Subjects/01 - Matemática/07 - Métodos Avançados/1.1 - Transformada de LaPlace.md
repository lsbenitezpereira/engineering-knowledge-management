[TOC]

# Transformada de LaPlace

* É uma transformação linear que leva a função de um espaço para outro

* Cometário: o domínio de saída s pode ser complexo

* Pode ser definida de 0 a $+\infty$ (unilateral) ou de $-\infty$ a $+\infty$ (bilateral), sendo que cada uma tem propriedades diferentes

* De forma geral, utilizaremos a definição unilateral (que é utilizada para sinais causais)

* ==a unilateral também restringe apenas para a resposta em regime permanente?==

* generalization of the continuous-time Fourier transform

* Possibilita representar uma entrada arbitrária x(t) como uma combinação linear de componentes exponenciais
  
* **Definição formal**

  * Seja f uma função definida para $t\geq0$
  * A integral:
  $$
  L(f(t)) = \int_0^\infty e^{-st}f(t)dt, s\in \C
  $$
  * Será chamada de Transformada de Laplace, desde que a integral convirja
  * S é uma variável complexa, chamada de frequência complexa. ==S possui unidade de 1/t?==
  * É uma transformação do tipo $ f(t) \rightarrow F(s) $, onde chamamos de F o resultado da integral
  * No caso particular em que $s$ for puramente imaginário ($s=jw$), teremos a transformada de Fourier
  
* **Ordem exponencial**

  * Dizemos de uma função f é de ordem exponencial c se existem
    constante M&gt;0 e T&gt;0 tal que
    $$|f(t)| <= Me^{ct}, \forall t>=T$$
  * Ou seja, se ela não cresce mais rápido que uma exponencial.
  * Se, em um limite, uma função de ordem exponencial estiver
    multiplicando uma exponencial que vai para zero, podemos usar um
    teorema para garantir que o limite vai para zero

* **Respostas racionais**

  * Muitas transformadas levam em funções racionais (divisão de polinômios)

  * Lembrando que um polinómio pode ser escrito como um produto ou como uma soma

  * Ou seja:

    ![img](Images - 1.1 - Transformada de LaPlace/1555005016454.png)

  * Zeros: roots of the numerator (X(s) is zero)

  * Poles: roots of the denominator (X(s) is infinite)

  * X(s) will be rational whenever x(t) is a linear combination of real/complex exponentials or  linear constant coefficient differential equations

  * Podemos visualizar isso usando, por exemplo, um Bode Plot, ou um Diagrama de Polos e Zeros
  
  * se o grau do numerador for x menor que o grau do denominador, a função terá x zeros no infinito.
  

## Região de convergência

* values of $s = \sigma + jw$ for which the transform converges (where the result exists)
* Ficaremos apenas com a restrição do tipo $s>c$, após a transformada (c é uma constante genérica da função que esta sendo transformada)
* Como $s$ é uma variável complexa ($s = \sigma + j\omega$)e apenas a sua parte real irá causar divergência (raciocínio instintivo: aplique a identidade de Euler na parte imaginária, tire o módulo e perceba que será sempre 1), analisaremos apenas $\R(s)>c$
* Para valores de s fora da ROC, o resultado obtido não corresponde à transformada de LaPlace de x(t)
* both the algebraic expression and the range of values of $s$ for which this expression is valid are required to fully define an LaPlace transform
* We can visualize that in an complex plane (or *s-plane*)
- **Propriedades**

  - The ROC is an strip parallel to the imaginary axis (so it depends only on the real part of s)
  - The ROC does not contain any poles (For rational Laplace transforms). The boundary in the s-plane is as dashed line
  - If x(t) is of finite duration and is absolutely integrable, then the ROC is the entire s-plane. podemos interpretar que a resposta possui denominador unitário ($D(s)=1$) e, portanto, nao contém polos 
  - Se $x(t)$ for causal, a ROC de $X(s)$ é à direita do polo com maior parte real
  - Se $x(t)$ for anticausal, a ROC de $X(s)$ é à esquerda do polo com menor parte real
  - A ROC é sempre delimitada por polos ou estende-se até infinito
  - for any signal with a Laplace transform, the ROC must be one of the cases above (entire s-plane, left half, right half or strip)
- **ROC the transformadas unilaterais**
  - Sempre o semi-plano à direita do pólo de Xn(s) com maior parte real
  - Isso e exatamente o mesmo caso da bilateral para sinais causais, que é o que a transformada unilateral avalia
  - Condição suficiente (mas não necessária) para a transformada unilateral convergir: f(t) deve ser contínua no intervalo $[0, \inf)$ e de ordem exponencial (veja a definição na conceituação)
- **Outras considerações**
  * A ROC de um produto é pelo menos a intersecção das duas ROCs. Pode ser maior, caso algum zero cancelar um polo
  * ==Se X(s) é absolutamente integrável, então X(s) não possui zeros==

## Transformada inversa

-   Caminho inverso

-   A transformada inversa também é linear

-   **Definição**
	-   Se F(s) corresponde a transformada de LaPlace de f(t), dizemos que f(t) é a transformada inversa de F(s)
	$$
	f(t)=L^{-1} \{F(s)\}
	$$
	* Podemos definir em termos da integral de linha (no plano complexo) da transformada: 
	
	  ![1556815705360](Images - 1.1 - Transformada de LaPlace/1556815705360.png)
	
	* c deve ser escolhido para que a linha de integração pertença à R.C. de X(s)
	
-   **Dicas**
	-   A inversa do produto nao é (diretamente) o produto das inversas
	-   $s^{-n}$: force um fatorial a aparecer
	-   Divisão de polinômios: quebre em frações particias
	-   Ao resolver uma EDO, isole no espaço s a função Y(s). Aplicando
		a derivada inversa sobre Y(s), chegaremos em y(t)

## Teoremas e propriedades
* ROC of any unilateral Laplace transform is always a right-half plane

* For bilateral transforms, we’ll explicity say what happens with the RC

* **Frequency shift**

  * Ou *Translação sobre o eixo s*

  * Região de convergência não muda
  $$
    L\{e^{at}f(t)\}=F(s-a)
  $$
  
  * Demonstração: aplicando na definição e, como as duas exponenciais terão a mesma base, somamos os exponentes. Como o expoente multiplicador é uma constante real, o resultado é só uma translação 

  * Para enfatizar a troca de s por s-a, podemos usar a notação:

  	![img](Images - 1.1 - Transformada de LaPlace/1542230764308.png)	
  	
  * Dicas de utilização
      * Use para evitar o uso de uma decomposição pro frações parciais 
      * Use quando o termo s do seno/cosseno está deslocado
      * Se parecer um quadrado irredutivel, talvez dê para completar quadrados
      * Se no numerador não aparecer o termo deslocado, force somando zero 
  
* **Time shift**

  * Se a>0, então:
    $$
    L\{f(t-a)u(t-a)\}=e^{-as}F(s)
    $$

  * Região de convergência fica deslocada pela mesma quantia

  * Demonstração: fazemos uma mudança de variáveis $v=t-a$ na integral, daí vai aparecer um a no expoente da exponencial e saírá da integral 

  * Se aparecer uma função deslocada por $a$ e multiplicada por $U_b$, quem manda é $b$: force o t fazendo uma substituição de variáveis. Ao calcular a Transformada, usaremos a linearidade da soma para separar o $f(t)$ do termo forçado $(a-b)$

  * Podemos enunciar de uma forma alternativa:

  	![img](Images - 1.1 - Transformada de LaPlace/1542671440214.png)
  
* **Time scaling**

  * ou *Fator de escala*
  * RC2 = a ⋅ (RC1)
  * $L \{f(at)\} = \frac{1}{|a|}F(\frac{s}{a})$
  * A partir daí tiramos a propriedade de time reversal, $L\{x(-t)\} = X(-s)$
  
* **Teorema da convuloção**

  * Se f(t) e g(t) são funçẽs contínua por partes em $[0, \infty)$ e de ordem exponencial, então:
    $$
    L\{f*g\}=F(s)G(s)
    $$

    $$
    L^{-1}\{F(s)G(s)\}=f*g
    $$

  * Ou seja, a transformada do produto é a convuloção das transformadas 

  * $RC \geq RC1 ∩ RC2$ (será maior se houver cancelamento de pólos e zeros)
  
* **Transformada da derivada**

  * ou *Diferenciação no domínio do tempo*
  * suponha de f é continua e que f’ é contínua por partes em
  	qualquer intervalo $0<=t<=A$
  * Suponha também que f seja de ordem exponencial
  * Então a transformada de LaPlace existe para s&gt;c (ver esse c
  	em *Ordem Exponencial*) e será:
  $$
  L\{f^{'}(t)\}=sL\{f(t)\}-f(0^-)
  $$

  * Para a segunda derivada, teremos:
    $$
    L\{f^{''}(t)\}=s^2L\{f(t)\} -sf(0^-) -f'(0^-)
    $$

  * Para a n-ésima derivada, teremos 
    $$
    L\{f^{(n)}(t)\}=s^nL\{f(t)\} -s^{n-1}f(0^-) - s^{n-2}f'(0^-) - ... - f^{(n-1)}(o^-)
    $$

  * O grau de s vai diminuindo e o grau da derivada vai aumentando

  * RC2 > RC1
  
  * Se nós sabemos as derivadas, mas não sabemos quem é $f(t)$ (não conhecemos sua dependência de t). Esse será o caso da resolução de uma equação diferencial  
  
  * Para a transformada completa (não unitaletal), os termos em $t=0$ somem
  
* **Derivada da transformada**

  * ou *Frequency diffenrentiation*

  * RC2 > RC1
  
  * Se f é contínua por partes e de ordem exponencial, então:
    $$
  L\{t^nf(t)\}=(-1)^n\frac{d^n}{ds^n}F(s)??
    $$
  
    $$
    L\{tf(t)\}=-F'(s)
    $$
  
* **Time integration**

    * $\int_{-\infty}^t f(\tau) d\tau \rightarrow \frac{1}{s}F(s)$
    * RC2 > RC1∩{ Re{s}>0 }

* **Frequency integration**

  * $L\left\{\frac{f(t)}{t}\right\}=\int_0^\infty F(s)ds$

* **Periodicidade**

  * Permite calcular a função em apenas um período

  * ==só para unilateral?==

  * Se f for uma função contínua por partes em $(0, \inf)$, de ordem exponencial e periódica com período T, então:
    $$
    L\{f(t)\}=\frac{1}{1-e^{-sT}}\int_0^T e^{-st}f(t)dt
    $$

  * Veja bem: essa integral até parece uma transformada de laplace, mas o limite de integração não é infinito, então não podemos usar as transformadas tabeladas  

* **Valor inicial**

  * Se $x(t)=0 \forall t>0$
  * Se x(0) não contém nenhum impulso ou suas derivadas
  $$
  f(0^+)=lim_{s\rightarrow\infty}sF(s)
  $$

* **Valor final**

  * Se $x(t)=0 \forall t>0$
  * Se x(0) não contém nenhum impulso ou suas derivadas
  * Se $\lim_{s\rightarrow 0} < \infty$
  $$
  f(\infty)=lim_{s\rightarrow0}sF(s)
  $$

* **Teorema de $s$ infinito**

      * ==provavelmente isso só vale ara unilateral==
      * Forma instintiva de interpretar: se F for uma função racional com a ordem do denominador maior que a ordem de numerador (o que faz sentido quando a ordem é exponencial), então esse limite sempre vai pra zero
      ![img](Images - 1.1 - Transformada de LaPlace/1542224863089.png)

* Outros

    * Se h(t) é real, its poles and zeros occurs in conjugate pairs  


## Análise de sistemas LIT

* Some characteristics are also easily identified from knowledge of the pole locations and the region of convergence.
* Y(s) = H(s )X(s ).
* H(s) is commonly referred to as the system function (or transfer function)
* ​:book: Oppe 693
* **Causality**
  *  If an LTI system is causal, then the ROC is a right sided plane.
  * When H(s) is rational, then the system is causal if and only if its ROC is the right half plane to the right of the rightmost pole, and the order of numerator is no greater than that of the denominator. 
* **BIBO stability**
  * An LTI system is stable if and only if the ROC of its system function H(s) includes the entire
    $jw$ axis
  * se estiver exatamente sobre o eixo, nao podemos garantir estabilidade (==ou podemos garantir que nao é?==)
  * A causal system with rational system function H(s)
    is stable if and only if all of the poles of H(s) lie in
    the left-half of the s-plane-i.e., all of the poles have
    negative real parts

## Dicas de resolução

* para denominadores com termos multiplicando, faça expansão por frações parciais
* 

# Aplicações

## Resolução de EDOs

*  Converte uma equação diferencial em uma equação algébrica

*  Geralmente é muito mais fácil de resolver do que a EDO em si

* **Procedimento**

  *  Aplicamos a Transformada em todos os termos da equação
  *  Nossos objetivo é encontrar $L\{f(t)\}$ para então aplicar a derivada inversa.
  *  Recomendo usar a notação enxugada $F(s)=L\{f(t)\}$ 
  *  Ou seja, isole $F(s)$

  ![img](Images - 1.1 - Transformada de LaPlace/Laplace-Transform-of-Elementary-Functions-1.jpg)

* **Análise de Y(s)**
  * A resposta pode ser decomposta em duas componentes (entrada e estado nulos)
  * Dica: mantenha as condições iniciais e a transformada de $X(s)$ até o final, assim ambas as parcelas se manterão separadas (e a resposta terá dois termos bonitinhos)
  * Transformada da Resposta ao entrada nulo: parte que contém $X(s)$, incluindo este ultimo
  * Transformada da Resposta à entrada nula: parte que contém as condições iniciais, incluindo-as
  * Polinómio característico: termos que multiplicam $Y(s)$
  * Função de transferência $H(s)$: termo que multiplica $X(s)$, sem incluir este
  * A função de transferência é a transformada da resposta ao impulso. Fornece a resposta em frequência do sistema



## Análise de Circuitos

-   pode ser aplicado a uma variedade maior de entradas que a análise
	fasorial.
-   Fornece a resposta em um único processo (não precisa somar a
	resposta transitória com a de regime permanente)
-   Circuitos equivalentes, com capacitores e indutores, existem apenas no domínio s, não podendo ser transformados de volta para o domínio do tempo.
-   Equacione preferencialmente a tensão no capacitor e a corrente no indutor 
-   A função de transferência H(s) de um circuito é a transformada de Laplace da resposta a um impulso, h(t).

* Utilizamos algumas transformações usuais para os principais componentes, e depois podemos trabalhar com o circuito resultante utilizando as técnicas tradicionais de circuitos

-   **Transformada do resistor**

	![img](Images - 1.1 - Transformada de LaPlace/1543275658912.png)

-   **transformada do indutor**

	![img](Images - 1.1 - Transformada de LaPlace/1543275718027.png)

	![img](Images - 1.1 - Transformada de LaPlace/1543275749573.png)

	![img](Images - 1.1 - Transformada de LaPlace/1543275799021.png)

-   **Transformada do capacitor**

	![img](Images - 1.1 - Transformada de LaPlace/1543275848881.png)

	![img](Images - 1.1 - Transformada de LaPlace/1543275876707.png)

	![img](Images - 1.1 - Transformada de LaPlace/1543275888470.png)

* **Etapas padronizadas**
1. Transformar o circuito do domínio do tempo para o domínio s.
  2. Resolver o circuito usando análise nodal, análise de malhas, transfor-
      mação de fontes, superposição ou qualquer outra técnica de análise de
      circuitos com a qual estejamos familiarizados. Todas são aplicáveis ao domínio s.
  3. Efetuar a transformada inversa da solução e, portanto, obter a solução factível no domínio do tempo.
