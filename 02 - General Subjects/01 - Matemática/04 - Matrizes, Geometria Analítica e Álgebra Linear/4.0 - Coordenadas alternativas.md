[TOC]

# Conceituação

* estudaremos aqui apena sistemas dextógenos (o produto vetorial de
  duas coordenadas dá a terceira)

# Coordenadas polares

* Outra forma de representar coisas, além da "tradicional"
    (retangular)

* Polar = medida em círculos concêntricos a partir do polo (origem)

* **Plano polar**

  * O ângulo é medido em relação ao eixo polar. Sentido positivo:
        anti-horário.

  * Polo → antiga origem

  * Eixo polar → antigas abcissas → reta com origem no polo → mede o
        módulo

  * Reta $\theta = \frac{\pi}{2}$ → antigas ordenadas

* **\Theta^TM\ThetaCoordenada polar**

  * Representação de um ponto na forma de (módulo, ângulo)

  * esse ângulo também é chamado de *ângulo azimutal *

    ![](./Images/6.0 - Coordenadas polares/media/image1.png)

    $$
    x = r \ast \text{cosθ}
    $$

    $$
    y = r \ast \text{senθ}
    $$

    $$
    \text{tgθ} = \frac{y}{x}
    $$

    $$
    r = \sqrt{x^{2} + y}²
    $$

  * Na forma polar, cada ponto pode ser representado de infinitas formas

  * Ex:
    $$
    \left( r,\theta \right) = \left( r,\theta + 2\pi \right) = \left( r,\theta - 2\pi \right) = \left( r,\theta + 4\pi \right) = \left( - r,\theta + \pi \right) = \ldots
    $$

  * Ou seja, $\left( r,\theta \right)$ não é um par ordenado, e sim uma
    família de pontos

  * Podemos interpretar um ponto como sendo um [número
    complexo]{.underline}

  * Conversão entre coordenadas polares e retangulares: utilizamos as
    equações

  ![](./Images/6.0 - Coordenadas polares/media/image2.png)

## Curvas no plano polar 

* Conjunto de pontos representados por coordenadas polares

* Formato genérico:

$$
r = f\left( \theta \right)
$$

* Ex: $\left( r,\theta \right),\text{tal}\text{que}r = \theta$

![](./Images/6.0 - Coordenadas polares/media/image3.png){width="4.603472222222222in"
height="3.6354166666666665in"}

-   Geralmente senos são simétricos em relação à y e cossenos em relação
    à x

-   [[https://www.desmos.com/calculator/stqeqpabqh]{.underline}](https://www.desmos.com/calculator/stqeqpabqh)

<!-- -->

-   **Retas**

    -   Definida só por um ângulo → passando pela origem

    -   $\text{Rsenθ} = a$ → reta paralela ao eixo x → y=a

    -   $\text{Rcosθ} = a$ → reta parelela ao eixo y → x=a

-   **Circunferências**

    -   $R = A$

        -   Centrada no polo com raio a

        -   Período $2\pi$

    -   $R = \text{Acosθ}$

        -   Centrada no eixo polar e tangente à $\theta = \frac{\pi}{2}$

        -   A = diâmetro

        -   A \> 0 → à direita do polo

        -   A \< 0 → à esquerda do polo

        -   Período $\pi$

    -   $R = \text{Asenθ}$

        -   Centrada no eixo $\theta = \frac{\pi}{2}$ e tangente ao eixo
            polar

        -   A = diâmetro

        -   A \> 0 → acima do polo

        -   A \< 0 → abaixo do polo

        -   Período $\pi$

* **Limaçon**

  * Do francês *caracol*

  * A voltinha do laço sempre acontece na origem

  * Genericamente:

  $$
  r = a \pm b \ast \text{cosθ}
  $$

  > Ou

  $$
  r = a \pm b \ast \text{senθ}
  $$

  * Período de 2pi

  * <u>Formato</u>
* B\>a → a curva tem um laço interno
  
* B=a → a curva é chamada de cardioide (parece um coração)
  
* B\<a → a curva não tem laço (parece um círculo amassado)
  
* Tudo em modulo
  
  * <u>[Alinhamento]{.underline}</u>
    -   Cosseno → alinhada com o eixo x (caracol deitado)
  
    -   Seno → alinhada com o eixo y (caracol de pé)
  
  * <u>[Orientação]{.underline}</u>
    -   B\>0 → aponta pro lado positivo (pra direita ou pra cima)
  
    -   B\<0 → aponta pro lado negativo (pra esquerda ou pra baixo)
  
*   **Rosáceas**

  * Formam várias pétalas

   * Genericamente:

  $$
  r = \cos\left( n \ast \theta \right)
  $$

  >Ou

  $$
  r = \text{sen}\left( n \ast \theta \right)
  $$

  * Valores fracionados de "a" fodem seu período

  * [Nº de pétalas]{.underline}

    * Válido para $n \in N$

    * N par → 2\*n pétalas

    * N ímpar → n pétalas

* **Elipse**
  
  * http://www.sato.prof.ufu.br/LeisKepler/node3.html
  
## Técnicas de esboço

* **Simetria**

  * Coisas que, quando espelhadas em relação a algo, se repetem

  * Se elas se repetem, podemos representar só um intervalo e dizer que
    o resto se repete

  * Considerando sempre os pontos P e Q, definidos em coordenadas
    polares $(r,\ \theta)$

* **Em relação ao eixo polar**
  * Se, e somente se,
    $\left( r_{p},\theta_{p} \right) = \left( r_{q},{- \theta}_{q} \right)$
  * Analisamos só de 0 a $\pi$
* **Em relação à** $\mathbf{\theta = \pi/2}$

  -   Se, e somente se,
      $\left( r_{p},\theta_{p} \right) = \left( r_{q},{\pi - \theta}_{q} \right)$

  -   Analisamos só de $- \frac{\pi}{2}\text{\ a\ }\frac{\pi}{2}$

* **Em relação ao polo**

  -   Se, e somente se,
      $\left( r_{p},\theta_{p} \right) = \left( r_{q},{\pi + \theta}_{q} \right)$

  -   Analisamos o 1º e 3º quadrante ou o 2º e o 4º

  -   Gira 180º em torno do polo

* **Passa pelo polo?**

  * Se o ponto $(0,\theta)$ pertence à reta

  * Ou seja, existe algum ângulo que zere o módulo?

* **Chutar pontos da curva**

  * Chutamos de forma inteligente

  * Escolhemos valores adequados para a simetria

  * Quantos pontos acharmos necessário (geralmente uns 4\~5)

  * Escolhemos ângulos notáveis ou que, quando inseridos na equação,
    virem notáveis

  * Ex: $\cos\left( \frac{\theta}{3} \right)$ → chutamos valores três
    vezes maior que os notáveis

* **Máximos e mínimos**

  * Procedimento normal

  * Deriva a função em relação à $\theta$

  * Derivada igual a zero: ponto crítico

  * Derivada segunda classifica entre ponto mínimo e máximo

  * *Detalhe:* é a máxima/mínima distância do ponto ao polo

  * *Ex:* $r = 1 + 2cos\theta$ *→* (3, 0º) é PMR e (-1, 180º) é PmR

  ![](./Images/6.1 - Coordenadas polares - Esboço de gráficos/media/image1.png){width="2.6132075678040243in"
  height="2.2433267716535434in"}

* **É preciso verificar para mais de uma volta?**
  * Todas as análises que fizemos são válidas só para o primeiro
    quadrante

  * E nos outros?

  * Substituímos $\theta$ por $\theta + 2k\pi$ e verificamos se o
    resultado é diferente

  * Se o resultado não for múltiplo do período da função, preciso testar
    outras voltas

  * Pelo quanto falta de $\text{kπ}$ até o período podemos saber a cada
    quantas voltas o traçado se repete

# Coordenadas cilíndricas 

-   Representa um ponto pela tripa ordenada (r, $\varphi$ , z)

-   As duas primeiras são a representação polar da projeção do ponto
    sobre o plano xy, enquanto a terceira é a distância orientada da
    projeção ao ponto

![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image1.png){width="2.120138888888889in"
height="1.7451388888888888in"}

![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image2.png){width="3.40625in"
height="3.46875in"}

$o \leq r < \infty$

$o \leq \phi \leq 2\pi$

$- \infty < z < \infty$

-   Produto dos vetores canônicos:

    ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image3.png){width="2.504861111111111in"
    height="1.45625in"}

-   Os vetores unitários dessa base são perpendiculares, de forma que o
    módulo continua sendo calculado da mesma
    forma,$\mathrm{|A|} = \sqrt{A_{p}^{2} + A_{\phi}^{2} + A_{z}^{2}}$

* **Conversões**

  * Conversão de cilíndricas para retangulares

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image4.png)

  * Conversão de retangulares para cilíndricas

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image5.png)

  * Conversão dos canônicos cilíndricos para retangulares:

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image6.png)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image7.png)

  * Conversão dos canônicos retangulares para cilíndricos:

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image8.png)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image9.png)

* **Exemplos de curvas **

  * Cilindro → $r = c$

  * Cone → $z = r$

* Elementos diferenciais

  * Dado um certo ponto, variações diferencais nas direções dos vetores canônicos encerarão um volume (quando as tres direções variarem) ou uma superfície (quando dois variárem, a superfície apontará na direção do terceiro)

  * O calculo do volume é dado pelo produto (tradicional, já que as diferenciais são quantidades escalares) das tres diferenciais

  * O calculo da superfície e dado pelo produto (tradicional) das duas diferenciais, multiplicado pelo canônico da terceira direção (já que superfícies são orientadas)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image10.png)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image11.png)



# Coordenadas esféricas

* Representa um ponto pela tripla ordenada$\left( \rho,\theta,\varphi \right)$

* $\rho$ → rô → distância $\overline{\text{OP}}$

* $\theta$ → teta → ângulo do eixo z com a reta $\overline{\text{OP}}$ (co-latitude)

* $\varphi$ → fi → ângulo do eixo x com a projeção de $\overline{\text{OP}}$ (azimutal)

![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image12.png)

$$
o \leq r < \infty
$$

$$
o \leq \theta \leq \pi
$$

$$
o \leq \phi \leq 2\pi
$$

* Produto dos vetores canônicos:
  * Os vetores unitários dessa base são perpendiculares, de forma que o módulo continua sendo calculado da mesma forma,$\mathrm{|A|} = \sqrt{A_{p}^{2} + A_{\theta}^{2} + A_{\phi}^{2}}$

  * Distância entre dois vetores: $d = \mathrm{|}r_{2} - r_{1}\mathrm{|}$

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image13.png)

* **Conversões**

  * usamos trigonometria

  * Polar para retangular:

  $$
  x = \rho \ast \text{senθ} \ast \text{cosφ}
  $$

  $$
  y = \rho \ast \text{senθ} \ast \text{senφ}
  $$

  $$
  z = \rho \ast \text{cosθ}
  $$

  * Retangular para polar:

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image14.png)

  * Canônicos esféricos para retangulares:

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image15.png)
  
  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image16.png)

  * Canônicos retangulares para esféricos:

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image17.png)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image18.png)

* **Exemplo de curvas esféricas**

  * Esfera de raio R → $\rho = R$

  * Plano vetical → $\varphi = c$

  * Cone → $\theta = c$

* Elementos diferenciais

  * mesma lógica das cilindricas

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image19.png)

  ![](./Images/6.2 - Coordenadas esféricas e cilíndricas/media/image20.png)
