# Conceptualization

* studies properties of spaces that are preserved under continuous deformations
* generalization of classical geometry
* it's like "rubber-sheet geometry" because it focuses on the fundamental structure of objects, ignoring details like distances and angles.
* For Projective Geometry and Homogeneous Coordinates, see in *Computer Vision*
* **Topological space**
  * set endowed with a structure (aka a topology)
  * allows defining continuous deformation of subspaces, and, more generally, all kinds of continuity
  * <u>Topologically equivalent spaces</u>
    * If two spaces are related by a homeomorphism
    * Examples: A circle and an ellipse, a sphere and a cube
    * Counter example (are not homeomorphic): A circle and a figure-eight
* **Continuous deformations**
  * Or *Homeomorphic transformation*, or *isotopy*, or *homotopy*, or *homeomorphism*
  * preserves topology
  * Function between two spaces that is bijective, continuous, and has a continuous inverse.
  * 
  * stretching
  * twisting
  * bending
  * Are NOT continuous transformation: tearing, gluing, etc
  * 
* **Topological properties**
  * Properties preserved under continuous deformations
  * connectedness
    *  A space is connected if it is "all in one piece."
    * Example: A circle is connected; two separate circles are not.
  * compactness
    * A space is compact if it can be "contained" in a finite sense.
      - Example: A closed interval $[0, 1]$ is compact; the open interval $(0,1)$ is not.
  * number of "holes" in a shape
* **Subfields**
  * <u>Differential Topology</u>: Merges topology with differential geometry, studying smooth manifolds
  * <u>Algebraic Topology</u>: Uses algebraic methods to study topological spaces, sometimes intersecting with geometric problems.
  * <u>Geometric Topology</u>: A subfield of topology that directly addresses problems related to geometry, such as studying surfaces, manifolds, and their embeddings.

## Manifolds

*  topological space that locally resembles Euclidean space near each point
* Riemannian manifold: allows distances and angles to be measured
* Lorentzian manifolds: include time?