# Institutions

## Legislative

* It has bicameral legislature, where the parlament requires approval from the Council
* European Parliament
  * 720 members elected by popular vote; seats are allocated by proportional
  * it does not formally possess the right of initiative (i.e. the right to formally initiate the legislative procedure) in the way that most national parliaments of the member states do, as the right of initiative is a prerogative of the European Commission
  * headquarters are in Strasbourg, France, and has its administrative offices in Luxembourg
  *  
  * organised into seven different parliamentary groups
*  Council of the European Union
  * Or *Council of Ministers*