Conceitos demográfico

-   Estudo sobre os fenômenos populacionais de uma região/país.

-   IBGE → Instituto brasileiro de geografia e estatística

<!-- -->

-   **População absoluta**

    -   População total

    -   População alta = populoso

        -   China → 1 bilhão e 300 milhões de habitantes

        -   Brasil → quinto país mais populoso

-   **População relativa**

    -   $\frac{população\ total}{área\ total}$

    -   Habitantes por quilômetros quadrados

    -   População relativa alta = densamente povoado

        -   Hong Kong

-   **Fertilidade**

    -   Nº de mulheres em idade fértil

    -   De forma geral, dos 15 aos 49 anos.

-   **Fecundidade**

    -   Número médio de filhos por mulher

-   **Taxa de fecundidade/natalidade**

    -   $\frac{\text{numero\ de\ nacimento}}{população\ absoluta}\ .1000$

-   **Taxa de mortalidade**

    -   $\frac{\text{numero\ de\ mortes}}{população\ absoluta}\ .1000$

    -   No brasil, homens jovens morrem mais.

-   **Crescimento Vegetativo**

    -   Ver mais em *"Teorias demográficas"*

    -   Natalidade -- mortalidade

-   **Crescimento horizontal**

    -   Diferença entre imigração e emigração

-   **Crescimento demográfico**

    -   Crescimento vegetativo + crescimento horizontal

    -   Ou seja, nem todo crescimento vegetativo ocasiona um crescimento
        demográfico.

Pirâmide etária -- países subdesenvolvidos

![https://geoportugal.files.wordpress.com/2010/09/piramidemonc.jpg](./Images/1.1 - Conceitos demográficos e Pirâmide etária/media/image1.jpeg){width="6.056735564304462in"
height="3.19911198600175in"}

-   Alta taxa de natalidade

-   Alta mortalidade

-   Baixa expectativa de vida

-   Precisam pensar em\...

    -   Desenvolvimento da infraestrutura

    -   Melhoria da saúde

    -   Preparar o mercado pra essa "mão de obra" que virá

-   Pirâmide etária -- países em desenvolvimento

> ![http://rio-negocios.com/wp-content/uploads/2015/02/piramide\_etaria\_20102.png](./Images/1.1 - Conceitos demográficos e Pirâmide etária/media/image2.png){width="5.231833989501312in"
> height="4.388534558180227in"}

-   Diminuição gradual da taxa natalidade

-   Diminuição gradual da mortalidade

-   Aumento da expectativa de vida

-   "Processo de envelhecimento da população"

-   Fruto da transição demográfica (ver *teoria demográficas)*

-   Precisam pensar em\...

    -   Previdência social (aumento da população improdutiva)

    -   Políticas de migração, afinal, a população não vai querer ocupar
        os empregos de base.

-   Pirâmide etária -- países desenvolvidos

![http://c3378583.r83.cf0.rackcdn.com/content/pic8fbee0c1e5d20965fb8ab47b8e80b9c0.png](./Images/1.1 - Conceitos demográficos e Pirâmide etária/media/image3.png){width="6.76399387576553in"
height="3.082147856517935in"}

-   Baixa natalidade

-   Baixa mortalidade

-   Alta expectativa de vida

-   Precisa pensar em\...

    -   Inventivo para a população velha trabalhar

    -   Adiamento da aposentadoria

    -   Políticas de imigração

-   Razão de dependência

    -   Relação entre População Economicamente Ativa e a população que
        não trabalha de um país.

    -   Desenvolvidos → em 50 e 75

    -   Desenvolvimento →entre 85 e 105

Fenômenos populacionais

-   Guerras, doenças, crises, tudo isso influencia a natalidade.

-   Se tem uma guerra, a população jovem morre, gerando também um
    déficit de nascimentos na próxima geração.

-   Políticas de imigração podem alterar drasticamente a pirâmide de uma
    população.

![http://image.slidesharecdn.com/piramadespopulacionais-130108112340-phpapp01/95/piramades-populacionais-42-638.jpg?cb=1357644503](./Images/1.1 - Conceitos demográficos e Pirâmide etária/media/image4.jpeg){width="3.9039162292213474in"
height="2.931301399825022in"}

![http://image.slidesharecdn.com/piramadespopulacionais-130108112340-phpapp01/95/piramades-populacionais-30-638.jpg?cb=1357644503](./Images/1.1 - Conceitos demográficos e Pirâmide etária/media/image5.jpeg){width="5.4473720472440945in"
height="4.834722222222222in"}
