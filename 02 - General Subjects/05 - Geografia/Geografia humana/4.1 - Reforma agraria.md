Primórdios da reforma agrária

-   Redistribuição das terras rurais

    -   Do latifúndio para o minifúndio.

    -   A terra é concentrada desde o início da colonização, onde a
        terra era dividida em grandes lotes de "capitanias" e
        "sesmarias"

    -   A lei das terras só aumentou a concentração

    -   \"Enquanto o trabalho era escravo, a terra era livre. Quando o
        trabalho ficou livre, a terra virou escrava\"

-   Diminuição das propriedades e aumento no número de proprietários
    (não ao latifúndio)

-   Aumento da produtividade.

-   Revoltas existem desde a muito tempo: Canudos** **(1893),
    Contestado** **(1908), etc.

-   Começaram a ser organizar após 1945, paralela a outras reformas de
    base (urbana, bancaria, estudantil, etc)

-   Ocupação da terra violência

-   Jango reformas de base reforma agrária deposto pela ditadura

-   Regime militar

    -   Estatuto da Terra, para "apaziguar"

    -   Previa uma reforma agrária, mas sem prazo definido.

-   1979 surgimento do MST

-   A redemocratização

    -   Reafirmação da reforma agraria

    -   "A terra tem função social"

    -   Art. 5º inciso XXII: é garantido o direto de propriedade

    -   Art. 5º inciso XXIII: a propriedade atenderá a sua função social

Hoje

-   Dois métodos: expropriação e compra

    -   Segundo a lei, "a propriedade rural que não cumprir a função
        social é passível de desapropriação"

    -   Atualmente, a maior parte é comprada, apesar disso ser muito
        criticado (a compra)

-   INCRA Instituto Nacional de Colonização e Reforma Agrária

-   Continua sim existindo muita terra improdutiva (latifúndio) em que
    não se produz NADA.

![](./Images/4.1 - Reforma agraria/media/image1.png){width="5.846153762029746in"
height="2.4339621609798776in"}

![](./Images/4.1 - Reforma agraria/media/image2.png){width="3.7465277777777777in"
height="3.4057895888014in"}
