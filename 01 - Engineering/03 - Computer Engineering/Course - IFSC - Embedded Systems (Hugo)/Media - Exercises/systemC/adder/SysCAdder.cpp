//============================================================================
// Name        : SysCAdder.cpp
// Author      : Hugo
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <systemc.h>
#include "fulladder.h"
#include <iostream>
using namespace std;

int sc_main (int argc, char* argv[]) {
	cout << "Hello SystemC" << endl; // prints !!!Hello World!!!

	sc_signal<bool>   _a, _b, _sum;
	sc_signal<bool>   _carry_in, _carry_out;
	FullAdder adder("FullAdder");

	adder.a(_a);
	adder.b(_b);
	adder.carry_in(_carry_in);
	adder.sum(_sum);
	adder.carry_out(_carry_out);

	sc_trace_file *wf = sc_create_vcd_trace_file("trace");
	// Dump the desired signals
	sc_trace(wf, _a, "Entrada_A");
	sc_trace(wf, _b, "Entrada_B");
	sc_trace(wf, _carry_in, "Entrada_Carry");
	sc_trace(wf, _carry_out, "Saida_Carry");
	sc_trace(wf, _sum, "Saida");

	cout << "@" << sc_time_stamp() << " Starting simulation." << endl;

	sc_uint<3> reg = 0;
	for(int i = 0; i < 8; i++){
		_a.write(reg[0]);
		_b.write(reg[1]);
		_carry_in.write(reg[2]);
		reg++;
		sc_start(1,SC_NS);


	}

	cout << "@" << sc_time_stamp() << " Ending simulation." << endl;

	sc_close_vcd_trace_file(wf);
	return 0;
}
