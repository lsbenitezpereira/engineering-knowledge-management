/*
 * HalfAdder.h
 *
 *  Created on: Apr 8, 2020
 *      Author: hugom
 */

#ifndef HALFADDER_H_
#define HALFADDER_H_

#include <systemc.h>

SC_MODULE (HalfAdder) {
	sc_in<bool>  a, b;
	sc_out<bool> s, c;

	void process() {
		s = a ^ b;
		c = a & b;
	}

	SC_CTOR(HalfAdder) {
		SC_METHOD(process);
		sensitive << a << b;
	}
};

#endif /* HALFADDER_H_ */
