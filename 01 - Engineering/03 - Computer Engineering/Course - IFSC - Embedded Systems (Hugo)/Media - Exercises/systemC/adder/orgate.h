/*
 * orgate.h
 *
 *  Created on: Apr 8, 2020
 *      Author: hugom
 */

#ifndef ORGATE_H_
#define ORGATE_H_

#include <systemc.h>

SC_MODULE (OrGate){
	sc_in<bool> a, b;
	sc_out<bool> out;

	void do_the_thing() {
		out.write(a.read() | b.read());
	}

	SC_CTOR(OrGate){
		SC_METHOD(do_the_thing);
		sensitive << a << b;
	}
};

#endif /* ORGATE_H_ */
