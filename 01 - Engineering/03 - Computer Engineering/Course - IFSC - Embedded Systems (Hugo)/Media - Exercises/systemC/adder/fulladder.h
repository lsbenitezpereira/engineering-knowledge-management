/*
 * FullAdder.h
 *
 *  Created on: Apr 8, 2020
 *      Author: hugom
 */

#ifndef FULLADDER_H_
#define FULLADDER_H_

#include <systemc.h>
#include "orgate.h"
#include "halfadder.h"

SC_MODULE (FullAdder) {
	sc_in<bool>  a, b, carry_in;
	sc_out<bool> sum, carry_out;

	SC_CTOR(FullAdder): _or("Or"), _adder01("Adder01"), _adder02("Adder02") {
		_adder01.a(a);
		_adder01.b(b);
		_adder01.s(_s_msum);
		_adder01.c(_s_carry1);

		_adder02.a(_s_msum);
		_adder02.b(carry_in);
		_adder02.s(sum);
		_adder02.c(_s_carry2);

		_or.a(_s_carry1);
		_or.b(_s_carry2);
		_or.out(carry_out);
	}

private:
	OrGate     _or;
	HalfAdder  _adder01;
	HalfAdder  _adder02;
	sc_signal<bool> _s_msum;
	sc_signal<bool> _s_carry1;
	sc_signal<bool> _s_carry2;

};



#endif /* FULLADDER_H_ */
