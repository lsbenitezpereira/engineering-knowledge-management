
#ifndef ALU_H_
#define ALU_H_

#include <systemc.h>

SC_MODULE(ALU){
    // Declare ports
    sc_in<sc_uint<8>  > a;
	sc_in<sc_uint<8>  > b;
	sc_in<sc_uint<3>  > op;
	sc_out<sc_uint<1> > flag_n;
	sc_out<sc_uint<1> > flag_z;
	sc_out<sc_uint<8> > out;

	sc_uint<8>         out_temp;

    // Main process
	void process(){
		// Operations
		//std::cout << "ula will exec now: " << op.read() << "\n";
		if (op.read()==0){ //LDA
			//std::cout << "         it's an LDA :) \n";
			out_temp = a.read();
		} else if (op.read()==1){ //ADD
			//std::cout << "         it's an ADD :) \n";
			out_temp = a.read() + b.read();
		} else if (op.read()==2){ //AND
			//std::cout << "         it's an AND :) \n";
			out_temp = a.read() & b.read();
		} else { //OR
			//std::cout << "         it's an OR :) \n";
			out_temp = a.read() | b.read();
		}

		// Flags
		if (out_temp == 0){
			flag_z = 1;
		} else {
			flag_z = 0;
		}
		if (out_temp >= 128){
			flag_n = 1;
		} else {
			flag_n = 0;
		}


		out = out_temp;
	}

	SC_CTOR(ALU){
		SC_METHOD(process);
		sensitive << a;
		sensitive << b;
		sensitive << op;
	}

    // Unit testbench
	static int testbench(const char * trace_file){
	    std::cout << "Testing alu...\n";
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);
	    
	    //Signals used in testbench
		sc_signal<sc_uint<8> > a;
		sc_signal<sc_uint<8> > b;
		sc_signal<sc_uint<3> > op;
		sc_signal<sc_uint<1> > flag_n;
		sc_signal<sc_uint<1> > flag_z;
		sc_signal<sc_uint<8> > out;

        //Include signals to trace
	  	sc_trace(tf, a, "a");
		sc_trace(tf, b, "b");
		sc_trace(tf, op, "op");
		sc_trace(tf, flag_n, "flag_n");
		sc_trace(tf, flag_z, "flag_z");
		sc_trace(tf, out, "out");

        //Component initialization
		ALU alu("Alu");

		// Signal Connections
		alu.a(a);
		alu.b(b);
		alu.op(op);
		alu.flag_n(flag_n);
		alu.flag_z(flag_z);
		alu.out(out);

		// Generate Stimuli
		a = 1;
		b = 2;
		op = 0; //LDA
		sc_start(1, SC_NS);
		assert(alu.out.read() == 1);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 0);

		a = 50;
		b = 10;
		op = 1; //ADD
		sc_start(1, SC_NS);
		assert(alu.out.read() == 60);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 0);

		a = 0;
		b = 0;
		op = 1; //ADD
		sc_start(1, SC_NS);
		assert(alu.out.read() == 0);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 1);

		a = 127;
		b = 0;
		op = 1; //ADD
		sc_start(1, SC_NS);
		assert(alu.out.read() == 127);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 0);

		a = 127;
		b = 1;
		op = 1; //ADD
		sc_start(1, SC_NS);
		assert(alu.out.read() == 128);
		assert(alu.flag_n.read() == 1);
		assert(alu.flag_z.read() == 0);

		a = 3;
		b = 2;
		op = 2; //AND
		sc_start(1, SC_NS);
		assert(alu.out.read() == 2);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 0);

		a = 255;
		b = 255;
		op = 2; //AND
		sc_start(1, SC_NS);
		assert(alu.out.read() == 2);
		assert(alu.flag_n.read() == 1);
		assert(alu.flag_z.read() == 0);

		a = 1;
		b = 2;
		op = 3; //OR
		sc_start(1, SC_NS);
		assert(alu.out.read() == 3);
		assert(alu.flag_n.read() == 0);
		assert(alu.flag_z.read() == 0);

	
		
        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}
};

#endif /* ALU_H_ */
