#ifndef NEANDER_H_
#define NEANDER_H_

#include <systemc>

#include "mux.h"
#include "alu.h"
#include "counter.h"
#include "reg.h"
#include "mem.h"
#include "control.h"

// Top-level
SC_MODULE(Neander){
    // Input Port
	sc_in<bool>  clock;


    // Internal Components
	Reg		 	_ac;
	Counter		_pc;
	Mux      	_mux_addr;
	Mem 		_mem;
	ALU			_alu;
	Control		_uc;

	// Internal Signals
	sc_signal<sc_uint<8> > mem_data_out;
	sc_signal<sc_uint<1> > ula_flag_n;
	sc_signal<sc_uint<1> > ula_flag_z;

	sc_signal<sc_uint<3> > ula_op;
	sc_signal<sc_uint<1> > ac_load;
	sc_signal<sc_uint<1> > pc_inc;
	sc_signal<sc_uint<1> > pc_load;

	sc_signal<sc_uint<1> > mem_addr_sel;
	sc_signal<sc_uint<1> > mem_wr;
	sc_signal<sc_uint<1> > mem_rd;

	sc_signal<sc_uint<8> > ac_data_out;
	sc_signal<sc_uint<8> > ula_out;

	sc_signal<sc_uint<8>  > pc_data_out;
	sc_signal<sc_uint<8>  > mem_addr_in;


    void process(){
		if (clock.read()==0){ //falling edge, everything is already stable
			std::cout << "@" << sc_time_stamp() << ") State=" << ToString(_uc.state) << "\n";
			//std::cout << "         ula_out=" << ula_out << "\n";
			//std::cout << "         ac_data_out=" << ac_data_out << "\n";
			//std::cout << "         mem_data_out=" << mem_data_out << "\n";
			//std::cout << "         pc_inc=" << pc_inc << "\n";
			//std::cout << "         instruction=" << mem_data_out << "\n";
			if (_uc.state==FETCH_INST){
				//std::cout << "         pc_data_out=" << pc_data_out << "\n";
				//std::cout << "         mem_addr_in=" << mem_addr_in << "\n";
				//std::cout << "         mem_wr=" << mem_wr << "\n";
			}
			if (_uc.state==DECODE_DECODE_INST){
				std::cout << "         pc_data_out=" << pc_data_out << "\n";
				std::cout << "         instruction=" << ToStringISA(_uc.instruction_decode.read()) << " (" << _uc.instruction_decode.read() << ")\n";
			}

			//if (_uc.state==DECODE_DECODE_INST){
			//	std::cout << "@" << sc_time_stamp() << ") DECODE_DECODE_INST\n";
			//	std::cout << "         pc_data_out=" << pc_data_out << "\n";
			//	std::cout << "         instruction=" << mem_data_out << "\n";
			//}
//
			//if (_uc.state==EXECUTE_MEM_WRITE){
			//	std::cout << "          writing " << ac_data_out << " to memory in positon " << mem_addr_in << "\n"; 
			//}
			//if (_uc.state==EXECUTE_ALU_AC_WRITE){
			//	std::cout << "         writing " << ula_out << " to ac output\n"; 
			//}
		}
	}

	Mem* mem(){
		return &_mem;
	}

	// Main process
	void execution_stats(){
		cout << "++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
		cout << "Instructions Executed: " << _uc.instruction_count() << endl;
		cout << "               Cycles: " << _uc.cycles() << endl;
		cout << "                  CPI: " << _uc.cycles()/(double)_uc.instruction_count() << endl;
		cout << "++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	}

	SC_CTOR(Neander):
		_ac("AC"), _pc("PC"), _mux_addr("MUX_Addr"),
		_mem("MEM"), _alu("ALU"), _uc("UC")
	{
		//Connect Signals
		_uc.instruction_decode(mem_data_out);
		_uc.ula_flag_n(ula_flag_n);
		_uc.ula_flag_z(ula_flag_z);
		_uc.ula_op(ula_op);
		_uc.ac_load(ac_load);
		_uc.pc_inc(pc_inc);
		_uc.pc_load(pc_load);
		_uc.clock(clock);
		_uc.mem_addr_sel(mem_addr_sel);
		_uc.mem_wr(mem_wr);
		_uc.mem_rd(mem_rd);
		
		_alu.a(mem_data_out);
		_alu.b(ac_data_out);
		_alu.op(ula_op);
		_alu.flag_n(ula_flag_n);
		_alu.flag_z(ula_flag_z);
		_alu.out(ula_out);

		_pc.inc(pc_inc);
		_pc.load(pc_load);
		_pc.clock(clock);
		_pc.in(mem_data_out);
		_pc.out(pc_data_out);


		_mux_addr.sel(mem_addr_sel);
		_mux_addr.a(pc_data_out);
		_mux_addr.b(mem_data_out);
		_mux_addr.out(mem_addr_in);

		_mem.clock(clock);
		_mem.wr(mem_wr);
		_mem.rd(mem_rd);
		_mem.addr_in(mem_addr_in);
		_mem.data_in(ac_data_out);
		_mem.data_out(mem_data_out);

		_ac.load(ac_load);
		_ac.in(ula_out);
		_ac.out(ac_data_out);

		//Declare process
		SC_METHOD(process);
		sensitive << clock;
	}

    // Unit testbench
	static int testbench(const char * trace_file){
		std::cout << "Testing top entity...\n";
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);

	    //Signals used in testbench
		sc_clock clock("clock", sc_time(20,SC_NS), 0.5, sc_time(10,SC_NS), false);

        //Include signals to trace
		sc_trace(tf, clock, "clock");

        //Component initialization
		Neander neander("Neander");
		Mem* mem_ptr = neander.mem();

		// Signal Connections
		neander.clock(clock);

		sc_trace(tf, neander.mem_data_out, "mem_data_out");
		sc_trace(tf, neander.ula_flag_n, "ula_flag_n");
		sc_trace(tf, neander.ula_flag_z, "ula_flag_z");
		sc_trace(tf, neander.ula_op, "ula_op");
		sc_trace(tf, neander.ac_load, "ac_load");
		sc_trace(tf, neander.pc_inc, "pc_inc");
		sc_trace(tf, neander.pc_load, "pc_load");
		sc_trace(tf, neander.mem_addr_sel, "mem_addr_sel");
		sc_trace(tf, neander.mem_wr, "mem_wr");
		sc_trace(tf, neander.mem_rd, "mem_rd");
		sc_trace(tf, neander.ac_data_out, "ac_data_out");
		sc_trace(tf, neander.ula_out, "ula_out");
		sc_trace(tf, neander.pc_data_out, "pc_data_out");
		sc_trace(tf, neander.mem_addr_in, "mem_addr_in");
		sc_trace(tf, neander._uc.state, "state");


		// Generate Stimuli
		sc_uint<8> my_program[15] = {
			208, // 00 (LDA)
			128, // 01 (OPERAND)
			144, // 02 (ADD)
			129, // 03 (OPERAND)
			240, // 04 (JN )
			10 , // 05 (OPERAND)
			128, // 06 (STA)
			130, // 07 (OPERAND)
			224, // 08 (JMP)
			12 , // 09 (OPERAND)
			128, // 10 (STA)
			131, // 11 (OPERAND)
			112  // 12 (HLT)
		};
		sc_uint<8> my_data[10] = {
			15,
			-20
		};
		mem_ptr->set_mem(
			my_program,
			15,
			my_data,
			10
		);
		//std::cout<<"--------------------------------------\n";
		//std::cout<<"Memory at the begining:\n";
		//mem_ptr->dump_mem();

		sc_start(1000, SC_NS);
		std::cout<<"--------------------------------------\n";
		std::cout<<"Memory at the end:\n";
		mem_ptr->dump_mem(128, 132);

		assert(mem_ptr->get_mem(131) == 251);
		
        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}

private:
};

#endif /* NEANDER_H_ */
