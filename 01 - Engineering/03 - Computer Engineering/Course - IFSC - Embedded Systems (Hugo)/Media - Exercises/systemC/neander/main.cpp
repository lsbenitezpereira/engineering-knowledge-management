
/* 
g++ -I/home/benitez/sysC/lib/systemc-2.3.3/include main.cpp /home/benitez/sysC/lib/systemc-2.3.3/lib64-linux64/libsystemc.a -o main && ./main
*/

#include <systemc.h>
#include "neander.h"

#include <iostream>
using namespace std;

int sc_main( int argc, char *argv[] )  {
	sc_report_handler::set_verbosity_level(0);

    // Abaixo estão as rotinas de testes unitários de cada módulo.
    // Não é possível no systemC executá-las simultaneamente, desta
    // forma, apenas um dos testes devem ser descomentados por vez.
    
	//Mux::testbench("trace");
	//Reg::testbench("trace");
	//Counter::testbench("trace");
	//ALU::testbench("trace");
	//Mem::testbench("trace");
	//Control::testbench("trace");
	Neander::testbench("trace");

	cout << "Ending Simulation!" << endl;

	return 0;
};
