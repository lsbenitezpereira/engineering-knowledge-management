
#ifndef COUNTER_H_
#define COUNTER_H_

#include <systemc.h>

SC_MODULE(Counter){
    // Declare ports
    sc_in<sc_uint<1>  > inc;
	sc_in<sc_uint<1>  > load;
	sc_in_clk           clock;
	sc_in<sc_uint<8>  > in;
	sc_out<sc_uint<8> > out;

	sc_uint<8>          out_temp;

    // Main process
	void process(){
		if (clock.read()==1) { //rising edge
			if (load.read() == 1){
				out_temp = in;
			}
			else if (inc.read() == 1){ //load==1 and inc==1 is not excepted...?
				out_temp += 1;
			}
			//else { do nothing }
			out = out_temp;
		}
	}

	SC_CTOR(Counter){
		SC_METHOD(process);
		sensitive << clock;
	}

    // Unit testbench
	static int testbench(const char * trace_file){
	    std::cout << "Testing counter...\n";
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);
	    
	    //Signals used in testbench
		sc_clock clock("clk", sc_time(20,SC_NS), 0.5, sc_time(10,SC_NS), false);
		sc_signal<sc_uint<1> > inc;
		sc_signal<sc_uint<1> > load;
		sc_signal<sc_uint<8> > in;
		sc_signal<sc_uint<8> > out;

        //Include signals to trace
	  	sc_trace(tf, inc, "inc");
		sc_trace(tf, load, "load");
		sc_trace(tf, clock, "clock");
		sc_trace(tf, in, "in");
		sc_trace(tf, out, "out");

        //Component initialization
		Counter counter("Counter");

		// Signal Connections
		counter.inc(inc);
		counter.load(load);
		counter.clock(clock);
		counter.in(in);
		counter.out(out);

		// Generate Stimuli
		inc = 0;
		load = 1;
		in = 10;
		sc_start(30, SC_NS);
		assert(counter.out.read() == 10);

		inc = 1;
		load = 0;
		sc_start(20, SC_NS);
		assert(counter.out.read() == 11);

		inc = 1;
		load = 0;
		in = 128;
		sc_start(20, SC_NS);
		assert(counter.out.read() == 12);

		inc = 0;
		load = 1;
		in = 128;
		sc_start(20, SC_NS);
		assert(counter.out.read() == 128);

		in = 0;
		sc_start(1, SC_NS);
		assert(counter.out.read() == 128); //load should be synchronous

		sc_start(20, SC_NS);
		
        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}
};

#endif /* COUNTER_H_ */

