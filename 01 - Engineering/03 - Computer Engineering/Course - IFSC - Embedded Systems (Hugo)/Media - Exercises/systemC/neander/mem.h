#ifndef MEM_H_
#define MEM_H_

#include <systemc.h>
#include <iomanip>

SC_MODULE(Mem){
    // Declare ports
	sc_in<sc_uint<1>  > wr;
	sc_in<sc_uint<1>  > rd;
	sc_in_clk           clock;
	sc_in<sc_uint<8>  > addr_in;
	sc_in<sc_uint<8>  > data_in;
	sc_out<sc_uint<8> > data_out;

	sc_uint<8>			memory[256];

    // Main process
	void process(){
		if (clock.read()==1){ //rising edge
			//data_out = 0; //by default, holds the last value or clear?
			if (wr.read()){
				//std::cout << "Writting n=" << addr_in.read() << '\n';
				memory[addr_in.read()] = data_in.read();
			} else if (rd.read()){
				//std::cout << "Reading n=" << addr_in.read() << '\n';
				data_out = memory[addr_in.read()];
			} else {
				//do nothing?
			}
		}
	}

	SC_CTOR(Mem){
		SC_METHOD(process);
		sensitive << clock;
	}

	//function that print the variable memory	
	
	
	//For trace/tests purpouse. Prints memory contents no stdout
	void set_mem(sc_uint<8>* list_program, unsigned int len_program, sc_uint<8>* list_data, unsigned int len_data){
		if (len_program>128) std::invalid_argument("Program section is too long");
		if (len_data>128)    std::invalid_argument("Data section is too long");

		for (int i=0; i<len_program; i++){
			memory[i] = list_program[i];
		}

		for (int i=0; i<len_data; i++){
			memory[i+128] = list_data[i];
		}
	}

	void dump_mem(unsigned int start = 0, unsigned int end = 256){
		if (end>256)   std::invalid_argument("End is too large");
		if (start>end) std::invalid_argument("Start is after end");

		for (int i=start; i<end; i++){
			std::cout << i << ") " << memory[i] << "\n";
		}
	}
	
	sc_uint<8> get_mem(unsigned int position){
		return memory[position];
	}

    // Unit testbench
	static int testbench(const char * trace_file){
		std::cout << "Testing memory...\n";
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);
	    
	    //Signals used in testbench
		sc_clock clock("clk", sc_time(20,SC_NS), 0.5, sc_time(10,SC_NS), false);
		sc_signal<sc_uint<1> > wr;
		sc_signal<sc_uint<1> > rd;
		sc_signal<sc_uint<8> > addr_in;
		sc_signal<sc_uint<8> > data_in;
		sc_signal<sc_uint<8> > data_out;

        //Include signals to trace
		sc_trace(tf, clock, "clock");
		sc_trace(tf, wr, "wr");
		sc_trace(tf, rd, "rd");
		sc_trace(tf, addr_in, "addr_in");
		sc_trace(tf, data_in, "data_in");
		sc_trace(tf, data_out, "data_out");
	  	
        //Component initialization
		Mem mem("Mem");

		// Signal Connections
		mem.clock(clock);
		mem.wr(wr);
		mem.rd(rd);
		mem.addr_in(addr_in);
		mem.data_in(data_in);
		mem.data_out(data_out);

		// Generate Stimuli
		wr = 1;
		rd = 0;
		addr_in = 0;
		data_in = 128;
		sc_start(30, SC_NS);

		wr = 0;
		rd = 1;
		sc_start(20, SC_NS);
		assert(mem.data_out.read() == 128); //stored value was there

		wr = 1;
		rd = 0;
		addr_in = 10;
		data_in = 255;
		sc_start(20, SC_NS);

		wr = 0;
		rd = 1;
		sc_start(20, SC_NS);
		assert(mem.data_out.read() == 255);
		
		wr = 0;
		rd = 1;
		addr_in = 0;
		sc_start(20, SC_NS);
		assert(mem.data_out.read() == 128); //stored value was still there

        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}
};

#endif /* MEM_H_ */
