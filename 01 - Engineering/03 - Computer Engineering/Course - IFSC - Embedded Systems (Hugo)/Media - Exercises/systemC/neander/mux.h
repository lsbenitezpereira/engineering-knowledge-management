
#ifndef MUX_H_
#define MUX_H_

#include <systemc.h>
#include <assert.h>

SC_MODULE(Mux){
    // Declare ports
    sc_in<sc_uint<1>  > sel;
	sc_in<sc_uint<8>  > a;
	sc_in<sc_uint<8>  > b;
	sc_out<sc_uint<8> > out;


    // Main process
	void process(){
		if (sel.read() == 0){
			out = a;
		} else {
			out = b;
		}
	}

	SC_CTOR(Mux){
		SC_METHOD(process);
		sensitive << sel;
		sensitive << a;
		sensitive << b;
	}

    // Unit testbench
	static int testbench(const char * trace_file){
	    std::cout << "Testing mux...\n";
		sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);

	    //Signals used in testbench
		sc_signal<sc_uint<1> > sel;
		sc_signal<sc_uint<8> > a;
		sc_signal<sc_uint<8> > b;
		sc_signal<sc_uint<8> > out;

        //Include signals to trace
		sc_trace(tf, sel, "Sel");
		sc_trace(tf, a, "Entrada_A");
		sc_trace(tf, b, "Entrada_B");
		sc_trace(tf, out, "Out");	

        //Component initialization
		Mux mux("Mux");

		// Signal Connections
		mux.sel(sel);
		mux.a(a);
		mux.b(b);
		mux.out(out);

		// Generate Stimuli
		sel = 0;
		a = 128;
		b = 200;
		sc_start(4, SC_NS);
		assert(mux.out.read() == 128);

		sel = 1;
		sc_start(4, SC_NS);
		assert(mux.out.read() == 200);

		b = 255;
		sc_start(4, SC_NS);
		assert(mux.out.read() == 255);

        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}
};

#endif /* MUX_H_ */
