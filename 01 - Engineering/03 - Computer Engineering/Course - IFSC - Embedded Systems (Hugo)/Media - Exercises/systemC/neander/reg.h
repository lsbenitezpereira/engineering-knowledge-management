
#ifndef REG_H_
#define REG_H_

#include <systemc.h>

SC_MODULE(Reg){
    // Declare ports
	sc_in<sc_uint<1>  > load;
	sc_in<sc_uint<8>  > in;
	sc_out<sc_uint<8> > out;

    // Main process
	void process(){
		if (load.read() == 1){
			out = in;
		}
	}

	SC_CTOR(Reg){
		SC_METHOD(process);
		sensitive << load;
	}

    // Unit testbench
	static int testbench(const char * trace_file){
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);
	    
	    //Signals used in testbench
		sc_signal<sc_uint<1> > load;
		sc_signal<sc_uint<8> > in;
		sc_signal<sc_uint<8> > out;

        //Include signals to trace
		sc_trace(tf, load, "load");
		sc_trace(tf, in,   "in");
		sc_trace(tf, out,  "out");

        //Component initialization
		Reg reg("Reg");

		// Signal Connections
		reg.load(load);
		reg.in(in);
		reg.out(out);
		
		// Generate Stimuli
		load = 1;
		in = 0;
		sc_start(1, SC_NS);
		assert(reg.out.read() == 0);

		load = 0;
		in = 128;
		sc_start(1, SC_NS);
		assert(reg.out.read() == 0);

		load = 1;
		in = 128;
		sc_start(1, SC_NS);
		assert(reg.out.read() == 128);
		
        // Close trace file
	  	sc_close_vcd_trace_file(tf);

	  	return 0;
	}
};

#endif /* REG_H_ */
