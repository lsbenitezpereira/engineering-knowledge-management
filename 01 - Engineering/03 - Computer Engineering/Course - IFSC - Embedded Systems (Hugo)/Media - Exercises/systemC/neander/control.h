
#ifndef CONTROL_H_
#define CONTROL_H_

#include <systemc.h>
#include <stdexcept>
#include "ISA.h"

enum State {
	FETCH_INST,
	DECODE_MEM_READ_INST,
	DECODE_MEM_READ_DATA,
	DECODE_LOAD_OP_DATA,
	DECODE_DECODE_INST,
	DECODE_LOAD_OP_ADDR,
	EXECUTE_MEM_WRITE,
	EXECUTE_JMP_EXEC,
	EXECUTE_ALU_AC_WRITE,
	EXECUTE_ALU_EXEC,
	HLT
};
inline const char* ToString(State v){
    switch (v){
        case FETCH_INST:		    return "FETCH_INST";
        case DECODE_MEM_READ_INST:	return "DECODE_MEM_READ_INST";
        case DECODE_MEM_READ_DATA:	return "DECODE_MEM_READ_DATA";
        case DECODE_LOAD_OP_DATA:	return "DECODE_LOAD_OP_DATA";
        case DECODE_DECODE_INST:	return "DECODE_DECODE_INST";
        case DECODE_LOAD_OP_ADDR:	return "DECODE_LOAD_OP_ADDR";
        case EXECUTE_MEM_WRITE:		return "EXECUTE_MEM_WRITE";
        case EXECUTE_JMP_EXEC:		return "EXECUTE_JMP_EXEC";
        case EXECUTE_ALU_AC_WRITE:	return "EXECUTE_ALU_AC_WRITE";
        case EXECUTE_ALU_EXEC:		return "EXECUTE_ALU_EXEC";
        case HLT:   				return "HLT";
        default:                    return "[Unknown]";
    }
}



SC_MODULE(Control){
    // Declare ports
	sc_in<sc_uint<8>  > instruction_decode;
	sc_in<sc_uint<1>  > ula_flag_n;
	sc_in<sc_uint<1>  > ula_flag_z;
	sc_in_clk           clock;

	sc_out<sc_uint<3> > ula_op;
	sc_out<sc_uint<1> > ac_load;
	sc_out<sc_uint<1> > pc_inc;
	sc_out<sc_uint<1> > pc_load;
	sc_out<sc_uint<1> > mem_addr_sel;
	sc_out<sc_uint<1> > mem_wr;
	sc_out<sc_uint<1> > mem_rd;

	// State declaration
	enum State state = FETCH_INST;

    // Main process
	void process(){
		if (clock.read()==1){ //rising edge
			_cycles += 1;
			// lógica de transição de estados
			
			switch (state){
				case FETCH_INST:
					mem_rd = 1; 
					pc_inc = 1;
					ac_load = 0;
					mem_addr_sel = 0;
					mem_wr = 0;				
					pc_load = 0;
					state = DECODE_MEM_READ_INST;
					_inst_count += 1;
					break;
				case DECODE_MEM_READ_INST:
					mem_rd = 0;
					pc_inc = 0;
					
					state = DECODE_DECODE_INST;
					break;
				case DECODE_MEM_READ_DATA:
					mem_addr_sel = 0;
					mem_rd = 0;
					state = EXECUTE_ALU_EXEC;
					break;
				case DECODE_LOAD_OP_DATA:
					mem_addr_sel = 1;
					mem_rd = 1;
					pc_inc = 0;
					state = DECODE_MEM_READ_DATA;
					break;
				case DECODE_DECODE_INST:
					instruction = instruction_decode.read();
					if (instruction==ISA_HLT){
						state = HLT;
					} else if (instruction==ISA_NOP){
						state = FETCH_INST;
					} else if (instruction==ISA_NOT){
						state = EXECUTE_ALU_EXEC;
					} else {
						state = DECODE_LOAD_OP_ADDR;
					}
					break;
				case DECODE_LOAD_OP_ADDR:
					mem_rd = 1;
					pc_inc = 1;
					if (instruction==ISA_STA){
						state = EXECUTE_MEM_WRITE;
					} else if (instruction>=ISA_JMP){
						state = EXECUTE_JMP_EXEC;
					} else {
						state = DECODE_LOAD_OP_DATA;
					}
					break;
				case EXECUTE_MEM_WRITE:
					mem_addr_sel = 1;
					mem_wr = 1;
					pc_inc = 0;
						
					state = FETCH_INST;
					break;
				case EXECUTE_JMP_EXEC:
					pc_inc = 0;
					if (instruction==ISA_JMP){
						pc_load = 1;
					} else if (instruction==ISA_JN && flag_n==1) {
						pc_load = 1;
					} else if (instruction==ISA_JZ && flag_z==1) {
						pc_load = 1;
					}
					//pc_inc = 0; ????
					//state = FETCH_INST;
					//mem_rd = 1; 
					//ac_load = 0;
					//mem_addr_sel = 0;
					//mem_wr = 0;				
					state = FETCH_INST;
					//_inst_count += 1;
					break;
				case EXECUTE_ALU_AC_WRITE:
					ac_load = 1;
					flag_n = ula_flag_n.read();
					flag_z = ula_flag_z.read();
					state = FETCH_INST;
					break;
				case EXECUTE_ALU_EXEC:
					if (instruction==ISA_ADD){
						//std::cout << "Ula, please exec an ISA_ADD\n";
						ula_op = 1;
					} else if (instruction==ISA_AND){
						ula_op = 2;
					} else if (instruction==ISA_OR){
						ula_op = 3;
					} else if (instruction==ISA_NOT){
						ula_op = 4;
					} else if (instruction==ISA_LDA){
						ula_op = 5;
					}
					state = EXECUTE_ALU_AC_WRITE;
					pc_inc = 0;
					break;
				case HLT:
					break;
				default:
					throw std::logic_error("Invalid state");

			}
		}
	}

	SC_CTOR(Control){
		SC_METHOD(process);
		sensitive << clock;
	}
	
	unsigned int cycles() { return _cycles; }
	unsigned int instruction_count() { return _inst_count; }

    // Unit testbench
	static int testbench(const char * trace_file){
		std::cout << "Testing control...\n";
	    sc_trace_file *tf = sc_create_vcd_trace_file(trace_file);

	    //Signals used in testbench
		sc_clock clock("clk", sc_time(20,SC_NS), 0.5, sc_time(10,SC_NS), false);
		sc_signal<sc_uint<8> > instruction_decode;
		sc_signal<sc_uint<1> > ula_flag_n;
		sc_signal<sc_uint<1> > ula_flag_z;

		sc_signal<sc_uint<3> > ula_op;
		sc_signal<sc_uint<1> > ac_load;
		sc_signal<sc_uint<1> > pc_inc;
		sc_signal<sc_uint<1> > pc_load;

		sc_signal<sc_uint<1> > mem_addr_sel;
		sc_signal<sc_uint<1> > mem_wr;
		sc_signal<sc_uint<1> > mem_rd;

        //Include signals to trace
		sc_trace(tf, instruction_decode, "instruction_decode");
		sc_trace(tf, ula_flag_n, "ula_flag_n");
		sc_trace(tf, ula_flag_z, "ula_flag_z");
		sc_trace(tf, ula_op, "ula_op");
		sc_trace(tf, ac_load, "ac_load");
		sc_trace(tf, pc_inc, "pc_inc");
		sc_trace(tf, pc_load, "pc_load");
		sc_trace(tf, clock, "clock");
		sc_trace(tf, mem_addr_sel, "mem_addr_sel");
		sc_trace(tf, mem_wr, "mem_wr");
		sc_trace(tf, mem_rd, "mem_rd");

        //Component initialization
		Control control("Control");

		// Signal Connections
		control.instruction_decode(instruction_decode);
		control.ula_flag_n(ula_flag_n);
		control.ula_flag_z(ula_flag_z);
		control.ula_op(ula_op);
		control.ac_load(ac_load);
		control.pc_inc(pc_inc);
		control.pc_load(pc_load);
		control.clock(clock);
		control.mem_addr_sel(mem_addr_sel);
		control.mem_wr(mem_wr);
		control.mem_rd(mem_rd);


		// Generate Stimuli - NOT instruction
		assert(control.state == FETCH_INST);
		
		sc_start(10, SC_NS);
		assert(control.state == DECODE_MEM_READ_INST);
		instruction_decode = ISA_NOT;

		sc_start(20, SC_NS);
		assert(control.state == DECODE_DECODE_INST);

		sc_start(20, SC_NS);
		assert(control.state == EXECUTE_ALU_EXEC);

		sc_start(20, SC_NS);
		assert(control.state == EXECUTE_ALU_AC_WRITE);

		sc_start(20, SC_NS);
		assert(control.state == FETCH_INST);


		// Generate Stimuli - AND instruction
		assert(control.state == FETCH_INST);
		
		sc_start(20, SC_NS);
		assert(control.state == DECODE_MEM_READ_INST);
		instruction_decode = ISA_AND;

		sc_start(20, SC_NS);
		assert(control.state == DECODE_DECODE_INST);

		sc_start(20, SC_NS);
		assert(control.state == DECODE_LOAD_OP_ADDR);

		sc_start(20, SC_NS);
		assert(control.state == DECODE_LOAD_OP_DATA);

		sc_start(20, SC_NS);
		assert(control.state == DECODE_MEM_READ_DATA);

		sc_start(20, SC_NS);
		assert(control.state == EXECUTE_ALU_EXEC);

		sc_start(20, SC_NS);
		assert(control.state == EXECUTE_ALU_AC_WRITE);

		sc_start(20, SC_NS);
		assert(control.state == FETCH_INST);


		// Generate Stimuli - HLT instruction
		assert(control.state == FETCH_INST);
		
		sc_start(20, SC_NS);
		assert(control.state == DECODE_MEM_READ_INST);
		instruction_decode = ISA_HLT;

		sc_start(20, SC_NS);
		assert(control.state == DECODE_DECODE_INST);

		sc_start(20, SC_NS);
		assert(control.state == HLT);
		
		sc_start(200, SC_NS);
		assert(control.state == HLT);

	
        // Close trace file
	  	sc_close_vcd_trace_file(tf);
	  	return 0;
	}

private:

	// Internal Signals
	sc_uint<8> instruction;
	unsigned int			_cycles = 0;
	unsigned int			_inst_count = 0;

	sc_uint<1> flag_n;
	sc_uint<1> flag_z;
};

#endif /* CONTROL_H_ */
