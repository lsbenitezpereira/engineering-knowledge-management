enum ISA {
    ISA_NOP = 0,
	ISA_NOT = 64,
	ISA_HLT = 112,
	ISA_STA = 128,
	ISA_ADD = 144,
	ISA_AND = 160,
	ISA_OR  = 176,
	ISA_LDA = 208,
	ISA_JMP = 224,
	ISA_JN  = 240,
	ISA_JZ  = 248
};
inline const char* ToStringISA(uint v){
    switch (v){
        case 0: 	return "ISA_NOP";
        case 64: 	return "ISA_NOT";
        case 112: 	return "ISA_HLT";
        case 128: 	return "ISA_STA";
        case 144: 	return "ISA_ADD";
        case 160: 	return "ISA_AND";
        case 176: 	return "ISA_OR ";
        case 208: 	return "ISA_LDA";
        case 224: 	return "ISA_JMP";
        case 240: 	return "ISA_JN ";
        case 248: 	return "ISA_JZ ";
        default:    return "[Unknown]";
    }
}