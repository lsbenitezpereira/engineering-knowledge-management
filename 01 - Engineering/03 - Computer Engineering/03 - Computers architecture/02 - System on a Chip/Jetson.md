[TOC]

# Jetson

* embedded computing boards with GPU
* architecture arm64
* [Playlist com vários projetos](https://www.youtube.com/playlist?list=PLDNdJT7M7NNePU62eH2Ly9ROXMea0tNOU)
* Desined for Machine Learning
* They all have the same software stack
* runs linux
* “low” comsumption
* tey have simple board and also deselopment kits
* About docker images and configurations for jetson, see [this repo]( https://github.com/helmut-hoffer-von-ankershoffen/jetson) (specifically [this image](https://github.com/helmut-hoffer-von-ankershoffen/jetson/blob/master/workflow/deploy/ml-base/src/Dockerfile))
* **Models**
  * Nano
    * cheap, 129 USD	
    * 128-Core Maxwell GPU, 4-Core 64-bit ARM CPU, 4GB LPDDR4, 16GB eMMC
    * 472 GFLOPS
    * 4W
* **Boot and startup**
  * gravar imagem
  * acho que vem com ubuntu ou ubuntu-based
  * vem com várias demos
  * A primeira atualização é bem lenta
* **Certification**
  * quite simple
  * https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-nano/
  * You submit a project; Within approximately 10-14 days, you will receive your project scores, and if approved, your appropriate Jetson AI certificate will be delivered.
* **headless**
  * tutorial: https://www.jetsonhacks.com/2019/08/21/jetson-nano-headless-setup/
  * the first tutorial is via usb, then you can ssh after that?
* **Power modes**
  * Mode 1: low power (5W)
  * Mode 0: full strength (10W)
  * `sudo nvpmodel -m <mode>`
  * To check what power mode the device is in: `sudo nvpmodel -q`

## Hardware resources and extensions

* Wifi
  * does not have builtin
  * a usb adapter is good, like [this one](https://www.sparkfun.com/products/15449); [tutorial](https://learn.sparkfun.com/tutorials/adding-wifi-to-the-nvidia-jetson/all)
  * or a boardadapter, like [this one](https://www.amazon.com/Wireless-AC8265-Wireless-Developer-Support-Bluetooth/dp/B07V9B5C6M/ref=sr_1_7_sspa?dchild=1&keywords=jetson+nano+kit&qid=1626444002&sr=8-7-spons&psc=1&smid=A3B0XDFTVR980O&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExT05FUlpJRjMwRTJYJmVuY3J5cHRlZElkPUEwMTE4MjI4MU5FMzNEMjVLRkU5MSZlbmNyeXB0ZWRBZElkPUExMDM4NDE4MkZQM09YOTJIQ0hYMiZ3aWRnZXROYW1lPXNwX210ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=); [tutorial](https://www.jetsonhacks.com/2019/04/08/jetson-nano-intel-wifi-and-bluetooth/)
* Camera
  * there are two CSI camera slots
  * basic gstreamer testing: `gst-launch-1.0 nvarguscamerasrc ! nvoverlaysink`
  * You can use raspberry camera; tutorial: https://www.jetsonhacks.com/2019/04/02/jetson-nano-raspberry-pi-camera/
  * tutorial openCV + gstreamer: https://www.okdo.com/getting-started/get-started-with-jetson-nano-4gb-and-csi-camera/#h-7-safe-shutdown-toc

## Others

* **Connecting to azure IoT**

  * tutorial device setup: https://dev.to/rohansawant/how-to-deploy-a-module-to-azure-iot-edge-runtime-on-linux-minified-57fp

    setup vscode for development: https://docs.microsoft.com/en-us/azure/iot-edge/how-to-vs-code-develop-module?view=iotedge-2020-11

    setup vscode for development, twicks that should be made for arm64: https://devblogs.microsoft.com/iotdev/develop-and-debug-arm64-iot-edge-modules-in-visual-studio-code-preview/
