[TOC]

# Conceptualization

* integrates all or most components of a computer
* are in contrast to the common traditional motherboard-based PC architecture
* **Things that are usually put togueder**
  * Networking
  * GPU
  * Periferals (just like a microcontroller)
  * etc
* **Adaptive Compute Acceleration Platform**
  * Foucs very much on heterogenous computing systems on the same chip
  * example: AMD versa


## Main niches

* **Embedded systems**
  * Raspberry, etc
* **Mobile phones**
  * Exynos, used mainly by Samsung's Galaxy series of smartphones
* **video game consoles**
  * Nvidia Tegra X1 used in the Nintendo Switch
  * AMD Flute, in Xbox and PlayStation
* **Personal Notebooks**
  * Qualcomm Snapdragon
  * ARM250



# Intel boards

## Edison

* From Intel

##  Neural Compute Stick (NCS)

* From Intel
* connects by ==usb== to a main processor
* auxiliaria processor, optimized for NN
* Myriad X VPU
* supported by the OpenVINO
* NCS 2
  * 16 cores
  * ~USD 99 (2020)

## Galileo

* Based on x86 architecture (32bits)

* Processor Quark SoC X1000

* Arduino-certified (just supported by the Arduino platform)

* Tempo de boot: 45s (para iniciar a Sketch e estar acessível via ssh. A conexão usb inicia um pouco antes)

* pinout: http://www.malinov.com/Home/sergey-s-blog/intelgalileo-programminggpiofromlinux

* Galileo via arduino (corrigindo configuraçõs da intel):https://github.com/majenkotech/IntelFixes/blob/master/fixgalileo.sh

* required fix:

  ```bash
  #!/bin/bash
  
  if [ "$ARCH" = "x86_64" ]; then
      FILE=galileo-toolchain-20150120-linux64.tar.bz2
  else
      FILE=galileo-toolchain-20150120-linux32.tar.bz2
  fi
  
  echo "Arch: $ARCH"
  
  URL=https://downloadmirror.intel.com/24619/eng/${FILE}
  DEST=${HOME}/.arduino15/packages/Intel/tools/i586-poky-linux-uclibc/1.6.2+1.0/
  
  wget -c ${URL}
  
  rm -rf "${DEST}/i586"
  mkdir -p "${DEST}"
  tar -C "${DEST}" -jxf ${FILE}
  cd "${DEST}/i586"
  sed -i 's/-perm +111/-perm \/111/g' install_script.sh
  ./install_script.sh
  
  ```

  





# Others

* Lattice
  * produce chips that are basically a processor + AI accelerator
  * application specific (face recognition, etc)
  * just inference
  * the model is fixed and can't be updated?
  * produce "domain-specific accelerator"













