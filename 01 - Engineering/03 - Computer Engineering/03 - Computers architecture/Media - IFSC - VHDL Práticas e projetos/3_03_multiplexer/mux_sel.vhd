-------------------------------------------------------
--! @file    mux_sel.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-01
--! @version 0.1
--! @brief   Multiplexer circuit
--!          Implemented with `sel` mecanism
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
	    
entity mux_sel is
    generic(
        N : integer := 5 --! number of bits
    );
    port (
        mux_in_a: IN std_logic_vector(N-1 downto 0);
        mux_in_b: IN std_logic_vector(N-1 downto 0);
        mux_sel: IN std_logic;
        mux_out: OUT std_logic_vector(N-1 downto 0)
    );
end entity mux_sel;

architecture rtl of mux_sel is
begin
    with mux_sel select
    mux_out <= mux_in_a when '0',
               mux_in_b when others;
end architecture rtl;