LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    constant N: integer := 4;

    -- Declaração dos componentes
    component mux_sel is
        generic(
            N: integer := 5
        );
        port (
            mux_in_a: IN std_logic_vector(N-1 downto 0);
            mux_in_b: IN std_logic_vector(N-1 downto 0);
            mux_sel: IN std_logic;
            mux_out: OUT std_logic_vector(N-1 downto 0)
        );
    end component mux_sel;

    component mux_when is
        generic(
            N: integer := 5
        );
        port (
            mux_in_a: IN std_logic_vector(N-1 downto 0);
            mux_in_b: IN std_logic_vector(N-1 downto 0);
            mux_sel: IN std_logic;
            mux_out: OUT std_logic_vector(N-1 downto 0)
        );
    end component mux_when;

    -- Declaração de sinais
    signal a_0: std_logic_vector(N-1 downto 0);
    signal b_0: std_logic_vector(N-1 downto 0);
    signal sel_0: std_logic;
    signal out_0: std_logic_vector(N-1 downto 0);

    signal a_1: std_logic_vector(N-1 downto 0);
    signal b_1: std_logic_vector(N-1 downto 0);
    signal sel_1: std_logic;
    signal out_1: std_logic_vector(N-1 downto 0);

BEGIN  -- inicio do corpo da arquitetura
    process
    begin
        --test mux_sel
        a_0 <= std_logic_vector(To_unsigned(3, N));
        b_0 <= std_logic_vector(To_unsigned(5 , N));
        sel_0 <= '0';

        --test mux_when
        a_1 <= std_logic_vector(To_unsigned(3, N));
        b_1 <= std_logic_vector(To_unsigned(5, N));
        sel_1 <= '0';

        ------------------------------
        wait for 250ns;

        --test mux_sel
        a_0 <= std_logic_vector(To_unsigned(1, N));
        b_0 <= std_logic_vector(To_unsigned(2 , N));
        sel_0 <= '1';

        --test mux_when
        a_1 <= std_logic_vector(To_unsigned(1, N));
        b_1 <= std_logic_vector(To_unsigned(2, N));
        sel_1 <= '1';

        wait;
    end process;

    dut0: mux_sel 
        generic map (
            N => N
        ) 
        port map (
            mux_in_a => a_0,
            mux_in_b => b_0,
            mux_sel => sel_0,
            mux_out => out_0
    );

    dut1: mux_when 
        generic map (
            N => N
        ) 
        port map (
            mux_in_a => a_1,
            mux_in_b => b_1,
            mux_sel => sel_1,
            mux_out => out_1
    );

END ARCHITECTURE stimulus;
