-------------------------------------------------------
--! @file    mux_when.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-01
--! @version 0.1
--! @brief   Multiplexer circuit
--!          Implemented with `when...else` mecanism
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
	    
entity mux_when is
    generic(
        N : integer := 5 --! number of bits
    );
    port (
        mux_in_a: IN std_logic_vector(N-1 downto 0);
        mux_in_b: IN std_logic_vector(N-1 downto 0);
        mux_sel: IN std_logic;
        mux_out: OUT std_logic_vector(N-1 downto 0)
    );
end entity mux_when;

architecture rtl of mux_when is
begin
    mux_out <= mux_in_a when mux_sel='0' else
               mux_in_b;
end architecture rtl;