-------------------------------------------------------------------
-- name        : my_package.vhd
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : Example with package creatation, functions and proceadures
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package my_package is
	-- Declaração do procedimento
	procedure zero_one_match(
        signal vec:   in std_logic_vector(7 downto 0);
		signal equal: out std_logic
    );
	
	-- Declaração da função
    function bcd_to_7seg (x: std_logic_vector) return std_logic_vector;
    --function "+" (x, y : std_logic_vector) return std_logic_vector;
end package my_package;

package body my_package is
	-- Corpo do procedimento
	procedure zero_one_match(
        signal vec:   in std_logic_vector(7 downto 0);
		signal equal: out std_logic
    ) is
        variable counter: integer;
	begin
        counter := 0;
        for i in vec'range loop
            if vec(i)='1' then
                counter := counter + 1;
            else
                counter := counter - 1;
            end if;
        end loop;

        if counter=0 then
            equal <= '1';
        else
            equal <= '0';
        end if;
        
	end;

	-- Corpo da função
	function bcd_to_7seg (x: std_logic_vector) return std_logic_vector is		
		variable segs : std_logic_vector(7 downto 0);
	begin
        case x(3 downto 0) is				
            --    ABCD		        .gfedcba
            when "0000" => segs := "11000000";	-- 0
            when "0001" => segs := "11111001";	-- 1
            when "0010" => segs := "10100100";	-- 2
            when "0011" => segs := "10110000";	-- 3
            when "0100" => segs := "10011001";	-- 4
            when "0101" => segs := "10010010";	-- 5
            when "0110" => segs := "10000010";	-- 6
            when "0111" => segs := "11111000";	-- 7
            when "1000" => segs := "10000000";	-- 8
            when "1001" => segs := "10010000";	-- 9
            when "1010" => segs := "10001000";	-- A
            when "1011" => segs := "10000011";	-- B
            when "1100" => segs := "10100111";	-- C
            when "1101" => segs := "10100001";	-- D
            when "1110" => segs := "10000110";	-- E
            when others => segs := "10001110";	-- F
        end case;
		return segs;
	end bcd_to_7seg;

end package body my_package;