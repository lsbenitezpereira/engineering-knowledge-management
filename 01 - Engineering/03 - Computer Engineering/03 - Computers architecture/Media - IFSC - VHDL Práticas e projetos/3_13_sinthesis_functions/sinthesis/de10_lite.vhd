-------------------------------------------------------------------
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : Example with package creatation, functions and proceadures
-------------------------------------------------------------------

LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use work.my_package.all;

entity de10_lite is 
	port (
		---------- CLOCK ----------
		ADC_CLK_10:	in std_logic;
		MAX10_CLK1_50: in std_logic;
		MAX10_CLK2_50: in std_logic;
		
		----------- SDRAM ------------
		DRAM_ADDR: out std_logic_vector (12 downto 0);
		DRAM_BA: out std_logic_vector (1 downto 0);
		DRAM_CAS_N: out std_logic;
		DRAM_CKE: out std_logic;
		DRAM_CLK: out std_logic;
		DRAM_CS_N: out std_logic;		
		DRAM_DQ: inout std_logic_vector(15 downto 0);
		DRAM_LDQM: out std_logic;
		DRAM_RAS_N: out std_logic;
		DRAM_UDQM: out std_logic;
		DRAM_WE_N: out std_logic;
		
		----------- SEG7 ------------
		HEX0: out std_logic_vector(7 downto 0);
		HEX1: out std_logic_vector(7 downto 0);
		HEX2: out std_logic_vector(7 downto 0);
		HEX3: out std_logic_vector(7 downto 0);
		HEX4: out std_logic_vector(7 downto 0);
		HEX5: out std_logic_vector(7 downto 0);

		----------- KEY ------------
		KEY: in std_logic_vector(1 downto 0);

		----------- LED ------------
		LEDR: out std_logic_vector(9 downto 0);

		----------- SW ------------
		SW: in std_logic_vector(9 downto 0);

		----------- VGA ------------
		VGA_B: out std_logic_vector(3 downto 0);
		VGA_G: out std_logic_vector(3 downto 0);
		VGA_HS: out std_logic;
		VGA_R: out std_logic_vector(3 downto 0);
		VGA_VS: out std_logic;
	
		----------- Accelerometer ------------
		GSENSOR_CS_N: out std_logic;
		GSENSOR_INT: in std_logic_vector(2 downto 1);
		GSENSOR_SCLK: out std_logic;
		GSENSOR_SDI: inout std_logic;
		GSENSOR_SDO: inout std_logic;
	
		----------- Arduino ------------
		ARDUINO_IO: inout std_logic_vector(15 downto 0);
		ARDUINO_RESET_N: inout std_logic
	);
end entity;


architecture rtl of de10_lite is
	signal vec :   std_logic_vector(7 downto 0);
	signal equal : std_logic;

    signal a : std_logic_vector(3 downto 0);
    signal b : std_logic_vector(3 downto 0);
    signal c : std_logic_vector(3 downto 0);
    signal d : std_logic_vector(3 downto 0);
    signal segs_1 : std_logic_vector(7 downto 0);
    signal segs_2 : std_logic_vector(7 downto 0);
    signal segs_3 : std_logic_vector(7 downto 0);
    signal segs_4 : std_logic_vector(7 downto 0);
begin
	-- Use function
	segs_1 <= bcd_to_7seg(a);
    segs_2 <= bcd_to_7seg(b);
    segs_3 <= bcd_to_7seg(c);
    segs_4 <= bcd_to_7seg(d);

    -- Use proceadure
	zero_one_match(vec, equal);

	-- Hardware mapping
	a <= SW(9 downto 8) & "00";
	b <= SW(9 downto 8) & "01";
	c <= SW(9 downto 8) & "10";
	d <= SW(9 downto 8) & "11";
	HEX0 <= segs_1;
	HEX1 <= segs_2;
	HEX2 <= segs_3;
	HEX3 <= segs_4;

	vec <= SW(7 downto 0);
	LEDR(0) <= equal;
end;

