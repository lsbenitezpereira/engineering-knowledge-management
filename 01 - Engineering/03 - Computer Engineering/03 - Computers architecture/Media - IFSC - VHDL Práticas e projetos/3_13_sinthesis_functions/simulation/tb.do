#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom ../source/my_package.vhd
vcom testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -height 15 -divider "Stimulus"
add wave -label vec /vec
add wave -height 15 -divider "Outpus"
add wave -label equal /equal


#Simula até um 500ns
run 100ns

wave zoomfull
write wave wave.ps