-------------------------------------------------------------------
-- name        : testbench.vhd
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : Example with package creatation, functions and proceadures
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Utilização das funções e procedimentos de my_package
use work.my_package.all;

entity testbench is

end entity testbench;

architecture RTL of testbench is
	signal vec :   std_logic_vector(7 downto 0);
	signal equal : std_logic;

    signal a : std_logic_vector(3 downto 0);
    signal b : std_logic_vector(3 downto 0);
    signal c : std_logic_vector(3 downto 0);
    signal d : std_logic_vector(3 downto 0);
    signal segs_1 : std_logic_vector(7 downto 0);
    signal segs_2 : std_logic_vector(7 downto 0);
    signal segs_3 : std_logic_vector(7 downto 0);
    signal segs_4 : std_logic_vector(7 downto 0);
begin
    -- Test function
    segs_1 <= bcd_to_7seg(a);
    segs_2 <= bcd_to_7seg(b);
    segs_3 <= bcd_to_7seg(c);
    segs_4 <= bcd_to_7seg(d);
    process
    begin
        a <= "0000";
        b <= "0001";
        c <= "0010";
        d <= "0011";
        wait for 1 ns;
        assert segs_1 = "11000000" severity failure;
        assert segs_2 = "11111001" severity failure;
        assert segs_3 = "10100100" severity failure;
        assert segs_4 = "10110000" severity failure;

        a <= "0001";
        b <= "0010";
        c <= "0011";
        d <= "0100";
        wait for 1 ns;
        assert segs_1 = "11111001" severity failure;
        assert segs_2 = "10100100" severity failure;
        assert segs_3 = "10110000" severity failure;
        assert segs_4 = "10011001" severity failure;

        wait;
    end process;

    -- Test proceadure
	zero_one_match(vec, equal);
    process
    begin
        vec <= "00001111";
        wait for 1 ns;
        assert equal = '1' severity failure;

        vec <= "00001110";
        wait for 1 ns;
        assert equal = '0' severity failure;

        vec <= "00000000";
        wait for 1 ns;
        assert equal = '0' severity failure;

        vec <= "11111111";
        wait for 1 ns;
        assert equal = '0' severity failure;

        vec <= "11001100";
        wait for 1 ns;
        assert equal = '1' severity failure;

        wait;
    end process;
end architecture RTL;