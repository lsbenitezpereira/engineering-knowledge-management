-------------------------------------------------------------------
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : FSM to control an alarm system
-------------------------------------------------------------------
LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;

entity de10_lite is 
	port (
		---------- CLOCK ----------
		ADC_CLK_10:	in std_logic;
		MAX10_CLK1_50: in std_logic;
		MAX10_CLK2_50: in std_logic;
		
		----------- SDRAM ------------
		DRAM_ADDR: out std_logic_vector (12 downto 0);
		DRAM_BA: out std_logic_vector (1 downto 0);
		DRAM_CAS_N: out std_logic;
		DRAM_CKE: out std_logic;
		DRAM_CLK: out std_logic;
		DRAM_CS_N: out std_logic;		
		DRAM_DQ: inout std_logic_vector(15 downto 0);
		DRAM_LDQM: out std_logic;
		DRAM_RAS_N: out std_logic;
		DRAM_UDQM: out std_logic;
		DRAM_WE_N: out std_logic;
		
		----------- SEG7 ------------
		HEX0: out std_logic_vector(7 downto 0);
		HEX1: out std_logic_vector(7 downto 0);
		HEX2: out std_logic_vector(7 downto 0);
		HEX3: out std_logic_vector(7 downto 0);
		HEX4: out std_logic_vector(7 downto 0);
		HEX5: out std_logic_vector(7 downto 0);

		----------- KEY ------------
		KEY: in std_logic_vector(1 downto 0);

		----------- LED ------------
		LEDR: out std_logic_vector(9 downto 0);

		----------- SW ------------
		SW: in std_logic_vector(9 downto 0);

		----------- VGA ------------
		VGA_B: out std_logic_vector(3 downto 0);
		VGA_G: out std_logic_vector(3 downto 0);
		VGA_HS: out std_logic;
		VGA_R: out std_logic_vector(3 downto 0);
		VGA_VS: out std_logic;
	
		----------- Accelerometer ------------
		GSENSOR_CS_N: out std_logic;
		GSENSOR_INT: in std_logic_vector(2 downto 1);
		GSENSOR_SCLK: out std_logic;
		GSENSOR_SDI: inout std_logic;
		GSENSOR_SDO: inout std_logic;
	
		----------- Arduino ------------
		ARDUINO_IO: inout std_logic_vector(15 downto 0);
		ARDUINO_RESET_N: inout std_logic
	);
end entity;


architecture rtl of de10_lite is
	-- Declaração de sinais
    signal clock        : std_logic;
    signal FS           : std_logic;
    signal MS           : std_logic;
    signal R            : std_logic;
    signal C            : std_logic;
    signal FM           : std_logic;
    signal MM           : std_logic;

	signal source: std_logic_vector(31 downto 0);
	signal probe: std_logic_vector(31 downto 0);

	-- Declaração de componentes
	component probes is
		port (
			source : out std_logic_vector(31 downto 0);                    -- source
			probe  : in  std_logic_vector(31 downto 0) := (others => 'X')  -- probe
		);
	end component probes;
begin
	-- Instances
	probes_0 : component probes
	port map (
		source => source, 
		probe  => probe  
	);

	PLL_inst : work.PLL PORT MAP (
		inclk0	 => MAX10_CLK1_50,
		c0	 => clock
	);


    dut: entity work.alarm 
        port map (
            clock   => clock,
            FS      => FS,
            MS      => MS,
            R       => R,
            C       => C,
            FM      => FM,
            MM      => MM
        );

	-- External port mapping
	FS	<= SW(0);
	MS	<= SW(1);
	R	<= SW(2);
	C	<= SW(3);

	probe(0) <= FM;
	probe(1) <= MM;
end;

