-------------------------------------------------------------------
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : FSM to control an alarm system
-------------------------------------------------------------------
-- Insert library and use clauses
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Begin entity declaration
ENTITY alarm IS
	-- Begin port declaration
	PORT (
		-- Declare control inputs
        clock   : IN    STD_LOGIC;
        FS      : IN    STD_LOGIC;
        MS      : IN    STD_LOGIC;
        R       : IN    STD_LOGIC;
        C       : IN    STD_LOGIC;
		
		-- Declare control outputs
        FM      : OUT   STD_LOGIC;
        MM      : OUT   STD_LOGIC
	);
-- End entity
END ENTITY alarm;

--  Begin architecture 
ARCHITECTURE logic OF alarm IS

	-- Declare enumerated state type
	TYPE state_type IS (state_init, state_wait, state_motion_0, state_motion_1, state_motion_2, state_front);
	
	-- Declare two signals named "current_state" and "next_state" to be of enumerated type
	SIGNAL current_state, next_state : state_type;
BEGIN
	-- Sequential process to control state assignment by making current_state equal to next state on
	-- Rising edge transitions
    -- The reset is asynchronous
	assignment: PROCESS (clock, R)
	BEGIN
		IF R = '1' THEN
			current_state <= state_init;
		ELSIF rising_edge(clock) THEN
			current_state <= next_state;
		END IF;
	END PROCESS assignment;
	
	-- Determine next_state based on current state and inputs
	transition: PROCESS (current_state, FS, MS, R, C)
	BEGIN
		CASE current_state IS
			WHEN state_init =>
				next_state <= state_wait;
			WHEN state_wait =>
				IF FS = '1' THEN
					next_state <= state_front;
				ELSIF MS = '1' THEN
					next_state <= state_motion_0;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN state_motion_0 =>
				next_state <= state_motion_1;
			WHEN state_motion_1 =>
				next_state <= state_motion_2;
			WHEN state_motion_2 =>
				next_state <= state_wait;
			WHEN others => --state_front
				IF C = '1' THEN
					next_state <= state_wait;
				ELSE
					next_state <= current_state;
				END IF;
		END CASE;
	END PROCESS transition;

	-- Moore output logic for outputs
    moore: PROCESS(current_state)
    BEGIN 
        FM <= '1';
        MM <= '1';
        CASE current_state IS
			WHEN state_init =>
				FM <= '0';
				MM <= '0';
			WHEN state_wait =>
				FM <= '0';
				MM <= '0';
			WHEN state_motion_0 =>
				FM <= '0';
				MM <= '1';
			WHEN state_motion_1 =>
				FM <= '0';
				MM <= '1';
			WHEN state_motion_2 =>
				FM <= '0';
				MM <= '1';
			WHEN others => --state_front
				FM <= '1';
				MM <= '0';
        END CASE;
    END PROCESS moore;
END ARCHITECTURE logic;