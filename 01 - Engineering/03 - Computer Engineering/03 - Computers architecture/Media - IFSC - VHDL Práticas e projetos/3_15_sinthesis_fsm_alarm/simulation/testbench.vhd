-------------------------------------------------------------------
-- author      : Leonardo Benitez
-- version     : 0.1
-- copyright   : Instituto Federal de Santa Catarina
-- description : FSM to control an alarm system
-------------------------------------------------------------------

LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.env.finish;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de sinais
    signal clock        : std_logic;
    signal FS           : std_logic;
    signal MS           : std_logic;
    signal R            : std_logic;
    signal C            : std_logic;
    signal FM           : std_logic;
    signal MM           : std_logic;


BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.alarm 
        port map (
            clock   => clock,
            FS      => FS,
            MS      => MS,
            R       => R,
            C       => C,
            FM      => FM,
            MM      => MM
        );

    -- Clock process
    process
    begin
        clock <= '0';
        wait for 50 ns;
        clock <= '1';  
        wait for 50 ns;
    end process;

    -- Test process
    process
    begin
        R <= '1';
        wait for 25 ns;
        --assert state==init
        assert FM='0' severity failure; 
        assert MM='0' severity failure; 
        
        R <= '0';
        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure; 

        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure; 

        FS <= '1';
        wait for 100 ns;
        --assert state==front
        assert FM='1' severity failure; 
        assert MM='0' severity failure; 

        
        FS <= '1';
        wait for 100 ns;
        --assert state==front
        assert FM='1' severity failure; 
        assert MM='0' severity failure; 

        FS <= '0';
        wait for 100 ns;
        --assert state==front
        assert FM='1' severity failure; 
        assert MM='0' severity failure; 


        C <= '1';
        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure; 

        C <= '0';
        MS <= '1';
        wait for 100 ns;
        --assert state==motion
        assert FM='0' severity failure; 
        assert MM='1' severity failure; 

        MS <= '0';
        wait for 100 ns;
        --assert state==motion
        assert FM='0' severity failure; 
        assert MM='1' severity failure; 

        wait for 100 ns;
        --assert state==motion
        assert FM='0' severity failure; 
        assert MM='1' severity failure; 

        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure; 

        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure;
        
        R <= '1';
        wait for 100 ns;
        --assert state==init
        assert FM='0' severity failure; 
        assert MM='0' severity failure;

        R <= '0';
        wait for 100 ns;
        --assert state==wait
        assert FM='0' severity failure; 
        assert MM='0' severity failure;
        
        wait;
    end process;
END ARCHITECTURE stimulus;
