#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom ../source/alarm.vhd
vcom testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -height 15 -divider "Inputs"
add wave -label clock   /clock
add wave -label FS      /FS
add wave -label MS      /MS
add wave -label R       /R
add wave -label C       /C
add wave -height 15 -divider "Outpus"
add wave -label FM      /FM
add wave -label MM      /MM


#Simula até um 500ns
run 5000ns

wave zoomfull
write wave wave.ps