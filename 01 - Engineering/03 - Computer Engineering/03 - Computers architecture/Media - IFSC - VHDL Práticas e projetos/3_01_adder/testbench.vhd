LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    constant N: integer := 5;

    -- Declaração do componente
    component adder is
        generic(
            N: integer := 5
        );
        port (
            a, b: IN std_logic_vector(N-1 downto 0);
            sum: OUT std_logic_vector(N-1 downto 0)
        );
    end component adder;

    -- Declaração de sinais
    signal a: std_logic_vector(N-1 downto 0);
    signal b: std_logic_vector(N-1 downto 0);
    signal sum: std_logic_vector(N-1 downto 0);

BEGIN  -- inicio do corpo da arquitetura
    process
    begin
        a <= std_logic_vector(To_unsigned(3, N));
        b <= std_logic_vector(To_unsigned(9, N));

        wait for 250ns;

        a <= std_logic_vector(To_unsigned(1, N));
        b <= std_logic_vector(To_unsigned(12, N));

        wait;
    end process;

    dut: adder 
        generic map (
            N => N
        ) 
        port map (
            a => a,
            b => b,
            sum => sum
    );
END ARCHITECTURE stimulus;
