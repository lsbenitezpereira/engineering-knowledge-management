LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY reg_4b_pipo IS
	PORT (
		D		: IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
		CLK		: IN	STD_LOGIC;
		EN		: IN	STD_LOGIC;
		Q		: OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END reg_4b_pipo;

ARCHITECTURE Comportamento OF reg_4b_pipo IS
BEGIN
	PROCESS(CLK)
	BEGIN
		IF (CLK'EVENT AND CLK='1' AND EN='1') THEN
			Q <= D;
		END IF;
	END PROCESS;
END Comportamento;