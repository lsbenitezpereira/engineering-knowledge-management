#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom paridade.vhd testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -radix binary    /sequence
add wave -radix binary    /enable
add wave -radix binary    /parity

#Simula até um 500ns
run 500ns

wave zoomfull
write wave wave.ps
