LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de sinais
    signal sequence: std_logic_vector(15 downto 0);
    signal enable:   std_logic;
    signal parity:   std_logic;

BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.paridade 
        port map (
            sequence  => sequence,
            enable    => enable,
            parity    => parity
        );

    -------------
    process
    begin
        sequence  <= "0000000000000001";
        enable    <= '0';
        wait for 100 ns;

        sequence  <= "0000000000000001";
        enable    <= '1';
        wait for 100 ns;

        sequence  <= "0000000000000011";
        enable    <= '0';
        wait for 100 ns;

        sequence  <= "0000000000000011";
        enable    <= '1';
        wait;
    end process;
END ARCHITECTURE stimulus;
