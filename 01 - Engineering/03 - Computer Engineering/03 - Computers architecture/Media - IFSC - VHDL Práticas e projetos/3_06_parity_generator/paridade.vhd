-------------------------------------------------------
--! @file    paridade.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-02
--! @version 0.1
--! @brief   Gerador de paridade par
--!          `parity` é 1 quando o número de 1s em `sequence` é par
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity paridade is
    port (
        sequence: in  std_logic_vector(15 downto 0);
        enable:   in  std_logic;
        parity:   out std_logic
    );
end entity paridade;

architecture rtl of paridade is
begin
    process (sequence, enable)
        variable partial_parity: std_logic;
    begin
        partial_parity := '0';
        for s in sequence'range loop
            partial_parity := partial_parity xor sequence(s);
        end loop;
        parity <= not(partial_parity) and enable;
    end process;
    
end architecture rtl;