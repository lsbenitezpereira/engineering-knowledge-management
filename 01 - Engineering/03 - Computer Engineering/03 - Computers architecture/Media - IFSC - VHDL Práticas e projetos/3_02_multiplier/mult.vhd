-------------------------------------------------------
--! @file    mult.vhd
--! @author  Leonardo Benitez
--! @version 0.1
--! @brief   Multiplier circuit
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
	    
entity mult is
    generic(
        N : integer := 5 --! number of bits
    );

    port (
        a : in std_logic_vector(N-1 downto 0);
        b : in std_logic_vector(N-1 downto 0);
        product : out std_logic_vector(2*N-1 downto 0)
    );
end entity mult;

architecture rtl of mult is
begin
    product <= std_logic_vector(unsigned(a) * unsigned(b)); 
end architecture rtl;