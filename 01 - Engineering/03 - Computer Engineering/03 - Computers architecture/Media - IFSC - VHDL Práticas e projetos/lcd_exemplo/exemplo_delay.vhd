library ieee;
use ieee.std_logic_1164.all;

entity FSM is
port(X: in std_logic;
	  RST: in std_logic;
	  CLK: in std_logic;
	  Y: out std_logic);
end FSM;

architecture Comportamento of FSM is
	TYPE ESTADO IS (S0, S1);
	SIGNAL estado_atual, proximo_estado: ESTADO;
	SIGNAL time : INTEGER RANGE 0 TO 10;
begin

	PROCESS(CLK, RST)
		VARIABLE count : INTEGER RANGE 0 TO 10;
	BEGIN
		IF (RST='0') THEN
			estado_atual <= S0;
			count := 0;
		ELSIF(CLK'EVENT AND CLK='1') THEN
			count := count + 1;
			IF (count > time) THEN
				estado_atual <= proximo_estado;
				count := 0;
			END IF;
		END IF;	
	END PROCESS;
	
	PROCESS(estado_atual)
	BEGIN
		
		CASE estado_atual IS
			WHEN S0 =>
				Y <= '0';
				time <= 0;
				IF (X='0') THEN
					proximo_estado <= S0;
				ELSE
					proximo_estado <= S1;
				END IF;
			
			WHEN S1 =>
				Y <= '1';
				time <= 5;
				proximo_estado <= S0;
				
		END CASE;
	END PROCESS;
	
end Comportamento;