--=================================================--
--	Sincronous Counter par/impar 3 bits
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Negative representaion: 2 complement's
--	Version: 1.0 					       14/08/2017
--=================================================--

-- Flip Flop JK
library ieee;
use ieee.std_logic_1164.all;

entity JK_FF is
	port (	
		Q, Qbar:	out std_logic;
		clock:		in std_logic;	-- FF is active in rising edge
		clear:		in std_logic;	-- Active in high
		preset:		in std_logic;	-- Active in high
		J, K:		in std_logic	
	);
end JK_FF;

architecture func of JK_FF is
    -- Interconnecting signals
    signal state: std_logic;
    signal input: std_logic_vector(1 downto 0);
	
begin
    -- Combine inputs into vector
    input <= J & K;	
	
	-- Sequencial statements
    process(clock, clear, preset) is
    begin
		if (clear='1') then
			state <= '0';
		elsif (preset='1') then
			state <= '1';
		elsif (rising_edge(clock)) then
			case (input) is
				when "11"	=> state <= not state;
				when "10"	=> state <= '1';
				when "01"	=> state <= '0';
				when others	=> null;
			end case;
		end if;
    end process;
	
    -- Concurrent statements
    Q <= state;
    Qbar <= not state;
end func;

-----------------------------------------------------
-- Sincronous Counter
library ieee;
use ieee.std_logic_1164.all;

entity lab5 is
	port (
		clock	: in 		std_logic;						-- Active in rising edge
		mode		: in 		std_logic;						-- Set the output to "0101"
		Q	: buffer 	std_logic_vector (3 downto 1)	-- Four bits output
	);
end entity lab5;

architecture struct of lab5 is
	-- Flip Flop JK component
	component JK_FF is
		port (							-- Positional structure: (Q, Qbar, Clock, Clear, Preset, J, K,)
			Q, Qbar:	out std_logic;
			clock:		in std_logic;
			clear:		in std_logic;	
			preset:		in std_logic;	
			J, K:		in std_logic
		);
	end component;
	
	-- Interconnecting signals??
	signal TA, TB, TC	: std_logic;
begin
	TA <= (mode and not Q(3) and not Q(2) and not Q(1)) OR (Q(3) and Q(2) and Q(1));
	TB <= Q(1) OR (not mode and not Q(1)) OR (mode and Q(3) and not Q(2)) OR (mode and Q(2) and not Q(1));
	TC <= Q(2);
	
	-- Do something
	A0: JK_FF port map (Q(1), open, clock, '0', '0', TA, TA);
	A1: JK_FF port map (Q(2), open, clock, '0', '0', TB, TB);
	A2: JK_FF port map (Q(3), open, clock, '0', '0', TC, TC);

end struct;
