#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom mult_control.vhd testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -radix binary    /clk
add wave -radix binary    /reset_a
add wave -radix binary    /start
add wave -radix unsigned  /count
add wave -radix binary    /input_sel
add wave -radix binary    /shift_sel
add wave -radix binary    /state_out
add wave -radix binary    /done
add wave -radix binary    /clk_ena
add wave -radix binary    /sclr_n

#Simula até x ns
run 1500ns

wave zoomfull
write wave wave.ps
