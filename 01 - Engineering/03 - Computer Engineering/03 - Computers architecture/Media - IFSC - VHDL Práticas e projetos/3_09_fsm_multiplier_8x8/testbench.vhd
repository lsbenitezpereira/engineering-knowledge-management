LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.env.finish;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de sinais
    signal clk:         std_logic;
    signal reset_a:     std_logic;
    signal start:       std_logic;
    signal count:       std_logic_vector(1 downto 0);
    signal input_sel:   std_logic_vector(1 downto 0);
    signal shift_sel:   std_logic_vector(1 downto 0);
    signal state_out:   std_logic_vector(2 downto 0);
    signal done:        std_logic;
    signal clk_ena:     std_logic;
    signal sclr_n:      std_logic;

BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.mult_control 
        port map (
            clk         => clk,
            reset_a     => reset_a,
            start       => start,
            count       => count,
            input_sel   => input_sel,
            shift_sel   => shift_sel,
            state_out   => state_out,
            done        => done,
            clk_ena     => clk_ena,
            sclr_n      => sclr_n
        );

    -- Clock process
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';  
        wait for 50 ns;
    end process;

    -- Test process
    process
    begin
        reset_a <= '1';
        wait for 25 ns;
        
        assert state_out = "000" severity failure;
        reset_a <= '0';
        start <= '1';
        wait for 1 ns;
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '0' severity failure; 
        wait for 100 ns;
        
        assert state_out = "001" severity failure;
        start <= '0';
        count <= "00";
        wait for 1 ns;
        assert input_sel = "00" severity failure;
        assert shift_sel = "00" severity failure;
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '1' severity failure; 
        wait for 100 ns;
        
        assert state_out = "010" severity failure;
        start <= '0';
        count <= "01";
        wait for 1 ns;
        assert input_sel = "01" severity failure;
        assert shift_sel = "01" severity failure;
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '1' severity failure; 
        wait for 100 ns;
        
        assert state_out = "010" severity failure;
        start <= '1'; --se ficasse nessa condição, iria para ERR
        wait for 1 ns;
        assert done = '0' severity failure;
        assert clk_ena = '0' severity failure;
        assert sclr_n = '1' severity failure; 
        
        assert state_out = "010" severity failure;
        start <= '0';
        count <= "10";
        wait for 100 ns;
        
        assert state_out = "011" severity failure;
        start <= '0';
        count <= "11"; 
        wait for 1 ns;
        assert input_sel = "11" severity failure;
        assert shift_sel = "10" severity failure;
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '1' severity failure;
        wait for 100 ns;
        
        assert state_out = "100" severity failure;
        assert done = '1' severity failure;
        assert clk_ena = '0' severity failure;
        assert sclr_n = '1' severity failure;
        wait for 100 ns;
        
        assert state_out = "000" severity failure;
        assert done = '0' severity failure;
        assert clk_ena = '0' severity failure;
        assert sclr_n = '1' severity failure;
        wait for 100 ns;
        
        assert state_out = "000" severity failure;
        start <= '1';
        wait for 1 ns;
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '0' severity failure;
        wait for 100 ns;
        
        assert state_out = "001" severity failure;
        assert done = '0' severity failure;
        assert clk_ena = '0' severity failure;
        assert sclr_n = '1' severity failure;
        wait for 100 ns;
        
        assert state_out = "101" severity failure; --ERR
        assert done = '0' severity failure;
        assert clk_ena = '1' severity failure;
        assert sclr_n = '0' severity failure;
        wait for 100 ns;
        
        assert state_out = "001" severity failure;
        wait for 100 ns;
        
        assert state_out = "101" severity failure; --ERR
        wait for 100 ns;
        
        assert state_out = "001" severity failure;

        wait;
    end process;
END ARCHITECTURE stimulus;
