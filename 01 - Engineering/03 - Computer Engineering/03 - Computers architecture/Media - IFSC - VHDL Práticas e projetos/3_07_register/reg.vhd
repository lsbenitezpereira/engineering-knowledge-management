-------------------------------------------------------
--! @file    reg.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-07
--! @version 0.1
--! @brief   Registrador tipo D com N bits
--!          Todas as operações ocorrem na subida de clk, com exceção de sclr_n
--!          sclr_n é baixo, limpe a saída (prioridade mais alta).
--!          Na subida do clock, verifique se clk_ena é alto.   
--!          clk_ena é alto, as saídas são atribuídas às entradas
--!          clk_ena é baixo, nada é feito.

-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reg is
    generic(
        N : integer := 8 --! number of bits
    );
    port (
        clk:        in  std_logic;
        sclr_n:     in  std_logic;
        clk_ena:    in  std_logic;
        datain:     in  std_logic_vector(N-1 downto 0);
        reg_out:    out std_logic_vector(N-1 downto 0)
    );
end entity reg;

architecture rtl of reg is
begin
    process (clk, sclr_n)
    begin
        if sclr_n = '0' then
            reg_out <= (others => '0');           
        elsif rising_edge(clk) and clk_ena = '1' then
            reg_out <= datain;
        end if;
    end process;
    
end architecture rtl;
