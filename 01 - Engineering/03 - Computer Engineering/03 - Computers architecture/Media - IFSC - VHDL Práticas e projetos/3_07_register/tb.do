#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom reg.vhd testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -radix binary    /clk
add wave -radix binary    /sclr_n
add wave -radix binary    /clk_ena
add wave -radix dec       /datain
add wave -radix dec       /reg_out
add wave -radix dec       /reg_out2

#Simula até um 500ns
run 500ns

wave zoomfull
write wave wave.ps
