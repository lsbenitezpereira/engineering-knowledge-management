LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.env.finish;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de constantes
    constant N: integer := 4;

    -- Declaração de sinais
    signal clk:     std_logic;
    signal sclr_n:  std_logic;
    signal clk_ena: std_logic;
    signal datain:  std_logic_vector(N-1 downto 0);
    signal reg_out: std_logic_vector(N-1 downto 0);
    signal reg_out2: std_logic_vector(N-1 downto 0);

BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.reg 
        generic map (
            N => N
        )
        port map (
            clk       => clk,
            sclr_n    => sclr_n,
            clk_ena   => clk_ena,
            datain    => datain,
            reg_out   => reg_out
        );

    dut2: entity work.reg 
        generic map (
            N => N
        )
        port map (
            clk       => clk,
            sclr_n    => '1',
            clk_ena   => '1',
            datain    => reg_out,
            reg_out   => reg_out2
        );

    -- Clock process
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';  
        wait for 50 ns;
    end process;

    -- Test process
    process
    begin
        sclr_n <= '0';
        wait for 25 ns;
        assert reg_out = "0000" severity failure;

        sclr_n <= '1';
        clk_ena <= '1';
        datain <= "0011";
        wait for 100 ns;
        assert reg_out = "0011" severity failure;

        sclr_n <= '1';
        clk_ena <= '1';
        datain <= "1001";
        wait for 100 ns;
        assert reg_out = "1001" severity failure;

        sclr_n <= '1';
        clk_ena <= '0';
        datain <= "1111";
        wait for 100 ns;
        assert reg_out = "1001" severity failure;

        sclr_n <= '0';
        clk_ena <= '0';
        datain <= "1111";
        wait for 100 ns;
        assert reg_out = "0000" severity failure;

        wait;
    end process;
END ARCHITECTURE stimulus;
