-------------------------------------------------------
--! @file    mult_8x8.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-14
--! @version 0.1
--! @brief   Hardware multiplier
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult_8x8 is
    port (
        dataa:              in  std_logic_vector(7 downto 0);
        datab:              in  std_logic_vector(7 downto 0);
        start:              in  std_logic;
        reset_a:            in  std_logic;
        clk:                in  std_logic;
        done_flag:          out std_logic;
        product8x8_out:     out std_logic_vector(15 downto 0);
        seg_a:              out std_logic;
        seg_b:              out std_logic;
        seg_c:              out std_logic;
        seg_d:              out std_logic;
        seg_e:              out std_logic;
        seg_f:              out std_logic;
        seg_g:              out std_logic
    );
end entity mult_8x8;

architecture rtl of mult_8x8 is
    -- Declaração de sinais internos
    signal aout:        std_logic_vector(3 downto 0);
    signal bout:        std_logic_vector(3 downto 0);
    signal product:     std_logic_vector(7 downto 0);
    signal shift_out:   std_logic_vector(15 downto 0);
    signal sum:         std_logic_vector(15 downto 0);
    signal product8x8:  std_logic_vector(15 downto 0);

    signal count:       std_logic_vector(1 downto 0);
    signal sel:         std_logic_vector(1 downto 0);
    signal shift:       std_logic_vector(1 downto 0);
    signal state_out:   std_logic_vector(2 downto 0);
    signal clk_ena:     std_logic;
    signal sclr_n:      std_logic;
    signal start_n:     std_logic;

    signal state_temp:        std_logic_vector(3 downto 0);
    signal segs:        std_logic_vector(7 downto 0);

begin
    mux4A: entity work.mux_sel
        generic map (
            N => 4
        ) 
        port map (
            mux_in_a => dataa(3 downto 0),
            mux_in_b => dataa(7 downto 4),
            mux_sel => sel(1),
            mux_out => aout
        );

    ----
    mux4B: entity work.mux_sel
        generic map (
            N => 4
        ) 
        port map (
            mux_in_a => datab(3 downto 0),
            mux_in_b => datab(7 downto 4),
            mux_sel => sel(0),
            mux_out => bout
        );

    ----
    mult4x4: entity work.mult
        generic map (
            N => 4
        ) 
        port map (
            a => aout,
            b => bout,
            product => product
        );

    ----
    shifter: entity work.shifter
        port map (
            input       => product,
            shift_cntrl => shift,
            shift_out   => shift_out
        );

    ----
    adder: entity work.adder
        generic map (
            N => 16
        ) 
        port map (
            a => shift_out,
            b => product8x8,
            sum => sum
        );

    ----
    product8x8_out <= product8x8;
    reg16: entity work.reg 
        generic map (
            N => 16
        )
        port map (
            clk       => clk,
            sclr_n    => sclr_n,
            clk_ena   => clk_ena,
            datain    => sum,
            reg_out   => product8x8
        );

    ----
    mult_control: entity work.mult_control 
        port map (
            clk         => clk,
            reset_a     => reset_a,
            start       => start,
            count       => count,
            input_sel   => sel,
            shift_sel   => shift,
            state_out   => state_out,
            done        => done_flag,
            clk_ena     => clk_ena,
            sclr_n      => sclr_n
        );

    ----
    start_n <= not start;
    counter: entity work.counter 
        port map (
            clk         => clk,
            aclr_n      => start_n,
            count_out   => count
        );

    ----
    state_temp <= "0" & state_out;
    seg_a <= segs(7);
    seg_b <= segs(6);
    seg_c <= segs(5);
    seg_d <= segs(4);
    seg_e <= segs(3);
    seg_f <= segs(2);
    seg_g <= segs(1);
    seven_segment_cntrl: entity work.seven_segment_cntrl 
        port map (
            input       => state_temp,
            segs        => segs
        );
end architecture rtl;