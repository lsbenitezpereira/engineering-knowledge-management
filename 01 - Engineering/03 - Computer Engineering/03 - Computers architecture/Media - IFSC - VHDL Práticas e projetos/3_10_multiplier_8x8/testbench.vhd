LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.env.finish;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de sinais
    signal dataa:               std_logic_vector(7 downto 0);
    signal datab:               std_logic_vector(7 downto 0);
    signal start:               std_logic;
    signal reset_a:             std_logic;
    signal clk:                 std_logic;
    signal done_flag:           std_logic;
    signal product8x8_out:      std_logic_vector(15 downto 0);
    signal seg_a:               std_logic;
    signal seg_b:               std_logic;
    signal seg_c:               std_logic;
    signal seg_d:               std_logic;
    signal seg_e:               std_logic;
    signal seg_f:               std_logic;
    signal seg_g:               std_logic;

BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.mult_8x8 
        port map (
            dataa           => dataa,
            datab           => datab,
            start           => start,
            reset_a         => reset_a,
            clk             => clk,
            done_flag       => done_flag,
            product8x8_out  => product8x8_out,
            seg_a           => seg_a,
            seg_b           => seg_b,
            seg_c           => seg_c,
            seg_d           => seg_d,
            seg_e           => seg_e,
            seg_f           => seg_f,
            seg_g           => seg_g
        );

    -- Clock process
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';  
        wait for 50 ns;
    end process;

    -- Test process
    process
    begin

        --sete a e b com valores altos
        -- wait for 500 ns;
        --assert product8x8_out = std_logic_vector(unsigned(a) * unsigned(b))
        reset_a <= '1';
        wait for 25 ns;
        
        dataa <= std_logic_vector(To_unsigned(255, 8));
        datab <= std_logic_vector(To_unsigned(100, 8));
        reset_a <= '0';
        start <= '1';
        wait for 50 ns;

        
        assert done_flag = '0' severity failure;
        start <= '0';
        wait for 400 ns; --4 clock cycles

        assert done_flag = '1' severity failure;
        assert product8x8_out = std_logic_vector(To_unsigned(25500, 16)) severity failure;
        
        -----------------
        wait for 100 ns;
        dataa <= std_logic_vector(To_unsigned(4, 8));
        datab <= std_logic_vector(To_unsigned(5, 8));
        start <= '1';
        wait for 100 ns;

        
        assert done_flag = '0' severity failure;
        start <= '0';
        wait for 400 ns; --4 clock cycles

        assert done_flag = '1' severity failure;
        assert product8x8_out = std_logic_vector(To_unsigned(20, 16)) severity failure;



        

        wait;
    end process;
END ARCHITECTURE stimulus;
