#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom "../Experimento 3.01 - Somador de n-bits/adder.vhd"
vcom "../Experimento 3.02 - Multiplicador de n-bits/mult.vhd"
vcom "../Experimento 3.03 - Multiplexador n-bits/mux_sel.vhd"
vcom "../Experimento 3.04 - Shifter/shifter.vhd"
vcom "../Experimento 3.05 - Decodificador/seven_segment_cntrl.vhd"
vcom "../Experimento 3.06 - Gerador de paridade/paridade.vhd"
vcom "../Experimento 3.07 - Registrador D de n-bits/reg.vhd"
vcom "../Experimento 3.08 - Contador/counter.vhd"
vcom "../Experimento 3.09 - FSM multiplicador 8x8/mult_control.vhd"
vcom mult_8x8.vhd
vcom testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -radix unsigned   -format literal /dataa
add wave -radix unsigned   -format literal /datab
add wave -radix binary     -format literal /start
add wave -radix binary     -format literal /reset_a
add wave -radix binary                     /clk
add wave -radix binary     -format literal /done_flag
add wave -radix unsigned   -format literal /product8x8_out
add wave -radix binary     -format literal /seg_a
add wave -radix binary     -format literal /seg_b
add wave -radix binary     -format literal /seg_c
add wave -radix binary     -format literal /seg_d
add wave -radix binary     -format literal /seg_e
add wave -radix binary     -format literal /seg_f
add wave -radix binary     -format literal /seg_g
add wave -radix binary     -format literal /dut/count
add wave -radix binary     -format literal /dut/state_out


#Simula até x ns
run 1500ns

wave zoomfull
write wave wave.ps
