--=================================================--
--	Flip Flop JK
--	Author:		Leonardo Benitez
--	Endianess:	litle endian
--	Version:	1.0					       09/10/2017
--=================================================--

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY main IS
	PORT (
		SW		: in	std_logic_vector (17 downto 0);
		LEDR	: out	std_logic_vector (17 downto 0)
	);
END main;

ARCHITECTURE FUNCIONAL OF main IS
	COMPONENT JK_FF IS
		PORT(
			JK				: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
			PRESET, CLEAR	: IN STD_LOGIC;	-- Active in 0
			CLK				: IN STD_LOGIC;
			Q				: OUT STD_LOGIC			
		);
	END COMPONENT;
BEGIN
	J1: JK_FF PORT MAP (SW(1 DOWNTO 0), SW(2), SW(3), SW(17), LEDR(0));
END FUNCIONAL;