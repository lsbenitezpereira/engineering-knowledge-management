-------------------------------------------------------
--! @file    counter.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-07
--! @version 0.1
--! @brief   Contador crescente de 2 bits
--!          A saída vai para "00" imediatamente quando aclr_n é baixo.
--!          Se aclr_n não é baixo, o contador é incrementado em 1 na subida do clock.
--!          aclr_n tem prioridade sobre a verificação da subida do clk. 
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
    port (
        clk:        in  std_logic;
        aclr_n:     in  std_logic;
        count_out:  out std_logic_vector(1 downto 0)
    );
end entity counter;

architecture rtl of counter is
begin
    process (clk, aclr_n)
        variable count_out_var: std_logic_vector(1 downto 0);
    begin
        if aclr_n = '0' then
            count_out_var := (others => '0');
        elsif rising_edge (clk) then
            count_out_var := std_logic_vector(unsigned(count_out_var) + 1);
        end if;
        count_out <= count_out_var; 
    end process;
end architecture rtl;
