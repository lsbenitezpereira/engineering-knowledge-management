LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.env.finish;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração de sinais
    signal clk:       std_logic;
    signal aclr_n:    std_logic;
    signal count_out: std_logic_vector(1 downto 0);

BEGIN  -- inicio do corpo da arquitetura
    dut: entity work.counter 
        port map (
            clk         => clk,
            aclr_n      => aclr_n,
            count_out   => count_out
        );

    -- Clock process
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';  
        wait for 50 ns;
    end process;

    -- Test process
    process
    begin
        aclr_n <= '0';
        wait for 25 ns;
        assert count_out = "00" severity failure;

        aclr_n <= '1';
        wait for 100 ns;
        assert count_out = "01" severity failure;

        wait for 500 ns;
        
        aclr_n <= '0';
        wait for 1 ns;
        assert count_out = "00" severity failure;

        wait for 100 ns;
        assert count_out = "00" severity failure;

        aclr_n <= '1';
        wait for 100 ns;
        assert count_out = "01" severity failure;

        wait;
    end process;
END ARCHITECTURE stimulus;
