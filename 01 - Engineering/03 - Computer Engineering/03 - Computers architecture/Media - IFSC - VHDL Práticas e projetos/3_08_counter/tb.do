#Cria biblioteca do projeto
vlib work

#compila projeto: todos os aquivo. Ordem é importante
vcom counter.vhd testbench.vhd

#Simula (work é o diretorio, testbench é o nome da entity)
vsim -t ns work.testbench

#Mosta forma de onda
view wave

#Adiciona ondas específicas
# -radix: binary, hex, dec
# -label: nome da forma de onda
add wave -radix binary    /clk
add wave -radix binary    /aclr_n
add wave -radix unsigned  /count_out

#Simula até um 1000ns
run 1000ns

wave zoomfull
write wave wave.ps
