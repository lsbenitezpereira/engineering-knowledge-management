-------------------------------------------------------
--! @file    adder.vhd
--! @author  Leonardo Benitez
--! @date    2021-11-02
--! @version 0.1
--! @brief   shifter circuit, 8 to 16 bits
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shifter is
    port (
        input:       in std_logic_vector(7 downto 0);
        shift_cntrl: in std_logic_vector(1 downto 0);
        shift_out:   out std_logic_vector(15 downto 0)
    );
end entity shifter;

architecture rtl of shifter is
begin
    process (shift_cntrl, input)
    begin
        --! Outra opção de implementação:
        --! Seta no começo `shift_out <= (others => '0');` e atribui com slicing
        if shift_cntrl = "00" then
            shift_out <= "00000000" & input;
        elsif shift_cntrl = "01" then
            shift_out <= "0000" & input & "0000";
        elsif shift_cntrl = "10" then
            shift_out <= input & "00000000";
        else
            shift_out <= "00000000" & input;
        end if;
    end process;
end architecture rtl;