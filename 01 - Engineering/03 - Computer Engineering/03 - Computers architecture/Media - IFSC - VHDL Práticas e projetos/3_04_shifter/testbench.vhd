LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

ENTITY testbench IS
END ENTITY testbench;

ARCHITECTURE stimulus OF testbench IS
    -- Declaração do componente
    component shifter is
        port (
            input:       in std_logic_vector(7 downto 0);
            shift_cntrl: in std_logic_vector(1 downto 0);
            shift_out:   out std_logic_vector(15 downto 0)
        );
    end component shifter;

    -- Declaração de sinais
    signal input:       std_logic_vector(7 downto 0);
    signal shift_cntrl: std_logic_vector(1 downto 0);
    signal shift_out:   std_logic_vector(15 downto 0);

BEGIN  -- inicio do corpo da arquitetura
    process
    begin
        input <= "00000001";

        shift_cntrl <= "00";
        wait for 100ns;

        shift_cntrl <= "01";
        wait for 100ns;

        shift_cntrl <= "10";
        wait for 100ns;

        shift_cntrl <= "11";
        wait;
    end process;

    dut: shifter port map (
        input       => input,
        shift_cntrl => shift_cntrl,
        shift_out   => shift_out
    );
END ARCHITECTURE stimulus;
