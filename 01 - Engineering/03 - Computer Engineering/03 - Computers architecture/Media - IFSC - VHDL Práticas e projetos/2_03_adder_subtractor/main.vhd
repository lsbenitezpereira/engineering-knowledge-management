--=================================================--
--	Four bits adder/subtractor
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Version: 1.0 					       25/09/2017
--=================================================--

library ieee;
use ieee.std_logic_1164.all;

entity main is
	port(
		SW		: in std_logic_vector (17 downto 0);
		LEDR	: out std_logic_vector (17 downto 0)
	);
end main;

architecture struct of main is
	component adder_subtractor_4b is
		port(
			mode	: in	std_logic;							-- 0=adder  1=subtractor
			A		: in	std_logic_vector (3 downto 0);		-- Number A (4  bits)
			B		: in	std_logic_vector (3 downto 0);		-- Number B (4 bits)
			S		: out	std_logic_vector (3 downto 0);		-- Result
			Cout	: out	std_logic;							-- Carry out
			V  		: out	std_logic							-- 1=Overflow		
		);
	end component;
begin
	ad0: adder_subtractor_4b port map (SW(17), SW(7 downto 4), SW(3 downto 0), LEDR(3 downto 0), LEDR(16), LEDR(17));
end struct;
