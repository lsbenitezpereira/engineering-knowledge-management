.data
display_width: .word 512		#512/8bits = 64 ----> 64 colunas
display_height: .word 256		#256/8bits = 32 ----> 32 linhas
unit_width: .word 8
unit_height: .word 8
white_color: .word 0xFFFFFFFF
red_color: .word   0xf41111
yellow_color: .word 0xeaff30
blue_color: .word 0x03469e
green_color: .word 0x026d0c
start_address: .word 0x10010000


.text	
	addiu	$sp, $sp, -48
	sw	$ra, 16($sp)
	
	#teste: draw line
	li	$a0, 0			# x0
	li	$a1, 30			# y0
	li	$a2, 0			# x1
	li	$a3, 3			# y1
	lw	$s0, yellow_color	#color
	
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$a3, 12($sp)
	sw	$s0, 20($sp)
	jal	draw_line		
	lw	$ra, 16($sp)	
	addiu	$sp, $sp, 48		# remove stack
	j	end
	
#void setPixel(int x, int y, int color);
set_pixel:
#set_pixel
#-----------------------------------	
#a0 		 32(sp)	
#---------------32(sp)

#	espaco	28(sp)
#s1		24(sp)	--->
#s0		20(sp)	--->color
#---------------
#ra		16(sp)--->
#---------------
#a3		12(sp)--->
#a2		8(sp)--->
#a1		4(sp)--->y
#a0		0(sp)--->x
	addiu	$sp, $sp, -48
	sw	$ra, 16($sp)
	
	la	$t0, 0x10010000
	mul	$t2, $a1, 256		# y * 256
	sll	$t1, $a0, 2		# x * 4
	add	$t9, $t1, $t2		# (y * 256) + (x * 4)
	add	$t9, $t9, $t0		# 0x10010000 +  (y * 256) + (x * 4)
	sw	$s0, 0($t9)		# armazena a cor na pos (x, y)
	
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 48
	jr	$ra

#void drawLine(int x0, int y0, int x1, int y1, int color);
draw_line:
#set_pixel
#-----------------------------------	
#a0 		 48(sp)	
#---------------48(sp)

#	espaco	44(sp)
#s5		40(sp) --->y
#s4		36(sp) --->x
#s3		32(sp) --->D
#s2		28(sp) --->dy
#s1		24(sp)	--->dx
#s0		20(sp)	--->color
#---------------
#ra		16(sp)--->
#---------------
#a3		12(sp)--->y1
#a2		8(sp)--->x1
#a1		4(sp)--->y0
#a0		0(sp)--->x0
	addiu	$sp, $sp, -48
	sw	$ra, 16($sp)
	sub	$s1, $a2, $a0		#dx = x1 - x0
	sw	$s1, 24($sp)
	sub	$s2, $a3, $a1		#dy = y1 - y0
	sw	$s2, 28($sp)
	sll	$s3, $s2, 1		#2 * dy 
	sub	$s3, $s3, $s1		#D = 2*dy - dx
	sw	$s3, 32($sp)
	add	$s5, $a1, $zero		# y = y0
	sw	$s5, 40($sp)
	add 	$s4, $a0, $zero		# x = x0
	sw	$s4, 36($sp)
for:
	bgt	$s4, $a2, end_draw	# if x > x1
	move	$a0, $s4
	move	$a1, $s5
	jal	set_pixel
	lw	$s3, 32($sp)
	blez	$s3, end_if		#D <= 0
	lw	$s5, 40($sp)
	lw	$s1, 24($sp)
	lw	$s2, 28($sp)
	addi	$s5, $s5, 1		# y = y + 1
	sll	$t0, $s1, 1		# 2 * dx
	sub	$s3, $s3, $t0		# D = D - 2 * dx
end_if:

	lw	$s3, 32($sp)
	lw	$s2, 28($sp)
	sll	$t1, $s2, 1
	add	$s3, $s3, $t1
	addi	$s4, $s4 1
	sw	$s3, 32($sp)
	j	for
end_draw:
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 48
	jr	$ra
end:
	li	$v0, 10
	syscall
