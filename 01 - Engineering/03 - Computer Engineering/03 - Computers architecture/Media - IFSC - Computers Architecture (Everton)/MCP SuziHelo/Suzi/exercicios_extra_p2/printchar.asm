.data
intro: .asciiz "Digite uma palavra: \n"
.align 2
text: .space 1024

 # -----------------------	
 # a0 		24(sp)
 # ------------- 24 Bytes
 #    espa�o     20(sp)
 # -------------
 # ra		16(sp)
 # -------------
 # a3		12(sp)
 # a2		8(sp)
 # a1		4(sp) ---> text
 # a0		0(sp)---> intro
 # -----------------------
 
.text
.globl main
main:
	addiu	$sp, $sp, -24
	sw	$ra, 16($sp)
	la	$a0, intro
	sw	$a0, 0($sp)
	sw	$s0, 8($sp)
	
	la	$a1, text
	sw	$a1, 4($sp)
	
	jal	print
	jal	read_string
	move	$a1, $v0
	
	li	$s0, 0
for:
	lb	$t2, 0($a1)
	beqz	$t2, while
	addi	$s0, $s0, 1
	addi	$a1, $a1, 1
	j	for
while:
	blez	$s0, end
	lw	$a1, 4($sp)
	subi	$s0, $s0, 1
	add	$a1 , $s0, $a1
	lb	$a0, 0($a1)
	jal	print_char
	j	while	
end:	
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	li	$v0, 10
	syscall
	
print:
	addiu	$sp, $sp, -24
	sw	$a0, 24($sp)
	sw	$ra, 16($sp)
	li	$v0, 4
	syscall
	lw	$a0, 24($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
	
read_string:
	addiu	$sp, $sp, -24
	sw	$ra, 16($sp)
	li	$v0, 8
	la	$a0, 0($a1)
	move	$t0, $a0
	syscall
	move	$v0, $t0
	lw	$a0, 24($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra

print_char:
	addiu	$sp, $sp, -24
	sw	$a0, 24($sp)
	sw	$ra, 16($sp)
	
	li	$v0, 11
	syscall

	lw	$a0, 24($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
