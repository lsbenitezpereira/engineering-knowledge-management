#include <stdlib.h>
#include <stdio.h>

#include "pilha.h"

struct pilhas {
    int topo;
    int data[TAMANHO_DADOS_PILHA];
};


//cria uma pilha para inteiros
pilha_t * cria_pilha (void)
{
    pilha_t *pilha = (pilha_t*)malloc(sizeof(pilha_t));

    pilha->topo = 0;

    return pilha;
}


//adiciona elemento
void push(int dado, pilha_t *pilha)
{
    int topo = pilha->topo;

    if (topo > TAMANHO_DADOS_PILHA) {
        fprintf(stderr, "Tamanho maximo da pilha atingido!\n");
        exit(EXIT_FAILURE);
    }

    pilha->data[topo] = dado;
    pilha->topo++;
}

//retira elemento do topo
int pop(pilha_t *pilha)
{
    int topo = pilha->topo;

    if (topo < 0 || topo > TAMANHO_DADOS_PILHA){
        fprintf(stderr, "Pilha corrompida!\n");
        exit(EXIT_FAILURE);
    }

    if (topo == 0) {
        fprintf(stderr, "pop() em pilha vazia!\n");
        return 0;
    }

    pilha->topo--;
    return pilha->data[topo - 1];
}

int pilha_get_topo(pilha_t *pilha)
{
    return pilha->data[pilha->topo-1];
}

void print_pilha(pilha_t *pilha)
{
    int i;
    if (pilha->topo==0)
    {
        printf("pilha vazia");
        exit(EXIT_FAILURE);
    }
    for (i=0; i<pilha->topo; i++)
    {
        printf("dado %d: %d \n", i, pilha->data[i]);
    }
}

int tamanho_pilha (pilha_t *pilha)
{
    return pilha->topo;
}


int verifica_pilha_vazia (pilha_t *pilha)
{
    if (pilha->topo ==0)
        return 0;
    return 1;
}

int procura_item (pilha_t *pilha, int item)
{
    int i;
    for (i=0; i< pilha->topo; i++)
        if (pilha->data[i]==item)
            return 1;
    return 0;
}
