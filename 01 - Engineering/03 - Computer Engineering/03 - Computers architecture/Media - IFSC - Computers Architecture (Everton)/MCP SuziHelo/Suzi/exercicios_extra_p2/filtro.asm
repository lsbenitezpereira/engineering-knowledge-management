.data
vtr_ent: .word 0:100
vtr_res: .word 0:100
N:	.word 10
a0:	.float 0.5
a1:	.float 0.5
a2:	.float 0.2
a3:	.float 0.25
a4: 	.float 0.05

#a0-----a0-----40($sp)
#-----	36($sp)
#ra-----32($sp)
#s3-----vetor_ent-----28($sp)
#s2-----vetor_res-----24($sp)
#s1-----N------20($sp)
#s0-----a4-----16($sp)
#a3-----a3-----12($sp)
#a2-----a2-----8($sp)
#a1-----a1-----4($sp)
#a0-----a0-----0($sp)
.text
.globl main
main:
	addiu	$sp, $sp, -40
	la	$s3, vtr_ent
	lw	$s1, N 
	move	$t0, $s1
	move	$t2, $s3
	gera_vetor:
	beqz	$t0, vetor_gerado
	li	$a1, 100
	li	$v0, 42
	syscall 
	sw	$a0, 0($t2)
	addi	$t2, $t2, 4
	subi	$t0, $t0, 1
	j	gera_vetor
vetor_gerado:
	lw	$a0, a0
	lw	$a1, a1
	lw	$a2, a2
	lw	$a3, a3
	lw	$s0, a4
	la	$s2, vtr_res
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$a3, 12($sp)
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	sw	$ra, 32($sp) 
	
	jal	filtro
	move	$a0, $v0
	
	li	$v0, 1
	syscall 
	
	addiu	$sp, $sp, 40
	
	li	$v0, 10
	syscall 
filtro:
	addiu	$sp, $sp, -40
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$a3, 12($sp)
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	sw	$ra, 32($sp) 
	
	move	$a0, $s1
	jal	
	