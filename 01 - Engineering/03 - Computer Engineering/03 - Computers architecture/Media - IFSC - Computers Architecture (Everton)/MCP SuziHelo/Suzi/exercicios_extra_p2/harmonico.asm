.text
.globl main
main:
	li	$a0, 4
	jal	harmonico
	mtc1 	$v0, $f12
	
	li	$v0, 2
	syscall	
	
	li	$v0, 10
	syscall 
harmonico:
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$ra, 16($sp)
	sw	$s0, 20($sp)
	
	ble 	$a0, 1, harmonico1
	
	#1/n
	mtc1	$a0, $f0
	cvt.s.w	$f0, $f0
	li 	$t0, 1
	mtc1	$t0, $f1
	cvt.s.w	$f1, $f1
	div.s	$f2, $f1, $f0
	mfc1	$s0, $f2
	
	#n-1
	addi	$a0, $a0, -1
	jal	harmonico
	
	mtc1	$v0, $f0
	mtc1	$s0, $f1
	add.s	$f0, $f0, $f1
	mfc1	$v0, $f0
	j	end	
harmonico1:
	li	$v0, 1
	mtc1	$v0, $f0
	cvt.s.w	$f0, $f0
	mfc1	$v0, $f0
	j	end	
end:
	
	lw	$a0, 0($sp)
	lw	$ra, 16($sp)
	lw	$s0, 20($sp)
	addiu	$sp, $sp, 24
	jr	$ra