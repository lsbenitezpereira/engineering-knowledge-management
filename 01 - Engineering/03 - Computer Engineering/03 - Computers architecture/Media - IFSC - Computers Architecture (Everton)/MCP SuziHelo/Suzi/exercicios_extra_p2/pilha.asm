.data
struct_pilha: .space 84		#20 * 4 bytes + 4 bytes

.text
.globl main
main:
	la	$a0, struct_pilha
	
#teste de inicialização	
	jal	init
#teste de push	
	li	$a1, 2
	jal	push
	move	$t0, $v0
	
	li	$a1, 5
	jal	push
	move	$t0, $v0
	
	li	$a1, 10
	jal	push
	move	$t0, $v0	
#teste de pop
	jal	pop
	move	$t1, $v0	

#teste size
	jal 	size
	move	$t2, $v0
#end
	li	$v0, 10
	syscall 	
#------------------------------------------------------	
init:
	li	$t0, 0
	sw	$t0, 80($a0)
	jr	$ra
#------------------------------------------------------	
push:
	lw	$t1, 80($a0)	# topo = pilha->topo
	bgt	$t1, 20, exit_failure	#topo >20 --> failure
	addi	$t1, $t1, 1	
	sw	$t1, 80($a0)	#topo++
	sll	$t1, $t1, 2
	add	$t1, $t1, $a0
	sw	$a1, 0($t1)
	li	$v0, 1
	jr	$ra
	
exit_failure:
	li	$v0, 0
	jr	$ra
#------------------------------------------------------
pop:
	lw	$t0, 80($a0)	#topo
	slt	$t1, $t0, $zero	#topo < 0
	sgt	$t2, $t1, 20	#topo > 20
	or	$t1, $t1, $t2
	bnez	$t1, exit_pop		#pilha corrompida	
	beqz	$t0, pilha_vazia
	subi	$t3, $t0, 1
	sw	$t3, 80($a0)	#topo--
	sll	$t0, $t0, 2
	add	$t0, $t0, $a0
	lw	$v0, 0($t0)

	j	exit_pop
	
pilha_vazia:
	li	$v0, 0
exit_pop:
	jr	$ra
#------------------------------------------------------	
size:
	lw	$v0, 80($a0)
	jr	$ra
#------------------------------------------------------		
