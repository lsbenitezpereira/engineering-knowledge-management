.text
.globl main
main:
	add $s0, $zero, $gp
	addi $t2, $zero, 1
	lb  $t0, 0($s0)
	slti $t1, $t0, 0 
	beqz  $t1, label1
	beq $t1, $t2, label2
label2:
	sw $t0, 8($s0)
	b Exit
label1:
	sw $t0, 4($s0) 
	b Exit

Exit: 
	li $v0, 10
	syscall 
