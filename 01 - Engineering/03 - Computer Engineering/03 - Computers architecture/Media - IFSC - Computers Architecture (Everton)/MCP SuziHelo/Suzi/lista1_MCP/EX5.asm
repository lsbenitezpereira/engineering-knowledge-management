.text
.globl main
main:
	add $s0, $zero, $gp
	lb  $t0, 0($s0)
	lb  $t1, 4($s0)
	beq $t0, $t1, label1
	b Exit
label1:
	sw $t0, 8($s0) 
	b Exit

Exit: 
	li $v0, 10
	syscall 
