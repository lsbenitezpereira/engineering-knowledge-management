.data 
string: 	.asciiz "teste "

.data 	0x10008000
string_ptr: 	.word string
num_carac: 	.word 0

.text
.globl main
main:
	lw	$t0, string_ptr
	
loop:
	addi	$t0,$t0,1 #para descontar o \0
	lb	$t1, 0($t0)
	addi	$t2, $t2, 1
	bnez 	$t1 , loop

store:
	sw 	$t2, num_carac