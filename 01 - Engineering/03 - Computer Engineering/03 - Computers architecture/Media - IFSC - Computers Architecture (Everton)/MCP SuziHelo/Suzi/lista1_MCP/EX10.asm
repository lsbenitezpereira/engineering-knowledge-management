.text
.globl main
main:
	add $s0, $zero, $gp
	lbu  $t0, 0($s0) #A
	lbu  $t1, 4($s0) #B
	multu $t0, $t1 #A*B
	mflo $t2
	sw $t2, 8($s0)