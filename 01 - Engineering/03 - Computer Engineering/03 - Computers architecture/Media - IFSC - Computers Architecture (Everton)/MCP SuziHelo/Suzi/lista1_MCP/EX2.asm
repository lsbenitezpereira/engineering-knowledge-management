.text
.globl main
main:
	add $s0, $zero, $gp
	lbu  $t0, 0($s0)
	lbu $t1, 4($s0)
	lbu $t2, 8($s0)
	add $t3, $t0, $t1 
	add $t3, $t2, $t3 
	sw $t3, 12($s0)
