.text
.globl main
main:
	add $s0, $zero, $gp
	lb  $t0, 0($s0) #A
	lb  $t1, 4($s0) #B
	lb  $t2, 8($s0) #C
	slt $t3, $t1, $t0 #if B<A T3=1 ELSE T3=0
	bnez $t3, label1 #if T3=1
	slt $t3, $t1, $t2 #if B<C T3=1 ELSE T3=0
	bnez $t3, label3 #if T3=1 
	slt $t3, $t2, $t0 #if C<A T3=1 ELSE T3=0
	bnez $t3, label4 #if T3=1
	b label5 #if T3=0	
label1: 
	slt $t3, $t2, $t0 #if C<A T3=1 ELSE T3=0
	beqz $t3, label6 #if T3=0
	slt $t3, $t2, $t1 #if C<B T3=1 ELSE T3=0
	bnez $t3, label2 #if T3=1
	sw  $t1, 12($s0) #B
	sw  $t2, 16($s0) #C
	sw  $t0, 20($s0) #A
	b Exit 	
label2:
	sw  $t2, 12($s0) #C
	sw  $t1, 16($s0) #B
	sw  $t0, 20($s0) #A
	b Exit 	
label3: 
	sw  $t0, 12($s0) #A
	sw  $t1, 16($s0) #B
	sw  $t2, 20($s0) #C
	b Exit
label4: 
	sw  $t2, 12($s0) #C
	sw  $t0, 16($s0) #A
	sw  $t1, 20($s0) #B
	b Exit
label5: 
	sw  $t0, 12($s0) #A
	sw  $t2, 16($s0) #C
	sw  $t1, 20($s0) #B
	b Exit
	
label6: 
	sw  $t1, 12($s0) #B
	sw  $t0, 16($s0) #A
	sw  $t2, 20($s0) #C
	b Exit
		
Exit: 
	li $v0, 10
	syscall
	