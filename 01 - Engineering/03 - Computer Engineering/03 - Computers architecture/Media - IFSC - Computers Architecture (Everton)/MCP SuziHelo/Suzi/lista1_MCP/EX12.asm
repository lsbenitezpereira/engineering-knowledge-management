.data
array:		.word	1,3,3,4,5,1,2,3,5,6,1,2,6,7

.data 	0x10008000
vetor: 		.word array #endereco 0x00
tam_vetor: 	.word 14 #endereco 0x04
valor_contado:	.word 5 #endereco 0x08
num_elementos:	.word 0 #endereco 0x0c

.text
.globl main
main:
	lw	$t0, vetor
	lw	$t1, tam_vetor
	lw	$t2, valor_contado
	lw	$t4, num_elementos
	
loop:
	beqz	$t1, exit
	subi	$t1, $t1, 1
	lw	$t3,0($t0)
	sub 	$t3, $t3,$t2
	addi 	$t0,$t0,4
	beqz	$t3, label
	b 	loop
	
label:
	addi 	$t4, $t4, 1
	sw 	$t4, num_elementos
	bnez 	$t1, loop
	
exit: