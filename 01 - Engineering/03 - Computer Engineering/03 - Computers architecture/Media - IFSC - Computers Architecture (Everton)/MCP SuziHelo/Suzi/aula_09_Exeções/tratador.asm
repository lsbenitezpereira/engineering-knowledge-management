.data
array: .word 1, 3, 5
.text
#overflow
li	$t0, 0x7fffffff 	#$t0 = max_int
li	$t1, 1			# $t1 = 1
add	$t3, $t0, $t1		#detects overflow

#store
add 	$s0, $zero,  $gp
addi 	$s0, $s0, 5
sw  	$s0, 1($s0)

# Load
la 	$t1, array
lw 	$t1, 1($t0)

.kdata
_msg1: .asciiz "\n!! Ocorreu uma exce��o!!\n"
_msg2: .asciiz "\n cause: Arithmetic overflow exception!\n"			#code 12
_msg3: .asciiz "\n cause: Address error exception caused by store!\n" 	#code 5
_msg4: .asciiz "\n cause: Address error exception caused by load or instruction fetch!\n"	#code 4	
_msg5: .asciiz "\n endere�o invalido: "	

.align 2
_regs:	.space	20

.ktext 0x80000180
move	$k0, $at 		# $k0 = $at
la 	$k1, _regs	 	# $k1 = address of _regs
sw	$k0, 0($k1)		# save $at
sw	$v0, 4($k1) 		# save $v0
sw	$a0, 8($k1) 		# save $a0

la 	$a0, _msg1 		# $a0 = address of _msg1
li 	$v0, 4			# $v0 = service 4
syscall 	

# Determine cause of the exception
mfc0 	$k0, $13		# Get cause register from coprocessor 0
srl	$a0, $k0, 2		# Extract exception code field (bits 2-6)
andi	$a0, $a0, 0x1f

beq	$a0, 12, msg2
beq	$a0, 5, msg3
beq	$a0, 4, msg4

msg2:
la 	$a0, _msg2 		# $a0 = address of _msg2
li 	$v0, 4			# $v0 = service 4
syscall 
j	finish	

msg3:
la 	$a0, _msg3 		# $a0 = address of _msg3
li 	$v0, 4			# $v0 = service 4
syscall

la 	$a0, _msg5 		# $a0 = address of _msg3
li 	$v0, 4			# $v0 = service 4
syscall

mfc0 	$k0, $8			#Copia endere�o de memoria invalido
li 	$v0, 1			#Imprime endere�o em int
add 	$a0, $k0, $zero
syscall
j	finish

msg4:
la 	$a0, _msg4		# $a0 = address of _msg4
li 	$v0, 4			# $v0 = service 4
syscall

la 	$a0, _msg5 		# $a0 = address of _msg3
li 	$v0, 4			# $v0 = service 4
syscall

mfc0 	$k0, $8			#Copia endere�o de memoria invalido
li 	$v0, 1			#Imprime endere�o em int
add 	$a0, $k0, $zero
syscall

j	finish

finish:				
la 	$k1, _regs 		# $k1 = address of _regs
lw 	$at, 0($k1) 		# restore $at
lw 	$v0, 4($k1) 		# restore $v0
lw 	$a0, 8($k1) 		# restore $a0

mfc0 	$k0, $14 		# $k0 = EPC
addiu	$k0, $k0, 4		# Increment $k0 by 4
mtc0 	$k0, $14 		# EPC = point to next instruction
eret

mfc0 	$k0, $14 		# $k0 = EPC
addiu	$k0, $k0, 4		# Increment $k0 by 4
mtc0 	$k0, $14 		# EPC = point to next instruction
eret


