#ready bit: It is set to 1 by the keyboard controller when a key is pressed. 
#It is cleared automatically when the receiver data register is read
 
#CONS_RECEIVER_CONTROL :  0xffff0000
#CONS_RECEIVER_READY_MASK : 0x00000001
#CONS_RECEIVER_DATA : 0xffff0004
.data

.text
main:
	jal	getchar
	move	$a1, $v0
		
	jal	print_char
	b	main

getchar:
	lw	$t0, 0xffff0000
	andi	$t0, $t0, 0x00000001	# Isolate ready bit
	beqz	$t0, getchar	#se o ready � 0 ele espera para pegar um caracter
	
	lbu	$v0, 0xffff0004   #get data
	
	jr	$ra

print_char:
	lw	$t0, 0xffff0008
	andi	$t0, $t0, 0x00000001	# Isolate ready bit
	beqz	$t0, print_char
	
	sb	$a1, 0xffff000c			#salva os dados e mostra na tela
	
	jr   	$ra

