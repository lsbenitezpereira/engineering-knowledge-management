.data
array: .word 10, 5, 11, 3, 20

.text
.globl main	
main:
	la	$a0, array
	li	$a1, 5		#size
	jal 	bubble			#chama o procedimento
	j	end
bubble:
	li	$t0, 0			#i=0
	subi	$t1, $a1, 1		#k = size-1
	
for1:	bge	$t0, $a1, end_for1	#i>=size
	li	$t2, 0			#j=0
for2:	
	bge	$t2, $t1, end_for2	#j>=k
	
	sll	$t3, $t2, 2		# t3 = j*4
	add	$t3, $a0, $t3		#v[j]
	lw	$t4, 0($t3)		#t4 = v[j]
	
	addi	$t5, $t2, 1		#t5 = j+1
	
	sll	$t5, $t5, 2
	add	$t5, $t5, $a0		# t5 =  v[j+1]
	lw	$t6, 0($t5)		#t6 = v[j+1]
	
	ble	$t4, $t6, end_if
	sw	$t6, 0($t3)		#v[j]=v[j+1]
	sw	$t4, 0($t5)		#v[j+1]=aux
	addi	$t2, $t2, 1		#j++
	j	for2
	
end_if:
	addi	$t2, $t2, 1		#j++
	j	for2
end_for2:
	subi	$t1, $t1, 1		#k--
	addi	$t0, $t0, 1		#i++
	j	for1
end_for1:
	jr	$ra
end:
	
		
