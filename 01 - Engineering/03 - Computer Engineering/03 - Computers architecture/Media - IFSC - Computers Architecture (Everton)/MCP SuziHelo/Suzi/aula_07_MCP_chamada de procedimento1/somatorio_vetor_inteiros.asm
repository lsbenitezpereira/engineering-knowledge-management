# Calcula o somatório de um vetor de inteiros
#int sum(int* v, int size) {
#	int sum = 0;
#	while(size--)
#		sum += *v++;
#	return sum;
#}
.data
array: .word 1, 2, 3, 1

.data 0x10008000
array_ptr: .word array
tamanho: .word 4

.text
.globl main	
main:
	lw	$a0, array_ptr
	lw	$a1, tamanho	
	jal 	sumofarray	#chama o procedimento
	move	$s0, $v0	#pega o resultado
	j	end
sumofarray:
	li	$v0, 0
while:
	beqz	$a1, end_while
	lw	$t2, 0($a0)
	add	$v0, $t2, $v0
	addi 	$a0,$a0,4
	subi	$a1, $a1, 1
	j	while

end_while:
	jr	$ra		#return resultado
end:
	
	
	
	