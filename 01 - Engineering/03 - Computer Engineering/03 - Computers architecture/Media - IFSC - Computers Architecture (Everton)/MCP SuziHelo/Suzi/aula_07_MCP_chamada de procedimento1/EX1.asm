.text
.globl main	
main:
	li	$a0, 2		#a
	li	$a1, 2		#b
	jal 	sumofsquare	#chama o procedimento
	move	$s0, $v0	#pega o resultado

sumofsquare:
	mul	$t0, $a0, $a0	#a*a
	mul	$t1, $a1, $a1	#b*b
	add	$v0, $t0, $t1	#a*a+b*b
	jr	$ra		#return resultado
	