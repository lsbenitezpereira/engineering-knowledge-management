# Memcopy
#void memcpy(char * src, char * dst, int bytes){
#	while(bytes--)
#		*dst++ = *src++;
#	}

.data
fonte:		.ascii	"ol"
destino:	.ascii	"--"
.text
.globl main
main:
	la	$a0, fonte
	la	$a1, destino
	li	$a2, 2
	jal	memcpy
	j	end
memcpy:
	beqz	$a2, end_function
	subi	$a2, $a2, 1	#byte--
	lb	$t0, 0($a0)
	sb      $t0, 0($a1)
	addi    $a0, $a0, 1
	addi    $a1, $a1, 1
	j       memcpy
end_function:
	jr	$ra	
end:
	
