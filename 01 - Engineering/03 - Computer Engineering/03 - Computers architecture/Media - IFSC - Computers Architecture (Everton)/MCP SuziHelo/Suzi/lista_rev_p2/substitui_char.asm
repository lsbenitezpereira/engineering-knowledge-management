.data 0x10010000
string: .asciiz "sobstitoi"
x: .asciiz "o"
y: .asciiz "u"

.text
main:
	la	$a0, string
	la	$a1, x
	la	$a2, y
	
	jal	substitui
	
	la	$a0, string
	li	$v0, 4
	syscall
	
	
	li	$v0, 10
	syscall
	
substitui:
	li	$t0, 0
	lb	$s1, 0($a1)
	lb	$s2, 0($a2)
for:
	lb	$s0, 0($a0)
	beqz	$s0, end_for
	beq	$s0, $s1, if
	addi	$t0, $t0, 1
	addi	$a0, $a0, 1
	j	for
	
if:
	sb	$s2, 0($a0)
	addi	$t0, $t0, 1
	addi	$a0, $a0, 1
	j	for
	
end_for:
	jr	$ra