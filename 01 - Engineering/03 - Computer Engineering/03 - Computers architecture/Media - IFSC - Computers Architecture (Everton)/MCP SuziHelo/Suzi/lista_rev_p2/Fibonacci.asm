
#-----------------------------------	
#a0 Fibonacci	 32(sp)	
#---------------32(sp)

#	espaco	28(sp)
#s1		24(sp)	
#s0		20(sp)	
#---------------
#ra		16(sp)
#---------------
#a3		12(sp)
#a2		8(sp)
#a1		4(sp)
#a0		0(sp)--->n


.text
main:

	li	$a0, 5

	jal	fibonacci
	move	$s0, $v0
	
	li	$v0, 10
	syscall				
	
fibonacci:
	addiu	$sp, $sp, -32
	sw	$ra, 16($sp)
	sw	$a0, 0($sp)
	sw	$s0, 20($sp)
	sw	$s1, 24($sp)
	
	bge	$a0, 2, fibo
	move	$v0, $a0
	j	end_fibonacci
fibo:
	addi	$a0, $a0, -1
	jal	fibonacci
	move	$s0, $v0
	
	lw	$a0, 0($sp)
	addi	$a0, $a0, -2
	jal	fibonacci
	move	$s1, $v0
	
	add	$v0, $s0, $s1
	
end_fibonacci:	
	lw	$ra, 16($sp)
	lw	$a0, 0($sp)
	lw	$s0, 20($sp)
	lw	$s1, 24($sp)
	addiu	$sp, $sp, 32
	jr	$ra
