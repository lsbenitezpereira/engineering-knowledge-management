
.text
main:
	addiu	$sp, $sp, -16
	li	$a0, 1		#n
	li	$a1, 0		#s
	jal	combinacao
	move	$t0, $v0
	
	addiu	$sp, $sp, 16
	li	$v0, 10
	syscall
	
combinacao:
	addiu	$sp, $sp, -32
	sw	$a0, 32($sp)
	sw	$a1, 36($sp)
	sw	$ra, 16($sp)
	sw	$s0, 20($sp)
	sw	$s1, 24($sp)
	
	ble	$a1, $a0, if2
	li	$v1, 1
	j	end_comb
if2:
	slt	$t0, $a0, $zero
	slt	$t1, $a1, $zero
	or	$t0, $t0, $t1
	beqz	$t0, if3
	li	$v1, 2
	j	end_comb
if3:
	bne	$a0, $a1, if4
	li	$v1, 3
	#j	end_comb
if4:
	seq	$t0, $a0, $zero
	seq	$t1, $a1, $zero
	or	$t0, $t0, $t1
	beqz	$t0, comb
	li	$v1, 4
	j	end_comb
comb:	
	#lw	$a0, 0($sp)
	jal	fatorial
	move	$s0, $v0
	
	lw	$a1, 36($sp)
	move	$a0, $a1
	jal	fatorial
	move	$s1, $v0
	
	lw	$a0, 32($sp)
	lw	$a1, 36($sp)
	sub	$a0, $a0, $a1
	jal	fatorial
	
	mul	$v0, $s1, $v0
	div	$v0, $s0, $v0
end_comb:
	lw	$ra, 16($sp)
	lw	$s0, 20($sp)
	lw	$s1, 24($sp)
	addiu	$sp, $sp, 32
	jr	$ra
	
fatorial:
#-----------------------------------	
#a0 (fatorial)  24(sp)	
#---------------24(sp)

#	espaco	20(sp)
#---------------
#ra		16(sp)
#---------------
#a3		12(sp)
#a2		8(sp)
#a1		4(sp)
#a0		0(sp)
	addiu	$sp, $sp, -24	#adicionar meu quadro de pilha
	sw	$ra, 16($sp)
	#implementacao da fatorial
	li	$v0, 1
	beq	$a0, 1, fatorial_end
	sw	$a0, 24($sp)
	addiu	$a0, $a0, -1
	jal	fatorial
	lw	$a0, 24($sp)
	mul	$v0, $a0, $v0	
fatorial_end:
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra

		 