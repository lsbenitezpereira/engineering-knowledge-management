.data
vector: .word	3, 1, 9, 5, 20

#-----------------------------------	
#a0 		  24(sp)	
#---------------24(sp)

#	espaco	20(sp)
#---------------
#ra		16(sp)
#---------------
#a3		12(sp)
#a2		8(sp)--->ub
#a1		4(sp)--->lb
#a0		0(sp)--->*x
#-------------------------------------

.text
.globl main
main:
 	addiu	$sp, $sp, -24
	la	$a0, vector		#*x
	sw	$a0, 0($sp)
	
	li	$a1, 0			#lb
	sw	$a1, 4($sp)
	li	$a2, 4			#ub
	sw	$a2, 8($sp)

 	jal	quicksort
 	
	addiu	$sp, $sp, 24
	j	end
	
particiona:
	sll	$t0, $a1, 2		# t0 =  4 * lb
	add	$t0, $t0, $a0		# t0 = & x[lb*4]
	lw	$t1, 0($t0)		# t1 = a = x[lb]	
	
	move	$v0, $a2		# v0 = up = ub
	move	$t3, $a1		# t3 = down = lb	
while1:
	bge	$t3, $v0, end_while
while2:
	sll	$t4, $t3, 2		# t4 = 4 * down
	add	$t4, $t4, $a0		# t4 = & x [4 * down]
	lw	$t5, 0($t4)		# t5 = x[down]
	
	sle	$t6, $t5, $t1		# t6 = (x[down] <= a)
	slt	$t7, $t3, $a2		# t7 = (down < ub)
	and	$t6, $t6, $t7		# t6 = ((x[down] <= a) && (down < ub))
	beqz	$t6, while3		# t6 != 0 ---> while3
	addi	$t3, $t3, 1		# down++
	j	while2
while3:
	sll	$t8, $v0, 2		# t8 = 4 * up
	add	$t8, $t8, $a0		# t8 = & x[4 * up]
	lw	$t9, 0($t8)		# t9 = x[up]
	ble	$t9, $t1, if		# x[up] <= a----> if
	subi	$v0, $v0, 1		# up--
	j	while3
if:	
	bge	$t3, $v0, while1
	sw	$t5, 0($t8)		# x[up] = x[down]
	sw	$t9, 0($t4)		# x[down] = x[up]
	j	while1
end_while:
	sw	$t9, 0($t0)		# x[lb] = x[up]
	sw	$t1, 0($t8)		# x[up] = a
	jr	$ra	
	
quicksort:
	bge	$a1, $a2, end_quicksort
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$ra, 16($sp)
	sw	$s0, 20($sp)
	
	jal	particiona
	move	$s0, $v0
	
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	subi	$a2, $s0, 1
	jal	quicksort
	
	lw	$a0, 0($sp)
	addi	$a1, $s0, 1
	lw	$a2, 8($sp)
	jal	quicksort
	
	lw	$ra, 16($sp)
	lw	$s0, 20($sp)
	addiu	$sp, $sp, 24
	
end_quicksort:
	jr	$ra

end:
	li	$v0, 10
	syscall		
