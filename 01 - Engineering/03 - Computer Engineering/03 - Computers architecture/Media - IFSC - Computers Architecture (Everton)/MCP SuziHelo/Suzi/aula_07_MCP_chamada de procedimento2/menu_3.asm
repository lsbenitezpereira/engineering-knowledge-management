.data
vetor: .word 0:1024 #inicializados com zero
menu1: .asciiz "1) gera numeros randomicos \n2) imprimir o array \n3) imprimir crescente \n4) imprimir decrescente \n5) somatorio \n6) media aritmetica \n7) encerra o programa\n"
tamanho: .asciiz "1) insira o tamanho desejado de elementos:\n"
linha: .asciiz "\n"

.text
menu:
	li	$v0, 4
	la	$a0, linha
	syscall
	
	#imprimir o menu
	li	$v0, 4
	la	$a0, menu1
	syscall 
	
	#ler o numero desejado
	li	$v0, 5
	syscall 
	
	move	$s1, $v0
	beq $s1, 1, inicializa
	beq $s1, 2, print_array
	beq $s1, 3, print_crescente
	beq $s1, 4, print_decrescente
	beq $s1, 5, somatorio
	beq $s1, 6, media
	beq $s1, 7, encerra
#-----------------------------------------------------------	
inicializa:
	#print string of quantity
	li	$v0, 4
	la	$a0, tamanho	
	syscall 
	
	#ler a quantidade de elementos
	li	$v0, 5
	syscall 
	move	$s3, $v0
	la	$s0, vetor
#-----------------------------------------------------------
random_numbers:
	move	$s2, $s0
	move	$t2, $s3
loop_random:
	beqz	$t2, menu		#if (qtd == 0)
	
	li	$v0, 42
	la	$a1, 100
	syscall

	sw	$a0, 0($s2)
	addi	$s2, $s2, 4
	subi	$t2, $t2, 1		# t2 = qtd --	
	j	loop_random
#-----------------------------------------------------------
print_array:
	move	$s2, $s0
	move	$t2, $s3
loop_print:
	beqz	$t2, menu
	lw	$t1, 0($s2)
	li	$v0, 1
	add	$a0, $t1, $zero
	syscall
	
	li	$a0, 32
	li	$v0, 11
	syscall
	
	addi	$s2, $s2, 4
	subi	$t2, $t2, 1
	j	loop_print 
#-----------------------------------------------------------	
print_crescente:
	move	$t2, $s3
	addiu	$sp, $sp, -24
	la	$a0, vetor		#*x
	sw	$a0, 0($sp)
	
	li	$a1, 0			#lb
	sw	$a1, 4($sp)
	addi	$t3, $t2, -1
	move	$a2, $t3			#ub
	sw	$a2, 8($sp)

 	jal	quicksort
 	addiu	$sp, $sp, 24
 	j	print_array
#-----------------------------------------------------------
print_decrescente:
	move	$t2, $s3
	addiu	$sp, $sp, -24
	la	$a0, vetor		#*x
	sw	$a0, 0($sp)
	
	li	$a1, 0			#lb
	sw	$a1, 4($sp)
	addi	$t3, $t2, -1
	move	$a2, $t3			#ub
	sw	$a2, 8($sp)

 	jal	quicksort
 	addiu	$sp, $sp, 24
print_array_dec:
	move	$s2, $s0
	move	$t2, $s3
	mul	$t3, $t2, 4		#pegar o ultimo elemento
	addi	$t3, $t3, -4
	add	$s2, $s2, $t3
loop_print_dec:
	beqz	$t2, menu
	lw	$t1, 0($s2)
	li	$v0, 1
	add	$a0, $t1, $zero
	syscall
	
	li	$a0, 32
	li	$v0, 11
	syscall
	
	addi	$s2, $s2, -4
	subi	$t2, $t2, 1
	j	loop_print_dec	
	
#-----------------------------------------------------------
somatorio:
	move	$s2, $s0
	move	$t2, $s3
	li	$t4, 0
loop_soma:
	beqz	$t2, end_soma
	lw	$t3, 0($s2)
	add	$t4, $t3, $t4
	subi	$t2, $t2, 1
	addi	$s2, $s2, 4
	j	loop_soma
end_soma:
	li	$v0, 1
	add	$a0, $t4, $zero
	syscall
	
	j	menu
#-----------------------------------------------------------
media:
	move	$s2, $s0
	move	$t2, $s3
	li	$t5, 0
media_loop:
	beqz	$t2, end_media
	lw	$t3, 0($s2)
	add	$t5, $t3, $t5
	subi	$t2, $t2, 1
	addi	$s2, $s2, 4
	j	media_loop
end_media:
	div	$t5, $t5, $s3
	
	li	$v0, 1
	add	$a0, $t5, $zero
	syscall 
	
	j	menu
#-----------------------------------------------------------	
encerra:
	li	$v0, 10
	syscall
#-----------------------------------------------------------
particiona:
	sll	$t0, $a1, 2		# t0 =  4 * lb
	add	$t0, $t0, $a0		# t0 = & x[lb*4]
	lw	$t1, 0($t0)		# t1 = a = x[lb]	
	
	move	$v0, $a2		# v0 = up = ub
	move	$t3, $a1		# t3 = down = lb	
while1:
	bge	$t3, $v0, end_while
while2:
	sll	$t4, $t3, 2		# t4 = 4 * down
	add	$t4, $t4, $a0		# t4 = & x [4 * down]
	lw	$t5, 0($t4)		# t5 = x[down]
	
	sle	$t6, $t5, $t1		# t6 = (x[down] <= a)
	slt	$t7, $t3, $a2		# t7 = (down < ub)
	and	$t6, $t6, $t7		# t6 = ((x[down] <= a) && (down < ub))
	beqz	$t6, while3		# t6 != 0 ---> while3
	addi	$t3, $t3, 1		# down++
	j	while2
while3:
	sll	$t8, $v0, 2		# t8 = 4 * up
	add	$t8, $t8, $a0		# t8 = & x[4 * up]
	lw	$t9, 0($t8)		# t9 = x[up]
	ble	$t9, $t1, if		# x[up] <= a----> if
	subi	$v0, $v0, 1		# up--
	j	while3
if:	
	bge	$t3, $v0, while1
	sw	$t5, 0($t8)		# x[up] = x[down]
	sw	$t9, 0($t4)		# x[down] = x[up]
	j	while1
end_while:
	sw	$t9, 0($t0)		# x[lb] = x[up]
	sw	$t1, 0($t8)		# x[up] = a
	jr	$ra	
	
quicksort:
	bge	$a1, $a2, end_quicksort
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$ra, 16($sp)
	sw	$s0, 20($sp)
	
	jal	particiona
	move	$s0, $v0
	
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	subi	$a2, $s0, 1
	jal	quicksort
	
	lw	$a0, 0($sp)
	addi	$a1, $s0, 1
	lw	$a2, 8($sp)
	jal	quicksort
	
	lw	$ra, 16($sp)
	lw	$s0, 20($sp)
	addiu	$sp, $sp, 24
	
end_quicksort:
	jr	$ra
