.text
.globl main
main:
	add 	$s0, $zero, $gp
	lb  	$t0, 0($s0) #a
	lb	$t1, 4($s0) #b
	lb	$t2, 8($s0) #c
	lb	$t3, 12($s0) #d
	seq	$t4, $t2, $t3 #c==d
	sgt	$t5, $t0, $t1 #a>b
	and	$t4, $t4, $t5 
	beqz	$t4, else
	div	$t2, $t3 #c/d
	mflo	$t4
	mul	$t5, $1, $t0 #b*a
	add	$t0, $t4, $t5
	j	end
else:
	or	$t1, $1, $t2
	and	$t0, $t0, $t1
end:	
	addi	$t0, $t0, 3 #a+=3
	