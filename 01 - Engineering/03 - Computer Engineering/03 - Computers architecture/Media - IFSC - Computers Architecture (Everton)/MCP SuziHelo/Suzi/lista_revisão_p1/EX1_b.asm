.text
.globl main
main:
	add 	$s0, $zero, $gp
	lb  	$t0, 0($s0) #a
	lb	$t1, 4($s0) #I
	lb	$t2, 8($s0) #N
	li	$t0, 0
	li	$t1, 0
	blt	$t1, $t2, for
	j	end
	
for:
	xori	$t3, $t1, 1
	add	$t0, $t0, $t3
	addi	$t1, $t1, 1
end:
	
	