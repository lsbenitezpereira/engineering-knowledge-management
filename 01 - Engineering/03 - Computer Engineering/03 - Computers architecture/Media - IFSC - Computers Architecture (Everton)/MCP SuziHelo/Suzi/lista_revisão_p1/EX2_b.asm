.text
.globl main
main:
	add 	$s0, $zero, $gp
	lb  	$t1, 0($s0) #q
	li	$t1, 10 #q=10
	beqz	$t1, end
	mul	$t2, $t0, $t1
	add	$t0, $t0, $t2
	subi	$t1, $t1, 1
end: