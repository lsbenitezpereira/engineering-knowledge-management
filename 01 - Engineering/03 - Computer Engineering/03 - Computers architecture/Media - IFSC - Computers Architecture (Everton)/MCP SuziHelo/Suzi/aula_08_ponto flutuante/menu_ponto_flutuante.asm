.data
vetor: .word 0:1024 #inicializados com zero
menu1: .asciiz "1) exibir acumulador \n2) zerar acumulador \n3) realizar soma \n4) realizar subtração \n5) realizar divisao \n6) realizar multiplicação \n7) encerra o programa\n"
linha: .asciiz "\n"
insira: .asciiz "insira um valor desejado\n"

.text
menu:
	li	$v0, 4
	la	$a0, linha
	syscall
	
	#imprimir o menu
	li	$v0, 4
	la	$a0, menu1
	syscall 
	
	#ler o numero desejado
	li	$v0, 5
	syscall 
	
	move	$s1, $v0
	beq $s1, 1, exibir_acumulador
	beq $s1, 2, zera_acumulador
	beq $s1, 3, soma
	beq $s1, 4, subtracao
	beq $s1, 5, divisao
	beq $s1, 6, multiplicacao
	beq $s1, 7, encerra
#-----------------------------------------------------------
exibir_acumulador:
	#print o elemento
	li	$v0, 2
	syscall

	j	menu
#-----------------------------------------------------------	
zera_acumulador:
	sub.s	$f12, $f12, $f12
	j	menu
#-----------------------------------------------------------	
soma:
	#print string of value
	li	$v0, 4
	la	$a0, insira	
	syscall
	
	#ler um valor
	li	$v0, 6
	syscall
	
	add.s	$f12, $f0, $f12

	j	exibir_acumulador
#-----------------------------------------------------------		
subtracao:
	#print string of value
	li	$v0, 4
	la	$a0, insira	
	syscall
	
	#ler um valor
	li	$v0, 6
	syscall
	
	sub.s	$f12, $f12, $f0
	
	j	exibir_acumulador
#-----------------------------------------------------------	
divisao:
	#print string of value
	li	$v0, 4
	la	$a0, insira	
	syscall
	
	#ler um valor
	li	$v0, 6
	syscall
	
	div.s 	$f12, $f12, $f0
	
	j	exibir_acumulador
#-----------------------------------------------------------	
multiplicacao:
	#print string of value
	li	$v0, 4
	la	$a0, insira	
	syscall
	
	#ler um valor
	li	$v0, 6
	syscall
	
	mul.s 	$f12, $f12, $f0
	
	j	exibir_acumulador
#-----------------------------------------------------------	
encerra:
	li	$v0, 10
	syscall
#-----------------------------------------------------------