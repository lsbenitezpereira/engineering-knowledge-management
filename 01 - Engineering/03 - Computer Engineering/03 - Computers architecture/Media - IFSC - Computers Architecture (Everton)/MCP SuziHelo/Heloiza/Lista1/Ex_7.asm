.text
.globl main
main:
	add $s0, $zero, $gp
	lb $t0, 0($s0)
	lb $t1, 4($s0)
	lb $t2, 8($s0)		
	slt $t3, $t0, $t1	#$t0 < $t1, $t3 = 1;
	bnez $t3, label1	#$t0 < $t1
	slt $t5, $t1, $t2	#$t1 < $t2, $t5 = 1;
	bnez $t5, label2	#$t1 > $t2;
	sb $t2, 12($s0)
	slt $t4, $t0, $t1	#$t0 < $t1;
	bnez $t4, label3
	sb $t0, 20($s0)
	sb $t1, 16($s0)
	sb $t2, 12($s0)
	b exit
	
label1:
	slt $t4, $t0, $t2	#$t0 < $t2
	bnez $t4, store1	#$t0 < $t2
	sb $t2, 16($s0)		#t2 > $t0
	sb $t0, 12($s0)
	sb $t1, 20($s0)
	b exit

label2:
	sb $t1, 12($s0)
	slt $t5, $t0, $t2	#$t0 < $t2
	bnez $t5, store3
	sb $t2, 16($s0)
	sb $t0, 20($s0)

label3:
	sb $t0, 16($s0)
	sb $t1, 20($s0)
	sb $t2, 12($s0)
	b exit
	
store1:
	sb	 $t0, 12($s0)
	slt	 $t4, $t1, $t2	#$t1 < $t2, $t4 = 1;
	bnez	 $t4, store2
	sb	 $t1, 20($s0)
	sb	 $t2, 16($s0)
	b	 exit

store2:
	sb	 $t1, 16($s0)
	sb	 $t2, 20($s0)
	sb	 $t0, 12($s0)
	b	 exit

store3:
	sb	 $t0, 16($s0)
	sb	 $t2, 20($s0)
	sb	 $t1, 12($s0)
	b	 exit
	
exit:
