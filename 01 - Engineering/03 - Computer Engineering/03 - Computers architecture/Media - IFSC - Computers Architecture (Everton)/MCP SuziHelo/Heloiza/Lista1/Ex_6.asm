.text
.globl main
main:
	add	 $s0, $zero, $gp
	lb	 $t0, 0($s0)
	lb	 $t1, 4($s0)
	slt	 $t2, $t0, $t1
	beqz	 $t2, label
	bnez	 $t2, label1
	
label:
	sw	 $t0, 8($s0)
	b	 exit

label1:
	sw	 $t1, 8($s0)
	b	 exit

exit:
	
