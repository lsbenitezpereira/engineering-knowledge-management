.data
fonte: .word 1, 2, 3, 4, 5
destino: .space 20

.data 0x10008000
ptr_fonte: .word fonte
ptr_destino: .word destino
tamanho: .word 5

.text
.globl main
main:
	lw 	$t0, ptr_fonte
	lw 	$t1, ptr_destino
	lw 	$t2, tamanho
loop:
	beqz 	$t2, exit
	lw 	$t3, 0($t0)
	sw 	$t3, 0($t1)
	addi	$t0, $t0, 4
	addi	$t1, $t1, 4
	subi	$t2, $t2, 1
	j	loop
exit: