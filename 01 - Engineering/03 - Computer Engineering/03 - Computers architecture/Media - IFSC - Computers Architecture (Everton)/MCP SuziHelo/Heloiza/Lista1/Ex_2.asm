.text
.globl main
main:
	add $s0, $zero, $gp
	lbu $t0, 0($s0)
	lbu $t1, 4($s0)
	lbu $t2, 8($s0)
	add $t0, $t0, $t1
	add $t0, $t0, $t2
	sw $t0, 12($s0)