.text
.globl main

main:
	add	 $s0, $zero, $gp
	li	 $t3, 3
	li	 $t4, 1
	lb	 $t0, 0($s0)
	lb	 $t1, 4($s0)
	lb	 $t2, 8($s0)
	add	 $t0, $t0, $t1
	add	 $t0, $t0, $t2
	div	 $t0, $t3
	mflo	 $t0
	slti	 $t1, $t0, 7	#t0 < t4
	beqz	 $t1, label
	sb	 $t4, 16($s0)
	b	 exit
	
label:
	sb	 $t4, 12($s0)	
	b	 exit
exit: