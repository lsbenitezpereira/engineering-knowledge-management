.data 
array: .word 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 1

.data 0x10008000
ptr_array: .word array
size: .word 12
search: .word 1
num_elementos: .word 0

.text
.globl main
main:
	add	$s0, $zero, $gp
	lw	$t0, ptr_array
	lw	$t1, size
	lw	$t2, search
	li	$t4, 0
	
loop:	beqz	$t1, exit
	subi	$t1, $t1, 1
	lw	$t3, 0($t0)
	addi	$t0, $t0, 4
	sub	$t3, $t3, $t2
	beqz	$t3, label
	b	loop
	
label:
	addi	 $t4, $t4, 1
	sw	 $t4, num_elementos
	bnez	 $t1, loop
exit:
	