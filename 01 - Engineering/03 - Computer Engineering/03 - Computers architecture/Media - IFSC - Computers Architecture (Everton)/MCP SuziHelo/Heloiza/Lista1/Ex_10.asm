.text
.globl main
main:
	add 	$s0, $zero, $gp
	lb 	$t0, 0($s0)
	lb 	$t1, 4($s0)
	multu 	$t0, $t1
	mflo	$t0
	sb 	$t0, 8($s0)
	