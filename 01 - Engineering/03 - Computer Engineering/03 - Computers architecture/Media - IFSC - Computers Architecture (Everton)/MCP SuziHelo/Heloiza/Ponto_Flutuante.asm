 .data
 valores: .asciiz "Entre com um valor para a opera��o desejada:"
 linha: .asciiz "\n"
 menu1: .asciiz "Menu:\n 1 - Exibir Acumulador\n 2 - Zerar Acumulador\n 3 - Realizar Soma\n 4 - Realizar Subtra��o\n 5 - Realizar Divis�o\n 6 - Realizar Multiplica��o\n 7 - Sair do programa\n"
 
 .text
 menu:
	
	li	$v0, 4
	la	$a0, linha
	syscall
	li 	$v0, 4					
	la 	$a0, menu1				
	syscall
	li 	$v0, 5 						
	syscall	
	move	$t0, $v0		
	beq 	$t0, 1, exibir
	beq 	$t0, 2, zerar
	beq 	$t0, 3, soma
	beq 	$t0, 4, subtracao
	beq 	$t0, 5, divisao
	beq 	$t0, 6, multiplicacao
	beq 	$t0, 7, sair
	
exibir:
	li	$v0, 2
	syscall
	j	menu
zerar:
	sub.s	$f12, $f12, $f12
	j	menu
soma:
	li	$v0, 4
	la	$a0, valores
	syscall
	li	$v0, 6		#le acumulador e aramazena em $f12
	syscall
	add.s	$f12, $f0, $f12
	j	exibir	
	
subtracao:
	li	$v0, 4
	la	$a0, valores
	syscall
	li	$v0, 6
	syscall
	sub.s	$f12, $f12, $f0
	j	exibir	
	
divisao:
	li	$v0, 4
	la	$a0, valores
	syscall
	li	$v0, 6
	syscall
	div.s	$f12, $f12, $f0
	j	exibir
multiplicacao:
	li	$v0, 4
	la	$a0, valores
	syscall
	li	$v0, 6
	syscall
	mul.s	$f12, $f0, $f12
	j	exibir
sair:
	li	$v0, 10
	syscall