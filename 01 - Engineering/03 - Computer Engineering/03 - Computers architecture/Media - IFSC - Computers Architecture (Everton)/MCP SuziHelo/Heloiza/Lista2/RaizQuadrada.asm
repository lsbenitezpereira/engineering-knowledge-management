#// Calcula a raiz quadrada (inteiro)
#  int isqrt(int num) {
#	int res = 0;
#	int bit = 1 << 30;
#	while (bit > num)
#		bit >>= 2;
#	while (bit != 0) {
#		if (num >= res + bit) {
#			num -= res + bit;
#			res = (res >> 1) + bit;
#		}
#		else
#			res >>= 1;
#			bit >>= 2;
#		}
#	return res;
#	}
#res = v0, bit = t0, num = a0

.text
.globl main
main:
	li	$a0, 16		#num
	jal	sqrt
	move	$s0, $v0
	j	exit

sqrt:
	li	$v0, 0		#res
	li	$t0, 1		#bit
	sll	$t0, $t0, 30
while1:	
	ble	$t0, $a0, while2
	srl	$t0, $t0, 2
	b 	while1
while2:
	beqz	$t0, end
	add	$t1, $v0, $t0
	blt	$a0, $t1, else
	sub	$a0, $a0, $t1
	srl	$t2, $v0, 1
	add	$v0, $t2, $t0
	srl	$t0, $t0, 2
	b 	while2
else:
	srl	$v0, $v0, 1
	srl	$t0, $t0, 2
	b 	while2	
	
end:	
	jr	$ra
exit:
