# Memcopy
#   void memcpy(char * src, char * dst, int bytes){
# 	while(bytes--)
# 	*dst++ = *src++;
#   }

.data
source:  .ascii "91"
destiny: .ascii "  "

.text
main:
	la	$a0, source
	la	$a1, destiny
	li	$a2, 2
	li	$v0, 0			# bytes copiados
	jal	memcoppy	
	j	exit

memcoppy:
	beqz	$a2, end_while
	subi	$a2, $a2, 1		# bytes--
	lb	$t0, 0($a0)
	sb	$t0, 0($a1)
	addi	$a0, $a0, 1
	addi	$a1, $a1, 1
	addi	$v0, $v0, 1
	b	memcoppy

	
end_while:
	jr	$ra
exit: