# Algoritmo de ordena��o BubbleSort
#  void bubble(int* v, int size) {
#	int i;			i = t0
#	int j;			j = t1
#	int aux;		aux = t2
#	int k = size - 1 ;	k = t3
#	for(i = 0; i < size; i++) {
#		for(j = 0; j < k; j++) {
#			if(v[j] > v[j+1]) {
#				aux = v[j];
#				v[j] = v[j+1];
#				v[j+1]=aux;
#			}
#		}
#		k--;
#	}
 # }


.data
array:	.word 16, 09, 17, 3

.text
main:
	la	$a0, array
	li	$a1, 4			#$a1 = tamanaho
	jal	BubbleSort
	#move	$s0, $v0
	j	exit
	
BubbleSort:
	li	$t0, 0			# $t0 = i
	li	$t1, 0
	addi	$t3, $a1, -1		# $t3 = k = tamanho - 1
	
	
for1:	bge	$t0, $a1, end_for	# i >= tamanho
	li	$t1, 0			# $t1 = j
	
	
for2:	bge	$t1, $t3, end_for2	# j >= k
	sll	$t7, $t1, 2		# $t2 = j * 4
	
	add	$t7, $t7, $a0		# $t7 = v + (j * 4)
	lw	$t4, 0($t7)		# $t4 = v[j]
	
	addi	$t5, $t1, 1		# $t5 = [j+1]
	sll	$t8, $t5, 2		# $t8 = (j+1)*4
	add	$t8, $t8, $a0
	lw	$t2, 0($t8)		# $t2 = v[j+1]
	
	ble	$t4, $t2, end_if
	sw	$t2, 0($t7)		# v[j] = v[j+1]
	sw	$t4, 0($t8)		# v[j+1] = v[j]

	b	end_if		

end_if:
	addi	$t1, $t1, 1
	b	for2
	
end_for2:
	subi	$t3, $t3, 1
	addi	$t0, $t0, 1
	b	for1
	
end_for:
	jalr	$ra
exit:

