 .text
 main:
 	jal 	getchar
 	move	$a1, $v0
 	
 	jal	print_char
 	b	main
 	
 getchar:
 	lw	$t0, 0xffff0000		# Controle Teclado
 	andi	$t0, $t0, 0x00000001	# Isola Ready bit	
 	beqz	$t0, getchar		# Ready = 0, espera para pegar um caracter
 	lbu	$v0, 0xffff0004		# Data
 	jr	$ra
 	
 print_char:
 	lw	$t1, 0xffff0008		# Controle Display
 	andi	$t1, $t1, 0x00000001
 	beqz	$t1, print_char		# Espera Comunicação
 	sb	$a1, 0xffff000c		# Salva os dados e mostra na tela
 	jr	$ra