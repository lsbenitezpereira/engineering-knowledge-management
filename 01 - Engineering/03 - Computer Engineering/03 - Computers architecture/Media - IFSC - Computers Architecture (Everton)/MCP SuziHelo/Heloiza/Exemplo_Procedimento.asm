#Defini��o do Procedimento
#Pseudo c�digo:
# int sumOfSquare(int a int b){
#	return a*a + b*b;
# }

#Uso do Procedimento
#Pseudo c�digo:
# int main(){
#	int c;
#	c = sumOfSquare(3,5);
# }
# c => $s0

.text
.globl main
main:
	li	$a0, 3
	li	$a1, 5
	jal	sumOfSquare
	move	$s0, $v0
	#add	$s0, $zero, $v0
	j 	exit
	
sumOfSquare:
	mul	$t0, $a0, $a0
	mul	$t1, $a1, $a1
	add	$v0, $t0, $t1
	jr	$ra
	
exit:
	