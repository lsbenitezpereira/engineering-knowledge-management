Ex:1
1 - Qual o valor final armazenado no registrador $t0?

R: $T0 = 0x00000040

2 - Qual o endere�o do byte menos significativo (byte 0) da palavra armazenada em 0x10008000 e o valor final
nele armazenado?

R:Endere�o: 0x10008000 Valor: 0x40

3 - Qual o endere�o do byte 1 da palavra armazenada em 0x10008000 e o valor final nele armazenado?

R: Endere�o: 0x10008001 Valor: 0x37



Ex2:
1 - Preencha a tabela abaixo com os valores observados ao final da execu��o do programa.

 Endere�o  	Valor    Endere�o    Valor 
0x10008000 	0xcc	0x10008004   0xaa
0x10008001 	0xdd	0x10008005   0x99
0x10008002 	0xee	0x10008006   0x88
0x10008003 	0x0f	0x10008007   0x77

2 - Analise o conte�do da tabela acima para escolher a afirma��o correta. Para o sistema computacional
simulado, dada uma palavra de mem�ria, pode-se afirmar que:

(x) os endere�os de mem�ria crescem do byte menos significativo para o byte mais significativo.
( ) os endere�os de mem�ria crescem do byte mais significativo para o byte menos significativo.

3 - Analise o programa e, em particular, o efeito da instru��o lw $t8,0($s0) para escolher a afirma��o
correta. Para o sistema computacional simulado, o endere�o de uma palavra na mem�ria corresponde:

( ) ao mesmo endere�o de mem�ria de seu byte mais significativo (MSByte).
(x) ao mesmo endere�o de mem�ria de seu byte menos significativo (LSByte).