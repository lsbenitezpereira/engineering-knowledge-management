#if((x % 4 == 0) && (x % 100 !=0) || (x % 400 == 0))
#x -> $t0

 .data
 msg1: .asciiz "Digite um ano para verfificar se ele � bissexto: "
 msg2: .asciiz "\nAno Bisexto " 
 msg3: .asciiz "\nO ano n�o � bissexto"
 
.text
.globl main
main:
	# Imprimir a string msg1!
  	li 	$v0, 4 		#carregando no reg $v0 o numero da chamada de sistema
  	la 	$a0, msg1
  	syscall
  	
  	# Leio a entrada do usu�rio
  	li 	$v0, 5
  	syscall
	
	li	$t0, 4
	li	$t1, 100
	li	$t2, 400
	div	$v0, $t0
	mfhi	$t3		#t3 = x % 4
	seq	$t3, $t3, 0	#t3 = (x % 4 == 0)
	div	$v0, $t1 	
	mfhi	$t4		#t4 = x % 100
	seq	$t4, $t4, 0	#t4 = (x % 100 == 0)
	xori	$t4, $t4, 1	#t4 != (X % 100 ==0)
	and	$t3, $t3, $t4	#t3 = t3 && t4
	div	$v0, $t2	
	mfhi	$t4		#t4 = x % 400
	seq	$t4, $t4, 0	#t4 = (x % 400 == 0)	
	or	$t3, $t3, $t4	#t3 = t3 || t4
	beqz 	$t3, false
  	li 	$v0, 4 		#carregando no reg $v0 o numero da chamada de sistema
  	la 	$a0, msg2
  	syscall
  	j	exit
  	
false:
  	li $v0, 4 #carregando no reg $v0 o numero da chamada de sistema
  	la $a0, msg3
  	syscall	
  	 	
exit: