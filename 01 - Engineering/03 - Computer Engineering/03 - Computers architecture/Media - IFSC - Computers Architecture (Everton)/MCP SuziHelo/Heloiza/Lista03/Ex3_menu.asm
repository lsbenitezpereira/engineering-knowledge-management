 .data
 tamanho: .asciiz "Entre com a quantidade de elementos:\n"
 array: .word 0:1024 #inicializados com zero
 linha: .asciiz "\n"
 menu1: .asciiz "Menu:\n 1 - Entrar com a quantidade de elementos\n 2 - Imprimir sem ordenar\n 3 - Imprimir crescente\n 4 - Imprimir decrescente\n 5 - Somat�rio\n 6 - M�dia\n 7 - Encerra\n"
 semente:.asciiz "Entre com a semente(numero) pra gerar vetor: "
 
 .text
 menu:
 	#imprimir MENU
	li 	$v0, 4					#string
	la 	$a0, menu1				#vetor usuario
	syscall
	#le numero MENU
	li 	$v0, 5 					#inteiro	
	syscall
	move 	$t4 ,$v0				#entrada MENU
	beq 	$t4, 1, inicio
	beq 	$t4, 2, imprimir
	beq 	$t4, 3, crescente
	beq 	$t4, 4, decrescente
	beq 	$t4, 5, somatorio
	beq 	$t4, 6, media
	beq 	$t4, 7, fim

#-----------------------------------------------------
 inicio:
 	li	$v0, 4
 	la	$a0, tamanho
 	syscall
 	
 	li	$v0, 5
 	syscall
 	
 	move	$s0, $v0
 	move	$t0, $v0
 	
	li 	$v0, 4				
	la 	$a0, semente			
	syscall
	li 	$v0, 5 					
	syscall
	move 	$t7 ,$v0	
	
 	la	$s1, array
 	jal	inicial
 	j	menu
 
 inicial:
 	beqz	$t0, end_inicial
 	li	$a1, 100
 	li	$v0, 42
 	syscall
 	
 	sw	$a0, 0($s1)
 	addi 	$s1, $s1, 4		#anda pelo vetor
	addi 	$t0, $t0, -1		#qtd -1
	b	inicial
 	
 end_inicial:
 	jr	$ra
 #-----------------------------------------------------
 
 imprimir:
 	move	$t0, $s0
 	la	$t1, array
 	
 loop:	lw	$a0, 0($t1)
 	beqz 	$t0, menu
 	
 	li	$v0, 1
 	syscall
 	
 	li 	$a0, 8
 	li	$v0, 11
 	syscall
 	addi	$t1, $t1, 4
 	subi	$t0, $t0, 1
 	b	loop
 #-----------------------------------------------------
 
 crescente:
 	li	$v0, 4
 	la	$a0, linha
 	syscall
 	
 	addiu	$sp, $sp, -24
 	la	$a0, array
 	sw	$a0, 0($sp)
 	li	$a1, 0
 	sw	$a1, 4($sp)
 	addi	$t2, $s0, -1
 	move	$a2, $t2
 	sw	$a2, 8($sp)
 	jal	quicksort
 	move	$t0, $s0
 	la	$t3, array
 	
 	imprime:
 	lw	$a0, 0($t3)
 	beqz	$t0, end_imprime
 	li	$v0, 1
 	syscall
 	li	$a0, 32
 	li	$v0, 11
 	syscall
 	addi	$t3, $t3, 4
 	sub	$t0, $t0, 1
 	b	imprime
 	
 	end_imprime:
 	lw	$ra, 16($sp)
 	addiu	$sp, $sp, 24
 	li	$v0, 4
 	la	$a0, linha
 	syscall
 	j	menu
 	
 
 decrescente:
  	li	$v0, 4
 	la	$a0, linha
 	syscall
 	
  	addiu	$sp, $sp, -24
 	la	$a0, array
 	sw	$a0, 0($sp)
 	li	$a1, 0
 	sw	$a1, 4($sp)
 	addi	$t2, $s0, -1
 	move	$a2, $t2
 	sw	$a2, 8($sp)
 	jal	quicksort
 	move	$t0, $s0
 
 	la	$t3, array 
 	mul 	$t4, $t0, 4				#pra chegar no ultimo elemento do vetor
	addi 	$t4, $t4, -4			#volto 1 casa pra pegar o verdadeiro elemento do vetor
	add 	$t3, $t3, $t4			#final do array
 	
 	imprime_d:
 	lw	$a0, 0($t3)
 	beqz	$t0, end_imprime_d
 	li	$v0, 1
 	syscall
 	li	$a0, 32
 	li	$v0, 11
 	syscall
 	addi	$t3, $t3, -4
 	sub	$t0, $t0, 1
 	b	imprime_d
 	
 	end_imprime_d:
  	li	$v0, 4
 	la	$a0, linha
 	syscall
 	j	menu
 
 somatorio:
 
 
 media:
 fim:
 	li $v0, 10
 	syscall


particiona:
 	sll	$t0, $a1, 2		# $t0 = lb * 4
 	add	$t0, $t0, $a0		# $t0 = x + (lb * 4)
 	lw	$t1, 0($t0)		# $t1= x[lb] --------- int a
 	addi	$t2, $a1, 0		# $t2 = lb  ---> down
 	addi	$v0, $a2, 0		# $t3 = ub  ---> up
 	
 while:	bge	$t2, $v0, end_while
 while2:
 	sll	$t4, $t2, 2
 	add	$t4, $t4, $a0		
 	lw	$t5, 0($t4)		# $t5 = x[down]
 	slt	$t6, $t2, $a2		# down < ub
 	sle	$t7, $t5, $t1		# x[down] <= a 
 	and	$t7, $t6, $t7		# x[down] <= a && down < ub
 	beqz	$t7, while3
 	addi	$t2, $t2, 1
 	b	while2
 while3:	
	sll	$t8, $v0, 2
	add	$t8, $t8, $a0
	lw	$t9, 0($t8)		# $t9 = x[up]
	ble	$t9, $t1, end_while3
	subi	$v0, $v0, 1
	b	while3
 end_while3:
	bge	$t2, $v0, while
	sw	$t5, 0($t8)		# x[up] = x[down]
	sw	$t9, 0($t4)		# x[down] = x[up]
	b	while

 end_while:
 	sw	$t9, 0($t0)		# x[lb] = x[up];
 	sw	$t1, 0($t8)		# x[up] = a;
 	jr	$ra

 quicksort:
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$ra, 16($sp)
	
	bge	$a1, $a2, else
	jal	particiona

	lw	$a0, 0($sp)
	lw	$a1, 4($sp)

	subi	$a2, $v0, 1
	jal	quicksort
	
	lw	$a0, 0($sp)

	addi	$a1, $v0, 1
	lw	$a2, 8($sp)
	
	jal	quicksort
 else:
 	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	lw	$a2, 8($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
	j 	menu
 
 	
 		
