.text
.globl main
main:	
	add 	$s0, $zero, $gp
	lw	$t0, 0($s0)
	lw	$t1, 4($s0)
	#sw	$t0, 0($s0) 
	#sw	$t1, 0($s1)
	bgt	$t0, $t1, store	#if $s0 > $s1
	addi	$s2, $t0, 0
	b	exit

store:
	addi	$s2, $t1, 0
exit:
