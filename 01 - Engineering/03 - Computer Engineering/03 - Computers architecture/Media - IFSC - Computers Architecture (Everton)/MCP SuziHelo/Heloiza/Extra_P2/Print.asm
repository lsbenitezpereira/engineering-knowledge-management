 .data
 intro: .asciiz	"Digite uma palavra: "
 text: .space 1024
 buffer: .word 1024
 
 .text
 main:
 	addiu	$sp, $sp, -24
 	sw	$ra, 16($sp)
 	#print(intro)
 	la	$a0, intro
 	sw	$a0, 0($sp)
 	li	$v0, 4
 	syscall
 	
 	# text = readString()
 	la	$a0, text
 	lw	$a1, buffer
 	sw	$a1, 4($sp)
 	li	$v0, 8
 	syscall
 	
 	li	$t0, 0		# i = 0
 	la	$t1, text
 for:	
 	lb	$t2, 0($t1)	# text [i]
	beqz	$t2, while
	addi	$t0, $t0, 1	# i++
	addi	$t1, $t1, 1	# text [i++]
	b	for

while:	la	$t1, text
	blez	$t0, end_while
	subi	$t0, $t0, 1	# --i
	add	$t2, $t0, $t1 	# text[--i]
	lb	$a0, ($t2)
	li	$v0, 11
	syscall
	b	while
	
end_while:
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
 	
 	