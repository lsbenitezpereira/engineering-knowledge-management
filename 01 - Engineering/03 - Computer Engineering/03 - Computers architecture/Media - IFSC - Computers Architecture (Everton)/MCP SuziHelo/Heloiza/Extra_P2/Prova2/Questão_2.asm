.text
.globl main
main:
#	empty- 8($sp)
#	ra --- 4($sp)
#	a0 --- 0($sp)
	addi	$sp,$sp,-12
	addi	$a0,$zero,1
	jal FIB
FIB:
#	empty- 8($sp)
#	ra --- 4($sp)
#	a0 --- 0($sp)
	addi	$sp,$sp,-12
	sw	$ra,16($sp)
	beqz	$a0,end0
	addi	$t0,$zero,1
	beq	$a0,$t0,end1
	subi	$a0,$a0,1
	subi	$t0,$a0,2
	add	$a0,$a0,$t0
	sw	$a0,12($sp)
	sw	$ra,4($sp)
	jal	FIB
	lw	$t0,0($sp)
	add	$v0,$v0,$a0
	lw	$ra,16($sp)
	jr	$ra
end1:
	addi	$v0,$zero,1
	lw	$ra,16($sp)
	jr	$ra
end0:
	add	$v0,$zero,$zero
	lw	$ra,16($sp)
	jr	$ra