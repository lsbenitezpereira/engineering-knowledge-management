

 .text
 main:
 	addiu	$sp, $sp, -24
 	sw	$ra, 16($sp)
 	
 	li	$a0, 4
 	sw	$a0, 0($sp)

 	jal	harmonico
 	
 	j	exit
 	
 harmonico:
	addiu	$sp, $sp, -24
 	sw	$ra, 16($sp)
 	sw	$a0, 0($sp)
 	sw	$s0, 20($sp)
 	
 	move	$t0, $a0
 	
 	bgt	$t0, 1, end_if
 	
 	li 	$v0, 1		# return 1
	mtc1 	$v0, $f12	# move p float
	cvt.s.w $f12, $f12	# converte p float
	mfc1 	$v0, $f12	# move p v0 em formato float
	
 	lw	$a0, 0($sp)
 	lw	$ra, 16($sp)
 	lw	$s0, 20($sp)
 	addiu	$sp, $sp, 24
 	jr	$ra
 	
 end_if:
 	li	$t1, 1
 	mtc1 	$a0, $f12	
	cvt.s.w $f12, $f12		# converte int p float
	mtc1  	$t1, $f1 		# move pro reg
	cvt.s.w $f1, $f1		# converte p float
	div.s 	$f2, $f1, $f12		# f2 = 1/n
	mfc1 	$s0, $f2		# movo pra s0 = 1/2, pra por na pilha e nao perder o valor
	

	#calcula (n-1)
	addi   $a0, $a0, -1		# a0 = n-1
	jal 	harmonico	
	
	mtc1  	$s0, $f12		# 1/n
	mtc1  	$v0, $f1		# h(n-1)
	add.s 	$f12, $f1, $f12		# 1/n + h(n-1)
	mfc1  	$v0, $f12		# return (1/n + h(n-1) - acumulacao das outras somas
	
 	lw	$a0, 0($sp)
 	lw	$ra, 16($sp)
 	lw	$s0, 20($sp)
 	addiu	$sp, $sp, 24
 	jr	$ra
 	
 exit:
 	li 	$v0, 2
	syscall			
