.data
array: .word 1, 2

.text
.globl main
main:
	#overflow
	li	$t0, 0x7fffffff
	li	$t1, 1
	addu	$t2, $t0, $t1
	add	$t3, $t0, $t1
	
	#sw desalinhado
	add 	$s0, $zero,  $gp
	addi 	$s0, $s0, 5
	sw  	$s0, 1($s0)

	#lw desalinhado
	la 	$t1, array
	lw 	$t1, 1($t0)
	
	# syscall
	li  $v0, 45      # service 1 is print integer
    	syscall
	
.kdata 
_msg1: 	.asciiz "\n!! Ocorreu uma exce��o!!\n"
_over: 	.asciiz "\n Cause: Arithmetic overflow exception!\n"					#code 12
_sw: 	.asciiz "\n Cause: Address error exception caused by store!\n" 				#code 5
_lw: 	.asciiz "\n Cause: Address error exception caused by load or instruction fetch!\n"	#code 4	
_sys:	.asciiz "\t Cause: System call exception by the 'syscall' instruction\n"
_fpe:	.asciiz "\t Cause: Floating-Point exception cause by a floating-point instruction\n"
_addr: 	.asciiz "\n endere�o invalido: "	

.align 2
_regs:	.space	20

.ktext 0x80000180

	move	$k0, $at 	#$k0 = $at
	la 	$k1, _regs 	#$k1 = address of _regs
	sw 	$k0, 0($k1) 	#save $at	
	sw 	$v0, 4($k1) 	#save $v0
	sw 	$a0, 8($k1) 	#save $a0
	
	la	$a0, _msg1
	li	$v0, 4
	syscall
	
	#identifica a exce��o
	mfc0 	$k0, $13	#Get cause register from coprocessor 0
	srl	$k0, $k0, 2	# Extract exception code field (bits 2-6)
	
	beq 	$k0,  4, addrl	#lw 
	beq 	$k0,  5, addrs	#sw
	beq 	$k0,  8, sys	#syscall
	beq 	$k0, 12, ovf	#overflow 
	beq 	$k0, 15, fpe	#ponto flutuante
	
	
addrl:
	la   	$a0, _lw	
	li   	$v0, 4		
	syscall
	
	la 	$a0, _addr
	li 	$v0, 4
	syscall
	
	mfc0 	$k0, $8		#Copia endere�o de memoria invalido
	li 	$v0, 1		#Imprime endere�o em int
	add 	$a0, $k0, $zero
	syscall 
	j	end		
	
addrs:
	la 	$a0, _sw	
	li 	$v0, 4			
	syscall
	
	la 	$a0, _addr
	li 	$v0, 4
	syscall
	
	mfc0 	$k0, $8		
	li 	$v0, 1		
	add 	$a0, $k0, $zero
	syscall 
	j	end
	
sys:
	la 	$a0, _sys		
	li 	$v0, 4			
	syscall
	j    	end

fpe:
	la 	$a0, _fpe  		
	li 	$v0, 4		
	syscall
	j    	end
	
ovf:		
	la 	$a0, _over	
	li 	$v0, 4		
	syscall
	
	b     	end
	
end:
	la	$k1, _regs		#$k1 = address of _regs
	lw 	$at, 0($k1)		#restore $at
	lw 	$v0, 4($k1)		#restore $v0
	lw 	$a0, 8($k1)		#restore $a0
	
	mfc0	$k0, $14		#$k0 = EPC
	addiu	$k0, $k0, 4
	mtc0	$k0, $14		#EPC = point to next intruction 
	eret
	
	mfc0 	$k0, $14 		# $k0 = EPC
	addiu	$k0, $k0, 4		# Increment $k0 by 4
	mtc0 	$k0, $14 		# EPC = point to next instruction
	eret

