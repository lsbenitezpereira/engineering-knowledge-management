#int fibonacci(int n){	
#	if(n < 2){
#		return n;
#   }
#    return fibonacci(n-1)+fibonacci(n-2);
#}
 .data
 msg1: .asciiz "Fibonacci(?): "
 linha: .asciiz "\n"

 
.text
main:
	addiu	$sp, $sp, -24
	sw	$ra, 16($sp)
	
	la	$a0, msg1
	li	$v0, 4
 	syscall
 	li	$v0, 5
 	syscall
 	
 	move	$a0, $v0
	jal	fibonacci
	
	move	$a0, $v0
 	li	$v0, 1
 	syscall
	
	#lw	$a0, 0($sp)
	lw	$ra, 16($sp)
	#lw	$s0, 20($sp)
	addiu	$sp, $sp, 24
	
	li	$v0, 10
	syscall
 # -----------------------	
 # a0 (fibonacci) 24(sp)
 # ------------- 24 Bytes
 #    espa�o     20(sp)
 # -------------
 # ra		16(sp)
 # -------------
 # a3		12(sp)
 # a2		8(sp)
 # a1		4(sp)
 # a0		0(sp)
 # -----------------------
 
 # int fibonacci (int n);	
fibonacci:
 	addiu	$sp, $sp, -24
 	sw	$a0, 0($sp)
 	sw	$s0, 20($sp)
 	sw	$ra, 16($sp)
 	
 	#seq	$t0, $a0, 0
 	#seq	$t1, $a0, 1
 	#or	$t2, $t0, $t1
 	#bnez	$t2, else
 	bge	$a0, 2, else
 	move	$v0, $a0
 	j	exit
 	
 else:
 	sw	$a0, 0($sp)
 	subi	$a0, $a0, 1
 	#move	$s0, $a0
 	jal	fibonacci
 	move	$s0, $v0
 	
 	lw	$a0, 0($sp)
 	subi	$a0, $a0, 2
 	#move	$s0, $a0
 	jal 	fibonacci
 	move	$s1, $v0
 	
 	add	$v0, $s0, $s1
 exit:
 	lw 	$s0, 20($sp)			
	lw 	$ra, 16($sp)			
	lw 	$a0, 0($sp)			
	addiu 	$sp, $sp, 24
	jr 	$ra
 	
