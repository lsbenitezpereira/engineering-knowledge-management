 # void substitui(char *string, char procura, char substitui, int tamanho)
 # 	for(i = 0; i < tamanho; i++)
 # 		if(string[i] == procura)
 #			string[i] = substitui 

 .data
 string: .asciiz "sobstitoi"
 caracter: .ascii "u"
 procura: .ascii "o"
 tamanho: .word 9
 
 .text
 main:
 
 	la	$a0, string
 	la	$a1, caracter
 	la	$a2, procura
 	la	$a3, tamanho
 	
 	jal	substitui
 	j	exit
 	
 substitui:
 
 	li	$t0, 0			# $t0 -> i = 0
 	#lb	$t1, 0($a0)		# $t1 -> string[0]
 	lb	$t2, 0($a1)		# $t2 -> caracter
 	lb	$t3, 0($a2)		# $t3 -> procura
 	lw	$t4, 0($a3)		# $t4 -> tamanho
 for:
 	lb	$t1, 0($a0)
 	beq	$t0, $t4, end_for	# i = tamanho, tamanho == 0
 	bne	$t1, $t3, end_if
 	sb	$t2, 0($a0)	
 	
 end_if:
 	#addi	$t0, $t0, 1
 	addi	$a0, $a0, 1
 	subi	$t4, $t4, 1
 	b 	for
 	
 end_for:
 	jr	$ra
 	
 exit:
 
 	li	$v0, 4
 	la	$a0, string
 	syscall
 	
 	
 
