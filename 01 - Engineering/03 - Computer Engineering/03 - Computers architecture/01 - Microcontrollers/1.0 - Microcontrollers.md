[TOC]

# Conceptualization

*  *MCU* = microcontroller unit
  
*  Microprocessador com várias funcionalidades (periféricos) disponı́veis em um único encapsulamento:
  
* Integrated circuit programmed to perform a specific task

* In microcontrollers generally the clock is a good performance predictor, mainly because they are deterministic 

* tightly coupled hardware and software components

* Perform a single function is expected to work with minimal or no human interaction

* operate in constrained environments where memory, computing power, and power supply are limited

* Firmware: a computer program tightly coupled to the hardware where it  resides, typically stored in a non-volatile memory embedded in a  hardware device

  ![image-20200229233615755](Images - 1.0 - Microcontrollers/image-20200229233615755.png)

  ![image-20200804153942164](Images - 1.0 - Microcontrollers/image-20200804153942164.png)

*  programmable via a JTAG port or a built in bootstrap loader (BSL) using RS-232 or USB ports.

* JTAG dá pra fazer debug por que consegue, entre outros, parar o processador 

* RTC (pra calendario): compensa o cristal com a temperature 

* **Applications**

  * Smart homes: smart fridge, microwave, tv, phone (not smartphone)
  * Automobilistic: injeção eletrônica, computador de bordo

## History

* 1947: transistor invention
  
* **Apollo Guidance Computer (AGC).**
  
  * early 1960s
  * Much more a microcontroller than a microprocessor
* **Central Air Data Computer (CADC)**
  
  * a chipset CPU for the F-14 Tomcat fighter named the MP944
  * completed by 1970 ( public disclosure of its existence was delayed until 1998)
* **i4004**
  
  * recognized as the first commercial, stand-alone single chip microprocessor 
  * launched by Intel in November 1971
* ## Classification

* The classifications are not mutually exclusive

* **Small**
  
  * Software is typically single-tasked
  * rarely requires an RTOS.
  
* **Distributed**
  
  * In this class we are not referring to what is traditionally known as a distributed computing system
  * CPU resides in a separate chip while the rest of components(memories,  I/O, co-processors, etc) are spread across one or several chips in what is typically called the processor chipset.
  
* **High-performance**
  
  * highly specialized embedded systems requiring fast computations, robustness, fault tolerance, and high maintainability
  * Include DSPs and ==FPGAs?==
  * ==In what DSP are different?==

## Project with embedded systems

* Restrições de projeto
  * Funcionalidade: a capacidade do sistema de executar a função para
    a qual foi projetado.
  * Custo: a quantidade de recursos necessários para conceber, projetar,
    produzir, manter e descartar um sistema embarcado.
  * Desempenho: a capacidade do sistema para executar sua função
    dentro das restrições temporais.
  * Tamanho: espaço fı́sico ocupado pela solução.
  * Energia: energia requerida por um sistema para desempenhar sua
    função.
  * Time to market: o tempo que leva desde a concepção do sistema
    até a implantação.
  * Manutenção: capacidade do sistema ser mantido funcional por toda
    sua vida útil.
* **Things to speed up development**
  * https://mynewt.apache.org/
  * https://mongoose-os.com/

## Common stacks

### TCP/IP

* Good impementation: lightweight IP, lwIP

### Filesystem

* **middleware FatFS**
  * provide f_open, f_write, etc)
  * We need to provide an Media Acess Interface (with fuctions like disk_status, disk write), that is specific for that hardware that we use
* **fat32**
  * compatible with linux and windows and macOS. Very imited in the aount of files

## Técnicas de programação

* Memória e desempenho são fundamentais

* Algumas coisas faremos mais baixo nível do que o normal, ou não usaremos as bibliotecas padrão

* **Tipos apropriados**

  * os tipos com o mesmo tamanho da word são mais eficientes em processamento
  * ex: um `unsigned int` no stm32 é mais eficiente que um `char`, mesmo que ocupe mais espaço
  * geralmente os microcontroladores já vem com typedefs para facilitar a compatibilidade, ex: `uint_t`

* **Divisão**

  * tente fazer com multiplo de 8
  * o compilador já transforma para desocamento de bits

* **Argumentos de função**

  * se forem muitos, os argumentos são passados pela pilha (stack), o que gera overhead
  * crie uma estrutura de dados e passe o ponteiro

* **Outras técnicas**

  * loop unrolling: escreve código inline

  * testar igualdade com zero costuma ser mais eficiente em assembly do que outros testes

  * campo de bit: não costuma ser eficiente pro compilador

  * o jeito eficiente de fazer flags geralmente é com bits em defines:

    ```
    unsigned int FLAGS
    #define FLAG_1 <clear/set/get bit para aquele posição>
    ```

    

    

* **Máquina de estados**

  * Or *Finite State Machine (FSM)*

  * :books: Firmware handbook, pg 117, excelent mealy machine example

  * :keyboard: C: Boilerplate do Renam com ponteiros de função (funçaõ=saída=nó, switchCase=aresta), máquina Moore:
  
    ```c
    //// fsm.h
    /* Definição dos estados */
    typedef enum {
        STATE_A,
        STATE_B,
        NUM_STATES
    } state_t;
    
    extern volatile state_t curr_state;
    
    ///// fsm.c //////
    /* Definição da estrutura mantenedora do vetor de estados */
    typedef struct {
        state_t myState;
        void (*func)(void);
    }fsm_t;
    
    /* Mapeamento entre estado e funções */
    fsm_t myFSM[] = {
        { STATE_A, f_stateA },
        { STATE_B, f_stateB }
    };
    
    /* Estado inicial  */
    volatile state_t curr_state = STATE_A;
    
    void do_fsm(){
        (*myFSM[curr_state].func)();
    }
    
    void f_stateA(void){...}
    void f_stateB(void){...}
    
    //////// In the application file /////////////
    switch (curr_state) {
        case STATE_A:
            if (transitionCondition) { ... curr_state = STATE_B;}
            break;
        case STATE_B:
            if (transitionCondition) { ... curr_state = STATE_A;}
            break;
    ```

* **Hierarchical State Machines**
  *  captures the commonality by organizing the states as a hierarchy
  * can be mapped to a traditional state machine, using naming conventions and repeating some code
  * can be properly implemented with OO code
  
* **Mixing C and assembly**

  * ==should this be here?==
  * função intrincisca: mapeia diretamente para uma instrução em assembly; Uma função, uma instruação; Architecture dependent;

## Processors benchmarks

* coreMarks: ?

## DSP applications

* Processing digitized signals requires high memory bandwidths
  and fast multiply accumulate operations
* Additions to the ARM architecture mean that ARM is well suited for many DSP
  applications.
* DSP applications are typically multiply and load-store intensive.
* Filtering is probably the most commonly used signal processing operation. It can be
  used to remove noise, to analyze signals, or in signal compression
* Another very common algorithm is the Discrete
  Fourier Transform (DFT)
* **Some ARM C functions**
  * int dot_product(sample *x, coefficient *c, unsigned int N)

# MSP430

* Mixed Signal Processor
* Family of microcontrollers developed by Texas Instruments
* from 1992
* Aimed to low power
* Altamente compatíveis entre si
* **Architecture**
  * Arquitectura Von-neumann
  * 16 bits (Extenção CPUX: 20 bits)
  * 16 Registers, general and specific
* **Name format**
  * MSP430{Device Memory}{Generation}{Family}{Series and Device Number} ...  [A][Temperature Range][Packaging][Distribution Format][Additional  Feature]
  * where fields between square brackets [] are optional (see :book: Jimenez 147 for explanations)
* **Development Kits**
  * One development kit can be compatible with more than one microcontroller (except for the SMD ones, because they are already soldered)
  * MSP-EXP430FR2355
    * roda bem em linux (plg and play)
    * encapsulamento smd
    * bem novo
* **Assembly**
  * Very similar to MIPS
  * consegue operar direto da memoria 
  * 27 instructions
  * 24 pseudo instructions 
  * Instructions length: one to three words
  *  seven addressing modes
  *  memory-mapped peripheral
  * Mixing C and Assemby
    * :book: Jimenez pg 238
    * The compiler also provides some Intrinsic Functions, starting with `__`
  * 
*  **Others**
  * MSPDebug: https://dlbeer.co.nz/mspdebug/
* **RTOS**
  * Deixaria o MSP bem carregado… mas dá
  * https://www.freertos.org/Free-RTOS-MSP430-IAR.html (esse é para um MSP430F149)
* **Para simular no proteus**
  * Exportando do codecomposer: configurações, hex utility, output format=intel hex
  * Importando no proteus: bota o microcontrolador, abre as suas opções, program file, seleciona o arquivo hex
* **Gravando na Flash/FRAM**
  * hardcoded no código
  * variável com modificador `const`
  * em alguns microcontroladores (exemplo: AVR) também é preciso indicar com alguma diretiva de compilador, como `PROGMEM`
  * exemplo texas, grando um binário piecewise: http://dev.ti.com/tirex/explore/node?devtools=MSP-EXP430FR2355&kernels=nortos&node=ALgz0uyaJU9utfiO5FlOSg__IOGqZri__LATEST&resourceClasses=example
  * 

## Power Supply

* Tensão de alimentação afeta o clock máximo (VCC maior, frequência maior)
* Alimentação analógica separada da digital
* **Power modes**
  * CPU off: can only be awaken by an interrupt or a reset

## Bootloader

* Or *Boot Sequence*

* Load the software

* Bootloader é o código executado pelo mcc antes do código da aplicação (antes da sua função main). 

* Após o reset, a cpu vai para o vetor de interrupção (reset). Há mcc, tipo o Arduino, que se vc manter um pino em alto, o vetor de reset é alterado para outro endereço que pode estar o bootloader de gravação por UART. Nesse caso é um código usado para o próprio MCC gravar a flash. 

  Se o bootloader não for de gravação, ele é responśavel por criar o estado inicial da aplicação. Antes do main, zerar as variáveis, carregar as constantes da flash para RAM, alocar os objetos (se c++) entre outras coisas.
  Se o processador executa um sistema operacional, o bootloader (BIOS ou UEFI do seu Pc) é responsável por mudar os rings de segurança, ligar o controlador de memória, ligar o hardware de vídeo básico. Quando o SO carergar, é necessário ainda configurar demais CPUs se for multicore, ... 
  No Android, há um bootloader especial para atualizar o sistema.

* The bootloader is written in memory, so if you sobrescribe it… bye bye

* Origin of the word: “pull oneself up by one’s bootstraps” denoting those with the ability of helping
  themselves without external means.

* **Como eu "congelo" o código que está gravado e impeço o regravamento?**

  * Quando você está com código de produção (produto), é possível bloquer a flash não permitindo a regravação, nem leitura. 
  * Isso é feito no processo de gravação no codecomposer. Cuidado que isso é permanete, não se pode mais acessar o mcc depois disso. Alguns tem senha, mas daí depende do fabricante.

* **Se eu tivesse só o chip do MSP430, como faço pra grava-lo?**

  * Não tenho certeza se a texas suporta gravação por por serial UART (isso é um código de boot loader). Caso contrário, você pode puxar os cabinhos 3V3, GND , SBWTDIO e SBWTCLK do kit e conectá-los nos respectivos pinos de outro MSP430. É possível vc usar o kit FR2355 e gravar o G2553 ou F2132

* **O que têm naquela parte da placa que serve para gravação?**

  * Tem o software/hardware que converte USB (USB é uma rede, protoloco de comunicação bem definido, corrente, tensão, etc...) para o protocolo do debugger da Texas. É um "tipo" de SPI: dados (SBWTDIO) e clock (SBWTCLK). Além disso, aquele hardware monitora o consumo do MSP430 para você otimizar a energia da aplicação.

* 

## Clock

* Onda quadrada estável, duty cicly 50%

* MSP tem um clock interno bom

* só precisa de cristal se quiser ser preciso pra carai

* three internal clock signals 

* Clock directly impacts consumption

* Tensão de alimentação afeta o clock máximo (VCC maior, frequência maior)

* Prescaler: divide a frequência

* **Definição do clock**

  * Qual é o evento mais rápido que o sistema deve lidar?
  * Que periféricos compartilham o clock do CPU?
  * Qual é a precisão do clock necessária? 

* **Fontes externas**

  * cristal

    * Have an startup time, related to the time to reach full amplitude ressonance

    ![image-20200324194358912](Images - 1.0 - Microcontrollers/image-20200324194358912.png)

  * Oscilador cerâmico

  * Rede RC

* **Fonte internas**

  * Depends on the family

  * Frequency locked loop (FLL)

    * Uses an Digitally Controlled Oscillator (DCO) to multiply the clock

      ![image-20200324202643150](Images - 1.0 - Microcontrollers/image-20200324202643150.png)

  * Basic clock

    * Better than FLL
    * MSP430x2xx and MSP430x1xx

  * includes three oscillators: LFXT1, XT2, DCO, and VLO (very-low-power, low-frequency oscillator)

    * VLO é 10kHz.

  * external crystals: can be low (32.768 KHz) or high frequency (450 KHz to 8 MHz) , selectable with the XTS control bit

* **Unified Clock System (UCS)**

  * Better than Basic
  * MSP430x5xx e MSP430x6xx

  * Redes internas

    * ACLK
      * Auxiliary. 
      * for peripherals low-frequency operation
    * MCLK
      * Master
      * To processor
    * SMCLK
      * Subsystem master
      * To peripherals that can work independently
        from CPU operation
    * MODOSC: fixo para o ADC

## GPIO

* agrupados em ports, geralmente de 8-em-8 (ou 16-em-16 em outros MCUs)
* cada pino pode ser configurado e alterado indivudualmente
* pull up e pull down
* cada pino fornece/drena até 6mA
* cada port fornece/drena até 48mA
* **registers**
  * PxDIR: direction $out/\overline{in}$
  * PxREN: $enable/\overline{disable}$ pull up/down resistor
  * PxOUT: write (if dir=out) or pull $up/\overline{down}$ (if dir=in) 
  * PxIN: read
  * PxSEL
    * operation mode
    * depends from pin-to-pin, I’ll put here some basics
    * 00=IO
    * SEL=10 and DIR=0: timer input (capture)
    * SEL=10 and DIR=1: timer output (pwm)
    * 

## Interruptions

* Auto-Vectorized interruption, by port or periferal 

* Vetor possue endereço fixo, mas a implementação pode ficar em
  qualquer lugar da memória
  
* Prioridade: fixa e definida previamente (datasheet)

* Globa lnterrup Enable: bit in Status Register

* nested, with unlimited depth level

* Quase todos os pinos quase sempre podem ser interrupção

* Trigado por borda

* :book: Jimenez 299

* **Registers**
  * PxIE: interrupt enable, $on/\overline{off}$
  * PxIES: Interrupt Edge Select. borda $descida/\overline {subida}$
  * PxIFG: interrupções pendentes $yes/\overline {no}$
  * GIE: Global Interrupt Enable (flag on status register SR)
    * `__bis_SR_register(GIE)`:  enable global
    * `__bic_SR_register(GIE)`:  disable global
  
* **Interrupt vector table:** Right in the begining of the memory map (fixed address)

* MSP430 código para interrupção e explicação:

  ```C
  #if defined(_TI_COMPILER_VERSION) || defined(IAR_SYSTEMS_ICC_)
  #pragma vector=PORT2_VECTOR
  __interrupt void Port_2(void)
  #elif defined(_GNUC_)
  void _attribute_ ((interrupt(PORT1_VECTOR))) Port_2 (void)
  #else
  #error Compiler not supported!
  #endif
  ```

  > "Se o compilador for da TI (Texas Instruments) ou da IAR, defina a ISR como (nome do vetor ou seja, endereço que ficará na tabela da CPU, Onde pular quando acontecer interrupções do PORT2):
  >
  > #pragma vector=PORT2_VECTOR
  >
  >  
  >
  > Declaração da função (qualquer nome, desde que colocado o \_\_interrupt na frente e #pragma antes. O serve para \_\_interrupt mudar a manipulação da pilha na entrada da função e a instrução assembly utilizada no retorno.)
  >
  > \_\_interrupt void Port_2(void)
  >
  > 
  >
  > "Se o compilador for GCC (o que não usamos no code composer), defina ISR como:
  >
  > void _attribute_ ((interrupt(PORT2_VECTOR))) Port_2 (void)
  >
  > 
  >
  > Se não for TI nem IAR nem GCC, aborte a compilação.

### Reset
* does not save the processor status or a return address
* does not return
* **power-on reset (POR)**
  * When initializing the system
  * Deixa a CPU em um estado conhecido
  * Does not execute nothing before reset
  * The circuit to POR can be internal or external (because you need to supervise the voltage and count time)
  * External CI: LP3470
  
  ![image-20200321192751436](Images - 1.0 - Microcontrollers/image-20200321192751436.png)

* **Browout-out reset (BOR)**
  * power supply problem, during execution
  * Monitores voltage (so it requires especial circuits, internal or external)
* **SVS**: ?
* **Watchdog**
  
  * see specific section
* **RST/NMI**
  * pino de reset externo
  * ==NMI?==
* **Power-up clear (PUC)**
  * Todas as outras interrupções sempre geram um PUC, mas não vice-versa
  * reset só do software
  * quando dá cagada em algo no software

  ![image-20200321193555015](Images - 1.0 - Microcontrollers/image-20200321193555015.png)

## Timer

* Ou *temporizador*

* Gera uma contagem em rampa ou triangular (crescente ou crescente-decrescente)

* 16 bits

* 1 ciclo de clock = 1 contagem

* Busywaiting: contar tempo sem timer, apenas contando o número de ciclos (while 1)

* cada MCU varia os timers que tem 

* WDT e RTC são abordados separademnete, e essa introdução é de timers em geral 

* We’ll describe most things here in term of timer A, for convenience

* **Eventos**

  * É possível atribuir interrupçõs para eventos de contagem:

  ![image-20200331151444140](Images - 1.0 - Microcontrollers/image-20200331151444140.png)

  * Overflow
  * Comparison
  
* **Tipos de timer**

  * WDT: watch dog. Todos tem
  * Tipo A: normal, IRQ por comparação. Interrupts: TACR0 e TACR1
  * Tipo B: improves PWM. ==just that?==
  * Tipo D: hi-res timing
  * RTCA:
  * RTCB:

* **Possui duas fontes indentepentes de interrupção (dois vetores)**

  * 0: só para o comparator 0 (TACR0). Maior prioridade
  * 1: overflow e outros comparadores. Comum a várias fontes, então temos que fazer um switch-case dentro da interrupção
  * IRS do vetor 0 do timer 0 que é do tipo A: `__interrupt void TIMER0_A0_ISR (void)`
  * *Counter* must count-to *Compare* value to generate action

* **CCRx registers**

  * x = 0, 1, 2
  * Capture and Compare Register
  * Each timer have its own
  * Sets the value that will triger an event (capture event or compare event)
  * Each CCR can be configured independently

* **Nomenclatura dos GPIOs**

  * T<timerType><timerNumber>.<comparador> (ex: TA0.1, comparador CCR1 do Timer 0 que é do tipo A)

  * Cada CCR fica vinculado à um GPIO específico

  * isso não é roteavel

    ![image-20200429114254172](Images - 1.0 - Microcontrollers/image-20200429114254172.png)

### **Counting modes**

* **Stopped**: default

* **continuous mode**

  * count till full range

  * Generate interrut, but does not reset countage

    ![image-20200429101003308](Images - 1.0 - Microcontrollers/image-20200429101003308.png)

* **Up mode**

  * resetting back to zero whenever the counter matches CCR0
  * Interrupts: CC0IFG and TAIFG

* ![image-20200409154950916](Images - 1.0 - Microcontrollers/image-20200409154950916.png)

* **Up-down**

  * goes up, reaches the value in CCR0, then goes down

  *  Allow to have longer period 

  * Interrupts: CC0IFG and TAIFG (espaçadas 1/2 periodo, por definição)

    ![image-20200426173028554](Images - 1.0 - Microcontrollers/image-20200426173028554.png)
  
* 


### Operation modes

* **Compare mode**

  * we should write `CAP` flag to 0 in TACCTLx ( Timer_A Capture/Compare Control Register)
  * To output signals or interrupts at specific time intervals
  
* **Capture mode**

  * uma borda em GPIO triga a leitura do contador
  * não precisar interromper o processador
  * Usad para leitura precisa de eventos externos
  * Podemos configurar para provocar ou não interrupção (mas a leitura do valor já é feita em hardware)
  * Usage
    * When a capture occurs, the TAR timer data is copied into the corresponding
      TACCRx register and the TACCRx CCIFG interrupt flag is set
    *  If a second capture were performed
      before the previous one was read, then the capture overflow bit or COV, would be
      set to indicate this condition
    * we should write `CAP` flag to 1 in TACCTLx ( Timer_A Capture/Compare Control Register)
  * GPIO configuration

    * PxDIR=0 (in)

    * PxSEL=10

* **Output mode**

  * cada CCR permite um output independente
  
  * Output modes: :book: jimenez pg 350
  
  * GPIO configuration
  
    * PxDIR=1 (out)
    * PxSEL=10 

### Watchdog Timer (WDT)

* special timer

* more limited than a generic timer

* generates an event (interrupt or reset) on overflow

* geralmente é usado apenas na função de reset

* the countage should be frequently canceled/reseted

  ![image-20200331152151170](Images - 1.0 - Microcontrollers/image-20200331152151170.png)

* **MSP430**

  * WDT ativado no poweron reset, para 32768 ciclos
  * whatdog register
    * higher 8bits are a password



## Analog signal chain

### **Analog comparators**

* set bit (or interruption, or…) if $V_+ > V_-$
* Faster than ADC
* MSP430 have an small RC filter after the comparator
* todos os pinos indicados com `CA`
* $value = \frac{V_{in}2^n}{V_{ref}}$

### **Analog-to-digital converters (ADC)**

* referencia interna ou externa
* Se for software, podemos desligar a CPU ou podemos só esperar
* ADC10 ou ADC12, com características diferentes (além do numero de bits)
* Possui um modo DTC (data transfer controller) que é tipo DMA (que pode gravar até em um periférico)
* Um único ADC, vários canais para poder chavear
* possui um clock proprio, `MODCLK`
* `ADCBUSY` (bit in register `ADCCTL1`): indicates an active sample or conversion operation
* The options for the sample-and-hold can be configured by `ADCSH` bits in the `ADCCTL1` register
* **Voltage reference**
  * two programmable and selectable voltage levels (VR+ and VR–) to define the upper and lower limits of the conversion.
  * Configured by `ADCSREFx` in register `ADCMCTL0`
  * Internal reference: 1.5-V, 2.0-V, or 2.5-V
  * External: VEREF+ and VEREF- pins
  * AVCC é o mesmo da alimentação geral
  * Ideal é ter uma referência externa muitooooooo boa. Se não tem, melhor usar as internas do chip
* **Convertion modes**
  * Setado em `ADCCONSEQx`
  * Single-channel single-conversion: A single channel is converted once.
  * Sequence-of-channels: A sequence of channels is converted once.
  * Repeat-single-channel: A single channel is converted repeatedly.
  * Repeat-sequence-of-channels: A sequence of channels is converted repeatedly
* **Input channel selection**
  * Bits `ADCINCHx` in `ADCMCTL0`
* **Trigger**
  * inicio da conversão trigado por software ou timer
  * An analog-to-digital conversion is initiated with a rising edge of the sample input signal SHI. The source for SHI is selected with the ADCSHSx
  * The polarity of the SHI signal source can be inverted with the ADCISSH bit.

### **Smart Analog Combo (SAC)**

* Present in some MSPs
* operational amplifiers
* connections and impedances programables
* Pinos `OA`

## Communication

* Specifically about the protocols, see in *Redes de Computadores*
* Here I’ll write just about the MSP430 specifics
* pinos indicados com `UCA` e `UCB`?
* pode usar mais de uma comunicação ao mesmo tempo, mas eles vão compartilhar as interrupções
* no proteus só funciona com o f2132
* **USART**
  * interrupção pode ser na: transmissão completa, recepção complexa, registrador de dados limpo
  * interrupção compartilhada entre os dois modulos uart.
  * envia um byte de cada vez
  * Multibyte precisa ser implementado em software
  * UCRXBUFx
    * Last received character from the receive shift register
    * Reading UCAxRXBUF resets the
      receive-error bits, the UCADDR or UCIDLE bit, and UCRXIFG. In 7-bit data
      mode, UCAxRXBUF is LSB justified and the MSB is always reset.
* **I2C**
  * Caso estiver em modo slave, Possui um modo low power que só acorda quando recebe comando
* **SPI**
  * MSB ou LSB configurável
  * interrupção no final da transmissão
  * as siglas nos pinos são invertidas, SOMI e SIMO
  * O SS é feito por GPIO normal
  * o clock só é gerado no momento da comunicação
* **OneWire**
  * Não possui implementação
  * pode ser feito por software com facilidade
  * pode também ser implementado a partir de uma modificação da UART
* **USB**
  * Many devices in the F5xx and F6xx MSP families contain the USB peripheral

## Main Microcontrollers

* MSP430FR2355
  * Main package: SMD
  * 24MHz
  * 32 KB FRAM, 4 KB SRAM
  * 44 IO
  * 12-bit ADC, 12-bit DACs
  * OpAmp/PGA 
  * Consumption in Active mode: 142 µA/MHz
* MSP430FR5739
  * Main package: SMD
  * 16 MHz
  * 16 KB FRAM, 1 KB SRAM
  * 32 IO
  * 10-bit ADC 
  * Consumption in Active Mode: 81.4 µA/MHz
* MSP430G2553
  * Main package: PTH
  * 16 MHz
  * 16 KB Flash, 512 Byte RAM
  * 24 IO, 2 16-bit Timers 
  * 10-bit ADC
  * Consumption in Active Mode: 230 µA at 1MHz
* MSP430G2231
  * Main package: PTH
  * 2KB Flash, 125 Byte RAM
  * 16 MHz
  * 10 GPIO
  * Consumption in Active Mode: 220 µA at 1 MHz
* MSP430G2452
  * Main package: PTH
  * 16 MHz
  * 8 KB Flash, 250 Byte RAM
  * 16 GPIO
  * Consumption in Active Mode: 220 µA at 1 MHz
* MSP430G2211
  * Main package: PTH
  * 16 MHz
  * 2 KB Flash, 125 Byte RAM
  * 10 GPIO
  * Consumption in Active Mode: 220 µA at 1 MHz



# Some applications

## Displays

* **7 seg**
  * Low cost
  * Anodo comum: gnd comum, aciona com vcc
  * Catodo comum: vcc comum, aciona com gnd
* **Display LCD**
  * caracter by character
  * There are several controllers (most common is HD44780)
  * parallel communication 4 (send in two parts) or 8 bits
  * you can use and SPI or I2C module ([example](https://www.filipeflop.com/produto/modulo-serial-i2c-para-display-lcd-arduino/)) to use less pins
  * Tamanho em especificado em colunas e linhas
  * Cada endereço é mapeado em linha e coluna
  * The characters in the display are stored in DDRAM memory
  * The characters drawing are stored in an internal ROM (called by him CGRAM - (Character Generator RAM). It can be altered to draw your own characters 
  * Initialzation
    * This routine if defined in the datasheet
    * Wait (~15ms)
    * write a few things (intace 4 or 8 bits, type of display, among others)
    * wait a little more
  * Usage
    * Set address (dont forget the MSB in 1 when sendind data)
    * the data in the Data Bus, pulse in E (the four high order bits (for 8-bit operation,
      DB4 to DB7) are transferred before the four low order bits (for 8-bit operation, DB0 to DB3))
    * The Busy flag is in the ==Enable== pin (renan’s library does not use it)
  * Tutorials and implementations
    * MSP430: renam
    * Raspberry: https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/overview
* **Graphic**
  * pixel level control
  * you can draw
* **Oled**
  * organic light-emitting diode
  * Very good contrast
  * its pixels consume energy only when they are on

## Sensors

* BME280
  * both I2C and SPI
  * Raspberry: https://www.raspberrypi-spy.co.uk/2016/07/using-bme280-i2c-temperature-pressure-sensor-in-python/

## Button debounce

* Se o tempo de responsta for maior do que 75ms o delay é humanamente perceptível 

* Botar o botão direto em uma Interrupção não é umaboa ideoa

* **Aproach 1: pooling**

  * Ler a cada x milisegundos
  * leia rápido o suficiente para não incomodar o usuário, mas devagar o suficiente para não ler duas vezes durante o bouncing 

* **Aproach 2: periodic counting**

  * mede periodicamente

  * Se n amostras estáveis consecutivas são obtidas, considera-se que a chave mudou o estado.

  * período: de 1 a 10ms

  * n= aproximadamente 12

    ![image-20200414212852577](Images - 1.0 - Microcontrollers/image-20200414212852577.png)

* **Aproach 3**

  * by hardwar

  ![image-20200414211620869](Images - 1.0 - Microcontrollers/image-20200414211620869.png)

  ![image-20200414211631155](Images - 1.0 - Microcontrollers/image-20200414211631155.png)

## RTC

* Real Time Clock
* Some microcntrollers have special hardware to do real time clock
* RTCC: RTC that can implementar a Calendar
* A typical clock source in RTC applications is a 32.768 kHz crystal. Since this
  base frequency can be successively divided by two until reaching exactly 1Hz

## PWM

* **PWM com modulação rápida**

  * Pulse Width modulation

  * Frequência: de 0 a ffff (top, $2^n$)

  * Duty cicle: $\frac{t_{on}}{t}=\frac{CCR}{top}$

  * Count mode: continous

  * (-) Muda duty cicle, muda a fase

  * (+) Conseguimos contar em uma frequência maior

    ![image-20200429113507444](Images - 1.0 - Microcontrollers/image-20200429113507444.png)

* **PWM com fase corrigida**

  * Count mode: up-down
  * Frequência: top*2
  * Duty Cicle: $\frac{CCR}{top}$
  * Fase fica sempre centralizada
  
* **PWM por software**
  
  * vc pode gerar pwm por software usando delays, contudo gera muito jitter no sinal.   Com irq, vc comuta o pino na isr. Usa-se essa técnica qnd o número de.comparadores do timer em hardware é insuficiente para aplicação. Por exemplo, no avr há apenas 2 comparadores.
  
* **Controlar velocidadede motor CC**
  
  * como é a relação? Linear? 
  * E o torque? 
  
* **Servo motor**
  
  * 2 de alimentação, 1 de controle
  * PWM define o ângulo (regra de tres simples)
  * (+) torque alto

## Expansão de IO

* Usar algum protocolo de comunicação para controlar IOs com menos pinos
* **PCF8574**
  * I2C
  * https://www.filipeflop.com/produto/modulo-serial-i2c-para-display-lcd-arduino/

























