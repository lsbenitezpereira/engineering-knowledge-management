

#define F_CPU 16000000UL
#define LED1				PD2
#define LED2 				PD5
#define LEDS_DDRD			DDRD
#define LEDS_PORT			PORTD

#define setBit(byte, bit)	byte = byte | (1 << bit)
#define clrBit(byte, bit)	byte = byte & (~(1 << bit))

#include <avr/io.h>
#include <util/delay.h>

int main()
{
	char i;
	// Inicializa��es
	LEDS_DDRD = 0xFF;

	// La�o principal
	while(1){
		for(i = 0; i < 8; i++)
		{
		//	LEDS_PORT = (1 << i);
			setBit(LEDS_PORT,i);
			_delay_ms(500);
		}
		LEDS_PORT = 0x00;
	}

	return 0;
}
