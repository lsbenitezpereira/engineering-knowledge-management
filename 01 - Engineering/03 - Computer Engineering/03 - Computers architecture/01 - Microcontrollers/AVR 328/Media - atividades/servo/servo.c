/* -----------------------------------------------------------------------------
 * File:			Timer.c
 * Project:			Exemplo de Uso do Timer
 * Author: 			Raffaele Parise & Hazael dos Santos Batista
 * Data de edi��o:	26/08/2014
 * -------------------------------------------------------------------------- */

// System Definitions
#define F_CPU 16000000
#define MCU_8_BITS

// Header Files
#include "LS_defines.h"
#include "LS_ATmega328.h"

#define INC		10
#define CLOCKWISE	0
#define CCLOCKWISE	1

// Main Function
int main()
{
	uint16 value = 1000;
	uint8 direction = CLOCKWISE;

	// Port Configuration
	setBit(DDRB, PB1);

	// Timer1 Configuration
	timer1ClockPrescaller8();
	timer1PhaseFrequencyCorrectPWMICR1Mode();
	timer1OC1ANonInvertedMode();
	ICR1 = 20000;
	timer1SetCompareAValue(value);

	while(1){ 
		/*if(direction == CLOCKWISE){
			value += INC;
			if(value >= 2000){
				value = 2000;
				direction = CCLOCKWISE;
			}
		} else {
			value -= INC;
			if(value <= 1000){
				value = 1000;
				direction = CLOCKWISE;
			}
		}
		timer1SetCompareAValue(value);
		*/
		timer1SetCompareAValue(2000);
		_delay_ms(30);
	}

	return 0;
}
