/* -----------------------------------------------------------------------------
 * File:			Acelerometro.c
 * Project:			Teste de uso de Girosc�pio, apesar do t�tulo
 * Author:			Leandro Schwarz
 * Last modified:	15/09/2014
 * -------------------------------------------------------------------------- */

// System Definitions
#define F_CPU	16000000UL
#define MCU_8_BITS

// Header Files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"

// Project Definitions
#define ST_IDLE 0
#define ST_UPDATE 1

#define IST_READ_X 0
#define IST_READ_Y 1

// Global Variables
volatile int16 x, y;
volatile uint8 int_state = IST_READ_X;
volatile uint8 state = ST_IDLE;

// Main Function
int main()
{
	
	lcdInit();
	lcdStdio();
	
	adcReferenceInternal();
	adcClockPrescaler128();
	adcDisableAutomaticMode();
	adcTriggerContinuous();
	adcDisableDigitalInput0();
	adcDisableDigitalInput1();
	adcSelectChannel(ADC0);
	adcActivateInterrupt();
	adcEnable();
	
	sei();
	
	while(1){
	
		switch(state){
		case ST_IDLE:
			break;
		case ST_UPDATE:
			printf("X = %d\nY = %d\n", x, y);
			break;
		}
	}
	
	return 0;
}

ISR(ADC_vect)
{
	switch(int_state){
	case IST_READ_X:
		x = ADC;
		int_state = IST_READ_Y;
		adcSelectChannel(ADC1);
		break;
	case IST_READ_Y:
		y = ADC;
		int_state = IST_READ_X;
		adcSelectChannel(ADC0);
		break;
	}
	
	state = ST_UPDATE;
}