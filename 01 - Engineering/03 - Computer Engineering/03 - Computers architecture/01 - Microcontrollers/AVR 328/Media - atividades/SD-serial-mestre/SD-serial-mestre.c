/* -----------------------------------------------------------------------------
 * File:			SD-serial-mestre.c
 * Project:			USART comunication with an SD card
 * Author:			Leonardo Santiago
 * Last modified:	06/10/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS


// Header files
#include <stdlib.h>
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"


//Projet definitions
#define IDLE 			0
#define UPDATE_DISPLAY 	1


//Global variables
volatile uint8	state = IDLE;


typedef struct system_flags_t{
	uint8 					: 1;
	uint8 adcChannel		: 1;
	uint8 unnused bits		: 1;
} system_flags_t;

state_t state;



int main(){

	//timer0 configuration
	timer0CTCMode();
	timer0SetCounterValue(155);
	timer0ClearCompareAInterruptRequest();
	timer0ActivateCompareAInterrupt();
	timer0ClockPrescaller1024();

	//interrupt configuration
	int0PullUpEnable();
	int0DDRConfigure();
	int0SenseRisingEdge();
	int0ClearInterruptRequest();
	int0ActivateInterrupt()	;

	// Configuração da USART
	usart8DataBits();
	usartSingleStopBit();
	usartParityNone();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartAsynchronousMode();
	usartActivateReceptionCompleteInterrupt();
	usartEnableReceiverBuffer();
	usartStdio();
	
	usartInit(9600);

	sei();

	while(1){
		switch(state){
		case IDLE:
			break;
		
		case UPDATE_DISPLAY:
			state = IDLE;
			break;
		}
	}	//while(1)
	return 0;
}

ISR(INT0_vect){
		printf("START\r");
}

ISR(TIMER0_COMPA_vect){
}
