/* -----------------------------------------------------------------------------
 * File:			Timer.c
 * Project:			Exemplo de Uso do Timer
 * Author: 			Leonardo S. e Raffaele parise
 * Data de edi��o:	26/08/2014
 * -------------------------------------------------------------------------- */

// System Definitions
#define F_CPU 16000000
#define MCU_8_BITS

// Header Files
#include "LS_defines.h"
#include "LS_ATmega328.h"

// Main Function
int main()
{

	setBit(DDRD, PD3);
	setBit(DDRD, PD4);
	setBit(PORTB, PB3);
	clrBit(DDRB, PB3);

	// Timer Configurations

	timer1ClockPrescaller1024();
	timer1CTCMode();
	timer1ActivateCompareAInterrupt();
	timer1SetCompareAValue(7812);
	sei();

	while(1){

		if(isBitClr(PINB, PB3)){

			while(isBitClr(PINB, PB3))
				;

			cplBit(PORTD, PD3);
	
		}
		
	}

	return 0;
}

ISR(TIMER1_COMPA_vect){

	cplBit(PORTD, PD4);

	return;
}
