/* -----------------------------------------------------------------------------
 * File:			externalInterrupt1.c
 * Project:			External interrupt example
 * Author:			Hazael dos Santos Batista
 * Last modified:	11/09/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS


// Header files
#include <stdlib.h>
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"


//Projet definitions
#define IDLE 			1
#define UPDATE_DISPLAY 	2
#define MOVE_MOTOR		3

#define READ_KEYPAD		1
#define READ_MOTOR		2

#define TOLERANCE		9

#define MOTOR_DDR		DDRC
#define MOTOR_PORT		PORTC
#define MOTOR_1_PIN		PC4
#define MOTOR_2_PIN		PC5


//Global variables
volatile uint8	state = UPDATE_DISPLAY;
volatile uint8 	adc_state = READ_MOTOR;

volatile double motor_angle;
volatile int16	angle = 0;
volatile int16	last_read;
volatile uint8	clear_angle = 0;


volatile int 	module = 1;


// Main function
int main()
{	
	//Port configuration
	setBit(DDRD, PD0);
	setBit(MOTOR_DDR, MOTOR_1_PIN);
	setBit(MOTOR_DDR, MOTOR_2_PIN);


	//LCD configuration
	lcdInit();
	lcdStdio();
	printf("  Atividade 2  \n  Leonardo S.  \n");
	_delay_ms(500);
	lcdClearScreen();
	

	//ADC configfuration 
	adcReferenceAvcc();
	adcClockPrescaler128();
	adcSelectChannel(ADC1);
	adcDisableDigitalInput0();
	adcDisableDigitalInput1();
	
	adcActivateInterrupt();
	adcEnable();


	//Timer configuration
	timer1CTCMode();
	timer1ClockPrescaller8();
	timer1SetCompareAValue(10000);
	timer1ClearCompareAInterruptRequest();
	timer1ActivateCompareAInterrupt();
	/*timer0ClockPrescaller1024();
	timer0CTCMode();
	timer0ActivateCompareAInterrupt();
	timer0SetCompareAValue(155);*/


	sei();

	while(1){
		switch(state){
		case IDLE:
			setBit(PORTD, PD0);
			clrBit(PORTD, PD0);
			break;
		
		case UPDATE_DISPLAY:
			printf("Angle = %d\nMotor = %d\n", angle, (int)motor_angle);
			state = IDLE;
			break;

		case MOVE_MOTOR:
			printf("Angle = %d\nMotor = %d\n", angle, (int)motor_angle);
			if ((int)motor_angle - 2 < angle && (int)motor_angle + 2 > angle){
				clrBit(MOTOR_PORT, MOTOR_1_PIN);
				clrBit(MOTOR_PORT, MOTOR_2_PIN);

				clear_angle = 1;

				setBit(PORTD, PD0);
				clrBit(PORTD, PD0);

				state = UPDATE_DISPLAY;
			}
			else if ((int)motor_angle < angle){
				setBit(MOTOR_PORT, MOTOR_1_PIN);
				clrBit(MOTOR_PORT, MOTOR_2_PIN);
			}
			else if ((int)motor_angle > angle){
				setBit(MOTOR_PORT, MOTOR_2_PIN);
				clrBit(MOTOR_PORT, MOTOR_1_PIN);
			}	
			break;
		}
	}

	return 0;

}


ISR(ADC_vect)
{
	switch (adc_state){
	case READ_KEYPAD:
		if(ADC != last_read && state != MOVE_MOTOR){
			if (clear_angle == 1){
				angle = 0;
				clear_angle = 0;
			}

			if (labs((int16)ADC - 146) < TOLERANCE)
				angle = (labs(angle)*10+0)*module;

			if (labs((int16)ADC - 371) < TOLERANCE)
				angle = (labs(angle)*10+1)*module;

			if (labs((int16)ADC - 334) < TOLERANCE)
				angle = (labs(angle)*10+2)*module;

			if (labs((int16)ADC - 294) < TOLERANCE)
				angle = (labs(angle)*10+3)*module;

			if (labs((int16)ADC - 481) < TOLERANCE)
				angle = (labs(angle)*10+4)*module;

			if (labs((int16)ADC - 456) < TOLERANCE)
				angle = (labs(angle)*10+5)*module;

			if (labs((int16)ADC - 429) < TOLERANCE)
				angle = (labs(angle)*10+6)*module;

			if (labs((int16)ADC - 560) < TOLERANCE)
				angle = (labs(angle)*10+7)*module;

			if (labs((int16)ADC - 542) < TOLERANCE)
				angle = (labs(angle)*10+8)*module;

			if (labs((int16)ADC - 522) < TOLERANCE)
				angle = (labs(angle)*10+9)*module;

			if (labs((int16)ADC - 248) < TOLERANCE){
				//module = (module == 1) ? -1 : 1;
				module *= -1;
				state = UPDATE_DISPLAY;
				/*if (module == 1)
					module = -1;
				else
					module = 1;*/
			}

			if (labs((int16)ADC - 79) < TOLERANCE)
				state = MOVE_MOTOR;

			if (labs((int16)ADC - 204) < TOLERANCE)
				angle = 0;

			if (angle > 135)
				angle = 135;

			if (angle < -135)
				angle = -135;
		}
		
		adc_state = READ_MOTOR;
		last_read = ADC;
		break;

	case READ_MOTOR:
		motor_angle =	((((double)ADC*270)/1024.0)-135.0)*-1;
		adc_state = READ_KEYPAD;
		break;
	}
	if (state != MOVE_MOTOR)
		state = UPDATE_DISPLAY;
}	

ISR(TIMER1_COMPA_vect)
{
	switch (adc_state){
	case READ_KEYPAD:
		adcSelectChannel(ADC0);
		break;

	case READ_MOTOR:
		adcSelectChannel(ADC1);
		break;

	}

	adcStartConversion();

}
