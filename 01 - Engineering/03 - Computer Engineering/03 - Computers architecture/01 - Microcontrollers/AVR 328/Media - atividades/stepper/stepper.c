/* -----------------------------------------------------------------------------
 * File:			stepper.c
 * Project:			Exemplo de motor de passo
 * Author: 			Hazael dos Santos Batista e Leonardo S.
 * Data de edi��o:	29/08/2014
 * -------------------------------------------------------------------------- */

// System Definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header Files
#include "LS_defines.h"
#include "LS_ATmega328.h"

// Project definitions
#define STEPPER_COIL1_DDR	DDRB
#define STEPPER_COIL1_PORT	PORTB
#define STEPPER_COIL1_BIT	PB0
#define STEPPER_COIL2_DDR	DDRB
#define STEPPER_COIL2_PORT	PORTB
#define STEPPER_COIL2_BIT	PB1
#define STEPPER_COIL3_DDR	DDRB
#define STEPPER_COIL3_PORT	PORTB
#define STEPPER_COIL3_BIT	PB2
#define STEPPER_COIL4_DDR	DDRB
#define STEPPER_COIL4_PORT	PORTB
#define STEPPER_COIL4_BIT	PB3

// Constant definitions
#define STATE_IDLE			0
#define STATE_MOTOR_MOVE	1

// Funcion Declaration
void stepperMove(uint8 step);

// Global variables
volatile uint8 stepperPosition = 0;

// Main Function
int main()
{
	//Port configuration
	setBit(STEPPER_COIL1_DDR, STEPPER_COIL1_BIT);
	setBit(STEPPER_COIL2_DDR, STEPPER_COIL2_BIT);
	setBit(STEPPER_COIL3_DDR, STEPPER_COIL3_BIT);
	setBit(STEPPER_COIL4_DDR, STEPPER_COIL4_BIT);

	// Timer0 configuration
	timer0ClockPrescaller1024();
	timer0CTCMode();
	timer0OC0AOff();

	timer0OC0BOff();
	timer0ActivateCompareAInterrupt();
	timer0SetCompareAValue(155);
	sei();
		
	while(1){
	}
	
	return 0;
}

void stepperMove(uint8 step)
{
	switch(step){
	case 0:
		setBit(STEPPER_COIL1_PORT, STEPPER_COIL1_BIT);
		clrBit(STEPPER_COIL2_PORT, STEPPER_COIL2_BIT);
		clrBit(STEPPER_COIL3_PORT, STEPPER_COIL3_BIT);
		clrBit(STEPPER_COIL4_PORT, STEPPER_COIL4_BIT);
		break;

	case 1:
		clrBit(STEPPER_COIL1_PORT, STEPPER_COIL1_BIT);
		setBit(STEPPER_COIL2_PORT, STEPPER_COIL2_BIT);
		clrBit(STEPPER_COIL3_PORT, STEPPER_COIL3_BIT);
		clrBit(STEPPER_COIL4_PORT, STEPPER_COIL4_BIT);
		break;

	case 2:
		clrBit(STEPPER_COIL1_PORT, STEPPER_COIL1_BIT);
		clrBit(STEPPER_COIL2_PORT, STEPPER_COIL2_BIT);
		setBit(STEPPER_COIL3_PORT, STEPPER_COIL3_BIT);
		clrBit(STEPPER_COIL4_PORT, STEPPER_COIL4_BIT);
		break;

	case 3:
		clrBit(STEPPER_COIL1_PORT, STEPPER_COIL1_BIT);
		clrBit(STEPPER_COIL2_PORT, STEPPER_COIL2_BIT);
		clrBit(STEPPER_COIL3_PORT, STEPPER_COIL3_BIT);
		setBit(STEPPER_COIL4_PORT, STEPPER_COIL4_BIT);
		break;

	default:
		clrBit(STEPPER_COIL1_PORT, STEPPER_COIL1_BIT);
		clrBit(STEPPER_COIL2_PORT, STEPPER_COIL2_BIT);
		clrBit(STEPPER_COIL3_PORT, STEPPER_COIL3_BIT);
		clrBit(STEPPER_COIL4_PORT, STEPPER_COIL4_BIT);
		break;
	}

	return;
}

ISR(TIMER0_COMPA_vect)
{
	stepperMove(stepperPosition);
	stepperPosition = (stepperPosition == 3)?0:(stepperPosition + 1);
	return;
}
