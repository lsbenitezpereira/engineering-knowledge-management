/* -----------------------------------------------------------------------------
 * File:			Atividade_07.c
 * Project:			Datalog on a SD card with POT, LDR, LM35 and RTC. 
 * Author:			Leonardo Santiago Benitez Pereira
 * Last modified:	18/11/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.H"
#include "LS_DS1307.h"

#include "ff.h"
#include "global.h"


// Project definitions
#define READ_LDR		0
#define READ_POT		1
#define READ_LM			2


// New data types
typedef struct systemFlags_t {
	volatile uint8 updateDisplay	: 1;
	volatile uint8 stateADC			: 2;
	volatile uint8 burnSD			: 1;
	volatile uint8 state			: 4;
	//uint8 unusedBits				: 0;
} systemFlags_t;


// Global Variables
uint8 hora, minuto, segundo, dia, mes, ano, diaSemana, modo;
uint16  miliSegundos;
volatile systemFlags_t systemFlags;
volatile uint16	row_pot = 0;
volatile uint16	row_temperature = 0;
volatile uint16 row_LDR = 0;

char temp_string[60];

FRESULT rc;				// Result code
FATFS fatfs;			// File system object
FIL fil;				// File object
DIR dir;				// Directory object
FILINFO fno;			// File information object
UINT 
bw;


//Function declaration
void fuck (FRESULT rc);


//---------------------------------------------------------------------------------------------
// Main function ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

int main()
{
	//LCD configuration
	lcdInit();
	lcdStdio();
	printf("  Atividade 2  \n  Leonardo S.  \n");
	_delay_ms(500);
	
	
	// Mount SD card
	rc = f_mount(&fatfs, "1", 1);
	if(rc)
		fuck(rc);
	rc = f_opendir(&dir, "");
	
	//Enter the directory
	rc = f_mkdir("DATA");
	if(rc == FR_EXIST)
		f_opendir(&dir, "/DATA");
	
	//Create/open the file
	rc = f_open(&fil,"/DATA/dados.csv", FA_WRITE | FA_CREATE_NEW);
	if (rc){
		rc = f_open(&fil,"/DATA/dados.csv", FA_WRITE | FA_OPEN_ALWAYS);
		if (rc)
			fuck(rc);
		f_lseek(&fil, f_size(&fil));
	}		
	else{
		//Write header
		strcpy(temp_string, "DATE;TIME;POT;LDR;TEMP\r\n");
		rc = f_write(&fil,temp_string,strlen(temp_string), &bw);
	}
	
	
	// Timer1 configfuration
	timer1SetCounterValue(0);
	timer1OC1AOff();
	timer1CTCMode();	
	timer1SetCompareAValue(24999);
	timer1ClearCompareAInterruptRequest();
	timer1ActivateCompareAInterrupt();
	timer1ClockDisable();
	
	//Global Interrupt enable
	sei();
	
	//ADC configuration 
	adcReferenceAvcc();
	adcClockPrescaler128();
	adcDisableDigitalInput0();
	adcDisableDigitalInput1();
	adcSelectChannel(ADC1);
	adcActivateInterrupt();
	adcDisable();
	
	// DS1307 configuration
	setBit(DDRC, PC4);
	setBit(DDRC, PC5);
	i2cMasterInit();
	ds1307EnableClock();
	ds1307SquareWave1Hz();
	
	//Interrupt configuration
	pcint23DDRConfigure();
	pcint23PullUpEnable();
	pcint23_16Enable();
	pcint23ActivateInterrupt();
	
	// Port configuration
	setBit(DDRC, PC3);	//Output
	setBit(DDRD, PD6);
	setBit(DDRB, PB0);
	
	// Variable initialization
	ds1307GetDate(&ano, &mes, &dia, &diaSemana);
	ds1307GetTime(&hora, &minuto, &segundo, &modo);
	systemFlags.updateDisplay	= 0;
	
	
	 while(1){
		if(systemFlags.updateDisplay){	
			systemFlags.updateDisplay = 0;
			
			//Read DS1307
			ds1307GetDate(&ano, &mes, &dia, &diaSemana);
			ds1307GetTime(&hora, &minuto, &segundo, &modo);
			miliSegundos = systemFlags.state*100;
			
			//Begin LCD_time
			setBit(PORTD, PD6);

			lcdCursorMoveFirstLine();
			switch(systemFlags.state){
			case 0:
				printf("  Conversao 00  \n    %.2d:%.2d:%.2d    \n", hora, minuto, segundo);
				break;
				
			case 1:
				printf("  Conversao 01  \n");
				break;
				
			case 2:
				printf("  Conversao 02  \n");
				break;
				
			case 3:
				printf("  Conversao 03  \n");
				break;
				
			case 4:
				printf("  Conversao 04  \n");
				break;
			
			case 5:
				printf("  Conversao 05  \n");
				break;
			
			case 6:
				printf("  Conversao 06  \n");
				break;
			
			case 7:
				printf("  Conversao 07  \n");
				break;
			
			case 8:
				printf("  Conversao 08  \n");
				break;
				
			case 9:
				printf("  Conversao 09  \n");
				systemFlags.state = 0;
				//adcDisable();
				timer1ClockDisable();
				break;
				
			default:
				printf("       pqp      \n");
			}
			
			//Finish LCD_time
			clrBit(PORTD, PD6);
			
			//Read sensors
			systemFlags.stateADC = READ_LDR;
			adcReferenceAvcc();
			adcSelectChannel(ADC0);
			adcEnable();
			adcStartConversion();
			
			systemFlags.state++;
		}
		
		if(systemFlags.burnSD){
			systemFlags.burnSD = 0;
			
			setBit(PORTC, PC3);//Begin write_time 
			sprintf(temp_string, "%02d-%02d-20%02d; %02d:%02d:%02d.%03d; %04d; %04d; %04d;\r\n",
								  dia, mes, ano, hora, minuto, segundo, miliSegundos, 
								  row_pot, row_LDR, row_temperature);
								  
			f_write(&fil,temp_string,strlen(temp_string), &bw);
			clrBit(PORTC, PC3);//Finish write_time
			
			setBit(PORTB, PB0);	//Begin save_Time 
			f_sync(&fil);
			clrBit(PORTB, PB0);	//Finish save_Time
		}
	}
	return 0;
}


//---------------------------------------------------------------------------------------------
// PCINT23 Interrupt(1hz) ---------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(PCINT2_vect)
{
	if(isBitSet(PIND, PD7)){
		//Activate timer1
		timer1ClearCompareAInterruptRequest();
		timer1SetCounterValue(24998);
		systemFlags.state = 0;
		timer1ClockPrescaller64();
		
	}
}

//---------------------------------------------------------------------------------------------
// Timer1 Interrupt(100ms CTC) ----------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(TIMER1_COMPA_vect)		
{		
	systemFlags.updateDisplay = 1;
}

//---------------------------------------------------------------------------------------------
// ADC Interrupt ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(ADC_vect)
{
	
	switch (systemFlags.stateADC){
	case READ_LDR:	
		row_LDR = ADC;
		//Now, lets read the POT
		systemFlags.stateADC = READ_POT;
		adcSelectChannel(ADC1);
		adcStartConversion();
		break;
		
	case READ_POT:
		row_pot = ADC;
		//Following the sequence, LM35
		systemFlags.stateADC = READ_LM;
		adcReferenceInternal();
		adcSelectChannel(ADC2);
		adcStartConversion();		
		break;

	case READ_LM:
		row_temperature = ADC;
		//In the end, burn everything  in SD
		systemFlags.burnSD = 1;
		break;
			
	}
}

//---------------------------------------------------------------------------------------------
// User Functions ----------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

void fuck (FRESULT rc)
{
	pcint23DeactivateInterrupt();
	timer1ClockDisable();
	adcDisable();
	
	printf("    game over   \n     error %u    ", rc);
	while(1);
}
