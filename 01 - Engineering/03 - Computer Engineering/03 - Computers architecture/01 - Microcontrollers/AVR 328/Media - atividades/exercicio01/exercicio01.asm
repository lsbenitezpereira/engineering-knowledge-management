
.include "m328pdef.inc"
.ORG 0x000

inicio:

	STS		UCSR0B, R16

	LDI		R16, 0xFF
	OUT		DDRD, R16
	LDI		R16, 0x01
	RJMP	principal

principal:

	OUT		PORTD, R16
	RCALL	atraso
	LSL		R16
	BRNE	principal
	LDI		R16, 0x01

	RJMP	principal

atraso:

	LDI		R19, 40

	volta:

		DEC		R17
		BRNE 	volta
		DEC		R18
		BRNE	volta
		DEC		R19
		BRNE	volta
		RET
