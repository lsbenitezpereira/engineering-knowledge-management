/* -----------------------------------------------------------------------------
 * File:			atividade_1.c
 * Project:			Atividade de acionamento de display multiplexado
 * Author: 			Leonardo Santiago
 * Data de edi��o:	01/09/2014
 * -------------------------------------------------------------------------- */
 
 // System Definitions

#define F_CPU	16000000UL
#define MCU_8_BITS

// Header Files

#include <avr/io.h>
#include <util/delay.h>

#include "LS_defines.h"
#include "LS_ATmega328.h"

// Project Definitions

#define DISPLAY_DATA_DDR		DDRD
#define DISPLAY_DATA_PORT		PORTD
#define DISPLAY_CONTROL_DDR		DDRB
#define DISPLAY_CONTROL_PORT	PORTB

#define BUTTON_1_DDR			DDRB
#define BUTTON_1_PORT			PORTB
#define BUTTON_1_PIN			PINB
#define BUTTON_1_BIT			PB4

#define BUTTON_10_DDR			DDRB
#define BUTTON_10_PORT			PORTB
#define BUTTON_10_PIN			PINB
#define BUTTON_10_BIT			PB6

#define COMMON_ANODE	0
#define COMMON_CATHODE	1

// Function Declaration

void update_display(unsigned int valor, unsigned char common);
unsigned char display7segmentos(unsigned char hex, unsigned char common);

// Global Variables

	volatile uint16 value = 1820;

int main()
{

	//Timer Configuration
	timer0ClockPrescaller1024();
	timer0CTCMode();
	timer0ActivateCompareAInterrupt();
	timer0SetCompareAValue(155);
	sei();
	

	// Port Configuration

	DISPLAY_DATA_DDR = 0xFF; 	// Output
	DISPLAY_CONTROL_DDR = 0x0F; // Output

	setBit(BUTTON_1_PORT, BUTTON_1_BIT);	// Pull-Up
	clrBit(BUTTON_1_DDR, BUTTON_1_BIT);		// Input
	setBit(BUTTON_10_PORT, BUTTON_1_BIT);  	// Pull-Up
	clrBit(BUTTON_10_DDR, BUTTON_1_BIT); 	// Input

	while(1){
		if(isBitClr(BUTTON_1_PIN, BUTTON_1_BIT)){
			_delay_ms(10);
			while(isBitClr(BUTTON_1_PIN, BUTTON_1_BIT)){
				;
			}
			value++;
			_delay_ms(10);
		}

		if(isBitClr(BUTTON_10_PIN, BUTTON_10_BIT)){
			_delay_ms(10);
			while(isBitClr(BUTTON_10_PIN, BUTTON_10_BIT)){
				;
			}
			value+=10;
			_delay_ms(10);
		}
	}//while(1) end
	return 0;
}//main() end

unsigned char display7segmentos(unsigned char hex, unsigned char common){
	unsigned char segments;
	switch(hex){
		case 0x00:	segments = 0b00111111; break;
		case 0x01:	segments = 0b00000110; break;
		case 0x02:	segments = 0b01011011; break;
		case 0x03:	segments = 0b01001111; break;
		case 0x04:	segments = 0b01100110; break;
		case 0x05:	segments = 0b01101101; break;
		case 0x06:	segments = 0b01111101; break;
		case 0x07:	segments = 0b00000111; break;
		case 0x08:	segments = 0b01111111; break;
		case 0x09:	segments = 0b01101111; break;
		case 0x0A:	segments = 0b01110111; break;
		case 0x0B:	segments = 0b01111100; break;
		case 0x0C:	segments = 0b00111001; break;
		case 0x0D:	segments = 0b01011110; break;
		case 0x0E:	segments = 0b01111001; break;
		case 0x0F:	segments = 0b01110001; break;
		default:	segments = 0b01110001; break;
	}

	if(common == COMMON_ANODE){

		segments = ~segments;

	}

	return segments;

}



ISR(TIMER0_COMPA_vect){

	unsigned int unidade;
	unsigned int dezena;
	unsigned int centena;
	unsigned int milhar;

	unidade = (value) % 10;
	dezena = (value/10) % 10;
	centena = (value/100) % 10;
	milhar = (value/1000) % 10;

	DISPLAY_CONTROL_PORT = 0xFF;
	DISPLAY_DATA_PORT  = (DISPLAY_DATA_PORT & 0x80) | display7segmentos (unidade, COMMON_ANODE);
	DISPLAY_CONTROL_PORT = 0xFE;
	_delay_ms (15);

	DISPLAY_CONTROL_PORT = 0xFF;
	DISPLAY_DATA_PORT = (DISPLAY_DATA_PORT & 0x80) | display7segmentos (dezena, COMMON_ANODE);
	DISPLAY_CONTROL_PORT = 0xFD;
	_delay_ms (15);


	DISPLAY_CONTROL_PORT = 0xFF;
	DISPLAY_DATA_PORT = (DISPLAY_DATA_PORT & 0x80) | display7segmentos (centena, COMMON_ANODE);
	DISPLAY_CONTROL_PORT = 0xFB;
	_delay_ms (15);


	DISPLAY_CONTROL_PORT = 0xFF;
	DISPLAY_DATA_PORT = (DISPLAY_DATA_PORT & 0x80) | display7segmentos (milhar, COMMON_ANODE);
	DISPLAY_CONTROL_PORT = 0xF7;
	_delay_ms (15);

	return;
}
