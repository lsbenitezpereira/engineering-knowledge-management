// -----------------------------------------------------------------------------
// Arquivo:			ACELEROMETRO.c
// Modulo:			Aceler�metro
// Autores:			Leandro Schwarz
//					Mayara de Sousa
// Vers�o:			1.0
// Modificado em:	07/09/2011
// Observa��es:		Para utilizar, � necess�rio alterar os valor das constantes
//					LCD_DADOS, LCD_CONTROLE, LCD_RS, LCD_E e LCD_D4, definidas 
//					no arquivo de cabe�alho
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Arquivos necess�rios --------------------------------------------------------

#include <avr/io.h>
#include <util/delay.h>
#include "ACELEROMETRO.h"

// -----------------------------------------------------------------------------
// Fun��o:		ACEL_inicia
// Argumentos:	Nenhum
// Retono:		Nenhum
// Objetivo:	Come�a a ler os eixos do aceler�metro
// -----------------------------------------------------------------------------
				

