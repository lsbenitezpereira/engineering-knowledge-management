// -----------------------------------------------------------------------------
// File:			ATmega328.c
// Module:			Microcontroller basic interface
// Author:			Leandro Schwarz
// Version:			3.1
// Last edition:	09/04/2012
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Header files ----------------------------------------------------------------

#include "ATmega328.h"

// -----------------------------------------------------------------------------
// Function:	EEPROM_write
// Parameters:	data, address
// Return:		none
// Objective:	Writes the data at the given address of the EEPROM memory
// -----------------------------------------------------------------------------

void EEPROM_write(unsigned char data,unsigned int address)
{
	// Wait for completion of previous write
	while(EECR & (1 << EEPE));

	// Set up address and data registers
	EEAR = address;
	EEDR = data;

	// Write logical one to EEMPE
	EECR |= (1 << EEMPE);

	// Start EEPROM write by setting EEPE
	EECR |= (1 << EEPE);

	return;
}

// -----------------------------------------------------------------------------
// Function:	EEPROM_read
// Parameters:	address
// Return:		data
// Objective:	Reads the data at the given address of the EEPROM memory
// -----------------------------------------------------------------------------

unsigned char EEPROM_read(unsigned int address)
{
	// Wait for completion of previous write
	while(EECR & (1 << EEPE));

	// Set up address register
	EEAR = address;

	// Start EEPROM read by writing EERE
	EECR |= (1 << EERE);

	// Return data from data register
	return EEDR;
}
/*
// -----------------------------------------------------------------------------
// Function:	SPI_master_init
// Parameters:	clock_prescaller
// Return:		none
// Objective:	Configures the SPI to master mode
// -----------------------------------------------------------------------------

void SPI_master_init(unsigned char clock_prescaller)
{
	// Set MOSI and SCK output, all others input
	SPI_DDR = (1 << SPI_MOSI) | (1 << SPI_SCK);

	// Enables SPI, sets master mode and sets teh clock prescaller
	SPCR = (1 << SPE) | (1 << MSTR) | (clock_prescaller & 0x03);
	if(clock_prescaller > 3)
		set_bit(SPSR,SPI2X);
	else
		clr_bit(SPSR,SPI2X);

	return;
}

// -----------------------------------------------------------------------------
// Function:	SPI_master_transmit
// Parameters:	data
// Return:		none
// Objective:	Transmit data to SPI bus
// -----------------------------------------------------------------------------

void SPI_master_transmit(unsigned char data)
{
	// Start transmission
	SPDR = data;

	// Wait for transmission complete
	while(!(SPSR & (1 << SPIF)));

	return;
}

// -----------------------------------------------------------------------------
// Function:	SPI_slave_init
// Parameters:	none
// Return:		none
// Objective:	Configures the SPI to slave mode
// -----------------------------------------------------------------------------

void SPI_slave_init(void)
{
	// Set MISO output, all others input
	SPI_DDR = (1 << SPI_MISO);

	// Enable SPI
	SPCR = (1 << SPE);

	return;
}

// -----------------------------------------------------------------------------
// Function:	SPI_slave_receive
// Parameters:	none
// Return:		data
// Objective:	Receive the data from SPI bus
// -----------------------------------------------------------------------------

unsigned char SPI_slave_receive(void)
{
	// Wait for reception complete
	while(!(SPSR & (1 << SPIF)));

	// Return data register
	return SPDR;
}
*/
// -----------------------------------------------------------------------------
// Function:	USART_init
// Parameters:	none
// Return:		none
// Objective:	Configures the USART
// -----------------------------------------------------------------------------

void USART_init(void)
{
	// Set FE1, DOR1 and UPE1 bit to zero before writing to UCSR0A
	UCSR0A &= 0xE3;
	USART_control.frame_error			= 0;
	USART_control.buffer_overflow_error	= 0;
	USART_control.parity_error			= 0;

	// Sets the mode
	switch(USART_control.mode)
	{
		case USART_MODE_ASYNCHRONOUS_NORMAL:
			clr_bit(UCSR0A,U2X0);			// Normal speed
			UCSR0C &= 0x3F;					// Asynchronous mode
			clr_bit(UCSR0C,UCPOL0);			// Write zero in asynchronous mode
			USART_control.polarity = 0;
			USART_control.ubrr = (F_CPU / 16 / USART_BAUD) - 1;
			break;
		case USART_MODE_ASYNCRHONOUS_DOUBLE_SPEED:
			set_bit(UCSR0A,U2X0);				// Double speed
			UCSR0C &= 0x3F;						// Asynchronous mode
			clr_bit(UCSR0C,UCPOL0);				// Write zero in asynchronous mode
			USART_control.polarity = 0;
			USART_control.ubrr = (F_CPU / 8 / USART_BAUD) - 1;
			break;
		case USART_MODE_SYNCHRONOUS_NORMAL:
			clr_bit(UCSR0A,U2X0);				// Normal speed
			UCSR0C = (UCSR0C & 0x7F) | 0x40;	// Synchronous mode
			if(USART_control.polarity == USART_TRANSMITTER_RISING_EDGE)
				clr_bit(UCSR0C,UCPOL0);
			else
				set_bit(UCSR0C,UCPOL0);
			USART_control.ubrr = (F_CPU / 8 / USART_BAUD) - 1;
			break;
		case USART_MODE_SYNCHRONOUS_SPI:
			clr_bit(UCSR0A,U2X0);				// Normal speed
			UCSR0C |= 0xC0;						// Synchronous SPI mode
			if(USART_control.polarity == USART_TRANSMITTER_RISING_EDGE)
				clr_bit(UCSR0C,UCPOL0);
			else
				set_bit(UCSR0C,UCPOL0);
			USART_control.ubrr = (F_CPU / 2 / USART_BAUD) - 1;
			break;
	}

	// Sets the baud rate
	UBRR0H = (unsigned char)(USART_control.ubrr >> 8);
	UBRR0L = (unsigned char)USART_control.ubrr;

	// Sets the polarity
	if(USART_control.transmitter_enabled)
		set_bit(UCSR0B,TXEN0);
	if(USART_control.receiver_enabled)
		set_bit(UCSR0B,TXEN0);

	// Sets the parity
	switch(USART_control.parity)
	{
		case USART_PARITY_DISABLED:	UCSR0C &= 0xCF;						break;
		case USART_PARITY_EVEN:		UCSR0C = (UCSR0C & 0xEF) | 0x20;	break;
		case USART_PARITY_ODD:		UCSR0C |= 0x30;						break;
	}

	// Sets the number of stop bits
	if(USART_control.stop_bits == USART_SINGLE_STOP_BIT)
		clr_bit(UCSR0C,USBS0);
	else
		set_bit(UCSR0C,USBS0);

	// Sets the number of data bits
	switch(USART_control.data_bits)
	{
		case USART_5_DATA_BITS:	clr_bit(UCSR0B,UCSZ02);	clr_bit(UCSR0C,UCSZ01);	clr_bit(UCSR0C,UCSZ00);	break;
		case USART_6_DATA_BITS:	clr_bit(UCSR0B,UCSZ02);	clr_bit(UCSR0C,UCSZ01);	set_bit(UCSR0C,UCSZ00);	break;
		case USART_7_DATA_BITS:	clr_bit(UCSR0B,UCSZ02);	set_bit(UCSR0C,UCSZ01);	clr_bit(UCSR0C,UCSZ00);	break;
		case USART_8_DATA_BITS:	clr_bit(UCSR0B,UCSZ02);	set_bit(UCSR0C,UCSZ01);	set_bit(UCSR0C,UCSZ00);	break;
		case USART_9_DATA_BITS:	set_bit(UCSR0B,UCSZ02);	set_bit(UCSR0C,UCSZ01);	set_bit(UCSR0C,UCSZ00);	break;
	}

	return;
}

// -----------------------------------------------------------------------------
// Function:	USART_check_error
// Parameters:	none
// Return:		true in case of error
// Objective:	Checks if an error occurred during transmission or reception
// -----------------------------------------------------------------------------

unsigned char USART_check_error(void)
{
	USART_control.frame_error			= tst_bit(UCSR0A,FE0);
	USART_control.buffer_overflow_error	= tst_bit(UCSR0A,DOR0);
	USART_control.parity_error			= tst_bit(UCSR0A,UPE0);
	return (USART_control.parity_error | USART_control.buffer_overflow_error | USART_control.frame_error);
}

// -----------------------------------------------------------------------------
// Function:	USART_transmit
// Parameters:	data
// Return:		none
// Objective:	Transmits the data in 5, 6, 7 or 8 bit modes
// -----------------------------------------------------------------------------

void USART_transmit(char data)
{
	while(!USART_TRANSMITTER_BUFFER_EMPTY());
	UDR0 = data;
	return;
}

// -----------------------------------------------------------------------------
// Function:	USART_transmit_9bits
// Parameters:	data
// Return:		none
// Objective:	Transmits the data in 9 bit mode
// -----------------------------------------------------------------------------

void USART_transmit_9bits(unsigned int data)
{
	unsigned char aux;

	while(!USART_TRANSMITTER_BUFFER_EMPTY());
	aux = ((data & 0x100) >> 8);
	if(aux)
		set_bit(UCSR0B,TXB80);
	else
		clr_bit(UCSR0B,TXB80);
	UDR0 = (unsigned char)data;
	return;
}

// -----------------------------------------------------------------------------
// Function:	USART_transmit_std
// Parameters:	data and the standard output data stream handler
// Return:		always 0
// Objective:	Transmits the data in 5, 6, 7 or 8 bit modes using the stdio
// -----------------------------------------------------------------------------

int USART_transmit_std(char data,FILE * stream)
{
	while(!USART_TRANSMITTER_BUFFER_EMPTY());
	UDR0 = data;
	return 0;
}

// -----------------------------------------------------------------------------
// Function:	USART_receive
// Parameters:	none
// Return:		data
// Objective:	Receives the data in 5, 6, 7, 8 bit modes
// -----------------------------------------------------------------------------

unsigned char USART_receive(void)
{
	unsigned char status;

	while(!USART_RECEPTION_COMPLETE());

	status = UCSR0A;
	USART_control.frame_error = tst_bit(status,FE0);
	USART_control.buffer_overflow_error = tst_bit(status,DOR0);
	USART_control.parity_error = tst_bit(status,UPE0);
	return UDR0;
}

// -----------------------------------------------------------------------------
// Function:	USART_receive_9bits
// Parameters:	none
// Return:		data
// Objective:	Receives the data in 9 bit mode
// -----------------------------------------------------------------------------

unsigned int USART_receive_9bits(void)
{
	unsigned char status;
	unsigned char byteh;
	unsigned char bytel;
	unsigned int byte;

	while(!USART_RECEPTION_COMPLETE());
	status = UCSR0A;
	byteh = UCSR0B;
	bytel = UDR0;

	USART_control.frame_error = tst_bit(status,FE0);
	USART_control.buffer_overflow_error = tst_bit(status,DOR0);
	USART_control.parity_error = tst_bit(status,UPE0);

	byte = (byteh & 0x02) << 7;
	byte |= bytel;
	return  byte;
}

// -----------------------------------------------------------------------------
// Function:	USART_receive_std
// Parameters:	standard input data stream handler
// Return:		data
// Objective:	Receives the data in 5, 6, 7 or 8 bit modes using the stdio
// -----------------------------------------------------------------------------

int USART_receive_std(FILE * stream)
{
	while(!USART_RECEPTION_COMPLETE());
	return (int)UDR0;
}

// -----------------------------------------------------------------------------
// Function:	USART_esvazia_buffer_receptor
// Parameters:	none
// Return:		none
// Objective:	Clear the receptor data buffer
// -----------------------------------------------------------------------------

void USART_clear_buffer_receptor(void)
{
	unsigned char aux;
	while(USART_RECEPTION_COMPLETE())
		aux = UDR0;
	return;
}
