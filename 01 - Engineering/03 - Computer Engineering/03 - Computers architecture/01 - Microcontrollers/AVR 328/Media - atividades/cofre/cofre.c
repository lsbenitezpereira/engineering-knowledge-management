/* -----------------------------------------------------------------------------
 * File:			Cofre.c
 * Project:			exemplo de cofre
 * Author: 			Leonardo S. e Raffaele parise
 * Data de edi��o:	22/08/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_keypad.h"
#include "LS_HD44780.h"

//constant definition
#define STATE_IDLE			0
#define STATE_READ_PASS		1
#define STATE_CHECK_PASS	2

//function declaration
void updateScreen();

//global variables
uint8 state=STATE_IDLE;
uint8 passwordIndex = 0;
uint16 entry = 0;

// Main function
int main()
{
	uint8 key;
	uint16 password = 1202;

	usartDisableReceiver();
	usartDisableTransmitter();

	//LCD configuration
	lcdInit();
	lcdStdio();
	printf(" Projeto Cofre  \n   Versao 1.0   \n");
	_delay_ms(1000);
	lcdClearScreen();

	// Keypad Configuration
	keypadInit(	0x01, 0x02, 0x03, 0x0A,
				0x04, 0x05, 0x06, 0x0B,
				0x07, 0x08, 0x09, 0x0C,
				0x0E, 0x00, 0x0F, 0x0D);
	
	state = STATE_READ_PASS;
	updateScreen();
	while(1){
		//State mahines
		switch(state){
			case STATE_IDLE:
				break;

			case STATE_READ_PASS:
				key = keypadRead();
				if(key != 0xFF){
					if(key<0x0A){
						entry *= 10;
						entry += key;
						passwordIndex++;
						if(passwordIndex == 4)
							state = STATE_CHECK_PASS;
						updateScreen();
					}
					//passwordIndex = (passwordIndex == 3)?0:(passwordIndex + 1);
					updateScreen();
				}
				break;

			case STATE_CHECK_PASS:
				if(password == entry){
					lcdCursorMoveFirstLine();
					printf("   ACERTOU!!!   \n\n");
					_delay_ms(3000);
				}
				else {
					lcdCursorMoveFirstLine();
					printf("    Burro...    \n\n");
					_delay_ms(3000);
				}
			
				entry = 0;
				passwordIndex = 0;
				state = STATE_READ_PASS;
				updateScreen();
				break;
		}	//switch

	}	//while

	return 0;
}	//main

void updateScreen()
{
	uint8 i;

	lcdCursorMoveFirstLine();
	printf(" Senha: ");
	for(i = 0; i<passwordIndex; i++)
		printf("*");
	for(; i<4; i++) 
		printf("-");
	printf("\n%d\n", entry);

	return;
}
