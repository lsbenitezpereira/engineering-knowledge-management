/* -----------------------------------------------------------------------------
 * File:			com_sinc_transm.c
 * Project:			
 * Author:			Leandro Schwarz
 * Last modified:	25/09/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU	16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"

// Project definitions
#define STATE_IDLE			0
#define STATE_TRANSMISSION	1

#define COM_DDR		DDRD
#define COM_PORT	PORTD
#define COM_CLOCK	PD3
#define COM_DATA	PD4

// Function declaration

// Global variables
volatile uint16 rawADC = 0;
volatile uint8 transmState = 0;
volatile uint8 state = STATE_IDLE;

// main function
int main()
{
	setBit(COM_DDR, COM_CLOCK);
	setBit(COM_DDR, COM_DATA);
	setBit(COM_PORT, COM_CLOCK);
	setBit(COM_PORT, COM_DATA);

	// TIMER0 configuration
	timer0ClockPrescaller1024();
	timer0CTCMode();
	timer0ClearCompareAInterruptRequest();
	timer0SetCompareAValue(155);

	// ADC configuration
	adcReferenceInternal();
	adcClockPrescaler128();
	adcEnableAutomaticMode();
	adcTriggerTimer0CompareMatchA();
	adcSelectChannel(ADC0);
	adcDisableDigitalInput0();
	adcClearInterruptRequest();
	adcActivateInterrupt();
	adcEnable();
	
	// TIMER2 configuration
	timer2ClockPrescaller8();
	timer2CTCMode();
	timer2ClearCompareAInterruptRequest();
	timer2SetCompareAValue(49);

	sei();

	while(1){
		switch(state){
		case STATE_IDLE:
			break;
		case STATE_TRANSMISSION:
			if((transmState % 2) == 0){
				if(isBitSet(rawADC, (transmState / 2)))
					setBit(COM_PORT, COM_DATA);
				else
					clrBit(COM_PORT, COM_DATA);
				clrBit(COM_PORT, COM_CLOCK);
			}else
				setBit(COM_PORT, COM_CLOCK);
			transmState++;
			if(transmState == 20)
				timer2DeactivateCompareAInterrupt();
			state = STATE_IDLE;
			break;
		}
	}
	return 0;
}

ISR(ADC_vect)
{
	timer0ClearCompareAInterruptRequest();
	rawADC = ADC;
	transmState = 0;
	timer2ClearCompareAInterruptRequest();
	timer2ActivateCompareAInterrupt();
}

ISR(TIMER2_COMPA_vect)
{
	state = STATE_TRANSMISSION;
}