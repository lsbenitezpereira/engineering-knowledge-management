/* -----------------------------------------------------------------------------
 * File:			com_sinc_transA.c
 * Project:			
 * Author:			Leandro Schwarz
 * Last modified:	25/09/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU	16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"

// Project definitions
#define STATE_IDLE				0
#define STATE_RECEPTION			1
#define STATE_UPDATE_DISPLAY	2

#define COM_DDR		DDRD
#define COM_PORT	PORTD
#define COM_PIN		PIND
#define COM_CLOCK	PD2
#define COM_DATA	PD3

// Function declaration

// Global variables
volatile uint8 state = STATE_IDLE;
volatile uint8 receptionState = 0;

// main function
int main()
{
	uint32 value = 0;

	setBit(COM_PORT, COM_DATA);
	clrBit(COM_DDR, COM_DATA);

	// LCD configuration
	lcdInit();
	lcdStdio();

	// INT0 configuration
	int0PullUpEnable();
	int0DDRConfigure();
	int0SenseRisingEdge();
	int0ClearInterruptRequest();
	int0ActivateInterrupt();

	// TIMER0 configuration
	timer0ClockPrescaller8();
	timer0CTCMode();
	timer0SetCompareAValue(142);

	sei();

	while(1){
		switch(state){
		case STATE_IDLE:
			break;
		case STATE_RECEPTION:
			if(isBitSet(COM_PIN, COM_DATA))
				setBit(value, receptionState);
			else
				clrBit(value, receptionState);
			receptionState++;
			if(receptionState == 10){
				state = STATE_UPDATE_DISPLAY;
				timer0DeactivateCompareAInterrupt();
			}else
				state = STATE_IDLE;
			break;
		case STATE_UPDATE_DISPLAY:
			lcdCursorMoveFirstLine();
			value = value * 275;
			value /= 256;
			printf("Temp = %u,%u C\n\n", (uint16)(value / 10), (uint16)(value % 10));
			receptionState = 0;
			value = 0;
			state = STATE_IDLE;
		}
	}
	return 0;
}

ISR(INT0_vect)
{
	state = STATE_RECEPTION;
	if(receptionState == 0){
		timer0ClearCompareAInterruptRequest();
		timer0ActivateCompareAInterrupt();
	}
	timer0SetCounterValue(0);
}

ISR(TIMER0_COMPA_vect)
{
	receptionState = 0;
	timer0DeactivateCompareAInterrupt();
	state = STATE_IDLE;
}
