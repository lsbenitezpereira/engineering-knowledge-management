/* -----------------------------------------------------------------------------
 * File:			atividade05_control.c
 * Project:			MCO60408 Atividade 05 - USART - Módulo de controle
 * Author:			Hazael dos Santos Batista
 * Last modified:	07/10/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.H"

// Project definitions
#define STATE_IDLE		0
#define STATE_RX		1
#define STATE_REFRESH	2

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

// Global Variables
volatile uint16 pothg;
volatile uint16 lm35;

typedef struct package_t{
	int8 buffer[11];
	uint8 index					: 4;
	uint8 state					: 1;
	volatile uint8 stopRequest	: 1;
	volatile uint8 systemState	: 2;
} package_t;

package_t package;

// Main function
int main()
{
	// Inicialização do controle de pacote
	package.index = 0;
	package.state = 0;
	package.stopRequest = 0;
	package.systemState = STATE_IDLE;
	
	// Configuração do INT0
	int0SenseFallingEdge();
	int0PullUpEnable();
	int0DDRConfigure();
	int0ClearInterruptRequest();
	int0ActivateInterrupt();
	
	// Configuração da USART
	usart8DataBits();
	usartSingleStopBit();
	usartParityNone();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartAsynchronousMode();
	usartActivateReceptionCompleteInterrupt();
	usartEnableReceiverBuffer();
	usartStdio();
	
	usartInit(9600);
	
	// Configuração do LCD
	lcdInit();
	
	sei();
	
	while(1){
		switch(package.systemState){
		case STATE_IDLE:
			break;
		case STATE_RX:
			// Recepção de dados
			while(!usartIsReceiverBufferEmpty()){
				package.buffer[package.index] = usartGetDataFromReceiverBuffer();
				if(package.buffer[package.index] == '\r'){
					package.index = 0;
					package.systemState = STATE_REFRESH;
					break;
				}
				package.index++;
			}
			break;
		case STATE_REFRESH:
			// Checagem se há requisição de parada de comunicação
			if(package.stopRequest == 0){
				package.systemState = STATE_RX;
			} else {
				package.systemState = STATE_IDLE;
				package.stopRequest = 0;
				package.index = 0;
				break;
			}
			
			// Tratamento de pacote
			while(package.buffer[package.index] != '\r'){
				switch(package.buffer[package.index]){
				case 'T':
					lm35 = 0;
					package.state = 0;
					break;
				case 'P':
					pothg = 0;
					package.state = 1;
					break;
				default:
					// Extração de dados numéricos do pacote recebido
					if(package.buffer[package.index] >= '0' && package.buffer[package.index] <= '9'){
						switch(package.state){
						case 0:
							lm35 *= 10;
							lm35 += package.buffer[package.index] - '0';
							break;
						case 1:
							pothg *= 10;
							pothg += package.buffer[package.index] - '0';
							break;
						}
					}
				}
				package.index++;
			}
			
			// Escrita dos valores no display de LCD
			lcdStdio();
			lcdCursorMoveFirstLine();
			printf("LM35: %u C\nTensao: %u.%02u V\n",	(uint16)(((uint32)lm35 * 110) / 1023),			\
														(uint16)((((uint32)pothg * 500) / 1023) / 100), \
														(uint16)(((uint32)pothg * 500) / 1023) % 100);
														
			
			package.index = 0;
			break;
		}
	}
}

ISR(INT0_vect)
{
	// Envio de pacotes START e STOP para o módulo sensor
	usartStdio();
	if(package.systemState == STATE_IDLE){
		printf("START\r");
		package.systemState = STATE_RX;
	} else if(package.stopRequest == 0){
		printf("STOP\r");
		package.stopRequest = 1;
	}
}
