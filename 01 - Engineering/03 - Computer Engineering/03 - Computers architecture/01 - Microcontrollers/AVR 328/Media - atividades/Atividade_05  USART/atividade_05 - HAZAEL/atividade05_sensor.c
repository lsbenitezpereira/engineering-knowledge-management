/* -----------------------------------------------------------------------------
 * File:			atividade05_sensor.c
 * Project:			MCO60408 Atividade 05 - USART - Módulo sensor
 * Author:			Hazael dos Santos Batista
 * Last modified:	07/10/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include <string.h>

// Project definitions
#define STATE_RX			0
#define STATE_LM35			1
#define STATE_POTHG			2
#define STATE_TX			3
#define STATE_BUFFER_READ	4

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

// Global Variables
volatile uint16 pothg;
volatile uint16 lm35;
volatile uint8 state = STATE_RX;
uint8 stopRequest = 0;

typedef struct package_t{
	int8 buffer[11];
	uint8 index		: 4;
	uint8 state		: 1;
} package_t;

package_t package;

// Main function
int main()
{
	// Inicialização do controle de pacote
	package.index = 0;
	package.state = 0;

	// Configuração do TIMER0
	timer0CTCMode();
	timer0SetCompareAValue(155);
	timer0ActivateCompareAInterrupt();
	
	// Configuração do ADC
	adcClockPrescaler128();
	adcDisableDigitalInput0();
	adcDisableDigitalInput1();
	adcActivateInterrupt();
	
	// Configuração da USART
	usart8DataBits();
	usartSingleStopBit();
	usartParityNone();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartAsynchronousMode();
	usartActivateReceptionCompleteInterrupt();
	usartEnableReceiverBuffer();
	usartStdio();
	
	usartInit(9600);
	
	sei();
	
	while(1){
		switch(state){
		case STATE_RX:
			// Recepção de dados
			while(!usartIsReceiverBufferEmpty()){
				package.buffer[package.index] = usartGetDataFromReceiverBuffer();
				if(package.buffer[package.index] == '\r'){
					package.buffer[package.index] = '\0';
					package.index = 0;
					state = STATE_BUFFER_READ;
					break;
				}
				package.index++;
			}
			break;
		case STATE_TX:
			// Envio de dados
			printf("T%04uP%04u\r", lm35, pothg);
			
			// Checagem se há requisição de parada de comunicação
			if(stopRequest == 1){
				stopRequest = 0;
				adcDisable();
				timer0ClockDisable();
			}
			state = STATE_RX;
			break;
		case STATE_BUFFER_READ:
			// Lê o dado recebido e checa se é uma requisição de início ou parada de transmissão
			if(strcmp(package.buffer, "START") == 0){
				timer0SetCounterValue(0);
				timer0ClockPrescaller1024();
				adcEnable();
			} else if(strcmp(package.buffer, "STOP") == 0){
				stopRequest = 1;
			}
			state = STATE_RX;
		}
	}
}

ISR(TIMER0_COMPA_vect)
{
	// Inicia o processo de aquisição de dados via sensores
	state = STATE_LM35;
	
	adcReferenceInternal();
	adcSelectChannel(ADC0);
	
	adcStartConversion();
}

ISR(ADC_vect)
{
	// Aquisição de dados provenientes do ADC
	switch(state){
	case STATE_LM35:
		lm35 = ADC;
		state = STATE_POTHG;
		adcReferenceAvcc();
		adcSelectChannel(ADC1);
		adcStartConversion();
		break;
	case STATE_POTHG:
		pothg = ADC;
		state = STATE_TX;
		break;
	}
}