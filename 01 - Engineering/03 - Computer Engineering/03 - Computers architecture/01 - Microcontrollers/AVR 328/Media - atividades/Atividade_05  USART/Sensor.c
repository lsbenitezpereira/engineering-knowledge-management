/* -----------------------------------------------------------------------------
 * File:			Sensor.c
 * Project:			USART communication between two microcontrollers
 * Author:			Leonardo Santiago
 * Last modified:	07/12/2014
 * -------------------------------------------------------------------------- */
 
// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include <stdlib.h>
#include <string.h>
#include "LS_defines.h"
#include "LS_ATmega328.h"

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

//Projet definitions
#define SEND_DATA		0
#define READ_POT		0
#define READ_LM			1

// New data types
typedef struct systemFlags_t {
	volatile uint8 send_data		: 1;
	volatile uint8 adc_state		: 1;
	uint8 unusedBits				: 6;
} systemFlags_t;

//Global variables
systemFlags_t systemFlags;
volatile uint16	row_pot = 0;
volatile uint16	row_temperature = 0;
int8 buffer[11];
uint8 count = 0;

//---------------------------------------------------------------------------------------------
// Main function ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

int main()
{
	//Port configuration
	setBit(DDRB, PB0);
	setBit(DDRB, PB5);
	clrBit(PORTB, PB5);
	
	//Variable initialization
	systemFlags.adc_state = READ_POT;
	systemFlags.send_data = 0;
	
	//ADC configuration 
	adcReferenceAvcc();
	adcClockPrescaler128();
	adcDisableDigitalInput0();
	adcDisableDigitalInput1();
	adcActivateInterrupt();
	adcDisable();

	//Timer configuration
	timer0CTCMode();
	timer0ClockDisable();
	timer0SetCompareAValue(155);
	timer0ClearCompareAInterruptRequest();
	timer0ActivateCompareAInterrupt();

	// Configuração da USART
	usart8DataBits();
	usartParityNone();
	usartSingleStopBit();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartEnableReceiverBuffer();
	usartActivateReceptionCompleteInterrupt();
	usartAsynchronousMode();
	usartInit(9600);
	usartStdio();

	//Global Interrupt enable
	sei();
	
	while(1){
		//check USART buffer
		while(!usartIsReceiverBufferEmpty()){
			buffer[count] = usartGetDataFromReceiverBuffer();
			if(buffer[count] == '\r'){
				buffer[count] = '\0';
				count = 0;
				
				if(strcmp(buffer, "START") == 0){
					setBit(PORTB, PB5);
					timer0ClockPrescaller1024();
					timer0SetCounterValue(0);
					adcEnable();
				}
				
				if(strcmp(buffer, "STOP") == 0){
					clrBit(PORTB, PB5);
					timer0ClockDisable();
					adcDisable();
				}
				
				break;
			}
			else
				count++;
		}
			
		if(systemFlags.send_data){
			printf("T%04uP%04u\r", row_temperature, row_pot);
			systemFlags.send_data = 0;
		}
	}
	return 0;
}

//---------------------------------------------------------------------------------------------
// TIMER0 Interrupt(100Hz) --------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(TIMER0_COMPA_vect)
{
	setBit(PORTB, PB0);
	
	//read the potentiometer(ADC0)
	systemFlags.adc_state = READ_POT;
	adcReferenceAvcc();
	adcSelectChannel(ADC1);
	adcStartConversion();
	
	clrBit(PORTB, PB0);
}

//---------------------------------------------------------------------------------------------
// ADC Interrupt(channel 0~1 ) ------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(ADC_vect)
{
	switch (systemFlags.adc_state){
	case READ_POT:
		row_pot = ADC;
		//Read the LM35
		systemFlags.adc_state = READ_LM;	
		adcReferenceInternal();
		adcSelectChannel(ADC0);
		adcStartConversion();
		break;

	case READ_LM:
		row_temperature = ADC;
		//everything done, send the information via USART
		systemFlags.send_data = 1;
		break;
	}
}

