/* -----------------------------------------------------------------------------
 * File:			Base.c
 * Project:			USART communication between two microcontrollers
 * Author:			Leonardo Santiago
 * Last modified:	07/12/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include <stdlib.h>
#include <string.h>
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

// New data types
typedef struct systemFlags_t {
	volatile uint8 receive_on	: 1;
	uint8 update_display		: 1;
	uint8 display_off			: 1;
	uint8 unusedBits			: 5;
} systemFlags_t;

//Global variables
systemFlags_t systemFlags;
uint16	pot = 0;
uint16	temperature = 0;
int8 buffer[20];
uint8 count = 0;

//---------------------------------------------------------------------------------------------
// Main function ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

int main()
{
	// Variable initialization
	systemFlags.receive_on = 0;
	
	// Configuração da USART
	usart8DataBits();
	usartParityNone();
	usartSingleStopBit();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartEnableReceiverBuffer();
	usartActivateReceptionCompleteInterrupt();
	usartAsynchronousMode();
	usartInit(9600);
	
	//LCD configuration
	lcdInit();
	lcdStdio();
	printf("  Atividade 5  \n  Leonardo S.  \n");
	_delay_ms(500);
	
	//Interrupt configuration
	pcint8PullUpEnable();
	pcint8DDRConfigure();
	pcint8ActivateInterrupt();
	pcint14_8Enable();
	pcint14_8ClearInterruptRequest();
	
	//Global Interrupt enable
	sei();
	
	while(1){
		if(systemFlags.receive_on){
			
			//check USART buffer
			while(!usartIsReceiverBufferEmpty()){
				buffer[count] = usartGetDataFromReceiverBuffer();
				if(buffer[count] == '\r'){
					buffer[count] = '\0';
					count = 0;
					
					//extract the row values
					temperature =	((buffer[1])-'0')*1000 + ((buffer[2])-'0')*100 + ((buffer[3])-'0')*10 + ((buffer[4])-'0');
					pot			=	((buffer[6])-'0')*1000 + ((buffer[7])-'0')*100 + ((buffer[8])-'0')*10 + ((buffer[9])-'0');
					
					systemFlags.update_display = 1;
					break;
				}
				else
					count++;
			}
			
			if(systemFlags.update_display){
				systemFlags.update_display = 0;
				printf("Temp.: %u\nPot.: %u.%02u \n",	
						(uint16)(((uint32)temperature * 110) / 1023),			
						(uint16)((((uint32)pot * 500) / 1023) / 100), 
						(uint16)(((uint32)pot * 500) / 1023) % 100);	
			}
			
			if(systemFlags.display_off){
				systemFlags.display_off = 0;
				printf("      OFF       \npress the button\n");
			}
		}
	}
	
	return 0;
}

//---------------------------------------------------------------------------------------------
// PCINT8 Interrupt ---------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(PCINT1_vect)
{
	if(isBitClr(PINC, PC0)){
		usartStdio();
		if(systemFlags.receive_on){
			systemFlags.receive_on = 0;
			systemFlags.display_off = 1;
			
			printf("STOP\r");
		}
		else{
			systemFlags.receive_on = 1;
			printf("START\r");
		}
		lcdStdio();
	}
}










