#ifndef USART_H
#define USART_H

#include "global.h"
#include <avr/io.h>
#include <stdio.h>

//Inicialização da USART
void USART_Init(void);

//Envia um byte pela USART
void USART_SendByte(char data);

//Recebe um byte pela USART
char USART_ReceiveByte();

int USART_PutChar(char c, FILE *stream);

int USART_GetChar(FILE *stream);

#endif
