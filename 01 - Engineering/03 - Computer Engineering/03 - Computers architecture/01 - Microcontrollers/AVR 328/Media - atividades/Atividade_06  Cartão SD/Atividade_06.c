/* -----------------------------------------------------------------------------
 * File:			Atividade_06.c
 * Project:			File system to SD card
 * Author:			Leonardo Santiago
 * Last modified:	03/11/2014
 * -------------------------------------------------------------------------- */

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"

#include "ff.h"
#include "global.h"

#include <string.h>
#include <stdlib.h>

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

//type definition
typedef struct state_t{
	uint8 ls			: 1;
	uint8 tree			: 1;
	uint8 cdout			: 1;
	uint8 cd			: 1;
} state_t;

//Function declaration
void fuck (FRESULT rc);
FRESULT scan_files (char* path, char range);

//Global variables
char temp_string[40] = "Little file system by Leonardo Santiago";
char current_dir[80] = "";

int8 buffer[20];
uint8 n = 0;
state_t state;

FRESULT rc;				// Result code
FATFS fatfs;			// File system object
FIL fil;				// File object
DIR dir;				// Directory object
FILINFO fno;			// File information object
UINT bw;


int main(){
	// USART configuration
	usart8DataBits();
	usartSingleStopBit();
	usartParityNone();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartAsynchronousMode();
	usartEnableReceiverBuffer();
	usartIsReceptionComplete();
	usartActivateReceptionCompleteInterrupt();
	usartStdio();
	usartInit(9600);

	sei();

	// Mount SD card
	rc = f_mount(&fatfs, "1", 1);
	if(rc)
		fuck(rc);

	// Open the root directory
	rc = f_opendir(&dir, "");
	if(rc)
		fuck(rc);

	// Ceate read-me
	rc = f_open(&fil,"read-me.txt", FA_WRITE | FA_CREATE_ALWAYS);
	//Write in read-me
	rc = f_write(&fil,temp_string,sizeof(temp_string), &bw);	
	//Close read-me
	rc = f_close(&fil);
	
	while(1){
		while(!usartIsReceiverBufferEmpty()){			
			buffer[n] = usartGetDataFromReceiverBuffer();
			if(buffer[n] == '\r'){
				buffer[n] = '\0';
				n = 0;
				
				if(strcmp(buffer, "ls") == 0)
					state.ls = 1;

				else if(strcmp(buffer, "tree") == 0)
					state.tree = 1;

				else if(strcmp(buffer, "cd..") == 0)
					state.cdout = 1;
					
				else if(buffer[0] == 'c' && buffer[1] == 'd')
					state.cd = 1;
				
				break;
			}
			else
				n++;
		}

		if(state.ls){
			state.ls = 0;
			scan_files (current_dir, 0);
			printf("\r\n");
		}
			
		if(state.tree){
			state.tree = 0;
			strcpy (&temp_string[0], ""); 
			scan_files(temp_string, 1);
			printf("\r\n");
		}
		
		if(state.cd){
			state.cd = 0;
			n = 3;
			do{
				buffer [n-3] = buffer [n];
				n++;
			}while(buffer [n-1] != '\0');
			
			strcat (&current_dir[0], "/");
			strcat (&current_dir[0], &buffer [0]);	
			
			rc = f_opendir(&dir, current_dir);
			if(rc){
				for (n = strlen(current_dir); current_dir[n] != '/'; n--){}
				current_dir[n] = '\0';
				printf("the folder does not exist\r\n\n");
			}
			else
				printf("current dir = %s\r\n\n", current_dir);
				
			n = 0;
		}

		if(state.cdout){
			state.cdout = 0;
			if(strcmp(current_dir, "")){
				for (n = strlen(current_dir); current_dir[n] != '/'; n--){}
				current_dir[n] = '\0';
				rc = f_closedir(&dir);
				n = 0;
			}
			
			printf("current dir = %s \r\n", current_dir); 
			
		}
	}
	return 0;
}


void fuck (FRESULT rc){
	printf("Failed with error %u.\r\n", rc);
	while(1);
}	


FRESULT scan_files (char* path, char range){		// Start node to be scanned (also used as work area) 
													// range > 1 = Search in sub-directoris 
    DIR dir;
    int i;
    char *fn;  						 // This function is assuming non-Unicode cfg. 
#if _USE_LFN
    static char lfn[_MAX_LFN + 1];   // Buffer to store the LFN 
    fno.lfname = lfn;
    fno.lfsize = sizeof lfn;
#endif

    rc = f_opendir(&dir, path);								// Open the directory
    if (rc == FR_OK) {
        i = strlen(path);
        for (;;) {
            rc = f_readdir(&dir, &fno);						// Read a directory item
            if (rc != FR_OK || fno.fname[0] == 0) break;	// Break on error or end of dir
            if (fno.fname[0] == '.') continue;				// Ignore dot entry
#if _USE_LFN
            fn = *fno.lfname ? fno.lfname : fno.fname;
#else
            fn = fno.fname;
#endif
            if (fno.fattrib & AM_DIR) {						// It is a directory
                printf("%s/%s\r\n", path, fn);
				if(range){
					sprintf(&path[i], "/%s", fn);
					rc = scan_files(path, 1);
				}
                if (rc != FR_OK) break;
                path[i] = 0;
            } else {										// It is a file
                printf("%s/%s\r\n", path, fn);
            }
        }
        f_closedir(&dir);
    }
    return rc;
}

