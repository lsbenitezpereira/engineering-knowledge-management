
#define F_CPU 16000000UL

#define setBit(byte, bit)	byte = byte | (1 << bit)
#define clrBit(byte, bit)	byte = byte & (~(1 << bit))
#define cplBit(byte, bit)	byte = byte ^ (1 << bit)
#define isBitSet(byte, bit)	(((byte >> bit) & 0x01) == 1)
#define isBitClr(byte, bit)	(((byte >> bit) & 0x01) == 0)
#define andMask(byte, bit)	((byte >> bit) & 0x01)

#include <avr/io.h>
#include <util/delay.h>

int main()
{
	char count = 0;
	// Configuração
	clrBit(DDRB, PB5);
	setBit(PORTB, PB5);
	DDRD = 0xFF;

	while(1){
		if(isBitClr(PINB, PB5)){
			_delay_ms(10);
			while(isBitClr(PINB, PB5));
			_delay_ms(10);
			count = count + 1;
			if(count >= 64)
				count = 0;
		}

		PORTD = count;
	}

	return 0;
}
