/* -----------------------------------------------------------------------------
 * File:			ds1307.c
 * Project:			
 * Author:			Leandro Schwarz
 * Last modified:	16/10/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU	16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"
#include "LS_DS1307.h"

// Project definitions

// New data types
typedef struct systemFlags_t {
	uint8 updateRtc		: 1;
	uint8 unusedBits	: 7;
} systemFlags_t;

// Function declaration
char * nomeDiaSemana(uint8 dia);

// Global variables
volatile systemFlags_t systemFlags;

// main function
int main()
{
	// Variable declaration
	uint8 hora, minuto, segundo, dia, mes, ano, diaSemana, modo;

	// VAriable initialization
	systemFlags.updateRtc = 0;

	// LCD configuration
	lcdInit();
	lcdStdio();
	printf("  DS1307 - RTC  \n");
	printf("v1.0 sei la o que\n");

	// DS1307 configuration
	setBit(DDRC, PC4);
	setBit(DDRC, PC5);
	i2cMasterInit();
	sei();
	ds1307EnableClock();
	ds1307SquareWave1Hz();

	// INT0 configuration
	int0PullUpEnable();
	int0DDRConfigure();
	int0SenseRisingEdge();
	int0ClearInterruptRequest();
	int0ActivateInterrupt();

	// Button configuration
	setBit(PORTD, PD6);
	clrBit(DDRD, PD6);

	while(1){

		// Update RTC
		if(systemFlags.updateRtc){
			ds1307GetDate(&ano, &mes, &dia, &diaSemana);
			ds1307GetTime(&hora, &minuto, &segundo, &modo);
			lcdClearScreen();
			printf("%02d/%02d/20%02d %s\n", dia, mes, ano, nomeDiaSemana(diaSemana));
			printf("%02d:%02d:%02d %d\n", hora, minuto, segundo, modo);
			systemFlags.updateRtc = 0;
		}
		// Read button
		if(isBitClr(PIND, PD6)){
			ds1307SetDate(2, 2, 18, 2);
			ds1307SetTime(15, 23, 41, DS1307_24);
			waitUntilBitIsSet(PIND, PD6);
			systemFlags.updateRtc = 1;
		}
	}

	return 0;
}

char * nomeDiaSemana(uint8 dia)
{
	switch(dia){
		case 1: return "dom";
		case 2: return "seg";
		case 3: return "ter";
		case 4: return "qua";
		case 5: return "qui";
		case 6: return "sex";
		case 7: return "sab";
	}
	return NULL;
}

ISR(INT0_vect)
{
	systemFlags.updateRtc = 1;
}
