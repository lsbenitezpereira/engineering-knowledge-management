/* -----------------------------------------------------------------------------
 * File:			ADXL345.c
 * Module:			ADXL345 Accelerometer Interface
 * Author:			Leonardo Santiago
 * Version:			1.0
 * Last edition:	02/12/2014
 * -------------------------------------------------------------------------- */
 
// -----------------------------------------------------------------------------
// Header Files ----------------------------------------------------------------
#include "ADXL345.h"
  
// -----------------------------------------------------------------------------
// Global variables ------------------------------------------------------------
uint8 buffer[10];

/*************************** PRIVATE FUNCTIONS ********************************/
 
/* -----------------------------------------------------------------------------
 * Read multiples registers from the ADXL345.
 * uint8 address		First register address
 * uint8 *buf			Buffer to the incoming data
 * uint8 num			Number of registers to read
 * -------------------------------------------------------------------------- */

void ADXL345_read_register(uint8 address, uint8 *buf, uint8 num) 
{
	buf[0] = address;
	i2cMasterSendData(ADXL345_DEVICE, I2C_WRITE, buf, 1);		//write in pointer
	i2cMasterSendData(ADXL345_DEVICE, I2C_READ, buf, num);		//read the register
	i2cMasterReadFromBuffer(buf, num);							//put what was read in "data"
	return;
}

/* -----------------------------------------------------------------------------
 * Write in a single register of the ADXL345.
 * uint8 address		register address 
 * uint8 val			value of the register
 * -------------------------------------------------------------------------- */
 
void ADXL345_write_register(uint8 address, uint8 val) 
{
	uint8 aux[2];
	aux[0] = address;
	aux[1] = val;
	i2cMasterSendData(ADXL345_DEVICE, I2C_WRITE, aux, 2);
}

/***************************** POWER MODE *************************************/

/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * link, auto-sleep, sleep and wake-up selectable (POWER_CTL register)
 * -------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
 * Activate low power mode (less current consumption, higher noise).
 * -------------------------------------------------------------------------- */
 
void ADXL345_low_power_on()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	setBit(buffer[0], 4);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Deactivate low power mode (more current consumption, smaller noise).
 * -------------------------------------------------------------------------- */
 
void ADXL345_low_power_off()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	clrBit(buffer[0], 4);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Return 0 on normal mode, 1 on low power mode.
 * -------------------------------------------------------------------------- */
 
uint8 ADXL345_get_power_mode(){
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	return isBitSet(buffer[0], 4);
}


/* -----------------------------------------------------------------------------
 * Activate Measure Mode(normal read).
 * -------------------------------------------------------------------------- */
void ADXL345_measure_on()
{
	ADXL345_write_register(ADXL345_POWER_CTL, 8);
}


/* -----------------------------------------------------------------------------
 * Deactivate Measure Mode(minimum power consumption).
 * -------------------------------------------------------------------------- */
void ADXL345_measure_off()
{
	ADXL345_write_register(ADXL345_POWER_CTL, 0);
}

/* -----------------------------------------------------------------------------
 * Return 1 in measure on mode, 0 in off mode
 * -------------------------------------------------------------------------- */
 
uint8 ADXL345_get_measure_mode(){
	ADXL345_read_register(ADXL345_POWER_CTL, buffer, 1);
	return isBitSet(buffer[0], 3);
}

/**************************** RANGE SELECTION *********************************/

/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * self test, spi, int invert,
 * full res and justify selectable (DATA_FORMAT register)
 * Get format function (DATA_FORMAT register)
 * -------------------------------------------------------------------------- */
 
 
/* -----------------------------------------------------------------------------
 * Select full resolution mode.
 * In this configuration, the output resolution increases with the 
 * g range set by the range bits to maintain a 4 mg/LSB scale factor.
 * -------------------------------------------------------------------------- */
void ADXL345_full_resolution()
{
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_DATA_FORMAT, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select 2g range.
 * -------------------------------------------------------------------------- */
 
void ADXL345_range_2g(){
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	clrBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	ADXL345_write_register(ADXL345_DATA_FORMAT, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select 4g range.
 * -------------------------------------------------------------------------- */
void ADXL345_range_4g(){
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	setBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	ADXL345_write_register(ADXL345_DATA_FORMAT, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select 8g range.
 * -------------------------------------------------------------------------- */
void ADXL345_range_8g(){
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	clrBit(buffer[0], 0);
	setBit(buffer[0], 1);
	ADXL345_write_register(ADXL345_DATA_FORMAT, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select 16g range.
 * -------------------------------------------------------------------------- */
void ADXL345_range_16g(){
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	setBit(buffer[0], 0);
	setBit(buffer[0], 1);
	ADXL345_write_register(ADXL345_DATA_FORMAT, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Return the range of the ADXL345.
 * -------------------------------------------------------------------------- */
uint8 ADXL345_get_range(){
	ADXL345_read_register(ADXL345_DATA_FORMAT, buffer, 1);
	buffer[0] &= 0b00000011;
	switch(buffer[0]){
	case 0b00000000:
		return 2;
		
	case 0b00000001:
		return 4;
		
	case 0b00000010:
		return 8;
		
	case 0b00000011:
		return 16;
		
	default:
		return 0;
	}


}

/*************************** DATA OUTPUT RATE *********************************/

/* -----------------------------------------------------------------------------
 * Select the 3200Hz output data rate. 
 * Normal mode typical current consumption		= 140uA.
 * No energy economy in low power mode.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_3200()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	setBit(buffer[0], 0);
	setBit(buffer[0], 1);
	setBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 1600Hz output data rate. 
 * Normal mode typical current consumption		= 90uA.
 * No energy economy in low power mode.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_1600()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	clrBit(buffer[0], 0);
	setBit(buffer[0], 1);
	setBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 800Hz output data rate. 
 * Normal mode typical current consumption		= 140uA.
 * No energy economy in low power mode.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_800()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	setBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	setBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 400Hz output data rate. 
 * Normal mode typical current consumption		= 140uA.
 * Low power mode typical current consumption	= 90uA.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_400()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	clrBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	setBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 200Hz output data rate. 
 * Normal mode typical current consumption		= 140uA.
 * Low power mode typical current consumption	= 60uA.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_200()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	setBit(buffer[0], 0);
	setBit(buffer[0], 1);
	clrBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 100Hz output data rate. 
 * Normal mode typical current consumption		= 140uA.
 * Low power mode typical current consumption	= 50uA.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_100()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	clrBit(buffer[0], 0);
	setBit(buffer[0], 1);
	clrBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 50Hz output data rate. 
 * Normal mode typical current consumption		= 90uA.
 * Low power mode typical current consumption	= 45uA.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_50()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	setBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	clrBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 25Hz output data rate. 
 * Normal mode typical current consumption		= 60uA.
 * Low power mode typical current consumption	= 40uA.
 * -------------------------------------------------------------------------- */
 
void ADXL345_output_rate_25()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	clrBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	clrBit(buffer[0], 2);
	setBit(buffer[0], 3);
	ADXL345_write_register(ADXL345_BW_RATE, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Return the output data rate of ADXL345.
 * -------------------------------------------------------------------------- */
 
uint16 ADXL345_get_output_rate()
{
	ADXL345_read_register(ADXL345_BW_RATE, buffer, 1);
	buffer[0] &= 0b00001111;
	switch(buffer[0]){
	case 0b00001111:
		return 3200;
		
	case 0b00001110:
		return 1600;
		
	case 0b00001101:
		return 800;
		
	case 0b00001100:
		return 400;
		
	case 0b00001011:
		return 200;
		
	case 0b00001010:
		return 100;
		
	case 0b00001001:
		return 50;
		
	case 0b00001000:
		return 25;
		
	default:
		return 0;
	}
}

/************************** MEASURE FUNCTION **********************************/

/* -----------------------------------------------------------------------------
 * Because the precision of the ADXL345, the read come in 16 bits,
 * least significant byte first by default.
 * To can work with integer variables, the output is in milliGravity.
 * IMPORTANT!!!
 * This function only work in full resolution mode.
 * -------------------------------------------------------------------------- */

void ADXL345_get_acceleration(int16 *ax, int16 *ay, int16 *az){
	ADXL345_read_register(0x32, buffer, 6);					// Read the register
	*ax = (((int16)buffer[1]) << 8) | buffer[0];			//shift the data in x axis
	*ay = (((int16)buffer[3]) << 8) | buffer[2];			//shift the data in y axis
	*az = (((int16)buffer[5]) << 8) | buffer[4];			//shift the data in z axis
	
	// Convert the row read of acceleration into Gravity(x1000) unit
	*ax /= 0.256;
	*ay /= 0.256;
	*az /= 0.256;	
	
	return;
}
