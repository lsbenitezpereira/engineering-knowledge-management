/* -----------------------------------------------------------------------------
 * File:			L3G4200D.h
 * Module:			L3G4200D Gyroscope Interface
 * Author:			Leonardo Santiago
 * Version:			1.0
 * Last edition:	02/12/2014
 * -------------------------------------------------------------------------- */
 
 
// -----------------------------------------------------------------------------
// Header files ----------------------------------------------------------------

#include "LS_defines.h"
#if __LS_DEFINES_H < 86
	#error Wrong definition file (LS_defines.h).
#endif
#include "LS_i2c_master.h"


// -----------------------------------------------------------------------------
// Constants definitions -------------------------------------------------------
#define L3G4200D_DEVICE		105
#define CTRL_REG1			0x20
#define CTRL_REG2			0x21
#define CTRL_REG3			0x22
#define CTRL_REG4			0x23
#define CTRL_REG5			0x24


// -----------------------------------------------------------------------------
// Functions declarations ------------------------------------------------------

// Private functions
void L3G4200D_read_register(uint8 address, uint8 *buf, uint8 num);
void L3G4200D_write_register(uint8 address, uint8 val);

//power mode
void L3G4200D_power_down();
void L3G4200D_sleep_mode();
void L3G4200D_normal_mode();
uint8 L3G4200D_get_power_mode();

// Output rate
void L3G4200D_output_rate_800();
void L3G4200D_output_rate_400();
void L3G4200D_output_rate_200();
void L3G4200D_output_rate_100();
uint16 L3G4200D_get_output_rate();
 
// Range selection
void L3G4200D_range_250();
void L3G4200D_range_500();
void L3G4200D_range_2000();
uint16 L3G4200D_get_range();
 
//measure
void L3G4200D_get_angular_speed(int16 *gx, int16 *gy, int16 *gz);




/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * WHO I AM test.
 * High pass filter activation.
 * interrupt configuration
 * FIFO mode
 * SPI communication mode
 * -------------------------------------------------------------------------- */


