/* -----------------------------------------------------------------------------
 * File:			L3G4200D.c
 * Module:			L3G4200D Gyroscope Interface
 * Author:			Leonardo Santiago
 * Version:			1.0
 * Last edition:	02/12/2014
 * -------------------------------------------------------------------------- */
 
 
// -----------------------------------------------------------------------------
// Header Files ----------------------------------------------------------------
#include "L3G4200D.h"

// -----------------------------------------------------------------------------
// Global variables ------------------------------------------------------------
uint16 L3G4200D_range = 250;
uint8 buffer[10];

/************************** PRIVATE FUNCTIONS *********************************/

/* -----------------------------------------------------------------------------
 * Read multiples registers from the L3G4200D.
 * uint8 address		First register address
 * uint8 *buf			Buffer to the incoming data
 * uint8 num			Number of registers to read
 * -------------------------------------------------------------------------- */

void L3G4200D_read_register(uint8 address, uint8 *buf, uint8 num) 
{
	buf[0] = address;
	i2cMasterSendData(L3G4200D_DEVICE, I2C_WRITE, buf, 1);		//write in pointer
	i2cMasterSendData(L3G4200D_DEVICE, I2C_READ, buf, num);		//read the register
	i2cMasterReadFromBuffer(buf, num);							//put what was read in "data"
	return;
}

/* -----------------------------------------------------------------------------
 * Write in a single register of the L3G4200D_DEVICE.
 * uint8 address		register address 
 * uint8 val			value of the register
 * -------------------------------------------------------------------------- */
 
void L3G4200D_write_register(uint8 address, uint8 val) 
{
	uint8 aux[2];
	aux[0] = address;
	aux[1] = val;
	i2cMasterSendData(L3G4200D_DEVICE, I2C_WRITE, aux, 2);
}

/******************************** POWER MODE ***********************************/
/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * bandwidth selection, individual axis activation.
 * -------------------------------------------------------------------------- */
 

/* -----------------------------------------------------------------------------
 * Enter in power down mode.
 * Typical supply current			5uA 
 * -------------------------------------------------------------------------- */
	
void L3G4200D_power_down()
{	
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	clrBit(buffer[0], 3);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}
	
	
/* -----------------------------------------------------------------------------
 * Enter in power sleep.
 * Sleep mode introduces a faster turn-on time compared to power-down mode.
 * Typical supply current			1.5mA 
 * -------------------------------------------------------------------------- */
	
void L3G4200D_sleep_mode()
{	
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	clrBit(buffer[0], 0);
	clrBit(buffer[0], 1);
	clrBit(buffer[0], 2);
	setBit(buffer[0], 3);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Enter in normal mode(measure on).
 * Typical supply current			6.1mA 
 * -------------------------------------------------------------------------- */
	
void L3G4200D_normal_mode()
{	
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	setBit(buffer[0], 0);
	setBit(buffer[0], 1);
	setBit(buffer[0], 2);
	setBit(buffer[0], 3);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}
	
	
/* -----------------------------------------------------------------------------
 * Return the power mode: 1 for normal, 2 for sleep, 3 for power down.
 * -------------------------------------------------------------------------- */
 
uint8 L3G4200D_get_power_mode()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	buffer[0] &= 0b00001111;
	switch(buffer[0]){
	case 0b00001111:
		return 1;
		
	case 0b00001000:
		return 2;
		
	default:
		return 3;
	}
}
	
	
/******************************** OUTPUT RATE **********************************/

/* -----------------------------------------------------------------------------
 * Select the 800Hz output data rate. 
 * -------------------------------------------------------------------------- */
 
void L3G4200D_output_rate_800()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	setBit(buffer[0], 7);
	setBit(buffer[0], 6);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}
	
	
/* -----------------------------------------------------------------------------
 * Select the 400Hz output data rate. 
 * -------------------------------------------------------------------------- */
 
void L3G4200D_output_rate_400()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	setBit(buffer[0], 7);
	clrBit(buffer[0], 6);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 200Hz output data rate. 
 * -------------------------------------------------------------------------- */
 
void L3G4200D_output_rate_200()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	clrBit(buffer[0], 7);
	setBit(buffer[0], 6);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Select the 100Hz output data rate. 
 * -------------------------------------------------------------------------- */
 
void L3G4200D_output_rate_100()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	clrBit(buffer[0], 7);
	clrBit(buffer[0], 6);
	L3G4200D_write_register(CTRL_REG1, buffer[0]);
	return;
}


/* -----------------------------------------------------------------------------
 * Return the output data rate of the L3G4200D.
 * -------------------------------------------------------------------------- */
 
uint16 L3G4200D_get_output_rate()
{
	L3G4200D_read_register(CTRL_REG1, buffer, 1);
	buffer[0] &= 0b11000000;
	switch(buffer[0]){
	case 0b11000000:
		return 800;
		
	case 0b10000000:
		return 400;
		
	case 0b01000000:
		return 200;
		
	case 0b00000000:
		return 100;
		
	default:
		return 0;
	}
}
	
/****************************** RANGE SELECTION ********************************/
/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * Block data update, big/litlle endian and self test selectable.
 * -------------------------------------------------------------------------- */
 
/* -----------------------------------------------------------------------------
 * Select 250dps range.
 * -------------------------------------------------------------------------- */
 
void L3G4200D_range_250()
{
	L3G4200D_read_register(CTRL_REG4, buffer, 1);
	clrBit(buffer[0], 4);
	clrBit(buffer[0], 5);
	L3G4200D_write_register(CTRL_REG4, buffer[0]);
	L3G4200D_range = 250;
	return;
}


/* -----------------------------------------------------------------------------
 * Select 500dps range.
 * -------------------------------------------------------------------------- */
 
void L3G4200D_range_500()
{
	L3G4200D_read_register(CTRL_REG4, buffer, 1);
	setBit(buffer[0], 4);
	clrBit(buffer[0], 5);
	L3G4200D_write_register(CTRL_REG4, buffer[0]);
	L3G4200D_range = 500;
	return;
}	
	
	
/* -----------------------------------------------------------------------------
 * Select 2000dps range.
 * -------------------------------------------------------------------------- */
 
void L3G4200D_range_2000()
{
	L3G4200D_read_register(CTRL_REG4, buffer, 1);
	setBit(buffer[0], 4);
	setBit(buffer[0], 5);
	L3G4200D_write_register(CTRL_REG4, buffer[0]);
	L3G4200D_range = 2000;
	return;
}

	
/* -----------------------------------------------------------------------------
 * Return the range of L3G4200D.
 * -------------------------------------------------------------------------- */

uint16 L3G4200D_get_range()
{
	L3G4200D_read_register(CTRL_REG4, buffer, 1);
	buffer[0] &= 0b00110000;
	switch(buffer[0]){
	case 0b00000000:
		L3G4200D_range = 250;
		return 250;
		
	case 0b00010000:
		L3G4200D_range = 500;
		return 500;
		
	default:
		L3G4200D_range = 2000;
		return 2000;
	}
}
	
/********************************** MEASURE ***********************************/

/* -----------------------------------------------------------------------------
 * Because the precision of the L3G4200D, the read come in 16 bits,
 * least significant byte first by default.
 * To can work with integer variables, the output have 10x gain.
 * -------------------------------------------------------------------------- */
	
void L3G4200D_get_angular_speed(int16 *gx, int16 *gy, int16 *gz)
{
  	L3G4200D_read_register(0x28 | 0x80, buffer, 6);			// Read the register
	*gx = (((int16)buffer[1]) << 8) | buffer[0];			//shift the data in x axis
	*gy = (((int16)buffer[3]) << 8) | buffer[2];			//shift the data in y axis
	*gz = (((int16)buffer[5]) << 8) | buffer[4];			//shift the data in z axis

	//convert the row read of angular speed to d/s(x10)
	switch(L3G4200D_range){
	case 250:
		*gx *=-0.0875;
		*gy *=-0.0875;
		*gz *=-0.0875;
		break;
		
	case 500:
		*gx *=-0.175;
		*gy *=-0.175;
		*gz *=-0.175;
		break;
		
	case 2000:
		*gx *=-0.7;
		*gy *=-0.7;
		*gz *=-0.7;
		break;
	}
	return;
}
	
