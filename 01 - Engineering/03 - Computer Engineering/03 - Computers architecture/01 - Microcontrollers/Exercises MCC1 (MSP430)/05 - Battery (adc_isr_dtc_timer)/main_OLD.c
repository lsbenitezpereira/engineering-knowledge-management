//***************************************************************************************
//  MSP usage example
//  Author: Leonardo Benitez
//  Date: 2020-8
//***************************************************************************************

#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "lib/bits.h"
#include "lib/gpio.h"
#include "boardDefinitions/MSP430FR2355.h"
#include "battery_monitoring/battery_monitoring.h"

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Disable the GPIO power-on

    /* Initializations */
    battery_monitoring_init(1);

    SET_BIT(PORT_DIR(LED1_PORT), LED1_BIT); //debug led

    /* Habilita IRQs*/
    __bis_SR_register(GIE);

    while(1){

        //debug led
        CPL_BIT(PORT_OUT(LED1_PORT), LED1_BIT);

        battery_monitoring_check();

        __bis_SR_register(LPM0_bits | GIE);                      // Enter LPM0 w/ interrupts
        __no_operation();                                        // Only for debug
        __delay_cycles(5000);
        __no_operation();
    }
}

