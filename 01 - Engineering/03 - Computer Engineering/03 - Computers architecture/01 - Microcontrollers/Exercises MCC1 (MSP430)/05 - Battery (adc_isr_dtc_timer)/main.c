/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************
//  MSP430FR235x Demo - ADC, Sample A2/A1/A0, internal 1.5V Ref.
//
//  Description: This example works on Repeat-sequence-of-channels Mode
//  with TB1.1 as the trigger signal.
//  A2/A1/A0 is sampled 16ADCclks with reference to 1.5V.
//  Internal oscillator times sample (16x) and conversion(13x).
//  Inside ADC_ISR A2/A1/A0 sample value put into array ADC_Result[3].
//  ACLK = default REFO ~32768Hz, MCLK = SMCLK = default DCODIV ~1MHz.
//
//  Note: The TB1.1 is configured for 200us 50% PWM, which will trigger ADC
//  sample-and-conversion every 200us. The period of TB1.1 trigger event
//  should be more than the time period taken for ADC sample-and-conversion
//  and ADC interrupt service routine of each channel, which is about 57us in this code
//
//                MSP430FR2355
//             -----------------
//         /|\|                 |
//          | |                 |
//          --|RST              |
//            |                 |
//        >---|P1.2/A2          |
//        >---|P1.1/A1          |
//        >---|P1.0/A0          |
//
//
//   Eason Zhou
//   Texas Instruments Inc.
//   January 2020
//   Built with IAR Embedded Workbench v7.12.1 & Code Composer Studio v9.2.0
//******************************************************************************
#include <msp430.h>
#include <stdint.h>

volatile unsigned int ADC_Result[3];                                    // 12-bit ADC conversion result array
volatile unsigned char i;


void init_clock_24MHz(void) {

    // Configure two FRAM wait state as required by the device data sheet for MCLK
    // operation at 24MHz(beyond 8MHz) _before_ configuring the clock system.
    FRCTL0 = FRCTLPW | NWAITS_2 ;

    P2SEL1 |= BIT6 | BIT7;                       // P2.6~P2.7: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);           // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag

    __bis_SR_register(SCG0);                     // disable FLL
    CSCTL3 |= SELREF__XT1CLK;                    // Set XT1 as FLL reference source
    CSCTL0 = 0;                                  // clear DCO and MOD registers
    CSCTL1 = DCORSEL_7;                          // Set DCO = 24MHz
    CSCTL2 = FLLD_0 + 731;                       // DCOCLKDIV = 327358*731 / 1
    __delay_cycles(3);
    __bic_SR_register(SCG0);                     // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));   // FLL locked

    /* CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
     * set XT1 (~32768Hz) as ACLK source, ACLK = 32768Hz
     * default DCOCLKDIV as MCLK and SMCLK source
     - Selects the ACLK source.
     * 00b = XT1CLK with divider (must be no more than 40 kHz)
     * 01b = REFO (internal 32-kHz clock source)
     * 10b = VLO (internal 10-kHz clock source) (1)   */
    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
}

void battery_death_callback(){
    //TODO: return 0? debounce?
}


void (*_death_callback)() = 0;
void battery_monitoring_init(uint8_t f, void (*death_callback)()){
    _death_callback = death_callback;
    i = 2;

    // Configure ADC A0~2 pins
    P1SEL0 |=  BIT0 + BIT1 + BIT2;
    P1SEL1 |=  BIT0 + BIT1 + BIT2;

    // Configure reference
    PMMCTL0_H = PMMPW_H;                                        // Unlock the PMM registers
    PMMCTL2 |= INTREFEN;                                        // Enable internal reference
    __delay_cycles(4000);                                        // Delay for reference settling

    // Configure ADC
    ADCCTL0 |= ADCSHT_2 | ADCON;                                // 16ADCclks, ADC ON
    ADCCTL1 |= ADCSHP  | ADCCONSEQ_3;                 // ADC clock MODCLK, sampling timer, software trig.,repeat sequence
    ADCCTL2 &= ~ADCRES;
    ADCCTL2 |= ADCRES_2;                                        // 12-bit conversion results
    ADCMCTL0 |= ADCINCH_2 | ADCSREF_0;                          // A0~2(EoS); Vref=3.3
    ADCIE |= ADCIE0;                                            // Enable ADC conv complete interrupt

    //timer
    TB0CCTL0 |= CCIE;                             // TBCCR0 interrupt enabled
    TB0CCR0 = (uint16_t)f*2000;                  // CCR on that value. T=2ms
    TB0CTL = TBSSEL__SMCLK | MC__UP;              // SMCLK, UP mode
}

uint8_t battery_monitoring_death_policy (){
    // Referência de 3.3V
    // V = 3.3*adc[n]/4095

    // v_total = ADC_Result[1]*10/30 // batery 0 + batery 1, about 7V (before opamp)
    // v_bat0 = 10/15*ADC_Result[2] //tensão no ponto intermeriário, v_med
    // v_bat1 = v_total - v_bat0
    //TODO: esses calculos acima estão certos?
    //TODO: considerar algum tipo de "debouncer"
    i = 2;
    if (ADC_Result[1]<3846 || ADC_Result[2]<3846){
        return 1;
    } else {
        return 0;
    }
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;                                   // Stop WDT
    PM5CTL0 &= ~LOCKLPM5;
    init_clock_24MHz();
    battery_monitoring_init(24, &battery_death_callback);

    while(1)
    {
        __bis_SR_register(LPM0_bits | GIE);                      // Enter LPM0 w/ interrupts
        __no_operation();                                        // Only for debug
    }
}

// ADC interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC_VECTOR
__interrupt void ADC_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC_VECTOR))) ADC_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(ADCIV,ADCIV_ADCIFG))
    {
        case ADCIV_NONE:
            break;
        case ADCIV_ADCOVIFG:
            break;
        case ADCIV_ADCTOVIFG:
            break;
        case ADCIV_ADCHIIFG:
            break;
        case ADCIV_ADCLOIFG:
            break;
        case ADCIV_ADCINIFG:
            break;
        case ADCIV_ADCIFG:
            ADC_Result[i] = ADCMEM0;
            if(i == 0){
                i = 2;
                if (battery_monitoring_death_policy()){
                    (*_death_callback)();
                }
            } else{
                i--;
            }
            break;
        default:
            break;
    }
}


/* Timer0_B0 interrupt service routine
 * Período: 2ms
 * Utilizado para trigar o ADC
 * */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_B0_VECTOR
__interrupt void Timer0_B0_ISR (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_B0_VECTOR))) Timer0_B0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    ADCCTL0 |= ADCENC | ADCSC;                               // Sampling and conversion start
}
