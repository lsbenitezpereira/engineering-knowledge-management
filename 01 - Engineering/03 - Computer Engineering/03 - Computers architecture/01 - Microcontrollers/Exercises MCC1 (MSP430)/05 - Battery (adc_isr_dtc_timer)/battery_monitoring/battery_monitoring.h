/*
 * battery_monitoring.h
 *
 *  Created on: 21 de ago de 2020
 *      Author: benitez
 */

#ifndef BATTERY_MONITORING_BATTERY_MONITORING_H_
#define BATTERY_MONITORING_BATTERY_MONITORING_H_

#include <msp430.h>

unsigned int ADC_Result[3];             // 12-bit ADC conversion result array
unsigned char i;

//@Brief: initializations
//@Hardware: timer B0, ADC
//@Parameter f: frequency
void battery_monitoring_init(int f);

//@Brief: check if the values read are normal
//@Return: 1 (normal) or 0 (not normal)
//TODO: receive a callback function as parameter
int battery_monitoring_check();


#endif /* BATTERY_MONITORING_BATTERY_MONITORING_H_ */
