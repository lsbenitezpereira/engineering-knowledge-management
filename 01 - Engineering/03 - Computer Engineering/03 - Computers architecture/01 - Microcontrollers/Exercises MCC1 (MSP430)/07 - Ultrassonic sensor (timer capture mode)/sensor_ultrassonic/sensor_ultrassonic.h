/*
 * sensor_ultrassonic.h
 *
 *  Created on: 22 de ago de 2020
 *      Author: benitez
 */

#ifndef SENSOR_ULTRASSONIC_SENSOR_ULTRASSONIC_H_
#define SENSOR_ULTRASSONIC_SENSOR_ULTRASSONIC_H_

#include <msp430.h>
#include <stdint.h>
#include "lib/bits.h"

void sensor_ultrassonic_init(uint8_t f);
uint16_t sensor_ultrassonic_read();


#endif /* SENSOR_ULTRASSONIC_SENSOR_ULTRASSONIC_H_ */
