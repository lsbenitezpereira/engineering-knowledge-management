/*
 *  sensor_ultrassonic.c
 *  Created on: 22 de ago de 2020
 *  Author: benitez
 *
 *  Read two ultrassonic sensors HCSR04
 *  Timer em modo captura.
 *  Pinout:
 *    MSP Pin      Signal
 *    P2.0/TB1.1   Echo0 (needs 5V->3.3V conversion)
 *    P2.2         Trig0
 *    P2.1/TB1.2   Echo1 (needs 5V->3.3V conversion)
 *    P2.4         Trig1
 */

#include "sensor_ultrassonic.h"



uint8_t _f;
volatile uint16_t CCR1_countA;
volatile uint16_t CCR1_countB;
volatile uint8_t CCR1_state = 0;

volatile uint16_t CCR2_countA;
volatile uint16_t CCR2_countB;
volatile uint8_t CCR2_state = 0;

// @Parameter f: global frequency in MHz
void sensor_ultrassonic_init(uint8_t f){
	_f = f;
	//TODO: till which distance we'll read? Should I use a prescaler?

	CLR_BIT(P2DIR, BIT0 | BIT1); // input
	SET_BIT(P2REN, BIT0 | BIT1); // pull enable
	CLR_BIT(P2OUT, BIT0 | BIT1); // pull down
    P2SEL0 = BIT0 | BIT1; //Input capture for P2.0

	SET_BIT(P2DIR, BIT2 | BIT4); //output 
}

uint16_t sensor_ultrassonic_read(){


    // Echo goes high for a period of time which will be equal to the time taken for the US wave to return back to the sensor
    // Enable Timer
	TB1CCTL1 |= CM_3 | CCIS_0 | CCIE | CAP | SCS;
	TB1CCTL2 |= CM_3 | CCIS_0 | CCIE | CAP | SCS;
                                                    // Capture in both rising and falling edge,
                                                    // Use CCI0A,
                                                    // Synchronous capture,
                                                    // Enable capture mode,
                                                    // Enable capture interrupt

    TB1CTL |= TBSSEL_2 | MC_2 | TBCLR;              // Use SMCLK as clock source, clear TB1R


    // kept Trigger high for 10us
    int count = 0;
    int max = _f*10;
    SET_BIT(P2OUT, BIT2 | BIT4);
    while (count<=max){count++;}
    CLR_BIT(P2OUT, BIT2 | BIT4);

    __bis_SR_register(LPM4_bits | GIE);
    __no_operation();

    //after wakeup...
    CLR_BIT(TB1CCTL1, CCIE);
    CLR_BIT(TB1CCTL2, CCIE);

    //TODO: should I return the converted value? (CCR1_countB - CCR1_countA)*1715/(_f*10000), converted to mm
    //TODO: should I return both distances, just one (as now), the average...?
	return CCR1_countB - CCR1_countA;
}

// Timer1 Interrupt Handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER1_B1_VECTOR
__interrupt void TIMER1_B1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_B0_VECTOR))) TIMER1_B0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(TB1IV,0x0A))
    {
        /* Vector  0:  No interrupt */
        case  TB1IV_NONE:
            break;

        /* Vector  2:  TBCCR1 CCIFG -> Comparação 1*/
        case  TB1IV_TBCCR1:
            TB1CCTL1 &= ~CCIFG;
            if (CCR1_state==0){ //Rising edge,begin of the read
                CCR1_countA = TB1CCR1;
                CCR1_state=1;
            } else if (CCR1_state==1){ //falling edge, end of the read
                CCR1_countB = TB1CCR1;
                CCR1_state=2;
            }
            break;
        /* Vector  4:  TBCCR2 CCIFG -> Comparação 2*/
        case TB1IV_TBCCR2:
            TB1CCTL2 &= ~CCIFG;
            if (CCR2_state==0){ //Rising edge,begin of the read
                CCR2_countA = TB1CCR2;
                CCR2_state=1;
            } else if (CCR2_state==1){ //falling edge, end of the read
                CCR2_countB = TB1CCR2;
                CCR2_state=2;
            }
            break;

        /* Vector 10:  TBIFG -> Overflow do timer 0*/
        case TB1IV_TBIFG:

            break;
        default:
            break;
    }
    if (CCR1_state==2 && CCR2_state==2){
        CCR1_state = 0;
        CCR2_state = 0;

        __bic_SR_register_on_exit(LPM4_bits);// Desativa suspensão do main
    }

}


