//***************************************************************************************
//  MSP usage example
//  Author: Leonardo Benitez
//  Date: 2020-1
//  Brief: Read button1 (with interrupt) and then increment the value shown at an 7seg display
//***************************************************************************************

#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "lib/bits.h"
#include "lib/gpio.h"
#include "boardDefinitions/MSP430FR2355.h"
#include "displays/timerA_display_mux.h"

volatile uint8_t i = 0;

void config_ext_irq(){
    SET_BIT (PORT_REN(BUTTON1_PORT), BUTTON1_BIT); /* Pull up/down */
    SET_BIT (PORT_OUT(BUTTON1_PORT), BUTTON1_BIT); /* Pull up */
    SET_BIT (PORT_IE(BUTTON1_PORT), BUTTON1_BIT);  /* Habilitação da IRQ apenas botão */
    SET_BIT (PORT_IES(BUTTON1_PORT), BUTTON1_BIT); /* Transição de nível alto para baixo */
    CLR_BIT (PORT_IFG(BUTTON1_PORT), BUTTON1_BIT);  /* Limpa alguma IRQ pendente */
}

void init_clock_24MHz(void) {

    // Configure two FRAM wait state as required by the device data sheet for MCLK
    // operation at 24MHz(beyond 8MHz) _before_ configuring the clock system.
    FRCTL0 = FRCTLPW | NWAITS_2 ;

    P2SEL1 |= BIT6 | BIT7;                       // P2.6~P2.7: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);           // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag

    __bis_SR_register(SCG0);                     // disable FLL
    CSCTL3 |= SELREF__XT1CLK;                    // Set XT1 as FLL reference source
    CSCTL0 = 0;                                  // clear DCO and MOD registers
    CSCTL1 = DCORSEL_7;                          // Set DCO = 24MHz
    CSCTL2 = FLLD_0 + 731;                       // DCOCLKDIV = 327358*731 / 1
    __delay_cycles(3);
    __bic_SR_register(SCG0);                     // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));   // FLL locked

    /* CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
     * set XT1 (~32768Hz) as ACLK source, ACLK = 32768Hz
     * default DCOCLKDIV as MCLK and SMCLK source
     - Selects the ACLK source.
     * 00b = XT1CLK with divider (must be no more than 40 kHz)
     * 01b = REFO (internal 32-kHz clock source)
     * 10b = VLO (internal 10-kHz clock source) (1)   */
    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
}

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Disable the GPIO power-on
    SET_BIT(PORT_DIR(LED1_PORT), LED1_BIT);

    init_clock_24MHz();
    config_ext_irq();
    timerA_display_mux_init();

    /* Entra em modo de economia de energia */
    __bis_SR_register(LPM0_bits + GIE);
}

/* Port 2 ISR (interrupt service routine) */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_2 (void)
#else
#error Compiler not supported!
#endif
{

    timerA_display_mux_write(i++);
    CPL_BIT(PORT_OUT(LED1_PORT), LED1_BIT); /* Liga/desliga LED quando detectado borda no botão */
    CLR_BIT(PORT_IFG(BUTTON1_PORT), BUTTON1_BIT);/* Limpa sinal de IRQ*/
}
