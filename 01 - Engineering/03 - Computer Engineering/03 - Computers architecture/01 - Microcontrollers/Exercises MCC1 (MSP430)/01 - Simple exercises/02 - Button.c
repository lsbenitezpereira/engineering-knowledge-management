//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  Texas Instruments, Inc
//  July 2013
//***************************************************************************************

#include <msp430.h>
#include "lib/bits.h"

#define LED_1   BIT0 //P1.0
#define LED_2   BIT6 //P6.6
#define BUTTON_1  BIT1 //P4.1
#define BUTTON_2  BIT4 //P3.4


void main(void) {
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
                                            // to activate previously configured port settings
    P1DIR |= LED_1;       // Set to output direction
    P6DIR |= LED_2;       // Set to output direction

    P4REN |= BUTTON_1; //enable pull resistor
    P4OUT |= BUTTON_1; //pull is up

    P3REN |= BUTTON_2; //enable pull resistor
    P3OUT |= 0; //pull is up


    P1OUT ^= LED_1;
    for(;;) {
        volatile unsigned int i;            // volatile to prevent optimization

        //P1OUT ^= LED_1;                      // Toggle
        //P6OUT ^= LED_2;                      // Toggle

   /*     i = 20000;                          // SW Delay
        do i--;
        while(i != 0);*/

        if (!TST_BIT(P3IN, BUTTON_2)){

            P6OUT = 0;

        }
        else{
            P6OUT = LED_2;
        }


/*
        if (!TST_BIT(P4IN, BUTTON_1)){
            i = 20000;                          // SW Delay
            do i--;
            while(i != 0);
        }
        if (!TST_BIT(P3IN, BUTTON_2)){
            i = 20000;                          // SW Delay
            do i--;
            while(i != 0);
        }*/
    }
}
