/*
 *  NOT WORKING!!!
 *  NOT WORKING!!!
 *  NOT WORKING!!!
 *
 *  MSP430 usage example
 *  Author: Leonardo Benitez
 *  Coauthor: Renan Augusto Starke
 *  Date: 2020/10
 *  Pinout:
 *                          .   .
 *                         /|\ /|\
 *              PCF8574T   10k 10k     MSP430FR2355
 *              -------     |   |   -------------------
 *             |    SDA|<  -|---+->| P1.2/UCB0SDA      |-
 *             |       |    |      |                   |
 *             |       |    |      |                   |
 *             |       |    |      |                   |
 *             |    SCL|<----+-----| P1.3/UCB0SCL      |
 *              -------            |                   |
 *
 */

/* System includes */
#include <msp430.h>
#include <stdint.h>

/* Project includes */
#include "lib/bits.h"
#include "displays/I2C_lcd.h"

/**
  * @brief  Configura sistema de clock para usar o Digitally Controlled Oscillator (DCO) em 24MHz
  *         Essa configuração utiliza pinos para cristal externo.
  * @param  none
  *
  * @retval none
  */

void init_clock_system(void) {

    // Configure two FRAM wait state as required by the device data sheet for MCLK
    // operation at 24MHz(beyond 8MHz) _before_ configuring the clock system.
    FRCTL0 = FRCTLPW | NWAITS_2 ;

    P2SEL1 |= BIT6 | BIT7;                       // P2.6~P2.7: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);           // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag

    __bis_SR_register(SCG0);                     // disable FLL
    CSCTL3 |= SELREF__XT1CLK;                    // Set XT1 as FLL reference source
    CSCTL0 = 0;                                  // clear DCO and MOD registers
    CSCTL1 = DCORSEL_7;                          // Set DCO = 24MHz
    CSCTL2 = FLLD_0 + 731;                       // DCOCLKDIV = 327358*731 / 1
    __delay_cycles(3);
    __bic_SR_register(SCG0);                     // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));   // FLL locked

    /* CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
     * set XT1 (~32768Hz) as ACLK source, ACLK = 32768Hz
     * default DCOCLKDIV as MCLK and SMCLK source
     - Selects the ACLK source.
     * 00b = XT1CLK with divider (must be no more than 40 kHz)
     * 01b = REFO (internal 32-kHz clock source)
     * 10b = VLO (internal 10-kHz clock source) (1)   */
    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
}


int main(void) {
    WDTCTL = WDTPW | WDTHOLD; /* Desliga Watchdog */
    PM5CTL0 &= ~LOCKLPM5;/* Disable the GPIO power-on default high-impedance mode */

    init_clock_system();
    lcd_init_4bits();

    /* Debug LED */
    P1DIR |= BIT0;
    CLR_BIT(P1OUT,BIT0);

    lcd_send_data(LCD_CLEAR, LCD_CMD);
    _delay_cycles(2000000);

    lcd_write_string("MSP430");

    //lcd_send_data(LCD_LINE_1, LCD_CMD);
    //lcd_write_string("BATATA");
    __bis_SR_register(LPM0_bits + GIE);
    return 0;
}

