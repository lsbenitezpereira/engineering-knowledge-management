/*
 *      Author: Leonardo Benitez
 *      Adaptado de Renan Augusto Starke que adaptou de
 *      "AVR e Arduino: Técnicas de Projeto, 2a ed. - 2012"
 *      que andaptou de alguem
 */

#ifndef _LCD_H
#define _LCD_H

#include <msp430.h>
#include <stdint.h>

#include "../lib/bits.h"

#define E_PIN  BIT2
#define RS_PIN BIT0
#define IDLE 0x00
#define DEVICE_I2C_ADDR 0x27 // endereço de 7-bits I2C (0x27 é o endereço do PCF8574T -- NXP)


typedef enum {LCD_CMD, LCD_DATA} lcd_data_t;

enum DISPLAY_CMDS {
    LCD_TURN_OFF = 0x08,
    LCD_CLEAR = 0x01,
    LCD_LINE_0 = 0x80,
    LCD_LINE_1 = 0xC0
};

/**
  * @brief  Configura hardware.
  * @param	Nenhum
  *
  * @retval Nenhum.
  */
void lcd_init_4bits();

/**
  * @brief  Envia um dado estático para o display: caractere ou comando.
  * @param data: valor do comando.
  * @param data_type: LCD_CMD para comando. LCD_DATA para caractere.
  *
  * @retval Nenhum
  */
void lcd_send_data(uint8_t data, lcd_data_t data_type);

/**
  * @brief  Escreve um string estática (sem printf) no LCD. 
  * @param c: ponteiro para a string em RAM
  *
  * @retval Nenhum
  */
void lcd_write_string(char *c);



#endif
