/*
 *      Author: Leonardo Benitez
 *      Adaptado de Renan Augusto Starke que adaptou de
 *      "AVR e Arduino: Técnicas de Projeto, 2a ed. - 2012"
 *      que andaptou de alguem

 *      NÃO FUNCIONA
 *      O problema são provavelmente os delays: multipleque tudo por 24
 *      o renam passou uma implementação em C++ que concerteza funciona
 */

#include "I2C_lcd.h"
#include "../lib/gpio.h"
#include "../lib/i2c_master_fr2355.h"


/**
 * @brief  Gera sinal pulso de enable por software
 * @param  Nenhum
 *
 * @retval Nenhum.
 */
static inline void enable_pulse(){
    __delay_cycles(1000);
    i2c_write_single_byte(DEVICE_I2C_ADDR, E_PIN);

    __delay_cycles(1000);
    i2c_write_single_byte(DEVICE_I2C_ADDR, IDLE);

    __delay_cycles(1000);
}

/**
 * @brief  Configura hardware: verificar lcd.h para mapa de pinos e nible de dados.
 * @param  Nenhum
 *
 * @retval Nenhum.
 */
void lcd_init_4bits()
{
    init_i2c_master_mode();

    i2c_write_single_byte(DEVICE_I2C_ADDR, RS_PIN | E_PIN);
    __delay_cycles(100000);

    // Interface de 8 bits
    i2c_write_single_byte(DEVICE_I2C_ADDR, 0x30);

    enable_pulse();
    __delay_cycles(10000);
    enable_pulse();
    __delay_cycles(100000);
    enable_pulse();

    i2c_write_single_byte(DEVICE_I2C_ADDR, 0x20);

    enable_pulse();

    // Interface de 4 bits 2 linhas
    // Mudar comando para displays maiores
    lcd_send_data(0x28, LCD_CMD);
    lcd_send_data(LCD_TURN_OFF, LCD_CMD);
    lcd_send_data(LCD_CLEAR, LCD_CMD);

    // Mensagem aparente e cursor inativo não piscante
    // Outros modos podem ser consultados no datasheet
    lcd_send_data(0x0C, LCD_CMD);
    lcd_send_data(LCD_LINE_0, LCD_CMD);


    /* Versao do doido
        #define LCD_4BITMODE 0x00
        #define LCD_5x8DOTS 0x00
        #define LCD_2LINE 0x08
        #define LCD_FUNCTIONSET 0x20
        #define LCD_DISPLAYCONTROL 0x08
        #define LCD_DISPLAYON 0x04
        #define LCD_CLEARDISPLAY 0x01
        #define LCD_ENTRYMODESET 0x04
        #define LCD_ENTRYLEFT 0x02
        lcd_send_data(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE, LCD_CMD);
        lcd_send_data(LCD_DISPLAYCONTROL | LCD_DISPLAYON, LCD_CMD);
        lcd_send_data(LCD_CLEARDISPLAY, LCD_CMD);
        lcd_send_data(LCD_ENTRYMODESET | LCD_ENTRYLEFT, LCD_CMD);
     */
}

/**
 * @brief Envia um dado estático para o display: caractere ou comando.
 * @param data: valor do comando.
 * @param data_type: LCD_CMD para comando. LCD_DATA para caractere.
 *
 * @retval Nenhum
 */
void lcd_send_data(uint8_t data, lcd_data_t data_type)
{
    if (data_type == LCD_CMD){
        i2c_write_single_byte(DEVICE_I2C_ADDR, IDLE);
        i2c_write_single_byte(DEVICE_I2C_ADDR, (0xF0 & data));
        enable_pulse();
        i2c_write_single_byte(DEVICE_I2C_ADDR, (0xF0 & (data << 4)));
        enable_pulse();
    }else {
        i2c_write_single_byte(DEVICE_I2C_ADDR, RS_PIN);
        i2c_write_single_byte(DEVICE_I2C_ADDR, RS_PIN | (0xF0 & data));
        enable_pulse();
        i2c_write_single_byte(DEVICE_I2C_ADDR, RS_PIN | (0xF0 & (data << 4)));
        enable_pulse();
    }

    // Delay adicional em caso de instruções lentas: limpeza, etc
    if ( (data == 0) && ( data_type < 4))
        __delay_cycles(10000);// _delay_ms(2);*/
}

/**
  * @brief Escreve um string estática no LCD.
  * @param c: ponteiro para a string em RAM
  *
  * @retval Nenhum
  */
void lcd_write_string(char *c)
{
   for (; *c!='\0'; c++)
       lcd_send_data(*c, LCD_DATA);
}


