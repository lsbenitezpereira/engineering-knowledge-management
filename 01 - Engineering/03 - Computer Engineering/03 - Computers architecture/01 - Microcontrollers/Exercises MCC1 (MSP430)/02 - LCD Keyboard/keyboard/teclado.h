/*  Keyboard library
 *  Created on: March 20th, 2020
 *      Author: Leonardo Benitez
 *
 *      Adaptado de AVR e Arduino: Técnicas de Projeto, 2a ed. - 2012.
 *      Instituto Federal de Santa Catarina
 */

#ifndef _KEY_H
#define _KEY_H

#include <msp430.h>
#include <stdint.h>


/* Configurações de hardware */
#define KEY_PORT P3

/**
  * @brief  Configura hardware.
  * @param  Nenhum
  *
  * @retval Nenhum.
  */
void key_4x4_init();

/**
  * @brief  Lê qual tecla esta pressionada
  * @param Nenhum
  *
  * @retval retorna o valor pressionado. Se nenhuma tecla for pressionada, retora 255
  */

unsigned char read_key();

#endif
