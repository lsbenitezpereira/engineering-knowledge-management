/*
 * relogio.c
 *
 *  Created on: Apr 17, 2020
 *      Author: Renan Augusto Starke
 *      Instituto Federal de Santa Catarina
 */

/* Includes de sistema */
#include <msp430.h>
#include <stdint.h>

/* Includes do projeto */
#include "lib/gpio.h"
#include "lib/bits.h"
#include "relogio.h"
#include "boardDefinitions/MSP430FR2355.h"

#define BUTTON_SAMPLES 12

#define DEBUG

volatile struct clock_t {
    uint8_t segundos;
    uint8_t minutos;
    uint8_t horas;

    uint8_t em_ajuste;
} meu_relogio;


/**
  * @brief  used to debounce
  * @param  none
  * @retval none
  */
void config_timerB_0(){
    TB0CCTL0 |= CCIE;                             // TBCCR0 interrupt enabled
    TB0CCR0 = 24000;                              // CCR on that value. T=1ms (at f=24MHz)
    TB0CTL = TBSSEL__SMCLK | MC__UP;              // SMCLK, UP mode
}


void init_relogio(){
    meu_relogio.segundos = 0;
    meu_relogio.minutos = 0;
    meu_relogio.horas = 0;

    PORT_DIR(RELOGIO_BUTTONS_PORT) = ~(BUTTON_ADJ | BUTTON_HORA | BUTTON_MIN);  // 0011 1000  -> ~ -> 1100 0111
    PORT_REN(RELOGIO_BUTTONS_PORT) = BUTTON_ADJ | BUTTON_HORA | BUTTON_MIN;
    PORT_OUT(RELOGIO_BUTTONS_PORT) = BUTTON_ADJ | BUTTON_HORA | BUTTON_MIN; //pull up
    SET_BIT(PORT_DIR(LED1_PORT), LED1_BIT);

    config_timerB_0();
}

uint8_t obter_segundos(){
    return meu_relogio.segundos;
}

uint8_t obter_minutos(){
    return meu_relogio.minutos;
}

uint8_t obter_horas(){
    return meu_relogio.horas;
}

uint8_t em_ajuste(){
    return meu_relogio.em_ajuste;
}

// TODO: gerar base de tempo de 1s e incrementar o tempo dentro dessa interrupção


/* ISR do watchdog: executado toda a vez que o temporizador estoura (1 segundo)
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(WDT_VECTOR))) watchdog_timer (void)
#else
#error Compiler not supported!
#endif
{
   meu_relogio.segundos++;

   if (meu_relogio.segundos > 60){
       meu_relogio.segundos = 0;
       meu_relogio.minutos++;
   }

   if (meu_relogio.minutos > 60){
     meu_relogio.minutos = 0;
     meu_relogio.horas++;
   }

   if (meu_relogio.horas > 23)
       meu_relogio.horas = 0;


   /* Desativa suspensão do main
   __bic_SR_register_on_exit(LPM0_bits);
}*/



/* Timer0_B0 interrupt service routine
 * Período: 1ms
 * Utilizado para o debouncer por amostragem: faz a verificação de botão periodicamente.
 * */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_B0_VECTOR
__interrupt void Timer0_B0_ISR (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_B0_VECTOR))) Timer0_B0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    static uint8_t counter_adj = BUTTON_SAMPLES;
    static uint8_t counter_min = BUTTON_SAMPLES;
    static uint8_t counter_hora = BUTTON_SAMPLES;
    static uint16_t i = 0;

    /* Debug: Piscar a cada +- 1s */
    if (++i>1000 && !meu_relogio.em_ajuste){
        meu_relogio.segundos++;

        if (meu_relogio.segundos > 60){
            meu_relogio.segundos = 0;
            meu_relogio.minutos++;
        }

        if (meu_relogio.minutos > 60){
          meu_relogio.minutos = 0;
          meu_relogio.horas++;
        }

        if (meu_relogio.horas > 23)
            meu_relogio.horas = 0;


        /* Desativa suspensão do main */
        __bic_SR_register_on_exit(LPM0_bits);

        CPL_BIT(PORT_OUT(LED1_PORT), LED1_BIT);
        i=0;
    }




    /* Se botão apertado: borda de descida */
    if (!TST_BIT(PORT_IN(RELOGIO_BUTTONS_PORT), BUTTON_ADJ))  {
        /* Se contagem = 0, debounce terminado */
        if ((--counter_adj)<=0) {
            meu_relogio.em_ajuste ^= BIT0; /* Entra/sai do modo ajuste */
            __bic_SR_register_on_exit(LPM0_bits);/* Acorda função main */
        }
    }
    else counter_adj = BUTTON_SAMPLES;

    /* Se botão apertado: borda de descida */
    if (!TST_BIT(PORT_IN(RELOGIO_BUTTONS_PORT), BUTTON_MIN))  {
        /* Se contagem = 0, debounce terminado */
        if ((--counter_min)<=0 && meu_relogio.em_ajuste) {
            meu_relogio.minutos++;
            if (meu_relogio.minutos > 60) meu_relogio.minutos = 0;
            __bic_SR_register_on_exit(LPM0_bits);/* Acorda função main */
        }
    }
    else counter_min = BUTTON_SAMPLES;

    /* Se botão apertado: borda de descida */
    if (!TST_BIT(PORT_IN(RELOGIO_BUTTONS_PORT), BUTTON_HORA))  {
        /* Se contagem = 0, debounce terminado */
        if ((--counter_hora)<=0 && meu_relogio.em_ajuste) {
            meu_relogio.horas++;
            if (meu_relogio.horas > 24) meu_relogio.horas = 0;
            __bic_SR_register_on_exit(LPM0_bits);/* Acorda função main */
        }
    }
    else counter_hora = BUTTON_SAMPLES;
}


