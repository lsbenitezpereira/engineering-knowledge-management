#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xSemaphoreHandle xMutexUART;
xSemaphoreHandle xCountingSemaphore;

//--------------------------------------------------------------------------------
static void vPeriodicTask(void *pvParameters)
{
	while(1)
	{
		vTaskDelay(500/portTICK_RATE_MS);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - About to generate an interrupt.\n");
		xSemaphoreGive(xMutexUART);
		
		NVIC_SetPendingIRQ((IRQn_Type) EXTI0_IRQn);	// ativa interrup��o por software
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - Interrupt generated.\n\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
static void vHandlerTask(void *pvParameters)
{
	
	while(1)
	{
		xSemaphoreTake(xCountingSemaphore, portMAX_DELAY);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Handler task - Processing event.\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
// Interrup��o ativa por software - Emprego do TC0 canal 0
//--------------------------------------------------------------------------------
void EXTI0_IRQHandler()
{
	static portBASE_TYPE xHigherPriorityTaskWoken;
	
	xHigherPriorityTaskWoken = pdFALSE;
	
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
			
	if(xHigherPriorityTaskWoken == pdTRUE)
	{
		//printf("xHigherPriorityTaskWoken == pdTRUE.\n");
	}

	//use to force a context switch from an ISR
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	/* Giving the semaphore unblocked a task, and the priority of the
	unblocked task is higher than the currently running task - force
	a context switch to ensure that the interrupt returns directly to
	the unblocked (higher priority) task.*/
}
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//---------------------------------------------------------------------------------------------
	// Habilitar interrup��o por SW
	//---------------------------------------------------------------------------------------------
	NVIC_EnableIRQ((IRQn_Type) EXTI0_IRQn);		// usa EXTI0 para gerar interrup��o por software

	/* configura Preempt Priority and Subpriority interrupt para o valor default (no STM32 � diferente!)*/
	NVIC_SetPriorityGrouping(0);

	/*IMPORTANTISSIMO
    	The priority of the interrupt has to be set to below configMAX_SYSCALL_INTERRUPT_PRIORITY
    	(FreeRTOSConfig.h), caso contrario a ISR que chama API do FreeRTOS vai travar!
    	Cuidado, pois h� uma invers�o, no ARM quando menor o nr, maior a prioridade!*/

	NVIC_SetPriority(EXTI0_IRQn, 6);	// ajusta prioridade para nivel 6, com 	configMAX_SYSCALL_INTERRUPT_PRIORITY = 5
	//---------------------------------------------------------------------------------------------

	//RTOS
	xMutexUART = xSemaphoreCreateMutex();
	xCountingSemaphore = xSemaphoreCreateCounting(10,0);
	
	if(xCountingSemaphore != NULL)
	{
		xTaskCreate(vHandlerTask,"Handler",100, NULL, 3, NULL);
		xTaskCreate(vPeriodicTask, "Periodic", 100, NULL, 1, NULL);
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
