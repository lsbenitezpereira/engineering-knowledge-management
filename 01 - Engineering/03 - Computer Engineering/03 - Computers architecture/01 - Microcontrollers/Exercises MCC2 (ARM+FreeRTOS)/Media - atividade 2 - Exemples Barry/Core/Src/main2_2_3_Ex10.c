#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xQueueHandle xQueue;
xSemaphoreHandle xMutexUART;	// a UART � compartilhada e deve ser alocada por cada tarefa

//--------------------------------------------------------------------------------
static void vSenderTask(void *pvParameters)
{
	uint32_t uiValueToSend;
	portBASE_TYPE xStatus;
	
	uiValueToSend = (uint32_t) pvParameters;
	
	while(1)
	{
		xStatus = xQueueSendToBack(xQueue, &uiValueToSend, 0);
		
		if(xStatus != pdPASS)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);				// pede o recurso
			printf("Could not send to the queue.\n");
			xSemaphoreGive(xMutexUART);								// libera o recurso
		}
		taskYIELD();
	}
}
//--------------------------------------------------------------------------------
static void vReceiverTask(void *pvParameters)
{
	uint32_t uiReceivedValue;
	portBASE_TYPE xStatus;
	const portTickType xTicksToWait = 250/portTICK_RATE_MS;
	
	while(1)
	{
		if(uxQueueMessagesWaiting(xQueue)!=0)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Queue should have been empty.\n");
			xSemaphoreGive(xMutexUART);
		}
			
		xStatus = xQueueReceive(xQueue, &uiReceivedValue, xTicksToWait);
		
		if(xStatus == pdPASS)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Received = %u\n", uiReceivedValue);
			xSemaphoreGive(xMutexUART);
		}
		else
		{	
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Could not Receive from the queue.\n");
			xSemaphoreGive(xMutexUART);
		}
		
	}
}

//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xQueue = xQueueCreate(5, sizeof(uint32_t));
	xMutexUART = xSemaphoreCreateMutex();	// acesso restrito ao hardware para o uso da UART
	
	if(xQueue != NULL)
	{	
		xTaskCreate(vSenderTask,"Sender 1",100, (void *)100, 1, NULL);
		xTaskCreate(vSenderTask,"Sender 2",100, (void *)200, 1, NULL);
		xTaskCreate(vReceiverTask, "Receiver", 100, NULL, 2, NULL);
		
		vTaskStartScheduler();	
	}
	else
	{
		// A fila nao pode ser criada
	}
	
	while (1);
}
//--------------------------------------------------------------------------------
