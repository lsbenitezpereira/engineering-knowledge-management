#include "init_from_hal.h"
//--------------------------------------------------------------------------------

unsigned int lfsr;			// vari�vel global para a gera��o de n�meros pseudo-aleatorios

xSemaphoreHandle xMutex;

void init_LFSR(unsigned int valor);
unsigned int prng_LFSR() ;

//--------------------------------------------------------------------------------
static void prvNewPrintString( const portCHAR * pcString)
{
	xSemaphoreTake(xMutex, portMAX_DELAY);
	{
		printf(pcString);
		printf("%u\n",prng_LFSR()& 0x000003E8);
	}
	xSemaphoreGive(xMutex);
}
//--------------------------------------------------------------------------------
static void prvPrintTask(void *pvParameters)
{
	char *pcStringToPrint;
	
	pcStringToPrint = (char *)pvParameters;
	
	while(1)
	{	
		prvNewPrintString(pcStringToPrint);
		
		vTaskDelay(prng_LFSR() & 0x000003E8); // valor entre 0 1000 ms;
	}
}
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	
	/* semente para o PRNG (deve ser gerada automaticamente em cada reinicializa��o do uC)
	 * pode ser empregado o ADC com algum entrada flutuante para gerar a aleat�riedade desse nr.
	 */
	init_LFSR(13);		// valor de semente fixo para efeito de teste.

	
	//RTOS
	
	xMutex = xSemaphoreCreateMutex();
	
	if(xMutex != NULL)
	{
		xTaskCreate(prvPrintTask, "Print1", 100, "Task1 ----------------- = ", 1, NULL);
		xTaskCreate(prvPrintTask, "Print2", 100, "Task2 ***************** = ", 2, NULL);
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
/*
 * Fun��o que passa o valor "semente" para a variavel utilizada na fun��o prng_LFSR()
 */
void init_LFSR(unsigned int valor)
{
	lfsr = valor;
}
//--------------------------------------------------------------------------------
/*
 * Fun��o PRNG - toda vez que for chamada, um novo n�mero � gerado e salvo em lfsr.
 * 	O primeiro valor de lfsr deve ser diferente de zero e deve ser gerado como semente
 */
unsigned int prng_LFSR() 	// Galois LFSRs, Liner-Feedback Shift Register, (PRNG)
{
	unsigned int lsb;

	if (lfsr==0)		// garantia para que valor nao seja zero
	    lfsr = 1;

	lsb = lfsr & 0x00000001;
	lfsr = lfsr >> 1;

	if (lsb)
	    lfsr = lfsr ^ 0x80000057;	//polinomio retirado de http://users.ece.cmu.edu/~koopman/lfsr/

	return lfsr;
}
//--------------------------------------------------------------------------------

