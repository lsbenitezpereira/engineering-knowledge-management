#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xQueueHandle xQueue;
xSemaphoreHandle xMutex;

typedef struct  
{
	unsigned char ucValue;
	unsigned char ucSource;
}xData;

xData xStructsToSend[2]=
{
	{100, 1},
	{200, 2}
};
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static void vSenderTask(void *pvParameters)
{
	portBASE_TYPE xStatus;
	const portTickType xTicksToWait = 250/portTICK_RATE_MS;
	
	while(1)
	{
		xStatus = xQueueSendToBack(xQueue, pvParameters, xTicksToWait);
		
		if(xStatus != pdPASS)
		{
			xSemaphoreTake(xMutex,portMAX_DELAY);				// pede o recurso
			printf("Could not send to the queue.\n");
			xSemaphoreGive(xMutex);								// libera o recurso
		}
		taskYIELD();
	}
}
//--------------------------------------------------------------------------------
static void vReceiverTask(void *pvParameters)
{
	xData xReceivedStructure;
	portBASE_TYPE xStatus;
	
	while(1)
	{
		if(uxQueueMessagesWaiting(xQueue)!=3)
		{
			xSemaphoreTake(xMutex,portMAX_DELAY);
			printf("Queue should have been full!\n");
			xSemaphoreGive(xMutex);	
		}
			
		xStatus = xQueueReceive(xQueue, &xReceivedStructure, 0);
		
		if(xStatus == pdPASS)
		{
			if(xReceivedStructure.ucSource == 1)
			{
				xSemaphoreTake(xMutex,portMAX_DELAY);
				printf("From Sender 1 = %u\n", xReceivedStructure.ucValue);
				xSemaphoreGive(xMutex);
			}
			else
			{
				xSemaphoreTake(xMutex,portMAX_DELAY);
				printf("From Sender 2 = %u\n", xReceivedStructure.ucValue);
				xSemaphoreGive(xMutex);				
			}
		}
		else
		{	
			xSemaphoreTake(xMutex,portMAX_DELAY);
			printf("Could not Receive from the queue.\n");
			xSemaphoreGive(xMutex);	
		}
		
	}
}
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xQueue = xQueueCreate(3, sizeof(xData));
	xMutex = xSemaphoreCreateMutex();	// acesso restrito ao hardware para o uso da UART
	
	if(xQueue != NULL)
	{	
		xTaskCreate(vSenderTask,"Sender 1",100, &(xStructsToSend[0]), 2, NULL);
		xTaskCreate(vSenderTask,"Sender 2",100, &(xStructsToSend[1]), 2, NULL);
		xTaskCreate(vReceiverTask, "Receiver", 100, NULL, 1, NULL);
		
		vTaskStartScheduler();	
	}
	else
	{
		
	}
	
	while (1);
}
//--------------------------------------------------------------------------------
