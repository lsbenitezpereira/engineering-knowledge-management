#define S1_G 0 //PB_10
#define S1_Y 1 //PB_11
#define S1_R 2 //PB_1

#define S2_G 3 //PB_7
#define S2_Y 4 //PB_6
#define S2_R 5 //PB_5

#define S3_G 6 //PB_3
#define S3_Y 7 //PB_2
#define S3_R 8 //PB_1

#define S4_G 9 //PC_15
#define S4_Y 10 //PC_14
#define S4_R 11 //PC_13

#define OPEN_TIME 1000;
#define CLOSING_TIME 500;

typedef enum {
    RESET = 0,
    S1_OPEN,
    S1_CLOSING,
    S2_OPEN,
    S2_CLOSING,
    S3_OPEN,
    S3_CLOSING,
    S4_OPEN,
    S4_CLOSING,
} State_Type;

typedef enum {
    COLOR_OFF=0,
    COLOR_GREEN,
    COLOR_YELLOW,
    COLOR_RED
} Semaphore_Color;

void init_sm();
void reset();
void s1_open();
void s1_closing();
void s2_open();
void s2_closing();
void s3_open();
void s3_closing();
void s4_open();
void s4_closing();
void decrement_timer();
void HAL_Delay(int t);
void HAL_GPIO_WritePin(uint16_t GPIO_Pin, int PinState);
void set_lights(Semaphore_Color S1, Semaphore_Color S2, Semaphore_Color S3, Semaphore_Color S4);