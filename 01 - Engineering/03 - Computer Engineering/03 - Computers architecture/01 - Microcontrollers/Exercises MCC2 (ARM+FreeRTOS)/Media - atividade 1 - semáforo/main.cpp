/*
Author: Leonardo Benitez
Date: 2020/12

To compile: g++ main.cpp -o main
To execute: ./main

References and useful links:
  https://www.guiadaengenharia.com/programacao-semaforo/
  https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjfk93X-93tAhXzK7kGHQ6DB2IQFjATegQIBBAC&url=https%3A%2F%2Fwww.stt.eesc.usp.br%2Fsetti%2Fstt602%2Fslides%2Fintersecoes%2520viarias%2520-%2520semaforos%2520%5B2013%5D.pdf&usg=AOvVaw1xaGfIKKo7dnsYOWoBPRCJ
  https://www.youtube.com/watch?v=NlDSwMSmLbA
*/

#include <iostream>
#include "main.h"
using namespace std;

/* This table contains a pointer to the function to call in each state.*/
void (*state_table[])() = {
    reset,
    s1_open,
    s1_closing,
    s2_open,
    s2_closing,
    s3_open,
    s3_closing,
    s4_open,
    s4_closing,
};

State_Type current_state;
int timer=0;
int count_debug = 0;

int main() {
    init_sm();
    cout << "Init completed\n";
    
    while (1) {
        /* current state is called once per iteration. */
        state_table[current_state]();
        decrement_timer();

        /* Do other functions, not related to this state machine.*/

        //debug          
        if (count_debug>=2){
            break;
        }
    }
    return 0;
};

void init_sm(){
  //TODO: init leds
  current_state = RESET;
}

void reset(){
    //do anything else before put things togueder
    HAL_Delay(1000);
    set_lights(COLOR_OFF, COLOR_OFF, COLOR_OFF, COLOR_OFF);
    HAL_Delay(1000);
    set_lights(COLOR_YELLOW, COLOR_YELLOW, COLOR_YELLOW, COLOR_YELLOW);
    HAL_Delay(1000);
    set_lights(COLOR_OFF, COLOR_OFF, COLOR_OFF, COLOR_OFF);
    HAL_Delay(1000);
    set_lights(COLOR_YELLOW, COLOR_YELLOW, COLOR_YELLOW, COLOR_YELLOW);
    
    //after put things togueder, start normal operation
    cout << "Restart sequence completed\n";
    set_lights(COLOR_GREEN, COLOR_RED, COLOR_RED, COLOR_RED);
    timer=OPEN_TIME;
    current_state = S1_OPEN;
}

/** State function
 *  @param: none
 */
void s1_open(){
    if (timer<=0){
        set_lights(COLOR_YELLOW, COLOR_RED, COLOR_RED, COLOR_RED);
        timer=CLOSING_TIME;
        current_state = S1_CLOSING;
    }
}

/** State function
 *  @param: none
 */
void s1_closing(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_GREEN, COLOR_RED, COLOR_RED);
        timer=OPEN_TIME;
        current_state = S2_OPEN;
    }
}

/** State function
 *  @param: none
 */
void s2_open(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_YELLOW, COLOR_RED, COLOR_RED);
        timer=CLOSING_TIME;
        current_state = S2_CLOSING;
    }
}

/** State function
 *  @param: none
 */
void s2_closing(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_RED, COLOR_GREEN, COLOR_RED);
        timer=OPEN_TIME;
        current_state = S3_OPEN;
    }
}

/** State function
 *  @param: none
 */
void s3_open(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_RED, COLOR_YELLOW, COLOR_RED);
        timer=CLOSING_TIME;
        current_state = S3_CLOSING;
    }
}

/** State function
 *  @param: none
 */
void s3_closing(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_RED, COLOR_RED, COLOR_GREEN);
        timer=OPEN_TIME;
        current_state = S4_OPEN;
    }
}

/** State function
 *  @param: none
 */
void s4_open(){
    if (timer<=0){
        set_lights(COLOR_RED, COLOR_RED, COLOR_RED, COLOR_YELLOW);
        timer=CLOSING_TIME;
        current_state = S4_CLOSING;
    }
}

/** State function
 *  @param: none
 */
void s4_closing(){
    if (timer<=0){
        set_lights(COLOR_GREEN, COLOR_RED, COLOR_RED, COLOR_RED);
        timer=OPEN_TIME;
        current_state = S1_OPEN;
        count_debug++; //debug
    }
}

/** Set the outputs given the state representation
 *  @param Si: set the led i to given state
 */
void set_lights(Semaphore_Color S1, Semaphore_Color S2, Semaphore_Color S3, Semaphore_Color S4){
    cout << S1 << S2 << S3 << S4 << '\n';
    
    if (S1==COLOR_OFF){
        HAL_GPIO_WritePin(S1_G, 0);
        HAL_GPIO_WritePin(S1_Y, 0);
        HAL_GPIO_WritePin(S1_R, 0);
    } else if (S1==COLOR_GREEN){
        HAL_GPIO_WritePin(S1_G, 1);
        HAL_GPIO_WritePin(S1_Y, 0);
        HAL_GPIO_WritePin(S1_R, 0);
    } else if (S1==COLOR_YELLOW){
        HAL_GPIO_WritePin(S1_G, 0);
        HAL_GPIO_WritePin(S1_Y, 1);
        HAL_GPIO_WritePin(S1_R, 0);
    } else if (S1==COLOR_RED){
        HAL_GPIO_WritePin(S1_G, 0);
        HAL_GPIO_WritePin(S1_Y, 0);
        HAL_GPIO_WritePin(S1_R, 1);
    }

    if (S2==COLOR_OFF){
        HAL_GPIO_WritePin(S2_G, 0);
        HAL_GPIO_WritePin(S2_Y, 0);
        HAL_GPIO_WritePin(S2_R, 0);
    } else if (S2==COLOR_GREEN){
        HAL_GPIO_WritePin(S2_G, 1);
        HAL_GPIO_WritePin(S2_Y, 0);
        HAL_GPIO_WritePin(S2_R, 0);
    } else if (S2==COLOR_YELLOW){
        HAL_GPIO_WritePin(S2_G, 0);
        HAL_GPIO_WritePin(S2_Y, 1);
        HAL_GPIO_WritePin(S2_R, 0);
    } else if (S2==COLOR_RED){
        HAL_GPIO_WritePin(S2_G, 0);
        HAL_GPIO_WritePin(S2_Y, 0);
        HAL_GPIO_WritePin(S2_R, 1);
    }

    if (S3==COLOR_OFF){
        HAL_GPIO_WritePin(S3_G, 0);
        HAL_GPIO_WritePin(S3_Y, 0);
        HAL_GPIO_WritePin(S3_R, 0);
    } else if (S3==COLOR_GREEN){
        HAL_GPIO_WritePin(S3_G, 1);
        HAL_GPIO_WritePin(S3_Y, 0);
        HAL_GPIO_WritePin(S3_R, 0);
    } else if (S3==COLOR_YELLOW){
        HAL_GPIO_WritePin(S3_G, 0);
        HAL_GPIO_WritePin(S3_Y, 1);
        HAL_GPIO_WritePin(S3_R, 0);
    } else if (S3==COLOR_RED){
        HAL_GPIO_WritePin(S3_G, 0);
        HAL_GPIO_WritePin(S3_Y, 0);
        HAL_GPIO_WritePin(S3_R, 1);
    }

    if (S4==COLOR_OFF){
        HAL_GPIO_WritePin(S4_G, 0);
        HAL_GPIO_WritePin(S4_Y, 0);
        HAL_GPIO_WritePin(S4_R, 0);
    } else if (S4==COLOR_GREEN){
        HAL_GPIO_WritePin(S4_G, 1);
        HAL_GPIO_WritePin(S4_Y, 0);
        HAL_GPIO_WritePin(S4_R, 0);
    } else if (S4==COLOR_YELLOW){
        HAL_GPIO_WritePin(S4_G, 0);
        HAL_GPIO_WritePin(S4_Y, 1);
        HAL_GPIO_WritePin(S4_R, 0);
    } else if (S4==COLOR_RED){
        HAL_GPIO_WritePin(S4_G, 0);
        HAL_GPIO_WritePin(S4_Y, 0);
        HAL_GPIO_WritePin(S4_R, 1);
    }
}

void decrement_timer(){
    timer--;
    HAL_Delay(1);
}


/* Debug functions, for compatibility with STM32 HAL */
void HAL_Delay(int t){
}

void HAL_GPIO_WritePin(uint16_t GPIO_Pin, int PinState){
}		

