[TOC]

# Famílias lógicas

-   Tocci chapter 8

-   Classificação dos circuitos integrados quanto ao tipo de
    transístores(Bipolar e Mos-Fet)

-   **Famílias lógicas bipolares:**
    -   RTL -- Resistor Transistor Logic -- Lógica de transístor e
        resistência.
    
    -   DTL -- Díode Transistor Logic -- Lógica de transístor e díodo.
    
    -   TTL -- Transistor Transistor Logic -- Lógica
        transístor-transístor.
    
    -   HTL -- High Threshold Logic -- Lógica de transístor com alto
        limiar.
    
    -   ECL -- Emitter Coupled Logic -- Lógica de emissores ligados.
    
    -   I2 L -- Integrated-Injection Logic -- Lógica de injecção
        integrada.
    
-   **Famílias lógicas MOS:**
    -   CMOS -- Complemantary MOS -- MOS de pares complementares
        NMOS/PMOS
    
    -   NMOS -- Utiliza só transístores MOS-FET canal N.
    
    -   PMOS - Utiliza só transístores MOS-FET canal P.
    
-   

# Interface entre famílias

## From 3.3V to 5V

-   Tutorial:
    [[https://hackaday.com/2016/12/05/taking-it-to-another-level-making-3-3v-and-5v-logic-communicate-with-level-shifters/]{.ul}](https://hackaday.com/2016/12/05/taking-it-to-another-level-making-3-3v-and-5v-logic-communicate-with-level-shifters/)

-   TI integrates:
    [[http://www.ti.com/logic-circuit/voltage-level-translation/overview.html]{.ul}](http://www.ti.com/logic-circuit/voltage-level-translation/overview.html)

-   TTL input

    -   a 3.3V TTL output can drive a 5V TTL input without any extra
        hardware required

![](./Images/Famílias lógicas e interfaces digitais/media/image1.png){width="2.8208333333333333in"
height="1.8194444444444444in"}

-   some CMOS are 3.3V compatible, so check the datasheet

-   **Two-diodes arrange**

    -   unidirectional

    -   add 0.7 volts to the high level, so it may be enogh to drive an
        5V cmos

    -   the resistor should be significantly less than the input
        impedance of the 5V gate

    -   D1 serves just to protection

    -   Recomendations: R=10k, 1n4148

        ![](./Images/Famílias lógicas e interfaces digitais/media/image2.png){width="3.1055555555555556in"
        height="1.4208333333333334in"}

-   **Simple mosfet**

    -   ivnerts the signal

    -   unidirectional

    -   Recomendation: R=10k, 2n7000 or BSS138

        ![](./Images/Famílias lógicas e interfaces digitais/media/image3.png){width="3.136111111111111in"
        height="1.9680555555555554in"}

- **mosfet wtf**

  -   biderectional

  -   how this works????

      ![](./Images/Famílias lógicas e interfaces digitais/media/image4.png){width="2.576388888888889in"
      height="2.029166666666667in"}

## From 5V to 3.3V

-   Some families are 3.3V but 5V tolerant, like the 74LVC series

-   Resistive

    -   does not preserve the fan-out characteristic

    -   parasite capacitances can screw the rise time

        ![](./Images/Famílias lógicas e interfaces digitais/media/image5.png){width="1.7659722222222223in"
        height="2.1020833333333333in"}

-   
