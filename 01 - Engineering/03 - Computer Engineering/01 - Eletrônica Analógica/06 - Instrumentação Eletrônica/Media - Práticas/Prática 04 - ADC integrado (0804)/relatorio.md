# Relatório 4

## Desenho do circuito

Para atender os requisitos propostos, projetou-se o seguinte circuito:

![img](00 circuit.png)

## Clock de conversão

Com o auxílio do osciloscópio, verificou-se que o tempo de conversão era de $107\mu S$, um pouco acima do valor desejado de $100 \mu S$. Dessa forma, é possível obter uma frequência de conversão de $f=\frac{1}{T} = 9345 KHz$.

![](relatorio4img1.png)

## início de escala e final de escala

![](02 ZS.png)

![](03 FS.png)

## Tabela de valores

| Entrada analógica | Saída digital |
| ----------------- | ------------- |
| 1.00 V            | 0000 0000 |
| 1.14 V            | 0000 1010 |
| 1.28 V            | 0001 0100 |
| 1.42 V            | 0001 1111 |
| 1.56 V            | 0010 1001 |
| 1.70 V            | 0011 0011 |
| 1.84 V            | 0011 1101 |
| 1.98 V            | 0100 0111 |
| 2.12 V            | 0101 0010 |
| 2.26 V            | 0101 1100 |
| 2.40 V            | 0110 0110 |
| 2.54 V            | 0111 0000 |
| 2.68 V            | 0111 1010 |
| 2.82 V            | 1000 0101 |
| 2.96 V            | 1000 1111 |
| 3.10 V            | 1001 1001 |
| 3.24 V            | 1010 0011 |
| 3.38 V            | 1010 1101 |
| 3.52 V            | 1011 1000 |
| 3.66 V            | 1100 0010 |
| 3.80 V            | 1100 1100 |
| 3.94 V            | 1101 0110 |
| 4.08 V            | 1110 0000 |
| 4.22 V            | 1110 1011 |
| 4.36 V            | 1111 0101 |
| 4.50 V              | 1111 1111 |

