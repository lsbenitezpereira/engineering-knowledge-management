## Resultados de simulação

* **Estágio 1**

  * Topologia de polarização com 4 resistores, usando JFET

  * $I_g \simeq 0$

  * $g_m = \frac{\Delta i_d}{\Delta v_{gs}}$

  * Ganho ac = 1.9 V/V

  * dissipação do JFET: 152 mW

  * Eficiência máxima teórica: 25%

  * Saída do primeiro estágio (com entrada de 100mV pico):

    ![1553355772508](Images - Relatório/1553355772508.png)

    ![1553360418931](Images - Relatório/1553360418931.png)

* **Estágio 2**
  * Topologia push-pull, com ponto de operação do BJT fixado por 2 diodos

  * Classe AB (condução revezada de transistores, mas com compensação de crossover distortion)

  * Ganho ac = 0.6 V/V

  * Dissipação de cada BJT: 52 mW (max: 40W)

  * gm teórico = 3.3 mA/V

  * gm simulado = ??? mA/V

  * Eficiência máxima teórica: 78.5%

  * Saída do segundo estágio (com entrada de 100mV pico):

  * ![1553360292271](Images - Relatório/1553360292271.png)

    ![1553360377708](Images - Relatório/1553360377708.png)

## Montagem em matriz de contatos

![1553359591788](Images - Relatório/1553359591788.png)

* **Estágio 1**

  * 

  * Vds = 15.9V

  * Id = 9.5mA

  * Gvs = -2V

  * Ganho DC:  7.15 V/V

  * gm (variando em dc)= 3.9 mA/V

  * Ganho ac (botnado uma senoide) = 2.4 V/V

  * Entrada (amarela) vs saída (azul):

    ![1553359530018](Images - Relatório/1553359530018.png)

    

* **Estágio 2**

  *  Ganho ac = 0.91 V/V
  *  $V_{b2}$ = 15.7 V
  *  $V_{b3}$ = 14.5 V
  *  $V_{CE1}$ = 2.7 V
  *  $I_{C1}$ = 1.2 mA
  *  Causa uma defasagem?
  *  Case do TIP31 e 32 é conectado ao coletor (precisar ser isolados, caso conectados no mesmo dissipador)
  *  Entrada do circuito (amarela) vs saída (saída)
  *  ![1553359428965](Images - Relatório/1553359428965.png)























## Dúvidas

Quais dados coletar da prática? so isso

como estabilizar? dissipador

Devo já tratar de aumentar o ganho? ta ok