### 1) Conceitue o comparador Schimitt Triger

É um circuito biestável na qual a saída se mantêm em $V_{out+}$ após a entrada ficar acima de um threshold $V_{T+}$ e se mantêm em $V_{out-}$ após a entrada ficar abaixo de um threshold $V_{T-}$

Para valores de entrada  $V_{T-}<V_{in}<V_{T+}$, a saída mantêm o valor anterior

![1560769321947](Images - Relatorio/1560769321947.png)

### 2) Discorra sobre o circuito comparador sem histerese

$V_{ref}=6V$

$V_{in}$ é determinado pela resistência do LDR, que varia com a luminosidade. O potênciometro permite ajustar a sensibilidade do LDR.

Caso $V_{in}<V_{ref}$, o led estará acesso

Caso $V_{in}>V_{ref}$, o led estará apagado

Caso $V_{in}\approx V_{ref}$, o led irá repicar

![img](Images - Relatorio/sem histerese.JPG)

### 2) Discorra sobre o circuito comparador com histerese

Similar ao anterior, porém a saída apresenta persistência (histerese)

Caso $V_{in}<-1,1V$, o led estará acesso

Caso $V_{in}>0,6V$, o led estará apagado

Caso $-1,1<V_{in}<0,6$, o led mantêm o estado anterior

![img](Images - Relatorio/com histerese.JPG)