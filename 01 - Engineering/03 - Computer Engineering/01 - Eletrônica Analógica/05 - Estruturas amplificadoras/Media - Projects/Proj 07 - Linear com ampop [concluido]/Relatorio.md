### 1) Apresente a resposta em frequência do amplificador

$w_1=63 Hz$

$w_2=68 KHz$

### 2) Discorra sobre os componentes que constituem a rede de realimentação, assim como o tipo de realimentação implementada

A realimentação é composta por dois resistores e um capacitor, sendo que o capacitor possui a função de diminuir o ganho das componentes de baixa frequência e os resistores possem a função de “amostrar” a saída, determinando o ganho da estrutura 

Realimentação de tensão parelelo-paralelo, ou seja:

![1560771198281](Images - Relatorio/1560771198281.png)



### 3) Explique por que o autor implementou um divisor de tensao na porta não inversora do ampop

Para fornecer um offset dc à entrada e, dessa forma, permitir a amplificação dos dois semiciclos utilizando apenas uma alimentação simples ($+15V$ e $0V$)

Esse offset é transmitido para a saída (com ganho 1) e é removido pelo capacitor $C_2$