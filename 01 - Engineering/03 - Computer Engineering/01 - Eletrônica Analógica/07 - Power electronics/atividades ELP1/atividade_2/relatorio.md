# Problema 7.5

## Enunciado

(a) What is the value of load resistance that separates continuous and discontinuous magnetizing inductance current in the flyback converter of Example 7-1?

(b) Graph Vo/Vs as the load changes from 5 to 20.

## Resolução teórica

Primeiramente, definiu-se os parâmetros do conversor:

![image-20201219124651575](Images - relatorio/image-20201219124651575.png)

O problema consiste em encontrar $R$ tal que $L_m = L_{m\text{ mínimo}}$

Reorganizando os termos da equação 7-12 do livro texto, podemos resolver com a função do Smath `findRoot`:

![image-20201219125013613](Images - relatorio/image-20201219125013613.png)

Os esforços nos componentes já haviam sido calculados no Exemplo 7.1 e não serão repetidos neste relatório.

Para traçar o gráfico utilizou-se a ferramenta online [Desmos](https://www.desmos.com):

![image-20201219124110718](Images - relatorio/image-20201219124110718.png)

Uma versão interativa do gráfico está disponível em https://www.desmos.com/calculator/9zw6zexsqn.

Perceba que as duas curvas se intersectam exatamente no ponto em que R está no limite da condução contínua: antes desse ponto  $\frac{V_0}{V_s}$ segue a curva vermelha, após esse ponto segue a curva preta.

## Simulação

Utilizou-se o simulador PSIM para verificar que quando $R_{out}=11.736\Omega$, o conversor opera no limite da condução contínua:

![image-20201219170707820](Images - relatorio/image-20201219170707820.png)

Nessa condições, a tensão de saída ainda está com o valor desejado de 5V:

![image-20201219171028998](Images - relatorio/image-20201219171028998.png)

# Problema 7.11

## Enunciado

A forward converter has parameters Vs  125 V, Vo  50 V, and R  25 , and the switching frequency is 250 kHz. Determine (a) the transformer turns ratio N1/N2 such that the duty ratio is 0.3, (b) the inductance Lx such that the minimum current in Lx is 40 percent of the average current, and (c) the capacitance required to limit the output ripple voltage to 0.5 percent.

## Resolução teórica

Primeiramente, definiu-se os parâmetros do conversor:

![image-20201219164043102](Images - relatorio/image-20201219164043102.png)

Respondeu-se aos items pedidos:

![image-20201219164159239](Images - relatorio/image-20201219164159239.png)

Calculou-se, adicionalmente, os esforços em alguns dos principais componentes:

![image-20201219164231764](Images - relatorio/image-20201219164231764.png)

## Simulação

Utilizou-se a ferramenta Power Stage Designer Tool 4.0, inserindo os parâmetros desejados e verificando se os valores sugeridos coincidiam com os valores calculados.

![image-20201219164354358](Images - relatorio/image-20201219164354358.png)

![image-20201219164838524](Images - relatorio/image-20201219164838524.png)

A simulação pode ser reproduzida importando o seguinte arquivo de texto:

```
SINGLE_SWITCH_FORWARD
125
125
50
2
250
0.7
80
30
80
0.7
90
47
```



