# 4.15

![image-20210313231030568](Images - Relatório/image-20210313231030568.png)

Utilizaremos uma aproximação usando Série de Fourier, sendo essa:

![image-20210314013038850](Images - Relatório/image-20210314013038850.png)

Calcularemos com apenas as duas primeiras harmônicas (2º e 4º). Para visualizarmos os efeitos da aproximação com apenas dois termos, utilizamos a ferramenta [falsted](http://www.falstad.com/fourier/):

![image-20210314190815099](Images - Relatório/image-20210314190815099.png)

A tensão e corrente de saída foi calculada conforme segue:

![image-20210314194321881](Images - Relatório/image-20210314194321881.png)

E as potências:

![image-20210314194828059](Images - Relatório/image-20210314194828059.png)

As grandezas calculadas estão em conformidade com a simulação, na carga R:

![image-20210314194542257](Images - Relatório/image-20210314194542257.png)

E na fonte:

![image-20210314195020832](Images - Relatório/image-20210314195020832.png)

# 4.23

![image-20210313231043472](Images - Relatório/image-20210313231043472.png)

![image-20210314000852582](Images - Relatório/image-20210314000852582.png)

![image-20210314000904808](Images - Relatório/image-20210314000904808.png)

Esforços nos diodos:

![image-20210314001416369](Images - Relatório/image-20210314001416369.png)

correntes de entrada = correntes de saída

Fator de potência da estrutura:

![image-20210314000919279](Images - Relatório/image-20210314000919279.png)

![image-20210314005057146](Images - Relatório/image-20210314005057146.png)

![image-20210314001644300](Images - Relatório/image-20210314001644300.png)