# Problema 6.10

## Enunciado

A buck converter has an input voltage that varies between 10 and 15 V and a load
current that varies between 0.5 A and 1.0 A. The output voltage is 5 V. For a
switching frequency of 200 kHz, determine the minimum inductance to provide
for continuous current for every operating possibility.

## Formulação do problema

No limite da condução continua, teremos (Hart, 2011, equação 6-53):
$$
L=\frac{(1-D)R}{2f}=\frac{\left(1-\frac{V_{out}}{V_{in}}\right)\frac{V_{out}}{I_{out}}}{2f}
$$
Pelos requisitos do projeto, devemos resolver:
$$
\\
\text{Minimize:}\\
L(V_{in}, I_{out})=\frac{\left(1-\frac{V_{out}}{V_{in}}\right)\frac{V_{out}}{I_{out}}}{2f}
\\

\text{Subject to:}
\\
V_{in} \in [10, 15]\\
I_{out} \in [0.5, 1]
$$

## Cálculos com Smath

Para resolver o problema de otimização com limitação no domínio contínuo, utilizou-se o método numérico GoldenSectionSearch:

![image-20201201202223321](Images - 6_10_relatorio/image-20201201202223321.png)

Com esses parâmetros em mãos, dimensionou-se capacitor e indutor:

![image-20201201202337174](Images - 6_10_relatorio/image-20201201202337174.png)

Por complitude, calculou-se os esforços no interruptor e diodo:

![image-20201201202415992](Images - 6_10_relatorio/image-20201201202415992.png)

## Simulação

Simulando no regime	permanente, temos os seguintes resultados:

![image-20201201202547715](Images - 6_10_relatorio/image-20201201202547715.png)

![image-20201201202724039](Images - 6_10_relatorio/image-20201201202724039.png)

![image-20201201202839435](Images - 6_10_relatorio/image-20201201202839435.png)

![image-20201130222431925](Images - 6_10_relatorio/image-20201130222431925.png)

# Problema 6.26

## Enunciado

Design a buck-boost converter to supply a load of 75 W at 50 V from a 40-V
source. The output ripple must be no more than 1 percent. Specify the duty ratio,
switching frequency, inductor size, and capacitor size.

## Calculos e simulação

A partir dos requisitos do projeto, calculou-se:

![image-20201201214724452](Images - relatorio/image-20201201214724452.png)

![image-20201201214809587](Images - relatorio/image-20201201214809587.png)

![image-20201201214842875](Images - relatorio/image-20201201214842875.png)



Por complitude, calculou-se os esforços na chave e diodo:

![image-20201201215645034](Images - relatorio/image-20201201215645034.png)

![image-20201201215252590](Images - relatorio/image-20201201215252590.png)

![image-20201201215442369](Images - relatorio/image-20201201215442369.png)

