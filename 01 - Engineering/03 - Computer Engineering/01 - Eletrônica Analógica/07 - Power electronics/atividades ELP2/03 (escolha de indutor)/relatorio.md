Aluno: Leonardo Santiago Benitez Pereira

# Definições iniciais

Para definir o valor o indutor para $∆I=10%$, utilizou-se a ferramenta Power Stage Designer:

![image-20220924145033278](Images - relatorio/image-20220924145033278.png)

Escolheu-se $L=100\micro H$

Para a corrente de $8.33A$, com densidade de corrente de $2450A/cm²$, e com apenas 1 fio em paralelo, é necessário utilizar um fio AWG 14 

# Utilizando um núcleo da Magnetics

Utilizou-se o núcleo 00K3515E090.

O indutor precisa de 38 espiras.

Haverá 0.28W de perdas, levando a uma elevação de temperatura de 6.3ºC

![image-20221002110056690](Images - relatorio/image-20221002110056690.png)

# Utilizando um núcleo da Thorton

Utilizou-se o núcleo E-55.

O indutor precisa de 68 espiras.

Haverá 6W de perdas, levando a uma elevação de temperatura de 64 ºC