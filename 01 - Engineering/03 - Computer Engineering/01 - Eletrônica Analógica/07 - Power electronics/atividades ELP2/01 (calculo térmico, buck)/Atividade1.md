Aluno: Leonardo Benitez

[TOC]

# Formas de onda teóricas

Utilizou-se a ferramenta Power Stage Designer:

![image-20220903221203923](Images - Atividade1/image-20220903221203923.png)

$R = \frac{V^2}{P} = 1.44 \Omega$

## Indutor

![image-20220903220831703](Images - Atividade1/image-20220903220831703.png)

![image-20220903220840339](Images - Atividade1/image-20220903220840339.png)

## Diodo

![image-20220903220904096](Images - Atividade1/image-20220903220904096.png)

![image-20220903220923656](Images - Atividade1/image-20220903220923656.png)

Max current: 8.75A
Min current: 7.91A
RMS current: 6.74A

## Transistor

![image-20220903221135687](Images - Atividade1/image-20220903221135687.png)

![image-20220903221041112](Images - Atividade1/image-20220903221041112.png)

Max current: 8.75A
Min current: 7.91A
RMS current: 4.9A

# Simulação

Utilizou-se a ferramenta PSIM

![image-20220911200945799](Images - Atividade1/image-20220911200945799.png)

## Indutor

![image-20220911200924793](Images - Atividade1/image-20220911200924793.png)

## Diodo

![image-20220911201109680](Images - Atividade1/image-20220911201109680.png)

## Transistor

![image-20220911201236061](Images - Atividade1/image-20220911201236061.png)

# Projeto dos componentes

## Diodo

### Perdas por condução

perdas (W) = tensão-direta (V) \* corrente-média-eficaz (A)

$V_{to} = 0.7V$

![image-20220911134501334](Images - Atividade1/image-20220911134501334.png)

$V_{on} = 1.15V$

![image-20220911181220186](Images - Atividade1/image-20220911181220186.png)

$I_{rms} = 6.74A $

$P_{condução} = V_{on}*I_{rms} = 7.75 W$

### Perdas por comutação

$Q_{rr} = 195 nC$
$E = 36 V$
$f = 100 KHz$
$P_{comutação} = Qrr*E*f = 0.70W$

### Calculo térmico

$P_{total} = P_{condução} + P_{comutação}$

$T_a= 50 ºC$
$T_j = 125 ºC$
$Rjc = 2 ºC/W$
$Rcd = 0.5 ºC/W$
$Rda_{sem-dissipador} = 75 ºC/W$
$Rja_{max} = \frac{T_j - T_a}{P_{total}}$
$Rda_{max} = Rja_{max} - Rjc - Rcd$

Precisa de dissipador, pois Rda sem dissipador (75.00) é maior que Rda_max (6.37)
Dissipador escolhido: Brazzeli BR 602 4’’ (Rda = 5.26)

![image-20220911200526102](Images - Atividade1/image-20220911200526102.png)

## Transistor

### Perdas por condução

$I_{eficaz} = 4.61A$
$r_{DF_{ON}} = 1.8*0.022 = 0.0396 ohm$

![image-20220904105612448](Images - Atividade1/image-20220904105612448.png)

$P_{cond}=I{eficaz}^2 * r_{DF_{ON}} = 0.84W$

### Perdas por comutação

$t_r= 84ns$
$t_f= 15ns$
$f = 100 KHz$
$i_{d_{on}} = i_{max} = 9.02A$
$v_{ds_{off}} = 36V$

$P = f/2*(tr+tf)*i_{d_{on}}*v_{ds_{off}} = 1.61W$

### Calculo termico

$P_{total} = P_{condução} + P_{comutação}$

$T_a= 50 ºC$
$T_j = 125 ºC$
$Rjc = 1.44 ºC/W$
$Rcd = 0.5 ºC/W$
$Rda_{sem-dissipador} = 62 ºC/W$
$Rja_{max} = \frac{T_j - T_a}{P_{total}}$
$Rda_{max} = Rja_{max} - Rjc - Rcd$

Precisa de dissipador, pois Rda sem dissipador (62.00) é maior que Rda_max (28.69)
Dissipador escolhido: BR 903 4'' (Rda = 19.53)

![image-20220911200505335](Images - Atividade1/image-20220911200505335.png)

