from power_electronics import *


###########
## Diodo
# TODO: a equação do P_diodo_condução está errada
# O certo é $Vto * ID_med + rt * ID_ef^2$
# Veja slide 7 da aula de cálculo térmico (aula 2)
print('# Diodo')
Von = 1.15 
Irms = 6.74
P_diodo_condução = Von*Irms
print("P_diodo_condução = %.2f W"%P_diodo_condução)


Qrr = 195E-9
E=36
f=100000
P_diodo_comutação = Qrr*E*f
print("P_diodo_comutação = %.2f W"%P_diodo_comutação)

P_diodo_total = P_diodo_condução + P_diodo_comutação
termal_calc(
    P = P_diodo_total,
    Ta = 50,
    Tj = 125,
    Rjc = 2,
    Rcd = 0.5,
    Rda = 75,
)
#########
## Transistor
print('------------')
print('# Transistor')
I_eficaz = 4.61
rdfon = 0.0396
P_transistor_condução = I_eficaz**2*rdfon
print("P_transistor_condução = %.2f W"%P_transistor_condução)

tr = 84E-9
tf = 15E-9
f = 100E3
idon = 9.02
vdsoff = 36
P_transistor_comutação = f/2*(tr+tf)*idon*vdsoff
print("P_transistor_comutação = %.2f W"%P_transistor_comutação)

P_transistor_total = P_transistor_condução + P_transistor_comutação
termal_calc(
    P = P_transistor_total,
    Ta = 50,
    Tj = 125,
    Rjc = 1.44,
    Rcd = 0.5,
    Rda = 62,
)
