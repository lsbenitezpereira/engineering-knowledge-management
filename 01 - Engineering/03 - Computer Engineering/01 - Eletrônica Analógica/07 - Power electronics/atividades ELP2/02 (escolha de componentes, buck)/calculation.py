import sys
sys.path.append('..')
from power_electronics import *


###########
## Diodo
print('# Diodo')
Von = 1.00
Irms = 6.75
P_diodo_condução = Von*Irms
print("P_diodo_condução = %.2f W"%P_diodo_condução)


Qrr = 195E-9
E=36
f=50000
Qrr = 680E-9
P_diodo_comutação = Qrr*E*f
print("P_diodo_comutação = %.2f W"%P_diodo_comutação)

P_diodo_total = P_diodo_condução + P_diodo_comutação
termal_calc(
    P = P_diodo_total,
    Ta = 50,
    Tj = 125,
    Rjc = 2.5,
    Rcd = 1,
    Rja = 62,
    case = 'TO-263',
)

#########
## Transistor
print('------------')
print('# Transistor')
I_eficaz = 4.91
rdfon = 1.19*0.320
P_transistor_condução = I_eficaz**2*rdfon
print("P_transistor_condução = %.2f W"%P_transistor_condução)

tr = 7E-9
tf = 4E-9
f = 50E3
idon = 9.16
vdsoff = 36
P_transistor_comutação = f/2*(tr+tf)*idon*vdsoff
print("P_transistor_comutação = %.2f W"%P_transistor_comutação)

P_transistor_total = P_transistor_condução + P_transistor_comutação
termal_calc(
    P = P_transistor_total,
    Ta = 50,
    Tj = 125,
    Rjc = 2.5,
    Rcd = 1,
    Rja = 40,
    case = 'TO-263',
)