Aluno: Leonardo Benitez

[TOC]

# Formas de onda teóricas

Utilizou-se a ferramenta Power Stage Designer:

![image-20220918115716164](Images - Atividade/image-20220918115716164.png)

$R = \frac{V^2}{P} = 1.44 \Omega$

## Diodo

![image-20220918115631311](Images - Atividade/image-20220918115631311.png)

## Transistor

![image-20220918115603718](Images - Atividade/image-20220918115603718.png)

## Capacitor

![image-20220918115646750](Images - Atividade/image-20220918115646750.png)

# Projeto dos componentes

## Diodo

### Escolha do componente

Realizou-se a escolha por meio do catalogo da Mouser.
Filtrou-se inicialmente por Diodes & Rectifiers, Mounting Style = SMD, Reverse Voltage > 400V, Forward Current > 7A, RoHs = True.
Sobraram 762 opções, e filtrou-se adicionalmente por Forward Voltage < 0.9 (objetivando diminuir as perdas).
Sobraram 12 opções. As mais baratas possuiam encapsumento TO-277A, porem não foi possível encontrar um dissipador para o esse encapsulamento e rejeitou-se essas opções. 

O componente escolhido foi o VS-12CDU06-M3, da fabricante Vishay, no encapsulamento TO-263AC.

### Perdas por condução

perdas (W) = tensão-direta (V) \* corrente-média-eficaz (A)

$V_{on} = 1.00V$

![image-20220918145004513](Images - Atividade/image-20220918145004513.png)

$I_{rms} = 6.75A $

$P_{condução} = V_{on}*I_{rms} = 6.75 W$

### Perdas por comutação

$Q_{rr} = 680 nC$
$E = 36 V$
$f = 50 KHz$
$P_{comutação} = Qrr*E*f = 1.22 W$

### Calculo térmico

$P_{total} = P_{condução} + P_{comutação}$

$T_a= 50 ºC$
$T_j = 125 ºC$
$Rjc = 2.5 ºC/W$
$Rcd = 1 ºC/W$
$Rja = 62 ºC/W$
$Rja_{max} = \frac{T_j - T_a}{P_{total}}$
$Rda_{sem-dissipador} = Rja - Rjc - Rcd$
$Rda_{max} = Rja_{max} - Rjc - Rcd$

Precisa de dissipador, pois Rda sem dissipador (58.50) é maior que Rda_max (5.91)
Dissipador escolhido: Aavid 7109DG, 200 feet/min 4'' (Rda = 3.90)

![image-20220918150227445](Images - Atividade/image-20220918150227445.png)

## Transistor

### Escolha do componente

Realizou-se a escolha por meio do catalogo da Mouser.
Filtrou-se inicialmente por MOSFET, Mounting Style = SMD, Technology = SiC, RoHs = True, Drain-Source Breakdown Voltage > 400V, Continuous Drain Current > 4.91A.
Sobraram 2 opções, e escolheu-se o mais barato.

O componente escolhido foi o C3M0065100J, da fabricante Wolfspeed, no encapsulamento TO-263-7.

### Perdas por condução

$I_{eficaz} = 4.91A$
$r_{DF_{ON}} = 1.19*0.320 \Omega$

![image-20220918114312127](Images - Atividade/image-20220918114312127.png)

$P_{cond}=I_{eficaz}^2 * r_{DF_{ON}} = 9.18 W$

### Perdas por comutação

$t_r= 7ns$
$t_f= 4ns$
$f = 50 KHz$
$i_{d_{on}} = i_{max} = 9.16A$
$v_{ds_{off}} = 36V$

$P = f/2*(tr+tf)*i_{d_{on}}*v_{ds_{off}} = 0.09 W$

### Calculo termico

$P_{total} = P_{condução} + P_{comutação}$

$T_a= 50 ºC$
$T_j = 125 ºC$
$Rjc = 2.5 ºC/W$
$Rcd = 1 ºC/W$
$Rja = 40 ºC/W$
$Rja_{max} = \frac{T_j - T_a}{P_{total}}$
$Rda_{sem-dissipador} = Rja - Rjc - Rcd$
$Rda_{max} = Rja_{max} - Rjc - Rcd$

Precisa de dissipador, pois Rda sem dissipador (40.00) é maior que Rda_max (4.59)
Dissipador escolhido: Aavid 7109DG, com ventilação forçada de  200 feet/min 4 (Rda = 3.90), que é o mesmo utilizado no diodo.

## Capacitor

### Escolha do componente

Realizou-se a escolha por meio do catalogo da Mouser.
Filtrou-se inicialmente por Capacitors, Mounting Style = SMD, RoHs = True, Capacitance = 100uF, Voltage Rating DC > 20V, Type = Electrolytic.
Sobraram 830 opções, das quais se escolheu a mais barata.

O componente escolhido foi o A780MS107M1JLAV030, da fabricante Kemet.

A vida útil esperada é de 4000 horas, na faixa de temperatura de -55°C a +125°C
