import json

def Qrr_from_Trr(E: float, L: float, Trr: float) -> float:
    E = 36
    L = 100E-6
    Trr = 2.6E-6
    Qrr = (Trr**2 * E/L)/3
    return Qrr

###########
## heatsinks
with open("../heatsinks.json") as f:
    heatsinks = json.load(f)

def termal_calc(P, Ta, Tj, Rjc, Rcd, Rja, case: str = "TO-220", prefered_height: float = 4, max_height: float = 10):
    Rda = Rja - Rjc - Rcd
    R_ja_max = (Tj - Ta)/P
    print("R_ja_max = %.2f"%R_ja_max)

    Rda_max = R_ja_max - Rjc - Rcd
    if Rda < Rda_max:
        print("Não precisa de dissipador, pois Rda sem dissipador (%.2f) é menor que Rda_max (%.2f)"%(Rda,Rda_max))
    else:
        print("Precisa de dissipador, pois Rda sem dissipador (%.2f) é maior que Rda_max (%.2f)"%(Rda,Rda_max))
        heatsink = chose_heatsink(heatsinks, Rda_max, case=case, prefered_height=prefered_height, max_height=max_height)
        print("Dissipador escolhido: %s %.0f'' (Rda = %.2f)"%(heatsink['name'], heatsink['height'], heatsink['Rda']))



def chose_heatsink(heatsinks, Rda_max, case, prefered_height: float = 4, max_height: float = 10):
    possibilities = []
    for heatsink in heatsinks:
        if case in heatsink["cases"] and heatsink["height"] <= max_height and heatsink["Rda"] <= Rda_max:
            possibilities.append(heatsink)

    if len(possibilities) == 0:
        # TODO: try again changing the height
        # TODO: try again with convecção forçada
        raise Exception("No heatsink found for Rda_max=%.2f and case=%s"%(Rda_max, case))
    
    # chose the argmin heigher that zero
    possibilities.sort(key=lambda heatsink: Rda_max - heatsink['Rda'])
    return possibilities[0]