[TOC]

Conceptualization
=================

-   categories of isolation:

    ![](./Images/Estruturas isoladoras/media/image1.png){width="3.2708333333333335in"
    height="2.0729166666666665in"}

Optocoupler
===========

-   funciona até uns 200KHz

-   (-) baixa tensão de isolação

-   ?

    ![](./Images/Estruturas isoladoras/media/image2.png){width="1.9479166666666667in"
    height="1.5in"}![](./Images/Estruturas isoladoras/media/image3.png){width="1.8333333333333333in"
    height="1.4791666666666667in"}![](./Images/Estruturas isoladoras/media/image4.png){width="1.8125in"
    height="1.4895833333333333in"}![](./Images/Estruturas isoladoras/media/image5.png){width="1.8541666666666667in"
    height="1.5104166666666667in"}

    ![](./Images/Estruturas isoladoras/media/image6.png){width="2.3333333333333335in"
    height="1.875in"}

# Transformador de pulso

* (+) operação em alta frequência
* (+) alta isolação de tensão
* limitado em razão cíclica: precisa ser o suficiente para desmagnetizar o núcleo
* se coloca um capacitor em série com o primário para tiarar o nível CC, se não satura o núcleo
* diferenças construtivas
  * próprio para altas frequências bem altas (centenas de KHz)
  * geralmente corrente baixas (só pra acionamento)

# Optic fiber

* Example: HFBR-3810Z
* High frequency (MHz)
* High noise immunity
