[TOC]

# Introdução

Neste relatório apresenta-se a implementação em software de simulação da estrutura osciladora *Wien Bridge* (Figura 1), nas frequências de $500Hz$ e $5000Hz$, utilizando o amplificador operacional TL072, implementada como parte da disciplina de Eletrônica 3 do curso de Engenharia Eletrônica.

![image-20210327235736235](Images - relatorio/image-20210327235736235.png)

> Figura 1 - Estrutura *Wien Bridge* (Sedra, 2015)

Utilizou-se o software Proteus para a simulação numérica do circuitos, utilizando os seguintes parâmetros de configuração:

![image-20210327232552943](Images - relatorio/image-20210327232552943.png)

![image-20210327232619984](Images - relatorio/image-20210327232619984.png)

# Oscilador em 500Hz

Projetou-se o circuito utilizando as equação demonstradas em sala de aula, onde desconsiderava-se as seguintes não-idealidades:

* Defasagem do ampop;
* Efeitos da carga no ampop;
* Impedância de entrada finita do ampop.

Com o valor de $R=6366.2\Omega$ e $C=50nF$, temos o circuito oscilando em $500Hz$ sem a necessidade de nenhum ajuste adicional.

![image-20210327231827138](Images - relatorio/image-20210327231827138.png)

![image-20210327231651964](Images - relatorio/image-20210327231651964.png)

# Oscilador em 5000Hz

## Projeto inicial

Partiu-se das equação demonstradas em sala, mas desta vez não-idealidades influenciaram de forma perceptível o funcionamento do circuito, que oscilou $f=4901Hz$ ($99Hz$ abaixo do desejado).

![image-20210327225837682](Images - relatorio/image-20210327225837682.png)

![image-20210327225514502](Images - relatorio/image-20210327225514502.png)

Abrindo a malha e excitando o circuito com uma senoide de frequência $5000Hz$, vemos que existe uma defasagem de $0.76^\circ $ (entre a entrada do bloco A e a saida do bloco B) e, portanto, os critérios de Barkhausen não são cumpridos na frequência desejada.

![image-20210327230658355](Images - relatorio/image-20210327230658355.png)

![image-20210327230742057](Images - relatorio/image-20210327230742057.png)

## Alterando o circuito

Para nos aproximarmos da frequência de oscilação desejada substitui-se os resistores do bloco B por potenciômetros, por meio dos quais foi possível obtermos uma frequência mais próxima da desajada, $f=5035Hz$, ao ajustador os potenciômetros para $R=2000*0.31=620\Omega$. Nessa frequência é evidente que os critérios de Barkhausen são cumpridos, apesar das equações utilizadas indicarem que o circuito deveria estar oscilando em $f= 5134Hz$.

![image-20210327225837682](Images - relatorio/novo.png)

![image-20210321232826755](Images - relatorio/image-20210321232826755.png)

# Conclusões

A escolha do valor do capacitor no bloco B é relevante para o funcionamento do circuito, uma vez que esse bloco está atuando como carga do bloco B. Inicialmente tentou-se implementar o circuito com $C=100nF$, mas oscilação foi quase uma onda triangular (ou uma senoide “caída”), e a estrutura só entrou em oscilação com uma ganho consideravelmente acima de 3. Dessa forma, foi necessário diminuir o capacitor para aumentar a impedância do bloco B.

Também foi necessário ajustar o valor de R por meio de potenciômetros: pelo calculado a frequência ficou abaixo, então subimos o R para “forçar a frequência a aumentar”. 

Por fim, destacamos que entrar e se manter em oscilação foi “delicado”, sendo necessário ajustes simultâneos nos dois potenciômetros do bloco B e no ganho do bloco A, embora tenha sido mais simples que o circuito de *phase-shift*.

