### SEDRA
SEDRA, Adel S.; SMITH, Kenneth C.. Microelectronic Circuits. 7. ed. Oxford: Oxford University Press, 2015
 Sedra e Smith (2015),
(SEDRA; SMITH, 2015)

### MALVINO
MALVINO, Albert Paul; BATES, David J.. Electronic Principles. 8. ed. New York: Mcgraw-hill, 2016.
Malvino e Bates (2016)
(MALVINO; BATES, 2016)



### 1n4007

VISHAY, 2011. General Purpose Plastic Rectifier. [S.l].

Vishay (2011)

(VISHAY, 2011)







|          |        |
| -------- | ------ |
| Vripple- | 4,3 mA |
| Vripple+ | 3,6 mA |
|          |        |
|          |        |
|          |        |
|          |        |
|          |        |
|          |        |
|          |        |

