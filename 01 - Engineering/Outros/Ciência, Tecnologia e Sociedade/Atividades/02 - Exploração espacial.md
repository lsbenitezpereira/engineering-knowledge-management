série moon machines

competição -> progresso? muriel acha que sim 



in the early 1960s when the then US president, John F Kennedy, announced the race to the moon

* Navegation system
  * First fucking embedded system
  * MIT inertial nevegation
  * inertial navegation was putted to seconds place, and just used in the dark side of the moon
* The human factor
  * duvidas no processo: o uqe seria atribuido ao operador humano? nada?
  * human conflicts almost scew everything up
  * Bottleneck: software, programming, logic, reasoning

# Nowadays

* Canada fundings
  * https://www.universetoday.com/145258/beyond-robotic-arms-canada-funds-technology-for-space-exploration/	
* BR
  * não é mais signatario da estação espacial?

# Planeta 01 - Terra

> “I believe that life on Earth is at an ever-increasing risk of being wiped out by a disaster, […] I think the human race has no future if it doesn’t go to space”. Stephen Hawking [1]

> “It is unknown whether we are the only civilization currently alive in the observable universe, but any chance that we are is added impetus for extending life beyond Earth” Elon Musk [2] 

>  “Our Government is positioning Canada’s space sector to reach for the Moon and beyond, […] to achieve world firsts in space science and exploration.” Navdeep Bains, ministro de Inovação, Ciência e Indústria do Canadá, após anunciar um fundo de US$ 4,36 milhões (2020) [3]

Um alarmista, um político ambicioso e um trapaceiro? Ou será que realmente deveríamos estar dedicando mais energia ao desenvolvimento de tecnologias aeroespaciais? 

Investir em exploração espacial não é sobre botar o pé na lua (ao menos não atualmente), e sim sobre baratear os custos para lançamento de satélites (beneficiando assim milhões de usuários de serviços de telecomunicações), desenvolvimento de materiais leves e resistentes, e até defesa nacional [4]. Em última análise, trata-se de uma questão de sobrevivência da espécie humana em um cenário de devastação bélica, escassez de recursos naturais, desastre biológico (#CoronaVirus), entre outros. 

Infelizmente para os retardatários, esse não é um bonde que se pega com facilidade, por exemplo: a missão Apolo 11 tomou 9 anos de planejamento [5], a Estação Espacial Internacional demorou 13 anos para ser montada [6] e a empresa SpaceX demorou 6 anos para lançar seu primeiro foguete [7]. E o Brasil? Possui um plano estratégico para o desenvolvimento aeroespacial? Ou vamos esperar até o bonde estar inalcançável? 

# Referências

[1] Phoebe Weston (2017). Stephen Hawking warns that humans must leave Earth in 100 years. Nzherald. Disponível em: https://www.nzherald.co.nz/technology/news/article.cfm?c_id=5&objectid=11850888

[2] Elon Musk (2018). Twitter. Disponível em: https://twitter.com/elonmusk/status/1011085383239589888

[3] Matt Williams (2020). Beyond Robotic Arms: Canada Funds Technology for Space Exploration. Universetoday. Disponível em: https://www.universetoday.com/145258/beyond-robotic-arms-canada-funds-technology-for-space-exploration/

[4] Sandra Erwin (2019). Trump signs defense bill establishing U.S. Space Force: What comes next. Spacenews. Disponível em: https://spacenews.com/trump-signs-defense-bill-establishing-u-s-space-force-what-comes-next/

[5] Wikipedia (2020). Apolo 11. Disponível em: https://en.wikipedia.org/wiki/Apollo_11

[6] Kelli Mars (2018). 20 Years Ago, Space Station Construction Begins. Nasa. Disponível em: https://www.nasa.gov/feature/20-years-ago-iss-construction-begins

[7] Wikipdia (2020). SpaceX. Disponível em: https://en.wikipedia.org/wiki/SpaceX