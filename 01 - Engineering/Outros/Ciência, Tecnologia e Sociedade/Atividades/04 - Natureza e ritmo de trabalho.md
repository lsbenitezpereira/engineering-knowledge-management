## Natureza; Ritmo circadiano; Ritmo de trabalho.

Durante os meses que morei na Finlândia tive a oportunidade de ir para o extremo norte do país para caçar a aurora boreal, dita um dos fenômenos naturais mais belos da natureza. A minha caçada foi fracassada e eu não tive sorte de ver a Aurora, mas pelo menos me deu um bom gancho para falar do tema de hoje.

![Aurora, Polar Lights, Northen Lights, Aurora Borealis](Images - 04 - Natureza e ritmo de trabalho/aurora-1185464_960_720.jpg)

> Fonte: [Noel Bauza, Pixabay](https://pixabay.com/photos/aurora-polar-lights-northen-lights-1185464/)

Finlandeses gostam muito de atividades *outdoor*: eles são a nação mais fisicamente ativa da União Europeia [1], precisamente. Certo dia fui correr ao redor do lago onde morava e, para minha surpresa, vi bastante gente fazendo o mesmo, embora fosse um dia super úmido e nublado. Famílias inteiras fazendo piquinique, *macarra* (salsicha na grelha), exercício físico, etc. 

![](/home/benitez/Pictures/01 - Trips/2019-09 Finlândia (Propicie)/Hämeenlinna - Aulanko/DSCN4010.JPG)

> Fonte: autoria própria

Em outra ocasião eu participei da *Syyspatikka*, a última trilha do outono, com um grupo local de escoteiros. Novamente: dia úmido, frio, mas dezenas de pessoas se reuniram para passar um tempo "juntas" (dentro da percepção finlandesa de proximidade) e em meio a natureza. 

Nessa ocasião os dias já estavam consideravelmente mais curtos: amanhecia em torno de 9hs e anoitecia perto das 16hrs. Gradualmente as horas de sol diminuiriam ao longo do próximos meses, e em algumas regiões não haveria mais sol nenhum durante várias semanas. O ritmo circadiano desse povo merece ser estudado [2,3].

![](/home/benitez/Pictures/01 - Trips/2019-09 Finlândia (Propicie)/Syyspatikka/69877224_2858648107497224_7881376824813420544_o.jpg)
![](/home/benitez/Pictures/01 - Trips/2019-09 Finlândia (Propicie)/Syyspatikka/*DSCN3679.JPG)

> Fonte: autoria própria

Uma última historinha: finlandeses e seu curioso ritmo de trabalho. O horário do cafezinho é **sagrado**. Todos os dias perto das 15hrs todos se reúnem na sala do café, para falar de qualquer coisa que não seja trabalho. Se você não quiser interromper o que está fazendo, eles (no plural) vão educadamente recomendar que você dê um tempo para refrescar as ideias. E vários exemplos similares estão presentes na rotina deles: trabalhar com seriedade, mas com um ritmo saudável e respeitando a divisão trabalho-vida pessoal.


![](/home/benitez/Pictures/01 - Trips/2019-09 Finlândia (Propicie)/Hamk Smart Research Unit/WhatsApp Image 2019-10-15 at 14.37.31(1).jpeg)

> Fonte: autoria própria

O que esses três relatos têm em comum? Nada, rsrs.
Eu quero chegar em algum lugar com isso? Sim.
No nosso afã diário por resultado muita vezes esquecemos que o bem-estar é fundamental para nossa produtividade, e que a nossa relação com o ambiente ao nosso redor (entenda *ambiente* incluindo porém não se limitando à *natureza*) exerce gigante influência sobre como nos sentimos. 

Shawn Achor aponta no livro *The Happiness Advantage* [4] que adota uma rotina voltada para o bem-estar e crescimento pessoal promove um grande aumento na produtividade no trabalho, na qualidade das relações interpessoais e na capacidade de convencer e comunicar ideias (o que novamente aumenta a produtividade e acelera o progresso profissional). 
Dessa forma, um ritmo de vida saudável não deve ser apenas uma recompensa depois de muito esforço: devemos nos dedicar ativamente à construirmos uma rotina saudável, pois todos os outros aspectos da nossa vida dependem disso.

## Referências

[1] - https://www.goodnewsfinland.com/finland-scores-highest-in-eu-in-happiness-and-physical-activity/


[2] - https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3793275/

[3] - https://www.nature.com/articles/srep39976

[4] - Achor, S. (2010). *The happiness advantage: The seven principles of positive psychology that fuel success and performance at work*. 

