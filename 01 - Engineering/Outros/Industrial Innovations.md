# Conceptualization

* If the third revolution was based on machines with embedded computers,
and the fourth revolution interconnected machines and things, including
  information about the materials and energy usage flowing into and out of a globally
  interconnected cyberphysical system of systems (sorce: Ammar)
  
* Historic overview

  * See details is *History/Modern History*
  * 1º Indutrial revolution (1780): mechanization (water and steam)
  * 2º Industrial Revolution (1870): electrification (mass production assembly lines)
  * 3º Industrial Revolution (1970): automatization (electronics and computers) 
  * 

* Bussines models

  * There is almost a logical sequence in all those revolutions

  * Each one changed the relation between the society and the industrial production 

  * The jobs became more high-values (and with less low-values workers, aka demitions)

  * Gradually the production became more produtive/faster, with higher alienation and more globalized
  
  * Volume vs diversty graph:
  
    ![1566863234153](Images - Industrial Innovations/1566863234153.png)

# Industry 4.0 - Digitalization

* or *advanced manufecturing*, or *smart industries*
* Based on ciber physics systems
* Automate routine tasks that are best done by robots
* The machines begin to take automatic decisions: when to turn on/off, speed up/down, control the production, etc
* **best quantity and mass production**
* **Historic begining**
  * Desejo de rastrear a produção (geralmente via RFid)
  * Manufatura configurável (é possível alterar facilmente o que a máquina vai produzir)
  * Começou em sei lá, 2008.
* **Enabling technologies**
  * Internet of Things (IoT)
  * Big Date and analytics 
  * Realidade aumentada aplicada ao ambiente industrial (o funcionário usando)
  * Cloud computing
  * Robôs colaborativos (ajudando humanos, inclusive como exoesqueleto) 
  * Ciber physics systems
* **Other annotations**
  * massive increase in safety, quality and reduced waste
  * RAMI 4.0: Reference Architecture Model Industrie 4.0


# Industry 5.0 - Personalization

* Historic
* allow customers to customise what they want
* takes the concept of personalisation to the next level
* **Worker level customization**
  * The personalization is also aplied to the worker, not only to the costumer
  * Improved tools and technics
  * Humans and robots working side-by-side
  * Enhanced decision making using AI
* **Society 5.0**
  * This revolution is more about us humans that about the robots 

