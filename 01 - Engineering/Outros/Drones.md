# Conceituação

* Aeronave grande (estilo avião): Veículo Aéreo Não Tripulado (VANT), quando possui um piloto em solo
* Drone comum: Aeronave nao tripulada remotamente controlada (RPA)
* **Drone Mavic pro**
  * altura máxima: 500m
  * custa uns 5k, e uns 8k em lojas
  * Sensores de colição só na frente 
  * 25 minutos de autonomia (sem filmação)
  * Função return to home automático, já desviando dos obtaculos
  * Reserva bateria para o retorno (já calculando os parâmetros do voo)
  * app no celular (da fabricante) para ver a filmação
  * até 65km/h
  * Modo atti: manual
  * pode voar em modo gps, atti e atti-gps
  * Com gps da pra travar em uma posição, estabilizar melhor, etc
  * em modo gps quando tiver acima de 8 satélites 
  * a bateria custa 800R
* **Interferencias**
  * rede eletrica
  * antena
  * tempestade csolar (baixe o app solar active). Faz o drone perder o controless
* **Outras anotações**
  * Bombeiros de SC: referencia nacional em formação de drones
  * Algumas baterias (==lítio?==) preciam ser transportada em bags especiais, pois possuem risco de explosão
  * DJI: top world drone maker

## Regulamentação

* pode voar até 120 metros (regulamentado por ==?==), e a no mínimo 30 metros sobre pessoas
* Só pilotam maiores de 18 anos (==mesmo as menores de 250g?==)
* **Classificação da anac**
  * P = peso máximo de decolagem
  * classe 1: P>150kg
  * classe 2: 25kg<P<150
  * Classe 3: P<25kg
* **Registros**
  * O drone precisa ser registradado no DECEA
  * O constrole remoto precisa ser inscrito na anatel 
  * O piloto deve estar inscrito (não precisa ter licença)
  * Drones maiores que 250g precisam ser registrados, e todos os voos devem ser informados com antecedência para serem autorizados com 48hs de antecedência









