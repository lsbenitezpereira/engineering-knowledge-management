# Ambientes de prototipação
## Basicão
-   [[Dispenser manual CA tech]{.underline}](http://grupocatech.com/produtos/dispenser-automatica-ca-8700/)
    
-   [[Impressora stencil Charmhigh US\$ 300]{.underline}](http://www.charmhigh-tech.com/sale-9509462-hottest-3040-high-precision-stencil-printer-solder-paste-pringting-machine-smt-production-line.html)
    
-   [[impressora stencil US\$ 950]{.underline}](https://www.smtmax.com/detail.php?id=44)
    
-   [[Impressora stencil VS1520A R\$ ??]{.underline}](http://www.directindustry.com/pt/prod/aurel-automation-spa/product-57033-386197.html?utm_source=ProductDetail&utm_medium=Web&utm_content=SimilarProduct&utm_campaign=CA)
    
-   [[Impressora stencil AE-3088 US\$ 7200]{.underline}](https://www.smtmax.com/detail.php?id=29)
    
-   [[Forno reflow T962A US\$ 270]{.underline}](http://www.charmhigh-tech.com/sale-7840319-t962a-benchtop-reflow-oven-300-320mm-1500w-ic-heater-infrared-bga-rework-station.html)
    
-   [[Forno reflow T962C US\$ 415]{.underline}](http://www.charmhigh-tech.com/sale-7840317-t962c-2500w-reflow-oven-400-600mm-infrared-ic-heater-bga-smd-smt-soldering-welding-sation.html)
    
-   [[Forno reflow T960 US\$ 1200]{.underline}](https://pt.made-in-china.com/co_taianpuhui/product_Reflow-Oven-T960-Automatic-Desktop-SMT-Reflow-Oven-Wave-Soldering-Machine-Taian-Puhui_hrroirsuy.html)
    
-   [[Forno reflow compacto R\$ ??]{.underline}](http://www.anacom.com.br/pt-br/produtos/eda/eletronica-prototipagem-rapida/695-protoflow.html)
    
-   [[Forno reflow FR-17 R\$ ??]{.underline}](https://www.omtec.ind.br/linha+smd+forno+de+refusao+para+placas+de+smd+fr-17.asp)
    
-   [[Pick and place CHMT36VA US\$ 3100]{.underline}](http://www.charmhigh-tech.com/sale-7971156-economic-model-chmt36va-smt-pick-and-place-machine-top-and-bottom-camera-externel-pc.html)
    
-   [[Pick and place VP-2000S US\$ 4000]{.underline}](https://www.smallsmt.biz/home/shop/#cc-m-product-12460016332)
    
-   [[Pick and place CHMT48VA US\$ 4100]{.underline}](http://www.charmhigh-tech.com/sale-7840268-advanced-smt-pick-and-place-machine-chmt48va-top-and-bottom-camera-ccd-system.html)
    
-   [[Pick and place TM220A US\$ ??]{.underline}](http://www.neodentech.com/products_details_1269929.html) (entre US\$ 4\~5k)

-   [[Pick and place Neoden4 US\$ 8500]{.underline}](https://www.ebay.com/itm/NeoDen4-SMT-Pick-and-Place-Machine-Vision-System-19-Feeders-4-Heads-0201-FPGA-/181927079966?_ul=BR)
    
-   [[Pick and place VP-2500DP US\$ 9000]{.underline}](https://www.smallsmt.biz/cl32-pick-and-place-machine/?gclid=EAIaIQobChMIpp3NtNj52gIVhoaRCh2M2Q1_EAAYAiAAEgL3tvD_BwE)
    
-   [[Pick and place TWS US\$ ??]{.underline}](http://www.tws-automation.com/Default.aspx)
    
-   [[Stencil + pick and place + reflow US\$ 4900]{.underline}](http://www.charmhigh-tech.com/sale-9410595-smt-pick-and-place-equipment-2500w-reflow-oven-surface-mount-technology.html)

-   [[stencil + pick and place + reflow (Aliexpress) US\$ 5000]{.underline}](https://pt.aliexpress.com/item/Small-SMT-Production-Line-Pick-and-place-machine-TM220A-Solder-Printer-Reflow-Oven-T-960-Manufacturer/1780387523.html)

-   [[Stencil + pick and place + reflow US\$ 7200]{.underline}](http://www.charmhigh-tech.com/sale-7840314-high-configuration-smt-line-60-feeders-4-heads-chmt560p4-smt-p-p-machine-reflow-oven-t961-solder-pas.html)

-   É preciso ter um espaço para estoque, um ambiente controlado para operar as máquinas e um bolsista exclusivo para operar os equipamentos.

-   Também é preciso levar em consideração os custos de manutenção e operação dos equipamentos.

Solda e montagem

-   Câmara de transferência ultravioleta: R\$ 1000 (se nós fabricamos umas top)
    
-   [[Câmara UV simples US\$ 230]{.underline}](https://www.amazon.com/Exposure-Unit-Hot-Foil-Printing/dp/B00VH7TMQI)
    
-   [[Estação de solda hakko R\$930]{.underline}](http://proesi.com.br/instrumentacao-e-soldagem-3/estacao-ferro-de-solda-e-soprador/estacao-de-solda-hakko-fx-888d-esd-220v.html)
    
-   [[Estação de solda Ersa R\$ 1400]{.underline}](https://www.raisa.com.br/estacao-de-solda/estacao-de-solda-digital-80w-rds80-220v-ersa-000003544.html)
    
-   [[Estação de solda e retrabalho ERSA R\$ ??]{.underline}](http://pspbrasil.com.br/portfolio-item/i-con-vario-4/)
    
-   [[Estação de Retrabalho Hikari R\$880]{.underline}](http://proesi.com.br/instrumentacao-e-soldagem-3/estacao-ferro-de-solda-e-soprador/estac-o-de-retrabalho-hikari-hk-939-pro-220v.html)
    
-   É preciso também ter um espaço exclusivo para soldagem, com os equipamentos fixos e organizados, além de diversos pequenos equipamentos (estanho, alicates, pinças, brocas para a dremmel, pasta de solda, paquímetro, percloreto de ferro, etc etc etc)

Impressora 3d

-   [[marca confiável e conhecida R\$ 16.999,00]{.underline}](https://www.makerbotstore.com.br/impressora-3d-makerbot-replicator)

-   [[2 bicos, boa para em conjunto de outros projetos R\$ 3.646,10 (igual a que tinha no meu estágio (lucas))]{.underline}](https://www.magazineluiza.com.br/impressora-3d-ctc-dual-extrusoras-mk8-factory-direct-abs-pla/p/6925123/ia/iaim/)

-   [[barata e simples R\$ 1.999,00]{.underline}](https://www.magazineluiza.com.br/impressora-3d-bivolt-no-brasil-nagano/p/5317077/ia/iapi/?partner_id=15210&utm_source=google&utm_medium=pla&utm_campaign=PLA_marketplace&gclid=Cj0KCQjwuMrXBRC_ARIsALWZrIgJhW7OW3qkZKCh0O2YwWMXcEgcK2Q5sEhbPGWhI6Q84L08JFeZpHIaAoYHEALw_wcB)
    
-   Precisaríamos comprar também diversos filamentos e alguns acessórios

Cortadoras e perfuradeiras

-   [[Cortadora Laser VS6040 60W R\$ 11000]{.underline}](https://www.multivisi.com.br/maquina-router-laser-cnc-vs6040-corte-e-gravacao-60x40cm-60w?parceiro=8518&gclid=EAIaIQobChMI2q2v2-f52gIVQ4CRCh1kZwahEAYYASABEgL_QvD_BwE)
    
-   [[Cortadora Laser]{.underline}](https://www.multivisi.com.br/maquina-router-laser-cnc-vs6040-corte-e-gravacao-60x40cm-60w?parceiro=8518&gclid=EAIaIQobChMI2q2v2-f52gIVQ4CRCh1kZwahEAYYASABEgL_QvD_BwE)

-   [[VS4040 50W R\$ 9000]{.underline}](https://www.americanas.com.br/produto/29734265/maquina-router-laser-vs4040-corte-e-gravacao-?WT.srch=1&epar=bp_pl_00_go_inf-aces_acessorios_geral_gmv&gclid=EAIaIQobChMIlKCx2tz52gIVkQuRCh2eiQu2EAQYBCABEgLkmPD_BwE&opn=YSMESP&sellerId=10409455000119)

-   [[Prototipadora mecânica R\$ ??]{.underline}](http://www.anacom.com.br/pt-br/produtos/eda/eletronica-prototipagem-rapida/685-protomat-s63.html)
    
-   [[Perfuradeira CNC6040 US\$ 2500]{.underline}](http://www.charmhigh-tech.com/sale-7840291-4th-axis-cnc6040-high-speed-version-desktop-cnc-router-machine-for-3d-engraving.html)
    
-   [[perfuradeira CNC3040 US\$ 960]{.underline}](http://www.charmhigh-tech.com/sale-7840292-professional-cnc3040-3d-cnc-router-cnc-engraving-drilling-and-milling-machine.html)

## Avançadinho

  **Nº**   **Nome**                       **Preço aproximado**   **Pregão**   **Item**

-------- ------------------------------ ---------------------- ------------ ----------

  1        Bancada para eletrônica        4000                                
  2        Estação de solda e dessolda    8500                                
  3        Dispenser automática           2700                                
  4        Forno de Refusão tipo gaveta   8500                                
  5        Pick and Place                 70000                               
  6        Impressora 3D                  40000                               
  7        Alicate amperímetro (2 un.)    866 (x2)               037/2017     1
  8        Osciloscópio portátil          15280                  037/2017     37

### Bancada de eletrônica

Bancada completa com acessórios para soldagem eletrônica. Estrutura em
chapa de aço carbono (14) com pintura feita através de processo
eletrostático com tinta de poliéster em pó termo-endurecível, acabamento
texturizado (preto) e micro texturizado (grafite), com suporte para
luminária e sistema de pé, mais par de colunas, com sapatas ajustáveis.

Dimensões da Bancada: 2000(A) x 1500(C) x 700(L) mm.

Dimensões da Mesa: 1500(C) x 700(L) mm.

Dimensões da Prateleira: 1500 (C) x 300 (L) mm.

Peso aproximado com os acessórios de 168Kg.



* **Acessórios mínimos a serem incluídos:**
  * 1\) Prateleira Adicional com as mesmas dimesões especificadas
  * 1\) Régua elétrica 220V com 10 tomadas ABNT NBR14136 embutidas padrão
    20A, dois disjuntores DIN 10A e um interruptor 6A para luminária.
  * 2\) Luminária LED 2x18W Bi-volt com fluxo luminoso de 1400lm.
  * 2\) Gaveteiro com 3 gavetas em chapa de aço carbono (18), puxadores
    estampados na própria peça com fechaduras de metal cromado e chaves
    independentes.
  * 3\) Telas perfuradas, no mínimo três, para ganchos porta ferramentas,
    gancho porta balancim, bandeja porta objetos, bandeja porta chaves, Kit
    de gaveteiros BIN.5 e suporte para rolo de solda de até 500g.
  * 4\) Kit de ganchos para ferramentas com 10 unidades
  * 5\) Lupa com luminária de LED, lente de vidro com proteção articulável
    com diâmetro de 127 mm, dioptria 5x ou 8x de aumento, tensão de entrada
    bi-volt automático, 48 lâmpadas de LED potência 6W, articulação dos
    braços de +/- 1 metro, molas embutidas, suporte de fixação tipo \"C\",
    pintura de poliéster em pó cor branca.
  * 6\) Gancho para balancim
  * 7\) Kit de gaveteiros BIN.5 com 9 unidades
  * 8\) Suporte para rolo de solda de até 500g
  * 9\) Bandeja porta objetos
  * 10\) Bandeja porta chaves
  * 11\) Kit antiestático 1,50 x 0,70 cm, com manta de duas camadas
    dissipativa e condutiva com cordão de aterramento duplo jack e duas
    pulseiras
  * 12\) Cadeira Executiva tipo Caixa com rodízios. Coluna à gás com
    regulagem de altura mínima de 60 mm e máxima de 75 mm, acabamento em
    PVC, rodízios de PU. Assento e encosto estofado em espumas injetadas de
    50 mm de altura, com acabamentos em tecido preto com proteção ESD.
  * 13\) Apoio para os pés feita em aço (14)

### Estação de solda e dessolda

* Estação de solda e dessolda multicanal de componentes eletrônicos, com
  reconhecimento automático das ferramentas e capaz de permitir a operação
  simultânea de no mínimo quatro ferramentas. A estação deve permitir ser
  configurada via cartão de memória MicroSD.
* Deve possuir um banco de energia para permitir operação contínua. Todas
  as funcionalidades, incluindo o gerador de vácuo (pressão de 700mbar) e
  ar (volume de ar de 2-20L/min), devem estar embutidas na estação
  central. Função standby com função selecionável de temperatura e tempo
  de ativação.
* Potência mínima da estação: 500W.
* Faixa de temperatura mínima: 150 -- 450 °C
* Dimensões aproximadas: 300 mm x 280 mm x 115 mm
* Peso aproximado: 10kg
* Faixa mínima de temperatura ambiente de funcionamento: 0 -- 40 °C

* **Ferramentas mínimas que devem estar incluídas:**
  * 1\) Estação de ar quente de no mínimo 200W, pesando no máximo 100g (sem o
    cabo de alimentação), com transferência de energia sem contato
  * 2\) Pinças de dessoldagem de no mínimo 2x40W de potência. Deve pesar no
    máximo 85g (sem o cabo de alimentação).
  * 3\) Ferramenta de solda de no mínimo 150W, pesando no máximo 35g (sem o
    cabo de alimentação). O cabo do equipamento deve ser resistente a calor
    e com propriedade antiestática. Apropriada para trabalho com componentes
    SMD
  * 4\) Ferramenta de dessolda de no mínimo 120W, pesando no máximo 250g (sem
    o cabo de alimentação).

### Dispenser automática

* Dispenser automática para aplicação de pasta de solda e adesivo cola.
  Deve permitir a inserção de componentes através de uma pinça a vácuo. A
  programação das funções deve ser realizado por meio de um LCD e teclas
  integrados, devendo o equipamento armazenar a última configuração
  ajustada. O equipamento deve permitir trabalhar simultaneamente com as
  funções de deposição de material e inserção de componentes, possuindo
  saídas independentes para aplicador e para o vácuo.
* O manômetro e o regulador de pressão devem estar integrados ao
  equipamento (pressão de trabalho máxima: 4 Bar).O equipamento deve
  possuir um fusível de proteção.
* Seleção de tensão com opção de 110 ou 220V.
* Consumo máximo de potência: 20W.
* Dimensões aproximadas: 98mm x 210mm x 250mm (AxLxP).
* Peso total aproximado: 3,2 Kg.

* **Acessórios mínimos incluídos**
  * 1x Pedal de acionamento para aplicador.
  * 1x Pedal de acionamento para vácuo.
  * 1x Suporte para seringa e caneta de vácuo.
  * 1x Adaptador para seringa de 10cc ou 30cc.
  * 1x Agulha para aplicação de pasta/adesivo.
  * 1x Caneta à vácuo.
  * 1x Ventosa 3mm.
  * 1x Ventosa 5mm.

* **Funções mínimas do equipamento**
  * Aplicador Manual com acionamento independente por pedal.
  * Aplicador Automático com disparo independente por pedal.
  * Aplicador contínuo com disparo independente por pedal.
  * Vácuo com acionamento independente por pedal.
  * Tempo do ponto programável (até 9999 ms).
  * Número de pontos programável em função automática ( até 999 pontos).
  * Intervalo entre cada ponto programável em função automática (até 9999
    ms).
  * Armazenamento na memória da última programação.

* **Materiais com os quais equipamento deve ser capaz de aplicar**
  * Pasta térmica
  * Pasta de solda estanho
  * Fluxo para solda estanho
  * Resinas
  * Colas
  * Óleos
  * Graxas
  * Silicone
  * Tintas

### Forno de refusão

* Forno de refusão (reflow) para soldagem sem chumbo (Lead-Free) de
  componentes eletrônicos SMD. Deve possuir perfis de refusão
  pré-programados e personalizados. Deve possuir um menu de navegação via
  display LCD e teclas de seta, possuindo também conectividade via USB.

* **Características**
  * \- Gaveta motorizada com dois ventiladores montados na base para
    resfriamento automático da PCB, com velocidade ajustável
  * \- Sensores de temperatura
  * \- Tamanho máximo da PCB: 230 mm x 305 mm (9\" x 12\")
  * \- Máximo pré aquecimento (temperatura/tempo): 220 °C, 999s
  * \- Máxima refusão (temperatura/tempo): 320 °C, 600s
  * \- Tratamento térmico longo (temperatura/tempo): 220 °C, 64h
  * \- Tempo de estabilização da temperatura inferior a 5 minutos
  * \- Fonte de alimentação: 230V, 50-60 Hz, monofásico, 3,2 kW
  * \- Dimensões aproximadas (L x A x P): 647 mm x 315 mm x 450 mm
  * \- Peso aproximado de 22 kg
  * \- Faixa mínima de temperatura ambiente de operação: 15-30 °C
  * \- Faixa mínima de umidade ambiente de operação: 30-80%

### **Pick and place**

* Equipamento pick and place para posicionamento de componentes
  eletrônicos, com controle principal via GUI (Graphical User Interface) .
  Deve possuir sistema de visão utilizando câmeras dual CCD, trilhos
  (rails) para posicionamento contínuo, feeder de componentes eletrônicos
  e 4 cabeças de posicionamento (placement heads).
* As cabeças de posicionamento devem possuir rot	ação de 180º e suportar a
  montagem de componentes 0201, BGA, QFN, TQFP240. Devem ser capazes de
  posicionar componentes de até 32x32mm e altura de 5mm. O equipamento
  deve poder ser usado com tiras de LED 0.8m/1.2m/1.5m. Deve ser capaz de
  trabalhar com trabalhar de até 310 x 1500mm. Velocidade de
  posicionamento (placement rate): 5000CPH (com sistema de visão ativado)
  e 10000CPH (com sistema de visão desligado).
* Tensão de alimentação: 110-220V.
* Potência: 180W.
* Peso máximo: 85 Kg.
* Tamanho máximo do equipamento: 880(L)x690(W)x490(H).
* Placement Accuracy ±0.02mm.
* X-Y Repeatability ±0.02mm.

* **Capacidade dos feeders**
  * Nº de fitas (tape feeder): 48 (mínimo de 8mm)
  * Nº de feeder por vibração (vibration feeder):5
  * Nº de feeder por bandeja (tray feeder):5

* **Acessórios mínimos incluídos**
  * 10x Nozzle (XN07=3,XN14=3,XN40=2,XN22=2)
  * 1x 8G Flash Drive
  * 1x cabo de alimentação (5M)
  * 1x Curso em video de treinamento
  * 2x Double Sided Adhesive Tape
  * 5x Allen wrench Set
  * 1x Caixa de ferramentas
  * 1x Reel holder stand
  * 1x feeders de vibração
  * 4x Rail Parts



### Impressora 3D

* Impressora 3D Ultimaker Extended 3
* Impressora 3D de alta precisão com extrusão dupla. Cabeça de impressão
  com bocal duplo (possuindo mecanismo de elevação no segundo bocal) e
  núcleos de impressão facilmente substituíveis. Temperatura do bocal deve
  atender, no mínimo, a faixa de 180 a 280 °C, com tempo de aquecimento
  inferior a 3 minutos. Mesa aquecida com capacidade de temperatura de 20C
  à 100C.
* Sistema de filamento aberto para materiais Nylon, PLA, ABS, CPE, CPE+,
  PC, TPU 95A, PP, PVA e similares. O equipamento deve suportar um
  filamento de 2.85 mm. Resolução de camada de no mínimo 20 mícrons até
  200 mícrons. Velocidade de impressão de no mínimo 16 milímetros cúbicos
  por segundo. Velocidade de movimento de até 300 mm/s. Precisão de eixos
  XY de 12.5 mícrons e no eixo Z de 2.5 mícrons.Volume de impressão para
  extrusão singular de 21,5 x 21,5 x 30 cm e para extrusão dupla de 19,7 x
  21,5 x 30 cm.
* Ruído de operação de no máximo 60 dBA. Conectividade com Wi-Fi, LAN,
  porta USB e monitoramento com câmera. Alimentação Bivolt, 100 - 240 V,
  50 - 60Hz, consumo máximo de 240W. O equipamento deve ser capaz de
  operar, no mínimo, na faixa de temperatura ambiente entre 15 e 32 Cº.



# Fabricação artesanal

-   *Home varnish tip: breu and alcohol*
-   Alcool auda a alinhas ao transferir dupla face
-   Cuidado ao transferir o outro lado da dupla face (pra não cagar a tinta do glossy)
-   impressora pra glossy tem que ser [laser]{.underline}
-   dica pra headers longos: usar broca de 1mm e alargar todos os furos com estilete
-   http://www.ricardoteix.com/circuito-impresso-photoresist/
-   https://www.youtube.com/watch?v=-KT6AhOZCEM

## Processo 1: Filme Fotoresistivo (Dry Film)

Utilizando o Dry Film para a confecção da PCI deve-se utilizar os
layouts negativos da placa, ou seja, em branco deve estar as trilhas e
pads enquanto em preto a parte que não se quer proteger.

É indicado utilizar o seguinte procedimento:

\- Imprimir o Bottom Layer Negativo Espelhado

\- Imprimir o Top Overlay Negativo

Caso a placa seja de dupla face basta imprimir também o Top Layer
Negativo, porém, passar antes do Top Overlay (Máscara).

Para imprimir os layouts negativos deve-se alterar as preferências de
cor em Page Setup -\> Advanced -\> Preferences, escolher branco para
Layer principal e Multilayer, e escolhar preto para o Mechanical 1.

Em Page Setup escolher tamanho de escala 1.00 e impressão em \'Gray\'.

Por último tem que selecionar a ferramenta Place Fill e desenhar um
quadrado em cima da placa a ser impressa, escolhendo a layer do quadrado
como Mechanical 1.

Materiais e Equipamentos Necessários:

* Película Fotoresistiva (Dry Film)
* \- Papel Transparente (Fotolito)
* \- Lâmpada de Luz Negra (ou Ultravioleta)
* \- Laminadora ou Ferro de passar
* \- Lâmina de vidro
* \- Percloreto de Ferro(FeCl3)
* \- Barrilha Leve ou Carbonato de Sódio(Na2CO3)
* \- Soda Cáustica ou Hidróxido de Sódio(NaOH)
* \- Alcool Isopropílico
* \- Recipiente de plástico
* \- Lã de Aço (Bombril
* \- Tesoura ou Estilete
* \- Fita adesiva
* \- Píncel

### Método de Confecção:

Após gerar adequadamente um fotolito com o layout negativo da sua placa
deve-se:

1° Ajustar a temperatura da laminadora para 110 °C\<p\>

2° Cortar um pedaço de dry film e retirar a primeira película protetora
(utilize pedaços de fita para desgrudar o filme)\<p\>

3° Colocar o dry film na área da placa cobreada desejada para
confecção\<p\>

4° Passar de 3 a 5 vezes a placa com o film fixado na laminadora\<p\>

5° Posicionar o fotolito em cima da placa com dry film com uma lâmina de
vidro por cima\<p\>

6° Expor a placa com fotolito à luz ultravioleta entre 8 - 15
minutos\<p\>

7° Retirar a segunda película protetora\<p\>

8° Revelação: Diluir em um recipiente com 200ml de água 1g de Barrilha
Leve (ou o suficiente para reagir)\<p\>

9° Corroer a placa em um recipiente com percloreto de ferro\<p\>

10° Remoção: Diluir em um recipiente com água meia colher de soda
cáustica.\<p\>

11° Limpar a placa e proteger com verniz\<p\>

### Precauções:

\- Deixar o dry film o mais uniforme possivel na placa de cobre

\- Expor a luz ultravioleta exatamente em cima da placa com o fotolito

\- Não deixar muito tempo na Revelação, a Barrilha pode retirar parte do
dry film

\- Cuidado ao manusear a soda cáustica. Colocar apenas o suficiente para
a Remoção

### Dicas:

\- Pode-se utilizar um pouco de água no processo de lavagem para aderir
melhor o dry film na placa

\- É recomendavél lavar e limpar a placa entre cada processo com acetato
de cloro, alcool isopropílico ou água.

## Processo 2: Tranferência de Calor

O procedimento da transferência é o comum. Basta imprimir nas
configurações padrões do Altium cuidando com a escala para 1.00 e a cor
de impressão sendo \'Black\' em Page Setup.\<p\>

É indicado utilizar o seguinte procedimento:

\- Imprimir o Bottom Layer Positivo

\- Imprimir o Top Overlay Positivo Espelhado

### Materiais e Equipamentos Necessários:

\- Papel Gloss

\- Laminadora ou Ferro de passar

\- Percloreto de Ferro(FeCl3)

\- Alcool Isopropílico

\- Lã de Aço (Bombril)

### Método de Confecção:

T = 140 ºC\<p\>

## Usando caixa de exposição UV

construção: https://www.youtube.com/watch?v=wRdey0U7HZM&feature=youtu.be

Referências de Montagem:

[[http://www.instructables.com/id/PCB-Exposure-Unit/]{.underline}](http://www.instructables.com/id/PCB-Exposure-Unit/)

[[http://www.instructables.com/id/How-to-make-a-printed-circuit-board-PCB-using-th/]{.underline}](http://www.instructables.com/id/How-to-make-a-printed-circuit-board-PCB-using-th/)

[[http://www.instructables.com/id/UV-Two-sided-Exposure-Box/]{.underline}](http://www.instructables.com/id/UV-Two-sided-Exposure-Box/)

[[http://www.instructables.com/id/UV-LED-Exposure-Box/]{.underline}](http://www.instructables.com/id/UV-LED-Exposure-Box/)

Referência de Confecção:

[[https://www.youtube.com/watch?v=tWnfnt2rNO0]{.underline}](https://www.youtube.com/watch?v=tWnfnt2rNO0)

[[https://www.youtube.com/watch?v=cRCFGZxmob0]{.underline}](https://www.youtube.com/watch?v=cRCFGZxmob0)

Produtos:

[[https://produto.mercadolivre.com.br/MLB-980875989-fita-led-uv-ultravioleta-smd5050-luz-negra-5mts-so-a-fita-\_JM]{.underline}](https://produto.mercadolivre.com.br/MLB-980875989-fita-led-uv-ultravioleta-smd5050-luz-negra-5mts-so-a-fita-_JM)

[[https://produto.mercadolivre.com.br/MLB-788490066-fita-led-5050-uv-ultravioleta-1m-luz-negra-\_JM]{.underline}](https://produto.mercadolivre.com.br/MLB-788490066-fita-led-5050-uv-ultravioleta-1m-luz-negra-_JM)

Outros:

[[https://www.youtube.com/watch?v=nx6afGUiN9o]{.underline}](https://www.youtube.com/watch?v=nx6afGUiN9o)

# Fabrication parameters

-   **IPC A610D standard**
    -   Acceptability of electronic assemblies

    -   [Class 1]{.underline}

        -   Commercial

        -   Just need to work

    -   [Class 2]{.underline}

        -   Industrial

        -   Interrupt work with long life cicle

    -   [Class 3]{.underline}

        -   Military/hospital

        -   Critical performance

        -   Can't stop/bug/fail
-   **Files needed**

    -   BOM

    -   Gerber

    -   NC drill files

    -   If possible, photos from a previously assembled board

## Specifications

-   **Cooper and track**
    -   Minimum track

    -   Minimum isolation

    -   Cooper thickness

-   **Holes/via**

    -   Minimum diameter

    -   Metalized (yes/no)

    -   Have buried via (very hard and expensive to make)

-   **Overlay**
    -   Color
    -   One or two sides

## Common parameters

* **Artesanal**
  * <u>Discrete components - 1mm</u>
    -   Hole diameter: 1.05mm

    -   Pad diameter: 1.8mm
  * <u>Discrete components - 0.8mm</u>
    -   Hole diameter: 0.9mm

    -   Pad diameter: 1.4mm

  * <u>Headers</u>

    -   Hole diameter: 1.1mm

    -   Pad diameter: 1.9mm

  * 

# Industrial fabrication

## Board

-   **Solder mask**

    -   Varnish that protect the board from oxidation and help the solder

    -   Usually green

    -   The pads are covered and don't receive the solder mask

-   **Tipos de substrato**

    -   [Fenolite]{.underline}

        -   Papel prensado impregnado com resina fenólica;

        -   Baixo custo

        -   Facilidade de usinagem

        -   Baixa resistência mecânica, térmica e à umidade

        -   Dilatação durante o processamento

        -   Propriedades dielétricas inferiores.

    -   [Fibra de vidro com epóxi;]{.underline}
        -   Manta de fibra de vidro trançada impregnada com resina epóxi;

        -   Custo mais elevado que a fenolite;

        -   Material abrasivo que prejudica a usinagem;

        -   Boa estabilidade mecânica; resistência térmica e à umidade;

        -   Propriedades dielétricas satisfatórias;

        -   Permite fabricação de placas multicamadas.

        -   Cuidado ao cortar placas de fibra de vidro: soldam uma poeira perigosa

    -   [Fibra de vidro com Teflon]{.underline}

        -   Manta de fibra de vidro trançada impregnada com Teflon®;

        -   Custo bem elevado;

        -   Material abrasivo que prejudica a usinagem;

        -   Boa estabilidade mecânica, resistência térmica e à umidade;

        -   Propriedades dielétricas excelentes em alta frequências;

        -   Baixa aderência ao cobre prejudica a soldagem[.]{.underline}

    -   [Poliéster]{.underline}

        -   Filme de poliéster utilizado para circuitos flexíveis;

        -   Baixo custo;

        -   Boa resistência mecânica, química e à umidade;

        -   Boas propriedades dielétricas;

        -   Baixa resistência térmica causa dificuldade de soldagem.

    -   [Poliimida]{.underline}

        -   Filme de poliimida utilizado para circuitos flexíveis;

        -   Custo mais elevado;

        -   Boa resistência mecânica, térmica, química e à umidade;

        -   Boas propriedades dielétricas;

        -   Soldagem facilitada.

-   **Espessura do substrato**

    -   Normalmente 1/16 polegadas

    -   Também pode ser de 1/32 ou 3/32

-   **Espessura do cobre**

    -   1 oz/ft² (onça/pé²) = 305,151727 g/m2

    -   Também pode ser de 1/2 oz/ft² ou 4 oz/ft²

### Tracks and holes

-   The surface must be cleaned (with abrasive of with chemicals)

-   The metallization of the holes is done with chemical deposition

-   Fiducial mark → help the machine to visually align the board/components (if the fiducial is a hole, it can't be
    metalized)

-   Tooling hole → to the machine turn the board? (can't be metalized)

-   **Types of process**
    -   Subtrative process

        -   Tirar o cobre pra formar o clearence (e consequentemente as trilhas)

        -   Chemical removal (with photo transferring of the circuit draw) or mechanical (Com fresa CNC)

        -   with UV:

![](./Images/3.1 - Fabrication - Industrial - Board/media/image4.png){width="3.1354166666666665in"
height="3.6979166666666665in"}

-   Depositive process

    -   Se deposita apenas o cobre necessário, sendo mais ecológico

    -   Impressão 3d de trilhas?

### Finishing

-   **Superficial finishing**
    -   Chemical process to improve the pcb
    
    -   [HASL]{.underline}
    
        -   Hot air solder leveling
    
        -   Apply some solder in the pads
    
    -   [ENIG]{.underline}
    
        -   Electroless Nickel Immersion Gold
    
        -   Application of nickel and a little (very very thin) of gold
    
    -   [Golden connector]{.underline}
    
        -   Application of nickel and gold
    
-   **Mechanical finishing**

    -   Bordas, recortes e vincos

    -   Vinco → só para montagem em painel

![](./Images/3.1 - Fabrication - Industrial - Board/media/image3.png){width="1.3645833333333333in"
height="0.90625in"}

-   Recorte → rasgo na placa

-   Picote → ponte de PCB entre recortes

![](./Images/3.1 - Fabrication - Industrial - Board/media/image2.png){width="2.9166666666666665in"
height="1.96875in"}



## PTH

### Formatação

-   How the terminal will be folded/cut
-   The component will can be "higher" or "lower"

![](./Images/3.3 - Fabrication - Industrial - PTH/media/image6.png){width="5.291666666666667in"
height="1.6979166666666667in"}

### Positioning

-   With a PTH pick and place

-   The machine bends the end of the terminal, fixing the component in the board

![](./Images/3.3 - Fabrication - Industrial - PTH/media/image3.png){width="2.0416666666666665in"
height="2.2291666666666665in"}

### Soldering

-   **Solder wave machine**

    -   Conveyor → esteira \#PT

    -   The speed is a very important parameter

    -   Can also sold SMD components

    -   Solder thief → extra (big) pad, to assure a good soldering

![](./Images/3.3 - Fabrication - Industrial - PTH/media/image2.png){width="5.65625in"
height="1.9583333333333333in"}

![](./Images/3.3 - Fabrication - Industrial - PTH/media/image4.png){width="2.9895833333333335in"
height="1.9166666666666667in"}

![](./Images/3.3 - Fabrication - Industrial - PTH/media/image5.png){width="5.260416666666667in"
height="2.3333333333333335in"}

-   **Selective solder wave**

-   **Point to point robotic solder**

## SMD

### Putting the component

-   Application of the solder past to the pad place the component
-   Sometimes a glued is also used (SMA, surface mount adhesive), to fix more the components
-   **Application of the solder past**

    -   To both fix and solder the component

    -   With a stencil or with dispenser

    -   [Stencil]{.underline}

        -   Plate with holes in the pads

        -   Usually made of inox steel

        -   The past can be applied in a manual or automatic printer

![](Images - 2.0 - Processos de fabricação/image3.png){width="2.2708333333333335in"
height="1.2291666666666667in"}

-   [Dispenser]{.underline}

    -   Apply the past exactly in the pad

    -   Very precise

    -   Don't need the stencil (that is very good)

    -   There are a few manual dispensers in the market, but is unusual

![](Images - 2.0 - Processos de fabricação/image2.png){width="1.6145833333333333in"
height="1.8958333333333333in"}

-   **Placement**

    -   Use a Pick and Place machine

    -   Feeders → where the component comes

    -   The machine takes the component with a vacuum tip (nozzler)

    -   Can have a vision system to improve the placement and check errors

    -   Can have more than one head, but the total time is poorly improved after the 4º head

    -   Type of feeder and nozzle to the different packages:

![](Images - 2.0 - Processos de fabricação/image6.png){width="4.125in"
height="3.9375in"}

![](Images - 2.0 - Processos de fabricação/image5.png){width="6.177083333333333in"
height="3.6145833333333335in"}

### Soldering

-   Soldering profile: setup of the temperature over time

-   **Reflow oven**

    -   (+) Cheap

    -   (-) all the board will be heated at the same temperature

![](Images - 2.0 - Processos de fabricação/image4.png){width="4.572916666666667in"
height="2.8333333333333335in"}

-   **InfraRed reflow**

    -   (+) high productivity

    -   (+) each zone of the PCB can have a different profile

    -   (-) The profile is hard to determine, because each component will absorb the heat in a different way (mainly because of the color).

    -   (-) Some parts of the PCB can't be reached

    -   So that soldering technic is usually just used in low complexity PCBs

![](Images - 2.0 - Processos de fabricação/image7.png){width="2.9479166666666665in"
height="1.9895833333333333in"}

-   **Forced convection of air**

    -   The best way today

    -   A hot air is ventilated to the boar

    -   (+) The heating is uniform

    -   (+) No shadow area

    -   (+) Different profiles for each part of the PCB

    -   (+) The soldering is better and more uniform, even in complex boards

-   **Vapour phase soldering**

![](Images - 2.0 - Processos de fabricação/image8.png){width="5.53125in"
height="2.75in"}