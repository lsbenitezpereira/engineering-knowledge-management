% Exemplo de an�lise de controlabilidade e observabilidade

A=[1 1;-2 -1];
B=[0;1];
C=[1 0];
D=0;

% Matriz de Controlabilidade
Mc=[B A*B]
% ou
ctrb(A,B)
% Posto da Matriz de Controlabilidade
rank(Mc)

% % Matriz de Controlabilidade da Sa�da
Mco=[C*B C*A*B]
rank(Mco)

% Matriz de Observabilidade
Mo=[C' A'*C']
% ou
obsv(A,C)
% Posto da Matriz de Observabilidade
rank(Mo)