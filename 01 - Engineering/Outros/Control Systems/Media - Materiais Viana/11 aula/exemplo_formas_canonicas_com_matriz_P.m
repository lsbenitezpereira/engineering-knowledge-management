% Exemplo de diagonalização com matriz de transformação
% primeiro exercício do slide 16

A=[-3 1;1 -3];
B=[1;2];
C=[2 3];
D=0;
% conversão para função de transferência
[NUM,DEN]=ss2tf(A,B,C,D)

a2=DEN(3);
a1=DEN(2);
b2=NUM(3);
b1=NUM(2);

% representação na forma canonica controlável
A2=[0 1;-a2 -a1];
B2=[0;1];
C2=[b2 b1];

% construção da matriz P a partir dos autovalores
autovalores=eig(A2);
lambda1=autovalores(1);
lambda2=autovalores(2);
P=[1 1;lambda1 lambda2];
Pinv=inv(P);
A3=Pinv*A2*P
B3=Pinv*B2
C3=C2*P

% quando a matriz não está na forma controlável
% a matriz P pode ser definida a partir dos autovetores

[autovetores, autovalores]=eig(A);
P=autovetores;
Pinv=inv(P);
A4=Pinv*A*P
B4=Pinv*B
C4=C*P

% conferindo os autovalores
eig(A)
eig(A2)
eig(A3)
eig(A4)

% conferindo as funções de transferência
[n,d]=ss2tf(A,B,C,0)
[n,d]=ss2tf(A2,B2,C2,0)
[n,d]=ss2tf(A3,B3,C3,0)
[n,d]=ss2tf(A4,B4,C4,0)


