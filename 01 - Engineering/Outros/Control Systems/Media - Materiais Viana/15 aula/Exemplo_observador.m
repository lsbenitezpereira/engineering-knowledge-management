% Exemplo de Projeto de Observador de Ordem Plena
% Slide 10 da apresentação EE10 - Observadores de Estado
clc
clear
format long

% Matrizes do Sistema
A=[0 20.6;1 0];
B=[0;1];
C=[0 1];

% projeto do observador de estados
% autovalores desejados
u1=-1.8+2.4i;
u2=-1.8-2.4i;

% polinomio característico desejado
Pd=conv([1 -u1],[1 -u2]);
alfa1=Pd(2);
alfa2=Pd(3);

% polinomio característico original
P=poly(A);
a1=P(2);
a2=P(3);

% Matriz de observabilidade
N=[conj(C') conj(A')*conj(C')];
rank(N)

W=[a1 1;1 0];

% Matriz de Transformação

Q=inv(W*conj(N'));

% Matriz de Ganho do Observador

Ke=Q*[alfa2-a2;alfa1-a1]

Ke1=Ke(1,1);
Ke2=Ke(2,1);

% OU 
% projeto do observador de estados com comando acker
% Utilizando o problema dual
Ke=acker(A',C',[u1 u2])'






