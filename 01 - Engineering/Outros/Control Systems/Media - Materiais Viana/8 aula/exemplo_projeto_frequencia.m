% Exemplo projeto frequencia
% Exemplo 4.12
% K. Ogata, Discrete Time Control Systems

clear
format long
clf

G=tf(1,[1 1 0]);

% valors desejados
mf=50;  % graus
mg=10;  %dB
Kv=2;

T=0.2;
Gz=c2d(G,T);
% Aplicando a transforma��o bilinear inversa
Gw=d2c(Gz,'tusin');

% Definindo o ganho a partir da especifica��o de erro
[nw,dw]=tfdata(minreal(tf([1 0],1)*Gw),'v');
K=Kv/(polyval(nw,0)/polyval(dw,0))

% Diagrama  de Bode da planta com Ganho
figure(1)
bode(K*Gw)
[Gm,Pm,Wcg,Wcp] = margin(K*Gw);

% Incremento do angulo de fase
fi_m=mf-Pm;
fi_m=28     % com a inclus�o do controlador em avan�o acrecenta se uma folga
% se n�o for suficiente, pode se aumentar este valor a� que a especifica��o
% seja atendida



% controlador em avan�o
% Gd(w)=(1+tau*w)/(1+alfa*tau*w)
alfa=(1-sind(fi_m))/(1+sind(fi_m));
delta_ganho=-20*log10(1/sqrt(alfa));

%verificando o ponto na curva de ganho onde ocorre este valor
upsilon=1.7  % (1.72)
tau=1/(upsilon*sqrt(alfa))

Gd=tf([tau 1],[alfa*tau 1])

% Diagrama  de Bode da planta com controlador
figure(2)
bode(K*Gd*Gw)
[Gm,Pm,Wcg,Wcp] = margin(K*Gd*Gw)
% margem de ganho em dB
20*log10(Gm)

% Convertendo o controlador em avan�o
Gdz=c2d(Gd,T,'tustin')

% Resposta ao degrau
FTMA=Gdz*K*Gz
FTMF=feedback(FTMA,1)
figure(3)
step(FTMF)

