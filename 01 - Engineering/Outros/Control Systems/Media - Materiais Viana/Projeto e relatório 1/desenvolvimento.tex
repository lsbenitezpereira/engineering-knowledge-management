\chapter{Desenvolvimento}
% ---
\section{Revisão de literatura}
% ---

Uma vez que os projetos de controladores discretos requerem o conhecimento de sinais e sistemas contínuos, discretos e dos seus métodos de transformação entre domínios, uma breve contextualização dos conceitos relevantes à eles é disposta.

% ---
\subsection{Sistema de segunda ordem}
% ---

Toma-se conhecimento que os sistemas analógicos lineares podem ser representados por funções de transferência no domínio da frequência. Como demonstrado por \citeonline{villaca2007}, os sistemas de segunda ordem (\autoref{eq_1}) são modelados em função dos seguintes parâmetros: frequência natural e fator de amortecimento, respectivamente, $\omega_{n}$ e $\zeta$.

\begin{equation}\label{eq_1}
	G(s) = \frac{\omega_{n}^{2}}{s^{2} + 2 \cdot \zeta \cdot \omega_{n} \cdot s + \omega_{n}^{2}}
\end{equation}

Ainda segundo \citeonline{villaca2007}, a partir da \autoref{eq_1} obtém-se algumas outras expressões: a do sobressinal (\autoref{eq_2}), do instante de pico (\autoref{eq_3}) e do tempo de acomodação à 5\% do valor de regime permanente (\autoref{eq_4}):

\begin{equation}\label{eq_2}
	M_{p} = e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}}
\end{equation}

\begin{equation}\label{eq_3}
	t_{p} = \frac{\pi}{\omega_{n} \cdot \sqrt{1-\zeta^{2}}}
\end{equation}

\begin{equation}\label{eq_4}
	t_{s5\%} = \frac{3}{\omega_{n} \cdot \zeta}
\end{equation}

Por meio dessas equações é possível determinar e modificar o comportamento do sistema para obter os parâmetros desejados. Porém na ocasião em que não se tenha a possibilidade de mudar esses parâmetros, deve ser projetado um controlador para que os requisitos do sistema sejam atingidos.

% ---
\subsection{Controlador digital}
% ---

Visto que há sistemas em que não se pode modificar a planta diretamente, uma opção para manipulação de sua resposta é o controlador digital. Um exemplo de projeto desse controlador utiliza a seguinte estrutura:

\begin{figure}[H]\centering 
\caption{\label{fig_1}Diagrama de blocos.}
    \includegraphics[width=1\textwidth]{sistema1.pdf}
\legend{Fonte: \citeonline{flabio2020}.}
\end{figure}

Na \autoref{fig_1} o bloco C é a função de transferência do controlador digital, que pode ser implementado diretamente ou pela conversão de um controlador analógico. Para o primeiro caso, a função de transferência da planta, do bloco G, deve ser levantada e o controlador deve ser escolhido de forma apropriada, no que a escolha do período de amostragem também é crucial para o controle da sistema.

No início do projeto, a planta é amostrada por um dispositivo \textit{Sample and Hold} (S/H) que é um amostrador ideal em cascata com um bloco \textit{Zero Order Hold} (ZOH) de acordo com \citeonline{trofino20??}, em que a sua função de transferência é dada pela \autoref{eq_5}:

\begin{equation}\label{eq_5}
	G(z) = \left( 1-z^{-1} \right) \cdot \mathcal{Z} \left[ \frac{G(s)}{s} \right]
\end{equation}

Dessa maneira a função de transferência de $C(z)$ pode ser modelada por um controlador de avanço de fase, por exemplo:

\begin{equation}\label{eq_6}
	C(z) = K_{c} \cdot \frac{(z+\alpha)}{(z+\beta)}
\end{equation}

E a função de transferência de malha fechada é dada por:

\begin{equation}\label{eq_7}
	\frac{Y(z)}{R(z)} = \frac{C(z) \cdot G(z)}{1+C(z) \cdot G(z)}
\end{equation}

Sabe-se que a equação caraterística do sistema é dada pelo denominador da função de transferência de malha fechada e isso expõe um método para o cálculo dos coeficiente do controlador. A \autoref{eq_8} permite calcular a contribuição da fase para os polos de malha fechada e a \autoref{eq_9} possibilita o cálculo do valor do ganho do controlador:

\begin{align}\label{eq_8}
	1 + C(z) \cdot G(z) &= 0 \nonumber \\
	C(z) \cdot G(z) &= -1 \nonumber \\
	C(z) \cdot G(z) &= e^{\pm j \cdot \pi} \nonumber \\
	\angle C(z) + \angle G(z) &= \pm \ang{180}
\end{align}

\begin{align}\label{eq_9}
	C(z) \cdot G(z) &= -1 \nonumber \\
	\left| C(z) \cdot G(z) \right| &= |-1| \nonumber \\
	 K_{c} \cdot \left| \frac{(z+\alpha)}{(z+\beta)} \cdot G(z) \right| &= 1 
\end{align}

Com esses métodos os polos de malha fechada podem ser escolhidos previamente no plano contínuo e levados ao plano discreto, por meio da \autoref{eq_10} \cite{ogata1987}. E a partir dessa relação o módulo e fase dos polos de malha fechada no plano discreto são computados, podendo estabelecer seu valor de acordo com os critérios do projeto.

\begin{align}\label{eq_10}
	s &= -\zeta \cdot \omega_{n} + j \cdot \omega_{n} \cdot \sqrt{1-\zeta^{2}} \nonumber \\
	z &= e^{T \cdot s} \\
	z &= e^{T \cdot \left( -\zeta \cdot \omega_{n} + j \cdot \omega_{n} \cdot \sqrt{1-\zeta^{2}} \right)} \nonumber \\
	|z| &= e^{-T \cdot \zeta \cdot \omega_{n}} & \angle z &= T \cdot \omega_{n} \cdot \sqrt{1-\zeta^{2}}
\end{align}

Ademais, o período de amostragem deve ser escolhido de acordo com o sistema. Caso queira ter o mesmo comportamento da resposta em frequência, a frequência de amostragem deve ser 20 vezes maior do que a frequência de sua banda passante, se a resposta transitória for subamortecida deve-se amostrar de 8 a 10 vezes por ciclo das oscilações senoidais amortecidas da saída do sistema de malha fechada, e, se for sobre amortecido, a frequência de amostragem deve ser de 8 a 10 vezes o tempo de subida da saída para uma entrada degrau, ou ainda, o período de amostragem deve ser 10 a 15 vezes menor que o tempo de resposta desejado.

Já para o controlador digital feito pela conversão do controlador analógico, é utilizado um tempo de amostragem de, pelo menos, 30 vezes menos do que a frequência da largura de banda superior segundo \citeonline{franklin2013}.

% ---
\section{Projeto do controlador digital}
% ---

% ---
\subsection{Função de transferência de um sistema contínuo}
% ---

Dado o circuito da \autoref{fig_2} e sua resposta ao degrau unitário nas \autoref{fig_3}, \autoref{fig_4} e \autoref{fig_5}, o fator de amortecimento e a frequência natural do sistema foram obtidas a partir da \autoref{eq_2} e da \autoref{eq_3}:

\begin{figure}[H]\centering 
\caption{\label{fig_2}Planta analógica.}
    \includegraphics[width=1\textwidth]{circuito1.pdf}
\legend{Fonte: \citeonline{flabio2020}.}
\end{figure}

\begin{figure}[H]\centering 
\caption{\label{fig_3}Valor experimental do tempo de pico.}
    \includegraphics[width=0.5\textwidth]{tempo-pico.png}
\legend{Fonte: \citeonline{flabio2020}.}
\end{figure}

\begin{figure}[H]\centering 
\caption{\label{fig_4}Valor experimental do sobressinal.}
    \includegraphics[width=0.5\textwidth]{sobressinal1.png}
\legend{Fonte: \citeonline{flabio2020}.}
\end{figure}

\begin{figure}[H]\centering 
\caption{\label{fig_5}Valor experimental do valor de regime permanente.}
    \includegraphics[width=0.5\textwidth]{sobressinal2.png}
\legend{Fonte: \citeonline{flabio2020}.}
\end{figure}

Pela \autoref{fig_4} e \autoref{fig_5}, o valor do sobressinal é:

\begin{align}\label{eq_11}
	M_{p} &= \frac{\Delta 1}{\Delta 2} \\
	M_{p} &= e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}} \nonumber \\
 	\frac{\Delta 1}{\Delta 2} &= e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}} \nonumber
\end{align}

Como $\Delta 1$ é igual a $\SI{144}{\milli\V}$ e $\Delta 2$ é igual a $\SI{496}{\milli\V}$:

\begin{align*}
 	\frac{\SI{144}{\milli\V}}{\SI{496}{\milli\V}} &= e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}} \\
 	0,290323 &= e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}} \\
 	\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}} &= \ln{0,290323} \\
 	\frac{\zeta}{\sqrt{1-\zeta^{2}}} &= -\frac{\ln{0,290323}}{\pi} \\
 	\left( \frac{\zeta}{\sqrt{1-\zeta^{2}}} \right)^{2} &= \left( -\frac{\ln{0,290323}}{\pi} \right)^{2} \\
 	\frac{\zeta^{2}}{1-\zeta^{2}} &= 0,1549790 \\
 	\frac{\zeta^{2}}{1-\zeta^{2}} \cdot \frac{\zeta^{-2}}{\zeta^{-2}} &= 0,1549790 \\
 	\left( \frac{1}{\zeta^{-2}-1} \right)^{-1} &= (0,1549790)^{-1} \\
 	 \zeta^{-2}-1 &= \frac{1}{0,1549790} \\
 	\zeta^{-2} &= \frac{1}{0,1549790}+1 \\
 	\zeta &= \frac{1}{\sqrt{\frac{1}{0,1549790}+1}} \approx 0,3663106
\end{align*}

E com o instante de pico de $19,20 \, \si{\milli\second}$, o valor da frequência natural é:

\begin{align*}
	t_{p} &= \frac{\pi}{\omega_{n} \cdot \sqrt{1-\zeta^{2}}} \\
	0,0192 &=  \frac{\pi}{\omega_{n} \cdot \sqrt{1-\zeta^{2}}} \\
	\omega_{n} &= \frac{\pi}{0,0192 \cdot \sqrt{1-\zeta^{2}}} \\
	\omega_{n} &\approx 175,8472962 \, \si{\radian\per\second} 
\end{align*}

Assim, por meio da \autoref{eq_1}, a função de transferência da planta analógica é:

\begin{align*}
	G(s) &= \frac{\omega_{n}^{2}}{s^{2} + 2 \cdot \zeta \cdot \omega_{n} \cdot s + \omega_{n}^{2}} \\
	G(s) &= \frac{30922,2715847}{s^{2}+128,829440328 \cdot s+30922,2715847}
\end{align*}

E seu tempo de acomodação é:

\begin{align}
	t_{s5\%} &= \frac{3}{\omega_{n} \cdot \zeta} \nonumber \\
	t_{s5\%} &= 46,5732055 \, \si{\milli\second} \nonumber
\end{align}

% ---
\subsection{Projeto de controlador digital no plano discreto pelo método do lugar das raízes}
% ---

Em seguida o controlador digital necessita das especificações mínimas da resposta ao degrau do sistema controlado para o seu projeto, elas são: degrau de referência de $1,0 \, \si{\V}$ a $1,5 \, \si{\V}$, tempo de acomodação à $5 \, \%$ igual a $19 \, \si{\milli\second}$, erro nulo em regime permanente para resposta ao degrau, valor de sobressinal percentual igual a $14 \, \%$ e estabilidade.

Analisando o valores limites para a frequência natural e o fator de amortecimento:

\begin{align*}
 	\zeta &\geq \frac{1}{\sqrt{\left( \frac{\pi}{\ln{M_{p}}} \right)^{2}+1}} \\
 	\zeta &\geq 0,5305067
\end{align*}

O valor de $0,6$ foi escolhido para o fator de amortecimento para obter uma pequena margem, e o valor limite da frequência natural é:

\begin{align*}
 	\omega_{n} &\geq \frac{3}{t_{s5\%} \cdot \zeta} \\
 	\omega_{n} &\geq 263,1578947 \, \si{\radian\per\second}
\end{align*}

E o valor de $250 \cdot \pi \, \si{\radian\per\second}$ foi escolhido para a frequência natural. Desse modo as especificações de sobressinal e de tempo de acomodação do sistema controlado devem ser:

\begin{align*}
	M_{p} &= e^{\frac{-\pi \cdot \zeta}{\sqrt{1-\zeta^{2}}}} \\
	M_{p} &= 9,4780225 \, \% \\
	t_{s5\%} &= \frac{3}{\zeta \cdot \omega_{n}} \\
	t_{s5\%} &= 6,3661977 \, \si{\milli\second}
\end{align*}

Dessarte a sua frequência de amostragem deve ser de 8 a 10 vezes a sua frequência amortecida, já que a planta é subamortecida. Portanto foi escolhido o valor de 10 vezes, isso proporciona um período de amostragem igual a:

\begin{align*}
	\omega_{d} &= \omega_{n} \cdot \sqrt{1-\zeta^{2}} = 200 \cdot \pi \, \si{\radian\per\second} \\
	\frac{\omega_{a}}{\omega_{d}} &= 10 \\
	\omega_{a} &= 2000 \cdot \pi \, \si{\radian\per\second} \\
	T_{a} &= \frac{2 \cdot \pi}{\omega_{a}} = 1 \, \si{\milli\second}
\end{align*}

Após terem sido calculadas a frequência natural, o fator de amortecimento e o período de amostragem, pode-se dar continuidade ao projeto do controlador. Por conseguinte foi escolhido um controlador que faz o cancelamento dos polos da planta, onde se precisa amostrar a planta por meio do S/H para obter seus polos:

\begin{align}\label{eq_13}
	G(z) &= \left( 1-z^{-1} \right) \cdot \mathcal{Z} \left[ \frac{G(s)}{s} \right] \nonumber \\
	G(z) &= \left( 1-z^{-1} \right) \cdot \mathcal{Z} \left[ \frac{30922,2715847}{s \cdot \left( s^{2}+128,829440328 \cdot s+30922,2715847 \right)} \right] \nonumber \\
	G(z) &= \left( \frac{z-1}{z} \right) \cdot \left( R(P_{1}) + R(P_{2}) + R(P_{3}) \right) \nonumber \\
	G(z) &= \left( z-1 \right) \cdot \left( \frac{1}{z-1} + \frac{-z + 0,8649655}{z^{2} - 1,8501853 \cdot z + 0,8791239} \right) \nonumber \\
	G(z) &= \left( 1 + \frac{\left( z-1 \right) \cdot \left( -z + 0,8649655 \right) }{z^{2} - 1,8501853 \cdot z + 0,8791239} \right) \nonumber \\
	G(z) &= \frac{0,01478022 \cdot \left( z + 0,9579285 \right)}{z^{2} - 1,8501853 \cdot z + 0,8791239}
\end{align}

Com a função de transferência da planta discretizada, os coeficientes do controlador devem ser obtidos, em que a $C(z)$ deve ter um dos polos igual a $(z-1)$ para garantir que o erro seja nulo. Porém devem ser calculados para quando o polo de malha fechada tenha as especificações desejadas, usando as \autoref{eq_8}, \autoref{eq_9} e \autoref{eq_11}: 

 \begin{align}
	|z| &= e^{-T \cdot \zeta \cdot \omega_{n}} = 0,6242284 \nonumber \\
	\angle z &= T \cdot \omega_{n} \cdot \sqrt{1-\zeta^{2}} = 0,6283185 \, \si{\radian\per\second} \nonumber \\
	z_{1} &= 0,5050114 + j \cdot 0,3669123 \nonumber \\
	C(z) &= K_{c} \cdot \frac{z^{2} - 1,8501853 \cdot z + 0,8791239}{(z+\beta) \cdot (z-1)} \nonumber \\
	G_{1}(z) &= \frac{K_{c}}{z+\beta} \nonumber \\
	G_{2}(z) &= \frac{0,01478022 \cdot \left( z + 0,9579285 \right)}{z-1} \nonumber \\
	\angle G_{1}(z_{1}) + \angle G_{2}(z_{1}) &= - \ang{180} \nonumber \\
	\angle G_{1}(z_{1}) - 129,3725294^{\circ} &= - \ang{180} \nonumber \\
	\angle G_{1}(z_{1}) &= - 50,6274706^{\circ} \nonumber
\end{align}

Com isso foi obtida a condição de ângulo, em que por análise trigonométrica, o valor de $\beta$ é:

\begin{equation}\label{eq_14}
	\beta = \frac{IM(z_{1}-RE(z_{1}) \cdot tg(-\angle G_{1}(z_{1}))}{tg(-\angle G_{1}(z_{1}))} \approx -0,2039208
\end{equation}

E o valor de $K_{c}$ é:

\begin{align*}
	 K_{c} &\cdot \left. \left| \frac{z^{2} - 1,8501853 \cdot z + 0,8791239}{(z+\beta) \cdot (z-1)} \cdot G(z) \right| \right|_{z = z_{1}} = 1  \\
	 K_{c} &\approx 13,1187485
\end{align*}

Assim, a função de transferência do controlador é:

\begin{equation}\label{eq_15}
	C(z) = 13,1187485 \cdot \frac{z^{2} - 1,8501853 \cdot z + 0,8791239}{(z-0,2039208) \cdot (z-1)}
\end{equation}

Depois de descoberta função de transferência do controlador, a função de transferência de malha fechada é obtida:

\begin{align}\label{eq_16}
	\frac{Y(z)}{R(z)} &= \frac{C(z) \cdot G(z)}{1+C(z) \cdot G(z)} \nonumber \\
	\frac{Y(z)}{R(z)} &= \frac{0,1939 \cdot z + 0,1857}{z^{2} - 1,01 \cdot z + 0,3897}
\end{align}

Onde seus polos são iguais a $0,5050114 \pm j \cdot 0,3669123$. E a sua resposta ao degrau na \autoref{fig_6} e seus parâmetros na \autoref{tab1} mostram a sua correspondência à especificação comparada a resposta do sistema original:

\begin{figure}[htb]\centering 
\caption{\label{fig_6}Resposta ao degrau do sistema controlado pelo controlador discreto.}
    \includegraphics[width=0.8\textwidth]{resposta-degrau1.png}
\legend{Fonte: o autor.}
\end{figure}

\begin{table}[htb]
\IBGEtab{%
  \caption{Comparação de parâmetros do sistema original e controlado.} \label{tab1}
}{%
  \begin{tabular}{cccc}
  \toprule
   \textbf{Sistema} & $\symbf{M_{p} (\%)}$ & $\symbf{t_{s5\%} (\si{\milli\second})}$ & $\symbf{e_{ss}}$ \\
  \midrule \midrule
   Original & 28,9414143 & 44,5409082 & - \\
  \midrule
   Especificação & 14 & 19 & 0 \\
  \midrule
   Controlado & 9,4780225 & 7 & 0 \\
  \bottomrule
\end{tabular}%
}{%
  \fonte{o autor.}%
  }
\end{table}

E sua estabilidade pode ser vista na \autoref{fig_7}, onde os seus polos de malha fechada estão destacados por estrelas vermelhas. Como a região de convergência da função de transferência de malha fechada contém o circulo unitário, o sistema é estável.

\begin{figure}[H]\centering 
\caption{\label{fig_7}Lugar das raízes do sistema controlado pelo controlador discreto.}
    \includegraphics[width=0.8\textwidth]{root-locus1.png}
\legend{Fonte: o autor.}
\end{figure}

% ---
\subsection{Projeto de controlador digital pela conversão de um controlador analógico}
% ---

Similar ao projeto anterior, o controlador analógico também pode ser realizado o cancelamento de polos e zeros da planta:

\begin{align*}
	C(s) &= K_{c} \cdot \frac{s^{2}+128,829440328 \cdot s+30922,2715847}{(s+\beta) \cdot s} \\
	s_{1} &= -\zeta \cdot \omega_{n} + j \cdot \omega_{n} \cdot \sqrt{1-\zeta^{2}} \\
	s_{1} &= -471,2388980 + j \cdot 628,3185307 \\
	G_{1}(s) &= \frac{K_{c}}{s+\beta} \nonumber \\
	G_{2}(s) &= \frac{30922,2715847}{s}  \\
	\angle G_{1}(s_{1}) + \angle G_{2}(s_{1}) &= - \ang{180} \\
	\angle G_{1}(s_{1}) - 126,8698976^{\circ} &= - \ang{180} \\
	\angle G_{1}(s_{1}) &= - 53,1301024^{\circ}
\end{align*}

Novamente foi obtida a condição de ângulo, em que por análise trigonométrica, o valor de $\beta$ é:

\begin{equation}
	\beta = \frac{IM(s_{1}-RE(s_{1}) \cdot tg(-\angle G_{1}(s_{1}))}{tg(-\angle G_{1}(s_{1}))} \approx 942,4777961
\end{equation}

E o valor de $K_{c}$ é:

\begin{align*}
	 K_{c} &\cdot \left. \left| \frac{s^{2}+128,829440328 \cdot s+30922,2715847}{(s+\beta) \cdot (s)} \cdot G(s) \right| \right|_{s = s_{1}} = 1  \\
	 K_{c} &\approx 19,9484139895
\end{align*}

Assim, o controlador analógico é:

\begin{equation}\label{eq_17}
	C(s) = 19,9484139895 \cdot \frac{s^{2}+128,829440328 \cdot s+30922,2715847}{(s+942,4777961) \cdot (s)}
\end{equation}

Em que amostrando por S/H com um período de amostragem 4 vezes menor que o original ($250 \, \si{\micro\second}$) resulta em:

\begin{equation}\label{eq_18}
	C(z) = \frac{19,95 \cdot z^{2} - 39,31 \cdot z + 19,39}{z^{2} - 1,79 \cdot z + 0,7901}
\end{equation}

E a função de transferência da planta nesse período de amostragem é:

\begin{equation}\label{eq_18}
	G(z) = \frac{0,0009559 \cdot z + 0,0009457}{z^{2} - 1,966 \cdot z + 0,9683}
\end{equation}

Com isso a função de transferência de malha fechada é:

\begin{equation}\label{eq_19}
	\frac{Y(z)}{R(z)} = \frac{0,01907 \cdot z^{3} - 0,01871 \cdot z^{2} - 0,01863 \cdot z + 0,01834}{z^{4} - 3,737 \cdot z^{3} + 5,26 \cdot z^{2} - 3,306 \cdot z + 0,7834}
\end{equation}

A sua resposta ao degrau na \autoref{fig_8} e seus parâmetros na \autoref{tab2} mostram o sistema controlado comparado à especificação e à resposta do sistema original:

\begin{figure}[htb]\centering 
\caption{\label{fig_8}Resposta ao degrau do sistema controlado pelo controlador analógico convertido para o domínio discreto.}
    \includegraphics[width=0.8\textwidth]{resposta-degrau2.png}
\legend{Fonte: o autor.}
\end{figure}

\begin{table}[htb]
\IBGEtab{%
  \caption{Comparação de parâmetros do sistema original e controlado pelo controlador analógico convertido para o domínio discreto.} \label{tab2}
}{%
  \begin{tabular}{cccc}
  \toprule
   \textbf{Sistema} & $\symbf{M_{p} (\%)}$ & $\symbf{t_{s5\%} (\si{\milli\second})}$ & $\symbf{e_{ss}}$ \\
  \midrule \midrule
   Original & 28,9414143 & 44,5409082 & - \\
  \midrule
   Especificação & 14 & 19 & 0 \\
  \midrule
   Controlado & 12,2045792 & 6 & 0 \\
  \bottomrule
\end{tabular}%
}{%
  \fonte{o autor.}%
  }
\end{table}

E sua estabilidade pode ser vista na \autoref{fig_9}, onde os seus polos de malha fechada estão destacados por estrelas vermelhas. Como a região de convergência da função de transferência de malha fechada contém o circulo unitário, o sistema é estável.

\begin{figure}[H]\centering 
\caption{\label{fig_9}Lugar das raízes do sistema controlado pelo controlador analógico convertido para o domínio discreto.}
    \includegraphics[width=0.8\textwidth]{root-locus2.png}
\legend{Fonte: o autor.}
\end{figure}

% ---
\subsection{Implementação do controlador digital}
% ---

Para o controlador implementado diretamente no domínio discreto, a taxa de amostragem deve ser de $1 \, \si{\milli\second}$, como descrito no fluxograma da \autoref{fig_11}. E como a entrada é um degrau que varia entre $1 \, \si{\V}$ até $1,5 \, \si{\V}$, a amplitude máxima da ação de controle é de $-4,61696 \, \si{\V}$ até $19,67812 \, \si{\V}$ como pode ser observado na \autoref{fig_10}.

\begin{figure}[H]\centering 
\caption{\label{fig_10}Ação de controle para entrada de $1,5 \, \si{\V}$ para o controlador discreto.}
    \includegraphics[width=0.8\textwidth]{recursiva-acao2.png}
\legend{Fonte: o autor.}
\end{figure}

\begin{figure}[H]\centering 
\caption{\label{fig_11}Fluxograma do controlador discreto.}
    \includegraphics[width=1\textwidth]{fluxograma1.png}
\legend{Fonte: o autor.}
\end{figure}

Já o controlador analógico convertido para o domínio discreto é implementado de acordo com o seguinte fluxograma:

\begin{figure}[H]\centering 
\caption{\label{fig_12}Fluxograma do controlador analógico convertido para o domínio discreto.}
    \includegraphics[width=1\textwidth]{fluxograma2.png}
\legend{Fonte: o autor.}
\end{figure}

A ação de controle desse controlador tem uma maior amplitude para ser aplicada. Com um sinal de entrada igual ao degrau que varia entre $1 \, \si{\V}$ até $1,5 \, \si{\V}$, a amplitude máxima da ação de controle é de $-6,56341 \, \si{\V}$ até $29,92262 \, \si{\V}$ como pode ser observado na \autoref{fig_13}.

\begin{figure}[H]\centering 
\caption{\label{fig_13}Ação de controle para entrada de $1,5 \, \si{\V}$ para o controlador analógico convertido para o domínio discreto.}
    \includegraphics[width=0.8\textwidth]{recursiva-acao4.png}
\legend{Fonte: o autor.}
\end{figure}