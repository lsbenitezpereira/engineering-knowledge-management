FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install --no-install-recommends -y -q \
        apt-utils \
        build-essential \
        ca-certificates \
        curl \
        git \
        python3 \
        python3-dev \
        python3-pip \
        python3-setuptools \
        wget

RUN apt-get update --fix-missing
RUN pip3 install --upgrade pip

RUN pip3 install control numpy pandas scipy matplotlib sympy lcapy==0.96 networkx==2.5 array-to-latex

RUN mkdir src
WORKDIR src/

# Add Jupyter notebook. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
RUN pip3 install jupyter
ADD https://github.com/krallin/tini/releases/download/v0.6.0/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
EXPOSE 8888
ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
