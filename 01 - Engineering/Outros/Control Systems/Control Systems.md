# Conceituação

*  Interconexão de componentes físicos que tem como
  finalidade controlar (regular/dirigir/ comandar) um
  determinado processo para fornecer uma resposta
  desejada.
*  “Control is applied optimization” Brunton
*  **History**
  *  The first automatic feedback controller used in an industrial process is gener-
    ally agreed to be James Watt’s flyball governor, developed in 1769 for controlling
    the speed of a steam engine 
*  **Landscape**
  *  ISA: international society of automation
*  **Sobre projetos em controle**
   *  o que sai mais caro: acionadores e drivers, ampops bons, profissional-hora

## Terminologia

*  **Processo**: Qualquer operação a ser controlada (Ex.: A fusão nuclear)
*  **Planta**: Qualquer dispositivo ou complexo físico a ser controlado (ex: reator)
*  **Sistema**: Equipamento ou fenômeno fı́sico.
* **Perturbações**: sinal de entrada que tende a afetar desfavoravelmente o valor da saída do sistema.
  * Interna: gerada dentro do sistema (realimentação positiva);
  * Externa: gerada fora do sistema (entrada adicional).
*  **Referência**: valor desejado da variável a ser controlada.
*  **Comparador**: elemento que gera o sinal de erro como a diferença entre a
   referência e o valor atual.
*  **Controlador**: elemento que manipula o sinal de erro e gera o sinal de
   controle de forma a corrigir o valor da variável controlada.
*  **Atuador**: dispositivo de potência que recebe o sinal de controle e produz a
   entrada para o processo/planta.
*  **Transdutor**: elemento responsável pela medição e conversão da variável
   controlada em uma grandeza adequada à comparação com a referência.
*  Usualmente representamos a função de saída por $C(s)$

## Tipos de controle

* **Malha aberta**

  * A ação de controle é independente da saída; na inexistencia de perturbações, costumam ser muito estáveis

  ![image-20201111145357945](Images - Control Systems/image-20201111145357945.png)

* **Sistema de Controle em Malha Fechada (MF)**
  
  * A variável de saída do processo a controlar tem efeito direto na ação
      de controle.
  * :keyboard: matlab: `<a>/.<b>` performs FTMF de a realimentado com b
  
  ![image-20201111145452001](Images - Control Systems/image-20201111145452001.png)

* **Controle adaptativo**: detecta variações na panta

  ![image-20201112090101514](Images - Control Systems/image-20201112090101514.png)

## Representações
* **EDO**

  * Ver em seção específica

* 

* 

* É comum escolhermos como variáveis de estados (ou varíavel principal, whatever, mas é a variável que vamos manter na modelagem até o final, não vamos substituir ela por ou) a grandeza física relacionada à elementos armazenados de energia:

  ![image-20201209090309634](Images - Control Systems/image-20201209090309634.png)

### Função de transferência

* ?
* Metodologia para modelagem em função de transferência s
  1. Escolher/identificar quem é entrada e quem é saída 
  2. Começar a escrever várias equações em s feito um maluco
  3. Equacionar uma única equação que use apenas entrada e saída
  4. Manipular pra que fique entrada/saída = algo


### Diagrama de blocos

* See in Signal and Systems
*  To draw a block diagram for a sys-
  tem, first write the equations that describe the dynamic behavior of each component.
  Then take the Laplace transforms of these equations, assuming zero initial conditions,
  and represent each Laplace-transformed equation individually in block form. Finally, as-
  semble the elements into a complete block diagram.
* :book: Block Diagram transformations: Dorf pg 81

### Diagrama de fluxo de sinal

* Complementar ao Diagrama de Blocos

* permite representar condições iniciais, o que o diagrama de bloco não permitia

* (+) simples e rápido de desenhar

* **elementos**

  * nodes = variables
  * edges = transfer functions (transmitances)
  * Percurso diréto: leva da entrada pra saída
  * laço: caminho fechado
  * laço disjunto: não se tocam 
  * do nó de entrada só saem edges, no nó de saída só chegam

* cascata série: produto das transmitâncias

* cascata paralela: soma das transmitâncias

* A Fomula de Mason pode ser usada para identificar a função de transferência:

  ![image-20201209081413965](Images - Control Systems/image-20201209081413965.png)

* O determinante do sistema é dado por:

  ![image-20201209081941163](Images - Control Systems/image-20201209081941163.png)

* Os cofatores $\Delta_k$ são obtidos de $\Delta$ removendo os laços que tocam os percursos $T_k$ .

### Espaço de estados

* (+) Bom para sistemas não lineares

* (+) Bom para sistema multiple-input-multiple-output

* (-) O tratamento matemático é todo vetorial, e raramente visualizável graficamente

* state: smallest set of variables such that knowledge of these variables at t=t0 , together with knowledge of
  the input for t $\geq$ t0 , completely determines the behavior of the system for any time
  t $\geq$ t0 .

* state variables: variables that are part of the State

* state vector: putting together the state variables

* state space: space whose axis are the state variables

* **Metodologia de projetos**

  1. Escolher as variáveis de estado (geralmente vC e iL)
  2. Definir o máximo possível de equações que levem de algo para essas variáveis, tipo $Vout = R1*iL$
  3. Equacionar n equações (n = ordem do sistema) que utilizem apenas as variáveis de estado e de entrada
  4. Separar os termos e montar a matriz

* pegamos uma equação diferencial de ordem $n$ e transformamos em um conjunto de equações diferenciais de ordem 1, organizadas em uma matriz

* Como todas as EDOs são de primeira ordem, todas as condições inciais são em t=0 (e portanto apenas um unroll é necessário para as equações recursivas; como assumimos que o sistema parte do repouso, das derivadas do estado são zero em t=0)

  * 

* 

* 

* 

* Matriz de transição (phi) representa a resposta livre do sistema, no tempo

* $\phi(t) = LTI\{ (sI-A)^{-1} \} = e^{At}$

* propriedades de phi:

  ![image-20210729230543660](Images - Control Systems/image-20210729230543660.png)

* 

* 

* 

* conjunto de equações diferenciais de primeira ordem

* Os autovalores de A sao iguais aos polos de G(s)

  ![image-20210729213402420](Images - Control Systems/image-20210729213402420.png)

  > cada setinha é um vetor, e não mais um sinal

  ![image-20210729213441296](Images - Control Systems/image-20210729213441296.png)
  $$
  \dot x = \text{vetor de ??? }(n \times 1) \\
  
  x = \text{vetor de estado }(n \times 1) \\
  
  A = \text{matriz de evolução }(n \times n) \\
  
  B = \text{matriz de controle/entrada }(n \times m) \\
  
  u = \text{vetor de controle/entrada }(m \times 1) \\
  
  y = \text{vetor de resposta/saída }(L \times 1) \\
  
  C = \text{vetor de resposta/saída }(L \times n) \\
  
  D = \text{matriz de transmissão direta }(L \times m) \\
  
  -------\\
  
  n = \text{numero de variáveis de estados}\\
  m = \text{numero de entradas}\\
  L = \text{numero de saidas}\\
  $$

* As equações são chamadas “eq diferencial de estado” e “eq de saída”

* autovalores de A = polos do sistema = raízes da equação característica

* **Associação de sistemas**

  * Série:

    ![image-20220530081040329](Images - Control Systems/image-20220530081040329.png)

    ![image-20220604204610721](Images - Control Systems/image-20220604204610721.png)

  * Paralelo:

    ![image-20220530082219947](Images - Control Systems/image-20220530082219947.png)

  * Feedback:

    ![image-20220530082251330](Images - Control Systems/image-20220530082251330.png)

* **Conversão EE->FT**

  * se dois modelos em EE levam a mesma função de transferência, então eles são o mesmo sistema para fins praticos (se comportam da mesma forma se observarmos apenas entrada e saída). Provavelmente são o mesmo circuito/sistema, mas modelado de uma forma diferente
  * ?

* **Conversão FT->EE**

  * dado:

    ![image-20220728193713680](Images - Control Systems/image-20220728193713680.png)

  * transforme para uma representação “canonica” em espaço de estados:

    ![image-20220728193732101](Images - Control Systems/image-20220728193732101.png)

  * (se o ganho DC não for 1, multiplique a matriz C pelo ganho)

  * obviamente isso é apenas uma possibilidade, há vários sistemas possíveis em espaço de estados para uma equação de função de transferência

  * Example:

    ![image-20220728193837865](Images - Control Systems/image-20220728193837865.png)

* 

* **Example 1**

  * Planta:

    ![img](Images - Control Systems/Untitled.png)
    
  * equação diferencial: ![image-20210926165250143](Images - Control Systems/image-20210926165250143.png)

  * equação recursiva:

    ![image-20210926165315771](Images - Control Systems/image-20210926165315771.png)

  * Dominio S: $G(s) = \frac{1}{RCs+1}$

  * Domínio Z, considerando entrada degrau:

    ![image-20210926165344109](Images - Control Systems/image-20210926165344109.png)

  * Espaço de estado:

    ![image-20220604204437803](Images - Control Systems/image-20220604204437803.png)

* **Example 2**

  * Planta:

    ![image-20210926165419946](Images - Control Systems/image-20210926165419946.png)

  * Sendo x1=vc e x2=il, temos:
    $$
    \dot x_1 = \frac{1}{C} u - \frac{1}{C}x_2 \\
    \dot x_2 = \frac{1}{L}x_1 - \frac{R}{L}x_2
    $$
    

* **Example 3**

  * Circuito de segunda ordem

  * [Falstad circuit](http://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWcMBMcUHYMGZIA4UA2ATmIxAQBYKLIKBTAWjDACgB3EFBQ8S67rzR4oHPtTD8uPcaM4sU4YosoiwyueDCKUKCdum9IrAOYhVh8yMyK6xgMohmvdYsbEXGuyABmAQwAbAGd6KC5WADcnDxBsNGihfDCVO2gkGHSoNNME2Pj3XmxsI1EzZik4unLqIpLjTmq8qskJDCMxRrA23K72gCcnME9FIaVbZHhWAdHXKzGwhEn5YfAXKXrV2QsW0QBjOdmLQTCwJjVYeEhKSARi7CpsCBg4Nn3tqR2dujALy7-L06McjPS7EBDKW7cDx6DDg6ggth+LQ6JLHYQgYjgJDMJBgf5ZUiE663EiqYiUCA-P7Eu4PSl-MQKSxMmyaFkYHQyVkbNEcyzo4wAexOJWuxGsaSy8AweG0mXG2FYQA)

    ![image-20220604203404111](Images - Control Systems/image-20220604203404111.png)

* modelagem:

  ```
  # Valores do circuito
  R1 = 10000
  R2 = 5000
  C1 = 10e-9
  C2 = 100e-9
  
  # Definição do sistema
  _A = np.array([[0, 1/(R2*C1)], [-1/(R1*C2), (-R1-R2)/(R1*R2*C2)]])
  _B = np.array([[0], [1/(R1*C2)]])
  _C = np.array([[1, 0]])
  _D = np.array([[0]])
  
  sys_ss = control.ss(_A, _B, _C, _D)
  t, y = control.step_response(sys_ss)
  plt.plot(t, y)
  ```

  

* **Outras anotações:**

  * convertendo de state space para funsfer function: $G(s) = C(sI-A)^{-1}B+D$
  * convertendo de state space para funsfer function: ??; ` [A,B,C,D]=tf2ss(num,den)`
  * convertendo de transfer function para state space: `Gs = control.ss2tf(sys);`

## Índices de desempenho

* Ou *métricas de mérito*

* o quanto o motor faz o que é preciso

* Para um certo sistema e uma certa entrada, as métricas podem geralmente ser calculadas de forma analítica

* geralmente/sempre esses parâmetros são obtidos a partir da FTMF

* geralmente considerand uma resposta ao degrau unitário

* :keyboard: Matlab `step_info()`: já trás uma série de metricas

* **métricas básicas**
  
  * <u>valor de regime permanente</u>
  
    * para uma certa entrada, geralmente degrau
    * Para resposta ao degrau ou impulso: $y(\infty) = \lim_{s\rightarrow 0}sY(s)$ (não esqueça de considerar o sinal de entrada)
    
  * <u>tempo de atraso ($t_d$)</u>
  
    * tempo até chegar até metade d
  
  * <u>tempo de subida</u>
  
    * tr=t90% - t10%
  
    * ou simplesmente t90%
  
    * Para segunda ordem, para $0.3<\zeta<0.8$:
      $$
      tr=\frac{2.16 \zeta + 0.6}{w_n}
      $$
    
  * <u>instance de pico (tp)</u>
  
    * Para segunda ordem:
      $$
      t_p = \frac{\pi}{w_n \sqrt{1-\zeta^2}} = \frac{\pi}{w_d}
      $$
    
  * <u>sobresinal máximo (Mp)</u>
  
    * percentual, $[0,1]$
  
    * Para segunda ordem:
      $$
      M_p = e^{-\frac{\pi \zeta}{\sqrt{1-\zeta^2}}}
      $$
      ![image-20210210225712133](Images - Control Systems/image-20210210225712133.png)
      
    * Sistemas de segunda ordem com o mesmo sobressinal possuem polos no mesmo ângulo:
  
      ![image-20210607215832227](Images - Control Systems/image-20210607215832227.png)
  
  * <u>setling time (ts)</u>
  
    *  até estabiizar em um percentual do valor permanente, geralmente uns 5%
    *  para sistemas de segunda ordem, geralmente podemos aproximar usando apenas a envoltória exponencial (4 tau)
    * Para segunda ordem, aproximando:
    
    $$
    t_{s5\%}= 3 \tau = \frac{3}{\zeta w_n} \\
    t_{s2\%}= 3.9 \tau = \frac{3.9}{\zeta w_n} \\
    t_{s1\%}= 4.6 \tau = \frac{4.6}{\zeta w_n} \\
    $$
    
    * Sistemas de segunda ordem com o mesmo t_s possuem polos na mesma vertical:
    
      ![image-20210607220035531](Images - Control Systems/image-20210607220035531.png)	
  
* **Erros**

  * <u>Erro de Regime Permanente ($e_{ss}$) genérico</u>

    * Sinal de erro (sinal logo após o somador) quando t->inf
    
    * Se o feedback for unitário, erro = valor de regime permanente - valor setado
    
    * Pode ser calculado diretamente pelo valor final do sinal de erro, inclusive usando teorema do valor final
    
    * O erro pode ser calculado diretamente por:
    
    * ![image-20210211085015868](Images - Control Systems/image-20210211085015868.png)
    
      ![image-20220613093101415](Images - Control Systems/image-20220613093101415.png)
    
    * Para segunda ordem, resposta à rampa: $e(\infty) = \frac{2\zeta}{w_n}$
    
    * $e_{ss}$ para espaço de estados: $e(t=\infty) = \lim_{s\rightarrow0} {sR(s)(I - C(sI-A)^{-1}B)}$
    
    * $e_{ss}$ para espaço de estados entrada degrau: $e(t=\infty) = 1+CA^{-1}B$
    
    * $e_{ss}$ para espaço de estados entrada rampa: $e(t=\infty) = \lim_{s\rightarrow0} {[(1+C(A^{-1})B)t + C(A^{-1})^2 B]}$
    
  * <u>Coeficiente de erro de posição estático (KP)</u>
    
    * relacionado à entrada degrau
    
    * Definido como:
    
      ![image-20210224215142714](Images - Control Systems/image-20210224215142714.png)
      $$
      e_{ss}=\frac{1}{1+K_P}
      $$
    
  * <u>Coeficiente de erro de velocidade estático (KV)</u>
    
    * Relacionado à entrada rampa
    
    * Definido como:
    
      ![image-20210224215847442](Images - Control Systems/image-20210224215847442.png)
      $$
      e_{ss}=\frac{1}{K_V}
      $$
    
  * <u>Coeficiente de erro de aceleração estático (KA)</u>
  
    * Relaciona à entrada parabólica
  
    * Definido como:
  
      ![image-20210224215346098](Images - Control Systems/image-20210224215346098.png)
      $$
      e_{ss}=\frac{1}{K_A}
      $$
  
* **Bandwidth**
  * ?
  * The bandwidth may often be an important factor when noise is present at the input and generated within the
    system
  * é relevante quando queremos leva em consideração algumas frequencias específicas do nossos sistema (chaveamentos, oscilações, etc) que queremos que o nosso controlador seja capaz 
  * [this video seems good](https://www.youtube.com/watch?v=evVi_D7C6mA&list=PLxdnSsBqCrrF9KOQRB9ByfB0EUMwnLO9o&index=10)

## Anotações sobre sistemas

* Muitas coisas aqui estão em Sinais e Sistemas, mas nem tudo eu consegui juntar

* Para sistemas de ordem maior que 2, nós podemos
  frequentemente usar as técnicas de pólos dominantes para
  aproximar o sistema por uma função de transferência de 2a
  ordem (dois pólos).

* Forma canônica de um sistema de segunda ordem:

  ![image-20210205083634869](Images - Control Systems/image-20210205083634869.png)

  > Zeta = fator de amortecimento; é adimencional; meio que “multiplica na frequencia”
  >
  > wn = frequencia natural

* A transformada inversa de Laplace da resposta ao degrau desse sistema é bem conhecida, dada por:

  ![image-20210210084105967](Images - Control Systems/image-20210210084105967.png)


* perceba que aquele arctg é apenas um angulo fixo de defasagem
* O termo $w_n\sqrt{1-\zeta^2}$ é usualmente chamado de frequência natural amortecida, $w_d$, e pode ser obtida diretamente diretamente do gráfico no caso subamortecido
* zeta maior -> oscila mais, mas sobre mais devagar?
* For a given $w_n$, the response is faster for lower $\zeta$, but the overshoot will be bigger (and probably the settling time too)
* se tem um polo na origem, erro permanente será zero?
* zeros só afetam o transitório?
* **Principais equações**
  * <u>Análise em malha fechada</u>
    * Bom para analizar quase tudo
    * $FTMF(s)=\frac{G(s)}{1+G(s)*H(s)}$
    * Equação característica: polinomio do numerador da FTMF
  * <u>Análise em malha aberta</u>
    * $FTMA=G(s)*H(s)$
    * para analisar estabilidade
  * <u>análise em malha direta</u>
    * Quando um sistema é análisdo sem a realimentação (malha aberta), também é chamado *malha direta*; improtantte pra analisar o transitório
    * $FTMD(s)=G(s)$
* **Tres casos de raízes**

  * se estiverem no semiplano direto, instável
  * <u>reais distintas</u>
    * ksi>1
    * superamortecido
    * Se ksi>>1, podemos aproximar por um sistema de primeira ordem (ver slide 3 número 25)
  * <u>Reais iguais</u>
    * ksi=1 
  * <u>complexas conjugadas</u>
    * if 0<ksi<1
    * if ksi=0, oscilates forever
    * Distancia entre as raízes = sigma = o quanto oscila
* **Orders above 2**

  * many sys-
    tems possess a dominant pair of roots and the step response can be estimated by second ordem system
  * <u>Inserção de polo</u>
    * diminuindo a estabilidade relativa;
    * deixando o tempo de acomodação mais lento.
    * Dado um sistema e terceira ordem na forma:
  * ![image-20210218082429950](Images - Control Systems/image-20210218082429950.png)
    * podemos aproximar por um sistema de segunda se $\frac{1}{T} \geq 10 |\zeta w_n|$ (equivalente ao terceiro polo ser 10x maior que os outros dois?)
  * <u>inserção de zero</u>
    * aumenta estabilidade relativa;
    * Tempo de acomodação menor (acelera a resposta)
    * Sobresinal maior
  * <u>Aproximação por segunda ordem</u>
    * quanto mais pra esquerda no locus plot (-inf) estiverem as raizes, mais rapidas elas são 
    * portanto, mais insignificantes
    * geralmetne podemos aproximar por duas raízes dominantes 
    * um integrador é sempre dominante
* **Systems with zeros**

  *  If the
    transfer function of a system possesses a finite zero and it is located relatively
    near the dominant complex poles, then the zero will materially affect the transient
    response of the system

## Estabilidade

* vai pro regime permanente fixo

* instáveis costumam ter um tempo de subida menor

* raízes instável são dominantes: uma raíz instável caga o polinomio inteiro

* estabilidade absoluta: é ou não é

* estabilidade relativa: fornece um grau de estabilidade, uma margem

* Adicionar um integrador (polo na origem) deixa o sistema mais instável

* Relative stability can also br measured by the settling time of each root or pair of roots. Therefore, a system
  with a shorter settling time is considered relatively more stable.

* **Estabilidade da resposta forçada**
  
  * Ou *BIBO*
  * é o critério mais usado, na prática
  * Condição (necessary and sufficient): estável se todos os pólos da FTMF estiverem
    localizados no semi-plano esquerdo do plano “s”, eixos não inclusos
  * pensando na ILT, vemos que um um polo com parte real positiva levará à uma exponencial com t positivo
  * Caso contrário, y(t) conterá uma componente de
    crescimento ilimitado quando t cresce.
  * <u>marginalmente estável</u>
    * only certain bounded inputs (sinusoids of the frequency of the poles) will cause the output to
      become unbounded
    * when poles are exactly in the jw exis
    * não decai a zero nem vai para o infinito
    * um sistema marginalmente estável não é estável
  
* **Estabilidade da resposta livre**

  * Ou *assintótica*
  * Muito similar à BIBO, ressaltarei aqui apenas as diferenças
  * Se, para qualquer condição inicial, a resposta vai a zero
  * Condição (necessary and sufficient): estável se todos os *modos* estiverem localizados no semi-plano esquerdo do plano “s”, incluso eixo
  * relembrando: modos =  raízes da FTMF antes de cancelarmos com os zeros (ou seja, a equação característica original)?

* **critério/método de routh-hurwitz**

  * Conta o número de raizes da FTMF no semiplano direito, sem calcularlas 

  * analisa estabilidade absoluta

  * (+) pode ser feito com termos genérico (tipo um K livre)

  * Condições necessárias: Todos os coeficientes do polinômio tenham o mesmo sinal? Nenhum dos coeficientes seja nulo?

  * <u>:keyboard: scilab: routh_t(<polinomo>)</u>

    * com variável livre só funciona se for na realentação

    * Exemplo, com FTMF=k/s^3…

      ```matlab
      s = %s;
      EC = s^3 …
      r=routh_t(1/EC, poly(0, ‘k’))
      disp(r)
      
      //testar um determinado K
      K = 0;
      [r,num]=routh_t(P1,K)
      if num==0
         disp("System is stable")
      else
         mprintf("There is %g sign changes in entries of first column.\nTherefore, system is unstable.", num)
      end
      ```
    
  * <u>prodecimento</u>
  
    * ache o polinomio numerador do FTMF:
  
      ![image-20210304000033301](Images - Control Systems/image-20210304000033301.png)
  
    * Reorganize os termos da seguinte forma (sempre duas linhas):
  
  ![image-20210304000113556](Images - Control Systems/image-20210304000113556.png)
  
  
  
    * multiplique em sizezage descendo, dividindo pelo inferior esquerdo
  
    * dica: pense em um determinante tomado 2x2 em 2x2 (originalmente o método era todo por determinante)
  
    * na forma:
  
      ![What is the Routh's stability criterion? - Quora](Images - Control Systems/main-qimg-cfee4f573e9bcf51787ab00c54b9bb0a.webp)
  
    * O ultimo termo será sempre igual ao termo independente
  
    * Case 1: No element in the first column is zero; proceed normally
  
    * Case 2 there is a zero in the first column, but some other elements of the row containing the zero; proceed normally (knowing that there is a pole exactly in the axis)
      in the first column are nonzero
  
    * Case 3: there is a zero in the first column, and the other elements of the row containing the zero are also zero; proceed using the coeficients of the derivate of the auxiliary equation (see in slide 23)
  
    * Case 4: as in the third case, but with repeated roots on the $j\omega$ axis.
  
  * <u>Análise do resultado</u>
  
    * Olharemos apenas para a primeira coluna, com os termos na sequência
    * o numero de trocas de sinal indica o numero de polos no semiplano direito 
    * Quando um termo é zero, geralmente a estabilidade é limítrofe (par de raízes no eixo imaginário)
    * Se houver uma variavel livre, podemos obter as faixasde valor tal que o sistema seja estável
  
  * Caso especial: linha toda zero
  
    * ?
  
* **Análise por state space**

  * location of the eigenvalues of the system matrix
  * Se todos os polos estiverem no semiplano direito
  * Se a matriz A for simbólica, dá pra fazer pelo critério de routh-hurwitz
  
* **Condição de pertinência**

  * a partir da equação de malha aberta
  * condição suficiente mas não necessária
  * Para ser estável um sistema deve ter $|GH|<1$ na frequência em que $fase(GH)=180$ (ou multiplos, 180 + k*360)
  * quanto mais distante desse limiar, mais estável 

* **Critério de Nyquist**

  * graphical technique for determining the stability of a dynamical system
  * can be applied to systems defined by non-rational functions
  * restricted to linear time-invariant (LTI) systems

* **Critério de barkhausen**

  * Critério para condição de oscilação em estruturas realimentadas
  * a complex pole pair must be placed on the imaginary axis of the complex frequency plane
  * Veja em *osciladores*

* **Lyapunov criterion:** for non linear

* **Circle criterion:** for non linear

## Considerações práticas

* **Sinal de controle**
  * muitas vezes há um limite que podemos atuar, ex: tensão de alimentação
  * Uma boa forma de calcular a ação de controle é reescrevendo o sistema para que a "saída" seja o bloco de controle, e daí rodar a simulação normalmente
  * Ou seja, o caminho direto será simplesmente C (controlador) e a realimentação será Y (planta), de forma que a saída do "sistema" será a saída do controlador

* **Escolha do sample period**
  * 8 a 10 vezes menor do que o tempo de subida desejado
  * 10 a 15 vezes menor que o tempo de resposta transitória DESEJADO
  * w_sample/w_d > 10 (número de amostras por oscilação amortecida)

* **Medindo sinais intermediários no simulador**

  * Geralmente os simuladores só fornecem a saída

  * Podemos criar um novo sistema tal que a saída seja, na verdade, o sinal intermediário de interesse

  * Visualizando o erro (ou melhor, o feedback, mas é só subtrair a referência):

    ![image-20220604193628236](Images - Control Systems/image-20220604193628236.png)

  * Visualizando o sinal de controle:

    ![image-20220604193732104](Images - Control Systems/image-20220604193732104.png)

* Delay nas simulações por equaçoes recursivas em espaço de estados
  * A culpa é do zoh do integrador, que dá 1 tempo de delay
  * Eu só vou ter um valor não zero de derivada na segunda iteração
  * com controlador são dois zoh, o que leva a 2 delays

## Outros

* **Tipo do sistema**

  * numero de polos na origem na FTMA

  * ou seja, numero de elementos integradores na malha aberta

    ![image-20210214114747567](Images - Control Systems/image-20210214114747567.png)
    
  * Para sistemas discretos no domínio z, é o número de polos em z=1

  * Para cada tipo de sistema e cada entrada, podemoschegar em expresões analíticas para os erros, conforme tabela do slide 3-29 do Joni

* **Experimental determination of the transfer function**

  * chose step or impulse response (usually lower than the normal machine operation)
  * apply the input and measure the output
  * collect the operation data
  * curve fitting; usually we use a sum of exponential, $\sum k_i e^{\tau_i t}$
  * if you want to use different ad hoc input functions, collect the data, fit a curve on the input, and take the lapace transform?

* **Non-minimum phase systems**

  * the system initially moves in the wrong direction

* **Software tools**

  * Matlab, octave and scilab have packages for control
  * Python Control Systems Library 

# Técnicas de análise

## Root Locus

* plotar como as raízes se movimentam no plano S ao variármos um parâmetro k

* K multiplica tudo (ganho)… existem variações? sim, as vezes é feito com o parâmetro sendo a resistência de carga de uma fonte chaveada (ou seja, o parâmetro é algo da planta)

* Developed by Evans in 1948

* provides a measure of the sensitivity of the roots of the
  system to a variation in the parameter being considered
  
* A partir da FTMA
  
* **Root locus plot**
  
  * :keyboard: ​scilab: evans(<FTMA>, <maxK>)
  
  * :keyboard: matlab: rlocus(<sys>)
  
  * :keyboard: python: `k = np.linspace(0.01, 1000, 100); r = control.root_locus(FTMA, k)`
  
  * :keyboard: scilab com melhorias do Joni:
  
    ```matlab
    s=%s;
    num= 1;
    den= s*(s+4)*(s^2+2*s+2);
    m= length(coeff(num))-1; n= length(coeff(den))-1;
    FTMA=syslin('c',num,den);
    clf(); evans(FTMA,500);
    xgrid(31); // adiciona uma grade de cor 31
    l=gca(); // retorna o identificador dos eixos
    l.isoview= 'on';
    l.data_bounds=[-4.5 1 -3 3]; // altera os limites dos dados
    l.x_location = 'origin'; // eixo x na origem
    l.y_location = 'origin'; // eixo y na origem
    l.y_label.position = [0,3.7]; // altera a posição do rotulo do eixo y
    l.y_label.text= 'jw'; // altera o rotulo do eixo y
    disp(l.children); // imprime o vector que contém os identificadores de todos os objetos filhos dos eixos
    legenda= l.children(1);
    legenda.visible= 'off'; // legenda invisível
    ramos= l.children(2); // espessura e cor de cada ramo
    n_ramos= n;
    cor= 2;
    thick= 3;
    for r=1:n_ramos
      ramos.children(r).thickness= thick;
      ramos.children(r).foreground= cor;
      cor= cor+1;
    end
    
    n_assintotas= n-m;
    if (n_assintotas>0) then
      for a=1:n_assintotas
        l.children(2+a).line_style=2; // assintotas tracejadas
      end
    
      if length(l.children)>=(n_assintotas+4) then
        l.children(n_assintotas+4).thickness=3; // espessura dos polos
        l.children(n_assintotas+4).mark_size=10;      
      end
      if length(l.children)>=(n_assintotas+5) then
        l.children(n_assintotas+5).thickness=3; // espessura dos zeros
        l.children(n_assintotas+5).mark_size=10;
        l.children(n_assintotas+5).mark_style=9;
      end
    elseif
      l.children(n_assintotas+3).thickness= 3; // espessura dos polos
      l.children(n_assintotas+3).mark_size= 10;
      l.children(n_assintotas+4).thickness= 3; // espessura dos zeros
      l.children(n_assintotas+4).mark_size= 10;
      l.children(n_assintotas+5).mark_style= 9;
    
    end
    
    [Ki,s1]=kpure(FTMA); // K para imaginario puro
    plot([real(s1) real(s1)],[imag(s1) -imag(s1)],'sr');
    disp(Ki,'k(inst)= ');
    disp(s1,'s(inst)= ');
    
    ```
    
    
    
  * m e n: os mais altos graus do numerador e denominador da FTMA, respectivamente
  
  * traçamos os polos para k de zero a infinito
  
  * Os polos se aproximam dos zeros (ou de initifito, se houver mais polos que zeros) quando k tende a infinito
  
  * Se em um ponto do eixo real há um número ímpar de polos e zeros à sua direita, esse ponto percente ao L.R.
  
  *  
  
  * Os polos que são vão para zero seguem assíntoras
  
  * As assíntotas são centradas em um ponto do eixo real dado por:
  
    <img src="Images - Control Systems/image-20210312001512757.png" alt="image-20210312001512757" style="zoom:80%;" />
  
  * e com ângulos dados por:
  
    <img src="Images - Control Systems/image-20210312001538141.png" alt="image-20210312001538141" style="zoom:80%;" />
  
  * ponto de ruptura: quando pois polos se unem e deixam o (ou chegam no) eixo real
  
  * O ponto de ruptura ocorrerá onde K alcança o seu ponto de máximo sobre o eixo real. Assim basta, diferenciar a equação de K(s) em relação a s, igualar a zero, pegar a máxima raiz real
  
* **analysis**

  * [Relationship Between Poles and Performance of a Dynamic System](https://www.youtube.com/watch?v=0tbr4OIufK8&list=PLxdnSsBqCrrF9KOQRB9ByfB0EUMwnLO9o&index=18)

## Bode plot

* Ver mais em *Análise de circuitos* e *Sinais e Sistemas*

* bastante usado para projeto de compensadores

* A partir da FTMA

* [Wolfram (online)](https://www.wolframalpha.com/input/?i=bode+plot+1%2F%28s*%28s%2B1%29%29)

* **Análise de margens**
  * Margens de fase e de ganho
  
  * determinar as margens de estabilidade (relativa)
  
  * Quão distantte está das condições de pertinência (ver em *estabilidade*)
  
  * cuidado: no bode plot, magnitude 1 = 0 dB
  
  * Margem de fase: no ponto crítico de magnitude, quanta fase falta para 180º
  
  * margemde ganho: no ponto crítico de fase, quanto ganho falta para zero
  
  * Margens positivas = sistema estável
  
  * <u>dicas práticas</u>
  
    * margem de fase deve estar entre 30° e 60°;
  
    * margem de ganho deve ser maior que 6dB.
  
    * :keyboard: Scilab plots (os resultados de margens estão bem estranhos, veja diretamente no gráfico!):
  
      ```matlab
      s=%s;
      num= 24;
      den= s*(s+2)*(s+4);
      FTMA= syslin('c', num, den);
      
      [mf ,fg]=p_margin(FTMA)
      [mg,ff]=g_margin(FTMA)
      printf('Margem de fase: %.2f graus\n', mf)
      printf('Frequência onde o ganho é 1: %.2f Hz\n', fg*100)
      printf('Margem de ganho: %.2f dB\n', mg)
      printf('Frequência onde a fase é 180º: %.2f Hz\n', ff*100)
      show_margins(FTMA);
      ```
  
  * A margem de fase ($\gamma$) pode ser determinada analiticamente para um sistema de 2º ordem:
  $$
    \gamma=\arctan\left(\frac{2\zeta}{\sqrt{\sqrt {4\zeta^4+1} - 2\zeta^2}}\right)
  $$
  
    ![image-20210318092216501](Images - Control Systems/image-20210318092216501.png)
  
  ![image-20210411011650232](Images - Control Systems/image-20210411011650232.png)
  
  > WG = frequencia (rads/s) onde o ganho é 1
  
* **Análise da inclinação**

  * variações rápidas no bode plot da FTMA são indicativos de instabilidade
  * essa análise é especialmente relevante na frequência de cruzamento da margem de ganho
  * Uma inclinação do módulo em -20dB/década é bem estável
  * Inclinação for de -40dB/década é ruim
  * Inclinação -60dB/década, o sistema provavelmente é instável.

## Nyquist plot

* (+) Permite analisar atrasos (tempo morto) na inicialização

* gráfico polar em função de $\omega$

* objetivo: identificar se há polos no semiplano direito

* **Fatores**

  * mesmos do bode

* **Sobre formas arbitrárias no plano polar**

  * baixas frequências?:

    ![image-20210320095458284](Images - Control Systems/image-20210320095458284.png)

  * altas f:

    ![image-20210320100220740](Images - Control Systems/image-20210320100220740.png)

* **analise**

  * Efeitos de ressonancia acontecem quando a amplitude é máxima. Se a máxima amplitude acontece em angulo 0 (DC?), não haverá nenhuma ressonanscia
  * polos forçam traçados horários
  * zeros forçam traçados antihorários
  * sistemas realizáveis no mundo possuem sempre uma tendência a traçados horários
  * de forma geral, quando a frequência aumenta o angulo aumenta e o módulo diminui
  * de forma geral, é siétrico em relação ao eixo polar (“x”)
  * Podemos traçar vários gráficos, para vários k, para analisar a estabilidade

* **numero de circulação de um dado ponto**

  * quantas vezes a curva descreve 360º ao redor do gráfico

* **plano domínio e plano imagem**

  * Valores de $s$ (domínio) e para onde eles levam na equação (imagem)
  *  
  * circulação de nada: circulação não envolvendo zero
  * circulação de um zero: circulação da origem no mesmo sentido
  * circulação de polos: circulação da orgem no sentido inverso
  * circulação de polos e zeros se anulam

* **Diagrama de nyquist**

  * Usamos a FTMA (ou 1+ FTMA?)

  * o raciocínio de circulação da origem no plano domínio é, no diagrama de nyquist, em relação ao ponto $(-1,0)$ (pois usamos a FTMA e não o denominador da FTMF)

  * Quando a circulação passa por cima de um polo ou zero, não inclui este

  * a curva de circulação de nyquist é dada por:

    ![image-20210320104430142](Images - Control Systems/image-20210320104430142.png)

  * Análse feita todo no plano imagem

  * por causa do $+1$, estamos interessados em analisar se a circulação no plano imagem passe por $-1+j0$ ao invés da origem
    $$
    Z = N + P
    $$

    > N = quantas vezes o plot circula $(-1,j0)$ no sentido horário, podendo ser negativo
    > P = número pólos de $1+G(s)H(s)$ no SPD

  * A estabilidade é definida por Z

  * Se Z<=0, a FTMF do sistema é ESTÁVEL;

  * Se Z>0, a FTMF do sistema é INSTÁVEL;

  * Traçado manual: ponto a ponto?

  * :keyboard: scilab:

  ```matlab
  s=%s;
  num=1;
  den=(s+1)*(s-2);
  FTMA=syslin('c', num, den);
  nyquist(FTMA, 1E-6, 1E+6);
  l=gca();
  //l.isoview= 'on';
  l.x_label.text= 'Re[GH(jw)]';
  l.y_label.text= 'Im[GH(jw)]';
  plot(-1,0, 'x')
  //zoom_rect([-2,-1,0,1])
  //title('Nyquist plot for k=1')
  ```

* **Analisando margem de fase a de ganho**

  * Podem ser obtidos diretamente do nyquist plot

  * Gain margin = $20\log\left(\frac{1}{\text{where plot cross x}}\right)$

  * Phase margin = angulo de cross com circulo unitário = $arctg\left(\frac{\text{y do cross}}{\text{x do cross}}\right)$

    ![image-20210324230321292](Images - Control Systems/image-20210324230321292.png)
    
    ```matlab
    s=%s;
    num= 24;
    den= s*(s+2)*(s+4);
    FTMA= syslin('c', num, den);
    clf();
    nyquist(FTMA, 1E-6, 1E+6);
    show_margins(FTMA, ‘nyquist’);
    [mf ,fg]=p_margin(FTMA) ; disp(fg,mf);
    [mg,ff]=g_margin(FTMA) ; disp(ff,mg);
    
    ```

## Nichols charts

* Loop gain vs loop phase? 
* 

# Técnicas de controle clássico

* the design of a control system is concerned with the plan of the system structure and the selection of suitable components and parameters.
  
* **Pretende melhorar**
  
  * robustez
  * estabilidade
  * resposta transitória
  * resposta de regime permanente
  
* **on–off controllers**
  
  * Or *Two-position*
  
  * the signal u(t) remains at either a maximum or minimum value
  
  * we can also add histeresis, or *diferencial gap*,  to prevent too-frequent operation of the on–off mechanism
  
    ![image-20201125090014804](Images - Control Systems/image-20201125090014804.png)

## Compensador

* botar um polo e um zero a mais

* é mais “cirurgico” que o PID

* Mainly using root locus

* Zeros e polos reais

* The compensator placed in the feedforward path is called a cascade compensator, but we can have it in the feedback, output (or load), or input

* in a generic way, most controller can be considered a compensator (even PID)

* We’ll focus on cascade compensators

  ![image-20210408105435822](Images - Control Systems/image-20210408105435822.png)

  ![image-20210408105552573](Images - Control Systems/image-20210408105552573.png)

* **Variação - Erro nulo**
  
  * Caso quisermos garantir erro nulo em um compensador, podemos projetar considerando que a planta possui um integrador
  
  * Depois na hora de finalizar o projeto simplesmente colocamos um integrador ali, hehe
  
  * ou seja, a planta enxerga como se o integrador fosse do controlador, e o controlador enxerga como se o integrador fosse da planta
  
    ![image-20210926165145998](Images - Control Systems/image-20210926165145998.png)
  
* **Ampop implementation**
  
  * dorf pg 756 (dá pra fazer um lead-lag colocando capacitores em paralelo com R3 e R4)
  * joni slide 5 pg 37

### Lead

* Ou *Compensador em Avanço de Fase*
* polo maior (em módulo) do que o zero

* É um filtro passa alta

* (+) incrementa a estabilidade relativa
  
* (+) fica mais rápido (rise time decrease)
  
* (+) aumento da margem de fase

* (+)  increases the system bandwidth

*  (-) há um incremento do erro de regime (principalmente s K for pequeno)

*  (-) Fica mais suceptível à ruidos de alta frequência

*  Dado por:

   ![image-20210331081506810](Images - Control Systems/image-20210331081506810.png)

* Cuidado com o ganho Kc, pode afetar o erro em regime permanente

* :man_scientist: [interesting paper](https://www.researchgate.net/publication/228589520_The_Application_of_the_Root_Locus_Method_for_the_Design_of_Pitch_Controller_of_an_F-104A_Aircraft)

* <u>Metodologia de design - Por LR</u>

  * escolhe-se KC, normalmente pela especificações de erro de regime permanente.

  * Escolhe-se um ponto $s1$ do plano “s”, que pertença ao lugar das raízes:
    $$
    s_1=-\zeta\omega_n + j\omega_n\sqrt{1-\zeta^2} = M_s \ang \theta_s
    $$

    > Use zeta e omega_n desejados
    
  * Avaliar a FTMA nesse ponto: $G(s_1)=M_G\angle\theta_G$

  * Para que o sistema compensado passe por esse ponto, temos que:

    ![image-20210403153620465](Images - Control Systems/image-20210403153620465.png)

    ![image-20210403153636111](Images - Control Systems/image-20210403153636111.png)

    > K_c = o que foi calculado acima
    >
    > M_g e theta_g = FTMA atual, nesse no ponto desejado
    >
    > M_s e theta_s = no ponto desejado
    >
    > 
    >
    > 

  * :keyboard: scilab:

    ```matlab
    // Desired parameters
    Kp=9;
    Kv=%inf;
    Ka=%inf;
    omega= 100;
    zeta= sqrt(2)/2;
    
    // Current system
    s= %s;
    numG= 1;
    denG= (0.1*s + 1)*(0.2*s + 1);
    
    // Calculate parameter
    clc();
    pi= %pi;
    G= syslin('c',numG,denG);
    H= 1;
    FTMF= G/.H;
    if Kp~=%inf then
    	Kc= horner((Kp/(G*H)), 0);
    elseif Kv~=%inf then
    	Kc= horner((Kv/(s*G*H)), 0);
    else
    	Kc = horner((Ka/(s*s*G*H)), 0);
    end
    disp(Kc, 'Kc= ');
    
    s1= -(zeta*omega)+((omega*(1-zeta^2)^0.5)*%i);
    [mS,tetaS]= polar(s1);
    tetaS= real(tetaS);
    Gs1= horner((G*H), s1);
    [mG,tetaG]=polar(Gs1);
    tetaG= real(tetaG);
    disp((180*tetaS/pi), 'tetaS= ',mS, 'mS= ');
    disp((180*tetaG/pi), 'tetaG= ',mG, 'mG= ');
    Tz= real((sin(tetaS)-(Kc*mG*sin(tetaG-tetaS)))/(Kc*mG*mS*sin(tetaG)));
    Tp= real(-((Kc*mG*sin(tetaS))+(sin(tetaG+tetaS)))/(mS*sin(tetaG)));
    disp(Tp,'Tp [s]= ',Tz,'Tz [s]= ');
    
    // Plot system
    numC= Kc*(Tz*s+1);
    denC= (Tp*s+1);
    Gc= syslin('c',numC,denC);
    FTMFc= (Gc*G)/.H;
    figure(1);
    clf(1);
    t= 0:0.002:2;
    step=csim('step',t,FTMF);
    step2=csim('step',t,FTMFc);
    plot(t,step,'b-',t,step2,'m-');
    xgrid(33);
    figure(2);
    clf(2);
    evans(G*H,100);
    xgrid(33);
    figure(3);
    clf(3);
    evans(Gc*G*H,100);
    xgrid(33);
    
    ```

* <u>Metodologia de deisng - Por Bode</u>

  * ?

  * compensador representado por:
  
    ![image-20210413232712396](Images - Control Systems/image-20210413232712396.png)
  
  * :book: dorf 708
  
  *  :keyboard: scialb:
  
    ```matlab
    clc();
    s= %s;
    pi= %pi;
    Kv= 10;
    mF2= 52;
    numG= 400;
    denG= s*(s^2+30*s+200);
    G= syslin('c',numG,denG);
    H= 1;
    FTMA= G*H;
    FTMF= G/.H;
    Kc= horner((Kv/(s*FTMA)), 0); // Kv=s*[Kc.GH(0)]
    KcGH= Kc*G*H;
    figure(1);
    clf(1);
    show_margins(KcGH,'bode');
    [mF,fG]= p_margin(KcGH);
    Fi= mF2-mF;
    Fi1= Fi+5; // adiciona 5 graus
    alfa= (1+sin(Fi1*pi/180))/(1-sin(Fi1*pi/180));
    Ganho_ad=-10*log10(alfa);
    disp(Ganho_ad,'Ganho_ad [dB]=');
    wGhz= 1.571; // tirado (em Hz) do Diag. de Bode de KcGH
    wG= wGhz*(2*pi);
    T= 1/((alfa^0.5)*wG);
    alfaT= alfa*T;
    disp(alfaT,'alfaT [s]= ',T,'T [s]= ');
    
    numC1= Kc*(alfaT*s+1);
    denC1= (T*s+1);
    Gc1= syslin('c',numC1,denC1);
    FTMFc1= (Gc1*G)/.H;
    Fi2= Fi+10; // adiciona 10 graus
    alfa= (1+sin(Fi2*pi/180))/(1-sin(Fi2*pi/180));
    Ganho_ad=-10*log10(alfa);
    disp(Ganho_ad,'Ganho_ad [dB]=');
    wGhz= 1.133; // tirado (em Hz) do Diag. de Bode de KcGH
    wG= wGhz*(2*pi);
    T= 1/((alfa^0.5)*wG);
    alfaT= alfa*T;
    disp(alfaT,'alfaT [s]= ',T,'T [s]= ');
    numC2= Kc*(alfaT*s+1);
    denC2= (T*s+1);
    Gc2= syslin('c',numC2,denC2);
    FTMFc2= (Gc2*G)/.H;
    clf
    //figure(0);
    //clf(0);
    t= 0:0.002:10;
    step0=csim('step',t,FTMF);
    step1=csim('step',t,FTMFc1);
    step2=csim('step',t,FTMFc2);
    plot(t,step0,'b-',t,step1,‘m-',t,step2,'g-');
    xgrid(35);
    xtitle('Step Response','Time (sec)','Amplitude');
    % Possível melhoria: No pega qual será o efeito da defasagem no GANHO e ajustar conforme
    ```
  
* <u>Metodologia de design - wtf</u>

  * List the system specifications and translate them into a desired root location for the
    dominant roots.
  2. whether Sketch the the root desired locus root with locations a constant can gain be realized.
   controller, Gc1s2 = K, and determine
  3. If a compensator is necessary, place the zero of the phase-lead compensator directly
  below the desired root location (or to the left of the first two real poles).
  4. Determine the pole location so that the total angle at the desired root location is 180°
  and therefore is on the compensated root locus.
  5. Evaluate the total system gain at the desired root location and then calculate the error
  constant.

### Lag

* Ou *Compensador em Atraso de Fase*
* Zero maior
* É um filtro passa baixa
* (+) reduz o erro de regime permanente
* (+) reduz o ruido de alta frequência
* (-) sistema fica mais lento (rise time icreases)
* (-) decreases the system bandwidth
* a diferença de design é pouca (comparando com compesador em avanço), a difereça existe só nos requisitos
* 
* <u>Design - por LR</u>
  * igual???	
* <u>Design - por bode</u>
  * escolhe-se KC, normalmente pela especificações de erro de regime permanente.
  * Se for dado zeta, ache a margem de fase $M_\phi$
  * por segurança, acrescentamos alguns graus
  *  
  * determinar alpha (o quanto Tz está acima de Tp)
  * Tz deve ser entre 0 e 1 (senão você fez um compensador em avanço, rsrs)

### Lead-Lag

* Ou *Compensador em avanço-atraso*

* bota os dois juntos

* Melhora a resposta transitória

* aumenta a estabilidade relativa

* equivalente à um filtro rejeita faixa

* <u>Design</u>
  
  * Os dois devem ser projetados juntos
  
  * dado por:
  
    ![image-20210413233806040](Images - Control Systems/image-20210413233806040.png)
  
  * primeiro projeta o em avanço (sem projtar o K) (focando na resposta transitória), depois o em atraso (focando na reposta permanente)
  
  * 


## PID

* fine tuning of PID controllers can be made on-site

* when the mathematical model of the plant is not known and there-
  fore analytical design methods cannot be used, PID controls prove to be most useful.

* (+) dá pra praticamente comprar pronto
  
* (-) não funciona muito bem para sistemas com muito tempo morto (daí a solução é controle digital?)
  
* zeros e polos complexos conjugados
  
* as vezes as constates são dada em termos de Tau (ou seja, o inverso de K)
  
* **Proportional controllers**
  
  * controle mais comples possível, apenas fecha a malha e dá um ganho
  * (+) Erro de regime diminui;
  * (+) Tempo de subida diminui;
  * (-) Amortecimento diminui??
  * (-) Sobressinal aumenta.
  * Kp = 1 = ganho nenhum
  
* **PI controllers**
  
  * é o controlador mais comum
  * mesmo em controladores PID, muitas vezes o D é desligado
  * (+) garante erro steady state nulo
  * (+) tempo de subida mais baixo
  * (-) settings time maior
  * (-) Less stable
  * (-) mais overshoot
  * processos de primeira ordem funcionam bem com PI
  * <u>efeitos da saturação</u>
    * quando a saída de controle é limitada à algum valor (não linearidade)
    * costumam ser bem ruins para o PI, 
  
* **PD controllers**
  
  * trás uma percepção de “erro futuro”, “previsão”
  * (-) deixa o sistema mais lento
  * (-) qualquer ruido deixa o sistema doido
  * (+) menos overshoot?
  * processos que possuem um integrador na malha aberta funcionam bem com PD
  * é usual botarmos um filtro passa faixa na entrada para diminuir essa sensibilidade
  * o zero do controlador PD está sempre a “direita” do pólo adicional. Desta forma, o controlador PD equivale
    a um Compensador de Avanço de Fase
  * <u>P-pseudo-D</u>
    * bota um filtro passa baixa
    * cut off high frequency noise
    * makes it more stable, less suceptible to noise
    * atua como um controlador de avanço de fase
    * the derivative term is $ps/(s+p)$
    * p = defines where you’ll filter frequency
    * é comum esse filtro ser de ordem mais alta
    * Normalmente, nos controladores comerciais, a posição deste pólo não é
      informada e nem pode ser modificada pelo usuário;
  
* **PID controllers**
  
  * o integrador zero o erro; pode ser usado sozinho, mas é instável
  
  * o derivador deixa mais estável; não pode ser usado sozinho
  
  * ==improper (non causal) transfer function?, with more zeros than poles?==
  
  * nada mais é do que um filtro ativo do tipo rejeita-faixa
  
  * Definida pela equação (pode estar no tempo ou não frequencia; em função de Tau ou dos polos/zeros):
  
    ![image-20210408090523987](Images - Control Systems/image-20210408090523987.png)
  
    ![image-20210408091039916](Images - Control Systems/image-20210408091039916.png)
  
    ![image-20201216081039655](Images - Control Systems/image-20201216081039655.png)
  
  * <u>PI+D</u>
  
    * moving the derivative control action to the feedback path,
    * faz com que variações bruscas no setpoint não causem caos
    * avoid the set-point kick phenomenon

### Implementation circuits

* Cuidado: o bloco de PID do simulink e scicos estão em termos de converter K, e não tau

* **ISA**

  * Mais “Acadêmica”
  
  * implementação comercial mais comum
  
  * usa 4 ampops
  
  * Uma regra prática para fixar a posição de “p” consiste em: $p=\beta/T_d$, com beta entre 8 e 10
  
  * ![image-20210414091338813](Images - Control Systems/image-20210414091338813.png)
  
    ![image-20210414091522568](Images - Control Systems/image-20210414091522568.png)
  
* **Série 2**

  * ?

    ![image-20210414092650438](Images - Control Systems/image-20210414092650438.png)

* **Série** **1**

  * mais difícil de separar as ações de controle;
  * é a menos usada
  * (-) difícil de sintonizar


  ![image-20201229165718908](Images - Control Systems/image-20201229165718908.png)
    
  ![image-20201229165736467](Images - Control Systems/image-20201229165736467.png)

* **Paralelo**

  * “Ideal”

  * parecida com a ISA (porém usa ganhos no lugar de constantes de tempo)
    
  * (-) usa mais componentes

    ![image-20210414092752615](Images - Control Systems/image-20210414092752615.png)

* 

* 


### Design methods

* **Choosing roots**
  * get FTMA and FTMF of the whole system (controller included)
  * chose the desired location of the poles
  * find Kp, Kd and Ki by manipulating the FTMF to have those poles 
  * if it’s not second order system, you may want to simplify
* **Design using root locus**
  * matlab can do it quite automatically: you put the requirements, it draw on the Root Locus the possible zones, then you just chose the parameters
  * setting time: vertical line
  * overshoot: V cone toward -inf
  * works well only for second order?

### Fine-tuning

* costuma precisar de bastante ajuste (*sintonia*); as vezes os fabricantes impoem algumas limitações nos ajustes que podem ser feitos
* most PID controllers are adjusted on-site
* some of the PID controllers may possess on-line automatic tuning
  capabilities.

#### **Ziegler–Nichols Rules**

* experimental approach

* when a mathematical model cannot be easily obtained

* can be done 100% in practice

* require the system to be already stable

* (-) é possível definir todos os parâmetros

* só para PID analógico?

* só use se dead time < tau/2

* ==consideraremos Overshoot máximo de 25% e  Razão de atenuação de 4:1 (1⁄4)?==

* Para aumentar a robustez à erros (de modelagem ou de medição), sugere-se diminuir os ganhos definidos
  com o método de Z-N.

* [Code - matlab implementation](http://faculty.washington.edu/lum/EducationalVideoFiles/Controls18/ZieglerNichols.m)

* **Método de malha aberta**

  * através da curva de reação	
  * só pode ser aplicado a plantas que não apresentem nem integradores,
    nem pólos complexos.
  *  
  * plot FTMA ao degrau
  * no ponto de inflexão, aproxime por uma reta e anote o parâmetros de ganho (K), constante de tempo (T), e delay time (L):
  
  ![image-20210415090241541](Images - Control Systems/image-20210415090241541.png)
  
  * parâmetros (ISA PID) a partir dessas medições:
  
    ![image-20210415091340214](Images - Control Systems/image-20210415091340214.png)
  
  * 
  
* **Método de malha fechada**

  * através da sensibilidade limite

  * o sistema precisa ter polos complexos

  * <u>practical proceadure</u>
    
    * starts only with proportional
    
    * increase it until oscilation (aaalmost unstable)
    
    * Anote o ganho $K_{cr}$ e o periodo $P_{cp}$
    
    * table to convert the terms (to ISA PID):
    
      ![image-20210415092606920](Images - Control Systems/image-20210415092606920.png)
    
  * <u>proceadura por lugar das raizes</u>

    * Quando utiliza-se o método do Lugar das Raízes, o ganho no qual o lugar das raízes cruza o eixo j é igual a Kcr e a frequência sobre o eixo j indica o valor de fcr
    
  * <u>procaeura por bode</u>
  
    * Por outro lado, ao utilizar-se os gráficos de Bode, determina-se a margem de ganho (MG) da planta na frequência de cruzamento de fase omega_phi. Desta forma, achamos:
      
      ![image-20210415101715803](Images - Control Systems/image-20210415101715803.png)

      ![image-20210415101738979](Images - Control Systems/image-20210415101738979.png)
  
      ```matlab
      clc();
      s=%s;
      numG= 150;
      denG= (s+1)*(s+2)*(s+10);
      
      %%%
      tmax= 10;
      it= tmax/1000;
      
      G= syslin('c',numG,denG);
      H= 1;
      FTMA= G*H;
      FTMF= G/.H;
      figure(1);
      clf(1);
      show_margins(FTMA,'bode');
      [mF,fG]= p_margin(FTMA);
      [mG,fF]= g_margin(FTMA);
      Kcr= 10^(mG/20);
      pi=%pi;
      wcr= fF*2*pi;
      
      Kp= 0.6*Kcr;
      Ki= Kp*wcr/pi;
      Kd= pi*Kp/(4*wcr);
      disp(Kd,'Kd= ',Ki,'Ki= ',Kp,'Kp= ');
      PID= Kp+(Ki/s)+(Kd*s);
      FTMFpid= (PID*G)/.H;
      PD= Kp+(Kd*s);
      FTMFpd= (PD*G)/.H;
      figure(2);
      clf(2);
      t= 0:it:tmax;
      step0=csim('step',t,FTMF);
      step1=csim('step',t,FTMFpid);
      step2=csim('step',t,FTMFpd);
      plot(t,step0,'b-',t,step1,'m-',t,step2,'g-');
      xgrid(33);
      xtitle('Step Response','Time (sec)','Amplitude');
      
      
      ```
      
      

#### Others

* Método CHR (Chien, Hrone, e Reswick): Método com regras de
  ajustes diferenciados, tanto para a mudança do valor de setpoint,
  quanto características regulatórias
* Método Cohen-Coon: Elaborado para processos com tempos mortos
  mais elevados (dead time menor que duas vezes a cte de tempo);
* Método ITAE (Integral do Módulo do Erro vezes o Tempo): Um dos
  melhores para sintonia dos parâmetros dos controladores;
* Método IMC (Modelo Interno): Determina-se os valores a partir do
  modelo de processo afim de obter o controlador mais adequado.
* **computational optimization approach** 
  * ?

## Preditor de smith

* Lida bem com tempo morto

## Linear quadratic regulator

# Controle digital

* (-) geralmente mais lento do que analógico
* Para encontrar a Eq recursiva a partir de um bloco H(z), iguale H(z)=Y(z)/E(z)=saída/entrada ; tire a transformada inversa (os termos Y que multiplicam z vão virar y(k+1)); isole y(k+1) em relação ao resto

## Realimentação

* a posição do amostrador muda as FTMT

* **Caso 1: medir analógico**

  * ?

    ![image-20210606184439872](Images - Control Systems/image-20210606184439872.png)
    $$
    FTMF = \frac{G(z)}{1+GH(z)}\\
    
    \text{onde:}\\ \\
    
    GH(z) = ZT\{ZOH(s)G(s)H(s)\}
    $$

* **Caso 2: medidor digital**

  * ?

    ![image-20210606184454428](Images - Control Systems/image-20210606184454428.png)
    $$
    FTMF = \frac{G(z)}{1+G(z)H(z)}\\
    
    \text{onde:} \\
    
    G(z) = ZT\{ZOH(s)G(s)\} \\
    H(z) = ZT\{ZOH(s)H(s)\}
    $$

* **Other annotations**
  * `control.minreal(<tf>)`: cancel pole-zeros; have minimum order
  * Se a realimentação é unitária, teremos que `Gz == control.minreal(Gz*Hz)`
  * FTMFz = control.feedback(Gz, Hz); Nesse caso o medidor é digital

## Métricas

* **Settling time**
  
  * Sistemas de segunda ordem com o mesmo ts possuem polos no mesmo raio
  
* **tempo de pico**
  
  * Sistemas de segunda ordem com o mesmo t_p possuem polos no mesmo ângulo
  
* **Erros**
  
  * Kp, Kv, Ka
  
  * ver no Slide 7 página 5 à 7 do Flabio
  
    ![image-20210609220754491](Images - Control Systems/image-20210609220754491.png)
    
    > os erros acima são para entrada ao degrau, rampa e parábola para sistemas tipo 0, 1 e 2,respectivamente

​	![image-20220509090746693](Images - Control Systems/image-20220509090746693.png)

## Anotações sobre sistemas discretos


* **Método dos resíduos**
  * Dado X(s), encontrar X(z)
  
  * é similar à aplicar `c2d`, mas...?
  
  * Uma forma alternativa é usando uma tabela de conversão
  
  * Procedimento:
  
    1. Encontre os polos de F(s)
  
    2. Para cada termo:
  
       1. Se multiplicidade for 1, substitua por:
  
          ![image-20220501172600323](Images - Control Systems/image-20220501172600323.png)
  
       2. Se multiplicidade for >1, substitua por:
  
          ![image-20220501172612744](Images - Control Systems/image-20220501172612744.png)
  
    3. Todos esses termos somados serão a F(z)

* **método da divisão polinomial**
  * Dado $X(z)$, encontrar $x[n]$
  * retorna os valores da função no tempo, e não uma equação analítica

* **Método das frações parciais**
  * Dado $X(z)$, encontrar $x[n]$ analiticamente
  
  * Ou seja, fazer a transformada inversa
  
  * :keyboard: control utils: `tf_expand_partial_format(Gz)`
  
  * Procedimento:
    1. eq auxiliar X(z)/z
    
    2. Expandir (==como fazer com matlab residue?==)
    
    3. Passar z pro outro lado
    
    4. aplicar a transformada Z inversa
    
       ![image-20220611190816185](Images - Control Systems/image-20220611190816185.png)


* **transformada Z para resolver equações recursivas**
  * dada a equação recursiva $x[n] = x[n-1] ...$, encontrar $x[n]$ em função apenas de $n$
  
  * :keyboard: control utils: `tf_reqeq_format(Gz)`
  
  * Procedimento:
    1. Aplicar a transformada Z (use a propriedade do deslocamento)
    
    2. Isolar X(z)
    
    3. Aplicar a transformada Z inversa
    
       ![image-20220611191144065](Images - Control Systems/image-20220611191144065.png)
  
* **Resolvendo equações recursivas semi determinadas**
  * semi deterinada = não são fornecidas condições iniciais suficientes (ex: grau 2 com 1 condição inicial)
  * como a equação é discreta, podemos resolver tempo-a-tempo
  * os primeiros termos serão não-determinados (dependerão de condições não dadas) serão índices negativos
  * como estamos assumindo causalidade, podemos considerar esses valores zero
  * portanto conseguimos calcular os termos não-determinados
  * é como fazer o "loop unrolling" dos primeiros termos
  
* **Equacionando o ZOH**

  * A função de transferência do ZOH é $G(s) = \frac{1}{s} - \frac{1}{s}e^{-sT} = \frac{1-e^{-sT}}{s}$

  * De G(s)=ZOH(s)G1(s) para G(z): 
    $$
    G(z) = (1-z^{-1})Z\left[\frac{G(s)}{s}\right] \\
    \text{e daí esse Z[...] podemos calcular pelo método dos resíduos}
    $$
    

* **Resposta de G(z) à uma entrada arbitrária**

  * tá certo isso???
  * lsim (recomendado) ou lfilter
  * See Ogata pg 42 (theory) and pg 63 (example)

   ```python
    x = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    T = 1
    
    # Usando lfilter
    n_z = Gz.num[0][0]
    d_z = Gz.den[0][0]
    y = lfilter([0 , n_z[0], n_z[1]], d_z, x)
       
    # Usando lsim
   t = np.linspace(0, tfinal, Kmax)
   y, t, x = lsim(FTMF, x, t);
   ```

* **A partir de G(z), implementar por equações recursivas**
  * aplica a transformada inversa, manipula
  * ver `tf_reqeq_format` e `tf_reqeq_exec`

* 

* 

* “A resposta ao impulso de um sistema G(z) é a transformada inversa de G(z)”

* a partir de G(z), como encontrar a resposta para uma entrada abitrária? lsim

*  

* como determinar o polo dominante? o que tiver a menor parte real; se tiver dois com partes imaginárias difernetes, pegue qualquer um;

* geralmente estamos interessados no pólo dominante da FTMF do sistema r*esultante* (após o controle)

*  

* We may use the z-transform of a transfer function to analyze the stability and transient response of a system

* A função de transferência analisa apenas à resposta ao estado nulo (considerando condições iniciais zero)

* Precision is the degree of exactness or discrimination with which a quantity is stated.

* The precision of the computer is limited by a finite word length.	

*  

*  

* **condição de ângulo e condição de móduolo**

  - Mecanismo matemático para verificar se um certo ponto de fato é um polo de malha fechada, a partir da malha aberta (na verdade, $1+G(z)H(z)$ ou $1+GH(z)$ dependendo se a realimentação é analógica ou digital)
  - Se o ponto z *de fato* for um polo de malha fechada, as condições de módulo e ângulo serão satisfeitas
  - FTMA avaliada no ponto z

## Técnicas de projeto

*  **Método do Lugar das Raízes**
  
  * ?
  
* **Resposta em frequencia**

  * <u>plano w</u>

    * transformação bilinear do plano z

    * fica similar à um plano $s$, mas ainda discreto

      ![image-20210702140505237](Images - Control Systems/image-20210702140505237.png)

    * frequencia fictícia $v$

      ![image-20210702141054611](Images - Control Systems/image-20210702141054611.png)

  * <u>procedimento</u>

    * transforme para w
    * identifique erro estático, margem de fase e margem de ganho
    * determine o ganho K para atender o erro estático desejado
    * ?
    * transforme de volta para z

  * **metódo analítico**

    * ?

  * **método da conversão**

    * discretizamos um controlador contínuo

    * meodo da conversão para digital: faça manualmente a substituição; talvez dê pra automatizar por usando a função c2d, que também usa método bilinear; Não fncona tão bem, especialmente para alguns sistemas especoficas

    * métodos: {"gbt", "bilinear", "euler", "backward_diff",) –
    
      “zoh”, “matched”} 
    
  * **?**
  
    * para controlador em avanço
  
    * escolha um ponto z1 para ser um pólo dominante de malha fechada (z1 = localização no plano z do pólo dominante; geralmente nos só sabemos os pólos em s, então fazemos a conversão $z=e^{Ts}$); analisar o módulo e o ângulo desse ponto z costuma ter várias propriedades no sistema de controle
  
    * escolha alfa de forma arbitrária (cancelar um polo é uma boa escolha), escolha beta para atender a condição de angulo e condição de módulo
  
    * só dá pra cancelar o polo se ele for real; se for complexo, use um controlador com uma ordem maior, com o numerador do controlador cancelando um polo complexo da planta e o denominador do controlador sendo 1 em (z-1) para ter erro zero e um em (z+beta)
    
      ![image-20210926164851138](Images - Control Systems/image-20210926164851138.png)	

* **?2**
  * dado zeta e wn desejados, encontrar o ponto z desejado
  - determinar, pela condição de módulo e ângulo, qual é o ângulo que o controlador deve fornecer
  - escolher a equação do controlador (k*(z-alpha)/(z-beta), nesse caso)

## Projeto em espaço de estados

* Método: escolha as métricas desejadas, encontre os dois polos dominantes de segunda ordem que satisfazem essas métricas, escolha os outros polos (de forma que não seja dominantes, ou seja, que possuam parte real maior do que os dois escolhidos)

* até agora estudamos sistemas em EE contínuos. Ao implementar no microcontrolador, discretizamos apenas o integrador (o resto são apenas multiplicação de matrizes)

* **Observador**

  * Not a controler, but something that is needed for most state space controlers
  * Estimar o estado X (em um instance) olhando apenas para as saídas Y (em vários instantes)
  * Regra prática: projete o observador tal que o seu tempo de resposta seja 5 vezes menor do que o tempo de resposta do sistema desejado
  * $\tilde {\bold X}$: estado estimado
  * a diferença entre $\bold X$ e $\tilde {\bold X}$ tende a zero no infinito
  * 
  * Ordem plena: estimar todos os estados
  * Ordem mínima: estimar só alguns estados
  * 
  * Separation principle: the full-state controller and the observer are designed indepently
  *  
  * Kalman Filter
    * Also known as *linear quadratic estimation (LQE)*
    * considers the measurements to be noisy
    * assumes that both the disturbance and noise are zero-mean Gaussian processes with known covariances
    * `Kf = lqe(A,Vd,C,Vd,Vn)`
    * have the same idea as any observer: receives the input and the output of our system, and returns an estimation of the states

* **Observability test**

  * it is possible to estimate any state in $R^n$ from a time-history of the measurements y(t)
  * Interestingly, the observability criterion is mathematically the dual of the controllability
    criterion. In fact, the observability matrix is the transpose of the controllability matrix for
    the pair $(A^T , C^T )$
  * se $rank([C^*|A^*C^*|...|(A^*)^{N-1}C^*])==N$
  * lembrado que os asteriscos significa <u>transposta conjugada</u>
  * se a matriz de observabilidade possui posto/rank igual a N, sendo N o número de estado (ordem de $\bold A$) 

* **Controlability test**

  * Also know as Popov–Belevitch–Hautus (PBH) test?
  * implies that any state in $R^n$ is reachable in a finite time with some actuation signal u(t)
  * in practice is not that useful, because the actuation signal may be extremely large
  * also implies arbitrary eigenvalue placement
  * se $rank([B|AB|...|A^{N-1}B]) == N$ 
  * <u>teste de controlabilidade da saída</u>
    * se é possível alcançar qualquer Y atuando apenas em u(t)
    * se $rank([CB | CAB | CA^2B | ... | CA^{N-1}B]) == N$ 
    * a controlalibidade PBH não é necessária nem suficiente para a controlabilidade da saída

* 

* 

* **Pole placement - Acker**

  * ?

  * control law $u = −Kx$

  * Permite escolher todos os polos, e não apenas os dois desejados. 

  * <u>Garantindo erro zero</u>

    * se a planta já tem um integrador (polo em s=0), beleza
    * se não, temos que adicionar um artificialmente no nosso controle (aí passamos a ter um proporcional-integral, basicamente)
    * Fazemos o projeto considerando duas matrizes expandidas $\hat A$ e $\hat B$
    * `K_hat = acker(A_hat, B_hat, eigenvalues)`
    * o integrador adicional fica após o sinal de erro, e deve ser incluido nas equações recursivas
    * o sinal de controle fica $u=-KX + K_i(\int y -r)$?

  * <u>Conjunto controlador+observador</u>

    * ?

      ![image-20220716211732605](Images - Control Systems/image-20220716211732605.png)

* **Symmetric root-locus**

* **Linear quadratic regulator**

  * Same control law as the Pole Placement
  * creates a cost function, that includes a term for the “cost of control” (amplitude of control signal?), and apply optimization techniques to it
  * `Kr = lqr(A,B,Q,R)`


## Considerações práticas

* alguns processadores (por exemplo, linha C2000 da texas) possuem periféricos específicos para implementar equações de controle
* a frequencia do PWM deve  ser multiplo inteiro do que a taxa de amostragem. O PWM deve ser 5~10 vezes mais RAPIDO do que a taxa de amostragem
* **problemas de taxa de amostragem mais alta que o necessário:**
  * a resolução (número de dígitos) dos parâmetros do controlador costumam ter que ser maiores
  * os polos costumam ficar mais próximos da instabilidade (módulo maior que 1 no plano z), o que nos dá menos margem para error
  * os delays do próprio processador tornan-se mais significativos, o que vai aumentar a diferença entre a teoria e a prática

# Outras técnicas

* **controle avançado**
  * Geralmente trabalha mais no domínio do tempo, ao invés da frequenia
  * Geralmente MIMO (multi input mult output)?
  * geralmente estocástico (descrito por características estatísticas), ao invés de determinístico
* **Model predictive control?**
  * ?