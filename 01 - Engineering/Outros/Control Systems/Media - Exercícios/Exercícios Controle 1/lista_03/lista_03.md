

## 1)

$$
FTMF = \frac{k}{s^3 + 3s^2 + (2+k)s + 5k}
$$


$$
FTMA = \frac{K(s+5)}{s(s+1)(s+2)}
$$
Trechos do eixo real pertencentes ao LR:

![image-20210410200240298](Images - lista_03/image-20210410200240298.png)

Limites das estabilidade pelo critério de routh-hurwitz:

![image-20210410200831984](Images - lista_03/image-20210410200831984.png)

Ponto de ruptura:

![image-20210410200921145](Images - lista_03/image-20210410200921145.png)

Traçado final pelo Scilab:

![img](Images - lista_03/9e8e8bb1-0862-4b25-9e73-010db3d9e69f.png)

## 2)

Traçando as componentes individualmente:

![image-20210410221714230](Images - lista_03/image-20210410221714230.png)

Traçando as componentes somadas:

```matlab
s= %s;
num=800*(s+1);
den= s*(s+4)*(s^2+14*s+100);
G= syslin('c',num,den);
clf();
bode(G,0.1,150,'rad');
bode_asymp(G,0.1,150);
```

![img](Images - lista_03/18c62c99-f83c-4f87-a393-91e2aecf19a2.png)

```matlab
s=%s;
num=800*(s+1);
den= s*(s+4)*(s^2+14*s+100);
FTMA= syslin('c', num, den);

[mf ,fg]=p_margin(FTMA)
[mg,ff]=g_margin(FTMA)
printf('Margem de fase: %.2f graus\n', mf)
printf('Frequência onde o ganho é 1: %.2f Hz\n', fg)
printf('Margem de ganho: %.2f dB\n', mg)
printf('Frequência onde a fase é 180º: %.2f Hz\n', ff)
show_margins(FTMA);
```

![img](Images - lista_03/50198fe6-c539-41fd-b312-e077438f5172.png)

Margem de fase: 56.52 graus
Frequência onde o ganho é 1: 1.02 Hz
Margem de ganho: 8.53 dB
Frequência onde a fase é 180º: 1.89 Hz

## 3)

![image-20210410221733301](Images - lista_03/image-20210410221733301.png)



## 4)



## 5)

$$
FTMA = \frac{250(s+10)}{s(s+1)(s^2+50s+2500)}
$$

$$
FTMF = \frac{250(s+10)}{x^4+51x^3+300x^2+250x+2500}
$$

Cujas raízes são (calculadas pela ferramenta [wolframalpha](https://www.wolframalpha.com)):

![image-20210410224410013](Images - lista_03/image-20210410224410013.png)

Aproximando pelas duas raízes complexas, temos:
$$
FTMF = \frac{250}{s^2 + 0.48s +7.34}
$$
Dos quais obtemos $\zeta=0.088$ e $w_n=2.7$

----

Gráficos para a FTMA não aproximada:

![https://cloud.scilab.in/static/tmp/15215a8c-8d00-4147-ab28-f60522ae1c88.png](Images - lista_03/15215a8c-8d00-4147-ab28-f60522ae1c88.png)

![image-20210407210611960](Images - lista_03/image-20210407210611960.png)

![image-20210407211024273](Images - lista_03/image-20210407211024273.png)

Margem de fase: 55.36 graus
Frequência onde o ganho é 1: 0.13 Hz
Margem de ganho: 52.23 dB
Frequência onde a fase é 180º: 7.21 Hz



## 6)



## 7)

Traçando as componentes indidualmente:

![image-20210411012626299](Images - lista_03/image-20210411012626299.png)

Traçado computacional:

![image-20210411011310937](Images - lista_03/image-20210411011310937.png)

![img](Images - lista_03/ae3f0a76-5d41-499c-bab4-c26697bf63e2.png)

Margem de fase: 61.21 graus
Frequência onde o ganho é 1: 0.12 Hz
Margem de ganho: 18.84 dB
Frequência onde a fase é 180º: 0.50 Hz



Para essa margem de fase, podemos aproximar por um sistema de segunda ordem com $\zeta \approx 0.63$

![image-20210411010859058](Images - lista_03/image-20210411010859058.png)

E, dado esse zeta e $f_G$ = 0.12 Hz, temos que $w_n=0.12/0.7=0.17 Hz$:

![image-20210411012316269](Images - lista_03/image-20210411012316269.png)