

## Introdução

Este documento descreve a elaboração de um controlador para o sistema:
$$
G(s)H(s) = \frac{s+10}{s 2+2s+1}
$$
Deseja-se atender os requisitos de $e_{ss}(degrau)=5\%$, $M_\phi=65^\circ$ e $t_{s5\%}=500ms$.

Analisaremos primeiramente as principais características do sistema original, na sequência propondo **compensador por avanço de fase** que leve o sistema à satisfazer os requisitos e, por fim, analisaremos o comportamento do sistema compensado. Sempre que for conveniente, incluiremos junto às figuras/cálculos os códigos para Scilab necessários para reproduzir os resultados, e os códigos na íntegra foram fornecidos juntamente à este relatório. O restante do documento está estruturado conforme segue:

[TOC]

## Desenvolvimento

### Análise do sistema original

A partir do diagrama de Bode (Figura 1), observa-se que o sistema possui margem de fase de $53.12 ^\circ$, na frequência de $4.9 Hz = 3.079 \text{ rad/s}$. Vemos que a fase nunca chega à $-180^\circ$ e, portanto, o sistema é sempre estável. 



<img src="Images - prova_02/image-20210417003611971.png" alt="image-20210417003611971" style="zoom:80%;" />

> Figura 1 - Diagrama de Bode do sistema original

```matlab
s=%s;
num=s+10;
den= s^2 + 2*s + 1;
FTMA= syslin('c', num, den);

[mf ,fg]=p_margin(FTMA)
[mg,ff]=g_margin(FTMA)
show_margins(FTMA);
```

As conclusões tomadas pelo diagrama de Bode podem ser confirmadas pelo diagrama de Nyquist (Figura 2), e também pelo Lugar das Raízes (Figura 3), onde para nenhum $K$ há polos no semiplano direito.

<img src="Images - prova_02/image-20210416232812716.png" alt="image-20210416232812716" style="zoom:80%;" />

> Figura 2 - Diagrama de Nyquist do sistema original

```matlab
nyquist(FTMA, 1E-6, 1E+6);
plot(-1,0, 'x')
show_margins(FTMA, 'nyquist');
```
<img src="Images - prova_02/image-20210416225019811.png" alt="image-20210416225019811" style="zoom:80%;" />

> Figura 3 - Lugar das raízes do sistema original

```matlab
evans(FTMA,500);
l=gca();
l.data_bounds=[-25 0 -10 10];
```

A partir da resposta ao degrau, conseguimos obter o seguintes índices: 

$$
t_r = 0.505-0.074 = 0.431s
$$

$$
t_p = 0.948s
$$

$$
M_p = 21.7\%
$$

$$
t_{s5\%}= 1.483s
$$

<img src="Images - prova_02/image-20210417111414175.png" alt="image-20210417111414175" style="zoom:80%;" />

> Figura 4 - Resposta ao degrau do sistema original
```matlab
t= linspace(0,6,501);
step= csim('step',t,FTMF);
plot2d(t,step);
```

Percebe-se claramente que o sistema original não atende os requisitos, sendo necessário então projetar um sistema de controle para tal.

### Projeto do compensador

Objetivando utilizarmos a metodologia de projeto de compensadores por Lugar das Raízes apresentada em sala de aula, **aproximaremos o sistema** por um sistema de com dois polos e nenhum zero.

O sistema desejado deve ter $\zeta=0.707$ (a partir do ábaco da Figura 5), e portanto $\omega_n=  \frac{3}{\zeta t_{s5\%}} = 8.48 \text{ rad/s}$.

<img src="Images - prova_02/image-20210417000951288.png" alt="image-20210417000951288" style="zoom:67%;" />

> Figura 5 - Ábaco de zeta

Já podemos equacionar o sistema desejado e calcular o parâmetro proporcional $Kc=1.9$, da seguinte forma:

```matlab
// Sistema atual
s=%s;
numG=s+10;
denG= s^2 + 2*s + 1;

// Parâmetros desejados
ess=0.05;
omega= 8.486563;
zeta= 0.707;

// Parâmetro proporcional
Kc = (1-ess)/(10*ess);
disp(Kc, 'Kc= ');

// Função de transferência
G= syslin('c',numG,denG);
H= 1;
FTMF= G/.H;
```

Na sequência calculamos o ponto complexo $s_1$ no plano $s$ com os parâmetros calculados acima, e o valor valor complexo de $G(s)H(s)$ nesse ponto:

```matlab
s1= -(zeta*omega)+((omega*(1-zeta^2)^0.5)*%i);
[mS,tetaS]= polar(s1);
tetaS= real(tetaS);
Gs1= horner((G*H), s1);
[mG,tetaG]=polar(Gs1);
tetaG= real(tetaG);
disp((180*tetaS/pi), 'tetaS= ',mS, 'mS= ');
disp((180*tetaG/pi), 'tetaG= ',mG, 'mG= ');
```

Por fim podemos calcular o valor do polo e do zero do compensador:

```matlab
Tz= real((sin(tetaS)-(Kc*mG*sin(tetaG-tetaS)))/(Kc*mG*mS*sin(tetaG)));
Tp= real(-((Kc*mG*sin(tetaS))+(sin(tetaG+tetaS)))/(mS*sin(tetaG)));
disp(Tp,'Tp [s]= ',Tz,'Tz [s]= ');
```

Entretanto, observamos pela resposta ao degrau do sistema com esse compesador (Figura 6) que o *settling time* **não atende** as especificações, provavelmente pela aproximação por um sistema de segunda ordem. 

<img src="Images - prova_02/image-20210417124536342.png" alt="image-20210417124536342" style="zoom:80%;" />

> Figura 6 - Resposta ao degrau com o primeiro compensador (em magenta) e do sistema original (em azul)

Desse forma, **repetiu-se o procedimento acima** utilizando $w_n=11$, um valor em que observou-se empiricamente que o sistema respondia de forma satisfatória. Com esse valor, obteve-se $t_{s5\%}=0.406s$, conforme pode ser visto na comparação da Figura 7.

<img src="Images - prova_02/image-20210417125341025.png" alt="image-20210417125341025" style="zoom:80%;" />

> Figura 7 - Comparação dos dois compensadores. Azul=resposta original, magenta=compensador com $w_n=8.48$, verde = compensador com $w_n=11$

O compensador final, com $w_n=11$, é um compensador em avanço de fase e corresponde à um filtro passa altas, como vemos no diagrama de Bode da Figura 8. Sua função de transferência é dada por:
$$
G_{compensador} = 1.9\frac{2.78s+1}{0.452s + 1}
$$

```matlab
Tz= real((sin(tetaS)-(Kc*mG*sin(tetaG-tetaS)))/(Kc*mG*mS*sin(tetaG)));
Tp= real(-((Kc*mG*sin(tetaS))+(sin(tetaG+tetaS)))/(mS*sin(tetaG)));
disp(Tp,'Tp [s]= ',Tz,'Tz [s]= ');
```

<img src="Images - prova_02/image-20210417134856674.png" alt="image-20210417134856674" style="zoom:80%;" />

> Figura 8 - Diagrama de Bode do compensador final.

O controlador projetado pode ser implementado pelo circuito da Figura 9, com os componentes calculados conforme segue:

$C_1= 10uF$ (valor arbitrado)

$C_2 = 1uF$ (valor arbitrado)

$R_1 = \frac{2.78}{10\times10^{-6}} = 278k\Omega$

$R_2 = \frac{0.452}{1\times10^{-6}} = 452k\Omega$

$R_3=10k\Omega$ (valor arbitrado)

$R_4 = 1.9\frac{R_1R_3}{R_2}=11.68k\Omega$

<img src="Images - prova_02/image-20210417143058132.png" alt="image-20210417143058132" style="zoom:65%;" />

<img src="Images - prova_02/image-20210417143201792.png" alt="image-20210417143201792" style="zoom:67%;" />

> Figura 9 - Circuito de controle


### Análise do sistema compensado

Na seção anterior abordou-se superficialmente o desempenho do sistema, objetivando uma escolha adequada do um controlador, e nesta seção analisaremos rigorosamente o sistema compensado.

No lugar das raízes (Figura 10) podemos ver que o polo e zero do compensador foram inseridos à direita do sistema original, de forma a “puxar o lugar das raízes para a direita” e tornar o sistema mais rápido, ainda sem tornar-lo instável.

<img src="Images - prova_02/image-20210417135025086.png" alt="image-20210417135025086" style="zoom:70%;" /> <img src="Images - prova_02/image-20210417135053165.png" alt="image-20210417135053165" style="zoom:70%;" />

> Figura 10 - Lugar das raízes do sistemas compensado, com zoom próximo da origem. 

A margem de fase de $70.24^\circ$ pode ser observada tanto a partir do diagrama de Bode (Figura 11) quanto a partir do diagrama de Nyquist (Figura 12). Houve uma melhoria de $70.24^\circ-53.12 ^\circ=17.12^\circ$, atendendo satisfatoriamente esse requisito do projeto.


![image-20210417135234719](Images - prova_02/image-20210417135234719.png)

> Figura 11 - Diagrama de Bode do sistemas compensado, com zoom próximo da origem. 

<img src="Images - prova_02/image-20210417135119998.png" alt="image-20210417135119998" style="zoom:67%;" /> <img src="Images - prova_02/image-20210417135140612.png" alt="image-20210417135140612" style="zoom:67%;" />

> Figura 12 - Diagrama de Nyquist do sistemas compensado, com zoom próximo da origem. 

A partir da resposta ao degrau (Figura 13 e Figura 14) observa-se que o compensador projetado responde melhor que o sistema original e que o sistema compensado apenas com o ganho proporcional $K_c$, tanto no regime transitório quanto permanente. Observa-se também que todos os requisitos foram cumpridos, sendo os índices de desempenho os seguintes:

$$
t_r = 0.108-0.008 = 0.1s
$$

$$
M_p = 13.7\%
$$

$$
t_p = 0.234 s
$$

$$
t_{s5\%}=0.406s
$$

<img src="Images - prova_02/image-20210417135649363.png" alt="image-20210417135649363" style="zoom:80%;" />

> Figura 13 - Resposta ao degrau do sistema compensado

<img src="Images - prova_02/image-20210417140300477.png" alt="image-20210417140300477" style="zoom:80%;" />

> Figura 14 - Comparação das respostas ao degrau. Azul=resposta original, magenta=apenas controlador proporcional; verde = compensador com $w_n=11$;

## Conclusões

Foram necessários ajustes heurísticos para obter um compensador satisfatório, mas foi possível atender todos os requisitos.

O processo de análise e design foi integralmente exposto, bem como as implementações em Scilab necessárias para reproduzir o trabalho.

