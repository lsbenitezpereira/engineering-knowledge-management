**qual foi o erro:** eu estava multiplicando a FTMF do sistema original pelo compensador, mas o compensadr deveria multiplica a FTMA

# 1

traçando o DB para $Kc*H*G$, obtem-se Mg=49.32º

Para obtermos a Margem de Fase desejada faltam $\phi=65-49.32=15.68$. Por questão de segurança, adicionamos mais 10°, de forma que phi=25.68º

$alpha = \frac{1+sen(phi)}{1-sen(phi)}=2.53$

ganho adicional = $10 \log(alpha)=4.03$, ganho esse que se dá na frequência w=3.41 rad/s (obtido a partir do bode abaixo)

$T = \frac{1}{3.41\sqrt \alpha}=0.1844s$

![image-20210417003232898](Images - prova_02/image-20210417003232898.png)

## Reustado

 Mg ficou melhor do que 65, mas a resposta temporal não



![image-20210417013716971](Images - ANOTAÇÊOS ERRADAS/image-20210417013716971.png)





# 2

desejado -115

 Para satisfazer a margem de fase especificada, o Diagrama de
Bode de KC.G(s) mostra uma wG compensada de 1.96 rad/s
 O ganho nessa frequencia é 11.99 dB, portanto a=10^{-11.99/20}=0.251

![image-20210417015459396](Images - ANOTAÇÊOS ERRADAS/image-20210417015459396.png)