# Problema 1

Resolução analítica:

xxx

Verificou-se o resultado com o Xcos e com a ferramenta online Symbolab:

![image-20201219235550819](Images - lista_01/image-20201219235550819.png)

![image-20201219235652812](Images - lista_01/image-20201219235652812.png)

# Problema 2

Empaquei na resolução manual 

montei um única equação, não consegui simplificar o suficiente para aplicar a ILT

Quarta ordem, raízes complexas



achou-se as raízes usando esta [ferramenta online](https://www.mathportal.org/calculators/polynomials-solvers/polynomial-roots-calculator.php)

resolveu-se a equação com o auxilio do octave: 

`s = 0; (10*s + 5)/(s*(s+0.61)*(s+0.19-3.11i)*(s+0.19 + 3.11i))`

# Problema 3

OK

# Problema 4

OK


# Problema 5



$FTMF(s)=\frac{G_1(s)G_2(s)}{1+G_1(s)G_2(s)*H(s)}$

Resposta ao impulso:

$I(s)=\frac{G_1(s)G_2(s)}{1+G_1(s)G_2(s)*H(s)}R(s)$

Resposta no tempo:

$i(s)=LPI\left\{\frac{G_1(s)G_2(s)}{1+G_1(s)G_2(s)*H(s)}R(s)\right\}$

