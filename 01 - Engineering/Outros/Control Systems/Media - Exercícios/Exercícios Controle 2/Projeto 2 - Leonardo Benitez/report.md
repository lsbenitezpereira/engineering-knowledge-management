Aluno: Leonardo Santiago Benitez Pereira

[TOC]

# Identificação da planta

A partir do circuito fornecido, obteve-se a sua representação em espaço de estados:
$$
A = \begin{bmatrix}
  0 &  1/(R2*C1)\\
 -1/(R1*C2) & (-R1-R2)/(R1*R2*C2)
\end{bmatrix}
\\ \\
B = \begin{bmatrix}
  0\\
 1/(R1*C2)
\end{bmatrix}
\\ \\
C = \begin{bmatrix} 1 & 0 \end{bmatrix}\\ \\
D = \begin{bmatrix} 0 \end{bmatrix}
$$
Simulando o sistema, obtem-se:

![image-20220716184445423](Images - report/image-20220716184445423.png)

![image-20220716184650045](Images - report/image-20220716184650045.png)

![image-20220716184701960](Images - report/image-20220716184701960.png)

O erro em regime permanente é zero, como observa-se nas simulações acima e pela equação abaixo.

![image-20220716192908934](Images - report/image-20220716192908934.png)

# Definição dos requisitos

A partir dos critérios expôstos nos requistos, definiu-se os valores de $t_{s5\%}$ e $M_p$ que deveriam ser atendidos no projeto. Adicionou-se também uma margem de segurança em ambos os valores. Sendo assim, os valores utilizados como base para os controladores projetados são:

| Parâmetro                        | Valor |
| -------------------------------- | ----- |
| $t_{s5\%}$ desejado (original)   | 33 ms |
| $t_{s5\%}$ desejado (com margem) | 28 ms |
| $M_p$ desejado (original)        | 16 %  |
| $M_p$ desejado (com margem)      | 15 %  |

Com base nesses requisitos, O período de amostragem foi escolhido como sendo $T=1.86ms$, 15 vezes menor do que o tempo de subida desejado $ts5=0.028s$

Ou seja, o sistema deve possuir os autovalores: $[(-107.14+177.42j), (-107.14-177.42j), -535.71]$

# Projeto do controlador

Primeiramente, observou-se a matriz de controlabilidade:
$$
\begin{bmatrix}
    \bold A && \bold B \\
    \bold C && 0
\end{bmatrix}
= 
\begin{bmatrix}
    0 && 712.25 && 0 \\
    -43.25 && -124.95 &&   43.25 \\
    -1 && 0 && 00
\end{bmatrix}
$$
que possui posto 3 e, portanto, o sistema é controlável.

Escolhe-se realizar o projeto por alocação de polos, considerando um servossistema do tipo 1, considerando que não há um integrador na planta.

Primeiro obteve-se as matrizes expandidas:

![image-20220716185018064](Images - report/image-20220716185018064.png)

E por meio do método de Acker obteve-se a matriz de ganhos $\hat K$:

![image-20220716185045643](Images - report/image-20220716185045643.png)
$$
\text{Onde:}\\
\hat K = \begin{bmatrix} 4.12 &   14.45 & -747.05 \end{bmatrix}
$$
Obteve-se e simulou-se então o sistema resultante, o qual se percebe que atende os requisitos:

![image-20220716185428687](Images - report/image-20220716185428687.png)

![image-20220716185255920](Images - report/image-20220716185255920.png)

![image-20220716185306024](Images - report/image-20220716185306024.png)

![image-20220716185346187](Images - report/image-20220716185346187-16580084274091.png)

# Projeto do observador

Primeiramente, observou-se a matriz de observabilidade:
$$
\begin{bmatrix}
    \bold C^* | \bold A^* \bold C^*
\end{bmatrix}
= 
\begin{bmatrix}
   1 && 0 \\
   0 && 712.25
\end{bmatrix}
$$
que possui posto 2 e, portanto, o sistema é completamente observável.

Escolheu-se um observador de ordem plena, calculado pelo método de Acker. Para tanto, aloca-se os polos para o sistema dual:

![image-20220716185801079](Images - report/image-20220716185801079.png)

de tal forma que o observador possua um tempo de acomodação 5 vezes mais rápido que o sistema desejado.

Resultando no seguinte sistema:

![image-20220716185824033](Images - report/image-20220716185824033.png)

![image-20220716193835255](Images - report/image-20220716193835255.png)
$$
\text{Onde:}\\
K_e = \begin{bmatrix} 4160.76 \\ 23352.99 \end{bmatrix}
$$
Simulando o sistema, percebe-se que o observador aproxima de forma satisfatória a dinâmica real do sistema:

![image-20220716190304489](Images - report/image-20220716190304489.png)

![image-20220716190317501](Images - report/image-20220716190317501.png)

# Conjunto observador+controlador

A resposta do sistema completo é dada por:

![image-20220716190412932](Images - report/image-20220716190412932.png)

![image-20220716194055867](Images - report/image-20220716194055867.png)

Obteve-se e simulou-se então o sistema resultante, o qual se percebe que atende os requisitos:

![image-20220716190437111](Images - report/image-20220716190437111.png)

![image-20220716190446645](Images - report/image-20220716190446645.png)

![image-20220716190454870](Images - report/image-20220716190454870.png)

![image-20220716190718119](Images - report/image-20220716190718119.png)

Na simulação com equações recursivas, para a entrada sendo uma onda quadrada de 1V a 1.5V, foi possível observar que o sinal de controle possui um valor mínimo de 0V e máximo de 1.7V. Dessa forma, o sistema atende todos os requisitos do projeto.

# Implementação em microcontrolador

## Fluxograma

A implementação em microcontroladores se dá a partir das equações recursivas, com o sinal de feedback sendo medido por um ADC e o sinal de controle sendo enviado para a planta. 

Como o período de amostragem é fixo, é usual utilizar uma interrupção ativada por um timer para controlar a execução do código. A cada execução da interrupção, o buffer de amostras passadas é atualizado (os elementos de $k-1$ vão pra $k-2$ e assim por diante) e uma iteração da equação recursiva é calculada.

O sinal de saída deve respeitar as limitações de hardware em questão, incluindo os limites de tensão e a corrente drenada do microcontrolador. Portanto pode ser necessário limitar o sinal (de 0V a 5V, por exemplo) e utilizar um driver entre o microcontrolador e a planta.

Ainda mantendo a generalidade da escolha de hardware e linguagem de programação, a implementação por ser esquematizada pelo seguinte fluxograma:

![image-20220716190907273](Images - report/image-20220716190907273.png)

## Código

O hardware escolhido foi um Arduino Mega, que possui um microcontrolador ATmega2560, e a implementação em código C seguiu o fluxograma apresentado anteriormente.

O código está disponível na íntegra no repositório online https://github.com/LeonardoSanBenitez/arduino-discrite-control.

A implementação não foi testada em hardware real devido ao curto tempo de implementação do projeto.

# Referências

Batista, Flavio (2021). Notas de aula de Sistemas de Controle II. Instituto Federal de Educação, Ciência e Educação de Santa Catarina.

Silveira, Jony (2020). Notas de aula de Sistemas de Controle I. Instituto Federal de Educação, Ciência e Educação de Santa Catarina.

Katsuhiko, Ogata (1995). Discrite-Time Control Systems. Prentice-Hall.

