Aluno: Leonardo Santiago Benitez Pereira

[TOC]

# Identificação da planta e requisitos

Obteve-se a função de transferência do sistema contínuo de segunda ordem a partir do ensaio experimental e das equações dadas, obtendo-se $\zeta=0.3663$ e $w_n=175.8$, portanto:
$$
G(s) = \frac{3.092 \times 10^{4}}{s^2 + 128.8 s + 3.092 \times 10^{4}}
$$
Pode-se perceber pela simulação do sistema que este é similar à planta real dada.

![image-20210722212454409](Images - relatório/image-20210722212454409.png)

A partir dos critérios expôstos nos requistos, definiu-se os valores de $t_{s5\%}$ e $M_p$ que deveriam ser atendidos no projeto. Adicionou-se também uma margem de segurança em ambos os valores, que também serve o papel de experimentação didádica para observarmos quão rápida pode ser a resposta desse sistema. Sendo assim, os valores utilizados como base para os controladores projetados são:

| Parâmetro     | Valor     |
| ---- | ---- |
| $t_{s5\%}$ desejado (original) | 33 ms |
| $t_{s5\%}$ desejado (com margem) | 7 ms |
| $M_p$ desejado (original) | 16 % |
| $M_p$ desejado (com margem) | 15 % |

Com base nesses requisitos, o período de amostragem foi escolhido como sendo $T=0.0004666$, 15 vezes menor do que o tempo de subida desejado $t_{s5\%}$=0.007.

# Projeto de controlador digital no plano z pelo método do lugar das raízes

Escolheu-se primeiramente um ponto $z_1$ que deveria ser um polo dominante de malha fechada:

<img src="Images - relatório/image-20210721213424486.png" alt="image-20210721213424486" style="zoom:67%;" />

Considerado o controlador da equação abaixo, é possível determinar $\alpha_1$ e $\alpha_2$ de forma a cancelarem os polos do bloco $G(z)$ e então determinar $\beta$ de forma a satisfazer as condições de módulo ($\left|C(z)G(z)\right|_{z=z1} = 1$) e de ângulo ($\angle C(z)G(z)_{z=z1} = 180º$):

$$
C(z) = k\frac{z^2 + \alpha_1 + \alpha_2}{z^2 + (\beta-1)z -\beta}
$$
Dessa forma o polo desejado é $z_1 = 0.7742 + 0.2662j$, e o controlador projetado é:
$$
C(z) = \frac{18.65 z^2 - 36.1 z + 17.57}{z^2 - 1.61 z + 0.61}, \text{    dt=0.0004666} \\
\beta = -0.61\\
K = 18.65 \\
\\
\text{Coeficientes do numerador: } = [18.65, -36.1, 17.57] \\
\text{Coeficientes do denominador: } = [1, -1.61, 0.61]
$$
A simulação do sistema comprova que os requisitos do projeto foram cumpridos com sucesso, conforme vê ne comparação:

![image-20210722222113891](Images - relatório/image-20210722222113891.png)

![image-20210722214337040](Images - relatório/image-20210722214337040.png)

A métrica de erro ao degrau em regime permanente pode ser calculada de por dois métodos diferentes: analiticamente pelo teorema do valor final (equação abaixo), ou numericamente via simulação por tempo suficiente. Ambos os métodos foram implementadas e levaram ao mesmo resultado de erro nulo ao degrau.
$$
e_{ss} = \lim_{z \rightarrow 1} \left(\left(1 - z^{-1}\right)*\left(\frac{1}{1+FTMA}\right)*\frac{z}{z-1}\right) = 0
$$

```python
((1 - control.tf([1], [1, 0], T))*(1/(1+FTMA))*control.tf([1, 0], [1, -1], T)).minreal()(1)
```



Pode-se observar que os polos de malha fechada do sistema resutante são $0.7742+0.2662j$ e  $0.7742-0.2662j$, que são os pontos escolhidos $z_1$ e $z_1^*$.

![image-20210722220459199](Images - relatório/image-20210722220459199.png)

----

Pode-se aplicar a transformada z inversa para obter as equações recursivas que implementam cada bloco separadamente, conforme segue:	


$$
y_{[n]} = 0.003299u_{[n-1]} + 0.003233u_{[n-2]} + 1.935y_{[n-1]} - 0.9417y_{[n-2]}
$$

$$
u_{[n]} = 18.65erro_{[n]} - 36.1erro_{[n-1]} + 17.57erro_{[n-2]} + 1.61u_{[n-1]} - 0.61u_{[n-2]}
$$

![image-20210722214655633](Images - relatório/image-20210722214655633.png)

![image-20210722220257452](Images - relatório/image-20210722220257452.png)

![image-20210722220027546](Images - relatório/image-20210722220027546.png)

É relevante observar também o sinal de atuação (utilizando a simulação por equações recursivas apresentada anteriormente), o qual não deve ultrapassar os limites físicos do hardware de implementação (que ainda não foi definido):

![image-20220529214216462](Images - relatório/image-20220529214216462.png)

O valor máximo do sinal de atuação é $10.35V$
O valor mínimo do sinal de atuação é $-7.79V$
As altas tensões e tensões negativas na saída do sinal podem ser um problema para a implementação em hardware, de forma que pode vir a ser necessário adaptar o projeto do controlador para facilitar a sua implementação.

# Projeto de controlador digital pela conversão de um controlador analógico

Escolheu-se um controlador do tipo PID ajustado pelo método de Ziegler-Nichols de malha aberta (apresentado pelo professor Jony na disciplina de Sistemas de Controle I), que consiste em identificar os parâmetros *constante de tempo ($T$)* e *delay time ($L$)* a partir da resposta de malha aberta ao degrau, conforme a figura abaixo. Como o sistema em questão possui resposta subamortecida, obteve-se os valores a partir da envoltória aproximada da resposta.

![image-20210721210432020](Images - relatório/image-20210721210432020.png)

![img](Images - relatório/Untitled.png)

Uma vez obtidos os valores de $L=0.005$ e $T=0.05$, calcula-se o valor dos parâmetros do controlador de acordo com a tabela abaixo:

![image-20210721212711110](Images - relatório/image-20210721212711110.png)

O controlador analógico projetado por essa metodologia atende os requisitos. Entretando, observou-se que o controlador convertido **não atendia** as especificações.

![image-20210722221459849](Images - relatório/image-20210722221459849.png)

![image-20210722221509596](Images - relatório/image-20210722221509596.png)



![image-20210722221548226](Images - relatório/image-20210722221548226.png)

![image-20210722221603419](Images - relatório/image-20210722221603419.png)

Diminuiu-se então o período de amostragem em 10x, obtendo-se assim um controlador satisfatório. O sistema final atende todos os requisitos do protejo, conforme vê ne comparação:

![image-20210722221706117](Images - relatório/image-20210722221706117.png)

![image-20210722222206615](Images - relatório/image-20210722222206615.png)

![image-20210722221755439](Images - relatório/image-20210722221755439.png)

# Fluxograma do controle implementado em microcontrolador

A implementação em microcontroladores se dá a partir das equações recursivas, com o sinal de feedback sendo medido por um ADC e o sinal de controle sendo enviado para a planta. 

Como o período de amostragem é fixo, é usual utilizar uma interrupção ativada por um timer para controlar a execução do código. A cada execução da interrupção, o buffer de amostras passadas é atualizado (os elementos de $k-1$ vão pra $k-2$ e assim por diante) e uma iteração da equação recursiva é calculada.

O sinal de saída deve respeitar as limitações de hardware em questão, incluindo os limites de tensão e a corrente drenada do microcontrolador. Portanto pode ser necessário limitar o sinal (de 0V a 5V, por exemplo) e utilizar um driver entre o microcontrolador e a planta.

Ainda mantendo a generalidade da escolha de hardware e linguagem de programação, a implementação por ser esquematizada pelo seguinte fluxograma:

<img src="Images - relatório/image-20210723215205929.png" alt="image-20210723215205929" style="zoom:80%;" />

## Implementação em microcontrolador

O hardware escolhido foi um Arduino Mega, que possui um microcontrolador ATmega2560, e a implementação em código C seguiu o fluxograma apresentado anteriormente.
Adaptou-se o projeto de forma que o sinal de controle estivesse dentro dos limites físicos do microcontrolador.
O período de amostragem foi escolhido como sendo T=5.0ms, resultado na seguinte equação de controle:
$$
C(z) = \frac{0.9329 z^2 - 0.9243 z + 0.4899}{z^2 - 1.17 z + 0.1696}\quad dt = 0.005s
\\
\\
\text{Equação recursiva de controle:}\\
u_{[n]} = +0.9329 erro_{[n+0]} -0.9243 erro_{[n-1]} +0.4899 erro_{[n-2]} +1.17u_{[n-1]} -0.1696u_{[n-2]}
$$
Esse novo sistema ainda atende aos requisitos iniciais do protejo, conforme vê ne comparação:

![image-20220601223807386](Images - relatório/image-20220601223807386.png)

![image-20220601223816739](Images - relatório/image-20220601223816739.png)

O valor máximo do sinal de atuação é 1.51V
O valor mínimo do sinal de atuação é 0.99V

O código está disponível na íntegra no repositório online https://github.com/LeonardoSanBenitez/arduino-discrite-control.

Infelizmente, a implementação real em microcontrolador ficou aquém do esperado. Segue uma captura do osciloscópio mostrando o sinal de PWM (canal amarelo) e a saída do sistema em malha fechada (canal azul):

![image-20220620072952129](Images - relatório/image-20220620072952129.png)

## Depuração da implementação

Verifiquei o tempo entre iniciar a leitura do ADC e escrever o valor no PWM: 

![image-20220620072836949](Images - relatório/image-20220620072836949.png)

O tempo parece adequado, rápido o suficiente.

Quando eu apenas simulo a leitura da planta (o restante do código permanece como está, mas utilizo a equação recursiva da planta ao invés da medição do ADC) me parece que o resultado é correto. Segue um exemplo, printando uma linha a cada execução da lei de controle:

```
--> Ref=1.50
last_measurement=1.00   u=1.47
last_measurement=1.14   u=1.42
last_measurement=1.37   u=1.44
last_measurement=1.53   u=1.48
last_measurement=1.58   u=1.51
last_measurement=1.56   u=1.51
last_measurement=1.52   u=1.51
last_measurement=1.50   u=1.50
last_measurement=1.49   u=1.50
last_measurement=1.49   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50
last_measurement=1.50   u=1.50

--> Ref=1.00
last_measurement=1.50   u=1.03
last_measurement=1.36   u=1.08
last_measurement=1.13   u=1.06
last_measurement=0.97   u=1.02
last_measurement=0.93   u=1.00
last_measurement=0.94   u=0.99
last_measurement=0.98   u=0.99
last_measurement=1.00   u=1.00
last_measurement=1.01   u=1.00
last_measurement=1.01   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
last_measurement=1.00   u=1.00
```

As medições do ADC também parecem estar corretas. Segue um exemplo com o sistema operando em malha aberta sem o controlador, mostrando apenas a medição do ADC:

```
--> Ref=1.50
1.05
1.17
1.33
1.52
1.62
1.66
1.66
1.60
1.49
1.40
1.34
1.30
1.34
1.43
1.52
1.57
1.64
1.63
1.56
1.50

--> Ref=1.00
1.42
1.31
1.14
0.99
0.86
0.78
0.79
0.85
0.94
1.05
1.14
1.15
1.13
1.06
0.97
0.87
0.84
0.83
0.87
0.95
```

Porem quando eu coloco tudo junto - tanto a medição da planta quanto a atuação em malha fechada, o resultado é bem aquém do esperado, como mostra a captura do osciloscópio da seção anterior. Segue um exemplo, mostrando apenas o sinal de controle:

```
--> Ref=1.50
1.57
1.56
1.61
1.61
1.59
1.52
1.44
1.39
1.41
1.43
1.48
1.57
1.62
1.63
1.62
1.59
1.51
1.45
1.43
1.41

--> Ref=1.00
0.97
0.99
0.93
0.90
0.94
1.01
1.09
1.15
1.18
1.14
1.07
1.00
0.92
0.88
0.88
0.93
0.99
1.07
1.13
1.15
```

Utilizei 10 casas decimais na implementação.

Desconfiei que as bibliotecas do arduino não estivessem trocando o valor do PWM imediatamente. Fiz a implementação do PWM "na unha",  configurando os registradores, mas o resultado foi o mesmo.

# Referências

Batista, Flavio (2021). Notas de aula de Sistemas de Controle II. Instituto Federal de Educação, Ciência e Educação de Santa Catarina.

Silveira, Jony (2020). Notas de aula de Sistemas de Controle I. Instituto Federal de Educação, Ciência e Educação de Santa Catarina.

Katsuhiko, Ogata (1995). Discrite-Time Control Systems. Prentice-Hall.
