import unittest
from control_utils import *
import control


class TestSignals(unittest.TestCase):
    def test_delta(self):
        k, u = delta(0, 10, 11)
        assert_almost_equal(k, [ 0.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9., 10.])
        assert_almost_equal(u, [1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])



class TestReqEq(unittest.TestCase):
    def test_circuit(self):
        R = 1
        C = 1
        T = 0.4
        tfinal = 10
        Gs = control.tf(1, [C*R, 1])
        Gz = control.c2d(Gs, T)
        x0, y0 = control.step_response(Gz, tfinal)
        y1 = tf_reqeq_exec(
            Gz, 
            np.ones(int(np.ceil(tfinal/T)) + 1), #step
            initial_conditions = [0]
        )
        assert len(y0) == len(y1)
        assert_almost_equal(y0, y1)

    def test_eq0(self):
        T = 0.1
        Gz = control.tf(
            [0.1, 0.03, -0.07],
            [1, -2.7, 2.42, -0.72],
            T
        )

        tfinal = 1
        kT = np.arange(0, tfinal, T)
        xz, yz = control.step_response(Gz, kT)

        yz2 = tf_reqeq_exec(
            Gz, 
            step(0, tfinal, T=T)[1],
            initial_conditions=yz[:5]
        )

        assert_almost_equal(yz, yz2)
        #plt.scatter(xz, yz, color='blue')
        #plt.scatter(xz, yz2, color='red')

        tfinal = 10
        kT = np.arange(0, tfinal, T)
        xz, yz = control.step_response(Gz, kT)

        yz2 = tf_reqeq_exec(
            Gz, 
            step(0, tfinal, T=T)[1],
            initial_conditions=yz[:5]
        )

        assert_almost_equal(yz, yz2)
        #plt.scatter(xz, yz, color='blue')
        #plt.scatter(xz, yz2, color='red')

        tfinal = 10
        kT = np.arange(0, tfinal, T)
        xz, yz = control.impulse_response(Gz, kT)

        yz2 = tf_reqeq_exec(
            Gz, 
            delta(0, tfinal, T=T)[1],
            initial_conditions=yz[:5]
        )

        assert_almost_equal(yz, yz2)
        #plt.scatter(xz, yz, color='blue')
        #plt.scatter(xz, yz2, color='red')