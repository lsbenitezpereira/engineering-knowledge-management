'''
# License
Copyright 2021 Leonardo Benitez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Anotações gerais
ic = C dv/dt
vl = L di/dt

Sistema de segunda ordem na forma canônica:
G(s) = wn^2/(s^2 + 2*ksi*wn*s + wn^2)

## Condição de angulo e módulo
se z1 for um polo da FTMF, então:
<FTMA(z1) = -+180
|FTMA(z1)| = 1

## Sampling
Amostrador digital:
* FTMA(z) = G(z)*H(z)
* FTMF(z) = G(z)/(1 + G(z)*H(z))

Amostrador analógico:
* FTMA(z) = GH(z) = ZT{ZOH(s)G(s)H(s)}
* FTMF(z) = G(z)/(1 + GH(z))
'''

import numpy as np
import control
import matplotlib.pyplot as plt
from control.matlab import lsim
from typing import Optional
import json

def step_info(FTMF:control.xferfcn.TransferFunction, tfinal:float, plot:bool=True, title='Step response')->dict:
    '''
    All the times are reported in units of `t`
    return metrics
    '''
    if FTMF.dt==0:
        # Continous system
        x, y = control.step_response(FTMF, tfinal, T_num=10000)
        T = tfinal/10000
    else:
        # Discrite system
        x, y = control.step_response(FTMF, tfinal)
        T = FTMF.dt
    metrics = {}
    
    # Sobressinal percentual
    metrics['Mp'] = (max(y)-y[-1])/(y[-1]-y[0])*100 
    metrics['tp'] = np.argmax(y)*T


    # Tempo de acomodação para 5%
    for i in range(len(x)-1, 0, -1):
        delta = abs((y[i]-y[-1])/y[-1])
        if delta >= 0.05:
            break
    metrics['ts5'] = i*T

    # Tempo de acomodação para 2%
    for i in range(len(x)-1, 0, -1):
        delta = abs((y[i]-y[-1])/y[-1])
        if delta >= 0.02:
            break
    metrics['ts2'] = i*T

    # Rising time
    tr = 0
    for i in range(len(x)-1):
        if y[i]>=y[-1]*0.1 and y[i]<y[-1]*0.9:
            tr += 1
    metrics['tr'] = tr*T

    # Delay time
    for i in range(len(x)-1):
        if y[i]>=y[-1]*0.5:
            break
    metrics['td'] = i*T
    
    # Steady state error to step
    metrics['ess'] = 1 - y[-1]

    # Final value for unit step
    metrics['final_value_step_simulated'] = y[-1]
    metrics['final_value_step_theoretical'] = final_value(FTMF)

    
    if plot:
        f = metrics['final_value_step_simulated']
        x, y = control.step_response(FTMF, tfinal)
        plt.hlines(f + f*metrics['Mp']/100, 0, metrics['tp'], label='Peak time', linestyle='dotted', linewidth=2, color='grey',)
        plt.hlines(f-0.05, metrics['ts5'], tfinal, linestyle='dashed', linewidth=1, color='grey')
        plt.hlines(f+0.05, metrics['ts5'], tfinal, linestyle='dashed', linewidth=1, color='grey')
        plt.vlines(metrics['ts5'], 0, f+0.05, label='Settling time', linestyle='dashed', linewidth=1, color='grey')
        plt.hlines(f, 0, tfinal, label='Ideal response', linestyles='--', linewidth=1.4, color='green')

        if FTMF.dt==0:
            # Continous system
            plt.plot(x,y, label='Actual response')
        else:
            # Discrite system
            plt.plot(x, y, '*', label='Actual response')
        
        plt.title(title)
        plt.legend(loc='lower right')
        plt.grid()

    return metrics

def step_info_theoretical(FTMF:control.xferfcn.TransferFunction)->dict:
    '''
    Second order LTI system is assumed
    return metrics
    '''
    T = FTMF.dt
    pole = control.pole(FTMF)[0]
    temp_0 = -np.log(np.abs(pole)) #temp0 = T*zeta*wn
    temp_1 = np.angle(pole)          #temp1 = T*wn*sqrt(1-zeta^2)
    temp_3 = temp_0/temp_1
    zeta = temp_3/np.sqrt(1+temp_3**2)
    wn = temp_0/(T*zeta)
    
    metrics = {}
    metrics['zeta'] = zeta
    metrics['wn'] = wn
    metrics['wd'] = wn*np.sqrt(1-zeta**2)
    metrics['Mp'] = np.exp(-np.pi*zeta/(np.sqrt(1-zeta**2)))*100
    metrics['ts5'] = 3/(zeta*wn)
    
    return metrics


def errors(FTMA:control.xferfcn.TransferFunction, eps:float=1e-3, verbose=True, plot: bool = False):
    T = FTMA.dt
    metrics = {}
    
    [[n]],[[d]]= control.tfdata(FTMA)
    K = np.polyval(n, 1)/np.polyval(d, 1)   #  lim z->1
    metrics['Kp'] = K
    metrics['ess_degrau'] = np.array(1)/(1+K)
    input = control.tf([1, 0], [1, -1], T)
    metrics['ess_degrau_theorem'] = np.real(((1 - control.tf([1], [1, 0], T))*(1/(1+FTMA))*input).minreal()(1))
    
    aux = control.minreal(control.tf([1, -1],[1, 0],T)*FTMA)  # onde tf([1 -1],[1 0],T) = (z-1)/z = 1-z^-1
    [[n]],[[d]]= control.tfdata(aux)
    K = (np.polyval(n, 1)/np.polyval(d, 1))/T
    if abs(K)<eps: K=0 # type: ignore
    if abs(K)>1/eps: K=np.inf # type: ignore
    metrics['Kv'] = K
    metrics['ess_rampa'] = np.array(1)/K
    z_at_minus_1 = control.tf([1], [1, 0], T)
    input = T*z_at_minus_1/((1-z_at_minus_1)**2)
    err = np.real(((1 - control.tf([1], [1, 0], T))*(1/(1+FTMA))*input).minreal()(1))
    if abs(err)<eps: err=0 # type: ignore
    if abs(err)>1/eps: err=np.inf # type: ignore
    metrics['ess_rampa_theorem'] = err

    aux = control.minreal(control.tf([1, -1],[1, 0],T)**2*FTMA)
    [[n]],[[d]]= control.tfdata(aux)
    K = (np.polyval(n, 1)/np.polyval(d, 1))/(T**2)
    if abs(K)<eps: K=0 # type: ignore
    if abs(K)>1/eps: K=np.inf # type: ignore
    metrics['Ka'] = K
    metrics['ess_parabola'] = np.array(1)/K
    input = T**2 * z_at_minus_1 * (1+z_at_minus_1) / ((1-z_at_minus_1)**3)
    err = np.real(((1 - control.tf([1], [1, 0], T))*(1/(1+FTMA))*input).minreal()(1))
    if abs(err)<eps: err=0 # type: ignore
    if abs(err)>1/eps: err=np.inf # type: ignore
    metrics['ess_parabola_theorem'] = err


    if verbose:
        _type = 'Above 3'
        if metrics['ess_rampa']==np.inf: _type = '0'
        elif metrics['ess_parabola']==np.inf: _type = '1'
        elif abs(metrics['ess_parabola'])<eps: _type = '2'
        print(f'System type: {_type}')
        print(f'Error analysis:\n{json.dumps(metrics, indent=4)}')

    if plot:
        plot_responses(FTMA/(1 + FTMA), 20, subtract_input=True)

    return metrics

def test_controlability(sys: control.statesp.StateSpace, verbose: bool = True) -> bool:
    Mc = np.block([[sys.A, sys.B],[-sys.C, 0]])
    controlable = np.linalg.matrix_rank(Mc) == Mc.shape[0]
    if verbose:
        print('The controlability matrix is:')
        print(Mc)
        print('Whose rank is:')
        print(np.linalg.matrix_rank(Mc))
        print(f"Therefore, the system {'is' if controlable else 'is NOT'} controlable.")
    return controlable

def test_observability(sys: control.statesp.StateSpace, verbose: bool = True) -> bool:
    N = control.obsv(sys.A, sys.C).T
    observable = np.linalg.matrix_rank(N) == N.shape[0]
    if verbose:
        print('The observability matrix is:')
        print(N)
        print('Whose rank is:')
        print(np.linalg.matrix_rank(N))
        print(f"Therefore, the system {'is' if observable else 'is NOT'} fully observable.")
    return observable

def plot_observer_comparison(sys: control.statesp.StateSpace, tfinal: float, initial_conditions: np.matrix, input='step', num_steps: int = 1000) -> None:
    n = sys.A.shape[0]
    assert initial_conditions.shape[0] == n
    if input != 'step':
        raise NotImplementedError()

    t = np.linspace(0, tfinal, num_steps)
    u = np.heaviside(t, 1)
    Y, t, X = control.matlab.lsim(sys, u, t, initial_conditions)

    half = int(X.shape[1]/2)
    for i in range(0, half):
        plt.subplots(1,2, figsize=(14, 4))
        plt.subplot(1,2,1)
        plt.plot(t, X[:, i])
        plt.xlabel("t")
        plt.ylabel(f"x{i+1}(t)")
        plt.grid(True)
            
        plt.subplot(1,2,2)
        plt.plot(t, X[:, i+half])
        plt.xlabel("t")
        plt.ylabel(f"x{i+1}_observed(t)")
        plt.grid(True)
        
    half = int(Y.shape[1]/2)
    for i in range(0, half):
        plt.subplots(1,2, figsize=(14, 4))
        plt.subplot(1,2,1)
        plt.plot(t, Y[:, i])
        plt.xlabel("t")
        plt.ylabel(f"y{i+1}(t)")
        plt.grid(True)
            
        plt.subplot(1,2,2)
        plt.plot(t, Y[:, i+half])
        plt.xlabel("t")
        plt.ylabel(f"y{i+1}_observed(t)")
        plt.grid(True)

def plot_responses(FTMF:control.xferfcn.TransferFunction, tfinal:float, figsize=(15, 4), subtract_input: bool = False):
    T = FTMF.dt
    Kmax = int(tfinal/T+1)
    t = np.linspace(0, tfinal, Kmax)
    plt.subplots(1,3, figsize=figsize)
        
    ##    
    plt.subplot(1,3,1)

    entrada = np.ones(t.shape[0])

    y, t, x = lsim(FTMF, entrada, t)
    if subtract_input:
        y = entrada - y
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta ao degrau")
    plt.xlabel("k*T")
    plt.grid(True)

    ##
    plt.subplot(1,3,2)
    entrada = t

    y, t, x = lsim(FTMF, entrada, t)
    if subtract_input:
        y = entrada - y
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta à rampa")
    plt.xlabel("k*T")
    plt.grid(True)
    
    ##
    plt.subplot(1,3,3)
    entrada = t**2 # type: ignore

    y, t, x = lsim(FTMF, entrada, t)
    if subtract_input:
        y = entrada - y
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta à parabola")
    plt.xlabel("k*T")
    plt.grid(True)


def plot_control_signal(t : np.ndarray, u : np.ndarray, title='Control signal $u(kT)$ for step response')->None:
    plt.step(t, u)
    print('O valor máximo do sinal de atuação é %.2f'%(u.max()))
    print('O valor mínimo do sinal de atuação é %.2f'%(u.min()))
    plt.hlines(u.max(), t[0], t[-1], linestyle='dashed', linewidth=1, color='red')
    plt.hlines(u.min(), t[0], t[-1], linestyle='dashed', linewidth=1, color='red')
    plt.title(title)
    plt.grid()





def pole_for_parameters(desired_zeta: float, desired_wd: float, ws: float, T) -> complex:
    '''
    Polo que satisfaz os parâmetros desejados
    Calculado pela condição de ângulo e módulo
    '''
    z1_abs = np.exp(-(2*np.pi*desired_zeta*desired_wd)/(np.sqrt(1-desired_zeta**2)*ws))
    z1_angle = T*desired_wd
    z1 = z1_abs*np.exp(z1_angle*1j)
    return z1


def gain_for_pole(FTMA, z1: complex, threshold_degrees: float = 1.0) -> float:
    '''
    Ganho para que z1 seja um polo de FTMF
    Calculado pela condição de ângulo e módulo
    Polo de FTMF = raiz da equação característica
    '''
    a = np.angle((FTMA)(z1))*180/np.pi
    assert np.abs(abs(a) - 180) < threshold_degrees, "Z1 can NOT be achieved just by changing the gain"
    k = 1/np.abs((FTMA)(z1))
    return k


   

def final_value(Gz:control.xferfcn.TransferFunction, input='step') -> float:
    '''
    g(t=+inf) when input is of a given function

    Apenas para sistemas discretos no domínio z

    Usualmente Gz será a FTMF

    TODO: implementar para os demais tipos de sistema
    
    TODO:  take a look at `errors` function
    ''' 
    if type(Gz)!=control.xferfcn.TransferFunction:
        #no espaço de estados
        #import sympy as sp
        #n = A.shape[0]
        #I = np.eye(n)
        #s = sp.symbols('s')
        #R = 1/s
        #M = sp.Matrix(C)*((s*I-sp.Matrix(A))**-1)*sp.Matrix(B)
        #ess = sp.limit(s*R*(1-M[0]),s,0)

        return -1

    if input == 'step':
        step = control.tf([1, 0], [1, -1], Gz.dt)
        return np.real((Gz * step * control.tf([1, -1], [1], Gz.dt)).minreal()(1))
    else:
        raise NotImplementedError()
##################
# Convertions
def parameters_from_metrics(Mp=None, tp=None, ts5=None):
    if tp and Mp:
        zeta = np.sqrt(1/(1 + 1/((-np.log(Mp)/np.pi)**2)))
        wn = np.pi/(tp*np.sqrt(1-zeta**2))
    elif ts5 and Mp:
        zeta = np.sqrt(1/(1 + 1/((-np.log(Mp)/np.pi)**2)))
        wn = 3/(zeta*ts5)
    else:
        raise NotImplementedError('Not implemented combination of metrics')
    return zeta, wn

def system_from_parameters(zeta, wn, T=None):
    if T:
        return control.tf([wn**2], [1, 2*zeta*wn, wn**2], T)
    else:
        return control.tf([wn**2], [1, 2*zeta*wn, wn**2])
    
    
def ss2tf(sys):
    '''
    convertendo para função de tranferência com a utilização da Equação:
    $G = C*inv(s*eye(2)-A)*B+D$
    abordagem com variáveis simbolicas
    
    Same as `sys_s = syscontrol.ss2tf(sys_ss)`
    '''
    s, t = sp.symbols('s t')

    # Matrizes
    A = sp.Matrix(sys.A)
    B = sp.Matrix(sys.B)
    C = sp.Matrix(sys.C)
    D = sp.Matrix(sys.D)

    I = np.eye(2)
    Gs = C*(s*I-A)**-1*B+D
    return Gs

def tf2ss(G):
    '''
    convertendo para o espaço de estados a partir da função de transferência
    Same as `sys_ss = control.tf2ss(sys_s)`
    '''
    [[n]],[[d]] = control.tfdata(G2)
    a1 = d[1]
    a2 = d[2]

    A = [[0, 1], [-a2, -a1]]
    A = np.matrix(A)
    B = [[0], [n[1]]]
    B = np.matrix(B)
    C = [1, 0]
    D = 0
    sys_ss = control.ss(A,B,C,D)
    return sys_ss
    

#############
# Transfer function manipulations
import scipy

def tf_reqeq_format(
    Gz: control.xferfcn.TransferFunction,
    input_function : str = 'u',
    output_function : str = 'y',
    precison : int = 4, #significat digits
) -> str:
    assert Gz.dt != 0, 'System should be discrite'
    eq = ''
    terms = []

    index = len(Gz.num[0][0]) - len(Gz.den[0][0])
    for n in Gz.num[0][0]:
        terms.append(f"%+.{precison}g {input_function}_{{[n%+d]}}"%(n, index))
        index -= 1

    # O primeiro indice é o que está sendo calculado
    # o negativo é pois as variáveis foram passadas pro outro lado
    index = -1
    for d in -Gz.den[0][0][1:]:
        terms.append(f"%+.{precison}g{output_function}_{{[n%+d]}}"%(d, index))
        index -= 1
    eq = f"{output_function}_{{[n]}} = " + " ".join(terms)
    return eq


def tf_reqeq_exec(
    Gz: control.xferfcn.TransferFunction,
    x: np.ndarray, #input to the function
    initial_conditions : list = []
) -> np.ndarray:

    # Initial conditions
    y = np.zeros(len(x))
    for i in range(len(initial_conditions)):
        y[i] = initial_conditions[i]
    
    # Recursive equation
    for i in range(len(initial_conditions), len(x)):
        y[i] = 0
        index = len(Gz.num[0][0]) - len(Gz.den[0][0])
        for n in Gz.num[0][0]:
            y[i] += n*x[i+index]
            index -= 1

        # O primeiro indice (i) é o que está sendo calculado
        # o negativo é pois as variáveis foram "passadas pro outro lado"
        index = -1
        for d in -Gz.den[0][0][1:]:
            y[i] += d*y[i+index]
            index -= 1

    
    return y


def ee_reqeq_exec(
    sys,
    initial_conditions,
    T,
    tfinal,
    input_function='unit_step',
    verbose: bool = True,
    plot: str = 'all',
) -> tuple:
    '''
    Return Y, t, X
    '''
    # Signal init
    Kmax = int(tfinal/T+1)
    index_max = Kmax+1
    k = np.linspace(0, Kmax, index_max)
    ref = apply_function(k, input_function)
        
    A = sys.A
    B = sys.B
    C = sys.C
    D = sys.D
    
    n = A.shape[0]
    X = np.empty((index_max, n))
    X_ponto = np.empty((index_max, n))
    y = np.empty(index_max)
    u = ref

    # Unroll first iterations
    j=0
    X[j, :] = initial_conditions.T
    X_ponto[j, :] = np.zeros(n)
    y[j] = C*np.matrix(X[j, :]).T + D*u[j]

    # Simulate all
    for j in range(1,Kmax):
        X[j, :] = T*X_ponto[j-1, :] + X[j-1, :]
        X_ponto[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T
        y[j] = C*np.matrix(X[j, :]).T + D*u[j]

    if plot:
        ee_reqeq_plot(y, k*T, X, ref, title=f"Resposta por eq. rec. do sistema\nEntrada {input_function}")
        
    return y, k*T, X


def ee_reqeq_exec_with_controller(
    sys_original,
    initial_conditions,
    T,
    tfinal,
    K_contr,
    k_contr_i,
    input_function='unit_step',
    verbose: bool = True,
    plot: str = 'all',
) -> tuple:
    '''
    Return Y, t, X
    '''
    # Signal init
    K = K_contr
    ki = k_contr_i
    Kmax = int(tfinal/T+1)
    index_max = Kmax+1
    k = np.linspace(0, Kmax, index_max)
    ref = apply_function(k, input_function)

    A = sys_original.A
    B = sys_original.B
    C = sys_original.C
    D = sys_original.D

    n = A.shape[0]
    X = np.empty((index_max, n))
    X_ponto = np.empty((index_max, n))
    y = np.empty(index_max)
    erro = np.empty(index_max)
    u = np.empty(index_max)
    qsi = np.empty(index_max)

    # Unroll first iterations
    j=0
    X[j, :] = initial_conditions.T[:, :-1]
    X_ponto[j, :] = np.zeros(n)
    qsi[j] = initial_conditions.T[:, -1]
    u[0] = -(K[0,0]*X[j, 0]+K[0,1]*X[j, 1])+ki*qsi[j]
    y[j] = C*np.matrix(X[j, :]).T + D*u[j]
    erro[j] = ref[j]-y[j]

    # Simulate all
    for j in range(1,Kmax+1):
        X[j, :] = T*X_ponto[j-1, :] + X[j-1, :]
        qsi[j] = T*erro[j-1]+qsi[j-1]
        u[j] = -(K[0,0]*X[j, 0]+K[0,1]*X[j, 1])+ki*qsi[j]
        X_ponto[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T
        y[j] = C*np.matrix(X[j, :]).T + D*u[j]
        erro[j] = ref[j]-y[j] 
    
    if plot:
        ee_reqeq_plot(y, k*T, X, ref, title=f"Resposta por eq. rec. do sistema com controlador\nEntrada {input_function}")

    return y, k*T, X


def ee_reqeq_exec_with_observer(
    sys_original,
    initial_conditions,
    T,
    tfinal,
    K_obs, # I could also pass sys_obs and do: `K_obs = sys_obs.A[n:, 0]`
    input_function='unit_step',
    verbose: bool = True,
    plot: str = 'all',
) -> tuple:
    '''
    Return Y, t, X
    '''
    # Signal init    
    A = sys_original.A
    B = sys_original.B
    C = sys_original.C
    D = sys_original.D
    n = A.shape[0]

    
    Kmax = int(tfinal/T+1)
    index_max = Kmax+1
    k = np.linspace(0, Kmax, index_max)
    ref = apply_function(k, input_function)

    A = sys_original.A
    B = sys_original.B
    C = sys_original.C
    D = sys_original.D

    n = A.shape[0]
    X = np.empty((index_max, n))
    X_ponto = np.empty((index_max, n))
    X_obs = np.empty((index_max, n))
    X_ponto_obs = np.empty((index_max, n))
    y = np.empty(index_max)
    y_obs = np.empty(index_max)
    u = ref

    # Unroll first iterations
    j=0
    X[j, :] = initial_conditions.T[:, :n]
    X_obs[j, :] = initial_conditions.T[:, n:n*2]
    X_ponto[j, :] = np.zeros(n)
    X_ponto_obs[j, :] = np.zeros(n)
    y[j] = C*np.matrix(X[j, :]).T + D*u[j]
    y_obs[j] = C*np.matrix(X[j, :]).T + D*u[j]

    # Simulate all
    for j in range(1,Kmax+1):
        X[j, :] = T*X_ponto[j-1, :] + X[j-1, :]
        X_obs[j, :] = T*X_ponto_obs[j-1, :] + X_obs[j-1, :]
        X_ponto[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T
        y[j] = C*np.matrix(X[j, :]).T + D*u[j]
        y_obs[j] = C*np.matrix(X_obs[j, :]).T + D*u[j]
        X_ponto_obs[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T + np.matrix(K_obs*(y[j]-y_obs[j])).T
    
    if plot:
        ee_reqeq_plot(y, k*T, X, ref, title=f"Resposta por eq. rec. do sistema com observador\nEntrada {input_function}")

    return y, k*T, X


def ee_reqeq_exec_with_observer_and_controller(
    sys_original,
    initial_conditions,
    T,
    tfinal,
    K_contr,
    k_contr_i,
    K_obs, # I could also pass sys_obs and do: `K_obs = sys_obs.A[n:, 0]`
    input_function='unit_step',
    verbose: bool = True,
    plot: str = 'all',
) -> tuple:
    '''
    Return Y, t, X
    '''
    # Signal init
    K = K_contr
    ki = k_contr_i
    
    A = sys_original.A
    B = sys_original.B
    C = sys_original.C
    D = sys_original.D
    n = A.shape[0]

    Kmax = int(tfinal/T+1)
    index_max = Kmax+1
    k = np.linspace(0, Kmax, index_max)
    ref = apply_function(k, input_function)

    A = sys_original.A
    B = sys_original.B
    C = sys_original.C
    D = sys_original.D

    n = A.shape[0]
    X = np.empty((index_max, n))
    X_ponto = np.empty((index_max, n))
    X_obs = np.empty((index_max, n))
    X_ponto_obs = np.empty((index_max, n))
    y = np.empty(index_max)
    y_obs = np.empty(index_max)
    erro = np.empty(index_max)
    u = np.empty(index_max)
    qsi = np.empty(index_max)

    # Unroll first iterations
    j=0
    X[j, :] = initial_conditions.T[:, :n]
    X_obs[j, :] = initial_conditions.T[:, n:n*2]
    X_ponto[j, :] = np.zeros(n)
    X_ponto_obs[j, :] = np.zeros(n)
    qsi[j] = initial_conditions.T[:, -1]
    u[0] = -(K[0,0]*X[j, 0]+K[0,1]*X[j, 1])+ki*qsi[j]
    y[j] = C*np.matrix(X[j, :]).T + D*u[j]
    y_obs[j] = C*np.matrix(X[j, :]).T + D*u[j]
    erro[j] = ref[j]-y[j]

    # Simulate all
    for j in range(1,Kmax+1):
        X[j, :] = T*X_ponto[j-1, :] + X[j-1, :]
        X_obs[j, :] = T*X_ponto_obs[j-1, :] + X_obs[j-1, :]
        qsi[j] = T*erro[j-1]+qsi[j-1]
        u[j] = -(K[0,0]*X[j, 0]+K[0,1]*X[j, 1])+ki*qsi[j] #shoudl K*X be matricial?
        X_ponto[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T
        y[j] = C*np.matrix(X[j, :]).T + D*u[j]
        y_obs[j] = C*np.matrix(X_obs[j, :]).T + D*u[j]
        erro[j] = ref[j]-y[j] 
        X_ponto_obs[j, :] = (A*np.matrix(X[j, :]).T + B*u[j]).T + np.matrix(K_obs*(y[j]-y_obs[j])).T

    if verbose:
        print(f'The control signal goes from {min(u)} to {max(u)}')

    if plot:
        ee_reqeq_plot(y, k*T, X, ref, title=f"Resposta por eq. rec. do sistema com observador e controlador\nEntrada {input_function}")
    
    return y, k*T, X


def ee_reqeq_plot(Y, t, X, r, title: str = None):
    #plt.figure()
    plt.plot(t, r, linestyle='--', linewidth=1.4, color='green', label='Ideal response')
    plt.plot(t, Y,'*', label='Actual response')
    plt.xlabel("t")
    #plt.ylabel("Value")
    plt.legend(loc='lower right')
    if title:
        plt.title(title)
    plt.grid(True)

def tf_zpk_format(
    sys: control.xferfcn.TransferFunction,
    precison : int = 4,
    output_function : str = 'Y',
) -> str:
    z, p, k = scipy.signal.tf2zpk(sys.num[0][0], sys.den[0][0])
    final_str = fr"{output_function}(z) = %.{precison}g*\frac{{"%(k)
    # Zeros
    for i in range(len(z)):
        final_str += fr"(s %+.{precison}f)"%(-z[i])
    final_str += '}{'

    # Poles
    for i in range(len(p)):
        final_str += fr"(s %+.{precison}f)"%(-p[i])
    final_str += '}'
    return final_str
# Test tf_zpk_format
## this
#GHz = control.tf([0.01758001, 0.01538798], [ 1., -1.67032005, 0.67032005])
#show_formatted(tf_zpk_format(GHz))
##is equal to this
#print("0.017580011508909754*control.tf([1, 0.87531137], [1, -1])*control.tf([1], [1, -0.67032005])")
## Because
#assert GHz.__format__('') == ((0.017580011508909754*control.tf([1, 0.87531137], [1, -1])*control.tf([1], [1, -0.67032005])).minreal()).__format__('')

def tf_expand_partial_format(
    Gz: control.xferfcn.TransferFunction,
    precison : int = 4,
    output_function : str = 'Y',
) -> str:
    terms = []
    Gz_aux = Gz/control.tf([1, 0], [1], Gz.dt)
    r, p, k = scipy.signal.residue(Gz_aux.num[0][0], Gz_aux.den[0][0])
    for i in range(len(r)):
        if r[i] == 0:
            continue
        else:
            terms.append(fr"\frac{{%.{precison}g*Z}}{{Z - %.{precison}g}}"%(r[i], p[i]))

    return f"{output_function}(Z)/Z = " +  " + ".join(terms)

def tf_inverse_transform_format(
    Gz: control.xferfcn.TransferFunction,
    precison: int = 4,
    output_function: str = 'y',
    threshold_zero: float = 0.0001,
) -> str:
    '''
    Given G(z), print g(t)
    '''
    # another way, using a symbolic lib:
    # from lcapy.discretetime import z
    # Yz=0.2707*z / (z**3 - 2.549*z**2 + 2.278*z - 0.7293)
    # xk=Yz.IZT()
    # print(xk.evalf())
    terms = []
    Gz_aux = Gz/control.tf([1, 0], [1], Gz.dt)
    r, p, k = scipy.signal.residue(Gz_aux.num[0][0], Gz_aux.den[0][0])
    for i in range(len(r)):
        # TODO: if r[i] and p[i] are complex
        print(r[i], '*', p[i])
        if np.abs(r[i]) < threshold_zero:
            continue
        elif p[i] == 0:
            #delta
            terms.append(fr'%+.{precison}f*\delta[n]'%(r[i]))
        else:
            # Exponential
            terms.append(f'%+.{precison}f*%.{precison}f^n'%(r[i], p[i]))
    return f"{output_function}(n) = " +   " ".join(terms)


def tf_residue_method(Gs, T: float = 1):
    '''
    Return G(z) given G(s)
    '''
    terms = []
    for pole in Gs.pole():
        # consideramos aqui todos os polos sendo simples
        terms.append((control.tf([1, -pole], 1) * Gs).minreal()(pole) * control.tf([1, 0], [1, -np.e**(pole*T)], T))
    terms_str = [term._repr_latex_().split('\\quad')[0][2:] for term in terms]
    
    return ' + '.join(terms_str), sum(terms).minreal()

def show_formatted(equation: str) -> None:
    from IPython.display import display, Markdown
    display(Markdown('$$' + equation + '$$'))

#############
# Signals
def delta(start, stop, num : Optional[int]=None, T : Optional[float]=None):
    if num:
        k = np.linspace(start, stop, num)
        y = np.zeros(num)
    elif T:
        k = np.arange(start, stop, T)
        y = np.zeros(len(k))
    else:
        raise ValueError('Either num or T must be specified')
    y[0] = 1   # para k=0
    #print("delta = \n",y)
    return k, y

def step(start, stop, num : Optional[int]=None, T : Optional[float]=None):
    if num:
        k = np.linspace(start, stop, num)
        y = np.ones(num)
    elif T:
        k = np.arange(start, stop, T)
        y = np.ones(len(k))
    else:
        raise ValueError('Either num or T must be specified')
    return k, y


def apply_function(x, input_function):
    if input_function == 'unit_step':
        y = 1.0*np.heaviside(x, 1)
    elif input_function == 'step_1_to_1.5':
        k_max = x.shape[0]
        y = np.concatenate((np.ones(int(k_max//4)), 1.5*np.ones(int(k_max//4)), np.ones(int(k_max//4)), 1.5*np.ones(int(k_max//4 + k_max%4))))
    elif input_function == 'none':
        y = x*0.0
    else:
        raise NotImplementedError()
    return y

#############
# Utils
from typing import Union, Tuple, List
from numpy import ndarray
def assert_almost_equal(
    a: Union[float, int, np.float64, List, ndarray], 
    b: Union[float, int, np.float64, List, ndarray], 
    threshold: float=0.01,
) -> None:
    '''
    a and b are expected to be of compatible types, and this is not checked
    '''
    if (type(a)==float) or (type(a)==int) or (type(a)==np.float64):
        if a==np.inf and b==np.inf:
            pass
        elif a==np.inf or b==np.inf:
            assert a==b # type: ignore
        else:
            assert abs(a-b)<threshold # type: ignore
    elif (type(a)==list) or (type(a) == np.ndarray):
        assert len(a)==len(b) # type: ignore
        for i in range(len(a)):
            assert_almost_equal(a[i], b[i], threshold) # type: ignore
    else:
        raise NotImplementedError(type(a), type(b))





def z_positive_to_negative(n, d):
    '''
    Takes a z equation represented with positive z expoent and return it represented with negative expoents
    I'm not sure if this is right
    '''
    n2 = n.copy()
    if len(n2) != len(d):
        for _ in range(len(d) - len(n2)):
            n2.insert(0, 0)
    return n2, d