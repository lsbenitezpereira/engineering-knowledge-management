% Exemplo Transformada Z
NT=size('LeonardoSantiagoBenitezPereira')(2)

%%% Polo à direita -> convergência monotônica
figure(1)
a = NT/50;
k = 0:10;
fk = a.^k;

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
zplane([],a)
title('polos de F(z)')


%%% polo à esquerda -> convergência oscilatório
figure(2)
a = -NT/50;
k = 0:10;
fk = a.^k;

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
zplane([],a)
title('polos de F(z)')

%%% Polo fora do circulo unitario à direita -> divergência monotônica
figure(3)
a = NT/10;
k = 0:10;
fk = a.^k;

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
zplane([],a)
title('polos de F(z)')

%%% Polo fora do circulo unitario à direita -> divergência oscilatória
figure(4)
a = -NT/10;
k = 0:10;
fk = a.^k;

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
zplane([],a)
title('polos de F(z)')

%%% Polos sobre o circulo unitário -> oscilação sustentada
figure(5)
W0T = NT/50;
k = 0:10;
fk = sin(W0T*k);

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
zplane([sin(W0T) 0], [1 -2*cos(W0T) 1])
title('polos de F(z)')


%%% 
figure(6)
m = round(NT/10);
k = 0:10;
fk = k*0;
fk(m+1) = 1;

subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolução temporal de f(k)')

subplot(1,2,2)
den = zeros(1, m+1)
den(1) = 1
zplane([], den)
title('polos de F(z)')