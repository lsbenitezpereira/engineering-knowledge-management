%Projeto 2 - SCT2
% Rayan M. Steinbach
%Observador de ordem plena
clc;close all;

NT = 21; R1=34e3;R2=18e3;C1=78e-9;C2=680e-9;
t=0:0.00001:0.15;

%X1 = VC1 e X2 = Vx
a11 = -1/(C1*R2); a12 = -a11;
a21 = 1/(C2*R2)+a11;
a22 = a12 - 1/(C2*R1)-1/(C2*R2);
A = [a11 a12; a21 a22];
B = [0;1/(R1*C2)];
C = [1 0]; D = 0;

ctr = ctrb(A, B);
Cntrol=det(ctr)
obs = obsv(A, C);
Obser=rank(obs)

figure(1);
% Polos desejados
ts = NT*1e-3; Mp=2*NT/100;
zeta = log(Mp)/sqrt(log(Mp)^2+pi^2)
wn = 3/(zeta*ts)

p1 = -zeta*wn + 1j*wn*sqrt(1-zeta^2);
p2 = -zeta*wn - 1j*wn*sqrt(1-zeta^2);
p3 = -10*abs(p1);
P = [p1 p2 p3];

%Projeto do controlador
A_chapeu = [A zeros(2,1);-C 0];
B_chapeu = [B;0];

% Matriz de ganhos do controlador K_chapeu = [k1 k2 -ki]
K_chapeu=acker(A_chapeu,B_chapeu,P)

% Verificando
eig(A_chapeu-B_chapeu*K_chapeu)

K = [K_chapeu(1) K_chapeu(2)];
Ki = -K_chapeu(3);
AA = [A-B*K B*Ki;-C 0];
BB = [0;0;1];
CC = [1 0 0];
DD = 0;

%Projeto do observador de ordem plena
PO = 1.8.*[real(p1) real(p2)]
Ke = acker(A',C',PO)'
eig(A-Ke*C)

AO = [A zeros(length(A)); Ke*C A-Ke*C];
BO = [B;B];
CO = eye(2*length(A));
DO = zeros(2*length(A),1);
ax = gca
ax.YLim = [0 1.5];
hold on
so = step(AO,BO,CO,DO,1,t);
plot(t,so(:,1),'ko')
step(AA,BB,CC,DD,1,t);
sori = step(A,B,C,D,1,t);
plot(t,sori,'r','LineWidth',2),legend('Observador','Controlado','Sem controle');
hold off

%Equa��es recursivas
T = 1e-4; k = 0:1:(0.15/T); 
r = ones(1,length(k)/2); r = [r 1.5.*r];
E = zeros(1,length(k)); Ep = zeros(1,length(k));
x1 = zeros(1,length(k)); x2 = zeros(1,length(k));
y = zeros(1,length(k)); yt = zeros(1,length(k));
x1p = zeros(1,length(k)); x2p = zeros(1,length(k));
x1t=zeros(1,length(k)); x2t=zeros(1,length(k));
x1tp=zeros(1,length(k)); x2tp=zeros(1,length(k));
e = zeros(1,length(k)); v = zeros(1,length(k));
u = zeros(1,length(k));
Ep(1) = r(1)-y(1);

for i=2:length(k)
    x1(i)=T*x1p(i-1)+x1(i-1);
    x2(i)=T*x2p(i-1)+x2(i-1);
    x1t(i) = T*x1tp(i-1)+x1t(i-1);
    x2t(i) = T*x2tp(i-1)+x2t(i-1);
    E(i) =T*Ep(i-1)+E(i-1);
    y(i) = C(1)*x1(i);
    yt(i)=C(1)*x1t(i);
    Ep(i) = r(i)-y(i);
    v(i) = K(1)*x1(i)+K(2)*x2(i);
    u(i) = Ki*E(i)-v(i);
    x1p(i) = A(1,1)*x1(i)+A(1,2)*x2(i);
    x2p(i) = A(2,1)*x1(i)+A(2,2)*x2(i)+B(2)*u(i);
    e(i) = y(i)-yt(i);
    x1tp(i) = A(1,1)*x1t(i)+A(1,2)*x2t(i)+B(1)*u(i)+Ke(1)*e(i);
    x2tp(i) = A(2,1)*x1t(i)+A(2,2)*x2t(i)+B(2)*u(i)+Ke(2)*e(i);
end
figure()
plot(k*T,y,'*g'); hold on; plot(k*T,y,'or'),step(AA,BB,CC,DD,1,t),legend('Sa�da sistema - Eq.Rec','Sa�da observador - Eq.Rec','Step - sistema anal�gico');hold off

figure();
plot(k*T,Ep,'*','DisplayName','Erro');
hold on;
plot(k*T,u,'o','DisplayName','A��o de controle');
plot(k*T,r,'-','DisplayName','Entrada do sistema');
grid;legend;
hold off;


figure();
subplot(2,2,1); plot(k*T,x1);
title('Comportamento vari�vel x1');grid;
subplot(2,2,3); plot(k*T,x2);
title('Comportamento vari�vel x2');grid;
subplot(2,2,2); plot(k*T,x1t);
title('Comportamento vari�vel x1t');grid;
subplot(2,2,4); plot(k*T,x2t);
title('Comportamento vari�vel x2t');grid;

