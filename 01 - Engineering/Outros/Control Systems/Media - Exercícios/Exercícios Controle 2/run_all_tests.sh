#!/usr/bin/env bash
# This is intended to run from within the containers, from the Exercícios Controle 2 directory
#export PYTHONPATH=../

mypy .
python3 -m unittest discover -s tests -v