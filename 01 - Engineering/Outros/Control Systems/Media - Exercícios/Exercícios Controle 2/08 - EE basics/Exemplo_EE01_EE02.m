% Representa��o de sistemas no espa�o de estados
% Circuito RLC S�rie

% parametros do circuito
R=5;
L=5e-3;
C=100e-6;

% Matrizes
A = [0 1/C;-1/L -R/L];
B = [0; 1/L];
C = [1 0];
D = 0;

% sistema no espa�o de estados
sys = ss(A,B,C,D)

% convertendo para fun��o de tranfer�ncia com a utiliza��o da Equa��o
s=tf([1 0],1);
G=C*inv(s*eye(2)-A)*B+D


% convertendo para fun��o de tranfer�ncia com a utiliza��o da fun��o do
% Matlab
[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d)

% convertendo para o espa�o de estados a partir da fun��o de transfer�ncia
a1 = d(2);
a2 = d(3);

A = [0 1;-a2 -a1];
B = [0;n(3)];
C = [1 0];
D = 0;
sys2 = ss(A,B,C,D)

% Resposta ao degrau
step(sys)
hold on
step(G)
step(G2)
step(sys2)
hold off

