%Sistemas de controle II
% Avalia��o te�rica 2
% Rayan M. Steinbach
% Quest�o 01

clc; clear all; close all;

t = 0:1e-9:1e-3;
R = 5; L=10e-3;C=1000e-6; K=0.2;
RC = R*C;
A = [-1/RC 1/C; -1/L 0]
B = [-K/RC; 1/L]
C = [1 0]
D = K

[ysis] = step(A,B,C,D,1,t);

% Equa��es recursivas
T = 1e-6;
tf = 1e-3;
k=0:tf/T;

u  = ones (1,length(k));
x1 = zeros(1,length(k));
x2 = zeros(1,length(k));
x1p= zeros(1,length(k));
x2p= zeros(1,length(k));
y  = zeros(1,length(k));
y(1)=C(1)*x1(1)+C(2)*x2(1)+D*u(1);
for i=2:length(k)
   x1(i) = T*x1p(i-1)+x1(i-1);
   x2(i) = T*x2p(i-1)+x2(i-1);
   
   y(i)  = C(1)*x1(i)+C(2)*x2(i)+D*u(i);
   
   x1p(i)= A(1,1)*x1(i)+A(1,2)*x2(i)+B(1)*u(i);
   x2p(i)= A(2,1)*x1(i)+A(2,2)*x2(i)+B(2)*u(i);
end

hold on
plot(k*T,y,'*','DisplayName', 'Eq. recursiva');
plot(t,ysis,'r','Linewidth',2,'DisplayName', 'Sistema cont�nuo');
grid;legend;
hold off;

