%Sistemas de controle II
% Avalia��o te�rica 2
% Rayan M. Steinbach
% Quest�o 03

clc; clear all; close all;
t = 0:0.001:10;

A = [0 1;-3 -2]; B = [0;1];
C = [1 0]; D = 0;

[ysis,xsis] = step(A,B,C,D,1,t);
figure('Name','Resposta ao degrau sistema original');
subplot(3,1,1);
plot(t,ysis);
title('Sa�da sistema original');
grid;

subplot(3,1,2);
plot(t,xsis(:,1));
title('Comportamento de x1');
grid;

subplot(3,1,3);
plot(t,xsis(:,2));
title('Comportamento de x2');
grid;

[num,den] = ss2tf(A,B,C,D);
G2 = zpk(tf(num,den))
polos = roots(den)

% Controlabilidade
Mc=ctrb(A,B); 
rMc = rank(Mc)

% Observabilidade
Mo = obsv(A, C);
rMo = rank(Mo)

%Projeto de controlador
zeta = 0.7; wn = 4;
s1 = -zeta*wn + 1j*wn*sqrt(1-zeta^2)
s2=-zeta*wn - 1j*wn*sqrt(1-zeta^2)
s3 = -10*abs(s1)
S = [s1,s2,s3];

Ahat = [A,zeros(length(A),1);-C,0];
Bhat = [B;0];
disp('Khat = [K1 K2 | -Ki]');
Khat = acker(Ahat,Bhat,S)

K = [Khat(1),Khat(2)]
Ki = -Khat(3)
AA = [A-B*K,B*Ki;-C,0];
BB = [0;0;1];
CC = [C 0];
DD = D;

PolosControlado = eig(Ahat-Bhat*Khat)

[ycon, xcon] = step(AA,BB,CC,DD,1,t);
figure();
step(AA,BB,CC,DD,1,t);
hold on
step(A,B,C,D,1,t); grid;
legend('Sistema Controlado', 'Sistema orginal');
hold off;

disp('Valores Te�ricos');
Mpt = exp(-pi*zeta/sqrt(1-zeta^2))*100
tpt = pi/(wn*sqrt(1-zeta^2))

disp('Valores simulados');
INFO = stepinfo(ycon, t, 'SettlingTimeThreshold',0.05);
Mp = (INFO.SettlingMax - ycon(end))/(ycon(end)-ycon(1))*100
tp = INFO.PeakTime



