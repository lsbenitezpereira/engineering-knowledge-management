%% ========================================================================
%Lista 2

%% ------------------------------------------------------------------------
%  ************************************************************************
%%Quest�o 3:
clear; clc;

T=0.002;

G1s = tf(1, [1 3])
G2s = tf(1, [1 5])

G1z = c2d(G1s, T, 'zoh')
G2z = c2d(G2s, T, 'impulse')/T

FTMA = G1z*G2z;
FTMF = feedback(G1z*G2z, 1)

Kp = dcgain(FTMA)
Kv = dcgain(FTMA*tf([1 -1], 1, T))/T
Ka = dcgain(FTMA*tf([1 -1], 1, T)*tf([1 -1], 1, T))/T^2

erro_degrau = 1/(1+Kp)
erro_rampa = 1/Kv
erro_parabola = 1/Ka

%% Ploter
figure(1)
rlocus(FTMA)

figure(2)
step(FTMF)

figure(3)
pzmap(FTMF)

%% Recursiva Blocos Separados:

[num_G1z, den_G1z] = tfdata(G1z ,'v');
[num_G2z, den_G2z] = tfdata(G2z ,'v');

plotSize=50;
kT=(0:plotSize-1)*T;
c=zeros(1, plotSize);
e=zeros(1, plotSize);
yG1z=zeros(1, plotSize);
r=zeros(1, plotSize);
r(1:plotSize)=1;
e(1)=r(1);
yG1z(1)=(1/den_G1z(1))*(num_G1z(1)*e(1));
c(1)=(1/den_G2z(1))*(num_G2z(1)*yG1z(1));

for n=2:plotSize
    e(n)=r(n)-c(n-1);
    yG1z(n)=(1/den_G1z(1))*(num_G1z(1)*e(n)+num_G1z(2)*e(n-1)-den_G1z(2)*yG1z(n-1));
    c(n)=(1/den_G2z(1))*(num_G2z(1)*yG1z(n)+num_G2z(2)*yG1z(n-1)-den_G2z(2)*c(n-1));
    e(n)=r(n)-c(n);
    yG1z(n)=(1/den_G1z(1))*(num_G1z(1)*e(n)+num_G1z(2)*e(n-1)-den_G1z(2)*yG1z(n-1));
    c(n)=(1/den_G2z(1))*(num_G2z(1)*yG1z(n)+num_G2z(2)*yG1z(n-1)-den_G2z(2)*c(n-1));
end

plot(kT, c, 'x')
hold on
plot(kT, e, '*')
hold on
step(FTMF)
hold off
%% ------------------------------------------------------------------------
%  ************************************************************************
%%Quest�o 4:
clear; clc;

T=0.1;

Gs = tf(1, [1 3]);
Hs = tf(1, [1 5]);

Gz = c2d(Gs, T, 'zoh');
Hz = c2d(Hs, T, 'zoh');

[num_Gz, den_Gz]=tfdata(Gz, 'v');
[num_Hz, den_Hz]=tfdata(Hz, 'v');

FTMF = feedback(Gz, Hz);
FTMF = zpk(FTMF);

[num_FTMF, den_FTMF] = tfdata(FTMF, 'v');

FTMA = Gz*Hz;
FTMA = zpk(FTMA);

step(FTMF);
hold on

Cz_step = FTMF*tf([1 0], [1 -1], T)
Cz_step = minreal(Cz_step)

% Cz_step=zpk(Cz_step)
% Cz_step =
%  
%     0.086394 z (z-0.9608)
%   --------------------------
%   (z-0.911) (z-1) (z-0.7906)

syms z k;

ck_step_sym = iztrans((0.086394*z*(z-0.9608))/((z-0.911)*(z-1)*(z-0.7906)), k)

plotSize = 100;
kT=T*(0:plotSize-1);
ck_step = double(subs(ck_step_sym, k, kT));
plot(kT, real(ck_step),'rx')
hold off

% rlocus(FTMA)

%% Recursiva Blocos Separados:
plotSize = 100;
kT=T*(0:plotSize-1);
c=zeros(1, plotSize);
e=zeros(1, plotSize);
r=zeros(1, plotSize);
b=zeros(1, plotSize);

r(1:plotSize)=1;
e(1)=r(1);
c(1)=(1/den_Gz(1))*(num_Gz(1)*e(1));
b(1)=(1/den_Hz(1))*(num_Hz(1)*c(1));

for n=2:plotSize   
    e(n)=r(n)-b(n-1);
    c(n)=(1/den_Gz(1))*(num_Gz(1)*e(n)+num_Gz(2)*e(n-1)-den_Gz(2)*c(n-1));
    b(n)=(1/den_Hz(1))*(num_Hz(1)*c(n)+num_Hz(2)*c(n-1)-den_Hz(2)*b(n-1));
    e(n)=r(n)-b(n);
    c(n)=(1/den_Gz(1))*(num_Gz(1)*e(n)+num_Gz(2)*e(n-1)-den_Gz(2)*c(n-1));
    b(n)=(1/den_Hz(1))*(num_Hz(1)*c(n)+num_Hz(2)*c(n-1)-den_Hz(2)*b(n-1));
end

plot(kT, c, '+')
hold on
step(FTMF);
hold on

%% Recursiva Bloco Inteiro:
kT=T*(0:plotSize-1);
c=zeros(1, plotSize);
e=zeros(1, plotSize);
r=zeros(1, plotSize);
b=zeros(1, plotSize);

r(1:plotSize)=1;

c(1)=(1/den_Gz(1))*(num_FTMF(1)*r(1));
c(2)=(1/den_Hz(1))*(num_FTMF(1)*r(2)+num_FTMF(2)*r(1)-den_FTMF(2)*c(1));

for n=3:plotSize   
    c(n)=(1/den_Gz(1))*(num_FTMF(1)*r(n)+num_FTMF(2)*r(n-1)+num_FTMF(3)*r(n-2)-den_FTMF(2)*c(n-1)-den_FTMF(3)*c(n-2));
end

plot(kT, c, 'x')
hold off

%% ------------------------------------------------------------------------
clear; clc;
%  ************************************************************************
%%Quest�o 5:
T=0.1;

G1s = tf(1, [1 5]);
G2s = tf(1, [1 3]);

G1z = c2d(G1s, T, 'zoh');
G2z = c2d(G2s, T, 'zoh');

FTMA = G2z;
FTMF = feedback(G2z, 1);

Cz_step=G1z*FTMF*tf([1 0], [1 -1], T)
% Cz_step=minreal(Cz_step)
Cz_step=zpk(Cz_step)
% Cz_step =
%  
%           0.0067987 z
%   ---------------------------
%   (z-1) (z-0.6544) (z-0.6065)
syms z k;
ck = iztrans((0.0067987*z)/((z-1)*(z-0.6544)*(z-0.6065)), z, k)


step(G1z*FTMF)
hold on
ezplot(ck)
hold off