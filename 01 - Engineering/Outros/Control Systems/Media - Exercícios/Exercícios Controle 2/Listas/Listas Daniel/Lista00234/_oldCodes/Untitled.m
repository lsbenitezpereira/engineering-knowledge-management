clear; clc;

G1s = tf(1, [1 0]);
G2s = tf([1 0], [1 2]);
H2s = G1s;

T=0.1;

G1z = c2d(G1s, T, 'zoh');
G2z = c2d(G2s, T, 'zoh');
H2z = c2d(H2s, T, 'zoh');

[num_G1z, den_G1z] = tfdata(G1z, 'v');
[num_G2z, den_G2z] = tfdata(G2z, 'v');
[num_H2z, den_H2z] = tfdata(H2z, 'v');

Gz = G1z*feedback(G2z, H2z);

plotSize=25;
kT=(0:plotSize-1)*T;

r=zeros(1, plotSize);
x=zeros(1, plotSize);
e=zeros(1, plotSize);
c=zeros(1, plotSize);
v=zeros(1, plotSize);

r(1:plotSize)=1;
x(1)=(1/den_G1z(1))*(num_G1z(1)*r(1));
e(1)=x(1);
c(1)=(1/den_G2z(1))*(num_G2z(1)*e(1));
v(1)=(1/den_H2z(1))*(num_H2z(1)*c(1));
for n=2:plotSize
    x(n)=(1/den_G1z(1))*(num_G1z(1)*r(n) + num_G1z(2)*r(n-1) - den_G1z(2)*x(n-1));
    
    e(n)=x(n)-v(n-1);
    c(n)=(1/den_G2z(1))*(num_G2z(1)*e(n) + num_G2z(2)*e(n-1) - den_G2z(2)*c(n-1));
    v(n)=(1/den_H2z(1))*(num_H2z(1)*c(n) + num_H2z(2)*c(n-1) - den_H2z(2)*v(n-1));
    
    e(n)=x(n)-v(n);
    c(n)=(1/den_G2z(1))*(num_G2z(1)*e(n) + num_G2z(2)*e(n-1) - den_G2z(2)*c(n-1));
    v(n)=(1/den_H2z(1))*(num_H2z(1)*c(n) + num_H2z(2)*c(n-1) - den_H2z(2)*v(n-1));
end

plot(kT, c, 'bx');
hold on
step(Gz);
hold on

c_exato = 0.355*(1-(exp(-3.302.*kT)));
plot(kT, c_exato, 'rO')
hold off

FTMA=G2z*H2z;

Kp = dcgain(FTMA)
ess = 1/(1+Kp)

%%
T=0.5;

GCalphaz = tf([1 -0.5], 1, T);
GCbetaz = tf(1, [1 -0.3125], T);
Gz = tf(1, [1 -0.5], T);
Hz = tf([1 0], [1 -1], T);
z1 = 0.5 +1j*0.25;

FTMA = GCbetaz*GCalphaz*Gz*Hz;

figure(1)
pzmap(FTMA)
hold on
zplane([], z1)
ylim([-1.2 1.2])
xlim([-1.2 1.2])
hold off

figure(2)
rlocus(FTMA)
hold on
zplane([], z1)
ylim([-1.2 1.2])
xlim([-1.2 1.2])
hold off

KC=abs(evalfr(1/FTMA, z1))

GC=KC*GCalphaz*GCbetaz
FTMF=feedback(GC*Gz,Hz);
figure(3)
pzmap(FTMF)

%%
clear; clc;
Gs = tf(1, [1 0]);
H = 1/2;

T=0.4;
Gz = c2d(Gs, T, 'zoh')
[num_Gz, den_Gz]=tfdata(Gz, 'v');

FTMFz = feedback(Gz, H*tf(1, [1 0], T));
FTMAz = Gz*H;

plotSize=25;
k=0:plotSize-1;
kT =k*T;

r=zeros(1, plotSize);
e=zeros(1, plotSize);
c=zeros(1, plotSize);

for n=2:plotSize
    r(n)=r(n-1)+1;
end
e(1)=r(1);
c(1)=(1/den_Gz(1))*(num_Gz(1)*e(1));

for n=2:plotSize
    e(n) = r(n)-H*c(n-1);
    c(n)=(1/den_Gz(1))*(num_Gz(1)*e(n)+num_Gz(2)*e(n-1)-den_Gz(2)*c(n-1));
    
%     e(n) = r(n)-H*c(n);
%     c(n)=(1/den_Gz(1))*(num_Gz(1)*e(n)+num_Gz(2)*e(n-1)-den_Gz(2)*c(n-1));    
end

figure(1)
hold on
plot(kT, r, 'gx')
plot(kT, e, 'r+')
plot(kT, c, 'b*')
step(FTMFz*tf(1, [1 -1], T))
xlim([0 fix(plotSize*T)])
hold off

Kp=dcgain(FTMAz)
Kv=(1/T)*dcgain(FTMAz*tf([1 -1], 1, T))