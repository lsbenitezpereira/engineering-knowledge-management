clear;
clc;

%% Lista 003 - Exercicio 1
T = 0.1; %s

G1 = tf(1,[1 0]);
G2 = tf([1 0],[1 2]);
Hs = tf(1,[1 0]);

G1z = c2d(G1,T,'zoh');
G2z = c2d(G2,T,'zoh');
Hz = c2d(Hs,T,'zoh');

FTMF2 = minreal(G2z/(1+(G2z*Hz)));

% a)
FTT = minreal(G1z*FTMF2);

% b)
Rz = tf([1 0],[1 -1],T); %degrau

Cz = FTT*Rz

[num,den] = tfdata(Cz,'v');

syms z k
Cz_sym = simplify(num(2)*z/(den(1)*z^2 + den(2)*z + den(3)))

ck = iztrans(Cz_sym, z, k);
