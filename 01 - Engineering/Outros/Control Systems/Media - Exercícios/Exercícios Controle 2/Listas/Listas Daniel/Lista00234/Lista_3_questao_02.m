clear
clc
 syms z
 T=.5;
 z=0.5+0.25i
 GH=z/((z-0.3)*(z-1))
 k=1/abs(GH)
 
 G=tf([k], [1 -.3], T)
 H=tf([1 0], [1 -1], T)
 FTMA=(G*H)
 ftmf=minreal(G/(1+FTMA))
 FTMF=zpk(ftmf)
 roots( [1 -0.9798 0.3])