clear
clc

T=0.1;

G1s=tf([1],[1 0]);
G1z=c2d(G1s,T)

G2s=tf([1 0],[1 2]);
G2z=c2d(G2s,T)

Hs=tf([1],[1 0]);
Hz=c2d(Hs,T)

FTMF=minreal(G1z*(G2z/(1+G2z*Hz)))

step(FTMF)
syms z
V=(0.1/(z-0.7187))*(z/(z-1));
VF=limit((1-z^(-1))*V,z,1)

hold on
kmax=20;
c(1)=0;
for k=2:kmax
    c(k)=0.7187*c(k-1)+0.1;
end
t=0:1:kmax-1;
stem(t*T,c)

%VF apos o bloco G1---->isso � um integrador-->RAMPA
V=(0.1/(z-1))*(z/(z-1));
VF=limit((1-z^(-1))*V,z,1)

%Erro ao degrau
FTMA=zpk(G2z*Hz)
syms z;
GH=(0.1)/(z-0.8187);
kp=limit(GH,z,1)
ess=1/(1+kp)

hold on;
k = 0:1:kmax;
C=-0.35549*(0.7187).^k+0.35549;

stem(k*T,C)