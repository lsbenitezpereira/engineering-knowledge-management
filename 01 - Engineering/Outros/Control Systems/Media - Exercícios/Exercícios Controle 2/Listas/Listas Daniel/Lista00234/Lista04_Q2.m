clear;
clc;

%% Lista 004 - Exercicio 2
T = 0.25; %s

G1 = tf(1,[1 0]);
G1z = c2d(G1,T,'zoh');

G2 = tf(1,[1 4]);
G2z = c2d(G2,T,'zoh');

Hs = tf(1,[1 0]);
GHz = c2d(G2*Hs,T,'zoh');

Rz = tf([1 0],[1 -1],T); %Degrau
Xz = Rz*G1z;

FT = 1/(1+GHz);

TVF = tf([1 -1],[1 0],T);

LIM = TVF*FT*Xz;

[num,den] = tfdata(LIM);
ess = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1))   % lim z --> 1

% Fazendo o teste ao Degrau
FTMF2 = G2z/(1+GHz);

FTT = G1z*FTMF2;
step(FTT)