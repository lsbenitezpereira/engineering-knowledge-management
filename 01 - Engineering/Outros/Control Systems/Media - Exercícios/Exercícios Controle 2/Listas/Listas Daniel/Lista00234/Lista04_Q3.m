clear;
clc;

%% Lista 004 - Exercicio 3
T = 0.1; %s

FTMF = tf(0.1694,[1 -1.5 0.6694],T);

step(FTMF)
%%
[z,p,k] = zpkdata(FTMF, 'v');
figure(1);
zplane(z,p);

% Para K = 1.694 Temos que z = 0.75 +j0.327

z = 0.75+j*0.327;

Z = abs(z)
ANG = angle(z)

csiWn = -log(Z)/T

Ts5 = 3/csiWn;

[Wnt,csit] = damp(FTMF);
Ts5t = 3/(csit(1)*Wnt(1))


% ANG = T*Wn*sqrt(1-csi^2)
% Wn*sqrt(1-csi^2) = ANG/T

% csiWn                 ANG
% -----*sqrt(1-csi^2) = ---
% csi                    T

%                       ANG
% csiWn*sqrt(1-csi^2) = --- * csi
%                        T

csi = sqrt((csiWn^2)/((ANG/T)^2 + csiWn^2));

Mp = exp(-(pi*csi)/(sqrt(1-csi^2)))
