%% Parâmetros Transitórios:

T=0.01;

FTMFz = tf([0.4674 -0.3393], [1 -1.5327 0.6607], T)
FTMFz = zpk(FTMFz)

Gz = FTMFz/(1 -FTMFz)
Gz = minreal(Gz)

[num, den] = tfdata(FTMFz, 'v');
roots(den)

fun_zeta = @(zeta) zeta/sqrt(1-zeta^2) - 0.6103
zeta = fzero(fun_zeta, 0.1)

figure(1)
pzmap(FTMFz)
grid on

figure(2)
step(FTMFz)
hold on
step(feedback(Gz/0.4674, 1))
