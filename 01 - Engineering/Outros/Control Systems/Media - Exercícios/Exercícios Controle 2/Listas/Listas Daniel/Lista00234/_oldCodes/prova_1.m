%% ========================================================================
% Quest�o 1
T=0.1;

G1s = tf(1, [1 0]);
G2s = tf(1, [1 1]);
Hs = tf(1, [1 2]);

Gs = G1s + G2s;
FTMAs = Gs*Hs;

G1z = c2d(G1s, T);
G2z = c2d(G2s, T);
Gz = c2d(Gs, T);
FTMAz = c2d(FTMAs, T);

FTMFz = Gz/(1+FTMAz);

figure(1)
step(FTMFz)

figure(2)
pzmap(FTMFz)

%% ------------------------------------------------------------------------
% Equa��o Recursiva

[G1z_num, G1z_den] = tfdata(G1z, 'v');
[G2z_num, G2z_den] = tfdata(G2z, 'v');
[FTMAz_num, FTMAz_den] = tfdata(FTMAz, 'v');

plotSize = 20;
k = 0:plotSize-1;
kT = k*T;

r = zeros(1, plotSize);
e = zeros(1, plotSize);
a = zeros(1, plotSize);
b = zeros(1, plotSize);
c = zeros(1, plotSize);
v = zeros(1, plotSize);

r(1:plotSize)=1;

i=1;
v(i)=0;
e(i)=r(i)-v(i);
a(i)=0;
b(i)=0;
c(i)=a(i)+b(i);

i=2;
v(i)=(1/FTMAz_den(1))*(FTMAz_num(2)*e(i-1)-FTMAz_den(2)*v(i-1));
e(i)=r(i)-v(i);
a(i)=(1/G1z_den(1))*(G1z_num(2)*e(i-1)-G1z_den(2)*a(i-1));
b(i)=(1/G2z_den(1))*(G2z_num(2)*e(i-1)-G2z_den(2)*b(i-1));
c(i)=a(i)+b(i);

i=3;
v(i)=(1/FTMAz_den(1))*(FTMAz_num(2)*e(i-1)+FTMAz_num(3)*e(i-2)-FTMAz_den(2)*v(i-1)-FTMAz_den(3)*v(i-2));
e(i)=r(i)-v(i);
a(i)=(1/G1z_den(1))*(G1z_num(2)*e(i-1)-G1z_den(2)*a(i-1));
b(i)=(1/G2z_den(1))*(G2z_num(2)*e(i-1)-G2z_den(2)*b(i-1));
c(i)=a(i)+b(i);
    
for i=4:plotSize
    v(i)=(1/FTMAz_den(1))*(FTMAz_num(2)*e(i-1)+FTMAz_num(3)*e(i-2)+FTMAz_num(4)*e(i-3)-FTMAz_den(2)*v(i-1)-FTMAz_den(3)*v(i-2)-FTMAz_den(4)*v(i-3));  
    a(i)=(1/G1z_den(1))*(G1z_num(2)*e(i-1)-G1z_den(2)*a(i-1));
    b(i)=(1/G2z_den(1))*(G2z_num(2)*e(i-1)-G2z_den(2)*b(i-1));
    c(i)=a(i)+b(i);
    e(i)=r(i)-v(i);
end

figure(3)
plot(kT, c, '*')
hold on
plot(kT, e, 'X')
hold on
step(FTMFz)
xlim([0 2])
hold off

%% ========================================================================
% Quest�o 2
T=0.25;

G1s = tf(1, [1 0]);
G2s = tf(1, [1 4]);
H2s = tf(1, [1 0]);

FTMA2s = G2s*H2s;

G1z = c2d(G1s, T);
G2z = c2d(G2s, T);
FTMA2z = c2d(FTMA2s, T);

%C�lculo de Erro:
Kp = dcgain(FTMA2z)
Kv = (1/T)*dcgain(FTMA2z*tf([1 -1], 1, T))
Ka = (1/(T^2))*dcgain(FTMA2z*(tf([1 -1], 1, T))^2)

erro_degrau = 1/(1+Kp)
erro_rampa = 1/Kv
erro_parabola = 1/Ka

%Como o integrador G1z integra o degrau o transformando numa rampa, o erro
%ao degrau se torna erro a rampa. Resposta da quest�o: erro_rampa.

%% ========================================================================
% Quest�o 3
T=0.1;

% c(k+2)-1.5*c(k+1)+0.5*c(k)=0.1*e(k)
% (z^2)*Cz - 1.5*z*Cz + 0.5*Cz = 0.1*Ez
% Gz = Cz/Ez = 0.1/((z^2)-1.5*z+0.5)
Gz = tf(0.1, [1 -1.5 0.5], T);

FTMAz = Gz;

z1=0.75+1j*0.327;
%abs(K*FTMAz)=1 quando z=z1
K = abs(evalfr((1/FTMAz), z1));

FTMFz = feedback(K*Gz, 1);
step(FTMFz)