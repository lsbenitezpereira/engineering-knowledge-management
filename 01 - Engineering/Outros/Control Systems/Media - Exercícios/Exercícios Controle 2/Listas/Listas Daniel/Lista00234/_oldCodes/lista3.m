%% ========================================================================
%Lista 3
%  ************************************************************************
%%Quest�o 1:
clear; clc;

T=0.1;

G1s = tf(1, [1 0]);
G2s = tf([0 1], [1 2]);
H2s = tf(1, [1 0]);

G1z = c2d(G1s, T, 'zoh');
G2z = c2d(G2s, T, 'zoh');
H2z = c2d(H2s, T, 'zoh');

FTMF2z = feedback(G2z, H2z);

Gz = G1z*FTMF2z
Gz = zpk(Gz)
Gz = minreal(Gz)

% XSTEPz = tf([1 0], [1 -1], T);
% 
% GStepResp = Gz*XSTEPz
% GStepResp  = zpk(GStepResp)
% 
% [GStepResp_num, GStepResp_den] = tfdata(GStepResp, 'v');
% 
% z=sym('z');
% GStepResp_z_syms = poly2sym(GStepResp_num, z)/poly2sym(GStepResp_den, z)
% 
% k=sym('k');
% % GStepResp_k_syms = iztrans(GStepResp_z_syms, k)
% 
% % GStepResp =
% %  
% %        0.1 z
% %   ----------------
% %   (z-0.7187) (z-1)
% GStepResp_k_syms = iztrans(0.1*z/((z-0.7187)*(z-1)), k)
% 
% % ezplot(GStepResp_k_syms);
% 
% plotSize = 100;
% kT=T*(0:plotSize-1);
% ck_step = double(subs(GStepResp_k_syms, k, kT));
% plot(kT, real(ck_step),'rO')

% Gz =
%  
%         0.1 z^2 - 0.2 z + 0.1
%   ----------------------------------
%   z^3 - 2.719 z^2 + 2.437 z - 0.7187

%y(n)=0.1*x(n-1)-0.2*x(n-2)+0.1*x(n-3)+2,719*y(n-1)-2.437*y(n-2)+0.7187*y(n-3);

%% ------------------------------------------------------------------------
% Erro Regime permanente:
 
FTMA2z = G2z*H2z
Kp = dcgain(FTMA2z)

Kv = (1/T)*dcgain(FTMA2z*tf([1 -1], 1, T))
Ka = (1/(T^2))*dcgain(FTMA2z*tf([1 -1], 1, T)*tf([1 -1], 1, T))
erro_p = 1/(1+Kp)
erro_v = 1/Kv
erro_a = 1/Ka

%% --------------------------------------------------------------------------
%%Equa��o Recursiva (sistema inteiro):

[Gz_num, Gz_den] = tfdata(Gz, 'v');

plotSize=24;
x=zeros(1,plotSize);
for n=4:plotSize
    x(n)=1;
end
y=zeros(1,plotSize);
for n=4:plotSize
    y(n)=(1/Gz_den(1))*(Gz_num(2)*x(n-1)+Gz_num(3)*x(n-2)+Gz_num(4)*x(n-3)-Gz_den(2)*y(n-1)-Gz_den(3)*y(n-2)-Gz_den(4)*y(n-3));
end
figure(1)
plot(1:plotSize, y, 'x');
hold on
plot(1:plotSize, x, '*');
hold off
figure(3)
step(Gz)

%% --------------------------------------------------------------------------
%%Equa��o Recursiva (sistema separado):
[G1z_num, G1z_den] = tfdata(G1z, 'v');
[F2z_num, F2z_den] = tfdata(FTMF2z, 'v');

plotSize=24;
x=zeros(1,plotSize);
for n=4:plotSize
    x(n)=1;
end
y1=zeros(1,plotSize);
y2=zeros(1,plotSize);
for n=4:plotSize
    y1(n)=(1/G1z_den(1))*(G1z_num(2)*x(n-1)+G1z_den(2)*y(n-1));
    y2(n)=(1/F2z_den(1))*(F2z_num(1)*y1(n)+F2z_num(2)*y1(n-1)+F2z_num(3)*y1(n-2)-F2z_den(2)*y2(n-1)-F2z_den(3)*y2(n-2));
end
figure(2)
plot(1:plotSize, y, 'x');
hold on
plot(1:plotSize, x, '*');
hold off
figure(3)
step(Gz)

%% --------------------------------------------------------------------------
%%Equa��o Recursiva (sistema separado):
[G1z_num, G1z_den] = tfdata(G1z, 'v');
[G2z_num, G2z_den] = tfdata(G2z, 'v');
[H2z_num, H2z_den] = tfdata(H2z, 'v');

plotSize = 25;

kT=(0:plotSize-1)*T;

r=zeros(1, plotSize);
x=zeros(1, plotSize);
e=zeros(1, plotSize);
c=zeros(1, plotSize);
b=zeros(1, plotSize);

r(1:plotSize)=1;
x(1)=(1/G1z_den(1))*(G1z_num(1)*r(1));
e(1)=x(1);
c(1)=(1/G2z_den(1))*(G2z_num(1)*e(1));
b(1)=(1/H2z_den(1))*(H2z_num(1)*c(1));

for n=2:plotSize
    x(n)=(1/G1z_den(1))*(G1z_num(1)*r(n) + G1z_num(2)*r(n-1) - G1z_den(2)*x(n-1));
    e(n)=x(n)-b(n-1);
    c(n)=(1/G2z_den(1))*(G2z_num(1)*e(n) + G2z_num(2)*e(n-1) - G2z_den(2)*c(n-1));
    b(n)=(1/H2z_den(1))*(H2z_num(1)*c(n) + H2z_num(2)*c(n-1) - H2z_den(2)*b(n-1));
    e(n)=x(n)-b(n);
    c(n)=(1/G2z_den(1))*(G2z_num(1)*e(n) + G2z_num(2)*e(n-1) - G2z_den(2)*c(n-1));
    b(n)=(1/H2z_den(1))*(H2z_num(1)*c(n) + H2z_num(2)*c(n-1) - H2z_den(2)*b(n-1));
end

plot(kT, e, 'rx')
hold on
plot(kT, x, 'bx')
plot(kT, c, '*')
plot(kT, b, '+')
step(Gz)
hold off
%% ========================================================================
%  ************************************************************************
clc; clear;
%%Quest�o 2:

T=0.5;


Gc_num_z=tf([1 -0.5], 1, T);
G1_z=tf(1,[1 -0.5], T);
H1_z=tf([1 0],[1 -1], T);

FTMA_z = G1_z*H1_z*Gc_num_z;

z1 = 0.5 + 1j*0.25;

figure(1)
pzmap(FTMA_z)
hold on
zplane([],z1)
xlim([-2 2])
hold off

beta = 0.3126;
Gc_z=tf([1 -0.5],[1 -beta], T);

FTMA_z = G1_z*H1_z*Gc_z;

figure(2)
rlocus(FTMA_z)
hold on
zplane([],z1)
xlim([-2 2])
hold off

Kc = abs(evalfr(1/FTMA_z, z1));
FTMA_z = Kc*FTMA_z;

FTMF_z = feedback(Kc*Gc_z*G1_z, H1_z);
figure(3)
pzmap(FTMF_z)

figure(4)
step(FTMF_z)

Kp = dcgain(FTMA_z);
Kv = (1/T)*dcgain(FTMA_z*tf([1 -1], 1, T));
Ka = (1/(T^2))*dcgain(FTMA_z*tf([1 -1], 1, T)*tf([1 -1], 1, T));
erro_p = 1/(1+Kp);
erro_v = 1/Kv;
erro_a = 1/Ka;

%% ========================================================================
%  ************************************************************************
clc; clear;
%%Lista 4: Quest�o 1:

%% ========================================================================
%  ************************************************************************
clc; clear;
%%Quest�o 3 - parte 1:
T=1;

Gz = tf([1 -1], [1 -0.8187], T);
Hz = tf(0.1, [1 -1], T);
z1 = 0.3187;

FTMAz = Gz*Hz;
FTMAz = minreal(FTMAz);

K = abs(evalfr(1/FTMAz, z1));

%% ========================================================================
%  ************************************************************************
clc; clear;
%%Quest�o 2 - parte 2:

T=0.4;

Gs = tf(1, [1 0]);
Gz = c2d(Gs, T, 'zoh');
H = 0.5;

FTMFz = feedback(Gz, H);
step(FTMFz);
hold on

[num_Gz, den_Gz]=tfdata(Gz, 'v')

plotSize = 25;
kT = (1:plotSize)*T;

e=zeros(1, plotSize);
c=zeros(1, plotSize);
b=zeros(1, plotSize);
x=zeros(1, plotSize);
x(1:plotSize)=1;

e(1)=x(1)-b(1);
% c(1)=(1/den_Gz(1))*(num_Gz(1)*e(1));

for n=2:plotSize
    b(n)=H*c(n-1);
    e(n)=x(n)-b(n);
    c(n)=(1/den_Gz(1))*(num_Gz(1)*e(n)+num_Gz(2)*e(n-1)-den_Gz(2)*c(n-1));  
end

plot(kT, x, '+');
hold on
plot(kT, e, 'x');
hold on
plot(kT, c, '*');
hold off

%% Residue
clc; clear;
T=0.1;

Gz = tf(1, [1 2], T)*tf(1, [1 2], T)*tf([0 1], [1 -1], T)
Gz = zpk(Gz)

[num, den]=tfdata(Gz, 'v')
[r,p,k_aux] = residue(num, den)

syms z k;

Gz_res_sym = (z*r(1)/(z-p(1)))+(z*r(2)/(z-p(2))^2)+(z*r(3)/(z-p(3)))
gk = iztrans(Gz_res_sym, 'k')
gk = iztrans(z/((z+2)*(z+2)*(z-1)), 'k')
% ezplot(gk)

iztrans(z*r(1)/(z-p(1)), 'k')
iztrans(z*r(2)/(z-p(2)^2), 'k')
iztrans(z*r(3)/(z-p(3)), 'k')