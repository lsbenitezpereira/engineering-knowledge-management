%% ========================================================================
%Lista 4

%% ************************************************************************
clear; clc;
%%Quest�o 1 - Parte 1:
T=0.1;

G1s = tf(1, [1 0]);
G2s = tf([1 0], [1 2]);
H2s = tf(1, [1 0]);

STEPz = tf(1, [1 -1], T);
G1z = c2d(G1s, T, 'zoh');
[num_G1z, den_G1z] = tfdata(G1z, 'v');
G2z = c2d(G2s, T, 'zoh');
[num_G2z, den_G2z] = tfdata(G2z, 'v');
H2z = c2d(H2s, T, 'zoh');
[num_H2z, den_H2z] = tfdata(H2z, 'v');

FTMF2z = feedback(G2z, H2z);

Gz = G1z*FTMF2z;
Gz = minreal(Gz)
CzSTEPz = Gz*STEPz;
CzSTEPz = zpk(CzSTEPz)

figure(1)
hold on
grid on

step(Gz)

plotSize = 100;
kT=T*(0:plotSize-1);
plot(kT, 0.3555*(1-exp(-3.3031*kT)),'ro')

%%-------------------------------------------------------------------------
%Equa��o Recursiva:
x=zeros(1, plotSize);
e=zeros(1, plotSize);
v=zeros(1, plotSize);
c=zeros(1, plotSize);
r=zeros(1, plotSize);
r(1:plotSize)=1;

x(1)=(1/den_G1z(1))*(num_G1z(1)*r(1));
e(1)=x(1);
c(1)=(1/den_G2z(1))*(num_G2z(1)*e(1));
v(1)=(1/den_H2z(1))*(num_H2z(1)*c(1));

for i=2:plotSize
    x(i)=(1/den_G1z(1))*(num_G1z(1)*r(i)+num_G1z(2)*r(i-1)-den_G1z(2)*x(i-1));
    
    %Estimativa de um sinal de feedback presente:
    e(i)=x(i)-v(i-1);
    c(i)=(1/den_G2z(1))*(num_G2z(1)*e(i)+num_G2z(2)*e(i-1)-den_G2z(2)*c(i-1));
    v(i)=(1/den_H2z(1))*(num_H2z(1)*c(i)+num_H2z(2)*c(i-1)-den_H2z(2)*v(i-1));
    
    %C�lculo da equa��o recursiva baseado na estimativa de feedback:
    e(i)=x(i)-v(i);
    c(i)=(1/den_G2z(1))*(num_G2z(1)*e(i)+num_G2z(2)*e(i-1)-den_G2z(2)*c(i-1));
    v(i)=(1/den_H2z(1))*(num_H2z(1)*c(i)+num_H2z(2)*c(i-1)-den_H2z(2)*v(i-1));
    
end

plot(kT, c,'b*')
hold off
%% ************************************************************************
clear; clc;
%%Quest�o 2 - Parte 2:
T=0.4;

Gs = tf(1, [1 0]);
H = 1/2;

Gz = c2d(Gs, T, 'zoh')

FTMF = feedback(Gz, H);
FTMA = Gz*H;

%Erros de regime permanente:
Kp = dcgain(FTMA)
ep = 1/(1+Kp)
Kv = (1/T)*dcgain(FTMA*tf([1 -1], 1, T))
ev = 1/Kv
Ka = (1/T^2)*dcgain(FTMA*tf([1 -1], 1, T)*tf([1 -1], 1, T))
ea = 1/Ka

figure(1)
hold on
grid on

step(FTMF)

Cztep = FTMF*tf([1 0], [1 -1], T)
zpk(Cztep)

% Cztep =
%  
%       0.4 z
%   -------------
%   (z-1) (z-0.8)


plotSize = 100;
kT=T*(0:plotSize-1);
plot(kT, 2*(1-exp(-0.5579.*kT)),'g*')

hold off
%% ************************************************************************
clear; clc;
%%Quest�o 3 - Parte 1:
T=1;

K=1;
Gz = K*tf([1 -1], [1 -0.8187], T)
Hz = tf(0.1, [1 -1], T)

FTMF = feedback(Gz, Hz)
FTMF = minreal(FTMF)

figure(1)
step(FTMF)

Cztep = FTMF*tf([1 0], [1 -1], T)
Cztep = minreal(Cztep)

% Cztep =
%  
%       z
%   ----------
%   z - 0.7187

syms z k
ck_step_sym = iztrans(z/(z - 0.7187), k)

hold on
grid on
plotSize = 100;
kT=T*(0:plotSize-1);
ck_step = double(subs(ck_step_sym, k, kT));
plot(kT, real(ck_step),'rx')
hold off


