T = 0.1;

%% Dominio cont�nuo (s)
G1 = tf(1, [1 0]);
G2 = tf([1 0], [1 2]);
Hs = tf(1, [1 0]);

%% Dominio discreto (z)
G1z = c2d(G1, T, 'zoh');
G2z = c2d(G2, T, 'zoh');
G3z = c2d(Hs, T, 'zoh');

FTMFz = G2z/(1 + G2z*G3z);

Tz = G1z*FTMFz;

Tz = minreal(Tz)

%% Resposta ao degrau

z = tf('z', T);
Rz = z / (z - 1);

Cz = Tz*Rz;

%Cz = zpk(Cz)

TOL = sqrt(1e-7);

Cz = minreal(Cz, TOL)

%% Transformando a fun��o para simbolico

[num_Cz, den_Cz] = tfdata(Cz, 'v')

%z = sym('z');
z = tf('z', T);

%Cz_sym = poly2sym(num_Cz, z)/poly2sym(den_Cz, z)
Cz_sym = poly2sym(num_Cz)/poly2sym(den_Cz)

%% Separando em fra��es parciais

aux = tf(1, [1 0], T);

Cz_z = minreal(aux*Cz, TOL)

[num_Cz_z, den_Cz_z] = tfdata(Cz_z, 'v')

[r,p,k_aux] = residue(num_Cz_z, den_Cz_z)

k = 0:1:25
Ck = zeros(size(k,2),1);

%for n = 1:1:25
%    for i = 1:1:size(r,2)
%        Ck(n) = r(i,1)*p(i,1).^(n-1);
%    end
%end

Ck = r(1,1)*p(1,1).^k + r(2,1)*p(2,1).^k;

%% Gr�ficos

step(Tz);
hold on;
plot(k*T, Ck, 'o');

% Cz_z =
%  
%         0.1
%   ----------------
%   (z-1) (z-0.7187)

%% TESTE

%iztrans(FTMFz)

%z = tf('z', T);

% M�todo das fra��es parciais
% Tz_z = Tz/z;
% 
% zero_FTMFz = zero(Tz_z);
% polos_FTMFz = pole(Tz_z);
% 
% [r, p, k] = residue(zero_FTMFz(:)', polos_FTMFz(:)')
% 
% Cz_z = r(1,1)/(z - p(1,1)) + r(2,1)/(z - p(2,1))
% 
% iztrans(Cz_z)

% FTMFz =
%  
%         0.1 z^2 - 0.2 z + 0.1             C(z)
%   ---------------------------------- = ----------
%   z^3 - 2.719 z^2 + 2.437 z - 0.7187      R(z)
%  
% Sample time: 0.1 seconds
% Discrete-time transfer function.

% (z^3 - 2.719 z^2 + 2.437 z - 0.7187) * C(z) = (0.1*z^2 - 0.2*z + 0.1)*R(z)
% dividir tudo pelo maior coef. de z -> logo z^3
% (1 - 2.719*z^-1 + 2.437*z^-2 - 0.7187*z^-3) * C(z) = 
% (0.1*z^-1 - 0.2*z^-2 + 0.1*z^-3) * R(z)

% transformada inversa

% c(k) = 0.1*r(k-1) - 0.2*r(k-2)+ 0.1*r(k-3) + 2.719*c(k-1) - 2.437*c(k-2) + 0.7187*c(k-3)
