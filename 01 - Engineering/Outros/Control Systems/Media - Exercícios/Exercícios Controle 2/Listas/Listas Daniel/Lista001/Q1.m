clear;
clc;

%Numero de letras de:
% DanielHenriqueCamargodeSouza
NT = 28;

%% Exercicio 1
V0 = 5; %V
R = 1;
C = 1; 
T = 0.4;   %Periodo de Amostragem

% Sistema de 1º Grau
%
%                   K
%       Hs =   --------------
%               tau*s + 1

%% a)
tau = R*C;

num = 1;
den = [tau 1];

Hs = tf(num,den);
Hz = c2d(Hs,T,'zoh');   % Amostrada

%% b)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelo do sistema
%      u     ----------    c   
%    -----> |  G(z)  | -----> 
%           ----------         
%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% C(z)      0.3297
% ---- =  ----------
% U(z)    z - 0.6703

% z*C(z) - 0.6703*C(z) = 0.3297*U(z)
% c(k+1) - 0.6703*c(k) = 0.3297*u(k)
% c(k+1) = 0.6703*c(k) + 0.3297*u(k)
% Mudança de Variável --> k => k - 1
% c(k) = 0.6703*c(k-1) + 0.3297*u(k-1)

[numHz,denHz] = tfdata(Hz,'v');
t = 0:T:30*T;

u=ones(1,length(t));    % Simular com o Degrau Unitário
c=zeros(1,length(t));

for k=2:length(u)-1
   c(k) = -denHz(2)*c(k-1) + numHz(2)*u(k-1);
end

figure(1)
step(Hz,'r');
hold on;
step(Hs,'b');
%plot(t(1,1:length(t)),g1(1,1:length(g1)),'r*');
plot(t,c,'g*');
hold off;
grid on;

%% c)
% c(k+1) - 0.6703*c(k) = 0.3297*u(k)
% Z{c(k+1)} - 0.6703*Z{c(k)} = 0.3297*Z{u(k)}
% z*Vc(Z) - z*Vc(0) - 0.6703*Vc(Z) = 0.3297*Vs(Z)
% Vc(Z)*[z - 0.6703] = 0.3297*Vs(Z) + z*Vc(0)
% 
%            0.3297                  z*Vc(0)
% Vc(z) =  ------------ * Vs(z) + ------------
%           z - 0.6703              z - 0.6703
%        |                     |  |            |
%        -----------------------  -------------
%                   |                   |
%               Vc_z_vs             Vc_z_v0

syms k z;

Vc_z_v0 = simplify((z*V0)/(z+denHz(2)));        % Descrevendo Vc_z_v0
Vc_k_v0 = iztrans(Vc_z_v0, z, k);               % Z Inversa de Vc_z_v0

figure(2)
ezplot(Vc_k_v0,[0, 30]);

%% d)
Vs_kt = exp(-2*k*T);                            % Sinal de Entrada
Vs_kt_z = simplify(ztrans(Vs_kt, k, z));        % Transformada Z do Sinal de Entrada

Vc_z_vs = simplify(numHz(2)/(z + denHz(2)));    % Descrevendo Vc_z_vs
Vc_z_vs = simplify(Vc_z_vs*Vs_kt_z);            % Multiplica pela Entrada             

Vc_k_vs = iztrans(Vc_z_vs, z, k);

% Extraindo os Valores;
Vc_kvs = zeros(1,30);
for k=1:length(Vc_kvs)
    Vc_kvs(k) = double(simplify(subs(Vc_k_vs)));
end

figure(3)
plot(Vc_kvs,'r*');

% Prova Real
hold on
syms k;
Vck = 1.49*(0.67)^k - 1.49*(0.4493)^k;      % Equação Obtina na MÃO (Calculos)
ezplot(Vck,[0, 30])

%ht = iztrans(Hz_impulse)
% Vc_kt_t = zeros(1,length(t));
% Vs_kt = zeros(1,length(t));
% 
% syms z;
% [Num,Den] = tfdata(Hz_impulse);
% sys_syms = poly2sym(cell2mat(Num),z)/poly2sym(cell2mat(Den),z);