clc;
clear;
%% Lista 1 - Exercicio 3
T = 1;

G1s = tf(1,[1 3]);
% G1s =   1
%       -----
%        s+3

G1z = c2d(G1s,T);
%           0.3167
% G1z = -------------
%        z - 0.04979


% G2s =   1
%       -----
%        s+5

G2z = tf([1 0],[1 -exp(-5)],T);
H = 1;

Grz = G1z*G2z; % ftma
ftma = Grz;
zpk(ftma)
eq_c = 1+ftma   % Equa��o caracter�stica
[z,p,k] = zpkdata(eq_c,'v');

figure(1);
zplane(z,p);

ftmf = feedback(ftma,H);
[z,p,k] = zpkdata(ftmf,'v');

figure(2);
zplane(z,p);

figure(3);
step(ftmf);
hold on;

%% Equa��o Recursiva
% FTMF = C(z)/R(z)
% FTMF =           0.3167 z
%        --------------------------
%        z^2 + 0.2602 z + 0.0003355
% c(k) = 0.3167*r(k)-0.2602*c(k-1)-0.0003355*c(k-2)

k = 0:24;
rk = ones(1,length(k));
% Condi��o inicial p/ k = 0
c(1) = 0;
% k = 1
c(2) = 0.315;

for j=3:length(k),
    c(j)= 0.3167*rk(j)-0.2602*c(j-1)-0.0003355*c(j-2);
end

plot(k*T,c,'*');

%% Equa��o Recursiva
%    r          e   ---------          c
%      --> +( )---> | FTMA(z) |  --------->
%            -      ---------       |
%            ^                      |
%            |                      |
%            ------------------------
%
% FTMA(z) = C(z)/E(z)
%
%              FTMA(z)
% C(z) = ------------------ * R(z)
%          1 + FTMA(z)*H(z)
%
%  E(z)        1
% ----- = -------------
%  R(z)    1 + FTMA(z)
%
%  E(z)     z^2 - 0.05653*z + 0.003355
% ------ = ----------------------------
%  R(z)     z^2 - 0.2602*z  + 0.003355
%
% e(k) = r(k)-0.05653*r(k-1)+0.003355*r(k-2)+0.2602*e(k-1)-0.003355*e(k-2)
% 
%                      0.3167*z             C(z)
% FTMA(z) = ----------------------------- * ----
%            z^2 - 0.05653*z + 0.0003355    E(z)
%
% c(k) = 0.05653*c(k-1)-0.0003344*c(k-2)+0.3167*e(k-1)

% Condi��o inicial 
% p/ k = 0
c(1) = 0;
e(1) = rk(1)-c(1);
% p/ k = 1
c(2) = 0.315;
e(2) = rk(2)-c(2);

for j=3:length(k),
    c(j) = 0.05653*c(j-1)-0.0003344*c(j-2)+0.3167*e(j-1);
    e(j) = rk(j)-c(j);
end

plot(k*T,c,'o');

%% Equa��o Recursiva (Perguntar sobre isso)

%    r          e   ---------  u   --------    c
%      --> +( )---> | G1(z) | ---> | G2(z) | ------->
%            -      ---------      --------     |
%            ^                                  |
%            |                                  |
%            ------------------------------------

% U(z)        0.3167
% ----  = -------------
% E(z)     z - 0.04979

% z*uz-0.04979*uz = 0.3167*ez
% u(k) - 0.04979*u(k-1) = 0.3167*e(k)
% u(k) = 0.3167*e(k) + 0.04979*u(k-1)

% C(z)          z
% ----  = --------------
% U(z)     z - 0.006738

% z*uz = z*cz-0.006738*cz
% u(k) = c(k) - 0.006738*c(k-1)
% c(k) = u(k) + 0.006738*c(k-1)

% Condi��es Iniciais p/ k = 0
c(1) = 0;
e(1) = rk(1)-c(1);
u(1) = 0.3167*e(1);

for j=2:length(k)
    c(j) = u(j-1) + 0.006738*c(j-1);
    e(j) = rk(j)-c(j);
    u(j) = 0.3167*e(j) + 0.04979*u(j-1);
end

figure(4);
step(ftmf);
hold on;
plot(k*T,c,'*');

hold off;
