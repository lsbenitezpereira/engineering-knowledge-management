clear;
clc;

%% Exercicio 5
T = 1;

G1 = tf(1,[1 5]);
G2 = tf(1,[1 3]);

G1z = c2d(G1,T,'zoh');      % Com Segurador de Ordem Zero
G2z = c2d(G2,T,'zoh')      % Com Segurador de Ordem Zero

FTMFG2z = feedback(G2z,1)     % Malha Fechada no Bloco 2

FTMA = G1z*FTMFG2z

FTMF = feedback(FTMA,1)

%% a) Cálculo dos Erros! (Degrau, Rampa, Iperbole)

                    % SALVANDO APENAS
                    %syms x;
                    %FT_Kv_sym = poly2sym(cell2mat(num),x)/poly2sym(cell2mat(den),x);
                    %Kv = double(limit(FT_Kv_sym,1));

% Erro ao Degrau Kp e ess_kp
%
%        lim (G(z)H(z))   
%   Kp = z->1  
%                

[num,den] = tfdata(FTMA);
Kp = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));   % lim z --> 1
ess_kp = 1/(1+Kp)

% Erro a Rampa Kv e ess_kv
% 
%        lim      (1 - z^-1) G(z)H(z)      lim  (z - 1) 
%   Kv = z->1  ----------------------  =   z->1 -------  * FTMA ou Gz
%                        T                       T * z

num = [1 -1];
den = [T 0];
FT_Kv = tf(num,den,T);
FT_Kv = FT_Kv*FTMA;

[num,den] = tfdata(FT_Kv);
Kv = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_kv = 1/Kv

% Erro a Parabola Ka e ess_ka
%
%        lim      (1 - z^-1)^2 G(z)H(z)     lim  (z^2 - 2*z + 1) 
%   Ka = z->1  ----------------------    =  z->1 -------------  * FTMA ou Gz
%                        T^2                        T^2 * z^2

num = [1 -2 1];
den = [T^2 0 0];
FT_Ka = tf(num,den,T);
FT_Ka = FT_Ka*FTMA;

[num,den] = tfdata(FT_Ka);
Ka = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_ka = 1/Ka

%% b) Lugar das Raizes
figure(1);
rlocus(FTMA);

% Posição dos Polos de Malha Fechada
[z,p,k] = zpkdata(FTMF, 'v');
figure(2);
zplane(z,p);

%% c) Eq Recursiva

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelo do sistema
%      u    -----------    c   
%    -----> |  FTMA   | -----> 
%           -----------         
%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% c(z)          0.06292
% ---- = ------------------------
% u(z)   z^2 + 0.2602 z + 0.06112

% z^2*c(z) + 0.2602*z*c(z) + 0.06112*c(z) = 0.06292*u(z)
% c(k+2) + 0.2602*c(k+1) + 0.06112*c(k) = 0.06292*u(k)
% c(k+2) = - 0.2602*c(k+1) - 0.06112*c(k) + 0.06292*u(k)
% Mudança de Variavel k = k - 2
% c(k) = 0.2602*c(k-1) - 0.06112*c(k-2) + 0.06292*u(k-2)

[num,den] = tfdata(FTMF,'v');
t = -2*T:T:30*T;

u=ones(1,length(t));    % Simular com o Degrau Unitário
u(1) = 0;
u(2) = 0;

c=zeros(1,length(t));

for k=3:length(c)
   c(k) = -den(2)*c(k-1) -den(3)*c(k-2) + num(3)*u(k-2);
end

figure(3)
step(FTMF,'r');
hold on;
%step(Hs,'b');
%plot(t(1,1:length(t)),g1(1,1:length(g1)),'r*');
plot(t,c,'b*');
hold off;
grid on;


%% d) Ws/Wd
[Wn,csi] = damp(FTMF);

Wd = sqrt(1-csi.^2).*Wn;      %Frequência Natural Amortecida

Ws = (2*pi)/T;

A = Ws./Wd;      %Relação de Amostragem Satisfatória (8 a 10)