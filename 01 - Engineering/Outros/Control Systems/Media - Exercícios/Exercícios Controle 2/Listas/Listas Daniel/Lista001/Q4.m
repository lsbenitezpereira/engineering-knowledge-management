clear;
clc;

%% Exercicio 4
T = 1;

Gs = tf(1,[1 3]);
Hs = tf(1,[1 5]);

Gz = c2d(Gs,T,'zoh');      % Com Segurador de Ordem Zero
Hz = c2d(Hs,T,'zoh');      % Com Segurador de Ordem Zero

FTMA = Gz*Hz;
FTMF = Gz/(1+Gz*Hz)

%% a) Cálculo dos Erros! (Degrau, Rampa, Iperbole)

                    % SALVANDO APENAS
                    %syms x;
                    %FT_Kv_sym = poly2sym(cell2mat(num),x)/poly2sym(cell2mat(den),x);
                    %Kv = double(limit(FT_Kv_sym,1));

% Erro ao Degrau Kp e ess_kp
%
%        lim (G(z)H(z))   
%   Kp = z->1  
%                

[num,den] = tfdata(FTMA);
Kp = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));   % lim z --> 1
ess_kp = 1/(1+Kp)

% Erro a Rampa Kv e ess_kv
% 
%        lim      (1 - z^-1) G(z)H(z)      lim  (z - 1) 
%   Kv = z->1  ----------------------  =   z->1 -------  * FTMA ou Gz
%                        T                       T * z

num = [1 -1];
den = [T 0];
FT_Kv = tf(num,den,T);
FT_Kv = FT_Kv*FTMA;

[num,den] = tfdata(FT_Kv);
Kv = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_kv = 1/Kv

% Erro a Parabola Ka e ess_ka
%
%        lim      (1 - z^-1)^2 G(z)H(z)     lim  (z^2 - 2*z + 1) 
%   Ka = z->1  ----------------------    =  z->1 -------------  * FTMA ou Gz
%                        T^2                        T^2 * z^2

num = [1 -2 1];
den = [T^2 0 0];
FT_Ka = tf(num,den,T);
FT_Ka = FT_Ka*FTMA;

[num,den] = tfdata(FT_Ka);
Ka = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_ka = 1/Ka

%% b) Lugar das Raizes
figure(1);
rlocus(FTMA);

% Posição dos Polos de Malha Fechada
[z,p,k] = zpkdata(FTMF, 'v');
figure(2);
zplane(z,p);

%% c) Eq Recursiva

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelo do sistema
%      u    -----------    c   
%    -----> |  FTMF   | -----> 
%           -----------         
%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% C(z)       0.3167 z^2 - 0.0179 z + 0.0001063
% ---- =  ---------------------------------------
% U(z)    z^3 - 0.1063 z^2 + 0.06607 z - 0.003149

% z^3*C(z) - 0.1063*z^2*C(z) + 0.06607*z*C(z) - 0.003149*C(z) = 0.3167*z^2*U(z) - 0.0179*z*U(z) + 0.0001063*U(z)
% c(k+3) - 0.1063*c(k+2) + 0.06607*c(k+1) - 0.003149*c(k) = 0.3167*u(k+2) - 0.0179*u(k+1) + 0.0001063*u(k)
% c(k+3) = + 0.1063*c(k+2) - 0.06607*c(k+1) + 0.003149*c(k) + 0.3167*u(k+2) - 0.0179*u(k+1) + 0.0001063*u(k)
% Mudança de Variavel k = k - 3
% c(k) = + 0.1063*c(k-1) - 0.06607*c(k-2) + 0.003149*c(k-3) + 0.3167*u(k-1) - 0.0179*u(k-2) + 0.0001063*u(k-3)

[num,den] = tfdata(FTMF,'v');
t = -3*T:T:30*T;

u=ones(1,length(t));    % Simular com o Degrau Unitário
u(1) = 0;
u(2) = 0;
u(3) = 0;

c=zeros(1,length(t));

for k=4:length(c)
   c(k) = -den(2)*c(k-1) -den(3)*c(k-2) -den(4)*c(k-3) + num(2)*u(k-1) + num(3)*u(k-2) + num(4)*u(k-3);
end

figure(3)
step(FTMF,'r');
hold on;
%step(Hs,'b');
%plot(t(1,1:length(t)),g1(1,1:length(g1)),'r*');
plot(t,c,'b*');
hold off;
grid on;


%% d) Ws/Wd
[Wn,csi] = damp(FTMF);

Wd = sqrt(1-csi.^2).*Wn;      %Frequência Natural Amortecida

Ws = (2*pi)/T;

A = Ws./Wd;      %Relação de Amostragem Satisfatória (8 a 10)