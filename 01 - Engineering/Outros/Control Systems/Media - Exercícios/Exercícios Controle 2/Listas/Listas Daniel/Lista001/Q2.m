clear;
clc;

%Numero de letras de:
% DanielHenriqueCamargodeSouza
NT = 28;

%% Exercicio 2
R = 3;
C = 1;
L = 1;
T = 0.4;   %Periodo de Amostragem

% Sistema de 2º Grau
%
%                        Wn^2
%       Hs =   -------------------------
%               s^2 + 2*csi*Wn*s + Wn^2 

%% a)
Wn = sqrt(1/L*C);

% 2*csi*Wn = R/L
csi = (R/L)/(2*Wn);

num = Wn^2;
den = [1 2*csi*Wn Wn^2];

Hs = tf(num,den);
Hz = c2d(Hs,T,'zoh');   % Amostrada

%% b)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelo do sistema
%      u     ----------    c   
%    -----> |  G(z)  | -----> 
%           ----------         
%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% C(z)    0.05501 z + 0.03695
% ---- = ----------------------
% U(z)   z^2 - 1.209 z + 0.3012

% z^2*C(z) - 1.209*z*C(z) + 0.3012*C(z) = 0.05501*z*U(z) + 0.03695*U(z)
% c(k+2) - 1.209*c(k+1) + 0.3012*c(k) = 0.05501*u(k+1) + 0.03695*u(k)
% c(k+2) = 1.209*c(k+1) - 0.3012*c(k) + 0.05501*u(k+1) + 0.03695*u(k)
% Mudança de Variável --> k => k - 2
% c(k) = 1.209*c(k-1) - 0.3012*c(k-2) + 0.05501*u(k-1) + 0.03695*u(k-2)

[numHz,denHz] = tfdata(Hz,'v');
t = -2*T:T:30*T;

u=ones(1,length(t));    % Simulando com o Degrau Unitário
u(1) = 0;   % t = -2*T
u(2) = 0;   % t = -T

c=zeros(1,length(t));

for k=3:length(u)
   c(k) = -denHz(2)*c(k-1) -denHz(3)*c(k-2) +numHz(2)*u(k-1) +numHz(3)*u(k-2);
end

figure(1)
step(Hz,'r');
hold on;
step(Hs,'b');
%plot(t(1,1:length(t)),g1(1,1:length(g1)),'r*');
plot(t,c,'g*');
hold off;
grid on;

%% c)
% c(k+2) - 1.209*c(k+1) + 0.3012*c(k) = 0.05501*u(k+1) + 0.03695*u(k)
% Z{c(k+2)} - 1.209*Z{c(k+1)} + 0.3012*Z{c(k)} = 0.05501*Z{u(k+1)} + 0.03695*Z{u(k)}
% [z²C(Z) - z²*C(0) - z*C(1)] -1.209*[z*C(Z) - z*C(0)] + 0.3012*C(z) = 0.05501*[z*U(Z) - z*U(0)] + 0.03695*U(z)
% C(z)*[z² -1.209*z + 0.3012] = U(z)*[0.05501*z + 0.03695] + C(0)*[z² -1.209*z] +z*C(1) - U(0)*0.05501*z
%
%
%         0.05501 z + 0.03695                    z^2 - 1.209*z                            z                           0.05501*z
% C(z) = ---------------------- * U(z)  +  ------------------------ * C(0) + ------------------------ * C(1)  + ------------------------ * U(0)
%        z^2 - 1.209 z + 0.3012             z^2 - 1.209 z + 0.3012            z^2 - 1.209 z + 0.3012             z^2 - 1.209 z + 0.3012
%       |                             |   |                                                                                                    |
%       -------------------------------   ------------------------------------------------------------------------------------------------------
%                    |                                                                            |
%           Resposta a Entrada                                                       Resposta ao Estado Inicial
%
syms k z;

U_z = simplify(12*((z)/(z-1)));                   % Sinal de Entrada é o Degrau * 12V

C_z = simplify(((numHz(2)*z + numHz(3))/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*U_z); 

C_k = iztrans(C_z, z, k);

% Extraindo os Valores;
C_kd = zeros(1,30);
for k=1:length(C_kd)
    C_kd(k) = double(simplify(subs(C_k)));
end

figure(2)
plot(C_kd,'r.'); 
%% d)
C0 = 2;
C1 = 0;

syms k z;
% Resposta a Entrada Zero para C0 = 2 e C1 = 0... Temos:

C_z = simplify(((denHz(1)*z^2 + denHz(2)*z)/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*C0 ...
             + ((denHz(1)*z)/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*C1);

C_k = iztrans(C_z, z, k);

% Extraindo os Valores;
C_ke = zeros(1,30);
for k=1:length(C_kd)
    C_kd(k) = double(simplify(subs(C_k)));
end

figure(3)
plot(C_kd,'b*'); 

%% e)
C0 = 5;
C1 = 0;

U0 = 1; %Degrau

syms k z;
% Resposta Total para C0 = 5 e C1 = 0 e Entrada Degrau... Temos:
U_z = simplify((z)/(z-1));                   % Sinal de Entrada é o Degrau

C_z = simplify(((numHz(2)*z + numHz(3))/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*U_z ...
             + ((denHz(1)*z^2 + denHz(2)*z)/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*C0 ...
             + ((denHz(1)*z)/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*C1 ...
             + ((numHz(2)*z)/(denHz(1)*z^2 + denHz(2)*z + denHz(3)))*U0);

C_k = iztrans(C_z, z, k);

% Extraindo os Valores;
C_ke = zeros(1,30);
for k=1:length(C_ke)
    C_ke(k) = double(simplify(subs(C_k)));
end

figure(4)
plot(C_ke,'mo'); 