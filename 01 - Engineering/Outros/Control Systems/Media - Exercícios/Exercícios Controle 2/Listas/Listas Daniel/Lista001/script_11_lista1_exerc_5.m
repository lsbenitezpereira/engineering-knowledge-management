clc;
clear;
%% Lista 1 - Exercicio 5
T = 1;

% Definindo fun��es de transfer�ncias
G1s = tf(1,[1 5]);
% G1s =   1
%       -----
%        s+3

G1z = c2d(G1s,T);
% G1z =     0.1987
%       --------------
%        z - 0.006738

G2s = tf(1,[1 3]);
% G2s =   1
%       -----
%        s+3

G2z = c2d(G2s,T);
% G2z =     0.3167
%       --------------
%        z - 0.04979

Hz = 1;

% FTMA
ftma = G2z*Hz;
eq_c = 1+ftma   % Equa��o caracter�stica
[z,p,k] = zpkdata(eq_c,'v');

figure(1);
zplane(z,p);

% FTMF
ftmf = feedback(G2z,Hz);
ftmf = minreal(ftmf)
[z,p,k] = zpkdata(ftmf,'v');

figure(2);
zplane(z,p);

figure(3);
step(G1z*ftmf);
hold on;

%% Equa��o Recursiva
k = 0:24;
rk = ones(1,length(k));

%
%  rk             ------------------           c
% --------------> |G1(z) * FTMF(z) | ------------>
%                 ------------------
%
% C(z)             0.06292
% ----- = ---------------------------
% Rk(z)     z^2 + 0.2602z - 0.001799
%
% C(z)*z^2 + C(z)*0.2602 - C(z)*0.001799 = 0.06292*Rk(z)
% c(k) + 0.2602*c(k-1) - 0.001799*c(k-2) = 0.06292*rk(k-2)
% c(k) = 0.06292*rk(k-2)-0.2602*c(k-1)+0.001799*c(k-2);
%
% Condi��o inicial 
% p/ k = 0
c(1) = 0;
c(2) = 0;

for j=3:length(k)
    c(j) = 0.06292*rk(j-2)-0.2602*c(j-1)+0.001799*c(j-2);
end

plot(k*T,c,'o');

%% Equa��o Recursiva
%
%  rk  ---------     rb    -----------           c
% -----| G1(z) |---------> | FTMF(z) | ------------>
%      ---------           -----------
%
% C(z)       0.3167
% ----- = -------------
% Rb(z)     z + 0.267
%
% z*C(z) + 0.267*C(z) = 0.3167*Rb(z)
% c(k) + 0.267*c(k-1) = 0.3167*rb(k-1)
% c(k) = 0.3167*rb(k-1) - 0.267*c(k-1);
%
% Rb(z)      0.1987
% ----- = ---------------
% Rk(z)    z - 0.006738
%
% 0.1987*Rk(z) = z*Rb(z) - 0.006738*Rb(z)
% 0.1987*rk(k-1) = rb(k) - 0.006738*rb(k-1)
% rb(k) = 0.1987*rk(k-1) + 0.006738*rb(k-1);
%
% Condi��o inicial 
% p/ k = 0
c(1) = 0;
rb(1) = 0;

for j=2:length(k)
    rb(j) = 0.1987*rk(j-1) + 0.006738*rb(j-1);
    c(j) = 0.3167*rb(j-1) - 0.267*c(j-1);
end

plot(k*T,c,'*');

%  rk  --------- rb         e       ---------           c
% -----| G1(z) |----> +( )--------> | G2(z) | ------------>
%      ---------        -           ---------       |
%                       ^                           |
%                       |                           |
%                       -----------------------------
%

