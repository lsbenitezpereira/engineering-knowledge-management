clc;
clear;
%% Lista 1 - Exercicio 4
T = 1;
G1s = tf(1,[1 3]);
% G1s =   1
%       -----
%        s+3

G1z = c2d(G1s,T);
% G1z =     0.3167
%       --------------
%         z - 0.04979

G2s = tf(1,[1 5]);
% G2s =   1
%       -----
%        s+5

G2z = c2d(G2s,T);
% G2z =     0.1987
%       --------------
%        z - 0.006738

Hz = G2z;
ftma = G1z*Hz;
eq_c = 1+ftma   % Equa��o caracter�stica
[z,p,k] = zpkdata(eq_c,'v');

figure(1);
zplane(z,p);


ftmf = G1z/(1+(G1z*Hz));
ftmf = minreal(ftmf)
[z,p,k] = zpkdata(ftmf,'v');

figure(2);
zplane(z,p);

figure(3);
step(ftmf);
hold on;

%% Equa��o Recursiva
%
% FTMF = C(z)/R(z)
%
% FTMF =    0.3167 z - 0.002134
%        --------------------------
%        z^2 - 0.05653 z + 0.06326
%
% c(k)-0.05653*c(k-1)+0.06326*c(k-2) = 0.3167*r(k-1)-0.002134*r(k-2)
% c(k) = 0.05653*c(k-1)-0.06326*c(k-2)+0.3167*r(k-1)-0.002134*r(k-2)

k = 0:24;
rk = ones(1,length(k));

% Condi��o inicial p/ k = 0
c(1) = 0;
% k = 1
c(2) = 0.317;

for j=3:length(k),
    c(j) = 0.05653*c(j-1)-0.06326*c(j-2)+0.3167*rk(j-1)-0.002134*rk(j-2);
end

plot(k*T,c,'*');

%% Equa��o Recursiva
%
%    r          e        --------           c
%      --> +( )--------> | G(z) | ------------>
%            -           --------       |
%            ^                          |
%            |        u   --------      |
%            -------------| H(z) |<------
%                         --------
%
%  C(z)             0.3167
% ----- = G(z) = -------------
%  E(z)           z - 0.04979
%
% e(k-1)*0.3167 = c(k)-c(k-1)*0.04979
% c(k) = e(k-1)*0.3167+c(k-1)*0.04979
%
%  U(z)             0.1987
% ----- = H(z) = --------------
%  C(z)           z - 0.006738
%
% c(k-1)*0.1987 = u(k)-u(k-1)*0.006738
% u(k) = c(k-1)*0.1987+u(k-1)*0.006738
%
% e(k) = rk(k)-u(k)
%
% Condi��o inicial 
% p/ k = 0
c(1) = 0;
u(1) = 0;
e(1) = rk(1)-u(1);

for j=2:length(k)
    c(j) = e(j-1)*0.3167+c(j-1)*0.04979;
    u(j) = c(j-1)*0.1987+u(j-1)*0.006738;
    e(j) = rk(j)-u(j);
end

plot(k*T,c,'o');

hold off;