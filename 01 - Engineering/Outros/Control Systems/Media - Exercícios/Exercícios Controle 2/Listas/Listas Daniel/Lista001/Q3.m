clear;
clc;

%% Exercicio 3
T = 0.5;

G1s = tf(1,[1 3]);
%G2s = tf(1,[1 5]);

G1z = c2d(G1s,T,'zoh')      % Com Segurador de Ordem Zero
%G2z = c2d(G2s,T,'impulse')  % Impulse
G2z = tf([1 0],[1 -0.006737], T);

Gz = G1z*G2z

Hz = 1;

FTMA = Gz
FTMF = feedback(Gz,Hz)

%% a) Cálculo dos Erros! (Degrau, Rampa, Iperbole)

                    % SALVANDO APENAS
                    %syms x;
                    %FT_Kv_sym = poly2sym(cell2mat(num),x)/poly2sym(cell2mat(den),x);
                    %Kv = double(limit(FT_Kv_sym,1));

% Erro ao Degrau Kp e ess_kp
%
%        lim (GH(z))   
%   Kp = z->1  
%                

[num,den] = tfdata(FTMA);
Kp = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));   % lim z --> 1
ess_kp = 1/(1+Kp)

% Erro a Rampa Kv e ess_kv
%
%        lim      (1 - z^-1) GH(z)      lim  (z - 1) 
%   Kv = z->1  -------------------  =   z->1 -------  * FTMA ou Gz
%                        T                    T * z

num = [1 -1];
den = [T 0];
FT_Kv = tf(num,den,T);
FT_Kv = FT_Kv*FTMA;

[num,den] = tfdata(FT_Kv);
Kv = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_kv = 1/Kv

% Erro a Parabola Ka e ess_ka
%
%        lim      (1 - z^-1)^2 GH(z)      lim  (z^2 - 2*z + 1) 
%   Ka = z->1  ----------------------  =  z->1 -------------  * FTMA ou Gz
%                        T^2                    T^2 * z^2

num = [1 -2 1];
den = [T^2 0 0];
FT_Ka = tf(num,den,T);
FT_Ka = FT_Ka*FTMA;

[num,den] = tfdata(FT_Ka);
Ka = (polyval(cell2mat(num),1)/polyval(cell2mat(den),1));
ess_ka = 1/Ka

%% b) Lugar das Raizes
figure(1);
rlocus(FTMA);

% Posição dos Polos de Malha Fechada
[z,p,k] = zpkdata(FTMF, 'v');
figure(2);
zplane(z,p);

%% c) Eq Recursiva

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelo do sistema
%      u    -----------    c   
%    -----> |  FTMF   | -----> 
%           -----------         
%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% C(z)            0.3167 z
% ---- =  --------------------------
% U(z)    z^2 + 0.2602 z + 0.0003355

% z^2*C(z) + 0.2602*z*C(z) + 0.0003355*C(z) = 0.3167*z*U(z)
% c(k+2) + 0.2602*c(k+1) + 0.0003355*c(k) = 0.3167*u(k+1)
% c(k+2) = -0.2602*c(k+1) -0.0003355*c(k) + 0.3167*u(k+1)
% Mudança de Variavel k = k - 2
% c(k) = -0.2602*c(k-1) -0.0003355*c(k-2) + 0.3167*u(k-1)

[num,den] = tfdata(FTMF,'v');
t = -2*T:T:30*T;

u=ones(1,length(t));    % Simular com o Degrau Unitário
u(1) = 0;
u(2) = 0;

c=zeros(1,length(t));

for k=3:length(c)
   c(k) = -den(2)*c(k-1) -den(3)*c(k-2) + num(2)*u(k-1);
end

figure(3)
step(FTMF,'r');
hold on;
%step(Hs,'b');
%plot(t(1,1:length(t)),g1(1,1:length(g1)),'r*');
plot(t,c,'g*');
hold off;
grid on;


%% d) Ws/Wd
[Wn,csi] = damp(FTMF);

Wd = sqrt(1-csi.^2).*Wn;      %Frequência Natural Amortecida

Ws = (2*pi)/T;

A = Ws./Wd;      %Relação de Amostragem Satisfatória (8 a 10)