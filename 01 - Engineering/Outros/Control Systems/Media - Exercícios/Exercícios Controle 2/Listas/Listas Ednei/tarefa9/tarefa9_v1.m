%tarefa 9
clc; clear; close all;

A=[0 1;-8 -6];
B=[0; 1];
D=0;
X0=[1;0];

% Solu��o com a transformada de laplace
syms s
U=1/s;
X=inv(s*eye(2)-A)*X0+inv(s*eye(2)-A)*B*U
Y=simplify(X);
pretty(Y);
y=ilaplace(Y)
pretty(y);
autovalores = eig(A)


% comparando os resultados
% resposta da sulu��o encontrada
figure();
hold on;
t=0:.01:10;
y2=1.75*exp(-2*t)-0.875*exp(-4*t)+0.125 + 3.5*exp(-4*t)-3.5*exp(-2*t);
%y2=y(1)+y(2)
plot(t,y2,'o')



% resposta com o comando lsim do Matlab
u=ones(1,length(t));
C=[1 1];
D=0;
sistema = ss(A,B,C,D); 
y2=lsim(sistema,u,t,X0);
plot(t,y2,'*')
legend('y(t) da L^-^1','da Vari�veis de estado');


