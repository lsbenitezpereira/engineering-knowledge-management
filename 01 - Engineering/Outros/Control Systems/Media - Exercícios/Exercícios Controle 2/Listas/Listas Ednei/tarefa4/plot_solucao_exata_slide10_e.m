clc;clear;close all;

% fun��o impulso
k = 0:150;                    
delta = zeros(1,length(k));
delta(1) = 1;   % para k=0

% solu��o exata
xe1 = k.*((-1).^k) -1*(-1).^k ;
xe = 1*delta+xe1;
plot(k,xe,'*')

hold on
T=1;
num=[1]            % 1
den=[1 2 1]     %(z^2 2z +1)
FTMA=tf(num,den,T)


impulse(FTMA)
axis([0,150,-200,2e00]);
xlabel('D'); ylabel('Un'); title('Resposta ao Impluso Malha Aberta');



