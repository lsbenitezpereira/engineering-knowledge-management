clc; close all;
%Parte 1 - Tarefa 4
disp('Parte 1');
RC = 0.083; T = 0.1;
% Obten��o da fun��o de transferencia discreta
Gs1 = tf(1,[RC 1]) %#ok<*NOPTS>
Gz1 = c2d(Gs1,T)

%Resposta ao degrau
[sys1,kT1]= step(Gz1);


% Equa��o recursiva: y(k+1) = 0.7003e(k) + 0.2997y(k)
yk = zeros(1,length(kT1));
for k=1:length(kT1)-1
    yk(k+1) = 0.7003 + 0.2997*yk(k);
end

%Equa��o exata 
yex = zeros(1,length(kT1));
yex(2:length(kT1))= 1 - 0.2997.^(1:(length(kT1)-1));
figure('Name','Part 1 - degree response');
plot(kT1,sys1,'*k');
hold on
plot(kT1,yk,'or');
plot(kT1,yex,'xg');
legend('G(z)','recursiva','exata');
hold off