clc;clear;close all;

% fun��o impulso
k = 0:20;                    
delta = zeros(1,length(k));
delta(1) = 1;   % para k=0

% solu��o exata
xe1 = 4*(-1).^k -10*(-0.5).^k;
xe = 6*delta+xe1;
plot(k,xe,'*')

figure();
T=1;
num=[1 3]           % z + 3
den=[1 1.5 0.5]     %(z^2 +1,5z +0,5)
FTMA=tf(num,den,T)

impulse(FTMA)
axis([0,50,0,5]);