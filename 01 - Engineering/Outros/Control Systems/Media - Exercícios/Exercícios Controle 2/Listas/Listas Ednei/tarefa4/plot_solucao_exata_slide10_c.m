clc;clear;close all;

% fun��o impulso
k = 0:150;                    
delta = zeros(1,length(k));
delta(1) = 1;   % para k=0

% solu��o exata
xe1 = -261.38*(0.04174).^k +11.38*(0.958).^k;
xe = 250*delta+xe1;
plot(k,xe,'*')

hold on
T=1;
num=[10]            % 10
den=[1 -1 0.04]     %(z^2 -z +0,04)
FTMA=tf(num,den,T)

impulse(FTMA)
axis([0,150,0,12]);
xlabel('D'); ylabel('Un'); title('Resposta ao Impluso Malha Aberta');



%*******************************************************************
%Resposta ao Degrau;
figure();

% fun��o impulso
k = 0:150;                    
uk = ones(1,length(k));

% solu��o exata
xe1 = -261.38*(0.958).^k +11.38*(0.04174).^k;
xe = 250*uk+xe1;
plot(k,xe,'*')

hold on
T=1;
num=[10]            % 10
den=[1 -1 0.04]     %(z^2 -z +0,04)
FTMA=tf(num,den,T)


step(FTMA)
axis([0,150,0,300]);
xlabel('D'); ylabel('Un'); title('Resposta ao Degrau Malha Aberta');


hold off
