clc; close all;
%Parte 1 - Tarefa 4
disp('Parte 1');
RC = 0.083; T = 0.1;
% Obten��o da fun��o de transferencia discreta
Gs1 = tf(1,[RC 1]) %#ok<*NOPTS>
Gz1 = c2d(Gs1,T)

%Resposta ao degrau
[sys1,kT1]= step(Gz1);


% Equa��o recursiva: y(k+1) = 0.7003e(k) + 0.2997y(k)
yk = zeros(1,length(kT1));
for k=1:length(kT1)-1
    yk(k+1) = 0.7003 + 0.2997*yk(k);
end

%Equa��o exata 
yex = zeros(1,length(kT1));
yex(2:length(kT1))= 1 - 0.2997.^(1:(length(kT1)-1));
figure('Name','Part 1 - degree response');
plot(kT1,sys1,'*k');
hold on
plot(kT1,yk,'or');
plot(kT1,yex,'xg');
legend('G(z)','recursiva','exata');
hold off

%Parte 2 - Tarefa 4
disp('Parte 2');
T2 = 1;

%FTMF discreta
Gs2=tf(1,[1,1,0]);
Hs2=tf(1,1);
Gz2=c2d(Gs2,T2);
FTMF=minreal(Gz2/(1+Gz2))
[sys2,kT2]=step(FTMF);

%Equa��o recursiva: c(k+2) = c(k+1)-0.632c(k)+0.368r(k+1)+0.264r(k)
ck=zeros(1,length(kT2));
%k=-1
ck(2) = 0.368;
for k=1:length(kT2)-2
   ck(k+2)= ck(k+1)-0.632*ck(k)+0.368+0.264;
end

%Equa��o recursiva: c(k+2) = 1.368c(k+1)-0.368c(k)+0.368e(k+1)+0.264e(k)
%                   e(k) = r(k) - c(k)
%                   e(k+1) = r(k+1) - c(k+1)
cz=zeros(1,length(kT2));
%k=-1
cz(2) = 0.368;
for k=1:length(kT2)-2
   cz(k+2)= 1.368*cz(k+1)-0.368*cz(k)+0.368*(1-cz(k+1))+0.264*(1-cz(k));
end

figure('Name','Part 2 - System degree response');
plot(kT2,sys2,'*k'); 
hold on
plot(kT2,ck,'or'); 
plot(kT2,cz,'xg');
legend('FTMF', 'recursiva FTMF','recursiva G(z)');
hold off