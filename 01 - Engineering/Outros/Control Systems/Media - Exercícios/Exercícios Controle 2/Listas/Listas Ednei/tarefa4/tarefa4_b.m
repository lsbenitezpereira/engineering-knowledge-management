% Exemplo de Equacionamento da fun��o de transfer�ncia discreta de sistemas
% malha fechada
% tarefa 4 b)
clear;clf;clc;close all;
hold on

% Fun��es de Transfer�ncia Cont�nuas

Gs=tf(1,[1 1 0])
Hs=tf(1,1)

FTMFs=feedback(Gs,1)
step(FTMFs);

% Per�odo de amostragem
T = 1;

% Fun��o Transfer�ncia Discreta Ramo Direto
Gz=c2d(Gs,T)

% Fun��o de Transferencia Discreta Malha Aberta 
GHz=c2d(Gs*Hs,T)

% Fun��o de Transfer�ncia em malha fechada
% Caso a) medidor anal�gico
FTMFz=minreal(Gz/(1+GHz))  % n�o � possivel usar o comando feedback
step(FTMFz)

% entrada
k=0:40;
r=ones(1,length(k));

%equa��o recursiva do Sistema
% comportamento de c(k) a partir da equa��o recursiva
% c(k+2) = 0,3697r(k+1) +0,2642r(k) +c(k+1) -0,6321c(k)

% condi��es iniciais
xc(1) = 0;      % para k=0
xc(2) = 0.368;
for j = 1:length(k)-2
    xc(j+2) = 0.3697*r(j+1) +0.2642*r(j) +xc(j+1) -0.6321*xc(j);
end

plot(k*T,xc,'o');


% Equa��o recursiva Ramo direto
% c(k-2)=0,3679e(k+1)+0,264e(k)+1,368c(k+1)-0,3679c(k)
% e(k) = r(k) - c(k)

% condi��es iniciais
xc2(1) = 0;      % para k=0
xc2(2) = 0.368;

for j = 1:length(k)-2
    xc2(j+2)= 1.368*xc2(j+1)-0.3679*xc2(j)+0.3679*(r(j+1)-xc2(j+1))+0.264*(r(j+1)-xc2(j));
end
plot(k*T,xc2,'x');

xlabel('kT');
ylabel('x(kT)');
title('evolu��o temporal de c(kT)');
legend('G(s)','G(z)','recursiva', 'recursiva Ramo Direto');
axis([0,40,0,1.6]);


hold off
