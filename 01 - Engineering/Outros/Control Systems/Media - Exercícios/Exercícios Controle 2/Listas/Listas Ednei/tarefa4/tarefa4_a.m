% Exemplo de Equacionamento da fun��o de transfer�ncia discreta de um sistema cont�nuo mais amostrador ideal mais ZOH
% tarefa 4 a) Ednei Freiberger
clc;clear;close all;
hold on
RC=0.083
T=0.1     %per�odo de amostragem


% Fun��o de Transfer�ncia Cont�nua
num=[1]; 
den=[RC 1];
Gs=tf(num,den)
step(Gs);

% Fun��o de Transfer�ncia Discreta
Gz=c2d(Gs,T)
step(Gz);

%equa��o recursiva do Sistema
% comportamento de x(k) a partir da equa��o recursiva
% x(k+1) = ax(k) +be(k)
% e(k) = 0 , k<0
% e(k) = 1, k>=1

a=exp(-T/(RC))
b=1-exp(-T/(RC))
k=0:20

% entrada
ek=ones(1,length(k));

% condi��es iniciais
xr(1) = 0;      % para k=0

for j = 1:length(k)-1
    xr(j+1) = a*xr(j) +b*ek(j);
end


hold on
plot(k*T,xr,'o')


% solu��o exata
xe1 = -1*(0.2997).^k ;
xe = 1.*ek + xe1;
plot(k*T,xe,'*')

xlabel('kT')
ylabel('x(kT)')
title('evolu��o temporal de y(kT)')

legend('G(s)','G(z)','recursiva','exata');
axis([0,2,0,1.2]);


hold off