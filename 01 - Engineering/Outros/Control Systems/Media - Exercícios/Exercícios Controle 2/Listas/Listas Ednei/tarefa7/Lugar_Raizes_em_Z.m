%slide 10 Lugar Raizes Plano Z
clc;clear;close all;format long;


%Especifica��es Projeto
Zeta=0.5;
ts=2;    %Ts5%
T=0.2;   %Tempo Amostragem
num=[1];
den=[1 2 0];
Gs=tf(num,den);
Gz=c2d(Gs,T)

%ts2%=4/(Zeta*Wn)
Wn=4/(Zeta*ts);

%Wd=Wn*sqrt(1 - Zeta^2)
Wd=Wn*sqrt(1-Zeta^2)

%Ws=2*pi/T
Ws=2*pi/T

%Numero de pontos por Oscila��o
disp('N�mero de Pontos por Oscila��o:')
NPO=Ws/Wd

%Z1 Polo dominante de Malha Fechada desejado, Semi plano esq. sup.
s1=-Zeta*Wn+j*Wn*sqrt(1-Zeta^2);
z1=exp(s1*T)

%Mostra Fun��o Transferencia Gz
disp('Gz:')
zpk(Gz)

% considalfaerando que o zero do controlador (alfa) cancela um polo da planta
polos=pole(Gz);
alfa = -polos(2)

G2z=minreal(tf([1 alfa],1,T)*Gz);
[n2,d2]=tfdata(G2z,'v');

% fi2 � o �ngulo de G2z quando z=z1
fi2=angle(polyval(n2,z1)/polyval(d2,z1));
% fi1 � o �ngulo de G1z quando z=z1
fi1=-pi-fi2;
beta=(imag(z1)-real(z1)*tan(-fi1))/tan(-fi1)

num2=[1 real(alfa)];
den2=[1 beta];
Cz=tf(num2,den2,T)

FTMA=minreal(Cz*Gz)
disp('FTMA sem K')
zpk(FTMA)

[NFTMA,DFTMA]=tfdata(FTMA,'v');
K=abs((polyval(NFTMA,z1)/polyval(DFTMA,z1)));
K=1/real(K)

FTMA=minreal(Cz*Gz*K)
disp('FTMA sem K')
zpk(FTMA)

FTMF=feedback(FTMA,1)

figure()
step(FTMF)

figure()
rlocus(Gz)