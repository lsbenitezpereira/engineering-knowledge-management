%Espa�o de estados tarefa 10
clc;clear;close all;

%a)slide 3 EE06

A1=[-5 1 0; 0 -2 1; 20 -10 1]
B1=[0; 0; 1]
C1=[-1 1 0]
D1=0

syms s
% entrada do tipo degrau
R1=1/s;
R2=1/s^2;
disp('Erro entrada ao Degrau')
ess1=limit(s*R1*(1-C1*inv(s*eye(3)-A1)*B1),s,0)
%erro entrada degrau unit�rio=4/5
disp('Erro entrada a Rampa')
ess2=limit(s*R2*(1-C1*inv(s*eye(3)-A1)*B1),s,0)
%erro entrdada a rampa oo (infinito)

%b)slide 4 EE06
%erro ao Degrau
disp('Erro entrada ao Degrau')
ess3=1+(C1*inv(A1)*B1)    %4/5=0.8
%erro a rampa
ess4=limit((1+C1*inv(A1)*B1)*s,s,inf)+(C1*(inv(A1)^2)*B1)  %oo


%c) Slide 11 EE07

C1=100e-9;
C2=680e-9;
R1=60e3;
R2=18e3;


%2) Matriz
A=[0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R1*R2*C2)]
B=[0; 1/(R1*C2)]
C=[1 0]
D=0

[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d)

step(G2)


% Matriz de Controlabilidade
Mc=[B A*B]
disp('Determinante Mc')
det(Mc)  %se det=0->matriz singular  
% ou
ctrb(A,B)
% Posto da Matriz de Controlabilidade
rank(Mc)

% % Matriz de Controlabilidade da Sa�da
Mco=[C*B C*A*B]
rank(Mco)


disp('Matriz de Observabilidade')
% Matriz de Observabilidade
Mo=[C' A'*C']
% ou
obsv(A,C)
% Posto da Matriz de Observabilidade
rank(Mo)

hold on
sistema=ss(A,B,C,D)
step(sistema,'*');

x0=[0;0]  %sistema 2 ordem
t=0:0.001:0.10;
u=ones(1,length(t));
y2=lsim(sistema,u,t,x0);
plot(t,y2,'o');

