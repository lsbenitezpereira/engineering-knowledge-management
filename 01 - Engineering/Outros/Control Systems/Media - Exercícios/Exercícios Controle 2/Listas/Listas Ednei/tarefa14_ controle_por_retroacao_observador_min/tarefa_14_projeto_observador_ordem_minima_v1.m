% Ednei Freiberger v1.0
% tarefa 14 EE07 do slide EE11
clc; clear ;close all;
format long;

NT=15   %letras nome + sobrenome
C1=100e-9;
C2=680e-9;
R1=4e3*NT;
R2=18e3;


% Matriz
A=[0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R1*R2*C2)];
B=[0; 1/(R1*C2)];
C=[1 0];
D=0;
sistema=ss(A,B,C,D)

%****************************************
%    An�lise do Sistema Original 
%****************************************
% resposta ao degrau
[Yo,t,Xo] = step(sistema);
% separando as sa�das
figure('Name','Resposta Degrau Sistema Original');
grid();
subplot(3,1,1)
plot(t,Xo(:,1),'linewidth',2)
title('Estado x1')
xlabel('t'); ylabel('x1');
hold on
subplot(3,1,2)
plot(t,Xo(:,2),'linewidth',2)
title('Estado x2')
xlabel('t'); ylabel('x2');
hold on
subplot(3,1,3)
plot(t,Yo,'linewidth',2)
title('Sa�da Y')
hold on
xlabel('t'); ylabel('Y');

%Verificando polos e zeros sistema original
[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d);
zpk(G2)


%verificando Polos Sistema Original
syms s;
disp('Equa��o caracteristica Sistema Original')
eq_carac_ori=det(s*eye(2)-A)
pretty(solve(eq_carac_ori))


% teste de observabilidade
disp('Matriz observabilidade, Rank')
Mo=[conj(C') conj(A')*conj(C')]
rank_Mo = rank(Mo)
if (rank_Mo >= size(Mo,1) &(det(Mo)~=0))   %para ser control�vel, rank=numero de linhas
   disp('Sistema Control�vel')             %determinante Mc#0
else
   disp('Sistema n�o Control�vel')
end


%********************************************
% Projeto de controle Planta                % 
%********************************************
% autovalores desejados do sistema controlado
s1 = -2.112676056338028e+002 +2.882485254595439e+002i;
s2 = -2.112676056338028e+002 -2.882485254595439e+002i;
s3 = real(s1)*10;                  % polo n�o dominante;


A_chapeu = [A zeros(2,1);-C 0];
B_chapeu = [B;0];

% Matriz de ganhos do controlador K_chapeu = [k1 k2 -ki]
K_chapeu=acker(A_chapeu,B_chapeu,[s1 s2 s3])


% Verificando Autovalores da Matriz
disp('Raizes do Sistema Controlado:')
eig(A_chapeu-B_chapeu*K_chapeu)
eq_car=poly(A_chapeu-B_chapeu*K_chapeu)
roots(eq_car)    



K = [K_chapeu(1) K_chapeu(2)];
Ki = -K_chapeu(3);
AA = [A-B*K B*Ki;-C 0];
BB = [0;0;1];
CC = [1 0 0];
DD = 0;

sistema_comp=ss(AA,BB,CC,DD)

%compra��o entre sistema Original e Compensado
figure(2);
plot(t,Yo,'b');
hold on;
step(sistema_comp,t,'r');
legend('Sistema Original','Sistema Compensado')

tfinal=0.04;
T = 0.001; k = tfinal/T;
t=0:T:tfinal;

[Y,t2,X] = step(sistema_comp,tfinal);
% t2 novo para melhorar gr�fico
% sistema compensado
figure('Name','Estados Sistema Compensado');
subplot(2,2,1)
plot(t2,X(:,1),'linewidth',2)
title('Estado x1')
xlabel('t'); ylabel('x1');
subplot(2,2,2)
plot(t2,X(:,2),'linewidth',2)
title('Estado x2')
xlabel('t'); ylabel('x2');
subplot(2,2,3)
plot(t2,X(:,3),'linewidth',2)
title('Estado x3')
xlabel('t'); ylabel('x3');
subplot(2,2,4)
plot(t2,Y,'linewidth',2)
title('y')
hold on
xlabel('t'); ylabel('Y');


%*****************************************
% Equa��es Recursivas Sistema Compensado  
%***************************************
%   Microcontrolador
T = 0.0001; k = 0:0.04/T;
r = ones(1,length(k));

E = zeros(1,length(k));
u = zeros(1,length(k));
x1 = zeros(1,length(k));
x2 = zeros(1,length(k));
y = zeros(1,length(k));
E_ponto = zeros(1,length(k));
x1_ponto = zeros(1,length(k));
x2_ponto = zeros(1,length(k));

e_ponto(1) = r(1)-y(1);


for j=2:length(k)
   % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    

    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=CC(1)*x1(j)+CC(2)*x2(j)+DD*r(j);  
   
    % erro
    E_ponto(j)=r(j)-y(j);
    
    E(j)=T*E_ponto(j-1)+E(j-1);
    
    u(j)=Ki*E(j)-(K(1)*x1(j)+K(2)*x2(j));
    
    % Equa��o diferencial de estados:  Xponto=(Ax+Bu)
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+B(2)*u(j); 
      
end

figure(3)
subplot(2,2,1);hold on;
plot(k*T,x1,'o');grid;
legend('Step','Eqs. Recursivas')
subplot(2,2,2);hold on;
plot(k*T,x2,'o');grid;
legend('Step','Eqs. Recursivas')
subplot(2,2,3);hold on;
plot(k*T,E,'o');grid;
legend('Step','  Eqs. Recursivas')
subplot(2,2,4);hold on;
plot(k*T,y,'o');grid;
legend('Step','Eqs. Recursivas')


figure()
plot(k*T,E_ponto,'o');grid;hold on;
plot(k*T,u,'x');grid;
title('An�lise Sistema')
xlabel('t'); ylabel('Y');
legend('Erro','A��o de Controle')



%********************************************
% Projeto observador                       %
%********************************************

% Auto valores Observador
s4 = -200;
s5 = -200;


% observador de ordem m�nima
Abb=[A(2,2)];
Aab=[A(1,2)];
L=[s4];
Ke=acker(Abb',Aab',L)'


Aaa=0;
Aba=[A(2,1)];
Ba=0;
Bb=[B(2)];

Achapeu=Abb-Ke*Aab;
Bchapeu=Achapeu*Ke+Aba-Ke*Aaa;
Fchapeu=Bb-Ke*Ba;

Cchapeu=[0 0;1 0;0 1];
Dchapeu=[1;Ke];

% simula��o com equa��es recursivas do observador

eta1(1)=0;
eta1_ponto(1)=Fchapeu(1)*u(1);


for j=2:length(k)

% observador de ordem m�nima
    % Equa��es dos integradores do observador
    eta1(j)=T*eta1_ponto(j-1)+eta1(j-1);
 
    % Equa��o diferencial de estados do observador
    eta1_ponto(j)=Achapeu(1,1)*eta1(j)+Bchapeu(1)*y(j)+Fchapeu(1)*u(j);
    
    % equa��o de sa�da do observador de ordem m�nima
    xtil1(j)=Cchapeu(1,1)*eta1(j)+Dchapeu(1)*y(j);
    xtil2(j)=Cchapeu(2,1)*eta1(j)+Dchapeu(2)*y(j);
   
end
% plotando as vari�veis originais
figure()
subplot(3,1,1)
plot(k*T,x1,'*')
subplot(3,1,2)
plot(k*T,x2,'*')
subplot(3,1,3)
plot(k*T,y,'*')


% plotando as vari�veis do observador
subplot(3,1,1);hold on
plot(k*T,xtil1,'o');
legend('X1','X1_till');
subplot(3,1,2);hold on;
plot(k*T,xtil2,'o');
legend('X2,X2_till')



%%************************************************
% Conectando observador a planta
%*************************************************


E = zeros(1,length(k));
u = zeros(1,length(k));
x1 = zeros(1,length(k));
x2 = zeros(1,length(k));
y = zeros(1,length(k));
E_ponto = zeros(1,length(k));
x1_ponto = zeros(1,length(k));
x2_ponto = zeros(1,length(k));

e_ponto(1) = r(1)-y(1);
eta1(1)=0;
eta1_ponto(1)=Fchapeu(1)*u(1);



for j=2:length(k)
   % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);  
    eta1(j)=T*eta1_ponto(j-1)+eta1(j-1);

    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=CC(1)*x1(j)+CC(2)*x2(j)+DD*r(j);  
   
    % erro
    E_ponto(j)=r(j)-y(j);
    
    E(j)=T*E_ponto(j-1)+E(j-1);
   
    % equa��o de sa�da do observador de ordem m�nima
    xtil1(j)=Cchapeu(1,1)*eta1(j)+Dchapeu(1)*y(j);
    xtil2(j)=Cchapeu(2,1)*eta1(j)+Dchapeu(2)*y(j);
   
    
    u(j)=Ki*E(j)-(K(1)*xtil1(j)+K(2)*xtil2(j));
    
    % Equa��o diferencial de estados:  Xponto=(Ax+Bu)
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+B(2)*u(j); 
    
    
    % Equa��o diferencial de estados do observador
    eta1_ponto(j)=Achapeu(1,1)*eta1(j)+Bchapeu(1)*y(j)+Fchapeu(1)*u(j);
      
      
end


% plotando as vari�veis originais
figure(6)
subplot(3,1,1)
plot(k*T,x1,'*')
title('Estado Sistema X1')
xlabel('t'); ylabel('X2');

subplot(3,1,2)
plot(k*T,x2,'*')
title('Estado Sistema X2')
xlabel('t'); ylabel('X2');

subplot(3,1,3)
plot(k*T,y,'*')
title('Sa�da Sistema')
xlabel('t'); ylabel('Y');



% plotando as vari�veis do observador
subplot(3,1,1);hold on
plot(k*T,xtil1,'o');
legend('X1','X1_till');
subplot(3,1,2);hold on;
plot(k*T,xtil2,'o');
legend('X2,X2_till')




