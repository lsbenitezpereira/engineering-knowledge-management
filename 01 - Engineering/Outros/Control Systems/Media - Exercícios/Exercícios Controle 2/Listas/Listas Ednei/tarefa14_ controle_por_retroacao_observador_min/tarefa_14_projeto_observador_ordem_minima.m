% Ednei Freiberger
% tarefa 14 EE07 do slide EE11
clc; clear ;close all;
format long;

NT=15   %letras nome + sobrenome
C1=100e-9;
C2=680e-9;
R1=4e3*NT;
R2=18e3;


% Matriz
A=[0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R1*R2*C2)];
B=[0; 1/(R1*C2)];
C=[1 0];
D=0;
sistema=ss(A,B,C,D)

% resposta ao degrau
[Y,t,X] = step(sistema);
% separando as sa�das
figure('Name','Resposta Degrau Sistema Original');
grid();
subplot(3,1,1)
plot(t,X(:,1))
title('x1')
xlabel('t'); ylabel('Y');
hold on
subplot(3,1,2)
plot(t,X(:,2))
title('x2')
xlabel('t'); ylabel('Y');
hold on
subplot(3,1,3)
plot(t,Y)
title('y')
hold on
xlabel('t'); ylabel('Y');

%Verificando polos e zeros sistema original
[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d);
zpk(G2)


%verificando Polos Sistema Original
syms s;
disp('Equa��o caracteristica Sistema Original')
eq_carac_ori=det(s*eye(2)-A)
pretty(solve(eq_carac_ori))


% teste de observabilidade
disp('Matriz observabilidade, Rank')
Mo=[conj(C') conj(A')*conj(C')]
rank_Mo = rank(Mo)
if (rank_Mo >= size(Mo,1) &(det(Mo)~=0))   %para ser control�vel, rank=numero de linhas
   disp('Sistema Control�vel')             %determinante Mc#0
else
   disp('Sistema n�o Control�vel')
end

% autovalores desejados do sistema controlado
s1 = -2.112676056338028e+002 +2.882485254595439e+002i;
s2 = -2.112676056338028e+002 -2.882485254595439e+002i;
s3 = real(s1)*10;                  % polo n�o dominante;

J=[s1 s2];
K=acker(A,B,J);

% Auto valores Observador

s4 = -200;
s5 = -200;


% observador de ordem m�nima
Abb=[A(2,2)];
Aab=[A(1,2)];
L=[s4];
Ke=acker(Abb',Aab',L)'




Aaa=0;
Aba=[A(2,1)];
Ba=0;
Bb=[B(2)];

Achapeu=Abb-Ke*Aab;
Bchapeu=Achapeu*Ke+Aba-Ke*Aaa;
Fchapeu=Bb-Ke*Ba;

Cchapeu=[0 0;1 0;0 1];
Dchapeu=[1;Ke];

% simula��o com equa��es recursivas

T=0.001;
k=0:round(0.1/T);
u=ones(1,length(k));
% condi��es iniciais
x1(1)=0;
x2(1)=0;
x3(1)=0;
x1_ponto(1)=0;
x2_ponto(1)=0;
x3_ponto(1)=0;
y(1)=C(1)*x1(1)+C(2)*x2(1);

eta1(1)=0;

eta1_ponto(1)=Fchapeu(1)*u(1);


for j=2:length(k)
% sistema original
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    

    % Equa��o diferencial de estados:  Xponto=A*X+B*U
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+B(2)*u(j); 
   
    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=C(1)*x1(j)+C(2)*x2(j)+D*u(j);
% observador de ordem m�nima
    % Equa��es dos integradores do observador
    eta1(j)=T*eta1_ponto(j-1)+eta1(j-1);
 
    % Equa��o diferencial de estados do observador
    eta1_ponto(j)=Achapeu(1,1)*eta1(j)+Bchapeu(1)*y(j)+Fchapeu(1)*u(j);
    
    % equa��o de sa�da do observador de ordem m�nima
    xtil1(j)=Cchapeu(1,1)*eta1(j)+Dchapeu(1)*y(j);
    xtil2(j)=Cchapeu(2,1)*eta1(j)+Dchapeu(2)*y(j);
   
end
% plotando as vari�veis originais
subplot(3,1,1)
plot(k*T,x1,'*')
subplot(3,1,2)
plot(k*T,x2,'*')
subplot(3,1,3)
plot(k*T,y,'*')

% plotando as vari�veis do observador
subplot(3,1,1)
plot(k*T,xtil1,'o')
subplot(3,1,2)
plot(k*T,xtil2,'o')


hold off

