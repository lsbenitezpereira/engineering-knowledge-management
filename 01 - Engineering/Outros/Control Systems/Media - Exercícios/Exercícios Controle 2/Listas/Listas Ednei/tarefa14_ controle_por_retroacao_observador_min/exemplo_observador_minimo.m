% Exemplo de Projeto de um observador de estados de ordem m�nima

clc
clear
clf

A=[0 1 0;0 0 1;-6 -11 -6]
B=[0;0;1];
C=[1 0 0];
D=[0];
sistema=ss(A,B,C,D);

% resposta ao degrau
[Y,t,X] = step(sistema);
% separando as sa�das
subplot(2,2,1)
plot(t,X(:,1))
title('x1')
hold on
subplot(2,2,2)
plot(t,X(:,2))
title('x2')
hold on
subplot(2,2,3)
plot(t,X(:,3))
title('x3')
hold on
subplot(2,2,4)
plot(t,Y)
title('y')
hold on


[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d);
zpk(G2)



% observador de ordem m�nima
Abb=[0 1;-11 -6];
Aab=[1 0];
L=[-10 -10];
Ke=acker(Abb',Aab',L)'


eee
Aaa=0;
Aba=[0;-6];
Ba=0;
Bb=[0;1];

Achapeu=Abb-Ke*Aab;
Bchapeu=Achapeu*Ke+Aba-Ke*Aaa;
Fchapeu=Bb-Ke*Ba;

Cchapeu=[0 0;1 0;0 1];
Dchapeu=[1;Ke];

% simula��o com equa��es recursivas

T=0.02;
k=0:round(7/T);
u=ones(1,length(k));
% condi��es iniciais
x1(1)=0;
x2(1)=0;
x3(1)=0;
x1_ponto(1)=0;
x2_ponto(1)=0;
x3_ponto(1)=0;
y(1)=C(1)*x1(1)+C(2)*x2(1)+C(3)*x3(1);

eta1(1)=0;
eta2(1)=0;
eta1_ponto(1)=Fchapeu(1)*u(1);
eta2_ponto(1)=Fchapeu(2)*u(1);
    



for j=2:length(k)
% sistema original
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    % Equa��o diferencial de estados:  Xponto=A*X+B*U
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+A(1,3)*x3(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+A(2,3)*x3(j)+B(2)*u(j); 
    x3_ponto(j)=A(3,1)*x1(j)+A(3,2)*x2(j)+A(3,3)*x3(j)+B(3)*u(j);  
    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=C(1)*x1(j)+C(2)*x2(j)+C(3)*x3(j)+D*u(j);
% observador de ordem m�nima
    % Equa��es dos integradores do observador
    eta1(j)=T*eta1_ponto(j-1)+eta1(j-1);
    eta2(j)=T*eta2_ponto(j-1)+eta2(j-1);
    % Equa��o diferencial de estados do observador
    eta1_ponto(j)=Achapeu(1,1)*eta1(j)+Achapeu(1,2)*eta2(j)+Bchapeu(1)*y(j)+Fchapeu(1)*u(j);
    eta2_ponto(j)=Achapeu(2,1)*eta1(j)+Achapeu(2,2)*eta2(j)+Bchapeu(2)*y(j)+Fchapeu(2)*u(j);
    % equa��o de sa�da do observador de ordem m�nima
    xtil1(j)=Cchapeu(1,1)*eta1(j)+Cchapeu(1,2)*eta2(j)+Dchapeu(1)*y(j);
    xtil2(j)=Cchapeu(2,1)*eta1(j)+Cchapeu(2,2)*eta2(j)+Dchapeu(2)*y(j);
    xtil3(j)=Cchapeu(3,1)*eta1(j)+Cchapeu(3,2)*eta2(j)+Dchapeu(3)*y(j);
end

% plotando as vari�veis originais
subplot(2,2,1)
plot(k*T,x1,'*')
subplot(2,2,2)
plot(k*T,x2,'*')
subplot(2,2,3)
plot(k*T,x3,'*')
subplot(2,2,4)
plot(k*T,y,'*')

% plotando as vari�veis do observador
subplot(2,2,1)
plot(k*T,xtil1,'o')
subplot(2,2,2)
plot(k*T,xtil2,'o')
subplot(2,2,3)
plot(k*T,xtil3,'o')



hold off

