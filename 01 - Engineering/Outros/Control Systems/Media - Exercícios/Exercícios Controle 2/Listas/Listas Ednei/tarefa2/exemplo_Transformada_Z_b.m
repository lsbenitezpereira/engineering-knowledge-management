% Exemplo Transformada Z

% f(k) = a^k
% F(z) = z/(z-a)

% Rela��o entre os polos de F(z) e a evolu��o temporal de f(k)
clc;clear;
a = -0.3;
k = 0:10;
fk = a.^k;

syms n
ztrans(-0.3^n)

figure(1)
subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolu��o temporal de f(k)')

subplot(1,2,2)
zplane([],a)
title('polos de F(z)')