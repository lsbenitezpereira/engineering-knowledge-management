% Exemplo Transformada Z

% f(k) = sen(aWoK)
% F(z) = z.sen(aWo)/(z^2-2zcos(aWo)+1)

% Rela��o entre os polos de F(z) e a evolu��o temporal de f(k)
clc;clear;
a = 0.3;
k = 0:21;
fk = sin(a.*k);

syms n Wo
ztrans(sin(0.3*Wo*n))

figure(1)
subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolu��o temporal de f(k)')

subplot(1,2,2)
zplane([],roots([1 -2*cos(0.3) 1]))
title('polos de F(z)')