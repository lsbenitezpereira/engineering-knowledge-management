% Exemplo Transformada Z

% f(k) = impluso(k-2)
% F(z) = 1/z^2

% Rela��o entre os polos de F(z) e a evolu��o temporal de f(k)
clc;clear;
a = 1;
k = 0:10;
fk = zeros(1,11);
fk(3) = 1;

%syms n Wo
%ztrans(sin(0.3*Wo*n))

figure(1)
subplot(1,2,1)
plot(k,fk,'*')
xlabel('k')
ylabel('f(k)')
title('evolu��o temporal de f(k)')

subplot(1,2,2)
zplane([],roots([1 0 0]))
title('polos de F(z)')