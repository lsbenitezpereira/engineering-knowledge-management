% EE03 - Solu��o da equa��o diferencial de estados

% Exemplo 1 slide 5
clear
format long


A=[0 1 0;0 0 1;-24 -26 -9];
B=[0; 0; 1];
C=[1 1 0];
D=0;
X0=[1;0;2];

% Solu��o com a transformada de laplace
syms s
U=1/(s+1);
X=inv(s*eye(3)-A)*X0+inv(s*eye(3)-A)*B*U
Y=C*X
y=ilaplace(Y)

% comparando os resultados
% resposta da sulu��o encontrada
t=0:.01:10;
y1=-6.5*exp(-2*t)+19*exp(-3*t)-11.5*exp(-4*t);
subplot(2,1,1)
plot(t,y1)

% resposta com o comando lsim do Matlab
u=exp(-t);
sistema = ss(A,B,C,D); 
y2=lsim(sistema,u,t,X0);

subplot(2,1,2)
plot(t,y2)

