%Espa�o de estados tarefa 8
clc;clear;close all;

L1=0.001;
C1=0.0001;
R1= 10 ;


%2) Matriz
A=[-1/(C1*R1) 1/C1; -1/L1 0]
B=[0;1/L1]
C=[1 0]
D=0

%4)
% convertendo para fun��o de tranfer�ncia com a utiliza��o da Equa��o
s=tf([1 0],1);
G1=C*inv(s*eye(2)-A)*B+D


% convertendo para fun��o de tranfer�ncia com a utiliza��o da fun��o do
% Matlab
[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d)


%Fun��o de Transfer�ncia
num2=[1/(L1*C1)];
den2=[1 1/(R1*C1) 1/(L1*C1)];
G3=tf(num2,den2)



%5)
[A2,B2,C2,D2] = tf2ss(n,d)


%6)
figure();
hold on;
step(G1,'*');
step(G2,'o');
step(G3);
legend('da Equa��o','da ss2tf','da Fun��o Transfer�ncia');





