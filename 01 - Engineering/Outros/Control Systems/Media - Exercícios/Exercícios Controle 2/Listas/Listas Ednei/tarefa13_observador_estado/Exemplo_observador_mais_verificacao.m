% Exemplo de Projeto de Observador de Ordem Plena
% Verifica��o dos estados observados atrav�s do sistema expandido
clc; clear ;close all
format long

% Matrizes do Sistema
A = [0 500;-25 -100];
B = [0;25];
C = [1 0];
D = 0;

% projeto do observador de estados
% autovalores desejados
u1=-1000;
u2=-1000;

% Matriz de observabilidade
N=[conj(C') conj(A')*conj(C')];
rank(N)

% Matriz de Ganhos do Observador
Ke=acker(A',C',[u1 u2])'


% Verifica��o atrav�s do sistema expandido
AA=[A zeros(length(A));Ke*C A-Ke*C];
BB=[B;B];
CC=eye(2*length(A));
DD=zeros(2*length(A),1);
sys=ss(AA,BB,CC,DD);
step(sys)








