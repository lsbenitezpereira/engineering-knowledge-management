% Exemplo de servosistemas para planta do tipo zero
% Ednei Freiberger  15  tarefa12(5)
clear; clc; close all;
format long

NT=15   %letras nome + sobrenome
C1=100e-9;
C2=680e-9;
R1=4e3*NT;
R2=18e3;


% Matriz
A=[0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R1*R2*C2)];
B=[0; 1/(R1*C2)];
C=[1 0];
D=0;

[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d);
zpk(G2)

figure(1)
step(G2)    %ta do sistema 0,1s
title('Resposta ao o Degrau Sistema Original')
xlabel('t'); ylabel('Y');
grid();

%verificando Polos Sistema Original
syms s;
disp('Equa��o caracteristica Sistema Original')
eq_carac_ori=det(s*eye(2)-A)
raizes_orig=solve(eq_carac_ori)



% autovalores desejados
s1 = 1;
s2 = 1;


% teste de observabilidade
disp('Matriz observabilidade, Rank')
Mo=[conj(C') conj(A')*conj(C')]
rank_Mo = rank(Mo)
if (rank_Mo >= size(Mo,1) &(det(Mo)~=0))   %para ser control�vel, rank=numero de linhas
   disp('Sistema Control�vel')             %determinante Mc#0
else
   disp('Sistema n�o Control�vel')
end   



% Matriz de Ganhos do Observador
Ke=acker(A',C',[s1 s2])'



% Per�odo de Amostragem 
T = 0.001;
t=0:T:0.1; k = 0:0.1/T;

% Verifica��o atrav�s do sistema expandido
AA=[A zeros(length(A));Ke*C A-Ke*C];
BB=[B;B];
CC=eye(2*length(A));
DD=zeros(2*length(A),1);
sys=ss(AA,BB,CC,DD);
figure()
step(sys,t)

%verificando os Polos do observador
disp('Polos do Observador')
eq_carac_observador=det(s*eye(2)-(A-Ke*C))
pretty(solve(eq_carac_observador))


%*****************************%
%   Equa��es diferenciais     %
%*****************************%
%   Matlab 
u = ones(1,length(k));
x = zeros(2,length(k));
y = zeros(1,length(k));
x_ponto = zeros(2,length(k));
x_tio=zeros(2,length(k));   %x~
x_tio_ponto=zeros(2,length(k));

for i=2:length(k)
   x(:,i) = T*x_ponto(:,i-1) + x(:,i-1);
   x_tio(:,i) = T*x_tio_ponto(:,i-1) + x_tio(:,i-1);
   y(i) = C*x(:,i);
   x_ponto(:,i) = A*x(:,i)+B*u(i);
   x_tio_ponto(:,i) = A*x_tio(:,i)+B*u(i) + Ke*(y(i)-C*x_tio(:,i));
end
figure()
plot(k*T,y,'*');grid;hold on;
step(G2);



% separando os elemntos do vetor x com x_tio
x1a=x(1,:);
x1b=x(2,:);
x1a_tio=x_tio(1,:);
x1b_tio=x_tio(2,:);


figure()
subplot(2,1,1)
plot(t,x1a,'*')
title('x1a, x1a_tio')
hold on
plot(t,x1a_tio,'o')
legend('x1a','x1a_tio')
subplot(2,1,2)
plot(t,x1b,'*')
title('x1b, x1b_tio')
hold on
plot(t,x1b_tio,'o')
legend('x1b','x1b_tio')




%************************************
%      Microcontrolador             %
%************************************
u = ones(1,length(k));
x1 = zeros(1,length(k));
x2 = zeros(1,length(k));
y = zeros(1,length(k));
x1_ponto = zeros(1,length(k));
x2_ponto = zeros(1,length(k));
x1_tio=zeros(1,length(k));   %x~
x2_tio=zeros(1,length(k));   %x~
x1_tio_ponto=zeros(1,length(k));
x2_tio_ponto=zeros(1,length(k));


for j=2:length(k)
   % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);
    x1_tio(j) = T*x1_tio_ponto(j-1) + x1_tio(j-1);
    x2_tio(j) = T*x2_tio_ponto(j-1) + x2_tio(j-1);

    % Equa��o de Sa�da: Y=C*X+D*U
    y1(j)=CC(1)*x1(j)+CC(2)*x2(j)+D*u(j);  
   
  
    % Equa��o diferencial de estados: A*x+B*u
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+B(2)*u(j); 
    % Equa��o diferencial de estados:  A*x_tio+B*u(i) + Ke*(y(i)-C*x_tio(:,i))
    x1_tio_ponto(j)=A(1,1)*x1_tio(j)+A(1,2)*x2_tio(j)+B(1)*u(j)+Ke(1)*(y1(j)-(C(1)*x1_tio(j)+C(2)*x2_tio(j)));
    x2_tio_ponto(j)=A(2,1)*x1_tio(j)+A(2,2)*x2_tio(j)+B(2)*u(j)+Ke(2)*(y1(j)-(C(1)*x1_tio(j)+C(2)*x2_tio(j)));
    
   
end
figure()
plot(t,y1,'*');grid;hold on;
step(G2);title('Resposta ao Degrau Eq. Recur. MicroC.')

figure()
title('Resposta ao Degrau Eq. Recur. MicroC.')
subplot(2,1,1)
plot(t,x1,'*')
title('x1, x1_tio')
hold on
plot(t,x1_tio,'o')

legend('x1','x1_tio')
subplot(2,1,2)
plot(t,x2,'*')
title('x2, x2_tio')
hold on
plot(t,x2_tio,'o')
legend('x','x2_tio')



