% Exemplo de determinação da fórmula de Ackreman

% Matrizes do Sistema
A=[0 1 0;0 0 1;-1 -5 -6];
B=[0;0;1];

%Autovalores Desejados
u1=-2+4i;
u2=-2-4i;
u3=-10;

% Matriz de Controlabilidade
M=[B A*B A^2*B]    % ou
M=ctrb(A,B)
rank(M)            % teste de controlabilidade

% Equação característica desejada
eq_des=conv([1 -u1],conv([1 -u2],[1 -u3]))    % ou
eq_des=poly([u1 u2 u3])

% Substituição da matriz A no polinômio desejado phi(A)
phi_A=A^3+14*A^2+60*A+200*eye(3)      % ou 
phi_A=polyvalm(eq_des,A)

% Fórmula de Ackreman
K=[0 0 1]*inv(M)*phi_A     % ou
K=acker(A,B,[u1 u2 u3])

% Verificação dos Autovalores
eq_car=poly(A-B*K)
roots(eq_car)     % ou
eig(A-B*K)