% An�lise de um Sistema representado no Espa�ode Estado com equa��es recursivas na forma expandida
clf

% Matrizes do Sistema
A=[0 1 0;0 0 1;-1 -5 -6];
B=[0;0;1];
C=[1 1 1];
D=0;
sistema=ss(A,B,C,D);
% Condi��es Iniciais
x0=[1; 1; 1];

% entrada nula
t=0:30/999:30;
u=zeros(1,length(t));

% resposta para entrada nula
figure(1)
[Y,t,X]=lsim(sistema,u,t,x0);

% separando os elemntos do vetor X
x1a=X(:,1);
x1b=X(:,2);
x1c=X(:,3);



subplot(2,2,1)
plot(t,x1a)
hold on
subplot(2,2,2)
plot(t,x1b)
hold on
subplot(2,2,3)
plot(t,x1c)
hold on
subplot(2,2,4)
plot(t,Y)
hold on




% simula��o com discretiza��o dos integradores
% I(z)=T/(z-1)=Xi(z)/Xi_ponto(z)
% xi(k)=T*xi_ponto(k-1)+xi(k-1)

clear u
T=0.1;
k=0:30/T;
u=zeros(1,length(k));

% condi��es iniciais
x1(1)=x0(1);  %  para k = 0
x2(1)=x0(2);  %  para k = 0
x3(1)=x0(3);  %  para k = 0
x1_ponto(1)=0;  %  para k = 0
x2_ponto(1)=0;  %  para k = 0
x3_ponto(1)=0;  %  para k = 0
y(1)=C(1)*x1(1)+C(2)*x2(1)+C(3)*x3(1)+D*u(1);  %  para k = 0

for j=2:length(k)
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    % Equa��o diferencial de estados:  Xponto=A*X+B*U
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+A(1,3)*x3(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+A(2,3)*x3(j)+B(2)*u(j); 
    x3_ponto(j)=A(3,1)*x1(j)+A(3,2)*x2(j)+A(3,3)*x3(j)+B(3)*u(j);  
    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=C(1)*x1(j)+C(2)*x2(j)+C(3)*x3(j)+D*u(j);
end

subplot(2,2,1)
plot(k*T,x1,'*')
ylabel('x1')
subplot(2,2,2)
plot(k*T,x2,'*')
ylabel('x2')
subplot(2,2,3)
plot(k*T,x3,'*')
ylabel('x3')
subplot(2,2,4)
plot(k*T,y,'*')
ylabel('y')

hold off

