% Exemplo de determina��o da f�rmula de Ackreman
% slide 7 EE08 
clc;clear;close all;

% Matrizes do Sistema
A=[0 1 0;0 0 1;-1 -5 -6];
B=[0;0;1];

disp('An�lise Sistema Original')
C=[1 1 1];
D=0;
sistema_orig=ss(A,B,C,D);
% Condi��es Iniciais
x0=[1; 1; 1];

% entrada nula
t=0:30/999:30;
u=zeros(1,length(t));

% resposta para entrada nula
figure(1)
[Y,t,X]=lsim(sistema_orig,u,t,x0);

% separando os elemntos do vetor X
x1a=X(:,1);
x1b=X(:,2);
x1c=X(:,3);

figure(1)
suptitle('Desempenho do Sistema Original')
subplot(2,2,1)
plot(t,x1a)
title('Vari�vel de estado x1')
hold on
subplot(2,2,2)
plot(t,x1b)
title('Vari�vel de estado x2')
hold on
subplot(2,2,3)
plot(t,x1c)
title('Vari�vel de estado x3')
hold on
subplot(2,2,4)
plot(t,Y)
title('Resposta ao estado nulo (Y)')
hold off


%Fator Amortecimento Original Zeta
Zeta=0.7  
%Frequencia Natural original Wn
Wn=10
s1=-Zeta*Wn+j*Wn*sqrt(1-Zeta^2)


%Autovalores Desejados
disp('Autovalores Desejados')
u1=s1;      %raizes desejados
u2=s1';
u3=-abs(s1)*10 %polo n�o dominante


% Matriz de Controlabilidade
M=[B A*B A^2*B]    % ou
M=ctrb(A,B)
rank(M)            % teste de controlabilidade
                   % para ser control�vel rank(M)>=numero colunas de M
det(M)             % determinate de M#0
                   
                   
% Equa��o caracter�stica desejada
disp('Equa��o Caracter�stica Desejada')
eq_des=poly([u1 u2 u3])


% Substitui��o da matriz A no polin�mio desejado phi(A)
%phi_A=A^2+14*A+100*eye(3)      % ou 
phi_A=polyvalm(eq_des,A)


% F�rmula de Ackreman
K=[0 0 1]*inv(M)*phi_A     %pol ou
K=acker(A,B,[u1 u2 u3])
%projeto do controlador com a matriz de ganho k


% Verifica��o dos Autovalores
disp('Verifica��o dos Autovalores')
eq_car=poly(A-B*K)
disp('Verifica��o das Raizes')
roots(eq_car)     % ou
eig(A-B*K)



% An�lise do Sistem Compensado
sistema = ss(A-B*K, [0;0;0], eye(3), [0;0;0]);
% sistema=ss(A,B,C,D);
% Condi��es Iniciais
x0=[1; 1; 1];


% entrada nula
t=0:30/999:1;
u=zeros(1,length(t));

% resposta para entrada nula
figure(1)
[Y,t,X]=lsim(sistema,u,t,x0);

% separando os elemntos do vetor X
x1a=X(:,1);
x1b=X(:,2);
x1c=X(:,3);


figure(2)
subplot(2,2,1)
suptitle('Desempenho do Sistema Controlado')
plot(t,x1a)
hold on
subplot(2,2,2)
plot(t,x1b)
hold on
subplot(2,2,3)
plot(t,x1c)
hold on
subplot(2,2,4)
plot(t,Y)
hold on



%*************************
% Equa��es recursiva
%*************************

% simula��o com discretiza��o dos integradores
% I(z)=T/(z-1)=Xi(z)/Xi_ponto(z)
% xi(k)=T*xi_ponto(k-1)+xi(k-1)


T = 0.01; %tempo de acomoda��o 1s
Q = A-B*K;
n=0:1/T;  %tempo final 1s
u=zeros(1,length(n));
x1(1)=x0(1);  %  para k = 0
x2(1)=x0(2);  %  para k = 0
x3(1)=x0(3);  %  para k = 0
x1_ponto(1)=0;  %  para k = 0
x2_ponto(1)=0;  %  para k = 0
x3_ponto(1)=0;  %  para k = 0
y(1)=C(1)*x1(1)+C(2)*x2(1)+C(3)*x3(1)+D*u(1);  %  para k = 0

for j=2:length(n)
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    % Equa��o diferencial de estados:  Xponto=(A-BK)*X
    x1_ponto(j)=Q(1,1)*x1(j)+Q(1,2)*x2(j)+Q(1,3)*x3(j);  
    x2_ponto(j)=Q(2,1)*x1(j)+Q(2,2)*x2(j)+Q(2,3)*x3(j); 
    x3_ponto(j)=Q(3,1)*x1(j)+Q(3,2)*x2(j)+Q(3,3)*x3(j); 
    % Equa��o de Sa�da: Y=C*X+D*U
    y(j)=C(1)*x1(j)+C(2)*x2(j)+C(3)*x3(j)+D*u(j);
    
end

figure(3)
suptitle('Desempenho do Sistema Controlado')
subplot(2,2,1)
plot(n*T,x1,'*')
ylabel('x1')
subplot(2,2,2)
plot(n*T,x2,'*')
ylabel('x2')
subplot(2,2,3)
plot(n*T,x3,'*')
ylabel('x3')
subplot(2,2,4)
plot(n*T,y,'*')
ylabel('y')
%OU
hold on
plot(n*T,x1+x2+x3,'o')
ylabel('y')


hold off









