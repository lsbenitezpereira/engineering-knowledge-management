% an�lise do erro em regime permanante
% 
clear;clc;close all;
format long
num=1;
den=[1 1 0];
Gs=tf(num,den)
T=0.5;          %Per�odo Amostragem
Gz=c2d(Gs,T)    %G(z)+ZOH
FTMAz=Gz
zpk(FTMAz)


% erro pode ser verificado a partir da FTMF como sendo a diferen� entre a
% entrada e a sa�da
% por�m isto s� � poss�vel neste caso por que a realimenta��o � unit�ria
% coso n�o seja unit�ria, deve ser observado o sinal de sa�da do somador

hold on
FTMFz=feedback(Gz,1);
%step(FTMFz);

FTMFs=feedback(Gs,1);
%step(FTMFs);

tf=35;    %tempo final do gr'afico
%degrau
t=0:T:tf;
degrau=ones((tf/T)+1,1);
saida=lsim(FTMFz,degrau,t);
plot(t,saida,'r*');
plot(t,degrau,'bo');
erro_degrau=1-saida(end)
ylabel('Amplitude')
xlabel('t(s)')
title('Resposta ao Degrau')
legend('Resposta','Degrau');



figure();
hold on;
%rampa
rampa=t;
saida=lsim(FTMFz,rampa,t);
plot(t,saida,'bo');
plot(t,rampa,'r*');
erro_rampa=rampa(end)-saida(end)
ylabel('Amplitude')
xlabel('t(s)')
title('Resposta a Rampa')
legend('Resposta','Rampa');



figure();
hold on;
%parabola
parabola=t.^2;
saida=lsim(FTMFz,parabola,t);
plot(t,saida,'bo');
plot(t,parabola,'r*');
erro_parabola=parabola(end)-saida(end)
ylabel('Amplitude')
xlabel('t(s)')
title('Resposta a Par�bola')
legend('Resposta','Par�bola');



hold off
