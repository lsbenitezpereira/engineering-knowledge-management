%Lugar raizes Slide 10 Apresenta��o 8
clc;clear;close all;
format long

k=1;
T=0.1;           %Per�odo Amostragem (s)
%numd=[1 0];      %Gd(s)       
%dend=[1 -1];
Gds=tf(numd,dend)
Gdz=tf(numd,dend,T)   %Gdz * Amostrador

nump=[1];        %Gp(s)  
denp=[1 1];
Gps=tf(nump,denp)

%Gk(s) = 1-e^Ts/s  => ZOH Segurador de ordem zero
Gdkz=c2d(Gps,T)  % Gs*Gd   

FTMAz=k*Gdz*Gdkz

% importante, o lugar das ra�zes � tra�ado a partir 
% da fun��o de transfer�ncia de la�o aberto
figure(1);
rlocus(FTMAz)
% deslocando o mouse encima da linha verde do lugar das raize at� encontrar
% o ganho 1, verifica-se o valor dos polos de malha fechada para este ganho
% tamb�m � poss�vel ver os valores de fator de amortecimento e frequencia
% natural, que s�o os mesmos encontrado no exemplo anterior sobre
% transformada inversa com polos complexos

% os polos de malha fechada podem ser verificados a partir da FTMF
% por�m o lugar das ra�zes j� dava esta informa��o



figure(3);
hold on;
FTMFz=feedback(FTMAz,1)
disp('Polos FTMFz')
pole(FTMFz)
step(FTMFz);
tf=5;    %tempo final do gr'afico
%degrau
t=0:T:tf;
degrau=ones((tf/T)+1,1);
%saida=lsim(FTMFz,degrau,t);
%plot(t,saida,'r*');


legend('G(z) Step','G(z)','G(s)');


hold off


