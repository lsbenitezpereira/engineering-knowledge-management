% Exemplo de an�lise da resposta transit�ria
clear;clc;close all;
format long
T=2;
k=1;
num=1;den=[1 1 0];

Gs=tf(num,den)
Hs=tf(1,1)


Gz=c2d(k*Gs,T)        % Gs com ZOH
GHz=c2d(k*Gs*Hs,T)    % FTMA com ZOH

FTMFz=minreal(Gz/(1+GHz))  % n�o � possivel usar o comando feedback
step(FTMFz)


% clicando  com o bot�o direito do mouse na parte branca do gr�fico
% na op��o characteristics � possivel verificar os seguintes par�metros
% peak response - tempo de pico
% settling time - tempo de acomoda��o
% rise time - tempo de subida
% stead state - valor de regime permanente

% em properties/options � poss�vel escolher os parametros 
% de tempo de subida e tempo de acomoda��o

% armazenamento dos parametros considerando tempo de acomoda��o de 5%
 parametros = stepinfo(FTMFz,'SettlingTimeThreshold', 0.05)
 tempo_de_acomodacao=parametros.SettlingTime
 sobressinal=parametros.Overshoot

 disp('Polos FTMFz');
 pole(FTMFz)
 
 figure()
 rlocus(GHz)
 
figure()
FTMFs=feedback(k*Gs,1);
step(FTMFs)
disp('Polos FTMFs');
pole(FTMFs)

