% Exemplo de an�lise da resposta transit�ria ex2 slide 12 6 - Resposta
% transitoria
clear;clc;close all;
format long

%a) wn 0 a 30rad/s  zeta=0.7
%Tra�ar posi��o polos malha aberta
zeta=0.5;
T=2
k=1;

wn=30;

for wn=0:5:30
    disp(wn);
    
    num=wn^2;
    den=[1 2*zeta*wn wn^2];

    Gs=tf(num,den);
    Hs=tf(1,1);

    %considerando com amostrador ZOH
    Gz=c2d(k*Gs,T);        % Gs com ZOH
    GHz=c2d(k*Gs*Hs,T);    % FTMA com ZOH

    hold on
    % resposta ao degrau da FTMF
    
    FTMFz=minreal(Gz/(1+GHz))  % n�o � possivel usar o comando feedback
    
    figure(1);
    %subplot(2,2,1);
    hold on
    step(FTMFz)
    FTMFs=feedback(k*Gs,1);
    %axis([0,14,0,1]);
    
    figure(2);
    hold on
    %subplot(2,2,2);
    step(FTMFs);
    %axis([0,14,0,1]);

    figure(3)
    hold on
    %subplot(2,2,3);
    %lugar das Ra�zes
    rlocus(GHz)
    axis([-2,1,-2,2]);

    figure(4);
    hold on
    %subplot(2,2,4);
    rlocus(Gs)
    axis([-30,30,-100,100]);
    title(wn);

   % for i=1:0.1:100
   % end
end

hold off

