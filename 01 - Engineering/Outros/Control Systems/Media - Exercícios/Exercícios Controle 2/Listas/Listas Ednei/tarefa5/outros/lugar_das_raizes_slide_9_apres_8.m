%Lugar raizes Slide 9 Apresenta��o 8
clc;clear;close all;
format long


wn=10;          %Fequencia Natural Sistema
zeta=0.1;       %Fator Amortecimento
T=0.05;         %Per�odo Amostragem

num=wn^2;
den=[1 2*zeta*wn wn^2];
Gs=tf(num,den)
Gz=c2d(Gs,T);


FTMAz=Gz
% importante, o lugar das ra�zes � tra�ado a partir 
% da fun��o de transfer�ncia de la�o aberto
figure(1);
hold on;
rlocus(FTMAz)
% deslocando o mouse encima da linha verde do lugar das raize at� encontrar
% o ganho 1, verifica-se o valor dos polos de malha fechada para este ganho
% tamb�m � poss�vel ver os valores de fator de amortecimento e frequencia
% natural, que s�o os mesmos encontrado no exemplo anterior sobre
% transformada inversa com polos complexos

% os polos de malha fechada podem ser verificados a partir da FTMF
% por�m o lugar das ra�zes j� dava esta informa��o

figure(2);
rlocus(Gs);


figure(3);
hold on;
FTMFz=feedback(FTMAz,1);
pole(FTMFz)
%step(FTMFz);
tf=5;    %tempo final do gr'afico
%degrau
t=0:T:tf;
degrau=ones((tf/T)+1,1);
saida=lsim(FTMFz,degrau,t);
plot(t,saida,'r*');


%FTMF de G(s)
FTMFs=feedback(Gs,1)
step(FTMFs);
legend('G(z) Step','G(z)','G(s)');


hold off
