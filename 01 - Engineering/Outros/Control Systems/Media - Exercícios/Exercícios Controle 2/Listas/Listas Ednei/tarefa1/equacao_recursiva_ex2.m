% Exemplo de Algoritmo de Equa��es Recursivas
% Circuito RC
clc
clear
format long

NT=15;              % ednei freiberger
Vi=10;              % Tens�o Fonte
R = NT              % R = 15
C = 1e-3            % C = 1000uF
RC= R*C             % Const. Tempo
T = 5e-3;           % per�odo de amostragem
Tf = NT*10e-3;      % tempo final de simula��o
a = exp(-T/RC);     % condi��o inicial tens�o capacitor
b = (1- exp(-T/RC));

% Gr�fico "cont�nuo" com 1000 pontos
t = 0:Tf/999:Tf;
Vs=0;
Vs = Vs*exp(-t/RC) + Vi*(1-exp(-t/RC));
figure(1)
plot(t,Vs)
hold on             % lembrar de hold off no final

% Gr�fico discreto
k = 0:Tf/T;
Vsd = 0;
Vsd = Vsd*exp(-k*T/RC) + Vi*(1-exp(-k*T/RC));
plot(k*T,Vsd,'*r')

% Gr�fico discreto a partir da equa��o recursiva

% Condi��o Inicial
Vsr(1) = 0;     %
for j=1:length(k)-1
    Vsr(j+1)=a*Vsr(j) + b*Vi;
end
plot(k*T,Vsr,'ok')
title('f(t), f(kT) e f(kT)recursiva')
xlabel('t(ms)'); ylabel('Vs');
hold off
