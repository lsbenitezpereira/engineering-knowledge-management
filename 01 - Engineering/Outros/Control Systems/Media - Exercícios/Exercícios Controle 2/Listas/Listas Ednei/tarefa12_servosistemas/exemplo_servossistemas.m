% Exemplo de servosistemas para planta do tipo zero
clear; clc; close all;
G=tf(1,[1 3 2])
[n,d]=tfdata(G,'v')


% A partir do slide 9 da apresentação EE02
A = [0 1;-2 -3];
B = [0;1];
C = [1 0];
D = 0;

% autovalores desejados
s1 = -2+2i*sqrt(3);
s2 = -2-2i*sqrt(3);
s3 = -10;
A_chapeu = [A zeros(2,1);-C 0];
B_chapeu = [B;0];

% teste de controlabilidade   = n+1 ?
rank([A B;-C 0])

% Matriz de ganhos do controlador K_chapeu = [k1 k2 -ki]
K_chapeu=acker(A_chapeu,B_chapeu,[s1 s2 s3])

% Verificando
eig(A_chapeu-B_chapeu*K_chapeu)
