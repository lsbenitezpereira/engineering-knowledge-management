% Exemplo de servosistemas para planta do tipo 1 com integrador
clear; clc; close all;

G=tf(1,[1 3 2 0]);
[n,d]=tfdata(G,'v');
zpk(G)
FTMF_orig=feedback(G,1);
figure()
step(FTMF_orig);
title('Resposta ao o Degrau Sistema Original')
xlabel('t(ms)'); ylabel('Y');


[A2,B2,C2,D2] = tf2ss(n,d);
%Faz flip da matriz  
A=fliplr(flipud(A2))
B=fliplr(flipud(B2))
C=fliplr(flipud(C2))
D=fliplr(flipud(D2))



% Matriz de Controlabilidade
%Mc=[B A*B]
%disp('Determinante Mc')
%det(Mc)  %se det=0->matriz singular, n�o control�vel  
disp('Matriz controlabilidade, Rank')
Mc=ctrb(A,B)
% Posto da Matriz de Controlabilidade
rank_Mc=rank(Mc)   %para ser control�vel, rank=numero de linhas
if rank_Mc >= size(Mc,1)
   disp('Sistema Control�vel')
else
   disp('Sistema n�o Control�vel')
end   

disp('Matriz de Observabilidade, Rank')
% Matriz de Observabilidade
%Mo=[C' A'*C']
Mo=obsv(A,C)
% Posto da Matriz de Observabilidade
rank_Mo = rank(Mo)  %para ser control�vel, rank=numero de linhas
if rank_Mo >= size(Mc,1)
   disp('Sistema Observ�vel')
else
   disp('Sistema n�o Observ�vel')
end   


% autovalores desejados
s1 = -2+2i*sqrt(3);
s2 = -2-2i*sqrt(3);
s3 = -10;

J=[s1 s2 s3];

disp('Matriz de ganho')
K=acker(A,B,J)


% % Substitui��o da matriz A no polin�mio desejado phi(A)
% %phi_A=A^32+14*A^2+56*A+160*eye(3)      % ou 
% phi_A=polyvalm(eq_des,A)
% M=[B A*B A^2*B]
% % F�rmula de Ackreman
% K=[0 0 1]*inv(M)*phi_A     %pol ou



%**************************** %
% Verifica��o dos Autovalores
%*****************************%
disp('Verifica��o dos Autovalores')
eq_car=poly(A-B*K)
disp('Verifica��o das Raizes')
roots(eq_car)     % ou
eig(A-B*K)
%verificar se as raizes s�o [s1 s2 s3]


%Nova matriz do sistema pela lei X_ponto=Ax+Bu=(A-BK)x+BK(1)r
AA=A-B*K
BB=B*K(1)
CC=[1 0 0] %da lei controle y=x1
DD=[0]

t=0:0.01:5;
Y=step(AA,BB,CC,DD,1,t);
plot(t,Y);
title('Resposta ao o Degrau Sistema Compensado')
grid();



%*****************************************
%Equa��es recursiva
%******************************************

Q=zeros(3,3);
for i=1:size(Q,1) %linhas
    for j=1:size(Q,2) %colunas
       Q(i,j)=A(i,j)-(B(i,1)*K(1,j)); 
    end
end
   


% Condi��es Iniciais
x0=[0; 0; 0];

T = 0.01; %tempo de acomoda��o 5s
n=0:5/T;  %tempo final 5s
r=ones(1,length(n));
x1(1)=0;
x2(1)=0;    
x3(1)=0;
y(1)=CC(1)*x1(1)+CC(1)*x2(1)+CC(1)*x3(1)+DD*r(1);
% Equa��o diferencial de estados:  Xponto=(Ax+Bu)
x1_ponto(1)=Q(1,1)*x1(1)+Q(1,2)*x2(1)+Q(1,3)*x3(1)+B(1)*K(1)*r(1);  
x2_ponto(1)=Q(2,1)*x1(1)+Q(2,2)*x2(1)+Q(2,3)*x3(1)+B(2)*K(1)*r(1); 
x3_ponto(1)=Q(3,1)*x1(1)+Q(3,2)*x2(1)+Q(3,3)*x3(1)+B(3)*K(1)*r(1); 
    % Equa��o de Sa�da: Y=C*X+D*U


for j=2:length(n)
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    
    % Equa��o diferencial de estados:  Xponto=(Ax+Bu)
    x1_ponto(j)=Q(1,1)*x1(j)+Q(1,2)*x2(j)+Q(1,3)*x3(j)+B(1)*K(1)*r(j);  
    x2_ponto(j)=Q(2,1)*x1(j)+Q(2,2)*x2(j)+Q(2,3)*x3(j)+B(2)*K(1)*r(j); 
    x3_ponto(j)=Q(3,1)*x1(j)+Q(3,2)*x2(j)+Q(3,3)*x3(j)+B(3)*K(1)*r(j); 
    % Equa��o de Sa�da: Y=C*X+D*U
    
     y(j)=CC(1)*x1(j)+CC(2)*x2(j)+CC(3)*x3(j)+DD*r(j);
     
end

figure()
suptitle('Desempenho do Sistema Controlado')
subplot(2,2,1)
plot(n*T,x1,'*')
ylabel('x1')
subplot(2,2,2)
plot(n*T,x2,'*')
ylabel('x2')
subplot(2,2,3)
plot(n*T,x3,'*')
ylabel('x3')
subplot(2,2,4)
plot(n*T,y,'*')
ylabel('y')

figure()
plot(n*T,y,'o')
hold on 
plot(t,Y);
title('Resposta ao o Degrau Sistema Compensado')
xlabel('t(ms)'); ylabel('Y');
legend('Eqs. Recursivas','Fun��o Step')



