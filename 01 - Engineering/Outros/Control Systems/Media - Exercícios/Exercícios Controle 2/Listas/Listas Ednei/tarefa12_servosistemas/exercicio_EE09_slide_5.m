% Exemplo de servosistemas para planta do tipo zero
clear; clc; close all;

G=tf(1,[1 3 2 0]);
[n,d]=tfdata(G,'v');
zpk(G)

[A2,B2,C2,D2] = tf2ss(n,d);
%Faz flip da matriz  
A=fliplr(flipud(A2))
B=fliplr(flipud(B2))
C=fliplr(flipud(C2))
D=fliplr(flipud(D2))



% Matriz de Controlabilidade
%Mc=[B A*B]
%disp('Determinante Mc')
%det(Mc)  %se det=0->matriz singular, n�o control�vel  
disp('Matriz controlabilidade, Rank')
Mc=ctrb(A,B)
% Posto da Matriz de Controlabilidade
rank_Mc=rank(Mc)   %para ser control�vel, rank=numero de linhas
if rank_Mc >= size(Mc,1)
   disp('Sistema Control�vel')
else
   disp('Sistema n�o Control�vel')
end   

disp('Matriz de Observabilidade, Rank')
% Matriz de Observabilidade
%Mo=[C' A'*C']
Mo=obsv(A,C)
% Posto da Matriz de Observabilidade
rank_Mo = rank(Mo)  %para ser control�vel, rank=numero de linhas
if rank_Mo >= size(Mc,1)
   disp('Sistema Observ�vel')
else
   disp('Sistema n�o Observ�vel')
end   


% autovalores desejados
s1 = -2+2i*sqrt(3);
s2 = -2-2i*sqrt(3);
s3 = -10;

J=[s1 s2 s3];

disp('Matriz de ganho')
K=acker(A,B,J)


% % Substitui��o da matriz A no polin�mio desejado phi(A)
% %phi_A=A^32+14*A^2+56*A+160*eye(3)      % ou 
% phi_A=polyvalm(eq_des,A)
% M=[B A*B A^2*B]
% % F�rmula de Ackreman
% K=[0 0 1]*inv(M)*phi_A     %pol ou



%**************************** %
% Verifica��o dos Autovalores
%*****************************%
disp('Verifica��o dos Autovalores')
eq_car=poly(A-B*K)
disp('Verifica��o das Raizes')
roots(eq_car)     % ou
eig(A-B*K)
%verificar se as raizes s�o [s1 s2 s3]


%Nova matriz do sistema pela lei X_ponto=Ax+Bu=(A-BK)x+BK(1)r
AA=A-B*K
BB=B*K(1)
CC=[1 0 0] %da lei controle y=x1
DD=[0]

t=0:0.01:5;
Y=step(AA,BB,CC,DD,1,t);
plot(t,Y);
grid();



%*****************************************
%Equa��es recursiva
%******************************************

% Condi��es Iniciais
x0=[0; 0; 0];

T = 0.01; %tempo de acomoda��o 1s
n=0:1/T;  %tempo final 1s
r=ones(1,length(n));
x1(1)=0;
x2(1)=0;    
x3(1)=0;
y(1)=CC(1)*x1(1)+CC(1)*x2(1)+CC(1)*x3(1)+DD*r(1);
e(1)=r(1)-y(1)
u1(1)= K(1)*e(1)
u2(1)=-K(2)+x2(1)+K(1)*e(1)
u3(1)=-K(3)+x3(1)+K(1)*e(1)
% Equa��o diferencial de estados:  Xponto=(Ax+Bu)
x1_ponto(1)=AA(1,1)*x1(1)+AA(1,2)*x2(1)+AA(1,3)*x3(1)+BB(1)*u1(1);  
x2_ponto(1)=AA(2,1)*x1(1)+AA(2,2)*x2(1)+AA(2,3)*x3(1)+BB(2)*u2(1); 
x3_ponto(1)=AA(3,1)*x1(1)+AA(3,2)*x2(1)+AA(3,3)*x3(1)+BB(3)*u3(1); 
    % Equa��o de Sa�da: Y=C*X+D*U


for j=2:length(n)
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    
    y(j)=CC(1)*x1(j)+CC(2)*x2(j)+CC(3)*x3(j)+DD*r(j);
    
    e(j)=r(j)-y(j)
    
    u1(j)= K(1)+e(j)
    u2(j)=-K(2)+x2(j)+K(1)+e(j)
    u3(j)=-K(3)+x3(j)+K(1)+e(j)
    
    % Equa��o diferencial de estados:  Xponto=(Ax+Bu)
    x1_ponto(j)=AA(1,1)*x1(j)+AA(1,2)*x2(j)+AA(1,3)*x3(j)+BB(1)*u1(j);  
    x2_ponto(j)=AA(2,1)*x1(j)+AA(2,2)*x2(j)+AA(2,3)*x3(j)+BB(2)*u2(j); 
    x3_ponto(j)=AA(3,1)*x1(j)+AA(3,2)*x2(j)+AA(3,3)*x3(j)+BB(3)*u3(j); 
    % Equa��o de Sa�da: Y=C*X+D*U
          
    
end

figure()
suptitle('Desempenho do Sistema Controlado')
subplot(2,2,1)
plot(n*T,x1,'*')
ylabel('x1')
subplot(2,2,2)
plot(n*T,x2,'*')
ylabel('x2')
subplot(2,2,3)
plot(n*T,x3,'*')
ylabel('x3')
subplot(2,2,4)
plot(n*T,y,'*')
ylabel('y')
%OU
hold on
%plot(n*T,x1+x2+x3,'o')
ylabel('y')
hold off
