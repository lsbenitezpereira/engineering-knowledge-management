% Exemplo de servosistemas para planta do tipo zero sem integrador planta
% original
clear; clc; close all;
format long


C1=100e-9;
C2=680e-9;
R1=60e3;
R2=18e3;


% Matriz
A=[0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R1*R2*C2)];
B=[0; 1/(R1*C2)];
C=[1 0];
D=0;

[n,d] = ss2tf(A,B,C,D);
G2 = tf(n,d);
zpk(G2)

figure()
step(G2)    %ta do sistema 0,1s
title('Resposta ao o Degrau Sistema Original')
xlabel('t'); ylabel('Y');
grid();

%verificando Polos Sistema Original
syms s;
disp('Equa��o caracteristica Sistema Original')
eq_carac_ori=det(s*eye(2)-A)
pretty(solve(eq_carac_ori))


% autovalores desejados
s1 = -2.112676056338028e+002 +2.882485254595439e+002i;
s2 = -2.112676056338028e+002 -2.882485254595439e+002i;
s3 = real(s1)*10;                  % polo n�o dominante;


A_chapeu = [A zeros(2,1);-C 0];
B_chapeu = [B;0];

% teste de controlabilidade 
disp('Matriz controlabilidade, Rank')
Mc=[A B;-C 0]
rank_Mc = rank([A B;-C 0])
if (rank_Mc >= size(Mc,1) &(det(Mc)~=0))   %para ser control�vel, rank=numero de linhas
   disp('Sistema Control�vel')             %determinante Mc#0
else
   disp('Sistema n�o Control�vel')
end   



% Matriz de ganhos do controlador K_chapeu = [k1 k2 -ki]
K_chapeu=acker(A_chapeu,B_chapeu,[s1 s2 s3])


% Verificando Autovalores da Matriz
disp('Raizes do Sistema Controlado:')
eig(A_chapeu-B_chapeu*K_chapeu)
eq_car=poly(A_chapeu-B_chapeu*K_chapeu)
roots(eq_car)    



K = [K_chapeu(1) K_chapeu(2)];
Ki = -K_chapeu(3);
AA = [A-B*K B*Ki;-C 0];
BB = [0;0;1];
CC = [1 0 0];
DD = 0;

figure()
t=0:0.001:0.04;
Y=step(AA,BB,CC,DD,1,t);
plot(t,Y);
grid(); hold on;

%*****************************%
%   Equa��es diferenciais     %
%*****************************%
%   Matlab 
T = 0.0001; k = 0:0.04/T;
r = ones(1,length(k));

E = zeros(1,length(k));
u = zeros(1,length(k));
x = zeros(2,length(k));
y = zeros(1,length(k));
E_ponto = zeros(1,length(k));
x_ponto = zeros(2,length(k));

E_ponto(1) = r(1)-y(1);

for i=2:length(k)
   x(:,i) = T*x_ponto(:,i-1) + x(:,i-1);
   y(i) = C*x(:,i);
   E_ponto(i) = r(i)-y(i);
   E(i) = T*E_ponto(i-1) + E(i-1);
   u(i) = Ki*E(i) - K*x(:,i);
   x_ponto(:,i) =A*x(:,i)+B*u(i);
end
plot(k*T,y,'*');grid;


%   Microcontrolador
r = ones(1,length(k));

E = zeros(1,length(k));
u = zeros(1,length(k));
x1 = zeros(1,length(k));
x2 = zeros(1,length(k));
y = zeros(1,length(k));
E_ponto = zeros(1,length(k));
x1_ponto = zeros(1,length(k));
x2_ponto = zeros(1,length(k));

e_ponto(1) = r(1)-y(1);


for j=2:length(k)
   % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    

    % Equa��o de Sa�da: Y=C*X+D*U
    y1(j)=CC(1)*x1(j)+CC(2)*x2(j)+DD*r(j);  
   
    % erro
    E_ponto(j)=r(j)-y1(j);
    
    E(j)=T*E_ponto(j-1)+E(j-1);
    
    u(j)=Ki*E(j)-(K(1)*x1(j)+K(2)*x2(j));
    
    % Equa��o diferencial de estados:  Xponto=(Ax+Bu)
    x1_ponto(j)=A(1,1)*x1(j)+A(1,2)*x2(j)+B(1)*u(j);  
    x2_ponto(j)=A(2,1)*x1(j)+A(2,2)*x2(j)+B(2)*u(j); 
    
     
   
end

plot(k*T,y1,'o');grid;
title('Resposta ao o Degrau Sistema Compensado')
xlabel('t'); ylabel('Y');
legend('Step','Eqs. Matlab','Eqs. Recursivas')

figure()
plot(k*T,E_ponto,'o');grid;hold on;
plot(k*T,u,'x');grid;
title('An�lise Sistema')
xlabel('t'); ylabel('Y');
legend('Erro','A��o de Controle')
