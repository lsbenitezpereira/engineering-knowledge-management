% Exemplo de an�lise do per�odo de amostragem para sistemas subamortecidos
% varia��o da posi��o dos polos de malha fechada com a mudan�a do per�odo
% de amostragem

clear
clc
clf
T=[.5 0.2 0.1 0.05];
K=2;
Gp=tf(1,[1 1]);
H =tf(1,[1 0]);


    for j=1:length(T),
Gpz=c2d(Gp,T(j));
Hz=c2d(H,T(j));
FTMF=feedback(Gpz,Hz);
polos=pole(FTMF);
z1=polos(1);
z2=polos(2);
zplane([],polos)
hold on
axis([-1.1 1.1 -1.1 1.1])

pause
    end
    


hold off


