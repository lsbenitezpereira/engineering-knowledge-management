% Tarefa 3 - Ednei Freiberger
% Ex.7 lista transformada Z
clc
clear
% Solu��o da equa��o recursiva
% solu��o exata -> x(k)
% y(0)=0; 
% y(z)=(10z + 5)/(z^2 -1,2z +0,2)

% M�todo da fra��es parciais de X(z)/z - Transformada Z inversa
% Y(z)/z=1/(10z+5)/(z^2 -1,2z^2 +0,2z)
num = [10 5];
den = [1 -1.2 0.2 0];
[r,p,k] = residue(num,den)

% x(k) = 0.5*delta(k)-1*1^k+0.5*2^k
% y(k) = 25*delta(k) +18,75*1^k -43,75*0,2^k

% fun��o impulso
k = 0:20;                    
delta = zeros(1,length(k));
delta(1) = 1;   % para k=0

% solu��o exata
xe1 = 18.75*1.^k -43.75*0.2.^k;
xe = 25*delta+xe1;
plot(k,xe,'*')

hold on

% comportamento de x(k) a partir da equa��o recursiva
% y(k+1)-0,2y(k)=e(k)
% e(k) = 0 , k<0
% e(k) = 10 , k=1
% e(k) = 15, k>1

% entrada
ek(1)=10;
for i=2:length(k)
    ek(i)=15;
end
% condi��es iniciais
yr(1) = 0;      % para k=0


for j = 1:length(k)-1
    yr(j+1) = ek(j) +0.2*yr(j);
end

figure(1)
plot(k,yr,'o')
xlabel('k')
ylabel('y(k)')
title('evolu��o temporal de y(k)')

hold off
