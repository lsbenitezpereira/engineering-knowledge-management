% Tarefa 3 - Ednei Freiberger
% Ex.8 lista transformada Z
clc
clear
% Solu��o da equa��o recursiva
% solu��o exata -> x(k)
% y(0)=0; 
% y(z)=(10z)/(z^2 -1,018z +0,1637)

% M�todo da fra��es parciais de X(z)/z - Transformada Z inversa
% Y(z)/z=1/(10z)/(z^2 -1,018z +0,1637)
num = [10];
den = [1 -1.018 0.163 0];
[r,p,k] = residue(num,den)

% y(k) =  -16,126*0,2^k +16,162*(e^-0.2)^k

k = 0:20;
% solu��o exata
xe = -16.162*0.2.^k +16.162*0.818.^k;
plot(k,xe,'*')

hold on

% comportamento de x(k) a partir da equa��o recursiva
% y(k+1)-0,2y(k)=e(k)
% e(k) = 0 , k<0
% e(k) = 10.e^-0.2k , k>=0


% entrada

for i=1:length(k)
    ek(i)=10*exp(-0.2*(i-1));
end
% condi��es iniciais
yr(1) = 0;      % para k=0

%solu��o recursiva
for j = 1:length(k)-1
    yr(j+1) = ek(j) +0.2*yr(j);
end

figure(1)
plot(k,yr,'o')
xlabel('k')
ylabel('y(k)')
title('evolu��o temporal de y(k)')

hold off
