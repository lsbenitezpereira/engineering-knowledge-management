% EE06 - Rayan
clear; clc; close all;
t=0:1e-5:0.15;
% Matrizes do sistema
NT = 21;
R1 = 4e3*NT; R2 = 18e3; C1 = 100e-9; C2=680e-9;
A = [0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R2*R1*C2)];
B = [0;1/(R1*C2)];
C = [1 0];
D = 0;

% autovalores desejados
s1 = -2.112676056338028e2+2.882485254595439e2i;
s2 = -2.112676056338028e2-2.882485254595439e2i;
%Teste de controlabilidade
MC = ctrb(A',C');
ranke = rank(MC)

Ke = acker(A',C',[s1,s2])'
eig(A-Ke*C)

AA = [A zeros(length(A)); Ke*C A-Ke*C]
BB = [B;B]
CC = eye(2*length(A));
DD = zeros(2*length(A),1)

figure(1);

%eq rec

Ts=1e-6;
k=0:0.15/Ts;
r = ones(1,length(k));

x = zeros(2,length(k));
xt = zeros(2,length(k));
y = zeros(1,length(k));
yt = zeros(1,length(k));
e = zeros(2,length(k));
xp = zeros(2,length(k));
xpt = zeros(2,length(k));
xpt(:,1) = B*r(1);

for i=2:length(k)
    x(:,i) = Ts*xp(:,i-1) + x(:,i-1);
    xt(:,i) = Ts*xpt(:,i-1) + xt(:,i-1);
    y(i) = C*x(:,i);
    yt(i) = C*xt(:,i);
    e(:,i) = Ke*(y(i)-yt(i));
    xp(:,i) = A*x(:,i)+B*r(i);
    xpt(:,i) = A*xt(:,i)+B*r(i) + e(:,i);     
end

plot(k*Ts,y,'go', 'DisplayName','Sistema recursivo')
hold on;
sc = step(AA,BB,CC,DD,1,t);
plot(t,sc(:,1),'DisplayName','Sistema Controlado - y(1)','LineWidth',2);

hold off;
grid(); legend();
figure(2);
subplot(2,2,1); plot(k*Ts,x(1,:));
title('x1(t)'); grid();
subplot(2,2,2); plot(k*Ts,x(2,:));
title('x2(t)'); grid();
subplot(2,2,3); plot(k*Ts,xt(1,:));
title('xt1(t)'); grid();
subplot(2,2,4); plot(k*Ts,xt(2,:));
title('xt2(t)'); grid();