clc; close all;
% Exemplo de determina��o da f�rmula de Ackreman
zeta = 0.7; wn = 10;
% Matrizes do Sistema
A=[0 1 0;0 0 1;-1 -5 -6]
B=[0;0;1];
x0=[1;1;1]
%Autovalores Desejados
u1=-zeta*wn + 1j*wn*sqrt(1-zeta^2);
u2=-zeta*wn - 1j*wn*sqrt(1-zeta^2);
u3=-14;

% Matriz de Controlabilidade
%M=[B A*B A^2*B]    % ou
M=ctrb(A,B)
rank(M)            % teste de controlabilidade

% Equa��o caracter�stica desejada
%eq_des=conv([1 -u1],conv([1 -u2],[1 -u3]))    % ou
eq_des=poly([u1 u2 u3])

% Substitui��o da matriz A no polin�mio desejado phi(A)
%phi_A=A^3+14*A^2+60*A+200*eye(3)      % ou 
phi_A=polyvalm(eq_des,A)

% F�rmula de Ackreman
%K=[0 0 1]*inv(M)*phi_A     % ou
K=acker(A,B,[u1 u2 u3])

% Verifica��o dos Autovalores
eq_car=poly(A-B*K)
%roots(eq_car)     % ou
eig(A-B*K)

sys = ss(A-B*K, eye(3), eye(3), eye(3));
t = 0:0.01:4;
x = initial(sys,[1;1;1],t);
x1a = [1 0 0]*x';
x2a = [0 1 0]*x';
x3a = [0 0 1]*x';

T = 0.01;
Q = A-B*K;
k=0:4/T;
x1(1)=x0(1);  %  para k = 0
x2(1)=x0(2);  %  para k = 0
x3(1)=x0(3);  %  para k = 0
x1_ponto(1)=0;  %  para k = 0
x2_ponto(1)=0;  %  para k = 0
x3_ponto(1)=0;  %  para k = 0

for j=2:length(k)
    % Equa��es dos integradores
    x1(j)=T*x1_ponto(j-1)+x1(j-1);
    x2(j)=T*x2_ponto(j-1)+x2(j-1);    
    x3(j)=T*x3_ponto(j-1)+x3(j-1);
    % Equa��o diferencial de estados:  Xponto=(A-BK)*X
    x1_ponto(j)=Q(1,1)*x1(j)+Q(1,2)*x2(j)+Q(1,3)*x3(j);  
    x2_ponto(j)=Q(2,1)*x1(j)+Q(2,2)*x2(j)+Q(2,3)*x3(j); 
    x3_ponto(j)=Q(3,1)*x1(j)+Q(3,2)*x2(j)+Q(3,3)*x3(j);  
end
figure()
subplot(3,1,1); plot(t,x1,'*'); hold on; plot(k*T,x1a); grid;
title('Resposta � condi��o inicial')
ylabel('vari�vel de estado x1')
subplot(3,1,2); plot(t,x2,'*'); hold on; plot(k*T,x2a); grid;
ylabel('vari�vel de estado x2')
subplot(3,1,3); plot(t,x3,'*'); hold on; plot(k*T,x3a); grid;
xlabel('t (s)')
ylabel('vari�vel de estado x3')






















