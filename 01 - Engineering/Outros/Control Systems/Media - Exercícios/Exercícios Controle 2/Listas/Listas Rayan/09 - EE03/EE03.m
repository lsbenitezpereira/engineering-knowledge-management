% Erro em regime permanente no espa�o de estados
clc; close all;
A=[-5 1 0; 0 -2 1;20 -10 1];
B=[0;0;1];
C=[-1 1 0];

syms t
% entrada do tipo Degrau
essU = 1+C*inv(A)*B

%Entrada tipo Rampa
essR=limit(essU*t+C*inv(A)^2*B,t,inf)

% Observabilidade e Controlabilidade
NT = 21; R1 = 4e3*NT; R2 = 18e3; C1=100e-9; C2 = 680e-9;

A = [0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R2*R1*C2)];
B = [0;1/(R1*C2)]; C = [1 0]; D = 0;

% Controlabilidade
Mc = [B A*B]
detMc = det(Mc)

% Observabilidade
Mo = [C' A'*C']
rMo = rank(Mo)

