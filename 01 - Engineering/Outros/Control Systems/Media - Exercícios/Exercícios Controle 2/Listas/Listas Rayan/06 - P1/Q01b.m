% Q01 
% Rayan M. Steinbach
clc; close all;
Ta = 0.15; Zeta = 0.7; Wn = 2.5;

Gs = tf([1],[1 1]);
Gz = c2d(Gs,Ta)
Hs = tf([1],[1 0]);
GHs= Gs*Hs
GHz= zpk(c2d(GHs,Ta))

FTMF = minreal(Gz/(1+GHz));
figure(4);
step(FTMF)

s1=-Zeta*Wn+1j*Wn*sqrt(1-Zeta^2);
z1=exp(s1*Ta)

% Considera-se que o alfa ir� cancelar o polo (z-0.8607) de GHz
[numGz, denGz] = tfdata(Gz,'v');
[numGHz,denGHz] = tfdata(GHz,'v');
denG2 = [1 -1]
alpha = denGz;

% Utilizando o m�todo alternativo
G2 = polyval(numGHz,z1)/polyval(denG2,z1)
fi2 = angle(G2);
fi1 = -pi -fi2;
beta= (imag(z1)-real(z1)*tan(-fi1))/tan(-fi1)

C = tf(alpha,[1 beta],Ta)


%FTMA sem Kc
FTMA1 = minreal(C*GHz)

[NumFTMA,DenFTMA]=tfdata(FTMA1,'v');
Kc=abs((polyval(NumFTMA,z1)/polyval(DenFTMA,z1)));
Kc=1/real(Kc)
CK = Kc*C
FTMA2 = minreal(CK*GHz)

FTMF = zpk(minreal((CK*Gz)/(1+FTMA2)))

figure(1); rlocus(FTMF);
figure(2); step(FTMF);

%Equa��es recursivas;
k = 0:1:floor(5/Ta);
lsim(FTMF, k*Ta);
hold on;
r = 0:Ta:5;
%Para n=0
v(1) = 0; e(1) = r(1)-v(1); 
u(1) = 0; c(1) = 0;

%Para n=1
v(2) = 0.01071*u(1) + 1.861*v(1);
e(2) = r(2) - v(2);
u(2) = 5.175*e(2) - 4.455*e(1) +0.5388*u(1);
c(2) = 0.1393*u(1) + 0.5388*c(1);

for n=3:length(r)
    v(n) = 0.01019*u(n-2)+0.01071*u(n-1) +1.861*v(n-1) - 0.8607*v(n-2);
    e(n) = r(n) - v(n);
    u(n) = 5.175*e(n) - 4.455*e(n-1) + 0.5388*u(n-1);
    c(n) = 0.1393*u(n-1) + 0.8607*c(n-1);
end

plot(r, c,'*');
hold off





