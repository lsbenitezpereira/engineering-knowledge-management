%Q01 - P1 - Sistemas de controle II
clc;close all;
Ts = 0.1;    %Per�odo de amostragem
kMax = 50;  %N�mero de amostras

g1s = tf([1],[1 0])
g1z = c2d(g1s,Ts)

g2s = tf([1],[1 3]);
g2z = c2d(g2s,Ts)

Hs = tf([1],[1 6]);
Hz = c2d(Hs,Ts)

Gz = g1z+g2z
FTMA = zpk(Gz*Hz)
FTMF = zpk(minreal(Gz/(1+FTMA)))

Rz =tf([1 0], [1 -1],Ts)

Cz = zpk(Rz*FTMF)
[b,a] = tfdata(Cz,'v');
[r,p,k] = residuez(b,a)
k = 0:(kMax-1);
lsim(FTMF, k*Ts); % Resposta a rampa
hold on;
r = 0:Ts:(kMax-1)*Ts;
% Para n = 1
v(1) = 0;
e(1) = r(1) - v(1);
c(1) = 0;
a(1) = 0;
b(1) = 0;
for n=2:kMax
    v(n) = 0.0752*c(n-1)+0.5488*v(n-1);
    e(n) = r(n) - v(n);
    a(n) = 0.1*e(n-1)+a(n-1);
    b(n) = 0.0864*e(n-1)+0.7408*b(n-1);
    c(n) = a(n)+b(n);
end

k = 0:Ts:(kMax-1)*Ts;
plot(k, c,'*');
hold off

figure()
step(FTMF,5); hold on;
ck(1) = 6-5.6249-0.8185+0.4434;
for i=2:kMax
ck(i) = 6 - 5.6249*(0.9837)^(i-1) - 0.8185*(0.681)^(i-1) + 0.4434*(0.6249)^(i-1);
end
k = 0:Ts:(kMax-1)*Ts;
plot(k,ck,'o');

r = ones(1,kMax);
% Para n = 1
v(1) = 0;
e(1) = r(1) - v(1);
c(1) = 0;
a(1) = 0;
b(1) = 0;
for n=2:kMax
    v(n) = 0.0752*c(n-1)+0.5488*v(n-1);
    e(n) = r(n) - v(n);
    a(n) = 0.1*e(n-1)+a(n-1);
    b(n) = 0.0864*e(n-1)+0.7408*b(n-1);
    c(n) = a(n)+b(n);
end

plot(k, c,'*');
hold off



