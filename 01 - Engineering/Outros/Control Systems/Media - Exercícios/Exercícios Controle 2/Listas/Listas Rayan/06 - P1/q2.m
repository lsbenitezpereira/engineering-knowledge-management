T= 0.5   
num = [1]
den = [1 -0.5]
Gz = tf(num,den, T)
numh = [1 0]
denh = [1 -1]
Hz = tf(numh,denh, T)

FTMAs = minreal(Gz*Hz)

[numft, denft] = tfdata(FTMAs,'v')    

numc = [1 -0.5]
denc = [1]
Cz = tf(numc,denc, T)

z1 = 0.5 + 0.25*j
d2=[1 -1];

% fi2 � o �ngulo de G2z quando z=z1
fi2=angle(polyval(numft,z1)/polyval(d2,z1));
% fi1 � o �ngulo de G1z quando z=z1
fi1=-pi-fi2;
beta=(imag(z1)-real(z1)*tan(-fi1))/tan(-fi1)
disp('fi1:')
disp(fi1)
disp('fi2:')
disp(fi2)
disp('beta:')
disp(beta)

num2=[1];
den2=[1 beta];
Cz1=tf(num2,den2,T)

Cz=Cz1*Cz

FTMA1=minreal(Cz*FTMAs)
disp('FTMA sem K')

[NFTMA,DFTMA]=tfdata(FTMA1,'v');
K=abs((polyval(NFTMA,z1)/polyval(DFTMA,z1)));
K=1/real(K)

CzK=Cz*K;
FTMA2=minreal(Cz*FTMAs*K)
disp('FTMA2 com K')

FTMF=feedback(FTMA2,1)
