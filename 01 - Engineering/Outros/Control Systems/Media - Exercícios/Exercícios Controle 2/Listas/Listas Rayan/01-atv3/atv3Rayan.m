close all;
% Exercicio 7 - Lista 1
%y(k+1) - 0.2y(k) = e(k)
%y(k+1) = e(k) + 0.2y(k)
% e(k) = 0, k<0
% e(k) = 10, k=0
% e(k)=15, k >=1
% y(0) = 0
k = 20;
k1=linspace(0,20,21);
y = zeros(k+1,1); y(1)=0;
e = zeros(k+1,1); e(1) = 10;
for i=2:k+1
    e(i)=15;
end

for i=1:k
    y(i+1)=e(i)+0.2*y(i);
end

yex = zeros(k+1,1); yex(1)=0;
yex(2:k+1) = 18.75-8.75.*0.2.^(k1(2:k+1)-1);

figure;
stem(k1,y); hold on;
plot(k1,yex); hold off;
