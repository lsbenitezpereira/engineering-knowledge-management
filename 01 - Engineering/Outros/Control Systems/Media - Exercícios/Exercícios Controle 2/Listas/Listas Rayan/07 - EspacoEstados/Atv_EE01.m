% Representa��o de sistemas no espa�o de estados
% Rayan M. Steinbach
clc;close all;
% parametros do circuito
R=5; L=5e-3; C=100e-6;

% Matrizes
A = [0 -1/L;1/C -1/(R*C)];
B = [1/L;0];
C = [0 1];
D = 0;

% sistema no espa�o de estados
sys = ss(A,B,C,D)

% convertendo para fun��o de tranfer�ncia com a utiliza��o da Equa��o
s=tf([1 0],1);
G=C*inv(s*eye(2)-A)*B+D

% convertendo G(s) para espa�o de estados com a utiliza��o da fun��o do
% Matlab
[numG,denG] = tfdata(G,'v');
[A2,B2,C2,D2] = tf2ss(numG,denG)
sys2 = ss(A2,B2,C2,D2)

step(sys)
hold on
step(G)
step(sys2)
hold off
