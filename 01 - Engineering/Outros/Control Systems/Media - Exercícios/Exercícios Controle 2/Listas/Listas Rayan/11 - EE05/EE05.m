% EE05 - Rayan
clear; clc; close all;

% Matrizes do sistema
R1 = 6e4; R2 = 18e3; C1 = 100e-9; C2=680e-9;
A = [0 1/(R2*C1);-1/(R1*C2) -(R1+R2)/(R2*R1*C2)];
B = [0;1/(R1*C2)];
C = [1 0];
D = 0;


% autovalores desejados
s1 = -2.112676056338028e2+2.882485254595439e2i;
s2 = -2.112676056338028e2-2.882485254595439e2i;
s3 = -10e2;
A_chapeu = [A zeros(2,1);-C 0];
B_chapeu = [B;0];
% teste de controlabilidade   = n+1 ?
rank([A B;-C 0])

% Matriz de ganhos do controlador K_chapeu = [k1 k2 -ki]
K_chapeu=acker(A_chapeu,B_chapeu,[s1 s2 s3])


% Verificando
eig(A_chapeu-B_chapeu*K_chapeu)
t=0:0.00001:0.15;

K = [K_chapeu(1) K_chapeu(2)];
Ki = -K_chapeu(3);
AA = [A-B*K B*Ki;-C 0];
BB = [0;0;1];
CC = [1 0 0];
DD = 0;
step(AA,BB,CC,DD,1,t);
hold on;
step(A,B,C,D,1,t);

%Equa��es diferenciais
T = 0.0001; k = 0:0.15/T;
r = ones(1,length(k));

E = zeros(1,length(k));
u = zeros(1,length(k));
x = zeros(2,length(k));
y = zeros(1,length(k));
E_ponto = zeros(1,length(k));
x_ponto = zeros(2,length(k));

E_ponto(1) = r(1)-y(1);

for i=2:length(k)
   x(:,i) = T*x_ponto(:,i-1) + x(:,i-1);
   y(i) = C*x(:,i);
   E_ponto(i) = r(i)-y(i);
   E(i) = T*E_ponto(i-1) + E(i-1);
   u(i) = Ki*E(i) - K*x(:,i);
   x_ponto(:,i) =A*x(:,i)+B*u(i);
end
plot(k*T,y,'*');grid;
hold off;
