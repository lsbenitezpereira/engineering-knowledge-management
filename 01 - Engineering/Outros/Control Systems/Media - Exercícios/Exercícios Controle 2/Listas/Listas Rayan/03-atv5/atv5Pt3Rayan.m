clc; close all; clear;
T = 0.1;
Gdz = tf([1 0],[1 -1],T);
Gps = tf(1,[1 1]);
%Gk(s) -> ZOH
Gpz = c2d(Gps,T);
figure(1);
FTMA = Gpz*Gdz
rlocus(FTMA)
figure(2);
k = 2;
FTMF = feedback(k*FTMA,1);
p = pole(FTMF)
plot(real(p),imag(p),'-p');
zgrid;