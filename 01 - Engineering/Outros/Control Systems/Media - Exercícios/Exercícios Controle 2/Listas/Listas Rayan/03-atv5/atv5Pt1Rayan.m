  % Tarefa 5 - parte 1
clear; clc;
T=0.1;
Gs=tf(1,[1 1 0])
Gz=c2d(Gs,T)
FTMF= minreal(Gs/(1+Gs))
poles = pole(FTMF) %#ok<*NOPTS>
figure(1)
step(FTMF)
% clicando  com o bot�o direito do mouse na parte branca do gr�fico
% na op��o characteristics � possivel verificar os seguintes par�metros
% peak response - tempo de pico
% settling time - tempo de acomoda��o
% rise time - tempo de subida
% stead state - valor de regime permanente
%
% em properties/options � poss�vel escolher os parametros 
% de tempo de subida e tempo de acomoda��o
% armazenamento dos parametros considerando tempo de acomoda��o de 5%
 parametros = stepinfo(FTMF,'SettlingTimeThreshold', 0.05)