% Tarefa 5 - Parte 2
close; clc; clear; format long;

Gs=tf(1,[1 1 0])
T=0.1;
Gz=c2d(Gs,T);
FTMA=Gz

% Erro ao Degrau
[nd,dd] = tfdata(FTMA,'v');
kp = polyval(nd,1)/polyval(dd,1)

essd = (1+kp)^-1

FTMF=feedback(Gz,1);
t=0:T:30;
degrau=ones(length(t),1);
saidad=lsim(FTMF,degrau,t);
figure(1)
plot(t,degrau,'*',t,saidad,'--',t,saidad, '+')
errod = degrau(end) - saidad(end)

%Erro � rampa
% importante, erro em regime permanente � calculado a partir 
% da fun��o de transfer�ncia de la�o aberto
aux=minreal(tf([1 -1],[1 0],T)*FTMA);  % onde tf([1 -1],[1 0],T) = (z-1)/z = 1-z^-1
[nr,dr]=tfdata(aux,'v');
kv=(polyval(nr,1)/polyval(dr,1))/T;   % substituindo 1 em aux

if abs(kv) <=1e-3
    kv = 0
    essr = 'inf'
else
    kv
    essr=1/kv
end

% erro pode ser verificado a partir da FTMF como sendo a diferen�a entre a
% entrada e a sa�da, por�m isto s� � poss�vel neste caso por que a 
%realimenta��o � unit�ria. Caso n�o seja, deve ser observado o sinal de 
%sa�da do somador

rampa=t;
saidar=lsim(FTMF,rampa,t);
figure(2)
plot(t,rampa,'*',t,saidar,'+')
error = rampa(end) - saidar(end)
% Como o erro � rampa � infinito, o erro ao final da simula��o n�o condiz 
% com o erro real, afinal o erro continuar� crescendo linearmente at� o
% interrompimento do sistema ou at� ao infinito, em sistemas tipo 0.

%Erro � par�bola
aux = minreal(tf([1 -2 1],[1 0 0],T)*FTMA) % onde tf([1 -2 1],[1 0 0],T) = [(z-1)/z]^2 = [1-z^-1]^2
[np,dp] = tfdata(aux,'v');
ka = (polyval(np,1)/polyval(dp,1))/T^2;

if abs(ka) <=1e-3
    ka = 0
    essa = 'inf'
else
    ka
    essa=1/ka
end

parabola=t.^2;
saidap=lsim(FTMF,parabola,t);
figure(3)
plot(t,parabola,'*',t,saidap,'--',t,saidap, '+')
errop = parabola(end) - saidap(end) 
% O erro � par�bola cria a mesma situa��o do erro � rampa 
% para sistemas de tipo 1 e tipo 0
