% EE03 - Solu��o da equa��o diferencial de estados

% Exerc�cio slide 13
clc;close all;
syms s
A = [0 1;-8 -6]; B = [0;1]; x0=[1;0];C1 = [1 0]; C2 = [0 1];
M=eye(2)*s-A;
Minv=inv(M)
U=1/s;
xs=simplify(Minv*x0+Minv*B*U)
E = eig(M)

t=0:0.001:5;
xt = [0.125+1.75*exp(-2*t)-0.875*exp(-4*t);3.5.*exp(-4*t)-3.5*exp(-2*t)];
figure('Name', 'Estado 1 de x(t)');
subplot(2,1,1);plot(t,xt(1,:));
title('x1 - Solu��o encontrada');
u = ones(1,length(t));
s1 = ss(A,B,C1,0);
xt1 = lsim(s1,u,t,x0);
subplot(2,1,2); plot(t,xt1)
title('x1 - Solu��o matlab');

figure('Name', 'Estado 2 de x(t)');
subplot(2,1,1); plot(t,xt(2,:));
title('x2 - Solu��o encontrada');
s2 = ss(A,B,C2,0);
xt2 = lsim(s2,u,t,x0);
subplot(2,1,2);plot(t,xt2);
title('x2 - Solu��o matlab');




