clc; clear all;

K=2; T=2;

Gps=tf(1,[1 1]);
Gpz=c2d(Gps,T)
Gdz=tf(K*[1 0],[1 -1],T)
FTMF=feedback(Gdz*Gpz,1);
step(FTMF)
param = stepinfo(FTMF,'SettlingTimeThreshold',0.05)
polos=pole(FTMF)
z=polos(1)

modulo=abs(z)
angulo=angle(z)
TZetaWn=-log(modulo)
fator=TZetaWn/angulo
syms x
zeta=eval(solve(x/sqrt(1-x^2) == fator))
wn=TZetaWn/T/zeta
mp=exp(-pi*zeta/sqrt(1-zeta^2))
ts5=3/(zeta*wn)
wawd=2*pi/angulo