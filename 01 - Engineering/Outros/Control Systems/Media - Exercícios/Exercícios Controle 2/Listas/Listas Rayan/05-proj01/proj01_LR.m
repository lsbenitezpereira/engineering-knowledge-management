%Projeto 1 Controle II
%Rayan M. Steinbach
%Dados Planta
%Tp = 19,2ms
%Zeta=0,36631
%wn=175,84
%G(s)=175,84^2/(s^2 +128,829s +175,84^2)
clear;clc;close all;

%Gs a partir do comportamento da planta
Tp=19.2e-3; denG2=496e-3; d1=144e-3;
%Sobresinal Planta
MP= d1/denG2

%Fator Amortecimento Original ZETA
ZetaG=sqrt(log(MP)^2/(pi^2+log(MP)^2))

%Frequencia Natural original WN
WnG=pi/(Tp*sqrt(1-ZetaG^2))

nums=[WnG^2]; dens=[1 2*ZetaG*WnG WnG^2];
Gs=tf(nums,dens)

trG = (2.16*ZetaG+0.6)/WnG

ts5G = 3/(ZetaG*WnG)

Tp

MP

figure(1)
title('Resposta ao degrau sem controlador')
step(Gs)

%Caracteristicas Desempenho planta Original
%MP=29% tp=19.3ms  ts5%=44.5ms   Valor Final=1  

%-------------------------------------------------------%
%controlador Plano Z Lugar Raizes
%Requisitos de Projeto:
%ts5%=21ms   MP=10%    para entrada degrau de 1 para 1,5
Mp=0.1; Ts5=21e-3; Ta = 1.8e-3;

%Calculo Fator de Amortecimento (Zeta)
Zeta=sqrt(log(Mp)^2/(pi^2+log(Mp)^2))

%Calculo Frequ�ncia Natural n�o amortecida (Wn)
Wn=3/(Zeta*Ts5)

%Z1 Polo dominante de Malha Fechada desejado
s1=-Zeta*Wn+1j*Wn*sqrt(1-Zeta^2);
z1=exp(s1*Ta)

Gz=c2d(Gs,Ta)
disp('Gz:')
zpk(Gz)
%Encontrando beta
[numz,denz]=tfdata(Gz,'v');
alfa = denz
denG2=[1 -1];
fi2=angle(polyval(numz,z1)/polyval(denG2,z1));
fi1=-pi-fi2;
beta=(imag(z1)-real(z1)*tan(-fi1))/tan(-fi1)

num2=[1];
den2=[1 beta];
Cz1=tf(num2,den2,Ta)

num3=[alfa];
den3=[1 -1];  %integrador
Cz2=tf(num3,den3,Ta)

Cz=Cz1*Cz2

FTMA1=minreal(Cz*Gz)
disp('FTMA sem K')
zpk(FTMA1)

%C�lculo de K
[NFTMA,DFTMA]=tfdata(FTMA1,'v');
K=abs((polyval(NFTMA,z1)/polyval(DFTMA,z1)));
K=1/real(K)


CzK=Cz*K
FTMA2=minreal(Cz*Gz*K)
disp('FTMA2 com K')
zpk(FTMA2)

FTMF=feedback(FTMA2,1)
figure(4);
rlocus(FTMF)

%Equa��o Recursiva
n = 0:50; ref = ones(1,length(n)); % degrau
y = zeros(1,length(n));e = ones(1,length(n));
u = zeros(1,length(n)); u(1) = 1.642;
y(2) = 0.04606*u(1);
u(2) = 1.642*e(2)-2.799*e(1)+1.528*u(1);
for k=3:length(n)
    y(k) = 0.04606*u(k-1)+0.04262*u(k-2)+1.704*y(k-1)-0.793*y(k-2);
    e(k) = ref(k) - y(k);
    u(k) = 1.642*e(k)-2.799*e(k-1)+1.302*e(k-2)+1.528*u(k-1)-0.5279*u(k-2);
end
figure(2);
hold on;
plot(n*Ta,y,'*')
title('f(t), f(kT) e f(kT)recursiva')
xlabel('t(ms)'); ylabel('Y');
step(FTMF,length(n)*Ta); 
hold off;

%Degrau 1.5V
n = 0:50; ref(1:length(n)) = 1.5; % degrau
y = zeros(1,length(n));e = zeros(1,length(n));
u = zeros(1,length(n));
e(1) = ref(1)-y(1);
u(1) = 1.642*e(1);
y(2) = 0.04606*u(1);
e(2) = ref(2) - y(2);
u(2) = 1.642*e(2)-2.799*e(1)+1.528*u(1);
for k=3:length(n)
    y(k) = 0.04606*u(k-1)+0.04262*u(k-2)+1.704*y(k-1)-0.793*y(k-2);
    e(k) = ref(k) - y(k);
    u(k) = 1.642*e(k)-2.799*e(k-1)+1.302*e(k-2)+1.528*u(k-1)-0.5279*u(k-2);
end

figure(3)
opt = stepDataOptions('StepAmplitude',1.5);
hold on;
plot(n*Ta,y,'*')
title('f(t), f(kT) e f(kT)recursiva')
xlabel('t(ms)'); ylabel('Y');
step(FTMF,length(n)*Ta,opt); 
hold off;