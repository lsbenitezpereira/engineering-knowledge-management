%Projeto 1 Controle II
%Rayan M. Steinbach
clear;clc;close all;
%G(s)=175,84^2/(s^2 +128,829s +175,84^2)
numG = [0 0 175.84^2]; denG = [1 128.829 175.84^2];
G = tf(numG,denG);

%Requisitos de Projeto:
%ts5%=21ms   MP=10%    para entrada degrau de 1 para 1,5
Mp=0.1; Ts5=21e-3; Ta = 1.8e-3;
%Calculo Fator de Amortecimento (Zeta)
Zeta=sqrt(log(Mp)^2/(pi^2+log(Mp)^2))
%Calculo Frequ�ncia Natural n�o amortecida (Wn)
Wn=3/(Zeta*Ts5)
Gz = c2d(G,Ta);
%Determina��o do polo dominante no plano s
s1 = -Zeta*Wn+1j*Wn*sqrt(1-Zeta^2)
z1 = exp(s1*Ta)
Ms = abs(s1)
thetaS = angle(s1)
g1 = polyval(numG,s1)/polyval(denG,s1)
Mg = abs(g1)
thetaG = angle(g1)
Ki = 190
Kp = (-sin(thetaG-thetaS)/(Mg*sin(thetaS))) - (2*Ki*cos(thetaS))/Ms
Kd = (sin(thetaG)/(Ms*Mg*sin(thetaS))) + Ki/Ms^2

numC = [Kd+(Ki*Ta^2)+Kp*Ta -(2*Kd+Kp*Ta) Kd];
denC = [Ta -Ta 0];
Cz = zpk(tf(numC,denC,Ta))

FTMA = zpk(minreal(Cz*Gz))
FTMF = feedback(FTMA,1)
figure(2);
step(FTMF)
figure(3);
rlocus(FTMF)
