# Item a)

$\beta = -0.08306705601996371$
$K_c = 14.758185415337483$

# Item b)

Gráfico de x(kT)

![image-20210719120927743](Images - folha de respostas/image-20210719120927743.png)

# Item c)

ess = 0

# Item d)
Equação do erro: e(k)
$$
e[n] = x[n] - v[n] 
$$

Equação recursiva do bloco F:
$$
x[n] = 0.2*r[n-1] + 1*x[n-1]
$$

Equação recursiva do bloco C:
$$
u[n] = 14.76*e[n] - 8.855*e[n-1] + 0.08307*u[n-1]
$$

Equação recursiva do bloco G:
$$
c[n] = 0.1648*u[n-1] + 0.6703*c[n-1]
$$

Equação recursiva do boco GH:
$$
v[n] = 0.01758*u[n-1] + 0.01539*u[n-2] + 1.67*v[n-1] - 0.6703*v[n-2]
$$

Gráfico de c(kT)

![image-20210719121415474](Images - folha de respostas/image-20210719121415474.png)


# Item e)
U_max = 3.61

Gráfico de u(kT)

![image-20210719121610314](Images - folha de respostas/image-20210719121610314.png)