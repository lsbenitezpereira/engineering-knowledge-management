> Play is the highest form of research.
> Albert Einstein

# Robotics

* study of robots
* **What is a robot**
  * flexível e programável, diferente de um equipamento
  * substitute or replicate human actions
  * Bio-inspired robotics
    * making robots that are inspired by biological systems
* **TEchnologies**
  * pneumático
    * rápido
    * quase o mesmo preço do elétrico
    * oscila, não muito estável
  * Hidrautlico
    * Forte pakas
    * Muito usado em aplicações submersas
  * Elétrico
* **Choosing a robot**
  * task (grab, smash, etc)
  * play Load (o quanto consegue manusear na extreminade) and range
  * procure usar sempre a mesma marca de robo
    * because they talk esealy
    * because the programming will be the same 

## Main types

* cylindrical, , polar and vertically articulated?
* Robotics arm
  * 6 dof (human arm have 7)
  * You can remove the last actuator (remaining with 5 dof)
* Scara
  * 4 dof
* Cartesian
  * 3 dof
* Delta
  * rápido



## Other stufs

[Making A Lego Car Climb several types of Obstacles.](https://9gag.com/gag/a27zmNZ)

# Kinematics

* estudo do movimento 
  dos robôs sem levar em conta as forças 
  e as massas envolvidas.

  Cinemática direta: A partir das posições das articulações, encontrar a posição e orientação da ferramenta no espaço cartesiano da base. – 

  Cinemática inversa: Definir as posições das articulações, dada uma posição e orientação desejada para a ferramenta


## Sistemas de Referências

Ou Frames

descrição da posição e orientação de 
um objeto.
composto por 4 matrizes, que 
eqüivalem a uma matriz de posição 
(origem do sistema) e uma matriz de 
rotação.

A relação entre dois sistemas quaisquer 
é conseguida com uma translação e 
uma rotação.

Mapeamento: a mudança de descrição
de um frame para outro.
Um conjunto de transformações no 
mundo 2D pode ser representada 
completamente por uma matriz 3 x 3

## manipuladores

Um manipulador é uma cadeia cinética 
composta por elos e juntas

### Elos

Links

Os corpos da cadeia

### Juntas

Joints

As articulações entre os corpos. Conectam os elos e permitem a realização de movimentos de um elo em relação ao elo anterior.

[20 Mechanical Principles combined in a Useless Lego Machine](https://www.youtube.com/watch?v=M1-YeqGynlw)

Tipos de juntas
■ Revolução (R):
– 1 Dof (Rotação)
■ Prismática (P):
– 1 Dof (Translação)
■ Cilindrica (C):
– 2 Dof (Rotação + Translação)
■ Helicoidal (H)
– 1 Dof (Rotação/ Translação com acoplamento)
■ Planar (E) 
– 2 Dof (Translação em 2 direções)
■ Esférica (S) 
– 3 Dof (Rotação em 3 direções)

# Sensing

* About the sensors themselves, see in *Computer Engineering - Instrumentação Eletrônica*
* About algorithms to process/merge/analyze the sensor data, see in *Signal Processing*

# Navegation

* See in *Signal Processing*