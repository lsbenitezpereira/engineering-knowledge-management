

[playlist do Jackson Lago](https://www.youtube.com/playlist?list=PLHEJ7BYJW5DTcg8TxU-CWof3xDnzb-iDC)

[OCW interessante](https://ocw.tudelft.nl/courses/electrical-machines-and-drives/subjects/subject-1/)

[Book - Introduction to Electric Machines and Drives](https://www.amazon.com/Introduction-Electric-Machines-Drives-Novotny/dp/0974547042)

Motores eletricos
=================

-   Motor elétrico foi feito para trabalhar no talo (se não operar no
    seu 100% não é legal)

-   Rendimento:

$n\text{\%} = \frac{P_{\text{mecanica}}}{P_{\text{eletrica}\left( \text{ativa} \right)}} \ast 100$

* **Perdas**
  * Perdas de exitação (na propria bobina)
  * perdas no induzido da bobina
  * perdas no ferro
  * perdas mecânicas

## Stepper

(-) o torque não é constante, e sim pul

A stepper motor may have any number of coils. But these are connected in
groups called \"phases\". All the coils in a phase are energized
together.

Stepper control with digital counter:
<http://electronics-diy.com/electronic_schematic.php?id=516>

[https://circuitdigest.com/electronic-circuits/stepper-motor-driver]()

Standalone board control: <https://www.pololu.com/docs/0J71/all>

**Unipolar** drivers, always energize the phases in the same way. One
lead, the \"common\" lead, will always be negative. The other lead will
always be positive. Unipolar drivers can be implemented with simple
transistor circuitry. The disadvantage is that there is less available
torque because only half of the coils can be energized at a time.\
\
**Bipolar** drivers use H-bridge circuitry to actually reverse the
current flow through the phases. By energizing the phases with
alternating the polarity, all the coils can be put to work turning the
motor.\
A two phase bipolar motor has 2 groups of coils. A 4 phase unipolar
motor has 4. A 2-phase bipolar motor will have 4 wires - 2 for each
phase. Some motors come with flexible wiring that allows you to run the
motor as either bipolar or unipolar.

![](./Images/Motores e Conversores eletromecanicos/media/image1.png){width="7.2652777777777775in"
height="4.876388888888889in"}

* * 

## DC

* muito usados quando queremos controlar a velocidade
* Motores baseados em campo elétrico?
  * Todos os DC?
  * campos eletricos possuem baixa densidade de corrente
  * Então esse tipo de motor é usado apenas em aplicações com baixo torque 

### Brushless DC

* Or *BLDC*
* (-) exige mais manutensão que motorde indução
* (+) fácil de controlar a velocidade
* existe uma forte tendencia de, ao invés de usar motor CC, usar um inversor com motor de indução
* Applications: aquela scooters eletricas costumam ser um brushless CC
* imã permanente
* na verdade é um motor CA com inversor?
* você precisa alimentar com uma tensão alternada, não DC puro; a diferença para um CA é que ele aceita uma AC “chapada” só positivo-negativo sem frescura de PWM, 3 níveis, alta frequencia, etc. 

### motor DC

* Armadura: dentro

* estator: fora

* escova: geralmente de grafica (que é condutora e lubrificante sólida)

* mancal = rolamento?

* o espaço de entreferro, entre rotor e estator, costuma ser o menor possível

* imã pode ser natural ou eletroima (geralmente só quando precisa ser muito grande, que não se consiga de forma natural)

  ![image-20201216082013773](Images - Motores e Conversores eletromecanicos/image-20201216082013773.png)

* **modelling**

  * J representa a inércia, do rotor

  ![image-20201216083110501](Images - Motores e Conversores eletromecanicos/image-20201216083110501.png)

* **controle**
  * pela corrente de armadura ou do estator (caso for con eletroimã)
  * geralmente, pela de armadura 
  * o termo mecânico é mais lento que o termo elétrico, e é ele que dita o comportamento dinâmico
  * geralmente desprezamos o termo elétrico
* **outros**
  * motores parar gerar energia, que devem girar continuamente na mesma velocidade, possuem J alto (para a mesma massa, são mais gordinhos)
  * motores para mover coisas, que geralmente vão e param, possuem J baixo (para a mesma passa, são mais alongadinhos)

### ServoDC

torque maior que o servo normal?

## AC

* queremos que o campo seja um vetor girante, $\vec B = |B|*e^{jwt}$

  ![image-20210728203504754](Images - Motores e Conversores eletromecanicos/image-20210728203504754.png)

  ![image-20210710223129881](Images - Motores e Conversores eletromecanicos/image-20210710223129881.png)

* Coisas práticas
  * se formos diminuir a frequencia, é comum diminuirmos também um pouco da tensão para não causar saturação magnética no motor

### Síncrono

  * 
  * (+)  alto rendimento
  * (+) podem trabalhar como compensador síncrono para corrigir o fator de potência da rede
  * (+) Altos torques
  * (+) velocidade constante nas variações de carga
  * (+) baixo custo de manutenção
  * 
  * 
  * o circuito do rotor de um motor síncrono é alimentado por uma fonte CC; suprido através de anéis coletores e
    escovas (excitatriz estática) ou através de uma excitatriz girante sem escovas (brushless).
  * 
  * ==o motor brushlessCC é dessa??==
  * a rotação mecânica é da mesma frequência da elétrica
  * se perder o sincronismo o torque vai e volta (e é nulo na média) e provavelmente vai queimar; 
  * Idéia básica do eletroimã no estator com um imã fixo do rotor:

  ![image-20210710223307601](Images - Motores e Conversores eletromecanicos/image-20210710223307601.png)

  * a partida pode ser especialmente complicada, e a inércia deve ser vencida em menos de um ciclo elétrico 

  * Outra opção de partida é progressivamente aumentando a frequência de alimentação:

    ![image-20210728205253180](Images - Motores e Conversores eletromecanicos/image-20210728205253180.png)

### Motor de indução trifásico

* os mais usados a industria (~95%)
* Conseguimos controlar a relação entre frequência elétrica e mecânica
* O rotor é um eletroimã, alimentado por indução
* **Visão geral**
  * correntes no estador geram um campo variante;
  * esse campo girante induz tensões no rotor; induz corrente no rotor;
  * as correntes no rotor, que também possuem um campo magnético; esse campo não possui amplitude constante, depende do escorregamento (maior escorregamente, maior a intensidade do campo induzido no rotor)
  * o torque é proporcional ao produto vetorial do campo induzido (do rotor) com o campo indutor (do estador); o torque não será maximo na partida pois esse angulo entre os campos induzido e indutor é maior do que 90º







* trifásico = 3 fases no estator

* consideraremos no rotor 3 bobinas defasadas 120º (apesar deste não ser o caso mais usual)

* consideraremos 2 polos no estator (veja detalhes mais à frente)

  ![image-20210807195226857](Images - Motores e Conversores eletromecanicos/image-20210807195226857.png)

* o campo magnético gerado pelo estator gira em relação ao rotor, portanto há fluxo variável nas bobinas do rotor, portanto correntes induzidas no rotor

* correntes de reação aparecem no estator, anulando o campo gerado pelo rotor ($i_n^{'}$, onde é é a espira doestator a,b,c)

* se campo girante e rotor tiverem a mesma velocidade, o torque será zero

* se o rotor estiver parado, o torque existe mas não é máximo

* escorregamento (s): relacionado à diferença de velocidade mecânica e elétrica
  $$
  \begin{aligned}
  & s=\frac{w-w_r}{w} \\
  & w = w_s = \text{velocidade angular do campo elétrico} \\
  & w_r = \text{ velocidade angular do rotor} \\
  \end{aligned}
  $$
  
* velocidade de rotação maior = escorregamento menor

* mais carga = maior escorregamento

* a corrente de magnetização ($i_{ma}$): ?; geralmente é da ordem de 40% da corrente nominal; é grande pois é preciso haver um entreferro entre estator e rotor;

* 

* angulo de carga ($\delta$ ou $\gamma$???): angulo entre o campo girante e o eixo do rotor; Variável instantânea?

* denominamos as espiras do rotor x, y, e z (e r a resultante)

  ![image-20210728212021216](Images - Motores e Conversores eletromecanicos/image-20210728212021216.png)

  ![image-20210728212055524](Images - Motores e Conversores eletromecanicos/image-20210728212055524.png)

  ![image-20210730234826702](Images - Motores e Conversores eletromecanicos/image-20210730234826702.png)

  ![image-20210730234841022](Images - Motores e Conversores eletromecanicos/image-20210730234841022.png)

* u = momento  magnético gerado pelo rotor?

* Nr = ?

* Ne = ?

* Be = campo magnético gerado pelo estator

  ![image-20210728212319082](Images - Motores e Conversores eletromecanicos/image-20210728212319082.png)

  ![image-20210728213735016](Images - Motores e Conversores eletromecanicos/image-20210728213735016.png)

  ![image-20210730232814064](Images - Motores e Conversores eletromecanicos/image-20210730232814064.png)

  ![image-20210730233451510](Images - Motores e Conversores eletromecanicos/image-20210730233451510.png)

* $\tau$ = torque

* $\vec \tau = \vec u_x \times \vec B_e$

* $\tau=\frac{\text{Potencia ativa no entreferro}}{\text{velocidade síncrona}} =\frac{3I_2R_2/s}{w_s}$, se calculado em função da potencia dissipada no entreferro (a segunda parte do circuito equvalente); Torque de partida por ser encontrado por essa equação tomando s=1

* $\tau = \frac{\text{Potencia mecanica}}{\text{velocidade angular}} = \frac{3I_2R_2(\frac{1-s}{s})}{w_r}$, se calculado em função da potencia dissipada no resistor variável

* O torque será constante no tempo

* torque positivo = rotação no sentido antihorário

  ![image-20210728214124218](Images - Motores e Conversores eletromecanicos/image-20210728214124218.png)

  ![image-20210728214713901](Images - Motores e Conversores eletromecanicos/image-20210728214713901.png)

  

* o torque do motor deve ser sempre maior que o torque resistente da carga, em todos os pontos entre zero e a rotação nominal. Se não, o motor irá parar de acelerar antes da velocidade nominal

  ![image-20210826225730933](Images - Motores e Conversores eletromecanicos/image-20210826225730933.png)

* classe (ou indice de rendimento): determinada pela inclinação da curva; indica o rendimento; IR1 (pior) até IR5 (melhor);

* motor "mais resistivo" = torque de partida maior, mas mais perdas

* velocidade de acomodação: velocidade de rotação do motor em regime permanente; ponto onde o torque do motor é o mesmo do torque da carga (intersecção das curvas)

* velocidade à vazio: um pouco abaixo da velocidade síncrona; será tal que a potencia convertida compense as perdas mecânicas; ou seja, o torque no eixo será zero; 

* fator de serviço = ?

* 

* torque máximo: máxima transferencia de potencia para o entreferro; Torque para a condição em que $s=|Zth|/R2$ (onde Zth é o equivalente de thevenin da primeira parte do circuito, incluindo o indutor X2)

* 

* **numero de fases do rotor**

  * costumam ter várias
  * não faz diferença na operação do motor

* **Numero de fases do estator**

  * 3 (trifásico)

  * Poderíamos construir um motor bifásico; Nesse caso um ciclo da rede equivale à uma rotação mecânica

    ![image-20210715233449539](Images - Motores e Conversores eletromecanicos/image-20210715233449539.png)

* **numero de polos do estator**

  * numero de "divisões" de cada bobina do estator

  * cada bobina percorre o estator em várias "parcelas"

  * até agora estudamos o caso com 2 polos

    ![image-20210728222159203](Images - Motores e Conversores eletromecanicos/image-20210728222159203.png)
    
    <img src="Images - Motores e Conversores eletromecanicos/image-20210731001923617.png" alt="image-20210731001923617" style="zoom:80%;" />
    
  * lembrando que a velocidade do motor será um pouco abaixo disso

  * Bobinas de um estator de 4 polos:

    ![image-20210730212228664](Images - Motores e Conversores eletromecanicos/image-20210730212228664.png) ![image-20210730212341815](Images - Motores e Conversores eletromecanicos/image-20210730212341815.png)


* **circuito equivalente**

  *  $\dot I_{ma} = \frac{\dot V}{jX_m}$

  * R_r representa as perdas no rotor

  * R_f = R_c = perdas no ferro/core do núcleo devido à efeitos magnéticos

  * X_m = perdas de magnetização

  * R_e = R_1 = perdas no cobre do estator

  * X_e = X_1 = perdas no estator devido à dispersão magnética

  * X_2 = perdas por dispersão do rotor referidas ao primário

  * R_2 = perdas no cobre do rotor referidas ao primário

  * entre o estator e rotor podemos consderar um transformador ideal; É usual fazer o circuito equivalente com a parte do rotor referida ao primário

  * a resistencia variável no final representa conversão eletromecânica de energia, oq ue realmente vira potência mecânica

  * P_conv = potência elétrica que é convertida para mecânica

  * P_rot = perdas mecânicas rotóricas

  * P_eixo = potência mecânica entregue no eixo

  * P entrada elétrica = 3* P fase
  
    
    
    ![image](Images - Motores e Conversores eletromecanicos/image-20210731001225004.png)

* **Construção do rotor**

  * o imã pode ser permanente ou eletroimã
  * as bobinas do rotor geralmente são construidas com alumínio sólido, chamado de squirrel cage, injetado diretamente no corpo eletromagnético do rotor, todas cirto-circuitadas

* **Acionamento**

  * se quisermos controlar a velocidade, a forma mais viável é usar um inversor com frequencia controlável
  * corrente de partida é 6~8 vezes maior do que a nominal
  * Softstarter: geralmente implementado com um gradador CA, começando com uma tensão bem baixa (portanto torque baixo, impedindo o motor de acelerar muito) até a tensão nominal (onda a velocidade do motor será dada pela frrequência da senoide)
  * partida estrela-triangulo: primeiro lliga em estrela, depois chaveia para triangulo; tem dois “trancos” de arranque, apesar de eles serem menores do que a partida unica; 

* **Controle de torque**

  * ?

    ![image-20210415213324726](Images - Motores e Conversores eletromecanicos/image-20210415213324726.png)

* **acoplamento motor-carga**

  * Relação de transmissão = velocidade da carga / velocidade do motor; essa velocidade é n, rpm?
  * métodos: correia, engrenagens
  * J: inérgia de carga
  * Jvista pelo motor = Jcarga*R^2
  
* **Limitesde operação**
  * geralmente aguentam trabalhar com tensão até 10% acima da nominal
  * mais do que isso, só por pouco tempo (tipo na partida)
  
* **Perdas rotacionais**


    * Atrito, giro do ventilador, e demais perdas mecânicas
    * Na partida será zero
    * Geralmente é proporcional à velocidade, mas é uma boa aproximação considerarmos constante

* **normas**

  * NBR IEC 60529 (grau de proteção em motores)
  * NBR IEC 60034-5 (máquinas elétricas girantes)
* categoria do motor
  * relacionado ao tipo de curva?
  * determina também o parâmetro alpha
  * categoria N -> alpha=0.67
* **Outras coisas práticas**
  * geralmente trabalhamos próximos da velocidade síncrona
  * [Leitura e Interpretação de Placa de Identificação de Motores Elétricos](https://www.youtube.com/watch?v=ZLV_MHFdmWk)
  * regime (S1, S2...): se o motor deve operar continuamente, intermitente, etc
  * dimensionamentos elétricos: tensão, frequêncnia, método de partida
  * dimensionamentos mecânicos: potencia da carga, velocidade da carga, tipo de acoplamento, tipo de carga, inércia da carga, regime de trabalho
  * especificações contrutivas: forma, sentido de rotação, proteção térmica
  * especificações do ambiente: temperatura, alti
  * tempo de acelaração = velocidade em rpm * (J_total/(Tmotor - Tcarga))
  * 
  * Torque de partida define a carga que conseguimos partir
  * Torque máximo determina que o motor consegue responder à uma mudança de carga até esse valor
  * 
  * 
  * 
  * Existe uma tendência de usar motores trifásicos mesmo em redes monofásicas, colocando um retificardor+inversor antes

### Motor de indução monofásico

* Não temos um campo magnético girante, apenas pulsante

* torque de partida zero, portanto precisa se excitado externamente

* torque em velocidade nominal é suficiente para manter o motor girando

* (+) simples

* aplicações: baixa potência, como geladeiras ou ventiladores

* pode girar em qualquer sentido apenas dependendo da inicialização?

  ![image-20210831231213344](Images - Motores e Conversores eletromecanicos/image-20210831231213344.png)

* O rotor também é giaola de estilo, etc, quase tudo é igual

* **Técnicas de partida**

  * a ideia é sempre iniciar com um campo diferente mais forte
  * <u>enrolamento auxiliar</u>
    * colocado em um angulo diferente
    * Ra/Xa > Rp/Xp (resistenica e reatancia do enrolamento aulixiar e principal)
    * depois da partida é desligado?
    * o sentido de rotação é determinado pela forma como o enrolamento auxiliar é colocado
  * <u>capacitor permanente</u>
    * também há um enrolamento auxiliar, mas botamos um capacitor para aumentar a defasagem no tempo entre o campo do enrolamento principal e auxiliar
  * <u>capacitor de partida</u>
    * similar ao capacitor permanente, mas abrimos o circuito auxiliar após algum tempo
    * (+) maior torque de partida
    * o interruptor é aberto após ~80% da velocidade nominal
    * a interrução geralmente é com uma chave mecânica (centrífugo) que é aberta após uma certa velocidade
    * 
    * motores de alta potencia geralmente possuem tanto um capacitor de partida quando um capacitor permanente
  * <u>split phase</u>
    * é o mesmo que enrolamento auxiliar?
    * uma resistencia é usada para gerar a defasagem
    * na prática, a resistência é obtida com a bobina auxiliar sendo feita com um fio mais fino e mais enrolamentos 
    * só é usado para baixa potência
    * (+) mais barato

* 

* 

* 

* Os motores do tipo universal podem funcionar tanto com corrente contínua quanto com corrente alternada; motor monofásico cujas bobinas do estator são ligadas eletricamente ao rotor por meio de duas escovas; é possível obter um conjugado pulsante, mas unidirecional, a partir de um motor CC ligado a uma fonte de potência CA.

* 

* .

## Ensaio práticos

* a reatancia do rotor é bem maior do que a do estador

* **MIT - Rotor travado**

  * Objetivo: medir perdas no cobre

  * Não aplicamos tensão nominal

  * Começa com tensão zero e sobe até ter a corrente nominal

  * V1cc = tensão de fase que colocamos

  * I1n = corrente de fase nominal

  * $|\dot Z_{eq}| = V1cc/I1n$

  * P_0 = P_1 + P_2?

  * $cos\phi_{cc} = \frac{P_0}{3V_{1m}I_0}$

  * $\dot Z_{eq} = |\dot Z_{eq}| \angle \phi_{cc} = r_{eq} + jx_{eq}$

  * $r_1$ = resistencia de cada fase do estator= pode ser medido diretamente com multimetro

  * x2'' = reatancia do rotor referida ao estator = $\frac{xeq}{1+alpha}$ (onde alpha é parâmetro do motor)

  * Pcc = perdas no cobre = w1 + w2

  * medir potencias

  * a medida deve ser rápida, pois o motor provavelmente esquentará pois o ventiladorzinho não estará rodando

    ![image-20210807190948612](Images - Motores e Conversores eletromecanicos/image-20210807190948612.png)

* **MIT - Rotor livre**

  * Objetivo: Determinar perdas no ferro e parâmetros Rf e Xm
  * determina também fator de potência e corrente de exitação $I_0$
  * Mesmas conexões do ensaio anterior
  * começamos com tensão nominal; Devemos curtocircuitar os watimetros e amperimetro para evitar a sua queima durante a partida
  * após a partida, conecte os instrumentos e meça: tensão de linha, corrente de inha e potência ativa total
  * R1 foi obtido do ensaio anterior
  * P_{rot} = ?
  * E_1 = ?
  * $P_f = P_0 - 3V_1I_o^2 - P_{rot}$
  * $R_f = \frac{3E_1^2}{P_f}$
  * $x_m = E_1/I_m$
  * 
  * fazemos medições para várias tensões, com o objetivo de medir as perdas rotacionais; paramos quando a corrente começar a aumentar o
  * os parâmetros são calculados apenas para a tensão nominal
  * a potencia possui uma relação quadrática com a tensão
  * extrapolando a curva para o ponto onde V=0, obtemos o valor aproximado das perdas rotacionais
  * 
  
* **MIT - Ensaio de carga**

  * Objetivos: obter rendimento, fator de potência, torque e corrente norminais
  
  * há um tacômetro no feito para medir a velocidade
  
  * há um freio de focault no eixo (conectado no eixo fica um disco metálico, bobinas alimentadas por cc induzem corrente no disco, freiam)
  
  * há um dinanômetro conectado... no que? multiplicamos pelo braço de alavanca para obtermos o torque no motor
  
  * geralmente começamos com uma tensão reduzida e vamos aumentando, para não precisar curtocircuitar os watimetros e amperimetros
  
  * variamos a carga (controlando a corrente CC do freio) até chegarmos à carga nominal
  
    ![image-20210828224726841](Images - Motores e Conversores eletromecanicos/image-20210828224726841.png)
    
  * <u>calculos</u>
  
    * repetidos para cada ponto de operação
    *  para cada ponto
    * $Peixo = velocidade*torque = w2*T$
    * $P1=W1+W2$ 
    * $n\% = Peixo/P1*100$
    * $cos_phi=P1/(sqrt(3)V_LI_L)$
    * $s\% = (nq-n2)/n1*100$
  
* **Outras anotaçẽos**

  * se a tensão de linha for 380V (por exemplo) e o motor estiver ligado em Y, cada fase terá 220V; Se estiver em delta, cada fase terá 380V
  * $w$ é em radianos por segundo, $n$ em rpm
  * torque é medido em N/m
  * torque de partida define a carga que eu consigo partir
  * torque máximo define o quanto que a carga pode chegar depois de estar rodando
  * Medição de potencia em sistema trifásicos - Método dos dois watímetros
    * funciona bem se as fases forem equilibradas?
  * redes trifásicas
    * 380V = tensão eficazes de linha de uma rede trifásica usual 



Motores de combustão
=============================

-   Motores com carburador: economizam gasolina se ficar em ponto motor.
    Motores com injeção eletrônica (a maioria dos modernos): economiza
    gasolina se ficar engranado na descida (pois a injeção irá cortar
    automaticamente o combustivel, o que não acontence no ponto morto)

# Jet engines

Turboprop: mix jet and blade. Very efficient

turbofan: jet with a fan in front to increase intake?



Example for A320 and B707 (a turbofan):

* Air intake: 380m3/s (one Olympic pool every 10s)
* Core stream (or primary): Hot gas
* Bypass stream (or secondary): cold gas
* Bypass ratio= core/bypass
* lower bypass ratio -> Higher speed
* 80-90% of the trust is generated by the bypass stream



![image-20250201154000458](./Images - Motores e Conversores eletromecanicos/image-20250201154000458.png)

> 1 core 2 bypass



* 

* Core:

  * Low pressure blade to Compress -> Combust -> gather (high pressure motors)
  * Different blades for different pressure stages, rotating at different at apeed
  * Fixed guide vents between stages
  * compress
    * has 3 low pressure stages and 9 high pressure stages
  * combust
    * Combustion goes to 1459C and it's self sustained
  * Gather
    * Post combust blades are for gathering the energy to be used jn the compressor and intake
    * Have 1 high pressure (15k rpm) stages and 4 low pressure stages (5k rpm)

  

  

Reverser uses only bypass stream:

![image-20250201153753798](./Images - Motores e Conversores eletromecanicos/image-20250201153753798.png)





Outras coisas mecânicas
=======================

-   Degree of freedom (DOF)

    -   We count one degree of freedom for each independent direction in
        which a robot can move

    -   For nonrigid bodies, there are additional degrees of freedom
        within the robot itself

-   kinematic state (or *pose*): space position (x, y, z) + angular
    orientation(yaw, roll, and pitch)

-   dynamic state: rate of change of each kinematic dimension

# Geradores

* puxar mais corrente de um gerador mecânico faz ele diminuir sua velocidade de rotação'
* * 