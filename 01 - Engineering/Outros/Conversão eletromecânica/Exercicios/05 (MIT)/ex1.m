f_rede = 60
rpm_r = 1188
w_rede = f_rede*2*pi %rad/s
wr = (rpm_r/60)*2*pi %rad/s
p = floor(2*w_rede/wr) %considerando que o motor está próximo da velocidade síncrona

ws = 2/p * w_rede
rpm_s = ws*60/(2*pi)
s = (ws-wr)/ws

printf("a) Quantos polos possui este motor? %d\n", p)
printf("b) Qual a velocidade síncrona desta máquina? %.2f rpm\n", rpm_s)
printf("c) Velocidade do campo gerado pelo enrolamento do estator para um observador situado no rotor: %.2f rpm\n", rpm_s - rpm_r) 
printf("Qual a velocidade do campo gerado pelo enrolamento do estator para um observador situado no estator? %.2f rpm\n", 60*f_rede)
printf("f) Qual a velocidade do campo gerado pelo enrolamento do rotor para um observador situado no rotor? %.2f rpm\n", 0)
printf("g) Qual a velocidade do campo gerado pelo enrolamento do rotor para um observador situado no estator? %.2f rpm\n", rpm_r)
printf("k) Qual o escorregamento nesta condição? %.2f\n", s)
printf("l) Qual a frequência das correntes rotóricas? %.2f rad/s \n", s*ws) 


