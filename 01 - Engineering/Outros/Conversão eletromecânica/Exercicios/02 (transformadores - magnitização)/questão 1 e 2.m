% Questão 1)
Bmax = 1.5
A = 0.03^2
lm = 0.48
ur = 2000
u = ur*4*pi*10^-7
w = 2*pi*60
V1 = 220
V2 = 127

% a) o número de espiras do primário e do secundário para garantir a relação de transformação desejada sem saturar o núcleo.
phiMax = Bmax*A

N1max = sqrt(2)*V1/(phiMax*w)
N1 = N1max
N2 = N1*V2/V1

%b) a reatância e a indutância de magnetização deste transformador.
reluctance = lm/(u*A)
Lm = N1^2/reluctance
Xm = j*w*Lm

%c) a corrente de magnetização deste transformador.
Im = sqrt(2)*V1/Xm

%d) a potência reativa para operação a vazio deste transformador.
S = V1*conj(Im)
Q = imag(S)


%%%%%%
% Questão 2) Qual será a corrente de magnetização se este transformador for alimentado pelo lado de baixa tensão em 127V?
Lm = N2^2/reluctance
Xm = j*w*Lm
Im = sqrt(2)*V2/Xm %que é maior do que quando alimentado pelo primário


