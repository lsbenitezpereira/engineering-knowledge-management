% Os resultados dos ensaios num trafo monofásico de 100KVA, 14,6KV/220V foram:
% curto-circuito: 200V   7A    600W

V1cc = 200;
I1n = 7;
Pcc = 600;
a = 14600/220;


cos_phi = Pcc/(V1cc*I1n);
Z_eq = V1cc/I1n;
Z_eq_fasor = Z_eq*e^(j*acos(cos_phi));

req = real(Z_eq_fasor);
xeq = imag(Z_eq_fasor);

r1 = req/2
x1 = xeq/2

r2 = r1/a^2
x2 = x1/a^2