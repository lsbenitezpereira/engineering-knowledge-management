
clear all
clc


%ensaio em curto-circuito


Pcc=(200)/3
V1cc=65
I1n=2.6/sqrt(3)
r1=15.5

Zeq=V1cc/I1n
Req=Pcc/I1n^2
Xeq=sqrt(Zeq^2-Req^2)
r2=Req-r1
x2=Xeq/(1+0.67)
x1=0.67*x2

%ensaio em vazio
Po=180
V1n=380;
Io=1.65/sqrt(3);

Pfe=Po-3*r1*Io^2-47.6

cos_fio=Po/(3*V1n*Io)
fio=acos(cos_fio)
angulo=fio*180/pi


E1=V1n-(r1+j*x1)*complex(Io*cos(fio),-Io*sin(fio))

mE1=abs(E1)
fi_E1=angle(E1)*180/pi


rp=3*mE1^2/Pfe

Ip=mE1/rp

Im=sqrt(Io^2-Ip^2)

xm=mE1/Im


%%%%%%%%%%%%%%%%%%%%%%%
s=[0.001:.001:.999];
f=60;
p=4;

v1=380+0j;
n1=120*f/p;
Prot_vazio=0;
x2=x2*1j;    
x1=x1*1j;   
xm=xm*1j;   



for i=1:999
 
z1=r1+x1;
z2(i)=r2/s(i)+x2;
zp=(rp*xm)/(rp+xm);
zeq(i)=z1+(z2(i)*zp)/(z2(i)+zp);
i1(i)=v1/zeq(i);
n2(i)=n1*(1-s(i));
i2(i)=i1(i)*(zp/(z2(i)+zp));
Pmi=3*r2*(1-s(i))/s(i)*abs(i2(i))^2;
w2=(4*pi*f/p)*(1-s(i));
Prot=Prot_vazio*n2(i)/n1;
T(i)=(Pmi-Prot)/w2;
n(i)=(Pmi-Prot)*100/abs(3*v1*real(i1(i)));
Peixo(i)=(Pmi-Prot)/736;
end

n2_carga=[  1744 1757 1773 1789 1796 ]
I1_carga=[  2.5 2.2 1.95 1.8 1.75]
n_carga=[ 91.99 89.36 78.91 63.69 0]
n2_placa=[  0 1715]
I1_placa=[  6.8*2.6 2.6]

g1=figure;
g1=plot(n2,n,'b-',n2_carga,n_carga,'b*');
hold on
%plot(n2,abs(i1),'m-',n2,T,'r-',n2,Peixo,'b-',n2,n,'y-');

g2=figure;
g2=plot(n2,sqrt(3)*abs(i1),'m-',n2,T,'r-',n2_carga,I1_carga,'k*',n2_placa,I1_placa,'b*');
title('grafico I1(n2),T(n2),Peixo_CV(n2)');
ylabel('corrente(A),T(Nm),Peixo[CV]');
xlabel('velocidade(rpm)');