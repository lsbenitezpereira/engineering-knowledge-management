% Motor em estrela
V = 380 %tensão de fase no motor;se estiver e estrela, divida o valor medido por sqrt(3)
Io = 1.65/sqrt(3)  %nominal; se estiver em estrela não precisa dividir
W1 = 380
W2 = -200
Po = W1 + W2% Potência medida total;
Prot = 47.6 % obtido graficamente, ou as vezes ja conhecido

R1 = 15.5 %medido diretamente
X1 = 12.685 %do ensaio de rotor travado

% Calculos
cos_phi = Po/(3*V*Io)
phi  = acos(cos_phi)
Pf = Po - 3*R1*Io^2 - Prot

E1 = V - (R1+j*X1)*complex(Io*cos(phi),-Io*sin(phi))
Rp = 3*abs(E1)^2/Pf
Ip = abs(E1)/Rp
Im = sqrt(Io^2 - Ip^2)
Xm = abs(E1)/Im









% Gráfico; Permite obter as perdas rotacionais
%VLs = [380, 340, 300, 260, 220, 180, 140]
%Pos = [180, 120, 120, 100, 90, 70, 70]
%plot(VLs.^2, Pos, 'x')
