% Motor em delta
V1cc =65 %tensão de fase no motor;se estiver e estrela, divida o valor medido por sqrt(3)
I1m=2.6/sqrt(3) %nominal; se estiver em estrela não precisa dividir
%Pcc = W1 + W2
Pcc= 200 %W
r1 = 15.5 %medido diretamente
alpha = 0.67 %parâmetro do motor

cos_phi = Pcc/(3*V1cc*I1m)
Zeq = (V1cc/I1m)*e^(j*acos(cos_phi))

req = real(Zeq)
xeq = imag(Zeq)


r2_linha = req - r1 
x2_duaslinhas = xeq/(1+alpha)
x1_linha = alpha*x2_duaslinhas