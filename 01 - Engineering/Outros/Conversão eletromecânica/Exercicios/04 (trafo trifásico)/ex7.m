FP_load = 0.7
S_load =  0.85*20000*e^(j*acos(FP_load))
V1 = 690
V2 = 480

%%%
a = V1/V2
I2 = conj(S_load/V2)
I1 = I2/a

printf("potência aparente que está sendo transmitida para a carga através do núcleo magnético: %s\n", num2str(V2*(I2-I1)))
printf("relacao Sind/S2: %f\n", 1 - 1/a)