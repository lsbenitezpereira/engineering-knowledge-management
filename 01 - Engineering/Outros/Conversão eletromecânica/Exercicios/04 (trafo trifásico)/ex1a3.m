clear
V1 = 15000
V2 = 120
FP_load = 1
S_load =  100000*e^(j*acos(FP_load))
connection_type = "star-delta"
%%%

if (connection_type=="star-star")
  conversion_factor_amplitude = 1
  conversion_factor_angle = 0
elseif (connection_type=="star-delta")
  conversion_factor_amplitude = 1/sqrt(3)
  conversion_factor_angle = -30
elseif (connection_type=="delta-delta")
  conversion_factor_amplitude = 1
  conversion_factor_angle = 0
elseif (connection_type=="delta-star")
  conversion_factor_amplitude = sqrt(3)
  conversion_factor_angle = +30
end


a = conversion_factor_amplitude*V1/V2
printf("a) Relação de transformação: %f\n", a)
printf("b) Corrente no secundário: %s A\n", num2str(conj(S_load/V2)))
printf("c) VA primário = %f com angulo %dº\n",  V1/sqrt(3), 0)
printf("c) VB primário = %f com angulo %dº\n",  V1/sqrt(3), 120)
printf("c) VC primário = %f com angulo %dº\n",  V1/sqrt(3), 240)
printf("c) Va secundário = %f com angulo %dº\n", V2/sqrt(3), 0+conversion_factor_angle)
printf("c) Vb secundário = %f com angulo %dº\n", V2/sqrt(3), 120+conversion_factor_angle)
printf("c) Vc secundário = %f com angulo %dº\n", V2/sqrt(3), 240+conversion_factor_angle)
