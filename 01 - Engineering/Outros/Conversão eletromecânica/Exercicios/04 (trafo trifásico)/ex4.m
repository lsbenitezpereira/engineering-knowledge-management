V1 = 68000
V2 = 15000
FP_load = 0.97
S_load = 10e6*e^(j*acos(FP_load))


a = V1/V2
printf("a) a relação de transformação dos 2 transformadores: %.3f", a)
printf("b) o fasor corrente na carga (IA): %d", num2str(conj(S_load/V2)))
%printf("c) o fasor corrente na fonte (Ia);")
%printf("d) as correntes do primário e do secundário de cada transformador;")
%printf("e) as potências ativa, reativa e aparente da carga e da fonte;")
%printf("f) as potências ativa, reativa e processadas por cada transformador:")
