N1 = 360
N2 = 750
Bmax = 1.8 %T
ur = 2400
x = 5.6e-2 %m
lm = 80e-2
f = 60

%a
A = x*x
w = 2*pi*f
phiMax = Bmax*A
V1 = N1*phiMax*w/sqrt(2)

%b
a = N1/N2

%c
u = ur*4*pi*10^-7 %H/m
reluctance = lm/(u*A)
Lm = N1^2/reluctance
Xm = j*w*Lm


printf("a) O valor eficaz máximo da tensão que pode ser aplicada ao primário sem saturar o núcleo: %.2f\n", V1)
printf("b) A relação de transformação: %.2f\n", a)
printf("c) A reatância de magnetização referiada ao primário: %s\n", num2str(Xm))