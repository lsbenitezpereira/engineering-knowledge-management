V = 220
ws = 1800*rpm2rads %b) a velocidade do campo girante;


% Parâmetro do circuito equivalente
R1=0.41
X1=0.76
R2=0.55
X2=0.92
Xm=46
Rc=200.

% Condição de máximo torque
Zc = (j*Xm*Rc)/(j*Xm + Rc)
Z1 = R1 + j*X1
Zth = (Zc*Z1)/(Zc+Z1) + j*X2
s = abs(R2/Zth)
wr = -s*ws + ws
Rs = R2*(1-s)/s

% Resolução do circuito
Z2 = j*X2 + R2 + Rs
Zc = (j*Xm*Rc)/(j*Xm + Rc)
Z1 = R1 + j*X1
Zeq = Z1 + (Z2*Zc)/(Z2 + Zc)
Iin = V/Zeq

S1in = V*Iin'
Sin = 3*S1in
Pin = real(Sin) %a potência elétrica de entrada do motor;

E0 = V - Z1*Iin
I2 = E0/Z2
P1conv = Rs*abs(I2)^2
Pconv = 3*P1conv %potência elétrica convertida em mecânica
Tind = Pconv/wr %o torque mecânico induzido; Nm


% Resolução do sistema mecânico
losses_axis  = 234 % perdas mecânicas, consideraremos uma constante
Paxis = Pconv - losses_axis
Taxis = Paxis/wr
n = Paxis/(Pin) % eficiência total do motor
FP = abs(Pin)/abs(Sin)