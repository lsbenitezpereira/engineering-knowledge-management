V = 220;
Pmecanic = 7500;
ws = 1200*rpm2rads; %b) a velocidade do campo girante;
for wr = 0:ws
  s = (ws-wr)/ws; %a) o escorregamento;

  % Parâmetro do circuito equivalente
   R1=0.294;
   R2=0.144;
   X1=0.503;
   X2=0.209;
   Xm=13.25;
   Rc=600;
  Rs = R2*(1-s)/s;

  Z2 % Resolução do circuito
  Z2 = j*X2 + R2 + Rs;
  Zc = (j*Xm*Rc)/(j*Xm + Rc);
  Z1 = R1 + j*X1;
  Zeq = Z1 + (Z2*Zc)/(Z2 + Zc);
  Iin = V/Zeq;

  S1in = V*Iin';
  Sin = 3*S1in;
  Pin = real(Sin); %a potência elétrica de entrada do motor;

  E0 = V - Z1*Iin;
  I2 = E0/Z2;
  P1conv = Rs*abs(I2)^2;
  Pconv = 3*P1conv; %potência elétrica convertida em mecânica
  Pes=3*real(Z2*abs(I2)^2); % Potência no entreferro
  Tind = Pes/ws; % Torque induzido; Nm; Pode ser calculado também por Tind = Pconv/wr

  % Resolução do sistema mecânico
  losses_axis  = Pconv - Pmecanic; % perdas mecânicas, consideraremos uma constante
  Paxis = Pconv - losses_axis;
  Taxis = Paxis/wr;
  n = Paxis/(Pin); % eficiência total do motor
  FP = abs(Pin)/abs(Sin);
  
  y(wr+1) = Tind;
endfor  
plot((0:ws)*rads2rpm, y)
