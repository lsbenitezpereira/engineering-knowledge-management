% Circuito 1
Zload = 4 + j*3
Zline = 0.18 + j*0.24
Vs = 480
Is = Vs/(Zload + Zline)
Sline = Zline*Is*conj(Is) % 1482.7 + 1977.0i

% Circuito 2
a1 = 1/10
a2 = 10
Is = Vs/(a1^2*(Zline + a2^2*Zload))
Iline = Is*a1
Sline = Zline*Iline*conj(Iline) % 16.570 + 22.093i

% Podemos observar a diminuição nas perdas ao utilizar o transformador