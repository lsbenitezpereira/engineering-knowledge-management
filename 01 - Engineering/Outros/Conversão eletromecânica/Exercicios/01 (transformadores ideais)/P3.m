% Bloco superior
Ce = 1/(j*150*10^-6)
Re = 7
Ze = Ce*Re/(Ce+Re)
Ve = 220*e^(j*0)
Ie = Ve/Ze
a3 = 36.36
Id = Ie/a3
Vd = Ve*a3

% Bloco inferior
Vf = Vd
a4 = 12
a5 = 5.25
Vh = Vf*a4*a5
Sh = 100000*e^(j*acos(0.7))
Ih = conj(Sh/Vh)
If = Ih/(a4*a5)

% Bloco central
Ic = Id + If
Vc = Vf
Va = 5000*e^(j*0)
a1 = 0.2
a2 = Va/(Vc*a1)