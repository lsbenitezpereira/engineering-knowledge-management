Sl = 100000*e^(j*acos(0.7))
Pl = real(Sl)
Ql = imag(Sl)
Vl = 220*e^(j*0)
Il = conj(Sl/Vl)
Vs = 8000*e^(j*0)

% Relações de transformação
Ib = 0.5
a2 = abs(Il)/abs(Ib)
a1 = abs(Vs)/(abs(Vl)*a2)

% Sendo o transformador ideal, as potências de entrada são iguais às da saída