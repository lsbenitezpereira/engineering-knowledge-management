import java.util.List;
import java.util.ArrayList;

public class StudentDaoImplMemory extends StudentDao{
    protected ArrayList<Student> students = new ArrayList<>()
;
    @Override
    List<Student> getAllStudents() {
        return this.students;
    }

    @Override
    void updateStudent(Student s) {
        // not needed: when the user changes the object, that's it
    }

    @Override
    void deleteStudent(Student s) {
        students.remove(s);
    }

    @Override
    void addStudent(Student s) {
        students.add(s);
    }

}
