package buttons;

public class WindowsTextField implements TextField {
    protected String windowsText;
    @Override
    public String getText(){
        return this.windowsText;
    }

    @Override
    public void setText(String text){
        this.windowsText = text;
    }

}
