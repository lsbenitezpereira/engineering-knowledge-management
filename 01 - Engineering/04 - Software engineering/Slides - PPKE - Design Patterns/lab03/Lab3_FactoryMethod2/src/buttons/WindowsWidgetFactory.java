package buttons;

public class WindowsWidgetFactory extends WidgetFactory{
    public TextField createTextField(){
        return new WindowsTextField();
    }

    public Button createButton(){
        return new WindowsButton();
    }

    public Dialog createDialog(Button b, TextField t){
        return new WindowsDialog(b, t);
    }

}