package buttons;

public interface TextField {
    public String getText();
    public void setText(String text);
}
