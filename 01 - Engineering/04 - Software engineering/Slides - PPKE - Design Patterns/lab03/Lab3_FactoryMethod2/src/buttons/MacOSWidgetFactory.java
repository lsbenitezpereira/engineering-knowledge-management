package buttons;

public class MacOSWidgetFactory extends WidgetFactory{
    public TextField createTextField(){
        return new MacOSTextField();
    }

    public Button createButton(){
        return new MacOSButton();
    }

    public Dialog createDialog(Button b, TextField t){
        return new MacOSDialog(b, t);
    }

}