package buttons;

public class MacOSTextField implements TextField {
    protected String macText;
    @Override
    public String getText(){
        return this.macText;
    }

    @Override
    public void setText(String text){
        this.macText = text;
    }

}
