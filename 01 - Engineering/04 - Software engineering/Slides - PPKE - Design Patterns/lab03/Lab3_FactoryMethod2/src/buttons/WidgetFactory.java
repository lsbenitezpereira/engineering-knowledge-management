package buttons;

public abstract class WidgetFactory{
    public abstract TextField createTextField();
    public abstract Button createButton();
    public abstract Dialog createDialog(Button b, TextField t);
}