package buttons;

public abstract class Dialog {
    protected Button button;
    protected TextField textField;

    public Dialog(Button b, TextField t) {
        this.button = b;
        this.textField = t;
    }

    //public abstract Button getButton();

    public abstract void renderWindow();
}