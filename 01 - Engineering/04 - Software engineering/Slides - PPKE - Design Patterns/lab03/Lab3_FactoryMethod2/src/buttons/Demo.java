package buttons;

public class Demo {
    public static void main(String[] args) {
        String osName = System.getProperty("os.name").toLowerCase();
        System.out.println("Running on " + osName);

        WidgetFactory factory;
        if (osName.contains("mac")) {
            factory = new MacOSWidgetFactory();
        } else {
            factory = new WindowsWidgetFactory();
        }
        Button b = factory.createButton();
        TextField t = factory.createTextField();
        Dialog d = factory.createDialog(b, t);

        d.renderWindow();
    }
}