package Lab3_FactoryMethod.application;
import Lab3_FactoryMethod.framework.*;

public class TextDocument extends Document {
    protected String text;

    public TextDocument(String text){
        this.text = text;
    }

    @Override
    public void open(String name) {
        System.out.println("Concrete document opening: " + name);
        this.setName(name);
    }

    @Override
    public void display(){
        System.out.println(text);
    }
}
