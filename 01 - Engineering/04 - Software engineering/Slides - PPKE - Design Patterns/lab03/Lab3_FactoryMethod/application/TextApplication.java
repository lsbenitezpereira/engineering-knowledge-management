package Lab3_FactoryMethod.application;
import Lab3_FactoryMethod.framework.*;


public class TextApplication extends Application {
    @Override
    public Document createDocument(){
        return new TextDocument("");
    }

    public static void main(String[] args){
        Application myApp = new TextApplication();
        myApp.newDocument("doc 1");
        myApp.newDocument("doc 2");
        myApp.openDocument("doc 3");
    }
}
