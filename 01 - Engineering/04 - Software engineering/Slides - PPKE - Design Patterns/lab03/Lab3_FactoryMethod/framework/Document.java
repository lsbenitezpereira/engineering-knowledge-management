package Lab3_FactoryMethod.framework;

public abstract class Document {
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void open(String name);
    public abstract void display();
}
