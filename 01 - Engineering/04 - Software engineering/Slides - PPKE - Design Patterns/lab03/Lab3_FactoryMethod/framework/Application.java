package Lab3_FactoryMethod.framework;
import java.util.ArrayList;


public abstract class Application {
    private ArrayList<Document> docs = new ArrayList<>();
    
    public abstract Document createDocument();
    
    public void newDocument(String name) {
        System.out.println("FRamework create document:" + name);
        Document d = this.createDocument();
        d.setName(name);
        docs.add(d);
    }
    
    public void reportDocs() {
        for (Document d : docs) {
            System.out.println(d.getName());
        }
    }

    public void openDocument(String name) {
        System.out.println("Framework opening document: " + name);
        Document d = this.createDocument();
        d.open(name);
        docs.add(d);
    }
}