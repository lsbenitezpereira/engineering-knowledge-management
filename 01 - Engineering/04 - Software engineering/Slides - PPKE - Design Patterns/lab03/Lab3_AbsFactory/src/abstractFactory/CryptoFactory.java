package abstractFactory;

public abstract class CryptoFactory {
    public abstract Encoder getEncoder();
    public abstract Decoder getDecoder();
}
