package abstractFactory;

public interface Decoder {
    String decode(String encodedMessage);
}