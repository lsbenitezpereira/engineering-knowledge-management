package abstractFactory;

public class Client {
    private Encoder encoder;
    private Decoder decoder;

    public Client() {
        CryptoFactory factory = new SimpleCryptoFactory();
        this.encoder = factory.getEncoder();
        this.decoder = factory.getDecoder();
    }

    public void simulateEncodingAndDecoding(String message) {
        String encoded = encoder.encode(message);
        System.out.println("Encoded: " + encoded);
        String decoded = decoder.decode(encoded);
        System.out.println("Decoded: " + decoded);
    }

    public static void main(String[] args) {
        Client simpleClient = new Client();
        simpleClient.simulateEncodingAndDecoding("HelloWorld");
    }
}