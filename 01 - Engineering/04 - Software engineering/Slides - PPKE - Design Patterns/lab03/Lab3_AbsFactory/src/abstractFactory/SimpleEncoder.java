package abstractFactory;

public class SimpleEncoder implements Encoder {
    @Override
    public String encode(String message) {
        return message + "-simpleEncoded";
    }
}