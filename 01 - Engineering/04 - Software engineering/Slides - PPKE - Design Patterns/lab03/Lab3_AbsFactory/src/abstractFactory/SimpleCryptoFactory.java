package abstractFactory;

public class SimpleCryptoFactory extends CryptoFactory {
    public Encoder getEncoder(){
        return new SimpleEncoder();
    }

    public Decoder getDecoder(){
        return new SimpleDecoder();
    }
}
