package abstractFactory;

public class SimpleDecoder implements Decoder {
    @Override
    public String decode(String encodedMessage) {
        return encodedMessage.replace("-simpleEncoded", "");
    }
}    