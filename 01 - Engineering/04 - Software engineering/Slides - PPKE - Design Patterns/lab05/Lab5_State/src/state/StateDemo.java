package state;

public class StateDemo {

	public static void main(String[] args) {
		
		//Create new server listening on port 8800
		TCPConnection server = new TCPConnection(8800);
		
		//Erroneously try to send data before connection is established
		server.send("Hello");

		//Erroneously try to close connection before connection is established
		server.close();

		//Establish connection from source IP
		server.connect("192.168.8.1");

		//Send data
		server.send("Hello");

		//Try to establish connection again
		server.connect("10.30.2.110");

		//Close connection
		server.close();

		//Try to send data after connection is closed
		server.send("Hello again");
	}

}
