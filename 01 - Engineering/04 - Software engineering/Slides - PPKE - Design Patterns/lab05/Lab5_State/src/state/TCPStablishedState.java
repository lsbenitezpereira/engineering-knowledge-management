package state;

public class TCPStablishedState implements TCPState {
    @Override
    public void send(TCPConnection conn, String message){
        System.out.println("Sending mesage to " + conn.getServer() + ":" + conn.getPort() + ", message: " + message);
    }

    @Override
    public void close(TCPConnection conn){
        System.out.println("Closing closed state");
        conn.setCurrentState(new TCPClosedState());
    }
    
    @Override
    public void connect(TCPConnection conn, String server){
        System.out.println("Nothing to be done");
    }
}
