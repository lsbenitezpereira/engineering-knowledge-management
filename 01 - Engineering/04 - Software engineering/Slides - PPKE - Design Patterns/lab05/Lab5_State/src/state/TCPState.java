package state;

public interface TCPState {
    void send(TCPConnection conn, String message);

    void close(TCPConnection conn);

    void connect(TCPConnection conn, String server);

}
