package state;

public class TCPClosedState implements TCPState{
    @Override
    public void send(TCPConnection conn, String message){
        System.out.println("Error, cannot send message in state closed");
    }
    @Override
    public void close(TCPConnection conn){
        System.out.println("Closing closed state, nothing to be done");
    }
    
    @Override
    public void connect(TCPConnection conn, String server){
        System.out.println("Connecting...");
        conn.setCurrentState(new TCPStablishedState());
    }
}
