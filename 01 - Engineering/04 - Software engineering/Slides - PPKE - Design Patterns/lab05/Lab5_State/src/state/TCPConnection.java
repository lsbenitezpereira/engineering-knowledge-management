package state;

public class TCPConnection {
    TCPState currentState;
    String server;
    int port;

    public TCPConnection(int port) {
        this.port = port;
        this.currentState = new TCPClosedState();
    }

    public String getServer() {
        return server;
    }

    public int getPort() {
        return port;
    }

    public TCPState getCurrentState(){
        return currentState;
    }

    public void setCurrentState(TCPState currentState) {
        this.currentState = currentState;
    }

    public void send(String message) {
        currentState.send(this, message);
    }

    public void close() {
        currentState.close(this);
    }

    public void connect(String server) {
        this.server = server;
        currentState.connect(this, server);
    }

}
