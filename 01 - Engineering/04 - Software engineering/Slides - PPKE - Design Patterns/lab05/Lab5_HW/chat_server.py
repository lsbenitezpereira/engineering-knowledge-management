from typing import List
import logging
import client


class ChatServer:
    _clients: List['client.Client']

    def __init__(self) -> None:
        self._clients = []

    def connect(self, client: 'client.Client') -> None:
        self._clients.append(client)

    def disconnect(self, client: 'client.Client') -> None:
        self._clients.remove(client)

    def send(self, from_name: str, to_name: str, message: str) -> bool:
        for client in self._clients:
            if client.get_name() == to_name:
                client.receive(from_name, message)
                return True
        logging.warning(f"User {to_name} not found")
        return False

    def broadcast(self, from_name: str, message: str) -> None:
        for client in self._clients:
            if client.get_name() != from_name:
                client.receive(from_name, message)
