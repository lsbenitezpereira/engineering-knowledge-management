from typing import List
import logging
from chat_server import ChatServer
from client import Client
from states import StateOnline, StateAway, StateBusy

logging.basicConfig(level=logging.INFO)

server = ChatServer()
alice = Client(server, "Alice")
bob = Client(server, "Bob")
charlie = Client(server, "Charlie")
charlie.set_state(StateAway())

alice.broadcast("Hello everyone!")  # charlie is away
bob.broadcast("Hello Alice!")
charlie.send("Hello Alice!", "Alice")

print("|A|"+"-"*50)
alice.send("lol, where is bob?!", "Bob")
bob.send("Doing some design pattern assignment", "Alice")
alice.send("wow, I forgot about it, I need to go to", "Bob")
alice.set_state(StateBusy())
bob.send("okay nerd, bye", "Alice")  # Alice is busy

print("|B|"+"-"*50)
charlie.set_state(StateOnline())  # charlie see 2 messages missed
charlie.broadcast("Hello my chatters!")  # alice is busy
bob.broadcast("hey dude")  # alice is busy
charlie.send("fnished?", "Bob")
charlie.send("chat-gpt all the way, but yes", "Charlie")

print("|C|"+"-"*50)
alice.send("I need Pálinka", "Charlie")  # alice goes online; sees "okay nerd, bye", "Hello my chatters!", "hey dude"
charlie.send("already wanting to quit this master?", "Alice")
alice.send("cheeezzzz that assigment took longer than expected", "Charlie")
charlie.send(":smile:", "Alice")
charlie.send("let's party?", "Alice")
alice.send("I'm in!", "Charlie")
server.disconnect(alice)
server.disconnect(charlie)

print("|D|"+"-"*50)
bob.send("How are you doing?", "Alice")  # alice is disconnected
bob.send("How are you doing?", "Charlie")  # charlie is disconnected
bob.broadcast("How are you doing?")  # no one receives it
bob.broadcast("hello?")  # silence

print("|F|"+"-"*50)
ghost = Client(server, "Ghost")
ghost.set_state(StateAway())
ghost.send("hooooo", "Bob")
bob.send("WHAT THE...", "Ghost")  # ghost is away (displayed and counted)
ghost.set_state(StateBusy())
bob.send("am I alone?", "Ghost")  # ghost is busy (counted)
ghost.set_state(StateAway())  # ghost displays 1 message, count is 2
ghost.send("BU!", "Bob")
server.disconnect(bob)
ghost.set_state(StateOnline())  # ghost receive the count of 2 messages missed
