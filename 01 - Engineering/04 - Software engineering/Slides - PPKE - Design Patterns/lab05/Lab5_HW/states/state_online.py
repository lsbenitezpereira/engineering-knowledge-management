import logging
import client
from states.state import State


class StateOnline(State):
    def receive(self, client: 'client.Client', from_name: str, message: str) -> None:
        logging.info(f"{client.get_name()} received a message from {from_name} saying: {message}")

    def send(self, client: 'client.Client', message: str, to_name: str) -> bool:
        return client.get_server().send(client.get_name(), to_name, message)

    def broadcast(self, client: 'client.Client', message: str) -> None:
        client.get_server().broadcast(client.get_name(), message)
