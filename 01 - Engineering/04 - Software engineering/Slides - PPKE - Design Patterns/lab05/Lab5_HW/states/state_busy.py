import logging
from typing import List, Tuple
import client
from states.state import State
from states.state_online import StateOnline


class StateBusy(State):
    _pending_messages: List[Tuple['client.Client', str, str]] = []

    def receive(self, client: 'client.Client', from_name: str, message: str) -> None:
        # messages received are not displayed
        # only displayed once the user returns to online or goes to away
        client.add_pending_message(from_name, message)

    def send(self, client: 'client.Client', message: str, to_name: str) -> bool:
        # Sending a message automatically puts you in online state
        client.set_state(StateOnline())
        return client.get_server().send(client.get_name(), to_name, message)

    def broadcast(self, client: 'client.Client', message: str) -> None:
        client.set_state(StateOnline())
        client.get_server().broadcast(client.get_name(), message)
