import logging
import client
from states.state import State


class StateAway(State):
    def receive(self, client: 'client.Client', from_name: str, message: str) -> None:
        # all messages received are displayed immediately, and also counted
        # As the user returns to Online, they are told that they missed X number of messages
        logging.info(f"{client.get_name()} received a message from {from_name} saying: {message}")
        client.add_pending_message(None, None)

    def send(self, client: 'client.Client', message: str, to_name: str) -> bool:
        # you are allowed to send messages to other users while in away state
        return client.get_server().send(client.get_name(), to_name, message)

    def broadcast(self, client: 'client.Client', message: str) -> None:
        client.get_server().broadcast(client.get_name(), message)
