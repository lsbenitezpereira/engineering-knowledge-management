from abc import ABC, abstractmethod
import client


class State(ABC):
    @abstractmethod
    def receive(self, client: 'client.Client', from_name: str, message: str) -> None:
        pass

    @abstractmethod
    def send(self, client: 'client.Client', message: str, to_name: str) -> bool:
        pass

    @abstractmethod
    def broadcast(self, client: 'client.Client', message: str) -> None:
        pass
