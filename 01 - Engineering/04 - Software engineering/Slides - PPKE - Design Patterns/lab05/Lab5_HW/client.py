from typing import List, Tuple, Optional
import logging
import chat_server
from states import State, StateOnline, StateAway, StateBusy


class Client():
    _server: 'chat_server.ChatServer'
    _name: str
    _current_state: State
    _pending_messages: List[Tuple[Optional[str], Optional[str]]] = []

    def __init__(self, server: 'chat_server.ChatServer', name: str) -> None:
        self._server = server
        self._name = name
        self._current_state = StateOnline()
        self._pending_messages = []
        self._server.connect(self)

    def get_name(self) -> str:
        return self._name

    def get_server(self) -> 'chat_server.ChatServer':
        return self._server

    def set_state(self, new_state: State) -> None:
        # Since the states are stateless, any state-transition logic should be implemented here
        # Since this requires the Client to know about the states, adding new states would require changes here
        if isinstance(new_state, StateOnline):
            total_already_displayed: int = 0
            for from_name, message in self._pending_messages:
                if from_name is not None and message is not None:
                    new_state.receive(self, from_name, message)
                else:
                    total_already_displayed += 1
            if total_already_displayed > 0:
                logging.info(f"{self.get_name()} may have missed {total_already_displayed} messages (already displayed)")
            self._pending_messages = []
        elif isinstance(new_state, StateAway):
            temp_pending_messages = self._pending_messages.copy()
            self._pending_messages = []
            for from_name, message in temp_pending_messages:
                if from_name is not None and message is not None:
                    new_state.receive(self, from_name, message)  # Display and add to count
                else:
                    self._pending_messages.append((None, None))  # Just keep the count
        elif isinstance(new_state, StateBusy):
            pass  # Nothing to be done

        self._current_state = new_state

    def add_pending_message(self, from_name: Optional[str], message: Optional[str]) -> None:
        self._pending_messages.append((from_name, message))

    # Methods delegated to the states
    def receive(self, from_name: str, message: str) -> None:
        return self._current_state.receive(self, from_name, message)

    def send(self, message: str, to_name: str) -> bool:
        return self._current_state.send(self, message, to_name)

    def broadcast(self, message: str) -> None:
        return self._current_state.broadcast(self, message)
