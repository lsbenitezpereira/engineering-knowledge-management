package Lab5_Prototype;

public interface CharacterPrototype {

    void setName(String string);

    void display();

    CharacterPrototype clone();

}
