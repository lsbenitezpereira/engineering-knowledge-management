package Lab5_Prototype;

public class Character implements CharacterPrototype {
    String name;
    String characterClass;
    String armor;
    String weapon;

    @Override
    public void display() {
        System.out.println("I am a powerful " + characterClass + ", in a fancy " + armor + ", ready to attack you with a " + weapon);
    }
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getCharacterClass() {
        return characterClass;
    }
    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }
    public String getArmor() {
        return armor;
    }
    public void setArmor(String armor) {
        this.armor = armor;
    }
    public String getWeapon() {
        return weapon;
    }
    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public Character clone() {
        Character c = new Character();
        c.setArmor(this.getArmor());
        c.setCharacterClass(this.getCharacterClass());
        c.setName(this.getName());
        c.setWeapon(this.getWeapon());
        return c;
    }
}
