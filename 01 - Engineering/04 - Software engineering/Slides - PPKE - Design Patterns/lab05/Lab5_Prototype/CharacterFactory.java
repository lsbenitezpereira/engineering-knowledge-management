package Lab5_Prototype;

import java.util.ArrayList;

public class CharacterFactory {
    ArrayList<Character> chars = new ArrayList<>();

    public CharacterFactory(){
        Character c1 = new Character();
        c1.setCharacterClass("mage");
        c1.setArmor("robe");
        c1.setWeapon("staff");
        chars.add(c1);

        Character c2 = new Character();
        c2.setCharacterClass("warrior");
        c2.setArmor("thick dragon leather");
        c2.setArmor("Heavy metal bar");
        chars.add(c2);
    }

    public Character createCharacter(String string) {
        for (Character c : chars) {
            if (c.getCharacterClass().equals(string)) {
                return c.clone();
            }
        }
        return null;
    }
}
