package Lab5_Prototype;

public class Game {
    public static void main(String[] args) {
        CharacterFactory factory = new CharacterFactory();

        // Create a warrior
        Character warrior1 = factory.createCharacter("warrior");
        warrior1.setName("Aragorn");
        warrior1.display();

        // Clone the warrior and modify
        Character warrior2 = warrior1.clone();
        warrior2.setName("Boromir");
        warrior2.display();

        // Create a mage
        Character mage1 = factory.createCharacter("mage");
        mage1.setName("Gandalf");
        mage1.display();

        // Clone the mage and modify
        Character mage2 = mage1.clone();
        mage2.setName("Saruman");
        mage2.display();
    }
}