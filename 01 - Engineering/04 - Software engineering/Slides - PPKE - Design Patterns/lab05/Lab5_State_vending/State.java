package Lab5_State_vending;

public interface State {

    void insertMoney(VendingMachine vendingMachine, int money);

    void selectProduct(VendingMachine vendingMachine, String p);

    void dispense(VendingMachine vendingMachine);

    void refund(VendingMachine vendingMachine);

}
