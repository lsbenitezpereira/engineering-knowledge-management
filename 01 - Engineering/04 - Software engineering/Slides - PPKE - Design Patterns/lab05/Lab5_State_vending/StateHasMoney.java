package Lab5_State_vending;

public class StateHasMoney implements State {

    @Override
    public void insertMoney(VendingMachine vendingMachine, int money) {
        vendingMachine.setMoney(vendingMachine.getMoney() + money);
    }

    @Override
    public void selectProduct(VendingMachine vendingMachine, String p) {
        vendingMachine.setProduct(p);
        vendingMachine.setState(new StateProductSelected());
    }

    @Override
    public void dispense(VendingMachine vendingMachine) {
        System.out.println("Do you even know what you want me to dispense? Yes? Then tell me, I'm not clairvoyant");
    }

    @Override
    public void refund(VendingMachine vendingMachine) {
        System.out.println("All right, keep your coins... *tling tling tling sounds *");
        vendingMachine.setMoney(0);
        vendingMachine.setState(new StateNoMoney());
    }

}
