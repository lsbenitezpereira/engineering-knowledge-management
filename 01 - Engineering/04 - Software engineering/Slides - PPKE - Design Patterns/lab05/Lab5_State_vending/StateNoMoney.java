package Lab5_State_vending;

public class StateNoMoney implements State{

    @Override
    public void insertMoney(VendingMachine vendingMachine, int money) {
        vendingMachine.setMoney(vendingMachine.getMoney() + money);
        vendingMachine.setState(new StateHasMoney());
    }

    @Override
    public void selectProduct(VendingMachine vendingMachine, String p) {
        System.out.println("Please pay first, and just then select the product");
    }

    @Override
    public void dispense(VendingMachine vendingMachine) {
        System.out.println("Money first, product later...");
    }

    @Override
    public void refund(VendingMachine vendingMachine) {
        System.out.println("Refund WHAT money, poor dumbass");
    }

}
