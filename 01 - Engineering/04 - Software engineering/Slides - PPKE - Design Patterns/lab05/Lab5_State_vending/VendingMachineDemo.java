package Lab5_State_vending;

public class VendingMachineDemo {
    public static void main(String[] args) {
        VendingMachine machine = new VendingMachine();

        machine.insertMoney(1);
        machine.selectProduct("Cola");  // TODO: this should return "Insufficient funds"
        machine.insertMoney(1);
        machine.selectProduct("Cola");
        machine.dispense();

        machine.insertMoney(2);
        machine.selectProduct("Chips");
        machine.refund();

        machine.insertMoney(1);
        machine.selectProduct("Candy");
        machine.dispense();
    }
}