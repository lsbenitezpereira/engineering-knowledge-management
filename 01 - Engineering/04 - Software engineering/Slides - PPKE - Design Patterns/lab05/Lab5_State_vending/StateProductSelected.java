package Lab5_State_vending;

public class StateProductSelected implements State{

    @Override
    public void insertMoney(VendingMachine vendingMachine, int money) {
        System.out.println("More money? Okay, I can't say no...");
    }

    @Override
    public void selectProduct(VendingMachine vendingMachine, String p) {
        vendingMachine.setProduct(p);
    }

    @Override
    public void dispense(VendingMachine vendingMachine) {
        System.out.println("Here it is");
        vendingMachine.setProduct("");
        vendingMachine.setMoney(0);
        vendingMachine.setState(new StateNoMoney());
    }

    @Override
    public void refund(VendingMachine vendingMachine) {
        System.out.println("All right, keep your coins... *tling tling tling sounds *");
        vendingMachine.setMoney(0);
        vendingMachine.setState(new StateNoMoney());
    }
}
