package Lab5_State_vending;

public class VendingMachine {
    State state;
    int money;
    String product;

    public VendingMachine() {
        this.money = 0;
        this.state = new StateNoMoney();
        this.product = "";
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        System.out.println("The new selected product is" + product);
        this.product = product;
    }


    public void insertMoney(int money) {
        state.insertMoney(this, money);
    }

    public void selectProduct(String p) {
        state.selectProduct(this, p);
    }

    public void dispense() {
        state.dispense(this);
    }

    public void refund() {
        state.refund(this);
    }

}
