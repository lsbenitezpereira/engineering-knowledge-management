package example;

public class Client {
    ChatServer server;
    String name;

    public Client(ChatServer server, String name) {
        this.server = server;
        this.name = name;
        this.server.connect(this);
    }

    public String getName(){
        return name;
    }

    public void receive(String from, String message){
        System.out.println(name + " received a message from " + from + " saying: " + message);
    }

    public boolean send(String message, String to){
        return server.send(name, to, message);
    }

    public void broadcast(String message){
        server.broadcast(name, message);
    }
    
}
