package example;

import java.util.ArrayList;

class ChatServer {
    ArrayList<Client> clients = new ArrayList<>();

    public void connect(Client c){
        clients.add(c);
    }

    public void disconnect(Client c){
        clients.remove(c);
    }

    public boolean send(String from, String to, String message){
        for (Client c : clients){
            if (c.getName().equals(to)){
                c.receive(from, message);
                return true;
            }
        }
        return false;
    }

    public void broadcast(String from, String message){
        for (Client c : clients){
            c.receive(from, message);
        }
    }

    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        Client c1 = new Client(server, "Alice");
        Client c2 = new Client(server, "Bob");
        Client c3 = new Client(server, "Charlie");
        // System.out.println("ok");
        c1.broadcast("Hello everyone!");
        c2.send("Hello Alice!", "Alice");
        c3.send("Hello Alice!", "Alice");
        c1.send("Hello Bob!", "Bob");
        c3.send("Hello Bob!", "Bob");
        c1.send("Hello Charlie!", "Charlie");
        c2.send("Hello Charlie!", "Charlie");
        c1.send("Hello Dave!", "Dave");
    }


}