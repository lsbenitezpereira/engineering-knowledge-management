package Lab2_Iterator;

import java.util.Collection;

public class IteratorDemo
{
  public static void main(String[] args) {
    Song song1 = new Song("Imagine", "John Lennon");
    Song song2 = new Song("Bohemian Rhapsody", "Queen");
    Song song3 = new Song("Hotel California", "Eagles");

    Playlist playlist = new Playlist();
    playlist.addSong(song1);
    playlist.addSong(song2);
    playlist.addSong(song3);

    for (Song song : playlist) {
        System.out.println(song);
    }
  }
}