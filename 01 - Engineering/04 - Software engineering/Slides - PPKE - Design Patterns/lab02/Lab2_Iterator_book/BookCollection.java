package Lab2_Iterator_book;
import java.util.ArrayList;

public class BookCollection {
    protected ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book book){
        books.add(book);
    }

    public int getSize(){
        return books.size();
    }

    private class BookCollectionIterator implements BookIterator{
        int position = 0;
        public boolean hasNext(){
            return position < books.size();
        }

        public Book next(){
            return books.get(position++);
        }
    }

    BookCollectionIterator iterator(){
        return new BookCollectionIterator();
    }
}
