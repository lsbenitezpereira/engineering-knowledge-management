package Lab2_Iterator_book;

public class LibrarySystem {
    public static void main(String[] args) {
        BookCollection library = new BookCollection();
        
        library.addBook(new Book("1984", "George Orwell"));
        library.addBook(new Book("To Kill a Mockingbird", "Harper Lee"));
        library.addBook(new Book("Pride and Prejudice", "Jane Austen"));

        System.out.println("Books in the library:");
        BookIterator iterator = library.iterator();
        while (iterator.hasNext()) {
            Book book = iterator.next();
            System.out.println(book);
        }

        System.out.println("\nTotal books: " + library.getSize());
    }
}
