package Lab2_Iterator_book;

public interface BookIterator {
    boolean hasNext();
    Book next();
}
