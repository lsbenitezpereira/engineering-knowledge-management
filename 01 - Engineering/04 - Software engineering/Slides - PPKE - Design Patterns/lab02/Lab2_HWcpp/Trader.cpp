#include <iostream>
#include <string>
#include <vector>
#include "Trader.h"
using namespace std;

/* Strategies */
Strategy::Strategy(){
}

string LowPriceStrategy::shouldBuy(const string& name, int price){
    if (price < 500) {
        return "BUY " + name + " :D";
    } else {
        return "DO NOTHING :|";
    }
};

string HighPriceStrategy::shouldBuy(const string& name, int price){
    if (price > 1000) {
        return "BUY " + name + " :D";
    } else {
        return "DO NOTHING :|";
    }
};

/* Trader */
Observer::Observer(){

}

Trader::Trader(const string& name, Strategy* strategy){
    this->name = name;
    this->strategy = strategy;
}


void Trader::update(const string& name, int price){
    cout << "I, trader " << this->name << ", got to know that thre stock " << name << " is now worth " << price << ". ";
    cout << "Given my current strategy, now I will " << this->strategy->shouldBuy(name, price) << endl;
}
