#include <iostream>
#include <algorithm>
#include "StockMarket.h"

/* Subject */
void Subject::registerObserver(Observer* o){
    observers.push_back(o);
}

void Subject::removeObserver(Observer* o){
    observers.erase(std::remove(observers.begin(), observers.end(), o), observers.end());
}

void Subject::notifyObservers(const string& name, int price){
    for (Observer* o : observers) {
        o->update(name, price);  // Call the update method on each observer
    }
}


/* Stock */
Stock::Stock(const string& name, int price){
    this->name = name;
    this->price = price;
}

string Stock::getName() const {
    return name;
}

int Stock::getPrice() const {
    return price;
}

void Stock::setPrice(int price) {
    this->price = price;
}


/* StockMarket*/
StockMarket::StockMarket() {
}

void StockMarket::addStock(Stock* stock) {
    stocks.push_back(stock);
}

void StockMarket::updateStockPrice(const string& name, int price){
    for (auto& stock : stocks) {
        if (stock->getName() == name) {
            stock->setPrice(price);
            break;
        }
    }
    this->notifyObservers(name, price);
}