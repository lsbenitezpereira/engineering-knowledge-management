#include <iostream>
#include "StockMarket.h"
#include "Trader.h"
using namespace std;

int main() {
    StockMarket* market = new StockMarket();
    Stock* s1 = new Stock("AAPL", 150);
    Stock* s2 = new Stock("GOOGL", 2800);
    Stock* s3 = new Stock("MSFT", 300);
    market->addStock(s1);
    market->addStock(s2);
    market->addStock(s3);
    
    // Create traders - observers. They have different buying strategies
    Strategy* stg1 = new LowPriceStrategy();
    Strategy* stg2 = new HighPriceStrategy();
    Trader* trader1 = new Trader("Alice", stg1);
    Trader* trader2 = new Trader("Bob", stg2);
    
    // Register traders as observers
    market->registerObserver(trader1);
    market->registerObserver(trader2);
    
    // Updating stock prices to trigger observers - different ones will buy
    market->updateStockPrice("AAPL", 160);
    market->updateStockPrice("GOOGL", 2750);
    market->updateStockPrice("MSFT", 290);

    //Dealocate objects
    delete market;
    delete s1;
    delete s2;
    delete s3;
    delete stg1;
    delete stg2;
    delete trader1;
    delete trader2;
}