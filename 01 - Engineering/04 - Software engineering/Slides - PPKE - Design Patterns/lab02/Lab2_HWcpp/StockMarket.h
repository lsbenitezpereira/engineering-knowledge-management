#ifndef STOCK_MARKET_H
#define STOCK_MARKET_H

#include <string>
#include <vector>
#include "Trader.h"
using namespace std;

class Subject {
    public:
        void registerObserver(Observer* o);
        void removeObserver(Observer* o);
        void notifyObservers(const string& name, int price);  // Push style: the observers receive what has changed
    private:
        std::vector<Observer*> observers;
};

class Stock {
    public:
        Stock(const string& name, int price);
        string getName() const;
        int getPrice() const;
        void setPrice(int price);
    private:
        string name;
        int price;
};

class StockMarket: public Subject {
    public:
        StockMarket();
        void addStock(Stock* stock);
        void updateStockPrice(const string& name, int price);
    private:
        vector<Stock*> stocks;
};

#endif // STOCK_MARKET_H