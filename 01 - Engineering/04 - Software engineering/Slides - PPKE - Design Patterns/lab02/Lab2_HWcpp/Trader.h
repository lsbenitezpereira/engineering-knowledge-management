#ifndef TRADER_H
#define TRADER_H

#include <string>
#include <vector>
using namespace std;

/* Strategies */
class Strategy{
    public:
        Strategy();
        virtual string shouldBuy(const string& name, int price) = 0;
};

class LowPriceStrategy : public Strategy{
    public:
        string shouldBuy(const string& name, int price) override;
};

class HighPriceStrategy : public Strategy{
    public:
        string shouldBuy(const string& name, int price) override;
};

/* Trader */
class Observer {
    public:
        Observer();
        virtual void update(const string& name, int price) = 0;  // Push style: the subject sends what has changed
};

class Trader: public Observer{
    public:
        Trader(const string& name, Strategy* strategy);
        void update(const string& name, int price);
    
    private:
        string name;
        Strategy* strategy;
};

#endif // TRADER_H