package Lab2_Strategy_formatter;

public class UpperCaseFormatter implements Formatter{
    public String format(String text){
        return text.toUpperCase();
    }
}
