package Lab2_Strategy_formatter;

public class LowerCaseFormatter implements Formatter{
    public String format(String text){
        return text.toLowerCase();
    }
}
