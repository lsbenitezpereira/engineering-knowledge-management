package Lab2_Strategy_formatter;

public interface Formatter {
    public abstract String format(String text);
}
