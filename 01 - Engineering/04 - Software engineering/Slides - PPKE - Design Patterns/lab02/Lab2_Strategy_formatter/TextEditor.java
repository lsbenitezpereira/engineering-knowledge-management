package Lab2_Strategy_formatter;

public class TextEditor {
    Formatter formatter;
    protected void setFormatter(Formatter formatter){
        this.formatter = formatter;
    };

    public String formatText(String text){
        return this.formatter.format(text);
    }
}
