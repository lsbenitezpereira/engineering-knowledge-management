package Lab2_Strategy_formatter;

public class FormatterDemo {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        String text = "this is a sample text. it demonstrates the strategy pattern. UPPERCASE and lowercase are supported.";

        editor.setFormatter(new UpperCaseFormatter());
        System.out.println("Uppercase: " + editor.formatText(text));

        editor.setFormatter(new LowerCaseFormatter());
        System.out.println("Lowercase: " + editor.formatText(text));

    }
}
