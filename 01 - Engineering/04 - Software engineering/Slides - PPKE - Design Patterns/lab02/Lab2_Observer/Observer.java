package Lab2_Observer;

public abstract class Observer {
    public abstract void update(int val);
}
