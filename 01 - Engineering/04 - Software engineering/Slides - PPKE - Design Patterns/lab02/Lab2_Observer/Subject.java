package Lab2_Observer;

import java.util.ArrayList;

//This subject is "push model": when sending the update, it also sends the values
public class Subject{
	protected int m_value;
    ArrayList<Observer> observers = new ArrayList<>();

    public void add(Observer o){
        observers.add(o);
    }

    public void remove(Observer o){
        observers.remove(o);
    }

    public void notifyObservers(){
        for(Observer o: observers){
            o.update(m_value);
        }
    }

	public void set_value(int value)
	{
		m_value = value;
		notifyObservers();
	}


	public static void main(String[] args) {
		Observer m_div_obj = new DivObserver(4);
		Observer m_mod_obj = new ModObserver(3);
		Subject subj = new Subject();
        subj.add(m_div_obj);
        subj.add(m_mod_obj);
		subj.set_value(14);
        System.out.println("-------------------");
        subj.set_value(9);
	}

}
