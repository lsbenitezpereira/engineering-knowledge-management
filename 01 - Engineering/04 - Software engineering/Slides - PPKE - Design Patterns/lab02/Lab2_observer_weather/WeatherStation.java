package Lab2_observer_weather;

public class WeatherStation extends Subject{
    int temperature = 0;
    int pressure = 0;
    float humidity = 0.0f;

    public void setMeasurements(int t, int p, float h){
        this.temperature = t;
        this.pressure = p;
        this.humidity = h;
        this.notifyObservers();
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public float getHumidity() {
        return humidity;
    }
    
}
