package Lab2_observer_weather;

public class CurrentDisplay extends Observer{
    WeatherStation station;
    

    public CurrentDisplay(WeatherStation station) {
        this.station = station;
    }

    public void update(){
        System.out.println("Current temperature: "+this.station.getTemperature()+"C; Humidity: "+this.station.getHumidity()+"; Pressure:"+this.station.getPressure());
    }
}
