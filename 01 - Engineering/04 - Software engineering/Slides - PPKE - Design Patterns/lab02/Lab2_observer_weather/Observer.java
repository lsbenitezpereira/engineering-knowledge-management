package Lab2_observer_weather;

public abstract class Observer {
    public abstract void update();
}
