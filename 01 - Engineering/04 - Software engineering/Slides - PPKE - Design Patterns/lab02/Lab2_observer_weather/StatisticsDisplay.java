package Lab2_observer_weather;

public class StatisticsDisplay extends Observer{
    int numMeasurement = 0;
    int sumTemperature = 0;
    int sumHumidity = 0;
    float sumPressure = 0.0f;
    WeatherStation station;
    

    public StatisticsDisplay(WeatherStation station) {
        this.station = station;
    }

    public void update(){
        this.numMeasurement++;
        this.sumTemperature += this.station.getTemperature();
        this.sumHumidity += this.station.getHumidity();
        this.sumPressure += this.station.getPressure();
        System.out.println("Based on "+this.numMeasurement+" measurements, average temperature: "+sumTemperature/numMeasurement+"; Humidity: "+sumHumidity/numMeasurement+"; Pressure: "+sumPressure/numMeasurement);

    }
}
