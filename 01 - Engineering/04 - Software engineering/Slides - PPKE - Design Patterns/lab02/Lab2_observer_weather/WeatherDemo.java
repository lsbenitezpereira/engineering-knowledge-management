package Lab2_observer_weather;

public class WeatherDemo {
    public static void main(String[] args) {
        //Create station
        WeatherStation station = new WeatherStation();

        //Create displays, subscribe
        CurrentDisplay obs1 = new CurrentDisplay(station);
        StatisticsDisplay obs2 = new StatisticsDisplay(station);
        station.add(obs1);
        station.add(obs2);

        //Make some measurements: temperature (C), humidity, pressure
        station.setMeasurements(20, 65, 30.4f);
        station.setMeasurements(22, 70, 29.2f);
        station.setMeasurements(18, 90, 29.2f);
    }
}
