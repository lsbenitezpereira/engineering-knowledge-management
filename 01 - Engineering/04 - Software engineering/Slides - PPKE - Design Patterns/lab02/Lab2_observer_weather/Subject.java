package Lab2_observer_weather;

import java.util.ArrayList;

public class Subject {
    ArrayList<Observer> observers = new ArrayList<>();
    public void add(Observer o){
        observers.add(o);
    }

    public void remove(Observer o){
        observers.remove(o);
    }

    public void notifyObservers(){
        for(Observer o: observers){
            o.update();
        }
    }
}
