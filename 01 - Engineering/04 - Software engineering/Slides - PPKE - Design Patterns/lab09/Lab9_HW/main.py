from typing import List
from book_modern import BookModern, Book
from book_legacy import BookLegacy
from book_adapter import BookLegacyAdapter
from database import BookDatabaseModern, BookDatabaseLegacy


# Pile of old books (stale and missing page)
legacy_book1 = BookLegacy("Mark Twain|Adventures of Huckleberry Finn|Shelf C2")
legacy_book2 = BookLegacy("J.K. Rowling|Harry Potter and the Sorcerer's Stone|Corner between D7 and D8")
book_db_legacy = BookDatabaseLegacy()
book_db_legacy.add_book(legacy_book1)
book_db_legacy.add_book(legacy_book2)

# Example queries - old database
print("Searching OLD database for books by Jane Austen:")
results: List[BookLegacy] = book_db_legacy.find_books_by_author("Jane Austen")
assert len(results) == 0
print("No results found (expected).")

print("\nSearching OLD database for books by Mark Twain:")
results: List[BookLegacy] = book_db_legacy.find_books_by_author("Mark Twain")
for book in results:
    print(f"book: {book.get_data()}")


######################################################################
# Pile of modern books (shinning and smelling like fresh pages)
print('|A|'+'-'*50)
modern_book1 = BookModern(author="Jane Austen", title="Pride and Prejudice", location="Shelf A3")
modern_book2 = BookModern(author="George Orwell", title="1984", location="Shelf B5")
modern_book3 = BookModern(author="Michelle Obama", title="Becoming", location="Shelf E1")
book_db = BookDatabaseModern()
book_db.add_book(modern_book1)
book_db.add_book(modern_book2)
book_db.add_book(modern_book3)

# Example queries - modern database
print("Searching MODERN database for books by Mark Twain:")
results: List[Book] = book_db.find_books_by_author("Mark Twain")
assert len(results) == 0
print("No results found (expected).")

print("\nSearching MODERN database for books titled 'Becoming':")
results2: List[Book] = book_db.find_books_by_title("Becoming")
for book in results2:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()}")


######################################################################
# Using the adaptors to add the old pile into a unified database
print('|B|'+'-'*50)
for legacy_book in book_db_legacy.get_all_books():
    book_db.add_book(BookLegacyAdapter(legacy_book))

print("Searching for books by Jane Austen:")
results: List[Book] = book_db.find_books_by_author("Jane Austen")
for book in results:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()}")

print("\nSearching for books titled '1984':")
results: List[Book] = book_db.find_books_by_title("1984")
for book in results:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()}")

print("\nSearching for books by Mark Twain:")
results: List[Book] = book_db.find_books_by_author("Mark Twain")
for book in results:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()}")

######################################################################
# Edge case: Try to search with an invalid title
print('|C|'+'-'*50)
results: List[Book] = book_db.find_books_by_title("Invalid Title")
assert len(results) == 0

# Edge case: repeated books
book_db.add_book(modern_book2)
results: List[Book] = book_db.find_books_by_title("1984")
assert len(results) == 2
print("\nSearching for books titled '1984':")
for book in results:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()}")

# Edge case: unformatted old book
try:
    BookLegacy("Invalid Data")
except ValueError as e:
    print(f"\nError was correctly raised: {e}")

# Edge case: same book appears in both old and new piles
modern_book_4 = BookModern(author="Mark Twain", title="Adventures of Huckleberry Finn", location="Shelf C2")
book_db.add_book(modern_book_4)
results: List[Book] = book_db.find_books_by_author("Mark Twain")
assert len(results) == 2
print("\nSearching for books by Mark Twain:")
for book in results:
    print(f"Author: {book.get_author()}, Title: {book.get_title()}, Location: {book.get_location()} (object class: {type(book).__name__})")
