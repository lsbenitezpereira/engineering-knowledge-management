from book_legacy import BookLegacy
from book_modern import Book


class BookLegacyAdapter(Book):
    _legacy_book: BookLegacy

    def __init__(self, legacy_book: BookLegacy) -> None:
        self._legacy_book = legacy_book

    def get_author(self) -> str:
        return self._legacy_book.get_data().split("|")[0]

    def get_title(self) -> str:
        return self._legacy_book.get_data().split("|")[1]

    def get_location(self) -> str:
        return self._legacy_book.get_data().split("|")[2]
