class BookLegacy:
    _data: str

    def __init__(self, data: str) -> None:
        if len(data.split("|")) != 3:
            raise ValueError("Invalid data format. Expected 'author|title|location'.")
        self._data = data

    def get_data(self) -> str:
        return self._data
