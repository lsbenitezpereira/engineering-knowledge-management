from abc import ABC, abstractmethod


class Book(ABC):
    @abstractmethod
    def get_author(self) -> str:
        pass

    @abstractmethod
    def get_title(self) -> str:
        pass

    @abstractmethod
    def get_location(self) -> str:
        pass


class BookModern(Book):
    _author: str
    _title: str
    _location: str

    def __init__(self, author: str, title: str, location: str) -> None:
        self._author = author
        self._title = title
        self._location = location

    def get_author(self) -> str:
        return self._author

    def get_title(self) -> str:
        return self._title

    def get_location(self) -> str:
        return self._location
