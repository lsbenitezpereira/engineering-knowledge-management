from typing import List
from book_modern import Book
from book_legacy import BookLegacy


class BookDatabaseModern:
    _books: List[Book]

    def __init__(self) -> None:
        self._books = []

    def add_book(self, book: Book) -> None:
        self._books.append(book)

    def find_books_by_author(self, author: str) -> List[Book]:
        return [book for book in self._books if book.get_author() == author]

    def find_books_by_title(self, title: str) -> List[Book]:
        return [book for book in self._books if book.get_title() == title]


class BookDatabaseLegacy:
    _books: List[BookLegacy]

    def __init__(self) -> None:
        self._books = []

    def add_book(self, book: BookLegacy) -> None:
        self._books.append(book)

    def get_all_books(self) -> List[BookLegacy]:
        return self._books

    def find_books_by_author(self, author: str) -> List[BookLegacy]:
        return [book for book in self._books if book.get_data().split("|")[0] == author]

    def find_books_by_title(self, title: str) -> List[BookLegacy]:
        return [book for book in self._books if book.get_data().split("|")[1] == title]
