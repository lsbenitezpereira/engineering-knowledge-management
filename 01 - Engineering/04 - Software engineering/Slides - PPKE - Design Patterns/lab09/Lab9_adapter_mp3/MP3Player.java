package Lab9_adapter_mp3;

public interface MP3Player {
    public String getMetadata();
    public void play();
    public void setVolume(Integer v);
}
