package Lab9_adapter_mp3;

public class MOVPlayerAdapter implements MP3Player{
    private MovPlayer adaptee = new MovPlayer();

    @Override
    public String getMetadata() {
        return "Title: " + this.adaptee.getTitle();
    }

    @Override
    public void play() {
        System.out.println(this.adaptee.getAudio());
    }

    @Override
    public void setVolume(Integer v) {
        this.adaptee.setVolume(v);
    }

}
