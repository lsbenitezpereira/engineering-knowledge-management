package Lab9_adapter_mp3;

class MediaPlayerDemo {
    public static void main(String[] args) {
        // Using original MP3 Player
        System.out.println("=== Using MP3 Player ===");
        MediaPlayerClient mp3Client = new MediaPlayerClient(new DefaultMP3Player());
        mp3Client.playMedia("song.mp3");
        mp3Client.stopMedia();
        
        // Using MOV Player through adapter
        System.out.println("\n=== Using MOV Player ===");
        MediaPlayerClient movClient = new MediaPlayerClient(new MOVPlayerAdapter());
        movClient.playMedia("video.mov");
        movClient.stopMedia();
        
        // Using MP4 Player through adapter
        System.out.println("\n=== Using MP4 Player ===");
        MediaPlayerClient mp4Client = new MediaPlayerClient(new MP4PlayerAdapter());
        mp4Client.playMedia("movie.mp4");
        mp4Client.stopMedia();
    }
}