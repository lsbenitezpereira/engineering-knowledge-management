package Lab9_adapter_mp3;

public class MediaPlayerClient {
    protected MP3Player player;
    
    public MediaPlayerClient(MP3Player player) {
        this.player = player;
    }

    public void playMedia(String string) {
        this.player.play();
    }

    public void stopMedia() {
        System.out.println("Sorry, MP3 are too stuborn to be stopped");
    }

}
