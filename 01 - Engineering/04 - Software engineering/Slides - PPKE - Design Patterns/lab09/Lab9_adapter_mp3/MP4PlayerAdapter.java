package Lab9_adapter_mp3;

public class MP4PlayerAdapter implements MP3Player{
    private MP4Player adaptee = new MP4Player();

    @Override
    public String getMetadata() {
        return "Title: " + this.adaptee.getTitle();
    }

    @Override
    public void play() {
        System.out.println(this.adaptee.getAudio());
    }

    @Override
    public void setVolume(Integer v) {
        this.adaptee.setVolume(v);
    }

}
