package Lab9_adapter_mp3;

public class DefaultMP3Player implements MP3Player{

    @Override
    public String getMetadata() {
        return "some legacy metadata";
    }

    @Override
    public void play() {
        System.out.println("Legacy behavior of playing MP3 stuff: fiii-gaaaa-roo, figaro figaro figaro!");
    }

    @Override
    public void setVolume(Integer v) {
        System.out.println("Legacy behavior for setting volume to " + v);
    }

}
