package inj;
// this is a constructor injection
// the main is doing the role of the injector
public class Main {
    public static void main(String[] args) {
        // Create dependencies
        CustomerNotifier notifier = new EmailNotifier();
        Logger logger = new ConsoleLogger();

        // Inject dependencies into OrderProcessor
        OrderProcessor orderProcessor = new OrderProcessor(notifier, logger);

        // Create an order
        Order order = new Order();
        order.setCustomerId("12345");
        order.setOrderDetails("2 x Pizza, 1 x Soda");

        // Process the order
        orderProcessor.processOrder(order);
    }
}
