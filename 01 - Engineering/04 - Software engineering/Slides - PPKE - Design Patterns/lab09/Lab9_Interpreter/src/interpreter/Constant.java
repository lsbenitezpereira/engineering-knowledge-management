package interpreter;

public class Constant extends BooleanExp {
    protected boolean value;
    public Constant(boolean v) {
        value = v;
    }

    @Override
    public boolean evaluate(Context c) {
        return value;
    }

}
