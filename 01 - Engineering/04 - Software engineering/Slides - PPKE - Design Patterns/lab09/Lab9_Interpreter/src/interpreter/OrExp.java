package interpreter;

public class OrExp extends BooleanExp {
    protected BooleanExp left;
    protected BooleanExp right;
    public OrExp(BooleanExp exp, BooleanExp exp2) {
        this.left = exp;
        this.right = exp2;
    }
    @Override
    public boolean evaluate(Context c) {
        return left.evaluate(c) || right.evaluate(c);
    }
    

}
