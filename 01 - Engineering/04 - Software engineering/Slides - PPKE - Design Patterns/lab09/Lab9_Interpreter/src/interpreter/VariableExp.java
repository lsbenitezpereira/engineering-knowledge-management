package interpreter;

public class VariableExp extends BooleanExp {
    protected String label;
    public VariableExp(String string) {
        label = string;
    }

    @Override
    public boolean evaluate(Context c) {
        boolean val = c.lookup(label);
        System.out.println("Variable "+label+": "+val);
        return val;
    }


}
