package interpreter;

public class NotExp extends BooleanExp {
    BooleanExp child;
    public NotExp(BooleanExp e) {
        child = e;
    }
    @Override
    public boolean evaluate(Context c) {
        return !child.evaluate(c);
    }
    
}
