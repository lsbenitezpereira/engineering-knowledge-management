package interpreter;

public class AndExp extends BooleanExp {
    protected BooleanExp left;
    protected BooleanExp right;
    public AndExp(BooleanExp a, BooleanExp b) {
        left = a;
        right = b;
    }

    @Override
    public boolean evaluate(Context c) {
        System.out.println("And expression: "+left.evaluate(c) + " && " + right.evaluate(c));
        return left.evaluate(c) && right.evaluate(c);
    }

}
