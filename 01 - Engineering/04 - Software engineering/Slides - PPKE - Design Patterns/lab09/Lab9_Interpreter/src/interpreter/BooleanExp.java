package interpreter;

public abstract class BooleanExp {
    public abstract boolean evaluate(Context c);
}
