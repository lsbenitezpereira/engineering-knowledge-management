package dp;

public class NewRectangleAdapter implements NewShape {
    protected LegacyRectangle r = new LegacyRectangle();

    @Override
    public void draw(int x1, int y1, int x2, int y2) {
        int d1 = x1*x1+y1*y1;
        int d2 = x2*x2+y2*y2;
        int x, y, w, h;
        if (d1<d2) {
            x = x1;
            y = y1;
        } else {
            x = x2;
            y = y2;
        }
        w = Math.abs(x1-x2);
        h = Math.abs(y1-y2);
        r.draw(x, y, w, h);
    }

}
