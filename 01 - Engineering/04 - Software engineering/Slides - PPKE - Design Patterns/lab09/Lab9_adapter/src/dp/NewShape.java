package dp;

public interface NewShape {
    public void draw(int x1, int y1, int x2, int y2);
}
