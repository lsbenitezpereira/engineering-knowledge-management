package dp;

public class Main {

	public static void main(String[] args) {
		NewShape[] shapes = {new NewLineAdapter(), new NewRectangleAdapter()};

		// A begin and end point from a graphical editor
		int x1 = 10, y1 = 20;
		int x2 = 30, y2 = 60;
		for (NewShape s : shapes) {
			s.draw(x1, y1, x2, y2);
		}
	}

}
