package Lab9_Facade;

// SoundSystem class
class SoundSystem {
    void on() {
        System.out.println("Sound system is on");
    }

    void off() {
        System.out.println("Sound system is off");
    }
}