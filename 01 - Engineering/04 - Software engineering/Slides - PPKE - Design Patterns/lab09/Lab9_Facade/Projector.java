package Lab9_Facade;

// Projector class
class Projector {
    void on() {
        System.out.println("Projector is on");
    }

    void off() {
        System.out.println("Projector is off");
    }
}