package Lab9_Facade;

public class FacadeTest {
    // Main class
    public static void main(String[] args) {
        // Create the facade
        HomeTheaterFacade homeTheaterFacade = new HomeTheaterFacade();

        // Watch a movie
        homeTheaterFacade.watchMovie("Inception");

        // End the movie
        homeTheaterFacade.endMovie();
    }
}
