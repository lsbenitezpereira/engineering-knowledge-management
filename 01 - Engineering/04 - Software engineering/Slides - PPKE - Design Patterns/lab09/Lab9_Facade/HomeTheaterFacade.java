package Lab9_Facade;

public class HomeTheaterFacade {
    DVDPlayer p = new DVDPlayer();
    Lights l = new Lights();
    Projector projector = new Projector();
    SoundSystem ss = new SoundSystem();


    public void watchMovie(String string) {
        l.dim();
        projector.on();
        ss.on();
        p.playMovie(string);
    }

    public void endMovie() {
        p.stop();
        l.brighten();
        projector.off();
        ss.off();
    }

}
