package Lab9_Facade;

// Lights class
class Lights {
    void dim() {
        System.out.println("Lights are dimmed");
    }

    void brighten() {
        System.out.println("Lights are brightened");
    }
}
