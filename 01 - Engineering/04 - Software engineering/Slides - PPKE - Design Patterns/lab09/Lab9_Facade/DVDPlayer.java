package Lab9_Facade;

// DVDPlayer class
class DVDPlayer {
    void playMovie(String movie) {
        System.out.println("Playing movie: " + movie);
    }

    void stop() {
        System.out.println("DVD Player stopped");
    }
}