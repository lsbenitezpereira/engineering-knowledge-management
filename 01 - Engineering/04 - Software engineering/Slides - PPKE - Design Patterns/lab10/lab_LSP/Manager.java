package lab_LSP;

public class Manager extends Employee implements IManager {
    @Override
    public void calculateHourlyRate(int rank) {
        setSalary(rank * 440);
    }

    @Override
    public void generatePerformanceReview() {
        System.out.println("Generating performance review...");
    }
}