package lab_LSP;

public class Main {
    public static void main(String[] args) {
        IManaged employee = new Employee();
        employee.setFirstname("Garo");
        employee.setLastname("Ke");
        employee.assignManager(new Manager());
        employee.calculateHourlyRate(2);
        System.out.println(employee.getFirstname() + " " + employee.getLastname() + " has a salary of " + employee.getSalary());

        IManaged manager = new Manager();
        manager.setFirstname("Andras");
        manager.setLastname("Kalman");
        manager.assignManager(new CEO());
        manager.calculateHourlyRate(2);
        System.out.println(manager.getFirstname() + " " + manager.getLastname() + " has a salary of " + manager.getSalary());

        IEmployee ceo = new CEO();
        ceo.setFirstname("Balint");
        ceo.setLastname("Kovacs");
        ceo.calculateHourlyRate(2);
        System.out.println(ceo.getFirstname() + " " + ceo.getLastname() + " has a salary of " + ceo.getSalary());
    }
}
