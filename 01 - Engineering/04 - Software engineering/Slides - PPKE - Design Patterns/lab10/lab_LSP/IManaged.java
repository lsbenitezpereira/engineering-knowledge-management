package lab_LSP;

public interface IManaged extends IEmployee {
    IEmployee getManager();
    void setManager(IEmployee manager);

    void assignManager(IEmployee manager);
}
