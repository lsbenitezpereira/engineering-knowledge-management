package lab_LSP;

public abstract class BaseEmployee implements IEmployee {
    private String firstname;
    private String lastname;
    private double salary;

    @Override
    public String getFirstname() {
        return firstname;
    }

    @Override
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public String getLastname() {
        return lastname;
    }

    @Override
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public void calculateHourlyRate(int rank) {
        this.salary = rank * 12;
    }
}
