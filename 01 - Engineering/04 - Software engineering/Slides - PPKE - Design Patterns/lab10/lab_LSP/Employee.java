package lab_LSP;

public class Employee extends BaseEmployee implements IManaged {
    private IEmployee manager;

    @Override
    public IEmployee getManager() {
        return manager;
    }

    @Override
    public void setManager(IEmployee manager) {
        this.manager = manager;
    }

    @Override
    public void assignManager(IEmployee manager) {
        this.manager = manager;
        System.out.println("Manager assigned.");
    }
}