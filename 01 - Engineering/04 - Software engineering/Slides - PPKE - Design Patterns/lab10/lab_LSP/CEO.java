package lab_LSP;

public class CEO extends BaseEmployee implements IManager {
    @Override
    public void calculateHourlyRate(int rank) {
        setSalary(rank * 200);
    }

    @Override
    public void generatePerformanceReview() {
        System.out.println("Generating CEO performance review...");
    }
}