package lab_LSP;

public interface IManager extends IEmployee {
    void generatePerformanceReview();
}
