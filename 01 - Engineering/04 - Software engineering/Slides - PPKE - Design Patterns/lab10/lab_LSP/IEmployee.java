package lab_LSP;

public interface IEmployee {
    String getFirstname();
    void setFirstname(String firstname);

    String getLastname();
    void setLastname(String lastname);

    double getSalary();
    void setSalary(double salary);

    void calculateHourlyRate(int rank);
}
