package lab_SRP;

    public class PersonValidator {
        public static boolean validate(Person person) {
            if (person.getName() == null || person.getName().isBlank()) {
                StandardMessages.displayValidationError("Name");
                return false;
            }
    
            if (person.getLastName() == null || person.getLastName().isBlank()) {
                StandardMessages.displayValidationError("Last name");
                return false;
            }
    
            return true;
        }
    }