package lab_SRP;

public class StandardMessages {
    public static void welcomeMessage() {
        System.out.println("Welcome to my application!");
    }

    public static void endApplication() {
        System.out.println("Press Enter to exit...");
        try {
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayValidationError(String fieldName) {
        System.out.println("You did not give us a valid " + fieldName + "!");
    }
}