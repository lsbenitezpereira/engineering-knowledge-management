package lab_SRP;

public class Main {
    public static void main(String[] args) {
        

        StandardMessages.welcomeMessage();

        Person user = PersonDataCapture.capture();

        boolean isUserValid = PersonValidator.validate(user);

        if (!isUserValid) {
            StandardMessages.endApplication();
            return;
        }

        AccountGenerator.createAccount(user);

        StandardMessages.endApplication();
    }
}

