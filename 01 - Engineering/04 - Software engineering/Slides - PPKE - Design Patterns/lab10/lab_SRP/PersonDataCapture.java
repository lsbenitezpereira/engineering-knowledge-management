package lab_SRP;

import java.util.Scanner;

public class PersonDataCapture {
    public static Person capture() {
        Scanner scanner = new Scanner(System.in);

        Person output = new Person();

        System.out.println("What is your name");
        output.setName(scanner.nextLine());

        System.out.println("What is your last name");
        output.setLastName(scanner.nextLine());

        return output;
    }
}