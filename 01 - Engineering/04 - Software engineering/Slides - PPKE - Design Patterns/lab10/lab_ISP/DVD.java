﻿package lab_ISP;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DVD implements IBorrowableDVD {
    private List<String> actors = new ArrayList<>();
    private int runtimeInMinutes;
    private Date borrowDate;
    private int checkoutDurationInDays;
    private String libraryId;
    private String title;

    @Override
    public List<String> getActors() {
        return actors;
    }

    @Override
    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    @Override
    public int getRuntimeInMinutes() {
        return runtimeInMinutes;
    }

    @Override
    public void setRuntimeInMinutes(int runtimeInMinutes) {
        this.runtimeInMinutes = runtimeInMinutes;
    }

    @Override
    public Date getBorrowDate() {
        return borrowDate;
    }

    @Override
    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    @Override
    public int getCheckoutDurationInDays() {
        return checkoutDurationInDays;
    }

    @Override
    public void setCheckoutDurationInDays(int checkoutDurationInDays) {
        this.checkoutDurationInDays = checkoutDurationInDays;
    }

    @Override
    public String getLibraryId() {
        return libraryId;
    }

    @Override
    public void setLibraryId(String libraryId) {
        this.libraryId = libraryId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void checkIn() {
        System.out.println("DVD checked in.");
    }

    @Override
    public void checkOut(String libraryId) {
        System.out.println("DVD checked out with Library ID: " + libraryId);
    }

    @Override
    public Date getDueDate() {
        return new Date(borrowDate.getTime() + (long) checkoutDurationInDays * 24 * 60 * 60 * 1000);
    }
}
