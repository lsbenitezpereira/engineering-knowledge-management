﻿package lab_ISP;
import java.util.Date;

public class AudioBook implements IBorrowableAudioBook {
    private Date borrowDate;
    private int checkoutDurationInDays;
    private int runtimeInMinutes;
    private String libraryId;
    private String title;

    @Override
    public Date getBorrowDate() {
        return borrowDate;
    }

    @Override
    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    @Override
    public int getCheckoutDurationInDays() {
        return checkoutDurationInDays;
    }

    @Override
    public void setCheckoutDurationInDays(int checkoutDurationInDays) {
        this.checkoutDurationInDays = checkoutDurationInDays;
    }

    @Override
    public int getRuntimeInMinutes() {
        return runtimeInMinutes;
    }

    @Override
    public void setRuntimeInMinutes(int runtimeInMinutes) {
        this.runtimeInMinutes = runtimeInMinutes;
    }

    @Override
    public String getLibraryId() {
        return libraryId;
    }

    @Override
    public void setLibraryId(String libraryId) {
        this.libraryId = libraryId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void checkIn() {
        System.out.println("AudioBook checked in.");
    }

    @Override
    public void checkOut(String libraryId) {
        System.out.println("AudioBook checked out with Library ID: " + libraryId);
    }

    @Override
    public Date getDueDate() {
        return new Date(borrowDate.getTime() + (long) checkoutDurationInDays * 24 * 60 * 60 * 1000);
    }
}
