﻿package lab_ISP;

public interface ILibraryItem {
    String getLibraryId();
    void setLibraryId(String libraryId);

    String getTitle();
    void setTitle(String title);
}