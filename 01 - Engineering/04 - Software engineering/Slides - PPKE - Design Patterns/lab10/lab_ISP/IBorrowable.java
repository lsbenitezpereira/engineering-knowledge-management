package lab_ISP;

import java.util.Date;

public interface IBorrowable {
    Date getBorrowDate();
    void setBorrowDate(Date borrowDate);

    int getCheckoutDurationInDays();
    void setCheckoutDurationInDays(int days);

    void checkIn();
    void checkOut(String libraryId);
    Date getDueDate();
}