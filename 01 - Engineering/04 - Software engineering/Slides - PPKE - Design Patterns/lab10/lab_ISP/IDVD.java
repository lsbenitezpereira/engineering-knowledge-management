package lab_ISP;
import java.util.List;


public interface IDVD extends ILibraryItem {
    List<String> getActors();
    void setActors(List<String> actors);

    int getRuntimeInMinutes();
    void setRuntimeInMinutes(int runtime);
}