package lab_ISP;
public class Main {
    public static void main(String[] args) {

        IBorrowableDVD dvdBorrowable = new DVD();
        dvdBorrowable.checkOut("12345");

        IDVD dvd = new DVD();
        dvd.setTitle("Inception");

        IBorrowable borrowable = new DVD();
        borrowable.checkOut("54321");
    }
}