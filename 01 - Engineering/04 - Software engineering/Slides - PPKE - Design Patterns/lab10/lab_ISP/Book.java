﻿package lab_ISP;
import java.util.Date;

public class Book implements IBorrowableBook {
    private Date borrowDate;
    private int checkoutDurationInDays;
    private String author;
    private int pages;
    private String libraryId;
    private String title;

    @Override
    public Date getBorrowDate() {
        return borrowDate;
    }

    @Override
    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    @Override
    public int getCheckoutDurationInDays() {
        return checkoutDurationInDays;
    }

    @Override
    public void setCheckoutDurationInDays(int checkoutDurationInDays) {
        this.checkoutDurationInDays = checkoutDurationInDays;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public int getPages() {
        return pages;
    }

    @Override
    public void setPages(int pages) {
        this.pages = pages;
    }
    
    @Override
    public void checkIn() {
        System.out.println("Book checked in.");
    }

    @Override
    public void checkOut(String libraryId) {
        System.out.println("Book checked out with Library ID: " + libraryId);
    }

    @Override
    public Date getDueDate() {
        return new Date(borrowDate.getTime() + (long) checkoutDurationInDays * 24 * 60 * 60 * 1000);
    }
}
