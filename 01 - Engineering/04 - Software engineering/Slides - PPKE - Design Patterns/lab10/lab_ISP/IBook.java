package lab_ISP;

public interface IBook {
    String getAuthor();
    void setAuthor(String author);

    int getPages();
    void setPages(int pages);
}