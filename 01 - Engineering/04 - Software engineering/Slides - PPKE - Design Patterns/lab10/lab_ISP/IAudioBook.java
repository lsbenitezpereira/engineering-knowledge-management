package lab_ISP;

public interface IAudioBook extends ILibraryItem {
    int getRuntimeInMinutes();
    void setRuntimeInMinutes(int runtimeInMinutes);
}