package lab_OCP;

public class ManagerAccounts implements BaseAccounts {
    @Override
    public EmployeeModel create(BaseApplicantModel applicant) {
        EmployeeModel output = new EmployeeModel();
        output.setFirstname(applicant.getFirstname());
        output.setLastname(applicant.getLastname());
        output.setEmail(applicant.getFirstname().substring(0, 1) + applicant.getLastname() + "@ppke.com");
        output.setManager(true);
        return output;
    }
}