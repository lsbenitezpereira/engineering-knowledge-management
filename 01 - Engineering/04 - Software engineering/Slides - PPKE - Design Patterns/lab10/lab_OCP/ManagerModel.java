package lab_OCP;
 
public class ManagerModel implements BaseApplicantModel {
    private String firstname;
    private String lastname;
    private BaseAccounts accountProcessor = new ManagerAccounts();

    @Override
    public String getFirstname() {
        return firstname;
    }

    @Override
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public String getLastname() {
        return lastname;
    }

    @Override
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public BaseAccounts getAccountProcessor() {
        return accountProcessor;
    }

    @Override
    public void setAccountProcessor(BaseAccounts accountProcessor) {
        this.accountProcessor = accountProcessor;
    }
}
