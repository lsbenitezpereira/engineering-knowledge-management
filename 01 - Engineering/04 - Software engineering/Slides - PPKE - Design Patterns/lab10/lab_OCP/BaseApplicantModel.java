package lab_OCP;

public interface BaseApplicantModel {
    String getFirstname();
    void setFirstname(String firstname);

    String getLastname();
    void setLastname(String lastname);

    BaseAccounts getAccountProcessor();
    void setAccountProcessor(BaseAccounts accountProcessor);
}