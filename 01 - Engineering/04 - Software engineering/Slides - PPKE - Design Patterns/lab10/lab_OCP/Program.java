package lab_OCP;

import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        List<BaseApplicantModel> models = new ArrayList<>();
        models.add(new PersonModel() {{
            setFirstname("Mike");
            setLastname("Tyson");
        }});
        models.add(new ManagerModel() {{
            setFirstname("Jim");
            setLastname("Bill");
        }});
        models.add(new ExecutiveModel() {{
            setFirstname("Andras");
            setLastname("Csimma");
        }});

        List<EmployeeModel> employees = new ArrayList<>();
        Accounts accountManager = new Accounts();

        for (BaseApplicantModel person : models) {
            employees.add(accountManager.create(person));
        }

        for (EmployeeModel emp : employees) {
            System.out.println(emp.getFirstname() + " " + emp.getLastname() + ": " + emp.getEmail());
        }
    }
}
