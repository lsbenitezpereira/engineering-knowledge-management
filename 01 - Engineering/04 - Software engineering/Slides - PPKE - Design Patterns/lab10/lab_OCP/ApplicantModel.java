package lab_OCP;

public interface ApplicantModel {
    String getFirstname();
    void setFirstname(String firstname);

    String getLastname();
    void setLastname(String lastname);

    BaseAccounts getAccountProcessor();
    void setAccountProcessor(BaseAccounts accountProcessor);
}