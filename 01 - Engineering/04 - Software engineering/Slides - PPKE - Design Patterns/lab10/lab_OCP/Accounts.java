package lab_OCP;

public class Accounts implements BaseAccounts {
    @Override
    public EmployeeModel create(BaseApplicantModel person) {
        EmployeeModel output = new EmployeeModel();
        output.setFirstname(person.getFirstname());
        output.setLastname(person.getLastname());
        output.setEmail(person.getFirstname().substring(0, 1) + person.getLastname() + "@ppke.com");
        return output;
    }
}
