package lab_DIP;

public interface IEmailer {
    void sendEmail(IPerson person, String message);
}