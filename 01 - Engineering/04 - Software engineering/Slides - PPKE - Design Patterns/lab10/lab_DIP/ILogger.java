package lab_DIP;

public interface ILogger {
    void log(String message);
}