package lab_DIP;

public interface IChore {
    String getChoreName();
    void setChoreName(String choreName);

    double getHoursWorked();
    void setHoursWorked(double hoursWorked);

    boolean isComplete();
    void setComplete(boolean isComplete);

    IPerson getOwner();
    void setOwner(IPerson owner);

    void completeChore();
    void performedWork(double hours);
}
