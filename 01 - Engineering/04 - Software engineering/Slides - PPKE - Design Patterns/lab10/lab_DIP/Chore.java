﻿package lab_DIP;


public class Chore implements IChore {
    private final ILogger logger;
    private final IEmailer emailer;

    private String choreName;
    private IPerson owner;
    private double hoursWorked;
    private boolean isComplete;

    public Chore(ILogger logger, IEmailer emailer) {
        this.logger = logger;
        this.emailer = emailer;
    }

    @Override
    public String getChoreName() {
        return choreName;
    }

    @Override
    public void setChoreName(String choreName) {
        this.choreName = choreName;
    }

    @Override
    public IPerson getOwner() {
        return owner;
    }

    @Override
    public void setOwner(IPerson owner) {
        this.owner = owner;
    }

    @Override
    public double getHoursWorked() {
        return hoursWorked;
    }

    @Override
    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    @Override
    public boolean isComplete() {
        return isComplete;
    }

    @Override
    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    @Override
    public void performedWork(double hours) {
        hoursWorked += hours;
        logger.log("Performed work on " + choreName);
    }

    @Override
    public void completeChore() {
        isComplete = true;
        logger.log("Completed " + choreName);

        emailer.sendEmail(owner, "The chore " + choreName + " is complete.");
    }
}
