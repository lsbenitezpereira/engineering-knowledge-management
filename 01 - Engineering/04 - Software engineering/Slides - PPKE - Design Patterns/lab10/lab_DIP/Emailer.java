﻿package lab_DIP;

public class Emailer implements IEmailer {
    @Override
    public void sendEmail(IPerson person, String message) {
        System.out.println("Simulating sending an email to " + person.getEmail() + " with the message: " + message);
    }
}