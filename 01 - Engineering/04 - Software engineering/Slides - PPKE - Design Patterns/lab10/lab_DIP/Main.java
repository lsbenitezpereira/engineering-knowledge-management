package lab_DIP;

public class Main {
    public static void main(String[] args) {
        IPerson person = Factory.createPerson();
        person.setFirstName("John");
        person.setLastName("Doe");
        person.setEmail("J.Doe@ppke.com");
        person.setPhoneNumber("123456789");

        IChore chore = Factory.createChore();
        chore.setChoreName("Take out the trash");
        chore.setOwner(person);

        chore.performedWork(3);
        chore.performedWork(1.5);
        chore.completeChore();
    }
}