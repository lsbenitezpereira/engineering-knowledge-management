package lab_DIP;

public class Factory {
    public static IPerson createPerson() {
        return new Person();
    }

    public static ILogger createLogger() {
        return new Logger();
    }

    public static IEmailer createEmailer() {
        return new Emailer();
    }

    public static IChore createChore() {
        return new Chore(createLogger(), createEmailer());
    }
}
