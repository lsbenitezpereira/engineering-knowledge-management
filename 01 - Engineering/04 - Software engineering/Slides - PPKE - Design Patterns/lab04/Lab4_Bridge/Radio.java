package Lab4_Bridge;

public class Radio implements Device {

    public void on(){
		// You can imagine various set-up operations happening here
		System.out.println("Radio switching on");
	}
	public void off(){
		// You can imagine various shutdown operations happening here
		System.out.println("Radio switching off");
	}
	public void tuneChannel(int channel) {
		//This is the actual implementation of how you tune to a channel - include low level details
		System.out.println("Radio tuning to channel "+channel);
	}
    
}
