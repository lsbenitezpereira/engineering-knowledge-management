package Lab4_Bridge;

public class CleverRemote extends RemoteControl {
    public CleverRemote(Device d) {
        super(d);
    }

    public void channelUp() {
        this.setChannel(currentChannel+1);
    }

    public void channelDown() {
        this.setChannel(currentChannel-1);
    }
}
