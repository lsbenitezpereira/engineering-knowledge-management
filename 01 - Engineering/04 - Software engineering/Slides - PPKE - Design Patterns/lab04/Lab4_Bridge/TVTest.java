package Lab4_Bridge;

public class TVTest {
	public static void main(String[] args) {
		Device myDevice = new Sony(); //This could be put in a factory
		
		RemoteControl myremote = new RemoteControl(myDevice);
		
		//high level calls to the abstraction
		myremote.on();
		myremote.setChannel(8);
		myremote.off();

		//Now create a better remote control, which lets oyu change the channel up or down
		// without changing the device being controlled
		CleverRemote myremote2 = new CleverRemote(myDevice);
		myremote2.on();
		myremote2.setChannel(6);
		myremote2.channelDown();
		myremote2.channelUp();
		myremote2.off();


		// Now control a Radio!
		Device myRadio = new Radio();
		myremote2.setDevice(myRadio);
		myremote2.on();
		myremote2.setChannel(6);
		myremote2.channelDown();
		myremote2.channelUp();
		myremote2.off();

	}
}
