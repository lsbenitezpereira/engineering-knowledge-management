
public class ShoppingCartVisitorImplPrice implements ShoppingCartVisitor {
    int totalPrice = 0;
    @Override
    public void visitBook(Book book) {
        totalPrice += book.getPrice();
    }

    @Override
    public void visitFruit(Fruit fruit) {
        totalPrice += fruit.getWeight()*fruit.getPricePerKg();
    }

    public void total() {
        System.out.println("Total price: " + totalPrice);
    }
}
