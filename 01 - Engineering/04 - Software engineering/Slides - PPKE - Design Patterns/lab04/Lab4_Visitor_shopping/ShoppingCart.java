import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();
        cart.addProduct(new Book(20, "Kurt Vonnegut", "Slaughterhouse Five"));
        cart.addProduct(new Book(10, "George Orwell", "1984"));
        cart.addProduct(new Fruit(2, 5, "Banana"));
        cart.addProduct(new Fruit(1, 3, "Apple"));
        ShoppingCartVisitorImplDisplay v1 = new ShoppingCartVisitorImplDisplay();
        cart.accept(v1);
        ShoppingCartVisitorImplPrice v2 = new ShoppingCartVisitorImplPrice();
        cart.accept(v2);
        v2.total();
    }

    protected List<Item> items = new ArrayList<Item>();

    private void addProduct(Item i) {
        items.add(i);
    }

    public void accept(ShoppingCartVisitor visitor) {
        for (Item i : items) {
            i.accept(visitor);
        }
    }
}
