
public class Book implements Item {
    protected int price;
    protected String author;
    protected String title;

    public Book(int price, String author, String title) {
        this.price = price;
        this.author = author;
        this.title = title;    
    }

    public int getPrice() {
        return price;
    }
    public String getAuthor() {
        return author;
    }
    public String getTitle() {
        return title;
    }

    @Override
    public void accept(ShoppingCartVisitor visitor) {
        visitor.visitBook(this);
    }

}
