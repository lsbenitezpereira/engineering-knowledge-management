
public class ShoppingCartVisitorImplDisplay implements ShoppingCartVisitor {
    @Override
    public void visitBook(Book book) {
        System.out.println("Book name: " + book.getTitle() + ", Price: " + book.getPrice() + ", Author: " + book.getAuthor());
    }

    @Override
    public void visitFruit(Fruit fruit) {
        System.out.println("Fruit name: " + fruit.getName() + ", Price: " + fruit.getWeight()*fruit.getPricePerKg() + ", Weight: " + fruit.getWeight());
    }

}
