
public class Fruit implements Item {
    protected int pricePerKg;
    protected int weight;
    protected String name;
    public Fruit(int weight, int pricePerKg, String name) {
        this.pricePerKg = pricePerKg;
        this.weight = weight;
        this.name = name;
    }
    public int getPricePerKg() {
        return pricePerKg;
    }
    public int getWeight() {
        return weight;
    }
    public String getName() {
        return name;
    }
    @Override
    public void accept(ShoppingCartVisitor visitor) {
        visitor.visitFruit(this);
    }

}
