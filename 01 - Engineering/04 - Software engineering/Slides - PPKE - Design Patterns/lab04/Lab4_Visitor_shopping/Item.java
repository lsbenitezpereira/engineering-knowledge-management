
public interface Item {
    public void accept(ShoppingCartVisitor visitor);
}
