
public interface ShoppingCartVisitor {
    public void visitBook(Book book);
    public void visitFruit(Fruit fruit);
}
