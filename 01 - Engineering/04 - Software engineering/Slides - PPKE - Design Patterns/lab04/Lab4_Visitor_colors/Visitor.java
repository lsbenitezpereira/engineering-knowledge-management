package Lab4_Visitor_colors;

public interface Visitor {
    public void visitBlue(Blue b);
    public void visitRed(Red r);
}
