package Lab4_Visitor_colors;

public class Blue extends Color {

    @Override
    public void accept(Visitor v) {
        v.visitBlue(this);
    }

}
