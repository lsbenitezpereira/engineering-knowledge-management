package Lab4_Visitor_colors;

public class CountingVisitor implements Visitor {
    protected int count_red = 0;
    protected int count_blue = 0;

    @Override
    public void visitBlue(Blue b) {
        count_blue++;
    }

    @Override
    public void visitRed(Red r) {
        count_red++;
    }

    public void reportNum() {
        System.out.println("Count of red: "+count_red+" count of blue: "+ count_blue);
    }
    
}
