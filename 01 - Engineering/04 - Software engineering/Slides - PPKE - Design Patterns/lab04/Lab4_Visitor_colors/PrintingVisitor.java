package Lab4_Visitor_colors;

public class PrintingVisitor implements Visitor {

    @Override
    public void visitBlue(Blue b) {
        System.out.println("This is a Blue object");
    }

    @Override
    public void visitRed(Red r) {
        System.out.println("This is a Red object");
    }
    
}
