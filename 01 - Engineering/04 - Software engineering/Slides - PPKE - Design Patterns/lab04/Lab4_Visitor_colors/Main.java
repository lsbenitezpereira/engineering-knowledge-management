package Lab4_Visitor_colors;

public class Main {

    public static void main(String[] args) {
        Color set[] =
        { new Red(), new Blue(), new Blue(), new Red(), new Red() };
        Visitor v1 = new PrintingVisitor();
        CountingVisitor v2 = new CountingVisitor();
        for (Color c : set)
        {
            c.accept(v1);
            c.accept(v2);
        }
        v2.reportNum();
    }
}
