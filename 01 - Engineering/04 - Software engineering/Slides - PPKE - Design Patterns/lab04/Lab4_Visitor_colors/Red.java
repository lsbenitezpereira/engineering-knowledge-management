package Lab4_Visitor_colors;

public class Red extends Color {

    @Override
    public void accept(Visitor v) {
        v.visitRed(this);
    }
    
}
