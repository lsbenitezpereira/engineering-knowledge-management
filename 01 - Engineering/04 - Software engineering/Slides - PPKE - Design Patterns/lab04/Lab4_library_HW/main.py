from library import Library
from items import Book, DVD, Magazine
from flyweights import AuthorFactory, GenreFactory
from visitors import CatalogVisitorImplDisplay, CatalogVisitorImplSearch, CatalogVisitorImplFee


library = Library()
author_factory = AuthorFactory()
genre_factory = GenreFactory()
library.add_item(Book(
    "1984",
    author_factory.get_author("George Orwell"),
    1949,
    genre_factory.get_genre("Fiction"),
    "ok",
))
library.add_item(Book(
    "To Kill a Mockingbird",
    author_factory.get_author("Harper Lee"),
    1960,
    genre_factory.get_genre("Fiction"),
    "good",
))
library.add_item(Book(
    "Harry Potter and the Philosopher's Stone",
    author_factory.get_author("J.K. Rowling"),
    1997,
    genre_factory.get_genre("Fantasy"),
    "good",
))
library.add_item(Book(
    "The Great Gatsby",
    author_factory.get_author("F. Scott Fitzgerald"),
    1925,
    genre_factory.get_genre("Fiction"),
    "poor, missing pages",
))
library.add_item(DVD(
    "The Shawshank Redemption",
    author_factory.get_author("Frank Darabont"),
    1994,
    genre_factory.get_genre("Drama"),
    "a little scratched",
    142,
))
library.add_item(DVD(
    "The Godfather",
    author_factory.get_author("Francis Ford Coppola"),
    1972,
    genre_factory.get_genre("Drama"),
    "ok",
    175,
))
library.add_item(DVD(
    "The Dark Knight",
    author_factory.get_author("Christopher Nolan"),
    2008,
    genre_factory.get_genre("Action"),
    "good",
    152,
))
library.add_item(DVD(
    "12 monkeys",
    author_factory.get_author("Terry Gilliam"),
    1995,
    genre_factory.get_genre("Sci-Fi"),
    "good",
    129,
))
library.add_item(Magazine(
    "National Geographic",
    2020,
    genre_factory.get_genre("Science"),
    "pages missing",
))
library.add_item(Magazine(
    "Time",
    2020,
    genre_factory.get_genre("News"),
    "",
))
library.add_item(Magazine(
    "The Economist",
    2020,
    genre_factory.get_genre("News"),
    "good, really great, even shining",
))
library.add_item(Magazine(
    "Vogue",
    2020,
    genre_factory.get_genre("Fashion"),
    "ok",
))


print(f"The library has {len(library)} items.")
print('-'*20)
v1 = CatalogVisitorImplDisplay()
library.accept(v1)
print('-'*20)
v2 = CatalogVisitorImplSearch(genre_factory.get_genre("Fiction"))
library.accept(v2)

v3 = CatalogVisitorImplSearch(genre_factory.get_genre("News"))
library.accept(v3)
print('-'*20)
v4 = CatalogVisitorImplFee()
library.accept(v4)
