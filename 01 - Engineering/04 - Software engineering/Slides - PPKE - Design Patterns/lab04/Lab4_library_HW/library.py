from typing import List
from items import Item
from visitors import CatalogVisitor


class Library:
    '''
    High level container for items
    '''
    _catalog: List[Item] = []

    def add_item(self, item: Item) -> None:
        self._catalog.append(item)

    def accept(self, visitor: CatalogVisitor) -> None:
        for item in self._catalog:
            item.accept(visitor)

    def __len__(self):
        return len(self._catalog)
