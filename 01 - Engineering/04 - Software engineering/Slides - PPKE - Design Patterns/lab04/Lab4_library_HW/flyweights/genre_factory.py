from typing import Dict
from flyweights import Genre


class GenreFactory:
    _genres: Dict[str, Genre] = {}

    def get_genre(self, genre: str) -> Genre:
        if genre not in self._genres:
            self._genres[genre] = Genre(genre=genre)
        return self._genres[genre]
