class Genre:
    _genre: str

    def __init__(self, genre: str):
        self._genre = genre

    def get_genre(self) -> str:
        return self._genre

    def __str__(self) -> str:
        return self._genre
