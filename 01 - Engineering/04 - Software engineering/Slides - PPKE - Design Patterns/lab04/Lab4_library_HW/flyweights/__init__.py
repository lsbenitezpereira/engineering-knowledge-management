from flyweights.author import Author
from flyweights.author_factory import AuthorFactory
from flyweights.genre import Genre
from flyweights.genre_factory import GenreFactory
