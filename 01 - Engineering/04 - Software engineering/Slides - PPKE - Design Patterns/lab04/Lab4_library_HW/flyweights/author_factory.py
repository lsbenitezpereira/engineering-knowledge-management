from typing import Dict
from flyweights import Author


class AuthorFactory:
    _authors: Dict[str, Author] = {}

    def get_author(self, name: str) -> Author:
        if name not in self._authors:
            self._authors[name] = Author(name=name)
        return self._authors[name]
