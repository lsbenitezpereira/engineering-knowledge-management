from visitors import CatalogVisitor
import items
from flyweights import Genre


class CatalogVisitorImplSearch(CatalogVisitor):
    _genre: Genre

    def __init__(self, genre: Genre):
        self._genre = genre

    def visit_book(self, book: 'items.Book') -> None:
        if book.get_genre() == self._genre:
            print(f"Found a Book o genre {self._genre}: {book}")

    def visit_dvd(self, dvd: 'items.DVD') -> None:
        if dvd.get_genre() == self._genre:
            print(f"Found a DVD of genre {self._genre}: {dvd}")

    def visit_magazine(self, magazine: 'items.Magazine') -> None:
        if magazine.get_genre() == self._genre:
            print(f"Found a Magazine of genre {self._genre}: {magazine}")
