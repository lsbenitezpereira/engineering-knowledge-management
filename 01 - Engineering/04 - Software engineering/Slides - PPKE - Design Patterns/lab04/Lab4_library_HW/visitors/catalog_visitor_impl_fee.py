from visitors import CatalogVisitor
import items


# These fee equations are just to show the concept of the pattern
class CatalogVisitorImplFee(CatalogVisitor):
    def _classify_sentiment(self, text: str) -> int:
        '''
        Sentiment classification from text.
        @param text: what should be classified.
        @return score from 1 to 5, where 1 is negative (bad) and 5 is positive (good).
        '''
        positive_words = ['good', 'excellent', 'great', 'poor']
        negative_words = ['bad', 'terrible', 'scratched', 'missing', 'broken']
        text = text.replace(',', '').replace('.', '').replace('!', '').replace('?', '').lower()
        bag_of_words = set(text.split())

        score = 3
        for word in bag_of_words:
            if word in positive_words:
                score += 1
            elif word in negative_words:
                score -= 1
        if score < 1:
            score = 1
        elif score > 5:
            score = 5
        return score

    def visit_book(self, book: 'items.Book') -> None:
        fee: int = 100*len(book.get_title())
        print(f"You have to pay HUF {fee} for the book: {book}")

    def visit_dvd(self, dvd: 'items.DVD') -> None:
        fee: int = 10*dvd.get_duration()
        print(f"You have to pay HUF {fee} for the DVD: {dvd}")

    def visit_magazine(self, magazine: 'items.Magazine') -> None:
        condition_sentiment: int = self._classify_sentiment(magazine.get_condition())
        fee: int = 1000*condition_sentiment
        print(f"You have to pay HUF {fee} for the magazine: {magazine}")
