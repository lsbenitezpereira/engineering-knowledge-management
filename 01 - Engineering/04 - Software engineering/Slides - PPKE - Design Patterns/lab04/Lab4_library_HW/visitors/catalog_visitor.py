from abc import ABC, abstractmethod
import items


class CatalogVisitor(ABC):
    @abstractmethod
    def visit_book(self, book: 'items.Book') -> None:
        pass

    @abstractmethod
    def visit_dvd(self, dvd: 'items.DVD') -> None:
        pass

    @abstractmethod
    def visit_magazine(self, magazine: 'items.Magazine') -> None:
        pass
