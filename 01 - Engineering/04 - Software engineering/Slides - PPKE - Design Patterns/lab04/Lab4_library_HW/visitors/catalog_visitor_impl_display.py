from visitors import CatalogVisitor
import items


class CatalogVisitorImplDisplay(CatalogVisitor):
    def visit_book(self, book: 'items.Book') -> None:
        print(f"Found a Book: {book}")

    def visit_dvd(self, dvd: 'items.DVD') -> None:
        print(f"Found a DVD: {dvd}")

    def visit_magazine(self, magazine: 'items.Magazine') -> None:
        print(f"Found a Magazine: {magazine}")
