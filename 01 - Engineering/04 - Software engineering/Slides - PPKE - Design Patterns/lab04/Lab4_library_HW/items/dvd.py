from flyweights import Author, Genre
from items import Item
from visitors import CatalogVisitor


class DVD(Item):
    _author: Author
    _duration: int

    def __init__(self, title: str, author: Author, year: int, genre: Genre, condition: str, duration: int):
        super().__init__(title, year, genre, condition)
        self._author = author
        self._duration = duration

    def accept(self, visitor: CatalogVisitor) -> None:
        return visitor.visit_dvd(self)

    def get_author(self) -> Author:
        return self._author

    def play(self) -> None:
        print(f"Playing {self._title} for {self._duration} minutes...")

    def get_duration(self) -> int:
        return self._duration

    def __str__(self):
        return f"DVD {self._title} by {self._author.get_name()} ({self._year}), with duration {self._duration} minutes"
