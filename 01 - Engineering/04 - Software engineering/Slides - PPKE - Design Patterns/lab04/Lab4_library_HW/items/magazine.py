from items import Item
from visitors import CatalogVisitor


class Magazine(Item):
    def accept(self, visitor: CatalogVisitor) -> None:
        return visitor.visit_magazine(self)

    def roll(self) -> None:
        print(f"The magazine {self._title} is ready to be used as a weapon!")

    def __str__(self):
        return f"Magazine {self._title} ({self._year})"
