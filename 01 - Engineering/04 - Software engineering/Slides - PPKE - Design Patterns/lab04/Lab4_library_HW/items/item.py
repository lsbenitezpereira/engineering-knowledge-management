from abc import ABC, abstractmethod
from flyweights import Genre
from visitors import CatalogVisitor


class Item(ABC):
    _title: str
    _year: int
    _genre: Genre
    _condition: str

    def __init__(self, title: str, year: int, genre: Genre, condition: str):
        self._title = title
        self._year = year
        self._genre = genre
        self._condition = condition

    @abstractmethod
    def accept(self, visitor: CatalogVisitor) -> None:
        pass

    def get_title(self) -> str:
        return self._title

    def get_year(self) -> int:
        return self._year

    def get_genre(self) -> Genre:
        return self._genre

    def get_condition(self) -> str:
        return self._condition
