from items import Item
from flyweights import Author, Genre
from visitors import CatalogVisitor


class Book(Item):
    _author: Author

    def __init__(self, title: str, author: Author, year: int, genre: Genre, condition: str):
        super().__init__(title, year, genre, condition)
        self._author = author

    def accept(self, visitor: CatalogVisitor) -> None:
        visitor.visit_book(self)

    def get_author(self) -> Author:
        return self._author

    def __str__(self):
        return f"Book {self._title} by {self._author.get_name()} ({self._year})"
