package Lab4_Builder;

public class BuilderDemo {
    public static void main(String[] args) {
        // Construct a car
        CarBuilder carBuilder = new CarBuilder(); //User-defined concrete builder
        VehicleDirector carDirector = new VehicleDirector(carBuilder); //Same director, different builder
        carDirector.construct();
        Car car = carBuilder.getProduct();
        System.out.println(car);
        //Products can have different methods, they are not even the same type
        System.out.println(car.transportPeople());

        // Construct a motorcycle
        MotorcycleBuilder motorcycleBuilder = new MotorcycleBuilder(); //User-defined concrete builder
        VehicleDirector motorcycleDirector = new VehicleDirector(motorcycleBuilder); //Same director, different builder
        motorcycleDirector.construct();
        Motorcycle motorcycle = motorcycleBuilder.getProduct();
        System.out.println(motorcycle);
        //Products can have different methods, they are not even the same type
        System.out.println(motorcycle.wroom());
    }
}
