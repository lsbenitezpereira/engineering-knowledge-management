package Lab4_Builder;

public class Car {
    private String carType;
    private int numSeats;
    private String engine;
    private boolean hasGPS;

    public void setCarType(String carType) {
        this.carType = carType;
    }
    public void setNumSeats(int numSeats) {
        this.numSeats = numSeats;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setHasGPS(boolean hasGPS) {
        this.hasGPS = hasGPS;
    }

    @Override
    public String toString() {
        return "Car Type: " + carType + ", Seats: " + numSeats + ", Engine: " + engine + ", GPS: " + hasGPS;
    }

    public String transportPeople() {
        return "Transporting people...";
    }
}
