package Lab4_Builder;

public class Motorcycle {
    private String motorcycleType;
    private int cylinderCapacity;
    private boolean hasGPS = false;
    private String engine;

    public Motorcycle(int cylinderCapacity) {
        this.cylinderCapacity = cylinderCapacity;
    }

    public void setMotorcycleType(String motorcycleType) {
        this.motorcycleType = motorcycleType;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setHasGPS(boolean hasGPS) {
        this.hasGPS = hasGPS;
    }

    @Override
    public String toString() {
        return "Motorcycle Type: " + motorcycleType + ", engine type " + engine + ", Cylinder Capacity: " + cylinderCapacity + ", GPS: " + hasGPS;
    }

    public String wroom() {
        //Depending on the cylinder capacity, the motorcycle will make a different sound
        if (cylinderCapacity <= 50) {
            return "Wroom!";
        } else if (cylinderCapacity <= 100) {
            return "Wrooom!";
        } else if (cylinderCapacity <= 150) {
            return "Wroooom!";
        } else {
            return "Wroooooom!";
        }
    }
}
