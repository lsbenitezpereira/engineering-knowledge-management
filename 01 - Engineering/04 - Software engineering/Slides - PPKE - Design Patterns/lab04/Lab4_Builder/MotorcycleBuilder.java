package Lab4_Builder;

public class MotorcycleBuilder implements VehicleBuilder {
    Motorcycle m = new Motorcycle(1000);
    @Override
    public void buildVehicleType() {
        m.setMotorcycleType("Kawasaki");
    }

    @Override
    public void buildSeats() {

    }

    @Override
    public void addEngine(String engine) {
        m.setEngine(engine+" engine customized for Kawasaki");
    }

    @Override
    public void addGPS(boolean hasGPS) {
        m.setHasGPS(hasGPS);
    }

    public Motorcycle getProduct() {
        return m;
    }

}
