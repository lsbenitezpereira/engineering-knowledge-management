package Lab4_Builder;

public class VehicleDirector {
    VehicleBuilder b;
    public VehicleDirector(VehicleBuilder carBuilder) {
        this.b = carBuilder;
    }

    public void construct() {
        b.buildVehicleType();
        b.buildSeats();
        b.addEngine("Petrol");
        b.addGPS(false);
    }

}
