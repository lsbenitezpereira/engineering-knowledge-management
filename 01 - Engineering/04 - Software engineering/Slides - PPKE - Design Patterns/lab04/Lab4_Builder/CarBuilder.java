package Lab4_Builder;

public class CarBuilder implements VehicleBuilder {
    Car c = new Car();
    @Override
    public void buildVehicleType() {
        c.setCarType("Toyota");
    }

    @Override
    public void buildSeats() {
        c.setNumSeats(5);
    }

    @Override
    public void addEngine(String engine) {
        c.setEngine(engine+" engine customized for Toyota car");
    }

    @Override
    public void addGPS(boolean hasGPS) {
        c.setHasGPS(hasGPS);
    }

    public Car getProduct() {
        return c;
    }

}
