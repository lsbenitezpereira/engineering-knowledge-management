package Lab4_Builder;

public interface VehicleBuilder {
    void buildVehicleType();
    void buildSeats();
    void addEngine(String engine);
    void addGPS(boolean hasGPS);
}
