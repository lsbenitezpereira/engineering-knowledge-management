import java.util.ArrayList;

public class TextRenderer {
    protected ArrayList<Glyph> list =  new ArrayList<>();
    protected CharacterFactory fac = new CharacterFactory();
    public void addCharacter(char c, String font) {
        list.add(fac.getCharacter(c, font));
    }
    public void render() {
        for (int i = 0; i < list.size(); i++) {
            int row = i / 10;
            int col = i % 10;
            list.get(i).draw(row, col);
        }
    }
    public static void main(String[] args) {
        TextRenderer textRenderer = new TextRenderer();
        String text = "Hello, world! This is a sample longer text.";
        for (char c : text.toCharArray()) {
            // Add character with font "Arial". Position should be calculated by textRenderer, based on the order of characters in the text.
            textRenderer.addCharacter(c, "Arial");
        }
        String text2 = "Welcome to the world of design patterns!";
        for (char c : text2.toCharArray()) {
            textRenderer.addCharacter(c, "Times New Roman");
        }
        textRenderer.render();
    }
}
