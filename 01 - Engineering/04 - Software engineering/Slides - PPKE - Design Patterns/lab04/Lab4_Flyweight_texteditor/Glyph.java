public interface Glyph {
    void draw(int x, int y);
}
