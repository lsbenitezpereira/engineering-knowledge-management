public class CharacterGlyph implements Glyph {
    char character;
    String font;
    public CharacterGlyph(char c, String font) {
        this.character = c;
        this.font = font;
    }

    @Override
    public void draw(int x, int y) {
        System.out.println("Drawing letter "+character+" with font "+font+" at coords "+x + ", "+y);    
    }
    
}
