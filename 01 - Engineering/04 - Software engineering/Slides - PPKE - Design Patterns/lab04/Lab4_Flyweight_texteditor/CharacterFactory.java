import java.util.HashMap;
import java.util.Map;

public class CharacterFactory {
    // protected Map<String, Glyph> charlist = new HashMap<String,Glyph>();
    protected Map<String, Map<String, Glyph> > charmap;
    public CharacterFactory() {
        this.charmap = new HashMap<String, Map<String, Glyph> >();
    }

    public Glyph getCharacter(char c, String font) {
        if (!this.charmap.containsKey(font)) {
            this.charmap.put(font, new HashMap<String, Glyph>());
        }
        Character c2 = Character.valueOf(c);
        if (this.charmap.get(font).containsKey(c2.toString())) {
            return this.charmap.get(font).get(c2.toString());
        } else {
            this.charmap.get(font).put(c2.toString(), new CharacterGlyph(c, font));
            return this.charmap.get(font).get(c2.toString());
        }
    }
}
