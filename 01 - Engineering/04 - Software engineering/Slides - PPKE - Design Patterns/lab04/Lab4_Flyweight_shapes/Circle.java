public class Circle implements Shape {
    String name;
    String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Circle(String color){
        this.name = "circle";
        this.color = color;
    }

    @Override
    public void draw(int x, int y){
        System.out.println("Drawing "+color+" "+name+" at coordinates ("+x+", "+y+")");
    }
}
