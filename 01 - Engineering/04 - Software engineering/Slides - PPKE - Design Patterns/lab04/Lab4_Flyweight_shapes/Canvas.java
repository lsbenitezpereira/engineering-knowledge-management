import java.util.ArrayList;

// Client code
public class Canvas {
    protected ArrayList<Shape> shapes = new ArrayList<>();
    protected ArrayList<Integer> x_coord = new ArrayList<>();
    protected ArrayList<Integer> y_coord = new ArrayList<>();
    protected ShapeFactory factory = new ShapeFactory();

    public void addCircle(String color, int x, int y){
        Shape shape = factory.getShape("circle", color);
        shapes.add(shape);
        x_coord.add(x);
        y_coord.add(y);
    }

    public void addSquare(String color, int x, int y){
        Shape shape = factory.getShape("square", color);
        shapes.add(shape);
        x_coord.add(x);
        y_coord.add(y);
    }

    public void draw(){
        for (int i = 0; i < shapes.size(); i++){
            shapes.get(i).draw(x_coord.get(i), y_coord.get(i));
        }
    }

    public static void main(String[] args) {
        Canvas canvas = new Canvas();
        canvas.addCircle("Red", 10, 20);
        canvas.addCircle("Red", 30, 40);
        canvas.addSquare("Blue", 50, 60);
        canvas.addSquare("Blue", 70, 80);

        canvas.draw();
    }
}