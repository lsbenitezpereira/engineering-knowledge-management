import java.util.ArrayList;

public class ShapeFactory {
    ArrayList<Shape> flyweights = new ArrayList<>();
    public Shape getShape(String name, String color){
        for (Shape s: flyweights){
            if (s.getColor().equals(color) && s.getName().equals(name)){
                return s;
            }
        }
    
        Shape item;
        if (name.equals("square")){
            item = new Square(color);
        } else if (name.equals("circle")){
            item = new Circle(color);
        } else {
            throw new NullPointerException("lala");
        }
        flyweights.add(item);
        return item;
    }
}
