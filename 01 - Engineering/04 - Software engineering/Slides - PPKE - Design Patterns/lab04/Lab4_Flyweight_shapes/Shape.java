public interface Shape {
    public String getName();

    public void setName(String name);

    public String getColor();

    public void setColor(String color);
    public void draw(int x, int y);
}
