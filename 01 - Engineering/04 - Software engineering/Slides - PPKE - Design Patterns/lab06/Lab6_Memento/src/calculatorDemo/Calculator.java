package calculatorDemo;

public class Calculator {
    protected Double value = 0.0;

    public Calculator(Double value) {
        this.value = value;
    }

    public void multiply(Double x) {
        value = value*x;
    }

    public Double getValue() {
        return value;
    }

    public CalculatorMemento createMemento() {
        return new CalculatorMemento(value);
    }

    public void restore(CalculatorMemento m){
        value = m.getValue();
    }    
}
