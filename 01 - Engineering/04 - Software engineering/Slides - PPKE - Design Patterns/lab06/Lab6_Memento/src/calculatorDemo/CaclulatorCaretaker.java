package calculatorDemo;
import java.util.Stack;

public class CaclulatorCaretaker {
    protected Stack<CalculatorMemento> mementos = new Stack<CalculatorMemento>();
    public void save(CalculatorMemento memento) {
        mementos.add(memento);
    }

    public CalculatorMemento undo() {
        return mementos.pop();
    }

}
