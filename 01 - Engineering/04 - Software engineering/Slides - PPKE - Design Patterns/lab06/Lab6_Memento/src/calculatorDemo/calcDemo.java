package calculatorDemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class calcDemo {
    public static void main(String[] args) {
        Calculator c = new Calculator(10.0);
        CaclulatorCaretaker h = new CaclulatorCaretaker();
        h.save(c.createMemento());
        Scanner sc = new Scanner(System.in);
        String input = "";
        while (!input.equals("exit")) {
            System.out.println("Enter an operation (double, halve, undo): ");
            input = sc.nextLine();
            if (input.equals("undo")) {
                c.restore(h.undo());
            } else if (input.equals("double")) {
                h.save(c.createMemento());
                c.multiply(2.0);
            } else if (input.equals("halve")) {
                h.save(c.createMemento());
                c.multiply(0.5);
            }
            
            System.out.println("Result: " + c.getValue());
        }

    }
}
