package calculatorDemo;

public class CalculatorMemento {
    protected Double value;

    public CalculatorMemento(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
