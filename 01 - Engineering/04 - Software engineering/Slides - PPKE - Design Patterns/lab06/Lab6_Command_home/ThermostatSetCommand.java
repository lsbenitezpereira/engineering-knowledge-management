package Lab6_Command_home;

public class ThermostatSetCommand implements Command {
    Thermostat thermostat;
    int temperature;
    public ThermostatSetCommand(Thermostat thermostat, int i) {
        this.temperature = i;
        this.thermostat = thermostat;
    }
    @Override
    public void execute() {
        this.thermostat.setTemperature(this.temperature);
    }


}
