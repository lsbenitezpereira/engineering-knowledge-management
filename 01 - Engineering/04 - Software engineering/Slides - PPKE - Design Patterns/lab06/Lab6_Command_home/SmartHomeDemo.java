package Lab6_Command_home;

public class SmartHomeDemo {
    public static void main(String[] args) {
        // Create devices
        Light livingRoomLight = new Light();
        Thermostat thermostat = new Thermostat();
        MusicPlayer musicPlayer = new MusicPlayer();

        // Create commands
        Command lightOn = new LightOnCommand(livingRoomLight);
        Command lightOff = new LightOffCommand(livingRoomLight);
        Command thermostatSet = new ThermostatSetCommand(thermostat, 22);
        Command musicPlay = new MusicPlayCommand(musicPlayer);
        Command musicPause = new MusicPauseCommand(musicPlayer);

        Command arrivingAtHome = new ArriveCommand(livingRoomLight, thermostat, musicPlayer);

        // Create remote control
        RemoteControl remote = new RemoteControl();
        // Set up remote control buttons
        remote.addButton("Light On", lightOn);
        remote.addButton("Light Off", lightOff);
        remote.addButton("Thermostat Set", thermostatSet);
        remote.addButton("Music Play", musicPlay);
        remote.addButton("Music Pause", musicPause);
        remote.addButton("Arriving Home", arrivingAtHome);

        // Press buttons
        remote.pressButton("Light On");
        remote.pressButton("Thermostat Set");
        remote.pressButton("Music Play");
        remote.pressButton("Music Pause");
        remote.pressButton("Light Off");

        remote.pressButton("Arriving Home");
    }
}