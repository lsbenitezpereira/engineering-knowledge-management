package Lab6_Command_home;

import java.util.HashMap;
import java.util.Map;

public class RemoteControl {
    protected Map<String, Command> buttons = new HashMap<>();
    public void addButton(String label, Command c) {
        buttons.put(label, c);
    }
    public void pressButton(String label) {
        if (buttons.containsKey(label)) {
            Command c = buttons.get(label);
            c.execute();
        } else {
            System.out.println("No such button");
        }
    }
}
