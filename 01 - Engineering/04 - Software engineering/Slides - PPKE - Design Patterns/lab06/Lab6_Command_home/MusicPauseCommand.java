package Lab6_Command_home;

public class MusicPauseCommand implements Command {
    MusicPlayer p;
    public MusicPauseCommand(MusicPlayer musicPlayer) {
        this.p = musicPlayer;
    }
    @Override
    public void execute() {
        this.p.pause();
    }

}
