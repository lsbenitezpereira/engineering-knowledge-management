package Lab6_Command_home;

public class LightOffCommand implements Command {
    Light light;
    public LightOffCommand(Light l) {
        light = l;
    }
    @Override
    public void execute() {
        light.switchOff();
    }

}
