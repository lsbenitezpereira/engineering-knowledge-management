package Lab6_Command_home;

public interface Command {
    public void execute();
}
