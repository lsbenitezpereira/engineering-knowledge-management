package Lab6_Command_home;

public class MusicPlayer {
    public void play() {
        System.out.println("Music player started");
    }
    public void pause() {
        System.out.println("Music player paused");
    }
    
}
