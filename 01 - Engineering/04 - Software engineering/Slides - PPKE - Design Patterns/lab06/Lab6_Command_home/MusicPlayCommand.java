package Lab6_Command_home;

public class MusicPlayCommand implements Command {
    MusicPlayer p;
    public MusicPlayCommand(MusicPlayer musicPlayer) {
        this.p = musicPlayer;
    }
    @Override
    public void execute() {
        this.p.play();
    }

}
