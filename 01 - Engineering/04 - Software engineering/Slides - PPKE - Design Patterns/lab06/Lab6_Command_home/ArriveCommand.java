package Lab6_Command_home;

public class ArriveCommand implements Command {
    Light light;
    Thermostat t;
    MusicPlayer p;
    public ArriveCommand(Light livingRoomLight, Thermostat thermostat, MusicPlayer musicPlayer) {
        this.light = livingRoomLight;
        this.t = thermostat;
        this.p = musicPlayer;
    }
    @Override
    public void execute() {
        this.light.switchOn();;
        this.p.play();
        this.t.setTemperature(22);
    }
}
