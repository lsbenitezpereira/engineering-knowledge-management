package Lab6_Memento_editor;

import Lab6_Memento_editor.texteditor.*;

public class SimpleTextEditorDemo {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        TextEditorHistory history = new TextEditorHistory();

        // Add some text and save the state
        editor.addText("Hello, ");
        history.save(editor.getMemento());
        System.out.println(editor);

        // Add more text and save the state
        editor.addText("world!");
        history.save(editor.getMemento());
        System.out.println(editor);

        // Add more text and save the state
        editor.addText(" How are you?");
        history.save(editor.getMemento());
        System.out.println(editor);

        // Undo the last addition
        TextEditorMemento undoMemento = history.undo();
        if (undoMemento != null) {
            editor.restore(undoMemento);
        }
        System.out.println("After first undo: " + editor);

        // Undo again
        undoMemento = history.undo();
        if (undoMemento != null) {
            editor.restore(undoMemento);
        }
        System.out.println("After second undo: " + editor);

        // Add new text
        editor.addText("Memento pattern!");
        System.out.println(editor);
    }
}