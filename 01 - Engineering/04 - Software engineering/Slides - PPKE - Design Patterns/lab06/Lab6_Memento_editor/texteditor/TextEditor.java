package Lab6_Memento_editor.texteditor;

public class TextEditor {
    String currentText = "";
    public void addText(String text){
        currentText = currentText + text;
    }

    public TextEditorMemento getMemento(){
        TextEditorMemento mem = new TextEditorMemento();
        mem.setText(currentText);
        return mem;
    }

    @Override
    public String toString(){
        return currentText;
    }
}
