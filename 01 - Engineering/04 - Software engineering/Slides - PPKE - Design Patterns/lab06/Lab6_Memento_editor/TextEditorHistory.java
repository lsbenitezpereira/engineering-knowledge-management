package Lab6_Memento_editor;

import java.util.Stack;

import Lab6_Memento_editor.texteditor.TextEditorMemento;

public class TextEditorHistory {
    protected Stack<TextEditorMemento> mementos = new Stack<TextEditorMemento>();
    public void save(TextEditorMemento memento) {
        mementos.add(memento);
    }

    public TextEditorMemento undo() {
        return mementos.pop();
    }

}
