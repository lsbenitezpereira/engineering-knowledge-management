import os
import logging
from abc import ABC, abstractmethod
from user import User, Role


class Document(ABC):
    _name: str

    @abstractmethod
    def view(self, user: User) -> str:
        pass

    @abstractmethod
    def edit(self, user: User, content: str) -> None:
        pass

    @abstractmethod
    def delete(self, user: User) -> None:
        pass


class DocumentReal(Document):
    def __init__(self, name: str) -> None:
        self._name = name

    def view(self, user: User) -> str:
        with open(self._name, 'r') as file:
            content = file.read()
        logging.info(f"Content of {self._name}: {content} (viwed by {user.getRole()})")
        return content

    def edit(self, user: User, content: str) -> None:
        with open(self._name, 'w') as file:
            file.write(content)
        logging.info(f"Edited {self._name} by {user.getRole()}.")

    def delete(self, user: User) -> None:
        os.remove(self._name)
        logging.info(f"Deleted {self._name} by {user.getRole()}.")


class DocumentProxy(Document):
    _real_document: DocumentReal

    def __init__(self, name: str) -> None:
        self._name = name
        self._real_document = DocumentReal(name)

    def view(self, user: User) -> str:
        logging.debug(f"{user.getRole()} is viewing {self._real_document._name}.")
        return self._real_document.view(user)

    def edit(self, user: User, content: str) -> None:
        if user.getRole() in [Role.admin, Role.editor]:
            logging.debug(f"{user.getRole()} is editing {self._real_document._name}.")
            self._real_document.edit(user, content)
        else:
            logging.error(f"{user.getRole()} is not authorized to edit {self._real_document._name}.")  # Error

    def delete(self, user: User) -> None:
        if user.getRole() == Role.admin:
            logging.debug(f"{user.getRole()} is deleting {self._real_document._name}.")
            self._real_document.delete(user)
        else:
            logging.error(f"{user.getRole()} is not authorized to delete {self._real_document._name}.")  # Error
