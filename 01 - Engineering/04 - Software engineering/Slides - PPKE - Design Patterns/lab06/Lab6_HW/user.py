from enum import Enum


class Role(Enum):
    admin = "admin"
    editor = "editor"
    viewer = "viewer"


class User:
    _name: str
    _role: Role

    def __init__(self, name: str, role: Role) -> None:
        self._name = name
        self._role = role

    def getRole(self) -> Role:
        return self._role

    def getName(self) -> str:
        return self._name
