from abc import ABC, abstractmethod
from typing import Optional
from document import Document
from user import User
import logging
import os


# TODO: the "redo" functionality was kinda an afterthought, so it's badly implemented
# Since the actions only print the error, instead of returning if the action suceeded, then undoing unsucesseful actions may not work as expected
# If the Proxy returned a boolean value, then the undo would be able to check if the action was successful or not
# Well... redo functionality was not in the specification =D

class Command(ABC):
    _user: User
    _document: Document

    def __init__(self, document: Document, user: User) -> None:
        self._document = document
        self._user = user

    @abstractmethod
    def execute(self) -> None:
        pass

    @abstractmethod
    def undo(self) -> None:
        pass


class CommandDocumentCreate(Command):
    def execute(self) -> None:
        if os.path.exists(self._document._name):
            logging.error(f"Document {self._document._name} already exists.")
            return
        self._document.edit(self._user, "")
        logging.debug(f"Created document {self._document._name} by {self._user.getRole()}.")

    def undo(self) -> None:
        if os.path.exists(self._document._name):
            os.remove(self._document._name)
            logging.debug(f"Undid create document {self._document._name}.")
        else:
            logging.error("Creation can not be undone.")


class CommandDocumentView(Command):
    def execute(self) -> None:
        self._document.view(self._user)

    def undo(self) -> None:
        # Nothing to undo
        pass


class CommandDocumentEdit(Command):
    _content: str
    _previous_content: Optional[str] = None

    def __init__(self, document: Document, user: User, content: str) -> None:
        super().__init__(document, user)
        self._content = content

    def execute(self) -> None:
        self._previous_content = self._document.view(self._user)
        self._document.edit(self._user, self._content)

    def undo(self) -> None:
        if self._previous_content is not None:
            self._document.edit(self._user, self._previous_content)
            logging.debug(f"Undid edit action for {self._document._name}.")
        else:
            logging.error("Edit can not be undone.")


class CommandDocumentDelete(Command):
    _previous_content: Optional[str] = None

    def execute(self) -> None:
        self._previous_content = self._document.view(self._user)
        self._document.delete(self._user)

    def undo(self) -> None:
        if self._previous_content is not None:
            self._document.edit(self._user, self._previous_content)
            logging.debug(f"Undid delete action for {self._document._name}.")
        else:
            logging.error("Deletion can not be undone.")
