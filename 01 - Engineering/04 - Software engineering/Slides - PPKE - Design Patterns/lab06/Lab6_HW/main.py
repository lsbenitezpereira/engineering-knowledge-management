import logging
from command import CommandDocumentCreate, CommandDocumentView, CommandDocumentEdit, CommandDocumentDelete
from document import DocumentProxy
from user import User, Role
from executor import CommandExecutor

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    executor = CommandExecutor()

    alice = User("Alice", Role.admin)
    bob = User("Bob", Role.editor)
    bobyJunior = User("BobyJunior", Role.viewer)

    document1 = DocumentProxy("doc1.txt")
    document2 = DocumentProxy("doc2.txt")

    command_create_doc1 = CommandDocumentCreate(document1, alice)
    executor.execute(command_create_doc1)

    command_create_doc2 = CommandDocumentCreate(document2, alice)
    executor.execute(command_create_doc2)

    command_view_doc1 = CommandDocumentView(document1, bobyJunior)
    executor.execute(command_view_doc1)

    command_edit_doc1 = CommandDocumentEdit(document1, bob, "Hello")
    executor.execute(command_edit_doc1)

    print('|A|'+'-'*30)

    command_view_doc1_after_edit = CommandDocumentView(document1, bobyJunior)
    executor.execute(command_view_doc1_after_edit)

    command_edit_doc1_by_viewer = CommandDocumentEdit(document1, bobyJunior, "Let me overwrite something here, lol")  # Error
    executor.execute(command_edit_doc1_by_viewer)
    print('|B|'+'-'*30)

    command_delete_doc1 = CommandDocumentDelete(document1, bob)
    executor.execute(command_delete_doc1)  # Error

    command_delete_doc2_by_viewer = CommandDocumentDelete(document2, bobyJunior)  # Error
    executor.execute(command_delete_doc2_by_viewer)

    command_delete_doc1 = CommandDocumentDelete(document1, alice)
    executor.execute(command_delete_doc1)

    command_delete_doc2 = CommandDocumentDelete(document2, alice)
    executor.execute(command_delete_doc2)

    print('|C|'+'-'*30)
    executor.undo_last_command()  # Undo delete doc2
    executor.undo_last_command()  # Undo delete doc1
    executor.undo_last_command()  # Undo failed delete doc2
    executor.undo_last_command()  # Undo delete doc1

    executor.undo_last_command()  # Undo edit doc1
    executor.undo_last_command()  # Undo view doc1 after edit

    executor.undo_last_command()  # Undo edit doc1
    executor.undo_last_command()  # Undo view doc1
    executor.undo_last_command()  # Undo create doc2
    executor.undo_last_command()  # Undo create doc1

    executor.undo_last_command()  # Error
