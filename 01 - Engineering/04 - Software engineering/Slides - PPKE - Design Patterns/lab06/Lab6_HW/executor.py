import logging
from typing import List
from command import Command


class CommandExecutor:
    _history: List[Command]

    def __init__(self) -> None:
        self._history = []

    def execute(self, command: Command) -> None:
        command.execute()
        self._history.append(command)

    def undo_last_command(self) -> None:
        if not self._history:
            logging.error("No commands to undo.")
            return
        command = self._history.pop()
        command.undo()
