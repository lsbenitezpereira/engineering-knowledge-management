package proxy;

public class FileImage implements Image {

    private String fileName;

    public FileImage(String name) {
        this.fileName = name;
        loadFromDisk(fileName);
    }

    private void loadFromDisk(String fileName2) {
        System.out.println("Loading " + fileName2);
        //Wait for 2 seconds
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted");
        }
    }

    @Override
    public void display() {
        System.out.println("Displaying image from file");
    }
    
}
