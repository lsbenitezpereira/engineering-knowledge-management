package proxy;

import java.util.ArrayList;
import java.util.List;

public class ImageBrowser {
    public static void main(String[] args) {
        List<Image> images = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            images.add(new ImageProxy("test_large"+i+".jpg"));
        }
        System.out.println("Images loaded");

        //image will be loaded from disk
        for (Image image : images) {
            image.display();
        }
    }
}
