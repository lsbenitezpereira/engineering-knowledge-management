package proxy;

public class ImageProxy implements Image{
    protected String path;
    protected FileImage realImage;
    public ImageProxy(String path) {
        this.path = path;
    }

    @Override
    public void display(){
        if (realImage == null){
            realImage = new FileImage(path);
        }
        realImage.display();
    }
    
    
}
