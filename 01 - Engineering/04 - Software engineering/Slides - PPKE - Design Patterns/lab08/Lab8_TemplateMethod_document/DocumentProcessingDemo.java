package Lab8_TemplateMethod_document;

class DocumentProcessingDemo {
    public static void main(String[] args) {
        System.out.println("Processing Financial Report:");
        DocumentProcessor financialReport = new FinancialReportProcessor();
        financialReport.processDocument();
        
        System.out.println("\nProcessing Legal Contract:");
        DocumentProcessor legalContract = new LegalContractProcessor();
        legalContract.processDocument();
    }
}