class CoffeeShopDemo {
    public static void main(String[] args) {
        // Order 1: Coffee with milk and whipped cream
        System.out.println("Preparing Order 1:");
        Beverage beverage1 = new Coffee();
        beverage1 = new Milk(beverage1);
        beverage1 = new WhippedCream(beverage1);
        
        beverage1.prepareBeverage();
        System.out.println("\nFinal Order: " + beverage1.getDescription());
        System.out.println("Total Cost: $" + String.format("%.2f", beverage1.cost()));
        
        // Order 2: Tea with caramel
        System.out.println("\nPreparing Order 2:");
        Beverage beverage2 = new Tea();
        beverage2 = new Caramel(beverage2);
        
        beverage2.prepareBeverage();
        System.out.println("\nFinal Order: " + beverage2.getDescription());
        System.out.println("Total Cost: $" + String.format("%.2f", beverage2.cost()));
    }
}