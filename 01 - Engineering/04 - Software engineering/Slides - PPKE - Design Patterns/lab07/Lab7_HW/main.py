import logging
from approvers import ApproverJunior, ApproverSenior, ApproverManager, ApproverDirector, ApproverNull
from document import Document


logging.basicConfig(level=logging.DEBUG)

# Initialize approvers
junior = ApproverJunior("Bob")
senior = ApproverSenior("Alice")
manager = ApproverManager("Charlie")
director = ApproverDirector("Diana")
nullApprover = ApproverNull.getInstance()

# Set up chain of responsibility
junior.setSuccessor(senior)
senior.setSuccessor(manager)
manager.setSuccessor(director)
director.setSuccessor(nullApprover)

# Create documents of varying importance levels
doc1 = Document(title="Doc Level 1", content="Routine Document, lol", importanceLevel=1)
doc2 = Document(title="Doc Level 2", content="Moderate Document, do not wipe your *** with it", importanceLevel=2)
doc3 = Document(title="Doc Level 3", content="Important Document, handle this stuff with care", importanceLevel=3)
doc4 = Document(title="Doc Level 4", content="Critical Document, heads will roll", importanceLevel=4)
doc5 = Document(title="Not for the commoner's eyes", content="Top Secret Document, ui ui ui", importanceLevel=5)

# Process documents to showcase the approval chain
logging.debug("Processing Doc Level 1:")
junior.approve(doc1)

logging.debug("Processing Doc Level 2:")
junior.approve(doc2)

logging.debug("Processing Doc Level 3:")
junior.approve(doc3)

logging.debug("Processing Doc Level 4:")
junior.approve(doc4)

logging.debug("Processing Doc Level 5 (should fail approval):")
junior.approve(doc5)

# Invalid importance levels
try:
    logging.debug("Attempting to create document with invalid importance level (0):")
    invalidDoc1 = Document(title="Invalid Doc", content="Should Fail", importanceLevel=0)
    junior.approve(invalidDoc1)
except ValueError as e:
    logging.error(f"Failed to create document with level 0: {e}")

try:
    logging.debug("Attempting to create document with invalid importance level (6):")
    invalidDoc2 = Document(title="Invalid Doc", content="Should Fail", importanceLevel=6)
    junior.approve(invalidDoc2)
except ValueError as e:
    logging.error(f"Failed to create document with level 6: {e}")
