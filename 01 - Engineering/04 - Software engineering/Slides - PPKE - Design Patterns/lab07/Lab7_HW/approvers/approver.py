from abc import ABC
import logging
from typing import Optional
from document import Document


class Approver(ABC):
    _name: str
    _approvalLevel: int
    _successor: Optional["Approver"]

    def __init__(self, name: str) -> None:
        self._name = name
        self._successor = None

    def setSuccessor(self, successor: "Approver") -> None:
        self._successor = successor

    def approve(self, document: Document) -> None:
        if self._successor:
            logging.debug(f"Document '{document.getTitle()}' being forwarded.")
            self._successor.approve(document)
        else:
            raise RuntimeError("Approval chain is incomplete: no successor set for approval.")
