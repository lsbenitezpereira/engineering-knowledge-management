import logging
from approvers.approver import Approver
from document import Document


class ApproverSenior(Approver):
    _approvalLevel = 2

    def approve(self, document: Document) -> None:
        if document.getImportanceLevel() <= self._approvalLevel:
            logging.info(f"Document '{document.getTitle()}' approved by Senior '{self._name}'.")
        else:
            super().approve(document)
