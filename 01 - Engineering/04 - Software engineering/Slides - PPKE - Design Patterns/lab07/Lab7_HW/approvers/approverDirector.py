import logging
from approvers.approver import Approver
from document import Document


class ApproverDirector(Approver):
    _approvalLevel = 4

    def approve(self, document: Document) -> None:
        if document.getImportanceLevel() <= self._approvalLevel:
            logging.info(f"Document '{document.getTitle()}' approved by Director '{self._name}'.")
        else:
            super().approve(document)
