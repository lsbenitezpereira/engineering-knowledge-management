import logging
from typing import Optional
from approvers.approver import Approver
from document import Document


class ApproverNull(Approver):
    _instance: Optional["ApproverNull"] = None

    def __init__(self, name: str = "Syrio Forel") -> None:
        super().__init__(name)  # Pass the default name to the parent class constructor

    def __new__(cls) -> "ApproverNull":
        # Singleton implementation to ensure only one instance exists
        if cls._instance is None:
            cls._instance = super(ApproverNull, cls).__new__(cls)
        return cls._instance

    def approve(self, document: Document) -> None:
        logging.error(f"Document '{document.getTitle()}' could not be approved by any approver.")

    @classmethod
    def getInstance(cls) -> "ApproverNull":
        return cls()
