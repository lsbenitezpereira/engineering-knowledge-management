import logging
from approvers.approver import Approver
from document import Document


class ApproverManager(Approver):
    _approvalLevel = 3

    def approve(self, document: Document) -> None:
        if document.getImportanceLevel() <= self._approvalLevel:
            logging.info(f"Document '{document.getTitle()}' approved by Manager '{self._name}'.")
        else:
            super().approve(document)
