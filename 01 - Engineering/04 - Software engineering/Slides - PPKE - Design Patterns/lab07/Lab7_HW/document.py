from datetime import datetime, timezone
from typing import Any


class Document:
    _title: str
    _content: str
    _importanceLevel: int
    _lastEditedTime: datetime

    def __init__(self, title: str, content: str, importanceLevel: int) -> None:
        if importanceLevel < 1 or importanceLevel > 5:
            raise ValueError("Importance level must be between 1 and 5")
        self._title = title
        self._content = content
        self._importanceLevel = importanceLevel
        self._lastEditedTime = datetime.now(timezone.utc)

    def getTitle(self) -> str:
        return self._title

    def setTitle(self, title: str) -> None:
        self._title = title
        self._updateLastEditedTime()

    def getContent(self) -> str:
        return self._content

    def setContent(self, content: str) -> None:
        self._content = content
        self._updateLastEditedTime()

    def getImportanceLevel(self) -> int:
        return self._importanceLevel

    def setImportanceLevel(self, importanceLevel: int) -> None:
        if importanceLevel < 1 or importanceLevel > 5:
            raise ValueError("Importance level must be between 1 and 5")
        self._importanceLevel = importanceLevel
        self._updateLastEditedTime()

    def getLastEditedTime(self) -> datetime:
        return self._lastEditedTime

    def _updateLastEditedTime(self) -> None:
        self._lastEditedTime = datetime.now(timezone.utc)
