public class LibraryManagementSystem {
    public static void main(String[] args) {
        // Create some books
        Book book1 = new Book("The Great Gatsby", "F. Scott Fitzgerald");
        Book book2 = new Book("To Kill a Mockingbird", "Harper Lee");
        Book book3 = new Book("1984", "George Orwell");

        // Create users
        User alice = new User("Alice", new RealBookList());
        User bob = new User("Bob", NullBookList.getInstance());

        // Alice borrows some books
        alice.borrowBook(book1);
        alice.borrowBook(book2);

        // Display borrowed books for both users
        alice.displayBorrowedBooks();
        System.out.println("Alice has borrowed " + alice.getBorrowedBookCount() + " books.\n");

        bob.displayBorrowedBooks();
        System.out.println("Bob has borrowed " + bob.getBorrowedBookCount() + " books.\n");

        // Bob tries to borrow a book
        bob.borrowBook(book3);
        bob.displayBorrowedBooks();
        System.out.println("Bob has borrowed " + bob.getBorrowedBookCount() + " books.\n");

        // Alice returns a book
        alice.returnBook(book1);
        alice.displayBorrowedBooks();
        System.out.println("Alice has borrowed " + alice.getBorrowedBookCount() + " books.\n");

        // Alice returns a book
        alice.returnBook(book2);
        alice.displayBorrowedBooks();
        System.out.println("Alice has borrowed " + alice.getBorrowedBookCount() + " books.\n");
    }
}