import java.util.ArrayList;
import java.util.List;

public class NullBookList implements BookList {
    protected NullBookList() {};
    protected static NullBookList globalref = null;

    public static NullBookList getInstance() {
        if (NullBookList.globalref == null) {
            globalref = new NullBookList();
        }
        return globalref;
    }

    @Override
    public int getNumber() {
        return 0;
    }

    @Override
    public List<Book> getList() {
        return new ArrayList<Book>();
    }
    
}
