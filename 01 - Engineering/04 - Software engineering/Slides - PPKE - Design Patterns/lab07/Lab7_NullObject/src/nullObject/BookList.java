import java.util.List;

public interface BookList {
    public int getNumber();
    public List<Book> getList();
}
