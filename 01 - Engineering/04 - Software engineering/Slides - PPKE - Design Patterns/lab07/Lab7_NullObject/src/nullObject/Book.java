
public class Book {
    protected String title;
    protected String author;
    public Book(String string, String string2) {
        this.title = string;
        this.author = string2;
    }

    public String toString() {
        return author+": "+title;
    }

}
