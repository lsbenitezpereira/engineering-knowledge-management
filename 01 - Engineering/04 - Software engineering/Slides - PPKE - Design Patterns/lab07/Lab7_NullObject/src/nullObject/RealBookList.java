import java.util.ArrayList;
import java.util.List;

public class RealBookList implements BookList {
    private List<Book> list = new ArrayList<Book>();

    public RealBookList() {}
    public RealBookList(List<Book> list) {
        this.list = list;
    }

    @Override
    public int getNumber() {
        return list.size();
    }

    @Override
    public List<Book> getList() {
        return list;
    }

}
