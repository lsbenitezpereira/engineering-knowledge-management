import java.util.List;

public class User {
    protected String name;
    protected BookList bookList;
    public User(String string, BookList bookList) {
        this.name = string;
        this.bookList = bookList;
    }

    public void displayBorrowedBooks() {
        List<Book> list = bookList.getList();
        System.out.println("User "+name+" has the following books: ");
        for (Book b : list) {
            System.out.println(b);
        }
    }

    public int getBorrowedBookCount() {
        return bookList.getNumber();
    }

    public void borrowBook(Book b) {
        List<Book> currentList = bookList.getList();
        currentList.add(b);
        bookList = new RealBookList(currentList);
    }

    public void returnBook(Book b) {
        List<Book> currentList = bookList.getList();
        boolean found = false;
        for (Book b1 : currentList) {
            if (b1.equals(b)) {
                found = true;
            }
        }
        if (!found) {
            System.out.println("Book "+b+" was not found in list of borrowed books of "+name);
        } else {
            currentList.remove(b);
        }
        if (currentList.size()==0) {
            bookList = NullBookList.getInstance();
        } else {
            bookList = new RealBookList(currentList);
        }
    }
}
