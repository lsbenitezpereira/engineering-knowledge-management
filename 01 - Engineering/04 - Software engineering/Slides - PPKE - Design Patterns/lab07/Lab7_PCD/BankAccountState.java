package Lab7_PCD;

public class BankAccountState {
    private int ID;
    public int getID() {
        return ID;
    }
    private int balance;
    public int getBalance() {
        return balance;
    }
    private int timestamp;
    public int getTimestamp() {
        return timestamp;
    }
    public BankAccountState(int i, int j, int k) {
        this.ID = i;
        this.balance = j;
        this.timestamp = k;
    }

}
