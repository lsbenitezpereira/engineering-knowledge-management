package Lab7_PCD;

public class BankAccount {
    public static int timestamp = 0;

    private BankAccountState state;
    public BankAccount() {
        state = new BankAccountState(0, 0, BankAccount.timestamp++);
    }
    public void deposit(int amount) {
        if (amount >= 0) {
            state = new BankAccountState(state.getID(), state.getBalance()+amount, BankAccount.timestamp++);
        } else {
            System.out.println("Trying to deposit a negative amount. Aborting.");
        }
    }
    public void withdraw(int amount) {
        if (amount >= 0) {
            if (state.getBalance() - amount >= 0) {
                state = new BankAccountState(state.getID(), state.getBalance()-amount, BankAccount.timestamp++);
            } else {
                System.out.println("Not enough funds in bank account. Aborting.");    
            }
        } else {
            System.out.println("Trying to withdraw a negative amount. Aborting.");
        }
    }
    public int getBalance() {
        return state.getBalance();
    }
    public String toString() {
        return "Bank account "+state.getID()+" with " +state.getBalance()+" EUR - last change at: "+state.getTimestamp();
    }

    public static void main(String[] args) {
        BankAccount acc1 = new BankAccount();
        System.out.println(acc1);
        acc1.deposit(100);
        acc1.deposit(-100);
        acc1.withdraw(50);
        System.out.println(acc1);
        acc1.withdraw(500);
        System.out.println(acc1);
    }
}