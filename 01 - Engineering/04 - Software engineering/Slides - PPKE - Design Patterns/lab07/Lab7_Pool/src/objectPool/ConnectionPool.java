package objectPool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConnectionPool {
    protected ConnectionPool() {};
    protected static ConnectionPool globalref = null;
    public static ConnectionPool getInstance() {
        if (globalref == null)
        globalref = new ConnectionPool();
        return globalref;
    }
    
    protected Map<String,List<DBConnection> > pool = new HashMap<>();
    public DBConnection getConnection(String url) {
        if (!pool.containsKey(url)) {
            pool.put(url, new ArrayList<DBConnection>());
        }
        List<DBConnection> connsToURL = pool.get(url);
        if (connsToURL.size()==0) {
            return new DBConnection(url);
        } else {
            return connsToURL.remove(0);
        }
    }
    public void releaseConnection(DBConnection conn) {
        pool.get(conn.getUrl()).add(conn);
    }
    public void shutdown() {
        for (List<DBConnection> list : pool.values()) {
            for (DBConnection conn : list) {
                conn.close();
            }
        }
        pool.clear();
    }
}
