package chainofresponsibility;

public class ChainDemo {
	public static void main(String[] args)
    {
        Handler node1 = new Handler();
        Handler node2 = new Handler();
        node1.setSuccessor(node2);
        Handler node3 = new Handler();
        node2.setSuccessor(node3);
        Handler node4 = new Handler();
        node3.setSuccessor(node4);
        node4.setSuccessor(node1);  // cicular



        for (int i = 1; i < 10; i++)
        {
            node1.handle(i);
        }
    }
}
