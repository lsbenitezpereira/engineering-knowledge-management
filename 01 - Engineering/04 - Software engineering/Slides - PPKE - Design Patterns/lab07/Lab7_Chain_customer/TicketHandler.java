package Lab7_Chain_customer;

public class TicketHandler {
    TicketHandler successor = null;
    public boolean handleTicket(SupportTicket t){
        if (successor != null){
            return successor.handleTicket(t);
        } else {
            return false;
        }
    }
    public void setNextHandler(TicketHandler h){
        successor = h;
    }

}
