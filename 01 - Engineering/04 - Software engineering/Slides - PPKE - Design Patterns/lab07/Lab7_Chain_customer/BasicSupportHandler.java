package Lab7_Chain_customer;

public class BasicSupportHandler extends TicketHandler{

    @Override
    public boolean handleTicket(SupportTicket t) {
        if (t.getType() == SupportTicket.TicketType.BASIC) {
            System.out.println("Basic handler is handling request: "+ t.getQuery());
            return true;
        } else {
            System.out.println("Basic handler forwarding request");
            return super.handleTicket(t);
        }
    }

}
