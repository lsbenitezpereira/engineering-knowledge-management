package Lab7_Chain_customer;

public class BillingSupportHandler extends TicketHandler{
    public BillingSupportHandler(){
    }

    @Override
    public boolean handleTicket(SupportTicket t) {
        if (t.getType() == SupportTicket.TicketType.BILLING) {
            System.out.println("Billing handler is handling request: "+ t.getQuery());
            return true;
        } else {
            System.out.println("Billing handler forwarding request");
            return super.handleTicket(t);
        }
    }

}
