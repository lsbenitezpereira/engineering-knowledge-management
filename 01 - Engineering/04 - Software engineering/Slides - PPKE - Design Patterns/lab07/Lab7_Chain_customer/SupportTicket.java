package Lab7_Chain_customer;

public class SupportTicket {
    public enum TicketType {BASIC, TECHNICAL, BILLING};
    protected String query;
    public String getQuery() {
        return query;
    }
    protected TicketType type;
    public TicketType getType() {
        return type;
    }
    public SupportTicket(String q, TicketType ticketType) {
        this.query = q;
        this.type = ticketType;
    }
}
