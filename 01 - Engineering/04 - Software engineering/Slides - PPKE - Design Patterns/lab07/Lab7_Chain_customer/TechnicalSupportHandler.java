package Lab7_Chain_customer;

public class TechnicalSupportHandler extends TicketHandler {

    @Override
    public boolean handleTicket(SupportTicket t) {
        if (t.getType() == SupportTicket.TicketType.TECHNICAL) {
            System.out.println("Tech handler is handling request: "+ t.getQuery());
            return true;
        } else {
            System.out.println("Tech handler forwarding request");
            return super.handleTicket(t);
        }
    }

}
