package Lab7_Chain_customer;

public class CustomerSupportSystem {
    public static void main(String[] args) {
        // Create handlers
        TicketHandler basicHandler = new BasicSupportHandler();
        TicketHandler technicalHandler = new TechnicalSupportHandler();
        TicketHandler billingHandler = new BillingSupportHandler();

        // Set up the chain
        basicHandler.setNextHandler(technicalHandler);
        technicalHandler.setNextHandler(billingHandler);

        // Create some test tickets
        SupportTicket basicTicket = new SupportTicket("How do I reset my password?", SupportTicket.TicketType.BASIC);
        SupportTicket technicalTicket = new SupportTicket("App crashes on startup", SupportTicket.TicketType.TECHNICAL);
        SupportTicket billingTicket = new SupportTicket("Charge on my account I don't recognize", SupportTicket.TicketType.BILLING);
        SupportTicket unknownTicket = new SupportTicket("Unknown issue type", null);

        // Process the tickets
        System.out.println("Processing Basic Ticket:");
        System.out.println(basicHandler.handleTicket(basicTicket));

        System.out.println("\nProcessing Technical Ticket:");
        System.out.println(basicHandler.handleTicket(technicalTicket));

        System.out.println("\nProcessing Billing Ticket:");
        System.out.println(basicHandler.handleTicket(billingTicket));

        System.out.println("\nProcessing Unknown Ticket:");
        System.out.println(basicHandler.handleTicket(unknownTicket));
    }
}