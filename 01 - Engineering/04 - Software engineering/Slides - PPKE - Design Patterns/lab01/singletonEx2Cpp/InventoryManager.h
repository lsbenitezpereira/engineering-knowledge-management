#ifndef INVENTORYMANAGER_H
#define INVENTORYMANAGER_H

#include <string>
#include "Logger.h"

class InventoryManager {
public:
    void addBook(const std::string& title, const std::string& author, double price);
    bool checkAvailability(const std::string& title);

private:
    static Logger& logger;
};

#endif // INVENTORYMANAGER_H
