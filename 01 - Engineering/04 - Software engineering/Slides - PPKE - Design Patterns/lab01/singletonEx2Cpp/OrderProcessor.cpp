#include "OrderProcessor.h"

Logger& OrderProcessor::logger = Logger::getLogger();

void OrderProcessor::placeOrder(const std::string& userId, const std::string& bookTitle) {
    logger.info("Processing order for user: " + userId + ", book: " + bookTitle);
    // TODO: Order processing logic here
    logger.info("Order placed successfully");
}

void OrderProcessor::cancelOrder(const std::string& orderId) {
    logger.info("Cancelling order: " + orderId);
    // TODO: Order cancellation logic here
    logger.info("Order cancelled successfully");
}
