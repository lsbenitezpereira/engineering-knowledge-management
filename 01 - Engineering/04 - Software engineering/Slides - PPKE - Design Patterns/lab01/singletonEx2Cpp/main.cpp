#include "Logger.h"
#include "InventoryManager.h"
#include "OrderProcessor.h"
#include "UserAuthenticator.h"

int main() {
    Logger logger = Logger::getLogger();
    
    logger.info("Application started");
    
    InventoryManager inventory = InventoryManager();
    OrderProcessor orderProcessor = OrderProcessor();
    UserAuthenticator auth = UserAuthenticator();
    
    auth.login("user123", "password456");
    inventory.addBook("Java Programming", "John Doe", 29.99);
    orderProcessor.placeOrder("user123", "Java Programming");
    
    logger.info("Application finished");
    
    return 0;
}
