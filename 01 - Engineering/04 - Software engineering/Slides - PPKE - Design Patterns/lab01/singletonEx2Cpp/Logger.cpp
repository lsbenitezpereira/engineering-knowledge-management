#include "Logger.h"
#include <iostream>

Logger* Logger::instance = nullptr;

Logger::Logger() {}

Logger& Logger::getLogger() {
    if (instance == nullptr) {
        instance = new Logger();
    }
    return *instance;
}

void Logger::log(const std::string& formattedMessage) {
    std::cout << formattedMessage << std::endl;
}

void Logger::info(const std::string& message) {
    log("INFO | " + message);
}

void Logger::warning(const std::string& message) {
    log("WARNING | " + message);
}

void Logger::error(const std::string& message) {
    log("ERROR | " + message);
}
