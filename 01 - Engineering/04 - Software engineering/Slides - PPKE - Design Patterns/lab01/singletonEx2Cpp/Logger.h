#ifndef LOGGER_H
#define LOGGER_H

#include <string>

class Logger {
public:
    static Logger& getLogger();
    
    void info(const std::string& message);
    void warning(const std::string& message);
    void error(const std::string& message);
    
private:
    Logger();
    void log(const std::string& formattedMessage);

    static Logger* instance;
};

#endif // LOGGER_H
