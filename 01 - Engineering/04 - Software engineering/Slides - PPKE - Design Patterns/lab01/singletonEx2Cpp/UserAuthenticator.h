#ifndef USERAUTHENTICATOR_H
#define USERAUTHENTICATOR_H

#include <string>
#include "Logger.h"

class UserAuthenticator {
public:
    bool login(const std::string& username, const std::string& password);
    void logout(const std::string& username);

private:
    static Logger& logger;
};

#endif // USERAUTHENTICATOR_H
