#ifndef ORDERPROCESSOR_H
#define ORDERPROCESSOR_H

#include <string>
#include "Logger.h"

class OrderProcessor {
public:
    void placeOrder(const std::string& userId, const std::string& bookTitle);
    void cancelOrder(const std::string& orderId);

private:
    static Logger& logger;
};

#endif // ORDERPROCESSOR_H
