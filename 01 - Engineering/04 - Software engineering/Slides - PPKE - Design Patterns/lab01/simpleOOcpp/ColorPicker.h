#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <iostream>
#include <vector>
#include <string>
#include "Color.h"

class ColorPicker {
private:
    std::vector<Color> colors;

public:
    ColorPicker();

    void createColor(std::string name, int r, int g, int b);

    void listColors() const;

    Color getColor(std::string name) const;
};

#endif // COLORPICKER_H
