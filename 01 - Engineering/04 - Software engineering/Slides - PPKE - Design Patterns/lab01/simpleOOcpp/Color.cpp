#include "Color.h"

// Constructor
Color::Color(int r, int g, int b, std::string name)
    : r(r), g(g), b(b), name(name) {}

// Getter for r
int Color::getR() const {
    return r;
}

// Getter for g
int Color::getG() const {
    return g;
}

// Getter for b
int Color::getB() const {
    return b;
}

// Getter for name
std::string Color::getName() const {
    return name;
}

// toString method
std::string Color::toString() const {
    return name + ": r=" + std::to_string(r) + " g=" + std::to_string(g) + " b=" + std::to_string(b);
}

// Overload the << operator for printing
std::ostream& operator<<(std::ostream& os, const Color& color) {
    os << color.toString();
    return os;
}
