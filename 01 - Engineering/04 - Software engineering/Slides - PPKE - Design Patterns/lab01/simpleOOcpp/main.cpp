#include <iostream>
#include <string>
#include "ColorPicker.h"  // Assuming ColorPicker and Color classes are similarly rewritten

int main() {
    ColorPicker cp;
    cp.createColor("magenta", 255, 0, 255);
    cp.listColors();
    
    Color red = cp.getColor("red");
    std::cout << "Red: " << red << std::endl;
    
    Color magenta = cp.getColor("magenta");
    std::cout << "Magenta: " << magenta << std::endl;
    
    return 0;
}
