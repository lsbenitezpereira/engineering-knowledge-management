#ifndef COLOR_H
#define COLOR_H

#include <iostream>
#include <string>

class Color {
private:
    int r;
    int g;
    int b;
    std::string name;

public:
    Color(int r, int g, int b, std::string name);

    int getR() const;

    int getG() const;

    int getB() const;

    std::string getName() const;

    std::string toString() const;

    friend std::ostream& operator<<(std::ostream& os, const Color& color);
};

#endif // COLOR_H
