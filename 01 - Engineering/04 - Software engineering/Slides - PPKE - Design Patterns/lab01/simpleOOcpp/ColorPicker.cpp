#include "ColorPicker.h"

ColorPicker::ColorPicker() {
    this->createColor("red", 255, 0, 0);
}

void ColorPicker::createColor(std::string name, int r, int g, int b) {
    Color c(r, g, b, name);
    colors.push_back(c);
}

void ColorPicker::listColors() const {
    for (const Color& c : colors) {
        std::cout << c << std::endl;
    }
}

Color ColorPicker::getColor(std::string name) const {
    for (const Color& c : colors) {
        if (c.getName() == name) {
            return c;
        }
    }
    return Color(0, 0, 0, "black");
}
