package compositeEx1;

import java.util.ArrayList;

public class Directory extends Item{
    private final String name;
    private final ArrayList<Item> includedFiles = new ArrayList<>();

    public Directory(String name) {
        super(name);
    }

    public void add(Item item) {
        includedFiles.add(item);
    }

    public void ls() {
        ls(0);
    }
    public void ls(int depth) {
        System.out.println(new String(new char[3*depth]).replace('\0', ' ') + name);
        depth += 1;
        for (Item item : includedFiles) {
            item.ls(depth);
        }
    }
}