package compositeEx1;

public class CompositeDemo {

    public static void main(String[] args) {
        Item music = new Directory("MUSIC");
        Item scorpions = new Directory("SCORPIONS");
        Item dio = new Directory("DIO");
        Item track1 = new File("Don't worry, be happy.mp3");
        Item track2 = new File("track2.m3u");
        Item track3 = new File("Wind of change.mp3");
        Item track4 = new File("Big city night.mp3");
        Item track5 = new File("Rainbow in the dark.mp3");
        music.add(track1);
        music.add(scorpions);
        music.add(track2);
        scorpions.add(track3);
        scorpions.add(track4);
        scorpions.add(dio);
        dio.add(track5);
        music.ls();
    }
}