package simpleOO;

import java.util.ArrayList;

public class ColorPicker {
    ArrayList<Color> colors = new ArrayList<>();

    public ColorPicker(){
        this.createColor("red", 255, 0, 0);
    }

    public void createColor(String name, int r, int g, int b){
        // TODO:
        // avoid duplication
        // check if values are between 0 and 255
        Color c = new Color(r, g, b, name);
        colors.add(c);
    }

    public void listColors(){
        for (Color c: colors){
            System.out.println(c);
        }
    }

    public Color getColor(String name){
        for (Color c: colors){
            if (c.getName().equals(name)){
                return c;
            }
        }
        return new Color(0, 0, 0, "black");
        //throw new Exception("Color not found");
    }
}
