package simpleOO;

public class ColorPickerDemo {
    public static void main(String[] args) {
        ColorPicker cp = new ColorPicker();
        cp.createColor("magenta", 255, 0, 255);
        cp.listColors();
        Color red = cp.getColor("red");
        System.out.println("Red: " + red);
        Color magenta = cp.getColor("magenta");
        System.out.println("Magenta: " + magenta);
    }
}
