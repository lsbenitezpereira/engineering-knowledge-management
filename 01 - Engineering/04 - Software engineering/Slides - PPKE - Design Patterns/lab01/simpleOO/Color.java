package simpleOO;


public class Color {
    private int r;
    private int g;
    private int b;
    private String name;

    public Color(int r, int g, int b, String name){
        this.r = r;
        this.g = g;
        this.b = b;
        this.name = name;
    }

    int getR(){
        return this.r;
    }

    int getG(){
        return this.g;
    }

    int getB(){
        return this.b;
    }

    String getName(){
        return this.name;
    }

    public String toString(){
        return this.getName() + ": r=" + this.r + " g=" + this.g + " b=" + this.b;
    }
}
