package singletonEx2;

class OrderProcessor {
    private static Logger logger = Logger.getLogger();
    public void placeOrder(String userId, String bookTitle) {
        logger.info("Processing order for user: " + userId + ", book: " + bookTitle);
        // TODO: Order processing logic here
        logger.info("Order placed successfully");
    }
    
    public void cancelOrder(String orderId) {
        logger.info("Cancelling order: " + orderId);
        // TODO: Order cancellation logic here
        logger.info("Order cancelled successfully");
    }
}