package singletonEx2;

class InventoryManager {
    private static Logger logger = Logger.getLogger();
    public void addBook(String title, String author, double price) {
        logger.info("Adding book: " + title + " by " + author);
        // TODO Add book to inventory logic here
        logger.info("Book added successfully");
    }
    
    public boolean checkAvailability(String title) {
        logger.info("Checking availability for: " + title);
        // TODO: Check availability logic here
        boolean isAvailable = true; // Placeholder
        logger.info("Availability status: " + isAvailable);
        return isAvailable;
    }
}