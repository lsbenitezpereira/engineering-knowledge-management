package singletonEx2;


public class OnlineBookstore {
    private static Logger logger = Logger.getLogger();
    public static void main(String[] args) {
        logger.info("Application started");
        
        InventoryManager inventory = new InventoryManager();
        OrderProcessor orderProcessor = new OrderProcessor();
        UserAuthenticator auth = new UserAuthenticator();
        
        // Simulate user interaction
        auth.login("user123", "password456");
        inventory.addBook("Java Programming", "John Doe", 29.99);
        orderProcessor.placeOrder("user123", "Java Programming");
        logger.info("Application finished");
    }
}