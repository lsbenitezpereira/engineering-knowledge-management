package singletonEx2;


public class Logger {
    private static Logger instance = null;
    private Logger(){
    }

    public static Logger getLogger(){
        if (instance == null){
            instance = new Logger();
        }
        return instance;
    }

    private void _log(String formattedMessage){
        System.out.println(formattedMessage);
    }

    public void info(String message){
        this._log("INFO | " + message);
    }

    public void warning(String message){
        this._log("WARNING | " + message);
    }

    public void error(String message){
        this._log("ERROR | " + message);
    }
}
