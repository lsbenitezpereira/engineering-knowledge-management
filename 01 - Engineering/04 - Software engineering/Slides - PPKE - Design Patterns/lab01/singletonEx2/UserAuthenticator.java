package singletonEx2;

class UserAuthenticator {
    private static Logger logger = Logger.getLogger();
    public boolean login(String username, String password) {
        logger.info("Login attempt for user: " + username);
        // TODO: Authentication logic here
        boolean isAuthenticated = true; // Placeholder
        logger.info("Authentication result: " + isAuthenticated);
        return isAuthenticated;
    }
    
    public void logout(String username) {
        logger.info("Logging out user: " + username);
        // TODO: Logout logic here
        logger.info("User logged out successfully");
    }
}