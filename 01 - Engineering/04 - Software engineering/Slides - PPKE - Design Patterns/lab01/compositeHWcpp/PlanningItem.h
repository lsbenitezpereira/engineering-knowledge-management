#ifndef PLANNING_ITEM_H
#define PLANNING_ITEM_H

#include <string>
#include <vector>
#include "Contact.h"
using namespace std;

class PlanningItem {
    public:
        virtual ~PlanningItem() = default;

        virtual string getName() const = 0;
        virtual string getDescription() const = 0;
        virtual double getTimeNeeded() const = 0;

        virtual void addProjectItem(PlanningItem* item) = 0;
        virtual void removeProjectItem(PlanningItem* item) = 0;
        virtual const vector<PlanningItem*>& getProjectItems() const = 0;
};


class PlanningItemPPKE : public PlanningItem {
    public:
        PlanningItemPPKE(const string& name, const string& description, const ContactPPKE* owner, double timeNeeded);

        string getName() const override;
        string getDescription() const override;
        double getTimeNeeded() const override;

        void addProjectItem(PlanningItem* item) override;
        void removeProjectItem(PlanningItem* item) override;
        const vector<PlanningItem*>& getProjectItems() const override;

    private:
        string name;
        string description;
        vector<PlanningItem*> projectItems;
        const ContactPPKE* owner;
        double timeNeeded;
};


class Deliverable : public PlanningItemPPKE {
    public:
        Deliverable(const string& name, const string& description, const ContactPPKE* owner);
        void addProjectItem(PlanningItem* item) override;  // Because it is a leaf
};

class Task : public PlanningItemPPKE {
    public:
        Task(const string& name, const string& description, const ContactPPKE* owner, double timeNeeded);
};

class Project : public PlanningItemPPKE {
    public:
        Project(const string& name, const string& description);
};

#endif // PLANNING_ITEM_H
