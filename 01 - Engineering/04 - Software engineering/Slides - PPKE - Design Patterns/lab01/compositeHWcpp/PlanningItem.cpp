#include <iostream>
#include <algorithm>
#include <stdexcept>
#include "PlanningItem.h"
using namespace std;

/* PlanningItemPPKE */
PlanningItemPPKE::PlanningItemPPKE(const string& name, const string& description, const ContactPPKE* owner, double timeNeeded){
    this->name = name;
    this->description = description;
    this->owner = owner;
    this->timeNeeded = timeNeeded;
}

string PlanningItemPPKE::getName() const {
    return this->name;
}

string PlanningItemPPKE::getDescription() const {
    string fullDescription = this->description;
    if (!projectItems.empty()) {
        fullDescription += "\nThis Item contains the following nested items:\n";        
        for (const PlanningItem* item : projectItems) {
            string itemDescription = item->getDescription();
            if (itemDescription.empty()) {
                fullDescription += "  " + item->getName() + "\n";
            } else {
                fullDescription += "  " + item->getName() + ": " + itemDescription + "\n";
            }
        }
    }
    return fullDescription;
}

double PlanningItemPPKE::getTimeNeeded() const {
    double totalTime = this->timeNeeded;
    for (const PlanningItem* item : projectItems) {
        totalTime += item->getTimeNeeded();
    }
    return totalTime;
}

void PlanningItemPPKE::addProjectItem(PlanningItem* item) {
    projectItems.push_back(item);
}

void PlanningItemPPKE::removeProjectItem(PlanningItem* item) {
    projectItems.erase(remove(projectItems.begin(), projectItems.end(), item), projectItems.end());
}

const vector<PlanningItem*>& PlanningItemPPKE::getProjectItems() const {
    return projectItems;
}


/* Deliverable */
Deliverable::Deliverable(const string& name, const string& description, const ContactPPKE* owner)
    : PlanningItemPPKE(name, description, owner, 0) {}

void Deliverable::addProjectItem(PlanningItem* item) {
    throw std::runtime_error("Deliverables are leaf objects, thus can not contain nested items.");
}


/* Task */
Task::Task(const string& name, const string& description, const ContactPPKE* owner, double timeNeeded)
    : PlanningItemPPKE(name, description, owner, timeNeeded) {}


/* Project */
Project::Project(const string& name, const string& description)
    : PlanningItemPPKE(name, description, NULL, 0) {}
