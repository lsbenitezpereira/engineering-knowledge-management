#ifndef CONTACT_H
#define CONTACT_H

#include <string>
using namespace std;

class Contact {
public:
    virtual ~Contact() = default;

    virtual string getFirstName() const = 0;
    virtual string getLastName() const = 0;
    virtual string getTitle() const = 0;
    virtual string getOrganization() const = 0;

    virtual void setFirstName(const string& newFirstName) = 0;
    virtual void setLastName(const string& newLastName) = 0;
    virtual void setTitle(const string& newTitle) = 0;
    virtual void setOrganization(const string& newOrganization) = 0;

    virtual string toString() const = 0;
};

class ContactPPKE : public Contact {
public:
    ContactPPKE();
    ContactPPKE(const string& newFirstName, const string& newLastName,
                const string& newTitle, const string& newOrganization);

    string getFirstName() const override;
    string getLastName() const override;
    string getTitle() const override;
    string getOrganization() const override;

    void setFirstName(const string& newFirstName) override;
    void setLastName(const string& newLastName) override;
    void setTitle(const string& newTitle) override;
    void setOrganization(const string& newOrganization) override;

    string toString() const override;

private:
    string firstName;
    string lastName;
    string title;
    string organization;
};

#endif // CONTACT_H
