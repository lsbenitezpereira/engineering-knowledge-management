#include <iostream>
#include "Contact.h"
#include "PlanningItem.h"
using namespace std;


/*
Warning: the objects below don't take full advantage of polimorphism, due to object slicing.
The best way to use polimorphism is either with pointers or explicitly indicating the class,
like the examples bellow (both raise the expected exception).

PlanningItemPPKE* deliverable = new Deliverable("Island Name", "", &contact1);
PlanningItemPPKE* task = new Task("Isle1", "Find out whether Fiji has better weather than French Polynesia", &contact1, 1.0);
deliverable->addProjectItem(task);
delete deliverable;
delete task;
//or
Deliverable deliverable = Deliverable("Island Name", "", &contact1);
PlanningItemPPKE task = Task("Isle1", "Find out whether Fiji has better weather than French Polynesia", &contact1, 1.0);
deliverable.addProjectItem(&task);
*/

int main() {
    // Initialize mock project
    ContactPPKE contact1 = ContactPPKE("John", "Deere", "CEO", "Wise Guys LTD");
    ContactPPKE contact2 = ContactPPKE("Moises", "Eckengerg", "Banker", "No Money, Inc.");
    ContactPPKE contact3 = ContactPPKE("Erik", "Oloffson", "Brave man", "Viking, Inc.");
    ContactPPKE contact4 = ContactPPKE("Lem", "Ming", "Slow guy", "BDA");

    PlanningItemPPKE project = Project("IslandParadise", "Acquire a personal island paradise");

    PlanningItemPPKE deliverable1 = Deliverable("Island Paradise", "", &contact1);
    PlanningItemPPKE task1 = Task("Fortune", "Get a lot of money", &contact4, 12.0);
    PlanningItemPPKE task2 = Task("Isle", "Find a good island", &contact2, 4.5);
    PlanningItemPPKE task3 = Task("Name", "Come up with a name for the island", &contact3, 1.1);

    project.addProjectItem(&deliverable1);
    project.addProjectItem(&task1);
    project.addProjectItem(&task2);
    project.addProjectItem(&task3);

    PlanningItemPPKE deliverable11 = Deliverable("$1,000,000", "(total money)", &contact1);
    PlanningItemPPKE task11 = Task("Fortune1", "Cheat on the lottery", &contact4, 12.5);
    PlanningItemPPKE task12 = Task("Fortune2", "Invest money in the real estate business for more money", &contact1, 16.3);
    task1.addProjectItem(&task11);
    task1.addProjectItem(&task12);
    task1.addProjectItem(&deliverable11);

    PlanningItemPPKE task21 = Task("Isle1", "Find out whether Fiji has better weather than French Polynesia", &contact1, 1.0);
    PlanningItemPPKE task22 = Task("Isle2", "Locate an island for auction on EBay", &contact4, 6.3);
    PlanningItemPPKE task23 = Task("Isle2a", "Buy the island", &contact3, 7.1);
    task2.addProjectItem(&task21);
    task2.addProjectItem(&task22);
    task2.addProjectItem(&task23);

    PlanningItemPPKE deliverable31 = Deliverable("Island Name", "", &contact1);
    task3.addProjectItem(&deliverable31);

    // Display values
    cout << "Calculating total time for the project" << endl;
    cout << "\t" << project.getDescription() << endl;
    cout << "Time Required: " << project.getTimeNeeded() << endl;
    
    return 0;
}
