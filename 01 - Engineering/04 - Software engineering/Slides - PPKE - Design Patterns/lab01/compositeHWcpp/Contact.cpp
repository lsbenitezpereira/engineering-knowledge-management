#include "Contact.h"
using namespace std;

ContactPPKE::ContactPPKE() : firstName(""), lastName(""), title(""), organization("") {}

ContactPPKE::ContactPPKE(const string& newFirstName, const string& newLastName,
                         const string& newTitle, const string& newOrganization)
    : firstName(newFirstName), lastName(newLastName), title(newTitle), organization(newOrganization) {}

string ContactPPKE::getFirstName() const {
    return firstName;
}

string ContactPPKE::getLastName() const {
    return lastName;
}

string ContactPPKE::getTitle() const {
    return title;
}

string ContactPPKE::getOrganization() const {
    return organization;
}

/////////

void ContactPPKE::setFirstName(const string& newFirstName) {
    firstName = newFirstName;
}

void ContactPPKE::setLastName(const string& newLastName) {
    lastName = newLastName;
}

void ContactPPKE::setTitle(const string& newTitle) {
    title = newTitle;
}

void ContactPPKE::setOrganization(const string& newOrganization) {
    organization = newOrganization;
}

string ContactPPKE::toString() const {
    return firstName + " " + lastName;
}
