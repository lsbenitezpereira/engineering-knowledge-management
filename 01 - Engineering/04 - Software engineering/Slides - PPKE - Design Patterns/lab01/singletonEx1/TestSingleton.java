package singletonEx1;

public class TestSingleton {
	public static void foo()
	{
	  GlobalClass.instance().set_value(1);
	  System.out.println("foo: global_ptr is " + GlobalClass.instance().get_value() + ", and the instance is: "+GlobalClass.instance() + "\n");
	}
	
	public static void bar()
	{
	  GlobalClass.instance().set_value(2);
	  System.out.println("bar: global_ptr is " + GlobalClass.instance().get_value() + ", and the instance is: "+GlobalClass.instance() + "\n");
	}
	
	public static void main(String[] args) {
		System.out.println("main: global_ptr is " + GlobalClass.instance().get_value() + ", and the instance is: "+GlobalClass.instance() + "\n");
		foo();
		bar();
	}
	
}
