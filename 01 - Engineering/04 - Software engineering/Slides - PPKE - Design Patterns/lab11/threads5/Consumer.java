

public class Consumer implements Runnable {
    SharedQueue queue;
    public Consumer(SharedQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                String item = queue.get();
                System.out.println(item);
                queue.notifyAll();
            }
        }
    }

    public static void main(String[] args) {
        SharedQueue queue = new SharedQueue();
        for (int i = 0; i < 5; i++) {
            Thread producer  = new Thread(new Producer(queue));
            Thread consumer = new Thread(new Consumer(queue));
            producer.start();
            consumer.start();
        }

        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
