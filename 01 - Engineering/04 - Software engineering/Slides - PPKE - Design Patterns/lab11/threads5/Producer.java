

public class Producer implements Runnable {
    SharedQueue queue;
    public Producer(SharedQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                while (queue.isFull()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                queue.put("Some message");
                queue.notifyAll();
            }
        }
    }
}
