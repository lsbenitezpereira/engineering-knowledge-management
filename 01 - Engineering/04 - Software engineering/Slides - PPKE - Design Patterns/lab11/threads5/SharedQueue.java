
import java.util.LinkedList;
import java.util.Deque;

public class SharedQueue {
    Deque<String> deque =  new LinkedList<>();
    static final int MAX_SIZE = 10;

    public boolean isFull() {
        return deque.size() == MAX_SIZE;
    }

    public boolean isEmpty() {
        return deque.isEmpty();
    }

    public void put(String s) {
        deque.addLast(s);
    }

    public String get() {
        return deque.removeFirst();
    }
}
