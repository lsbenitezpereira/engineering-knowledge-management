[TOC]

# Conceptualization

* operating system designed to operate and respond in real time, usually meaning to process data immediately as it comes in a more synchronous way. 

* **Programming model**

  * the RTOS library allows for semi-parallel programming (not truly, just time sharing)
  * Event driver: a task only has work (processing) to perform after the occurrence of the event that triggers it, and is not able to enter the Running state before the event has occurred
  * Orientado a multitasking:

  ![image-20210209090809335](Images - RTOS/image-20210209090809335.png)


* **Usually have**

  * Usually are a microkernel, so they have just a basic conjunto de serviços para tarefas interagirem com o HW
* **Usually do not have**

  * Interface gráfica
  * file system
  * stack de USB
  * stack de network
  * etc.
* **Choosing an RTOS**
  *  suporte em longo prazo (correção de bugs)
  *  desempenho
  *  latência e jitter adicionado no modelo de tarefas.
* **Other annotations**

  * As interrupções normais do uC ainda existem



## Real time systems

* must guarantee response within specified time constraints

* A resposta deve ser dada dentro de um certo tempo

* 

* Deadline MUST be accoplished

* not necessarily fast, mas previsível

* well-defined, fixed time constraints.

* Jitter (\Delta) = Max - min, for all samples

![img](Images - RTOS/Wed, 20 May 2020 141604.png)

* Hard RT: deve ocorrer dentro da janela de tempo determinada; qualquer resposta depois do tempo é inútil

* Soft RT: as tarefas devem seguir janelas de tempo, mas é aceitável transbordar

  

  ![image-20210209090922228](Images - RTOS/image-20210209090922228.png)

* **Applications**

  * fly-by-wire aircraft
  * anti-lock brakes

* **priorities**
  * interruptions may or may not have higher priority than other threads
  * preemtivo: seguir a maxima prioridade

## Análise de tarefas

* Podem ser periódicas, aperiodicas (pode acontecer a qualquer momento) ou esporádicas (aperiódica com tempo minimo entre acontecimentos)

* **Relacionamentos entre tarefas**
  * A precede B: $A > B$
  * A precede imediatamente B: $A \rightarrow B$
  * bloqueios: ? por exemplo causados por exclusão mútua

* **Nomemclatura**
  * cada tarefa do conjunto é indicada pelo índice $i$
  * maiscula vs minuscula?
  * 
  * arival time: $a_i$
  * Computing time: $C_i$
  * Deadline: $d_i$ (deadline relativo vs absoluto)
  * Start time: $s_i$
  * Finishing time: $f_i$
  * Lateness: $L_i = f_i – d_i$ (pode ser negativo, se terminar antes do dealine)
  * Tardiness: $E_i = max(0, f_i – d_i )$
  * Folga (laxity ou slack time): $X_i = d_i – a_i - C_i$
  *  
  * Período (para tarefas ciclicas): $P_i$
  * Tempo de resposta: $R_i = f_i - a_i = C_i + \text{tempo de interrupção}$
  *  
  * as vezes consideramos um jitter no tratamento de cada requisição; para fins de calculo, apenas acrecentamos $J_i$ ao $R_i$: $R_i = J_i + R_{i\text(sem jitter)}$

* **teste de escalnailidae**

  * exato: identificam  conjunto de tarefas que podem e que não podem ser realizadas
  * suficiente: garante os que dá pra escalonar, mas os conjuntos rejeitados talvez também possam; possui falso-negativos
  * necessários: os conjuntos aceitos talvez possam ser escalonados, e os rejeitados com certeza não; possui falso-positivos

* **análise por tempo de resposta - Prioridade estática**

  * somente para rate monotonic? ou é idependente do algoritmo de escalonament

  * 

  * Se $R_i<=D_i$, para todas as tarefas, então o conjunto é escalonado

  * funciona apenas se todos $D_i$ forem menor ou igual ao $P_i$ (deadline está dentro de cada período, o que é o usual)

  * o tempo de interrupção de uma tarefa é calculado considerando que ela será interrompida pelas tarefas com prioridade maior que ela; Deve ser feito de forma iterativa pela equação:
    $$
    R_i^{n+1} = C_i + \sum_{j \in HighPriorities(i)}\left\lceil \frac{R_i^n}{P_j} \right\rceil C_j
    $$
    

* * 
  
* 

# Principais RTOSs

* **FreeRTOS** http://www.freertos.org/index.html http://www.freertos.org/portmspgcc.html
* **uC**
  * very reliable
  * excelentes bibliotecas para periféricos
* **Zephyr**
  * Very small
  * IoT and low power focused
  * openSouce
  * feito pelo pessoal da Free Software Foundation (kernel do Linux)
  * Muito bem documentado
  * ==faz mais do que o freeRTOS?==
* **TinyOS**
  * http://www.tinyos.net/
* **PowerPac** http://www.iar.com/website1/1.0.1.0/964/1/ 
* **scmRTOS** http://scmrtos.sourceforge.net/ScmRTOS 
* **ecos** http://www.ecoscentric.com/index.shtml
* **Contiki RTOS**
  * has a simulator called Cooja
    which allows researcher and developers to simulate and emulate
    IoT and wireless sensor network (WSN) applications

## Free RTOS

* bought by amazon?
* Memory footprint: 10kB of Flash and +-0.5kB of RAM
* (+) highly portable
* [Short course: FreeRTOS on STM](https://www.youtube.com/playlist?list=PLnMKNibPkDnFeFV4eBfDQ9e5IrGL_dx1Q) 
* **license**
  * Modified GNU General Public License (GPL) and can be used in proprietary commercial applications
  * have a commercial license providing support and allowing kernel modification
  * :book: Barry (2009) pg 159
* **File structure**
  * The core FreeRTOS source code (task.c, queue.c, list.c). This is the code that is common to all ports.
  * A port layer for each microcontroller and compiler combination that is supported. Inludes the heap management implementations.
  * A project file or makefile to build a demo application for each microcontroller and compiler combination that is supported.
  * A set of demo tasks that are common to each demo application. These demo tasks are referenced from the port specific demo projects.
  * optional source file called croutine.c implements the FreeRTOS co-routine functionality
  * FreeRTOSConfig.h
* **Pratical tips**
  * Start project from scratch: :book: Barry (2009) pg 154


### Tasks fundamentals

* ou *thread*

* linha/sequência de execução de código

* implemented as C functions; must return void and take a void pointer parameter.

* never ends 

* programa que pensa que ele tem o processador inteiro para ele

* It is also possible to create a task from within another task

* the same task can be created several times

* Create with:

  ```C
  bool xTaskCreate(
  	taskFunction,
      name,
      stackSize,
      parameters,
      priority,
      taskHandler
  );
  ```
  
  
  
* stack size is in number of words (32 bits, in cortexM0)

* Priorities can be assigned from 0, which is the lowest priority, to (configMAX_PRIORITIES – 1), which is the highest priority; Any number of tasks can share the same priority
  
* Tip: assign the taskHandler to globals, so that every task can access every other task
  
* **states**

  * blocked

  * ready
  
  * running
  
  * Suspended:  not available to the scheduler.
  
    ![image-20210210214329210](Images - RTOS/image-20210210214329210.png)
  
* Debug thread: created specifically to debug and test other threads, which can be disabled

* Idle thread: just to wait;  has the lowest possible priority; is responsible for dealocating memory of deleted tasks, so it’s always good to left some time idle;

* **Task Control Block (TCB)**

  * ID, priority, program counter, next TCB, etc
  
* **blocking and non-blocking tasks**

  * ?

* **types of tasks**

  * ‘continuous processing: always have work to do; are always in either the Ready or the Running state
  * periodic: place itself into the Blocked state between each periodic evetn; can be implemented adding `vTaskDelayUntil( &xLastWakeTime, ( <delayMs> / portTICK_RATE_MS ) );` to a task

* **Contexto**

  * Program counter, pilha e registradores
  * Salva contexto ao sair, restaura contexto ao voltar

### Task scheduling

* dividindo o tempo do processador em time slots e alocando um slot para cada tarefa

* can be clock-driven (which is the case of FreeRTOS; based on system tick) or event-driven

* system tick: tempo que determina os time slots; uma tarefa só é trocada no tick; geralmente algo entre 1ms e 100ms; tick count: number of tick interrupts that have occurred since the scheduler was started

* Fluxo de execução:

   ![image-20210209092607725](Images - RTOS/image-20210209092607725.png)

* The FreeRTOS scheduler is the only entity that can switch a task in and out

* Uma task pode enviar mensagens para outra

* Only one task can exist in the Running state at any one time

* quando uma tarefa acaba antes, já entra a próxima

* **Pre-emption**

   * Or *desheduling*
   * The transition to a higher priority
   * occurs automatically, and without the knowledge of the task being pre-empted
   * asyncronous, doesnt wait for tick
   * can still have priorities, but a lower priority task can execute if a higher priority send “yield”

* **Idle hook**

   * call-back function
   * called by the idle task once per iteration of the idle task loop.
   * the function must be `void vApplicationIdleHook( void )`
   * USE_IDLE_HOOK must be set to 1 within FreeRTOSConfig.h

* **Tick hook**

  * call-back function
  * called by the kernel at each tick interrupt.
  * The function must be `void vApplicationTickHook(void)`
  * can only call the RTOS API that are “FromISR”

* **Classificação quanto à troca de processo**

  * preemtivo: o processo pode ser interrompido/trocado
  * não preemtivo: o processo que está rodando continua rodando até ele querer parar; Não necessariamente com preemção será melhor

* **classificação quanto à execução**

  * Batch: tasks of finite time; usually the jobs don't have interaction with the user
  * ciclic: tasks of infinite time (o hugo definou “ciclico” também como necessariamente offline)

* **classificação quanto ao momento de escalonamento**

  * offline: decidido antes do sistema começar a rodar
  * online: decidido em tempo de execução

* **classificação quanto ao momento de escalonamento**

   * estático: parâmetros (das tarefas e do escalonador, ex: prioridade) são fixos
   * dinâmico: parâmetros mudam durante a execução (mas para um certo job elas podem ficar constantes, por exemplo: a prioridade das tarefas muda, mas quando um job específico entra a sua prioridade não muda mais)

* **classificação quanto à atuação**

   * curto prazo: seleciona processos que vão usar a CPU dentre os que estão aptos à execução
   * longo prazo: seleciona processos que vão ser aceitos para escalonamento 

* **?**

   * guaranteed vs best effort (descarte de tarefas, perda de deadline, redução d)?

* **cyclic executive**

   * anything that executes task periodically, in sequence, and forever
   * table based: several, each one for a predetermined time
   * (+) very predictable
   * (+) can be statically analysed
   * (-) can not handle well sporadic tasks
   * frames: enforced time boundaries? least commo

* **métricas**

   * TA? Wt?



#### Scheduling algorithms

* ==Are all these implemented in FreeRTOS?==

* ==move to generic section==

* <u>First-come first-served</u>

   * non preemtive
   * batch oriented
   * prioridade variável
   * pior do que o Earliest Deadline First, há conjuntos que não podem ser escalonado por FCFS e podem ser escalonados por EDF (e o contrário não é verdadeiro)
   * bad latency

* <u>shortest job first (SJF)</u>
   * non preemtive
   * batch oriented

* <u>shortest remaining time next</u>

   * Preemtivo
   * batch oriented?

* <u>Earliest deadline first (EDF)</u>

   * Preemtivo

   * prioridade variável

   * é o algoritmo mais usado para prioridade dinâmica

   * Teste suficiente - Escalonável se:
     $$
     \sum_{i=1}^N \frac{C_i}{min(D_i, P_i)} \leq 1
     $$
     

* <u>round-robin</u>
  * Preemtivo
  * ciclic oriented
  * todas com mesma prioridade, executando por um mesmo tempo fixo
  *  fatias de tempo são atribuídas a cada processo em partes iguais e em ordem circular
  * Se um acaba antes, já entra próximo
  * (-) usually switch tasks more often than strictly needed
  * quantum: tempo do slot
  * Se o quantum for pequeno, havera muito overhead de switch
  * se o quantum for grande, o tempo de resposta do sistema será grande
  *  
  * tempo máximo de espera: $(n-1)*q$ (n processos, q quantum)

* <u>Fixed Priority Preemptive Scheduling</u>
  * Preemtivo
  * ciclic oriented
  * Default in FreeRTOS
  * always selects the highest priority task that is able to run
  * fixed: only the tasks can change the pritities (not the kernel)
  * each teask has a fixed priority
  * for each priorty level, holds a simple FIFO logic
  * Start running the tasks for lower priorities only when the queues for higher priorities are empty (thus can easily lead to statvation)

* <u>Cooperative scheduling</u>
  * não preemtivo
  * preemption doesnt happen
  * switch will only occur when either the Running state task enters the Blocked state or the Running state task explicitly calls taskYIELD().
  * You can also force a sinronous switch, like using an interuption
  * `taskYIELD()`: informs the scheduler that a switch to another task should occur now; volunteering to be removed from the Running state

* <u>loterry scheduling</u>

  * each job receive a "ticket"
  * the next ticket to be exeuted is randomly selected
  * important jobs may receive more than one ticket
  * the tickets may or may not be selected more than once

* <u>Rate Monotonic Scheduling (RMS)</u>

  * unique priority be assigned to each task in accordance with the tasks periodic execution rate ($P_i$)

  * The lowest priority is assigned to the task with the lowest frequency of periodic execution

  * offline

  * ciclic oriented

  * static priority

  * consideramos geralmente que deadline = periodo

  * Test - Can be scalonated without preemption if the condition is true (suficient but not necessary):

* <u>Combining into levels</u>

   * You can do things like first a fixedPRiorityPreemptive to separate between levels, with a round-robin between the high pririty ones, and a EDF for lower priorities


### Inter-task communication

* or *inter-process comunication (IPC)*

* used to sincronize tasks, signal events and unblock tasks

* condição de corrida: quando duas tasks tentam acessar uma mesmo recurso compartilhado, por exemplo uma variável global

* **Basic data structure - queues**

  * To more information, see in *Algorithms - data structures* or in *Matematical modelling - Queue theory*

  * can hold a finite number of fixed size data items

  * Accessed by Multiple Tasks

  * when reading from a queue, you can speficify a timeout such that in case the queue is empty you’ll wait (blocked) for x time for data

  * it is preferable to use the queue to
    transfer pointers to the data rather than copy the data itself

  * Often the receiver of
    the data needs to know where the data came from so it can determine how it should be processed. A
    simple way of achieving this is to use the queue to transfer structures where both the value of the data
    and the source of the data are contained in the structure fields

    ![image-20210217144142105](Images - RTOS/image-20210217144142105.png)

  * The functions are blocking, so the task will enter blocking state for at most X time while trying to complete the task

  * `xQueueSendToBack()`: send data to the back (tail) of a queue; 

  * `xQueueSendToFront()`: send data to the front (head) of a queue; blocks the task;  

  * `xQueueReceive()`: pop (read and remove) an item from a queue.

  * `xQueuePeek()`: read without remove

  * `uxQueueMessagesWaiting()`: query the number of items in a queue; non blocking

  * They should not be called form interrupts; they have special implementations for that

* **Interrupt management**

  * Interrupt can be much more than intertask communication, but I’ll register here
  * It should be noted that only API functions and macros that end in ‘FromISR’ or ‘FROM_ISR’ should
    ever be used within an interrupt service routine.
  * usually, the ISR trigger a handler with very high priority
  * can be nested
  *  software priority assigned to a task is in no way related to the hardware priority
    assigned to an interrupt source
  * Normal interrupts can have higher priority than the tick interrupt and syscall interrupt, so nothing the kernel
    does will prevent these interrupts from executing immediately 
  * if higher than the syscalls, the ISR cannot use the freeRTOS API

* **Channels**

  * Not implemented in FreeRTOS (AFAIK)
  * A sender sends data to all receivers
  * if there is no receiver, `send` will wait (it is blocking)
  * if there is no sender, `receiver` will wait (it is blocking)
  * enforces synchronous communication
  * buffered channels? until the buffer is full, the `send` is non blocking

* **Wait group**

  * Not implemented in FreeRTOS (AFAIK)
  * first you incredement a “counter” many times
  * each time someone calls `done`, the counter is decreased
  * the blocking function `wait()` will only return after the “counter” is zero again


### Resource sharing

* Access to perifeals, files, writing to global variables with non-atomic operations (like A |= 0x01) etc

* when a task starts using, it must *finish* using it without being interupted

* Reentrant function: one that  is safe to call the function from more than one task, or from both tasks
  and interrupts. 

* **Critical section**
  * Can not be switched out, [almost] even by interrupts
  * interrupts can still ocur, if their priority is higher than configMAX_SYSCALL_INTERRUPT_PRIORITY and if nested interrupts are allowed
  *  surrounded by `taskENTER_CRITICAL()` and `taskEXIT_CRITICAL()`
  * It is safe for critical sections to become nested
  * (-) may delay important interrupts

* **Scheduler suspension**
  * can be normally interrupted
  * (+) less aggressive than critical section, so can be used to longer operations
  * (-) cannot access RTOS API withing the section
  * (-) só funciona para arquiteturas monoprocessadas (1 cor)
  * surrounded by `vTaskSuspendAll()` and `xTaskResumeAll() `

* **busy waiting**

  * ? 

* **semáforo**

  * uma flag se pode ou não usar o recurso
  * (+) simple and easy to analyse
  * Types: binary, counting, recursise
  * When using Exceptions, be careful: if something grabs the semaphore and can raise an exception, ahve a `finally` statement to ensure the return
  * <u>binary semafores</u>
    * blocked or not
    * only one space
    * can only be taken if it is available
    * take/lock/down, P(), `xSemaphoreTake(<handler>, <maxDelay>) `, try to assume control; if sucessful, enter running state
    * give/unlock/up, V(), `xSemaphoreGive(<handler>)`, make it available
    * The names P and V come from dutch words
    * `vSemaphoreCreateBinary(<handler>) `
    * 
  * <u>counting semafores</u>
    * several spaces
    * (+) avoid information loss, because because several events can be simultaneusly “latched”
    * give increment count
    * take decrement count
    * `xSemaphoreCreateCounting(<maxCount>, <initialCount>) `
  * <u>recursive semafores</u>
    * ?

* **Mutex**

  * Or *lock*
  * mutual exclusion
  * ensure data consistency is maintained at all times
  * gurantee exclusive access to a resource, without being preemted
  * Can be seen as an specific type of binary semaphore? what is the difference? I have saw people refering to "mutex" as a general mecanism for task sincronization including semaphores, critical section, etc
  * (-) can cause priority inversion
  * A (non-recursive) mutex is either locked or unlocked.
  * <u>How it works</u>
    * No task can access the resource unless it holds the token
    * Take the token, access the resource, give the token back
    * similar to binary semaphore, but mutex automatically provide a basic ‘priority inheritance’ mechanism
  * <u>Priority inheritance</u>
    * the mutex-holder will be raised to the priority of the mutex-waiter
    * (-) more complex, so only recommended when necessary
    * (+) minimized the time of the priority inversion, because the mutex-holder wont be interrupted by medium priority tasks
  * <u>FreeRTOS API</u>
    * xSemaphoreCreateMutex(): return the handler
    * `xSemaphoreTake(<handler>, portMAX_DELAY )`: block until the mutex is successfully obtained
    * `xSemaphoreGive(<handler>)`

* **Design pattern - Reader-writer lock**

  * Allows concurrent access for read-only operations, while write operations require exclusive access
  * many people can read together, but no one can write
  * writer threads will not be able to acquire the lock while someone is reading it
  * when soeone is writting, no one can (1) write nor (2) read it
  * (+) good for mostly-read patterns
  * (-) can lead to write-starvation
  * you can minimize write starvation by preventing any new readers from acquiring the lock if there is a writer queued and waiting for the lock

* **Design pattern - double checked lock**

  * for scenarios where writning requires a lock, but reading no, and depneding if/when/who wrote you may need to write or to just read; first check if you _may_ need a lock, then actually try to acquire the lock, then check again;
  * Example usage: on the getInstance of a singleton: `if (instance == null) { synchronized (this) { if (instance == null) { instance = new Something(); } } } return instance;`

* **Design pattern - Active Object**

  * Decouples method execution from method invocation for objects that each reside in their own thread of control
  * ?

* **Design pattern - gatekeeper task**
  * task that has sole ownership of a resource
  * Only it can directly access the resource
  * a clean method of implementing mutual exclusion without the worry of
    priority inversion or deadlock
  *  uses a queue to serialize access
  * (+) Interrupts can also safelly use the gatekeeper

* **Design pattern - Actors**
  * For concurrent code
  * Similar to gatekeeper tasks
  * bans the notion of shared state
  * (+) easy to implement in a distributed manner
  * <u>actors</u>
    * execute concurrently, asynchronously, and
      share nothing.
    * work it out for themselves based on the messages they receive.
    * Actors may modify their own private state, but can only affect each other indirectly through messaging 
    * Two actors waiting for a message from each other can be a deadlock; the risk can be minimizing with timeouts in we waiting
  * <u>messages</u>
    * are immutable
    * are unidirectional: there’s no reply; If you want an actor to return a response, you include your own mailbox address in the message you send it, and it will (eventually) send the
      response as just another message to that mailbox.
    * Recipients of messages are identified by address, sometimes called "mailing address"; basically, a queue
    * are sent asynchronously 
    * Most implementations ensure that two messages sent from one actor to another maintain their order at arrival. 
    * For message handling, most implementations provide pattern matching
    * Send a message is always non-blocking
    * Read a message can be blocking
  * Distributed actor pattern
    * scale an application across multiple nodes
    * essentially integrates a message broker and the actor programming model into a single framework
    * frameworks: Akka, Orleans, Erlang OTP

* **Problems**

  * the best method of avoiding them is to consider its potential at design time
  * <u>priority inversion</u>
    
    * fenomeno onde uma tarefa de alta prioridade é obrigada a ficar esperando por que uma tarefa de baixa rioridade ficou ocupando o semáforo/mutex
  * <u>deadlock</u>
    * Ou *deadly embrace*
    
    * when two tasks cannot proceed because they are both waiting for a resource that is held by the other
      
    * a sequência das operações determina o acontecimento ou não do deadlock
    
    * condições para o deadlock acontece: exclusão mútua (um usar impede o outro), old and wait (you are holding some resources, but waiting to hold more), non preemtion (if you are alocated a resource, it can notbe taken from you by force), circular wait (one process wait for the other that waits for the first)
    
    * é possível identificar que acontecerá um deadlock em tempo de execução, mas é custoso e geralmente não é feito, então geralmente idetificamos que já aconteceu e nos recuperamos
    
    * como detectar: desenhar um diagrama com a alocação de processos e identificar ciclos? pode ser identificado de forma analítica
    
    * como recuperar (após detectar): preemção, rollback (vai salvando checkpoint dos estados; quando houver deadlock, voltar para um estado anterior e depois não deixar ele alocar os recursos problemáticos), matar o processo
    
    * tip: When allocating the same set of resources in different places in your code, always allocate them in the same order
      
      ![Traffic Deadlock - 9GAG](Images - RTOS/awQX5zx_460s.jpg)
      
      

### Gerenciamento de memória

* só nas trocas entre tarefas?
* se tu alocar pouca pilha a tua tarefa, voce se fode
* quando um tarefa acaba, o OS já libera a memória 
* 
* Boa prática: não faça malloc explicito dentro das tarefas, crie uma tarefa nova requisitando mais memória
* FreeRTOS allocates RAM from the FreeRTOS heap when a queue is created
*  
* freeRTOS provide better implementation (more deterministic and low footprint) than `malloc()`
* Used with pvPortMalloc() and vPortFree()
* defined in the files heap_1.c, heap_2.c and heap_3.c
* **heap_1**
  * does not free
  * all memory is allocate all at the begin of the code (statically), given by configTOTAL_HEAP_SIZE
* **heap_2**
  * can free
  * all memory is allocate all at the begin of the code (statically), given by configTOTAL_HEAP_SIZE
  * Efficient when allocation/free have always the same size, like an application that repeatedly
    creates and deletes the same task
* **heap_3**
  * can free
  * working heap defined by the ...?
  * thread safe by temporarily suspending the scheduler.
* **Stack overflow detection**
  * uxTaskGetStackHighWaterMark(<taskHandler>): query how near a task has come to overflowing the stack space allocated to it.
  * The stack overflow hook (or callback) is a function that is called by the kernel when it detects a stack
    overflow. Make available setting `configCHECK_FOR_STACK_OVERFLOW` to one or two (different levels of checking); (-) increase the time it takes to perform a context switch
  *  there is no real way of recovering from a stack overflow once it has occurred

### Others

* **Utils functions**

   * `vPrintString(<str>)`
   *  
   * `vTaskDelay(<int>) ` (receive number of tick interrupts); divide by the constant portTICK_RATE_MS to convert this to a more user friendly value in milliseconds
   * `vTaskDelayUntil(<int>)` instead specify the exact tick count value to wakeup; should be used
     when a fixed execution period is required
   * 
   * `vTaskStartScheduler()`: block normal executing, start threading; will only return if there is not enough heap memory remaining for the idle task to be created
   * 
   * ` prvSetupHardware()`: to be called in the begining
   * 
   * `printf-stdarg.c` that contains a minimal and stack efficient version of sprintf()
   * <u>task management</u>
     * pass NULL to the taskHandlers will affect your own task
     * `vTaskPrioritySet(<taskHandler>)`: change priority after the scheduler has been started
     * `uxTaskPriorityGet(<taskHandler>)`: get
     * `vTaskDelete(<taskHandler>)`
   * 
   * 
* **Configuration file**

  * FreeRTOSConfig.h
  * ?
* **Data types**
   * portable.h 
   * define a set of macros that detail the data types that are used
* **Low power modes**
   * tickless mode?

## Linux to RT

* Linux task manager: maximizes total throughput
* Have some very random latency sources, like page fault
* The jitter is high as fuck
* therefore the maximum execution time is probabilistic, not deterministic (unless you consider the worst case, which is generally not viable)
* There are some tool to analyse latency, like kernel shark (GUI) or Ftrace (CLI)
* **Possible solutions**
  * PREEMPTI_RT patch
    * allows to bound the latencies
    * https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/preemptrt_setup
  * User Space Partitioning: separate the RT functions
  * reserve some core (in a multicore processor) to the RT threads
  * Use a real time squeduler inside the linux kernel (like Cobalt) in parallel with the default linux squeduler
  * Dual OS partitioning using a hypervisor
  * Xenomai: a patch for the Linux kernel



## Applications / practical things

* **UART**
  * Placing each received character in a simple RAM buffer, then using a semaphore to unblock a task to process the buffer after a complete message had been received, or a break in transmission had been detected.
  * Interpret the received characters directly within the interrupt service routine, then use a queue to send the interpreted and decoded commands to a task for processing
  * This technique is only suitable if interpreting the data stream is quick enough to be performed entirely from within an interrupt.



























