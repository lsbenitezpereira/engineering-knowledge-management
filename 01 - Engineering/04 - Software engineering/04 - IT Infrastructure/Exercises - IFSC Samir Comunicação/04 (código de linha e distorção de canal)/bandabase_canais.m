%Packages for simulation
pkg load signal;
pkg load communications;

%Number of bits to be transmitted
Nb=1000;
% Generate Nb bits randomly
b=rand(1,Nb)>0.5;
%bit rate in bits/second
Rb=10e6;   
%Each waveform is represented by 10 samples so sampling
%frequency is 10 times the bit rate
fs=10*Rb;
NRZ_out=[];
RZ_out=[];
Manchester_out=[];


 
%Peak voltage +v of the waveform
Vp=1;
%Input bitstream as Bipolar NRZ-L waveform
for index=1:size(b,2)
 if b(index)==1
 NRZ_out=[NRZ_out [1 1 1 1 1 1 1 1 1 1]*Vp];
 elseif b(index)==0
 NRZ_out=[NRZ_out [1 1 1 1 1 1 1 1 1 1]*(-Vp)];
 end
end

%Input bitstream as Bipolar RZ waveform
for index=1:size(b,2)
 if b(index)==1
 RZ_out=[RZ_out [1 1 1 1 1 0 0 0 0 0]*Vp];
 elseif b(index)==0
 RZ_out=[RZ_out [1 1 1 1 1 0 0 0 0 0]*(-Vp)];
 end
end

%Input bitstream as Manchester-L waveform
for index=1:size(b,2)
 if b(index)==1
 Manchester_out=[Manchester_out [1 1 1 1 1 -1 -1 -1 -1 -1]*Vp];
 elseif b(index)==0
 Manchester_out=[Manchester_out [1 1 1 1 1 -1 -1 -1 -1 -1]*(-Vp)];
 end
end

%bitstream
figure(1)
hold on;
t= 0.1*1/Rb*(0:1:length(NRZ_out)-1);
subplot (3, 1, 1)
plot(t,NRZ_out);
axis([0 20/Rb -1.2 1.2])
title('NRZ');
xlabel('time (s)')
ylabel('Amplitude (V)')
subplot (3, 1, 2)
plot(t,RZ_out);
axis([0 20/Rb -1.2 1.2])
title('RZ');
xlabel('time (s)')
ylabel('Amplitude (V)')
subplot (3, 1, 3)
plot(t,Manchester_out);
axis([0 20/Rb -1.2 1.2])
title('Manchester');
xlabel('time (s)')
ylabel('Amplitude (V)')
hold off;

%%bitstream spectrum
NRZ_out_freq = 20*log10(abs(fftshift(fft(NRZ_out,4096))));
RZ_out_freq = 20*log10(abs(fftshift(fft(RZ_out,4096))));
Manchester_out_freq = 20*log10(abs(fftshift(fft(Manchester_out,4096))));

hold on;
Fs=fs;
figure(2)
subplot (3, 1, 1)
plot((-0.5:1/4096:0.5-1/4096)*Fs,NRZ_out_freq);
axis([0 400e6 -10 60])
title('NRZ');
xlabel('Hz')
ylabel('dB')
%%%%
subplot (3, 1, 2)
plot((-0.5:1/4096:0.5-1/4096)*Fs,RZ_out_freq);
axis([0 400e6 -10 60])
title('RZ');
xlabel('Hz')
ylabel('dB')
%%%%
subplot (3, 1, 3)
plot((-0.5:1/4096:0.5-1/4096)*Fs,Manchester_out_freq);
axis([0 400e6 -10 60])
title('Manchester');
xlabel('Hz')
ylabel('dB')
hold off;

%%%channel modelling  CAT5 cable
f1 = 10e6;
f2 = 100e6;
delta_f = f2-f1;
dB  = 40;
N = dB*fs/(22*delta_f);
f =  [f1 ]/(fs/2);
hc = fir1(round(N)-1, f,'low');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%filtering

NRZ_filtered = filter(hc,1,NRZ_out);
RZ_filtered = filter(hc,1,RZ_out);
Manchester_filtered = filter(hc,1,Manchester_out);

hold on;
figure(3)
subplot (3, 1, 1)
plot((-0.5:1/4096:0.5-1/4096)*fs,20*log10(abs(fftshift(fft(NRZ_filtered,4096)))),'color','r')
axis([0 240e6 -60 60])
title('NRZ filtrado')
grid on
%%%%%
subplot (3, 1, 2)
plot((-0.5:1/4096:0.5-1/4096)*fs,20*log10(abs(fftshift(fft(RZ_filtered,4096)))),'color','r')
axis([0 240e6 -60 60])
title('RZ filtrado')
grid on
%%%%%
subplot (3, 1, 3)
plot((-0.5:1/4096:0.5-1/4096)*fs,20*log10(abs(fftshift(fft(Manchester_filtered,4096)))),'color','r')
axis([0 240e6 -60 60])
title('Manchester filtrado')
grid on
hold off;


%%%%%%% filtered signals in time domain
figure(4)
subplot (3, 1, 1)
plot(t,NRZ_filtered);
axis([0 20/Rb -1.2 1.2])
title('NRZ');
xlabel('time (s)')
ylabel('Amplitude (V)')
grid on
%%%%%
subplot (3, 1, 2)
plot(t,RZ_filtered);
axis([0 20/Rb -1.2 1.2])
title('RZ');
xlabel('time (s)')
ylabel('Amplitude (V)')
grid on
%%%%%
subplot (3, 1, 3)
plot(t,Manchester_filtered);
axis([0 20/Rb -1.2 1.2])
title('Manchester');
xlabel('time (s)')
ylabel('Amplitude (V)')
grid on



