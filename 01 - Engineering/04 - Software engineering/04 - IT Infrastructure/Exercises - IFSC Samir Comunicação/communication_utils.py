__copyright__ = '''
MIT License

Copyright (c) 2021 Leonardo Benitez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
__license__ = "MIT"



import random
import math
import numpy as np
from scipy.integrate import simps

import matplotlib.pyplot as plt


################
## Utils
def random_sequence(total_bits_dado:int):
    bits = []
    for i in range(total_bits_dado):
        bits.append(random.getrandbits(1))

    return bits

def viz_modulation(t, signal, T_s, name:str):
    #Visualização do sinal no tempo e na frequência
    plt.figure(num=1,figsize=(20,7))
    plt.plot(t, signal)
    plt.title(f"Modulação {name}")
    plt.ylabel("Amplitude (V)")
    plt.xlabel("time (s)")

    S = np.fft.fft(signal)
    n = np.fft.fftfreq(S.size, d=t[1])
    plt.figure(num=2)
    plt.plot(n[0:int(n.size/32)], abs(S[0:int(S.size/32)]))
    plt.ylabel("|S(w)|")
    plt.xlabel("frequency (Hz)")
    
def bit_inverter(b:int, theshold:float) -> int:
    if b<theshold:
        return 1
    else:
        return 0
    
    
################
## Digital modulations
def mod_BPSK(s:list, fs:float, T_s:float, A:float=1):
    t, s_coded = bnrz(s, fs=fs, T_s=T_s, A=A)
    s_modulated = s_coded*(np.sqrt(2/T_s)*np.cos((2*np.pi/T_s)*t)) # BPSK
    return t, s_modulated

def demod_BPSK(t:list, s:list, fs:float, T_s:float):
    num_amostras_por_bit = int(fs*T_s)
    x = 0
    s_demod = []
    correlator = s*(np.sqrt(2/T_s)*np.cos((2*np.pi/T_s)*t))
    while x < len(t):
        integral = simps(correlator[x:x+num_amostras_por_bit], t[x:x+num_amostras_por_bit]) # Integrando em cada período de bit
        if integral >= 0: 
            a=1 # se for maior que zero provavelmente eh o bit 1
        else:
            a=0 #caso o valor da integral for menor do que zero (limiar de decisão), o bit provavelmente tem valor zero
        s_demod.append(a)
        x += num_amostras_por_bit
        
    return s_demod







def mod_BFSK(s:list, fs:float, T_s:float, Eb:float, base_functions:dict=None):
    if not base_functions:
        frequency_table = {
            1: 2/T_s,
            2: 3/T_s
        }
        base_functions = [
            lambda t: np.sqrt(2/T_s)*np.cos(2*np.pi*frequency_table[1]*t),
            lambda t: np.sqrt(2/T_s)*np.cos(2*np.pi*frequency_table[2]*t)
        ]
    t, s_coded = unrz(s, fs=fs, T_s=T_s, A=1)
    s_coded_inverted = [bit_inverter(bit, (s_coded.max() - s_coded.min())/2) for bit in s_coded]

    s_coded = np.sqrt(Eb)*np.array(s_coded)
    s_coded_inverted = np.sqrt(Eb)*np.array(s_coded_inverted)

    r1 = s_coded*base_functions[0](t)
    r2 = s_coded_inverted*base_functions[1](t)

    s_modulated = r1 + r2
    return t, s_modulated

def demod_BFSK(t:list, s:list, fs:float, T_s:float, base_functions:dict=None):
    if not base_functions:
        frequency_table = {
            1: 2/T_s,
            2: 3/T_s
        }
        base_functions = [
            lambda t: np.sqrt(2/T_s)*np.cos(2*np.pi*frequency_table[1]*t),
            lambda t: np.sqrt(2/T_s)*np.cos(2*np.pi*frequency_table[2]*t)
        ]
    num_amostras_por_bit = int(fs*T_s)
    x = 0
    s_demod = []
    for x in range(0, s.shape[0], num_amostras_por_bit):
        #corr phi1
        X = simps(s[x:x+num_amostras_por_bit]*base_functions[0](t[x:x+num_amostras_por_bit]), t[x:x+num_amostras_por_bit])

        #corr phi2
        Y = simps(s[x:x+num_amostras_por_bit]*base_functions[1](t[x:x+num_amostras_por_bit]), t[x:x+num_amostras_por_bit])
        
        if X-Y>0: 
            s_demod.append(1)
        else: 
            s_demod.append(0) 
        x += num_amostras_por_bit
    return s_demod



def mod_BASK(s:list, fs:float, T_s:float, Eb:float, base_functions:dict=None):
    if not base_functions:
        A = 5
        B = 10
        fc = 1/T_s
        base_functions = [
            lambda t: A*np.sin(2*np.pi*fc*t),
            lambda t: B*np.sin(2*np.pi*fc*t)
        ]
    t, s_coded = unrz(s, fs=fs, T_s=T_s, A=1)
    s_coded_inverted = [bit_inverter(bit, (s_coded.max() - s_coded.min())/2) for bit in s_coded]

    s_coded = np.array(s_coded)
    s_coded_inverted = np.array(s_coded_inverted)

    r1 = s_coded*base_functions[0](t)
    r2 = s_coded_inverted*base_functions[1](t)

    s_modulated = r1 + r2
    return t, s_modulated

def demod_BASK(t:list, s:list, fs:float, T_s:float, base_functions:dict=None, verbosity:int=0):
    '''
    Was done taking the mean euclidian distance between the signal and each base function, 
    then chosing the closest one
    '''
    if not base_functions:
        A = 5
        B = 10
        fc = 1/T_s
        base_functions = [
            lambda t: A*np.sin(2*np.pi*fc*t),
            lambda t: B*np.sin(2*np.pi*fc*t)
        ]
    num_amostras_por_bit = int(fs*T_s)
    x = 0
    s_demod = []
    plt.figure()
    for x in range(0, s.shape[0], num_amostras_por_bit):
        #corr phi1
        X = np.mean(np.sqrt(np.square(base_functions[0](t[x:x+num_amostras_por_bit]) - s[x:x+num_amostras_por_bit])))

        #corr phi2
        Y = np.mean(np.sqrt(np.square(base_functions[1](t[x:x+num_amostras_por_bit]) - s[x:x+num_amostras_por_bit])))
        
        if verbosity>1:
            plt.plot(t[x:x+num_amostras_por_bit], base_functions[0](t[x:x+num_amostras_por_bit]), color='red')
            plt.plot(t[x:x+num_amostras_por_bit], base_functions[1](t[x:x+num_amostras_por_bit]), color='blue')
            plt.plot(t[x:x+num_amostras_por_bit], s[x:x+num_amostras_por_bit], color='black')
        if X>Y: 
            s_demod.append(0)
        else: 
            s_demod.append(1) 
        x += num_amostras_por_bit
    return s_demod

################
## Line codes
def unrz(s:list, fs:float, T_s:float, A:float=1):
    '''
    Unipolar Non Return to Zero
    fs = frequencia do sinal amostragem
    T_s =  intervalo de bit (periodo por bit)
    A = amplitude do pulso
    '''
    num_amostras_por_bit = int(fs*T_s)
    t = np.linspace(0, T_s*len(s),num_amostras_por_bit*len(s))

    s_coded = np.array([])
    for i in s:
        if i == 1:
            s_coded = np.append(s_coded, A * np.ones(num_amostras_por_bit))
        elif i == 0:
            s_coded = np.append(s_coded, np.zeros(num_amostras_por_bit))

    return t, s_coded

def urz(s:list, fs:float, T_s:float, A:float=1):
    '''
    Unipolar Return to Zero
    '''
    num_amostras_por_bit = int(fs*T_s)
    t = np.linspace(0, T_s*len(s),num_amostras_por_bit*len(s))

    s_coded = np.array([])
    for i in s:
        if i == 1:
            s_coded = np.append(s_coded, A*np.ones(math.floor(num_amostras_por_bit/2)))
        elif i == 0:
            s_coded = np.append(s_coded, np.zeros(math.floor(num_amostras_por_bit/2)))
        s_coded = np.append(s_coded, np.zeros(math.ceil(num_amostras_por_bit/2)))

    return t, s_coded


def bnrz(s:list, fs:float, T_s:float, A:float=1):
    '''
    Bipolar Non Return to Zero
    '''
    num_amostras_por_bit = int(fs*T_s)
    t = np.linspace(0, T_s*len(s),num_amostras_por_bit*len(s))

    s_coded = np.array([])
    for i in s:
        if i == 1:
            s_coded = np.append(s_coded, A * np.ones(num_amostras_por_bit))
        elif i == 0:
            s_coded = np.append(s_coded, -A * np.ones(num_amostras_por_bit))

    return t, s_coded


def brz(s:list, fs:float, T_s:float, A:float=1):
    '''
    Bipolar Return to Zero
    '''
    num_amostras_por_bit = int(fs*T_s)
    t = np.linspace(0, T_s*len(s),num_amostras_por_bit*len(s))

    s_coded = np.array([])
    for i in s:
        if i == 1:
            s_coded = np.append(s_coded, A*np.ones(math.floor(num_amostras_por_bit/2)))
        elif i == 0:
            s_coded = np.append(s_coded, -A*np.ones(math.floor(num_amostras_por_bit/2)))
        s_coded = np.append(s_coded, np.zeros(math.ceil(num_amostras_por_bit/2)))

    return t, s_coded


def pqnrz(s:list, fs:float, T_s:float, A:float=1):
    '''
    Polar Quaternário NRZ
    '''
    num_amostras_por_bit = int(fs*T_s)
    t = np.linspace(0, T_s*len(s),num_amostras_por_bit*len(s))

    s_coded = np.array([])
    for idx in list(range(0, len(s), 2)):
        if s[idx]==0 and s[idx+1]==0:
            s_coded = np.append(s_coded, -3*A/2*np.ones(num_amostras_por_bit*2))
        elif s[idx]==0 and s[idx+1]==1:
            s_coded = np.append(s_coded, -A/2*np.ones(num_amostras_por_bit*2))
        elif s[idx]==1 and s[idx+1]==0:
            s_coded = np.append(s_coded, +A/2*np.ones(num_amostras_por_bit*2))
        elif s[idx]==1 and s[idx+1]==1:
            s_coded = np.append(s_coded, +3*A/2*np.ones(num_amostras_por_bit*2))

    return t, s_coded
