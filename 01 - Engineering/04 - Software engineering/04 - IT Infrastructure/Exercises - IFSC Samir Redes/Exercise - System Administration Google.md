# Conceptualization

**Learning Goals:**

1. Use the systems administration concepts you learned in the course to provide technical improvements to current processes.
2. Implement solutions based on an organization’s restrictions, like financial resources, number of users, etc.

**Overview**: You’ll take what you learned in the System Administration and IT  Infrastructure Services course and apply that knowledge to real-world  situations.

**Assignment**: For this writing project, you’ll be presented with three scenarios for  different companies. You’ll be doing the systems administration for each company’s IT infrastructure. For each scenario, present improvements to processes based on the company’s needs and current restrictions.  There’s no right or wrong answer to your consultation, but your  responses should explain the problem, the improvement, and the rationale behind them. Please write a 200-400 word process review for each  company presented to you.



# **Scenario 1**

**Software Company:**

Network Funtime Company is a small company that builds open-source software.  The company is made up software engineers, a few designers, one person  in Human Resources (HR), and a small sales team. Altogether, there are  100 employees. They recently hired you as a system administrator to come in and become their IT department.

When a new person is hired on, the HR person purchases a laptop for them to do their work. The HR  representative is unfamiliar with what type of hardware is out there; if a new employee requests a laptop, the HR person will purchase the  cheapest option for a laptop online. Because of this, almost everyone  has a different laptop model. The company doesn’t have too much revenue  to spend, so they don’t order laptops until someone gets hired at the  company. This leads to a few days of wait time from when someone starts  to when they can actually work on a laptop.

The company doesn’t  label their computers with anything, so if a computer is missing or  stolen, there’s no way to audit it. There’s no inventory system to keep  track of what’s currently in the fleet.

Once a computer is  purchased, the HR person hands it to the new employee to set up.  Software engineers that use Linux have to find a USB drive and add their preferred distribution to the laptop. Anytime someone needs something  from HR -- whether it’s office related or tech related -- they email the HR representative directly.

When a new employee gets a machine,  they’re given logins to use cloud services. They get a personal  orientation with HR to make sure they can login. This requires the HR  person to block off a few hours for every new employee. If an employee  forgets the login to their machine, they have no way to retrieve a  password and they have to reimagine their machine. Employees don’t have a strict password requirement to set for their computers.

The  company currently has many of their services in the cloud, such as  email, word processors, spreadsheet applications, etc. They also use the application, Slack, for instant communication.

------------------------

* If there is money available, buy a few laptops (1 to 5, or more depending on their future hire expectative) and pre-configure them, so a new employee will be able to begin his work immediately.
* Someone with technical familiarity with laptop's hardware should orientate the purchase of these notebooks AND organize guidelines to future purchases, considering the different necessities in the company (a notebook to a developer, or a designer, etc). It would be very positive if this same person stays available to solve tech-related issues with these notebooks, avoiding the HR representative to receive emails about it.
* The password management is not optimal, because they have to reimage the computer if the employee lost the password and because they do not ensure that the employee uses strong passwords. A simple option is to use some software to manage passwords, like KeePass (https://keepass.info/).
* Each one takes care of its computers, so there may be important information that is stored on them and, in case of disaster, this information would be lost. I would strongly recommend them to have backups and a disaster recovery plan.
* If the company heavily uses cloud services would be interesting to define a vendor that provide all the needed services, for instance: Microsoft has a solution to instant communication (MS Teams), spreadsheet and word processors (MS office online), and email (Outlook), and all those services have good integration between them, so would be interesting to preferentially use the services of Microsoft.
* They should use Directory Services to keep track of what’s currently in the fleet and to optimize the management of the computers. I would recommend OpenLDAP, because they use both Linux and Windows.

# Scenario 2

**Sales Company:**

W.D. Widgets is a small company that sells widgets. They’re mostly made up  of salespeople who work with lots of clients. You’ve taken over as the  sole IT person for this company of 80-100 people.

When HR tells  you to provision a machine for a new employee, you order the hardware  directly from a business vendor. You keep one or two machines in stock,  in case of emergency. The users receive a username that you generate for them. You then give them an orientation on how to login when they  start. You currently manage all of your machines using Windows Active  Directory. The company uses only Windows computers. When a new computer  is provisioned, you have to install lots of sales-specific applications  manually onto every machine. This takes a few hours of your time for  each machine. When someone has an IT-related request, they email you  directly to help them.

Almost all software is kept in-house,  meaning that you’re responsible for the email server, local machine  software, and instant messenger. None of the company’s services are kept on the cloud.

Customer data is stored on a single file server.  When a new salesperson starts, you also map this file server onto their  local machine, so that they can access it like a directory. Whoever  creates a folder on this server owns that folder and everything in it.  There are no backups to this critical customer data. If a user deletes  something, it may be lost for everyone.

The company generates a  lot of revenue and is rapidly growing. They’re expecting to hire  hundreds of new employees in the next year or so, and you may not be  able to scale your operations at the pace you’re working.



----------------------------





* They have to install lots of sales-specific applications manually onto every machine, so a Directory Service could automate this process.
* One sole IT personal to a fast-growing company with already 100 employees? I would strongly recommend them to hire more IT personal, thus avoiding a grow bottleneck because of the system administration.
* The company focus isn't IT, so to avoid the need to manage the hardware it would be a good idea to migrate their services to a cloud provider, like Azure or AWS.
* Unfortunately, they don't have good security policies: the customer data is stored on a single file server, without backup or redundancies. I would organize a backup plan for them and use RAID level 1 for redundancy.
* Instead of orientating new employees directly, I could record in video some simple instructions to optimize this process.
* The usage of instant communication services should be encouraged, so people with IT-related requests could communicate easily with me.

# Scenario 3

**Non-profit Company:**

Dewgood is a small, local non-profit company of 50 employees. They hired you as the sole IT person in the company. The HR person tells you when they  need a new computer for an employee. Currently, computers are purchased  directly in a physical store on the day that an employee is hired. This  is due to budget reasons, as they can’t keep extra stock in the store.

The company has a single server with multiple services on it, a file  server, and email. They don’t currently have a messaging system in  place. When a new employee is hired, you have to do an orientation with  them for login. You’re also responsible for installing all the software  they need on their machine, and mapping the file server to their  computer. The computers are managed through Windows Active Directory.  When an employee leaves, they’re currently not disabled in the directory service.

The company uses an open-source ticketing system to  handle all internal requests as well as external non-profit requests.  But the ticketing system is confusing and difficult to use, so lots of  the employees reach out to you directly to figure out how to do things.  In fact, so many things are difficult to find that employees typically  ask around when they have a question.

There are nightly backups in place of the file server. You store this information on a disk backup  and take it home with you everyday to keep it safe in case something  happens onsite. There’s also a small company website that’s hosted on  the single server at the company. This website is a single html page  that explains the mission of the company and provides contact  information. The website has gone down many times, and no one knows what to do when it happens.

------------------------



* It may be cheaper to buy directly from a business vendor.
* The management of the multiple services running in the single server could be improved using virtualization.
* The company will be more productive if they use an instant communication service. There are good cloud options, some of them free for small companies like Dewgood.
* Organize good management strategies in AD, for instance disabling the user when an employee leaves.
* The use of the ticketing system should be encouraged. There are cheap cloud options that could make it less confusing and difficult to use.
* It is a big security issue to take the backup disk with me every night. If you are concerned about an onsite disaster, you should do a remote backup. They should also consider having more than one single disk to backup and to organize a backup plan.
* There are very cheap cloud services to host their website, so it may be cost-effective to use them, considering that it takes a considerable amount of time from the sole IT personal to fix the website when it goes down.