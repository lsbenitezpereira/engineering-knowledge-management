#include <winsock.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define LOCAL_SERVER_PORT 12001
#define MAX_MSG 100

int main(int argc, char *argv[])
{
  
	WSADATA wsInformacao;
	SOCKET socketServidor;
	struct sockaddr_in service, cliente;
	int iResult,tamamnhoEndereco,bytes;
	char bufferEntrada[101],bufferSaida[101];

	// inicializa Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsInformacao);
	if (iResult != NO_ERROR) {
		printf("Erro na chamada da funcao WSAStartup().\n");
	}


	socketServidor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (socketServidor == INVALID_SOCKET) {
		printf("Erro na chamada da funcao socket(): %ld.\n", WSAGetLastError());
		WSACleanup();
		return 0;
	}


	// asocia a uma porta o servidor


	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr("172.16.108.155");
	service.sin_port = htons(9876);

	if (bind(socketServidor, (SOCKADDR *) &service, sizeof(service)) == SOCKET_ERROR ) {
		printf("Erro na chamada da funcao bind().\n");
		closesocket(socketServidor);
	    return 0;
	}


	tamamnhoEndereco = sizeof(cliente);
	printf("Aguardando...\n");
	while (1) {	

		
		bytes = recvfrom(socketServidor, bufferEntrada, 100, 0, (struct sockaddr *) &cliente, &tamamnhoEndereco);
		bufferEntrada[bytes] = '\0';
		printf("%s\n", bufferEntrada);

		
		sprintf(bufferSaida, "Quem esta %s", bufferEntrada);
		sendto(socketServidor, bufferSaida, strlen(bufferSaida), 0, (struct sockaddr *) &cliente, tamamnhoEndereco);
	}

	return 0;
}
