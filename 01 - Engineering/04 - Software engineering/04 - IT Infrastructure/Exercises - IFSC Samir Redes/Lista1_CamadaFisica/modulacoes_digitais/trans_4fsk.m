function trans_4fsk(noise)


%-------------------------------------------------------------------------%
%Call function to create file with signal to be used to transmission
gera_sinal_txt;
%-----------------------Systems�s Parameters------------------------------%
machinefmt  = 'native';             % Order for read/write bytes or bits
encoding    = 'UTF-8';              % Character encoding
fs          = 10e3;                 % Sample Frequency
fc          = 50e3;                 % Carrier Frequency
br          = 5e3;                  % Bit Rate
M           = 4;                    % Number of Symbols
ts          = 1/fs;                 % Sample Time
k           = log2(M);              % Bits for Symbols
m           = 0:M-1;                % Symbols�s Vector
baud        = br/k;                 % Baud rate
T           = 1/br;                 % Symbol�s Period
ppp         = round(fs/br);         % Points per period

%-------------------- Modulation's Parameters ----------------------------%
Eg          = 2;                    % Energy to basic pulse
d           = sqrt(2/Eg);           % control factor power
Eb          = sqrt(Eg);             % Mean Energy Bit

%-------------------------Information Signal------------------------------%
% Is used a pattern signal defined by a file txt
FID = fopen('signal_dig.txt','rb');
x1  = fscanf(FID,'%d');
ST  = fclose(FID);
t = 0:ts:(length(x1)-1)*ts;

%---------------------------Modulation 4-FSK------------------------------%
%Conversion Serial to Parallel to have 2 bit for each symbol, the
%conversion is point by point.
j = 1;
for i = 1:length(x1)/2;
    x_i(i) = x1(j);
    x_q(i) = x1(j+1);
    j=j+2;
end

% Convertion 2 bit to 4.
aux = 0;
h   = zeros(M,length(x_i));
for i = 1:length(x1)/2;
    aux = (x_i(i)*2)+ (x_q(i));
    switch(aux)
        case 0
            h(:,i)= [Eb 0 0 0];          %pino1
        case 1
            h(:,i)= [0 Eb 0 0];          %pino2
        case 2
            h(:,i)= [0 0 Eb 0];          %pino3
        case 3
            h(:,i)= [0 0 0 Eb];          %pino4
        otherwise
            h(:,i)= [0 0 0 0];
    end
end

% Buffer to keep signal code in same number of points of sample used to
% application of the carrier (200 KHz)
L = 20;             % Interpolation factor total
j = 1;
[k,n]=size(h);
h1 = zeros(M,L*n);
for i = 1:n;
    h1(1,j:j+(L-1)) = h(1,i);
    h1(2,j:j+(L-1)) = h(2,i);
    h1(3,j:j+(L-1)) = h(3,i);
    h1(4,j:j+(L-1)) = h(4,i);
    j = j + L;
end

% Define parameters to modulation
fs1     = fs*20;                            % Increase Sample frequency due interpolation
t1      = 0:(1/fs1):(length(h1)-1)*(1/fs1);
f       = ([-2^15:2^15-1]/2^16)*fs1;
h       = 0.7;                                % Index modulation 
fd      = h / (2*ts);                       % frequency deviate

% Defined frequency and carrier to each symbol
f1    = (fc - 2*fd);
f2    = (fc - fd);
f3    = (fc + fd);
f4    = (fc + 2*fd);
c1    = cos(2*pi*f1.*t1);
c2    = cos(2*pi*f2.*t1);
c3    = cos(2*pi*f3.*t1);
c4    = cos(2*pi*f4.*t1);

% Application of carrier in the signal used equation to modulation BFSK
y1    = h1(1,:).*c1 + h1(2,:).*c2 + h1(3,:).*c3 + h1(4,:).*c4;

%Application Nyquist transmission filter 
%Transmission Filter (Raised Cosine) - Fs=200KHz Ordem=50 - Fc=80KHz
%-Roll-off =0.15
Num     = [-8.91010704575857e-019 -0.000225259170682691 0.000325575281180007 -0.000217979119670361 2.29258417649406e-005 4.37241866875578e-019 0.000317116490412476 -0.000834430853669756 0.00112915870718186 -0.000823990192947162 2.1094898056869e-018 0.000695931848898281 -0.000499581450978272 -0.000648055071133562 0.00152670568659895 -5.30031472185194e-018 -0.00549310810232042 0.0139751884274892 -0.0207955699844335 0.0185141366008351 -2.1901842604404e-017 -0.0374616442239756 0.0892140499330372 -0.143373945315233 0.184591667841069 0.8 0.184591667841069 -0.143373945315233 0.0892140499330372 -0.0374616442239756 -2.1901842604404e-017 0.0185141366008351 -0.0207955699844335 0.0139751884274892 -0.00549310810232042 -5.30031472185194e-018 0.00152670568659895 -0.000648055071133562 -0.000499581450978272 0.000695931848898281 2.1094898056869e-018 -0.000823990192947162 0.00112915870718186 -0.000834430853669756 0.000317116490412476 4.37241866875578e-019 2.29258417649406e-005 -0.000217979119670361 0.000325575281180007 -0.000225259170682691 -8.91010704575857e-019];
y2      = conv(y1,Num);
off     = round(length(Num)/2);            %Calculate offset due convolution
y2      = y2(off:end-off+1);               %remove offset

%-------------------------------------------------------------------------%
%------------------------ Add Noisy AWGN ---------------------------------%
y = awgn(y2,noise);
%-------------------------------------------------------------------------%
%-------------------------- Plot Results ---------------------------------%
% Modulate Signal
figure(1)
subplot(2,1,1)
plot(t1,y2);
title('Sinal Modulado 4-FSK - Tempo')
subplot(2,1,2)
Y1 = abs(fftshift(fft(y2, 2^16)));
plot(f,Y1);
title('Sinal Modulado 4-FSK - Frequencia')

% Modulate Signal whith Noisy
figure(2)
subplot(2,1,1)
plot(t1,y);
title('Sinal Modulado 4-FSK com Ruido- Tempo')
subplot(2,1,2)
Y = abs(fftshift(fft(y, 2^16)));
plot(f,Y);
title('Sinal Modulado 4-FSK com Ruido- Frequencia')

%-------------------------- 4-FSK Demodulation----------------------------%
% Multiply carrier to make downconverter for each symbol�s frequency
z1     = y.*c1;
z2     = y.*c2;
z3     = y.*c3;
z4     = y.*c4;

% Decimation of signal using matched filter (Raised Cosine) 
z1    = decim20(z1);
z2    = decim20(z2);
z3    = decim20(z3);
z4    = decim20(z4);

% Decision Block to demodulate
big=0;
for i=1:length(z1);
    aux = [z1(i) z2(i) z3(i) z4(i)];
    big = max(aux);
    aux1 = find(aux==big);
    switch (aux1)
        case 1
            z_i(i) = 0;
            z_q(i) = 0;
        case 2
            z_i(i) = 0;
            z_q(i) = 1;
        case 3
            z_i(i) = 1;
            z_q(i) = 0;
        case 4
            z_i(i) = 1;
            z_q(i) = 1;;
    end
end

% Conversion Parallel to serial, the conversion is get point by point
z_i = [z_i(2:end) z_i(end)];
z_q = [z_q(2:end) z_q(end)];

clear j;
j=1;
for i = 1:2:length(x1);
    z(i:i+1)= [z_i(j) z_q(j)];
    j=j+1;
end

%Call function to convert demodulate signal to txt file.
ler_sinal_dig; 
%----------------------------Plot Results---------------------------------%
figure(3)
plot(t,z,t,x1-1.5,'r-');
title('Sinal Demodulado com Matlab');
axis([0 max(t) -max(x1)-1 max(x1)+1]);
grid;
legend('Sinal Demodulado','Sinal Original')
%-------------------------------------------------------------------------%
clear all;

