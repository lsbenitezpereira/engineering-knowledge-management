function trans_bpsk(noise)
%-------------------------------------------------------------------------%
%clear all;
%Call function to create file with signal to be used to transmission
gera_sinal_txt;
%-----------------------Systems�s Parameters------------------------------%
machinefmt  = 'native';             % Order for read/write bytes or bits
encoding    = 'UTF-8';              % Character encoding
fs          = 10e3;                 % Sample Frequency
fc          = 50e3;                 % Carrier Frequency
br          = 5e3;                  % Bit Rate
M           = 2;                    % Number of Symbols
ts          = 1/fs;                 % Sample Time
k           = log2(M);              % Bits for Symbols
m           = 0:M-1;                % Symbols�s Vector
baud        = br/k;                 % Baud rate
T           = 1/br;                 % Symbol�s Period
ppp         = round(fs/br);         % Points per period

%-------------------- Modulation's Parameters ----------------------------%
Eg          = 2;                    % Energy to basic pulse
d           = sqrt(2/Eg);           % control factor power
Eb          = d*Eg/log2(M);         % Mean Energy Bit

%-------------------------Information Signal------------------------------%
% Is used a pattern signal defined by a file txt
FID = fopen('signal_dig.txt','rb', machinefmt, encoding);
x1  = fscanf(FID,'%d');
ST  = fclose(FID);
t = 0:ts:(length(x1)-1)*ts;

%-------------------------- BPSK Modulation ------------------------------%
% Code signal using bipolar NRZ
x2 = nrzbi(x1,Eb,max(x1)*0.3);

% Interpolation by 20 to application of carrier using external function
x3 = inter20(x2);

% Define parameters to modulation
fs1     = fs*20;                            % Increase Sample frequency due interpolation
t1      = 0:(1/fs1):(length(x3)-1)*(1/fs1);
f       = ([-2^15:2^15-1]/2^16)*fs1;

% Application of carrier in the signal used equation to modulation
carrier = cos(2*pi*fc.*t1);
y1      = x3.*carrier;

%-------------------------------------------------------------------------%
%------------------------ Add Noisy AWGN ---------------------------------%
y = awgn(y1,noise);
%-------------------------------------------------------------------------%
%-------------------------- Plot Results ---------------------------------%
% Modulate Signal
figure(1)
subplot(2,1,1)
plot(t1,y1);
title('Sinal Modulado BPSK - Tempo')
subplot(2,1,2)
Y1 = abs(fftshift(fft(y1, 2^16)));
plot(f,Y1);
title('Sinal Modulado BPSK - Frequencia')

% Modulate Signal whith Noisy
figure(2)
subplot(2,1,1)
plot(t1,y);
title('Sinal Modulado BPSK com Ruido- Tempo')
subplot(2,1,2)
Y = abs(fftshift(fft(y, 2^16)));
plot(f,Y);
title('Sinal Modulado BPSK com Ruido- Frequencia')

%Plot Constellation of signal
scatterplot(complex(x2));
grid;
%-------------------------------------------------------------------------%
%---------------------------BPSK Demodulation-----------------------------%
% Multiply carrier to make downconverter
h       = y.*carrier;

% Decimation of signal using matched filter (Raised Cosine) 
h1    = decim20(h);

% Decision Block to demodulate
int_y(1)= h1(1);                        % Initialize variavel to integration
for i = 2:length(h1);
    int_y(i) = (int_y(i-1)+h1(i))/fs;   % Integration of signal
    if(int_y(i)>=0)                     
        z(i)=0;                        
    else                                
        z(i)=1;                        
    end
end
z = [z(2:end) 0]; %Discard first sample because integration.
%-------------------------------------------------------------------------%
%----------------------------Plot Results---------------------------------%
figure(4)
plot(t,z,t,x1-1.5,'r-');
title('Sinal Demodulado');
axis([0 max(t) -max(x1)-1 max(x1)+1]);
grid;
legend('Sinal Demodulado','Sinal Original')
%------------------------------------------------------------------------%

%Call function to convert demodulate signal to txt file.
ler_sinal_dig;

clear all;
