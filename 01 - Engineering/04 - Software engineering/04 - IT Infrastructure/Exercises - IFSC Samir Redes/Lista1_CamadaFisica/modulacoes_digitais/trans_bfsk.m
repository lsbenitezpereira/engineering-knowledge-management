function trans_bfsk(noise)

%-------------------------------------------------------------------------%
% clear all;
%Call function to create file with signal to be used to transmission
gera_sinal_txt;
%-----------------------Systems�s Parameters------------------------------%
machinefmt  = 'native';             % Order for read/write bytes or bits
encoding    = 'UTF-8';              % Character encoding
fs          = 10e3;                 % Sample Frequency
fc          = 50e3;                 % Carrier Frequency
br          = 5e3;                  % Bit Rate
M           = 2;                    % Number of Bits
ts          = 1/fs;                 % Sample Time
k           = log2(M);              % Bits for Symbols
m           = 0:M-1;                % Symbols�s Vector
baud        = br/k;                 % Baud rate
T           = 1/br;                 % Symbol�s Period
ppp         = round(fs/br);         % Points per period

%-------------------- Modulation's Parameters ----------------------------%
Eg          = 2;                    % Energy to basic pulse
d           = sqrt(2/Eg);           % control factor power
Eb          = sqrt(Eg);             % Mean Energy Bit

%-------------------------Information Signal------------------------------%
% Is used a pattern signal defined by a file txt
FID = fopen('signal_dig.txt','rb', machinefmt, encoding);
x1  = fscanf(FID,'%d');
ST  = fclose(FID);
t = 0:ts:(length(x1)-1)*ts;

%-------------------------- BFSK Modulation ------------------------------%
% Code signal using ON-OFF
x2 = codeonoff(x1,Eb,max(x1)*0.3);
% Buffer to keep signal code in same number of points of sample used to
% application of the carrier (200 KHz)
L = 20;             % Interpolation factor total
j = 1;
for i = 1:length(x2);
    x3(j:j+(L-1)) = x2(i);
    j = j + L;
end

% Define parameters to modulation
fs1     = fs*20;                            % Increase Sample frequency due interpolation
t1      = 0:(1/fs1):(length(x3)-1)*(1/fs1);
f1      = ([-2^15:2^15-1]/2^16)*fs1;
h       = 0.7;                              % Index modulation 
fd      = h / (2*ts);                       % frequency deviate

% Defined frequency and carrier to each symbol
f_bit1          = (fc + fd);
f_bit0          = (fc - fd);
carrier_bit1    = cos(2*pi*f_bit1.*t1);
carrier_bit0    = cos(2*pi*f_bit0.*t1);

% Application of carrier in the signal used equation to modulation BFSK
y1              = x3.*carrier_bit1 + (Eb-x3).*carrier_bit0;

%Application Nyquist transmission filter 
%Transmission Fliter (Raised Cosine) - Fs=200KHz Ordem=50 - Fc=60KHz -Roll-off =0.5 
Num     = [7.76297142456988e-034 4.67274492943356e-005 -3.87707477463236e-005 -1.44661085086985e-005 -5.1437849090923e-005 1.57276085872994e-019 7.01013510536594e-005 2.69100854094237e-005 9.87554749432386e-005 -0.000163771793689772 -1.57621694480505e-034 -0.000246735094443316 0.000225004929463786 9.34495669575377e-005 0.000375637377100049 -6.61791957215052e-019 -0.000696596267651357 -0.000325873761284664 -0.00152039758092473 0.00340105624785079 5.35760343794717e-034 0.0128431889820028 -0.0264561500769902 -0.0656749850401609 0.278005275105104 0.6 0.278005275105104 -0.0656749850401609 -0.0264561500769902 0.0128431889820028 5.35760343794717e-034 0.00340105624785079 -0.00152039758092473 -0.000325873761284664 -0.000696596267651357 -6.61791957215052e-019 0.000375637377100049 9.34495669575377e-005 0.000225004929463786 -0.000246735094443316 -1.57621694480505e-034 -0.000163771793689772 9.87554749432386e-005 2.69100854094237e-005 7.01013510536594e-005 1.57276085872994e-019 -5.1437849090923e-005 -1.44661085086985e-005 -3.87707477463236e-005 4.67274492943356e-005 7.76297142456988e-034];
y2      = conv(y1,Num);
off     = round(length(Num)/2);            %Calculate offset due convolution
y2      = y2(off:end-off+1);               %remove offset
%-------------------------------------------------------------------------%
%------------------------Add Noisy AWGN-----------------------------------%
y = awgn(y2,noise);
%-------------------------------------------------------------------------%
%-------------------------- Plot Results ---------------------------------%
% Modulate Signal
figure(1)
subplot(2,1,1)
plot(t1,y2,t1,x3,'r--');
title('Sinal Modulado 2-FSK - Tempo')
subplot(2,1,2)
Y1 = abs(fftshift(fft(y2, 2^16)));
plot(f1,Y1);
title('Sinal Modulado 2-FSK - Frequencia')

% Modulate Signal whith Noisy
figure(2)
subplot(2,1,1)
plot(t1,y,t1,x3,'r--');
title('Sinal Modulado 2-FSK com AWGN- Tempo')
subplot(2,1,2)
Y2 = abs(fftshift(fft(y, 2^16)));
plot(f1,Y2);
title('Sinal Modulado 2-FSK com AWGN- Tempo')
%-------------------------------------------------------------------------%

%---------------------------BPSK Demodulation-----------------------------%
% Multiply carrier to make downconverter for each syboml�s frequency
h_1     = y.*carrier_bit1;
h_0     = y.*carrier_bit0;

% Decimation of signal using matched filter (Raised Cosine) 
h1    = decim20(h_1);
h0    = decim20(h_0);

% Integrator Block to frequency 1
int1(1)= h1(1);                       % Initialize variavel to integration
for i = 2:length(h1);
    int1(i) = (int1(i-1)+h1(i))/fs;   % Integration of signal
end

% Integrator Block to frequency 0
int0(1)= h0(1);                       % Initialize variavel to integration
for i = 2:length(h0);
    int0(i) = (int0(i-1)+h0(i))/fs;   % Integration of signal
end

% Decision Block to demodulate
for i=1:length(int0);
    if (int1(i)>int0(i))
        z(i) = 1;
    elseif(int1(i)<int0(i))
        z(i) = 0;
    end
end
z = [z(2:end) 0]; %Discard first sample because integration.

%Call function to convert demodulate signal to txt file.
ler_sinal_dig; 

%----------------------------Plot Results---------------------------------%
figure(3)
plot(t,z,t,x1-1.5,'r-');
title('Sinal Modulado x Demodulado');
axis([0 max(t) -max(x1)-1 max(x1)+1]);
legend('Sinal Demodulado','Sinal Original')
%-------------------------------------------------------------------------%
clear all;