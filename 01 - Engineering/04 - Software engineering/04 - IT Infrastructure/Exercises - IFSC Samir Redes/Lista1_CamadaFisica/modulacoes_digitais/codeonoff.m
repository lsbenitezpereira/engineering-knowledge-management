function y=codeonoff(x,level,treshold);

% Function to code binary signal using on-off (to be used in FSK modulation)
% Parameters:   x = signal to code;
%               level = level of out after code
%               treshold = Level to comparation

for i = 1:length(x);
    if(x(i) >= treshold)
        y(i) = level;
    else
        y(i) = 0;
    end
end