function [mean] = mean_binary_sequence (s)
   t=0
   n=size(s)(2)
   for i=1:n
     t+=s(i)
   endfor
   mean = t/n
endfunction