#include <winsock.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[])
{
  
	WSADATA wsInformacao;
	SOCKET socketServidor,socketCliente;
	struct sockaddr_in service, cliente;
	int iResult,tamamnhoEndereco,bytes;
	char bufferEntrada[101],bufferSaida[101];

	// inicializa Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsInformacao);
	if (iResult != NO_ERROR) {
		printf("Erro na chamada da funcao WSAStartup().\n");
	}


    socketServidor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (socketServidor == INVALID_SOCKET) {
		printf("Erro na chamada da funcao socket(): %ld.\n", WSAGetLastError());
		WSACleanup();
		return 0;
	}


	// asocia a uma porta o servidor


	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr("127.0.0.1");
	service.sin_port = htons(27016);

	if (bind(socketServidor, (SOCKADDR *) &service, sizeof(service)) == SOCKET_ERROR ) {
		printf("Erro na chamada da funcao bind().\n");
		closesocket(socketServidor);
	    return 0;
	}
	
	// configura a fila de conex�es
	if (listen(socketServidor, 1) == SOCKET_ERROR) {
		printf("Erro na chamada da funcao listen().\n");
	}



	tamamnhoEndereco = sizeof(cliente);
	printf("Aguardando...\n");
	while (1) {	

        socketCliente = SOCKET_ERROR;
		if ((socketCliente = accept(socketServidor, NULL, NULL)) == SOCKET_ERROR) {
			continue;		
		}
	
		bytes = recv(socketCliente, bufferEntrada, 100, 0);
		bufferEntrada[bytes] = '\0';
		printf("Dados Recebidos: %s\n", bufferEntrada);

		sprintf(bufferSaida, "Ola %s!", bufferEntrada);
		send(socketCliente, bufferSaida, strlen(bufferSaida), 0);

		closesocket(socketCliente);

	}

	return 0;
}
