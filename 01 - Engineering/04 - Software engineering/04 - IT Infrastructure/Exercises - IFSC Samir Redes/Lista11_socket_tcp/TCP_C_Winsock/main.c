#include <winsock.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{

    WSADATA wsInformacao;
    struct sockaddr_in servidor;
    int iResult,tamamnhoEndereco,bytes;
	SOCKET socketCliente;
	struct sockaddr_in cliente;
	char bufferEntrada[101];
	

	// inicializa Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsInformacao);
	if (iResult != NO_ERROR) {
		printf("Erro na chadama da funcao WSAStartup()\n");
	}

    socketCliente = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (socketCliente == INVALID_SOCKET) {
		printf("Erro na chadama da funcao socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return 0;
	}

	
	// se conecta ao servidor
	tamamnhoEndereco = sizeof(servidor);
	servidor.sin_family = AF_INET;
	servidor.sin_addr.s_addr = inet_addr("127.0.0.1");
	servidor.sin_port = htons(27016);
	
	
    if (connect(socketCliente, (SOCKADDR*) &servidor, sizeof(cliente)) == SOCKET_ERROR) {
        printf("Erro na chadama da funcao connect().\n");
        WSACleanup();
        return 0;
    }


    send(socketCliente, "Fulano", 6, 0);
	bytes = recv(socketCliente, bufferEntrada, 100, 0);
	bufferEntrada[bytes] = '\0';
	printf("Resposta: %s\n", bufferEntrada);


  closesocket(socketCliente);
  WSACleanup();     
   
  system("PAUSE");	
  return 0;
}
