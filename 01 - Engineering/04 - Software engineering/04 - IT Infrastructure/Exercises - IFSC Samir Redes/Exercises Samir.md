## ESP8266 + venv + FreeRTOS

baixa tudo: `./config_esp8266sdk.sh`



instala venv: `sudo pip install venv`	

cria ambiente virutl: `python3 -m venv  Projeto_ESP8266/virtualPython/virtu/`

Ativa o ambiente birtual: `source ./Projeto_ESP8266/virtualPython/virtu/bin/bin/activate`



escolhe um exemplo do ESP: `cd /home/static/Projeto_ESP8266/esp-open-rtos/examples/blink`

dá o path do copilador: `source toolchain.sh`

substitui esptool.py (esp-open-sdk/xtensa-lx106-elf/bin/esptool.py) por uma vesão de python 3: `sudo pip install esptool`


compila e grava: `make flash -j4 -C .`
