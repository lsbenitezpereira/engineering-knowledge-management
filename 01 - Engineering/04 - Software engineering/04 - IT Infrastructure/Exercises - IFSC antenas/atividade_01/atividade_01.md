#### 1. Baseado na Lei de Ampère completa e na Lei de Lenz/Faraday, explique o mecanismo de geração e captação de ondas eletromagnéticas na faixa de radiofrequência realizada por antenas filamentares. Utilize figuras e esquemas para auxiliar na sua explicação.

Considere a existência de um elemento capaz de gerar um campo elétrico variante, uma fonte de tensão. Pela Lei de Ampere-Maxwell, temos que a variação do campo elétrico $\left(\frac{\partial \mathbf {E} }{\partial t}\right )$ causará um campo magnético $\bold B$:
$$
\nabla \times \mathbf {B} =\mu _{0}\left(\mathbf {J} +\varepsilon _{0}{\frac {\partial \mathbf {E} }{\partial t}}\right)
$$
Ou seja, a variação do primeiro causa o segundo e, pela Lei de Lenz/Faraday $\nabla \times \mathbf {E} =-{\frac {\partial \mathbf {B} }{\partial t}}$,  a variação do segundo também causa o primeiro. Perceba também que o termo $\frac{\partial \mathbf {E} }{\partial t}$ não requer a circulação de corrente elétrica, ou seja, o circuito de transmissão não precisa estar fechado.

Esse sistema de equações com $\bold E$ e $\bold B$ forma uma equação diferencial parcial, cuja resolução - que omitiremos por simplicidade - são equações no formato $y(t,x)=y_{max}\sin(kx-\omega t)$, que são equações de onda. Ou seja, esse padrão de campos elétrico e magnético oscilantes forma uma onda, capaz de transportar energia e informação.

![image-20201213124457854](Images - atividade_01/image-20201213124457854.png)

Para transformarmos essas ondas de volta em uma informação útil (por exemplo, um som que esta codificado no padrão de oscilação do campo elétrico), precisamos apenas construir um elemento que seja sensibilizado por essas ondas usando os mesmos mecanismos que foram usados para gerar-la: a variação do campo magnético causa um campo elétrico, que pode então excitar um amplificador de áudio, ser amostrado por um microcontrolador, etc.

#### 2. O que determina a polarização de uma onda eletromagnética?

A orientação do campo elétrico (polarização) é determinada pela fonte da onda (exemplo: posição da antena)

Por exemplo, para uma antena dipolo:

![Dipolo (antena) - Wikipedia, la enciclopedia libre](Images - atividade_01/440px-Dipole_receiving_antenna_animation_6_800x394x150ms.gif)

#### 3. O que significa coeficiente de reflexão de uma linha de transmissão?

Ao mudar de meio, parte da onda eletromagnética será refletida. Quanto melhor for o casamento das impedâncias características dos dois meio (ou seja, quanto mais próximas estas forem), menor será a reflexão. Para definir quão bom é o casamento utilizamos o coeficiente de reflexão, definido como:

![image-20201213102038410](Images - atividade_01/image-20201213102038410.png)

Na situação ideal em que $Z_L=Z_0$, temos que $\Gamma=0$.

#### 4. Classifique as ondas de rádio segundo os caminhos possíveis de propagação e sua relação com a frequência do sinal transmitido.

![image-20201213102218649](Images - atividade_01/image-20201213102218649.png)

#### 5. Calcular o comprimento de uma onda eletromagnética de frequência igual a 4,105 MHZ se propagando no vácuo.

$\lambda= c/f=73.08m$

#### 6. O que é uma fonte ou irradiador isotrópico e qual sua utilização?

Fonte ideal que propaga igualmente em todas as direções do espaço. Utilizada como abstração matemática para fins de cálculo.

#### 7. Calcular a densidade de potência de uma onda direta gerada por um irradiador isotrópico alimentado por um transmissor de 1W de potência, a uma distância de 3 km da fonte irradiadora.

$P=P_t/A$

$P=P_t/(4\pi r^2)$ (irradiador isotrópico)

$P=8.84\times 10^{-9}$ [W/m²]

#### 8. O que significa a impedância característica de um meio?

Relação entre o campo elétrico $\vec E$ e o campo magnético $\vec H$ em um meio. Se houver defasagem entre as duas ondas, a impedância será complexa, mas no caso do ar impedância é real e com valor de aproximadamente 377 ohms.

#### 9. Deseja-se que a distância máxima visual entre duas antenas seja igual a 54 km. Calcular as alturas dessas duas antenas, supostamente iguais, numericamente.

$S=3.569(\sqrt h_t + \sqrt h_r)$

$(54/(3.569*2))^2=57.2m$

#### 10. O que é o desvanecimento de uma onda eletromagnética e quais as suas principais causas?

Variações aleatórias ao longo do dia na intensidade do sinal. Ou seja, atenuações ou reforços variantes.

Causa mais comum: instabilidade das condições atmosféricas


#### 11. Explique o significado de frente de onda e de onda plana uniforme.

É a superfície que conecta pontos da onda que estão em fase com a fonte. O caso especial em que a superfície é um plano (ou seja, a onda se propaga de forma “reta”) é comummente utilizada para aproximar a frente de uma onda cuja fonte está muito distante.

#### 12. Seja um cabo coaxial com Z0= 50$\Omega$ terminado numa impedância de carga ZL. Calcular $\Gamma$, VSWR, a percentagem da potência incidente que é refletida e a perda de retorno para ZL= 80$\Omega$.

$\Gamma=\frac{80-50}{80+50}=\frac{3}{13}$

$VSWR = \frac{1+\frac{3}{13}}{1-\frac{3}{13}}=\frac{8}{5}$

$\%P_{refletida} = \frac{P_{refletida}}{P_{incidente}} = |\frac{3}{13}|^2 = \frac{9}{169}$%

$ret.loss = -20\log(\frac{3}{13})=12.74$ dB