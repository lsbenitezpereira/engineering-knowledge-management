# 1

para produzir características particulares de radiação que seriam dificeis alcançar com uma unica antena, como irradiar para todas as direções porém com maior intensidade em dois ângulos específicos. Além disso, esse padrão de irradiação provavelmente poderia ser modificado e adaptado, o que é difícil de fazer com uma unica antena.

por exemplo, se aumentarmos a potencia de duas das faces de um arranjo horizontal quadrado:

![image-20210423112252448](Images - atividade/image-20210423112252448.png)

# 2

os tipos mais comuns de arranjo são:

**arranjo horizontal:** variamos a quantidade de faces (antenas “lado a lado”), de forma a conseguirmos ajustar o ganho de azimute.

**arranjo vertical**: variamos a quantidade de níveis (antenas “empilhadas”), de forma a conseguirmos ajustar o ganho de elevação.

e, naturalmente, é possível utilizar uma combinações dos dois arranjos

# 3

Podemos ajustar:

* numero de faces (no arranjo horizontal) e o número de níveis (no arranjo vertical), assim como os dois juntos caso o arranjo for combinado
* geometria do arranjo (no arranjo horizontal), variando se elas estão dispostas em um triangulo, quadrado, pentagono, etc
* potencia de cada elemento
* defasagem de cada elemento
* A disposição mecânica dos elementos, ou seja, a posição deles em relação ao plano que os contem, “afundando” ou “sobressaindo” mais as antenas.

# 4

Se a impedância não estiver bem casada a transferência de potência da tinha de transmissão para a antena (ou vice versa) não será ótima. Isso acontece pois parte da energia transmitida é refletida e origina uma onda estacionária (conforme foi estudado no capitulo de propagação de ondas eletromagnética): 

Esse efeito pode ser medido pelo Standing Wave Ratio (SWR), dado por:

![image-20210423200124132](Images - atividade/image-20210423200124132.png)

> onde ![image-20210423200056468](Images - atividade/image-20210423200056468.png)

> e n = impedância intríseca do meio

Um bom circuito de casamento de impedâncias tem por objetivo minimizar a SWR

#  5

a impdência desejada da antena é z=300ohm, obtendo o perfeito casamento

a impedância atual é $z_{dipolo}$ = 73 + j42 ohm

Se utilizarmos um dipolo dobrado, obteremos uma impedancia mais próxima da impedancia desejada, pois $z_{dipolo dobrado} = 4*z_{dipolo} = 292 + j168$ ohm. A parte reativa podemos eliminar com um capacitor que possua $z_{capacitor} = -j168$ ohm , o que é obtido com:

$C=1/\pi f X = 1/ \pi 50*10^6*168 = 37.89 pF$

Com esse casamento, obtemos:

Tau = 

SWR = 





# 6

## a) 



r = 35786000 m

lambda=300/6500 = 0.04615 m

atenuação(db) =  $20*log(lambda/(4pi*r)))$ = -199.77 dB

## b)



PT=  20W = 43.01 dBm
GT =  35 dBi
GR = 40 dBi.

PR = potência recebida = -81.76 dBm