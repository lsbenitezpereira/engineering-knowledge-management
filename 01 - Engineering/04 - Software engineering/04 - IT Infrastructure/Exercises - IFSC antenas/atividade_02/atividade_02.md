## 1)

É o valor de resistencia que dissiparia (em calor) e mesma potência que é irradiada pela antena (em ondas eletromagnéticas). Ou seja, a resistencia de radiação é desejada para se irradiar mais potencia eletromagnética.

Não deve ser confundida com a resistênca ohmica da antena, que de fato dissipa energia na forma de calor e é indesejada.

## 2)

lambda = c/f = 300*10^6 / 300*10^6 = 1 [m]

lambda > 50*0.01 [m] -> dipolo infinitesimal



A resistencia de radiação de um dipolo infinitesimal é dada por:

$80\pi^2(L/\lambda)²$==78.76 m ohm

## 3)

O diagrama pode ser obtido de forma analítica - pela física da antena - ou de forma prática, medindo a intensidade do campo em diversos pontos ao redor da antena. 

Essa segunda abordagem foi utilizado na referencia [1] para ensaiar de forma “caseira” uma antena Yagi, utilizando o equipamento  NanoVNA (disponível na internet por cerca de 60 dólares), obtendo uma resposta condizendo com o esperado de forma teórica. 

Uma forma mais confiável e ainda de baixo custo de realizarmos esse ensaio é usando um equipamento como o HackRF em conjunto com o software GNUradio (disponível na internet por cerca de 350 dólares), conforme o projeto disponibilizado pelo laboratório SEEMOO Secure Mobile Networking Lab na referencia [2]

## 4)

R =$ \rho * L / A$

onde rho\_aluminio= 0.0292 [ohm mm^2/m]



L = comprimento da antena = 0.5 \* c/f = 0.5*300/950 [m]

e A=pi\*R^2 = pi 0,6^2   [mm^2]



R = 4 m ohm

## 5)

Da apostila dele:

*“para um dipolo de meia onda sem perdas, a parte real da impedância*
*de entrada é idêntica à sua resistência de radiação (uma vez que o máximo de corrente coincide*
*com seus terminais de alimentação).* 





*Para calcular a parte reativa da impedância de entrada deve-*
*se integrar a densidade de potência radiada na região de campos próximos (numa superfície*
*fechada envolvendo a antena e muito próxima a ela)”*

## 6)
**dipolo dobrado**

* Variação do dipolo meia onda
* condutores paralelos ligados na extremidade
* Impedancia: 300 ohms
* Perfil de radiação igual do dipolo
* (+) maior largura de banda
* (+) resistência pode ser alterada variando o tamanho e espaçamento dos condutores







dipolo em V

perfil diferente do dipolo: mais diretivo

maior direcionalidade, maior ganho

## 7)

Parecido com:

![image-20210227000925298](Images - atividade_02/image-20210227000925298.png)

Pelo diagrama do da apostila do cap 4, temos:

Z12 = +- 15 + j0 ohms

Conforme provado anteiormente, Z11 = 73 + j42,5 Ω

Então Zin1 = Zin2 = Z11 + Z12 = 88 + j42.5 ohm

## 8)

<img src="Images - atividade_02/image-20210227004950759.png" alt="image-20210227004950759" style="zoom:67%;" />

## Referencias

[1] https://www.rtl-sdr.com/measuring-the-radiation-pattern-of-a-yagi-antenna-with-a-nanovna/

[2] https://github.com/luelista/gr-radiationpattern