## 1)

![image-20210401003959893](Images - atividade_03/image-20210401003959893.png)

## 2)

Essa antena é constituida por um único elemento ativo (um dipolo meia onda), um elemento refletor, e vários elementos auxiliares (diretores). Dessa forma obtêm-se um ganho maior e uma melhor relação frente-costas

## 3)

Se a antena for “fininha” (circunferência muito menor do que o comprimento de onda) a sua irradiação será similar à um dipolo: omnidirecional e com polarização linear.

Já se a antena for “gordinha” (circuferência próxima do comprimento de onda) a sua irradiação é mais parecida com a antena espiral: diretiva e com polarização circular

[desenhar algo legal]

## 4)

Essa antena possui diversos elementos ativos (diferentemente da Yagi, que possuia apenas um), de diferentes tamanhos. Por possuírem dimensões diferentes, as astes são maximamente excitadas em frequência distintas e, portanto, a antena consegue operar em uma faixa de frequência larga.

## 5)

Na Front-fed parte da energia recebida/emitida é bloqueado, pois o alimentador está justamente na frente do sinal. Como alternativa, podemos utilizar como elemento refletor apenas uma parte da parábola, deixando alimentador fora do caminho do sinal, deslocar o alimentador do eixo da parábola, melhorando assim a eficiência da antena.

## 6)

O radome é uma estrutura construída ao redor da antena. Esse pode ser construído com material tal que reflita as ondas eletromagnéticas, diminuindo interferências laterais e diminuindo os efeitos de borda da antena (ambos são indesejados).

Outra opção é construir o radome com material tal que NÃO interfira com as ondas eletromagnéticas, utilizando-o apenas para proteger a antena do ambiente. Essa proteção é especialmente importante quando a antena opera em condições adversas, como em aplicações aeronáuticas ou marítimas.



## 7)

tags de RFID geralmente precisam ser pequenas, flexíveis e baratas. Antenas de quadro e antenas espirais são as mais utilizadas.

Tags com polarização circular possuem a vantagem de não precisar estarem alinhadas com o leitor, mas perdem aproximadamente 3db de ganho em relação à anteas com polarização linear



## 8) 

Antenas para radio difusão geralmente precisam possuir alta eficiência, visto que estarão transmitindo continuamente à grande potência. Também é desejado que estas sejam omnidirecionais, garantindo cobertura de sinal para toda a área desejada. Para cumprir esses requisitos é comum utilizarmos arranjos de diversas antenas.

Tão relevante quanto a antena em sí é projetar o sistema como um todo: que a torre de transmissão esteja no centra da área de serviço da emissora, que a torre seja alta o suficiente, que os cabos sejam de boa qualidade, etc.