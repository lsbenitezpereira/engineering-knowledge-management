resource "azuread_application" "app" {
  display_name               = "test-terraform-app"


  web {
    redirect_uris = [
      "http://localhost:8000/",
      "https://localhost:8000/",
      "https://${azurerm_public_ip.ip.ip_address}:8000/getAToken"
    ]

    implicit_grant {
      access_token_issuance_enabled = true
      id_token_issuance_enabled     = true
    }
  }
}

resource "azuread_application_password" "client-secret" {
  application_object_id = azuread_application.app.object_id
}
