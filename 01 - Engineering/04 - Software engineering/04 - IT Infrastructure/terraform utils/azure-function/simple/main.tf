  resource "azurerm_storage_account" "main" {
    name                     = var.name_simple
    resource_group_name      = azurerm_resource_group.main.name
    location                 = azurerm_resource_group.main.location
    account_tier             = "Standard"
    account_replication_type = "LRS"
  }


  resource "azurerm_storage_container" "main" {
    name                  = "contents"
    storage_account_name  = azurerm_storage_account.main.name
    container_access_type = "private"
  }

  resource "azurerm_application_insights" "main" {
    name                = var.name
    location            = azurerm_resource_group.main.location
    resource_group_name = azurerm_resource_group.main.name
    application_type    = "web"

    # https://github.com/terraform-providers/terraform-provider-azurerm/issues/1303
    tags = {
      "hidden-link:${azurerm_resource_group.main.id}/providers/Microsoft.Web/sites/${var.name}" = "Resource"
    }

  }

  resource "azurerm_app_service_plan" "main" {
    name                = var.name
    location            = azurerm_resource_group.main.location
    resource_group_name = azurerm_resource_group.main.name
    kind                = "FunctionApp"
    reserved            = true

    sku {
      tier = "Dynamic"
      size = "Y1"
    }
  }

  resource "azurerm_function_app" "main" {
    name                       = var.name
    location                   = azurerm_resource_group.main.location
    resource_group_name        = azurerm_resource_group.main.name
    app_service_plan_id        = azurerm_app_service_plan.main.id
    storage_account_name       = azurerm_storage_account.main.name
    storage_account_access_key = azurerm_storage_account.main.primary_access_key
    https_only                 = true
    version                    = "~3"
    os_type                    = "linux"
    
    app_settings = {
        "WEBSITE_RUN_FROM_PACKAGE" = "1"
        "FUNCTIONS_WORKER_RUNTIME" = "python"
        "APPINSIGHTS_INSTRUMENTATIONKEY" = azurerm_application_insights.main.instrumentation_key
        "APPLICATIONINSIGHTS_CONNECTION_STRING" = "InstrumentationKey=${azurerm_application_insights.main.instrumentation_key};IngestionEndpoint=https://japaneast-0.in.applicationinsights.azure.com/"
    }

    site_config {
          linux_fx_version= "Python|3.8"        
          ftps_state = "Disabled"
      }

    # Enable if you need Managed Identity
    # identity {
    #   type = "SystemAssigned"
    # }
  }
