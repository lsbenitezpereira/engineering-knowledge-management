resource "local_file" "env" {
    filename = "${path.module}/../notebooks/.env"
    content     = <<-EOT
      DB_CONN_STRING=${azurerm_cosmosdb_account.main.connection_strings[0]}
      DB_DATABASE=default
      FUNCTION_HOST=${module.azure-function-queries.function_app_default_hostname}
    EOT
}
