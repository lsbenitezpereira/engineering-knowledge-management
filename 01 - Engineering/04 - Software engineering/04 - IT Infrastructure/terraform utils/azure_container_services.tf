# We can use the innovationnorway's module or barebone, like in this tutorial: https://www.padok.fr/en/blog/terraform-azure-app-docker
resource "azurerm_container_registry" "main" {
  name                = var.name_simple
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  sku                 = "Basic"
  admin_enabled       = false
}


module "web_app_container" {
  source = "innovationnorway/web-app-container/azurerm"
  name = var.name_simple
  resource_group_name = azurerm_resource_group.main.name

  
  #docker_registry_url =  
  #docker_registry_username = 
  #docker_registry_password = 

  container_type = "compose"
  container_config = <<EOF
    version: "3.5"
    services:
      notebooks:
        container_name: SOC-notebooks
        build: 
          context: .
          dockerfile: ./notebooks/Dockerfile
        image: socautomations.azurecr.io/notebooks:latest
        container_name: notebooks
        ports:
          - "8888:8888"
EOF
}
