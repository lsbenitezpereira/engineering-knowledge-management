[TOC]

# Conceptualziation

-   or *regex*
-   string defined Search pattern
-   Varrem uma string procurando um padrão de texto
-   **regex processor**
    -   translates a regular expression in the above syntax into an internal representation that can be executed and matched against a string representing the text being searched in
    -   Thompson's construction algorithm: computes an equivalent nondeterministic finite automaton
-   **implementation**
    -   most languages have a regex engine
    -   [Speed benchmark](https://github.com/mariomka/regex-benchmark)


## How it works

* Each character is either a metacharacter, having a special meaning, or a regular character that has a literal meaning
* []
  * [a-z] (match all lower case letters from 'a' to 'z') 
* |
  * or
  * `gray|grey` can match "gray" or "grey".
* ()
  * grouping
  * define precedences
* \\
  * scape
  * the next metacharacter or character will be considered a character
* ^
  * starts with
  * 
* \$
  * Ends with
* quatifier
  * how often that a preceding element is allowed to occur
  * `?`:  zero or one
  * `*`: zero or more
  * `+`: one or more
  * `{n}`
  * `{min,}`
  * `{min,max}`
  *  
  * Repetitions such as `*` are *greedy*; when repeating a RE, the matching engine will try to repeat it as many times as possible
* wildcards
  * .
    * matches every character except a newline
    *  `a.*b` matches any string that contains an "a", and then the character "b" at some later point.
*  
*  
*  
*  
*  Regular expressions can be concatenated to form new regular expressions; if *A* and *B* are both regular expressions, then *AB* is also a regular expression
*  You can then ask questions such as “Does this string match the pattern?”, or “Is there a match for the pattern anywhere in this string?”.  You can also use REs to modify a string or to split it apart in various ways.
*  
*  
*  
* examples
  *  `ca*t` will match `'ct'` (0 `'a'` characters), `'cat'` (1 `'a'`), `'caaat'` (3 `'a'` characters), and so forth.

## Tools

* [Site para testar expressões regulares](http://regexpal.com/)

# Language specific

## python

* [python tutorial and console](https://www.w3schools.com/python/python_regex.asp#findall)

* Snippet:

  ```python
  pattern = f'\&{parameter}=(.*?)\&'
  result = re.findall(pattern, url)
  print(int(result[0]))
  ```

* match() and search() return None if no match can be found, or a match object if sucessfull

* capturing named groups:

  ```python
  s = "bob sue jon richard harry potter"
  r = re.compile('sue (?P<name>.*)ri')
  [m.groupdict() for m in r.finditer(s)]
  ```

  

* **match object**

  * where it starts and ends, the substring it matched, and more.
  * .group(): entire string matched
  * .group(<n>): group n matched
  * .start(): starting position of the match
  * .end(): ending position of the match


* 

* raw string notation

  * strings prefixed with `r`

  * for dont have to scape `\`

    | Regular String  | Raw string     |
    | --------------- | -------------- |
    | `"ab*"`         | `r"ab*"`       |
    | `"\\\\section"` | `r"\\section"` |
    | `"\\w+\\s+\\1"` | `r"\w+\s+\1"`  |

