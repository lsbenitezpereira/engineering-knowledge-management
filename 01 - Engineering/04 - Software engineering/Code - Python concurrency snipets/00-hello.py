import time
import asyncio

async def main():
    print('Hello ...')
    await asyncio.sleep(1)
    print('... World!')

t0 = time.time()
asyncio.run(main()) #->>>schedule it to be executed
t1 = time.time()
print(int(t1-t0))

