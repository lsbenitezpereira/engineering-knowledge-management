'''
Out of order result collection
'''

from concurrent.futures import ThreadPoolExecutor, as_completed
import time

def do_something(i: int) -> int:
    print('start', i)
    time.sleep(1)
    print('end', i)
    return i

with ThreadPoolExecutor() as executor:
    tasks = []
    for i in range(10):
        task = executor.submit(
            do_something,
            i,
        )
        tasks.append(task)
        
    print('-------------\nAll tasks sent\n-------------')
    for task in as_completed(tasks):
    	print('result collected', task.result())
    print('-------------\nAll tasks completed\n-------------')
