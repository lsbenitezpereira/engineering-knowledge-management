import time
import asyncio

async def batata():
    print('Hello ...')
    await asyncio.sleep(1)
    print('... World!')

t0 = time.time()
asyncio.run(batata)
#task = asyncio.create_task(batata())
#asyncio.run(task)
t1 = time.time()
print(int(t1-t0))

