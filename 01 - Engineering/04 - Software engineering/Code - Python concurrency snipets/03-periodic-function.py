'''
Out of order result collection
'''

from concurrent.futures import ThreadPoolExecutor
import time

done = False

def do_something() -> None:
    global done
    while(1):
        print("I'm doing something")
        if done:
            break
        time.sleep(1)

with ThreadPoolExecutor() as executor:
    task = executor.submit(do_something)
    time.sleep(2)
    done = True
print('-------------\nAll tasks completed\n-------------')
