'''
Collect the results in order
'''
from concurrent.futures import ThreadPoolExecutor
import asyncio
import time

def do_something(i: int) -> int:
    print('start', i)
    time.sleep(1)
    print('end', i)
    return i

async def do_all():
	with ThreadPoolExecutor() as executor:
		tasks = []
		loop = asyncio.get_event_loop()
		for i in range(10):
		    task = loop.run_in_executor(
		        executor,
		        do_something,
		        i,
		    )
		    tasks.append(task)
		    
		print('-------------\nAll tasks sent\n-------------')
		for response in await asyncio.gather(*tasks):
			print('result collected', response)
		print('-------------\nAll tasks completed\n-------------')
		
asyncio.run(do_all())
