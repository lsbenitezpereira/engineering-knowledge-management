/******************************************************************
 Title			winterGames library
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	Read a .csv file with the winner of the last winter games.
                The alocation of the vector is dinamic, and the alocation of the string "name" in
                each element of the struct is dinamic too.
				Line example: 1,Norway (NOR),13,13,10,36
 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "winterGames.h"

#define TAM_BUFFER 90   //max size of the line

struct Nations {
    int position;
    char *name;
    int gold;
    int silver;
    int bronze;
    int total;
};


/******************************************************************
Function:   readFile
Parameters: file name; stream used
Return:     address of the array of nations; Stream (by reference)
Comments:   end of array mark: position=-1
******************************************************************/
nations_t* readFile(char* file, FILE** fp){
    ///Local Variables
    char buffer [TAM_BUFFER];
    nations_t *nation;
    int i;
    int n_nations=0;

    ///Open file
    *fp = fopen (file, "r");
    if (*fp==NULL){
        perror ("Error in Main");
        exit (-1);
    }

    ///Count size and alloc memory
    while (fgets(buffer, TAM_BUFFER, *fp)!=NULL) n_nations++;
    rewind(*fp);
	nation = malloc (sizeof(struct Nations)*(n_nations+1));

	for (i=0; i<(n_nations); i++){
		if (fscanf(*fp, "%d,%90[^,],%d,%d,%d,%d", &nation[i].position, buffer, &nation[i].gold, &nation[i].silver, &nation[i].bronze, &nation[i].total) != 6){
            printf("Arquivo nao estruturado, erro na linha %d", i);
            exit (-1);
        }
        nation[i].name = malloc (strlen(buffer)+1);
        strcpy(nation[i].name, buffer);
		//printf("%4d| %36s | %4d | %4d | %4d | %4d |\n", nation[i].position, nation[i].name, nation[i].gold, nation[i].silver, nation[i].bronze, nation[i].total);
	}
    nation[i].position=-1;

	return nation;
}

/******************************************************************
Function:   printNations
Parameters: pointer to the nations array
Return:     void
Comment:    I assumed that the max string have 36 character.
            Otherwise the table will be unformatted (no error)
******************************************************************/
void printNations(nations_t* nation){
    int i;
    printf("| Pos.| %36s | gold | sil. | bro. | tot. |\n", "Nation");
    for (i=0; nation[i].position!=-1;i++){
  		printf("| %4d| %36s | %4d | %4d | %4d | %4d |\n", nation[i].position, nation[i].name, nation[i].gold, nation[i].silver, nation[i].bronze, nation[i].total);
    }
}

/******************************************************************
Function:   closeFile
Parameters: pointer to the nations array; stream used
Return:     zero if OK
******************************************************************/
int closeFile(nations_t* nation, FILE** fp){
    int i;
    for (i=0; nation[i].position!=-1;i++){
        //printf("line to free: %d\n", i);
        free (nation[i].name);
    }
    free(nation);
    if (fclose(*fp)!=0){
        perror ("Error in Main");
        return -1;
    } else
        return 0;
}


