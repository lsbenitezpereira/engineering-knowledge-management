/***********************************************
	INPUT USING SCANF
***********************************************/
for (i = 0; i < m; i++)
for (j = 0; j < n; j++) {
printf("A[%d][%d] = ",m,n);
scanf(%f", &A[i][j]); }

/***********************************************
	INITIALIZIN WITH ZEROS
***********************************************/
for(i=0,i<4;i++)
for(j=0,j<4,j++)
A[i][j] = 0;

/***********************************************
	Identity 
***********************************************/
for(i=0,i<3;i++)
for(j=0,j<3,j++){
if(i==j)A[i][j] = 1;
else A[i][j] = 0; }

/***********************************************
	C = A+B
***********************************************/
for(i=0,i<n;i++)
for(j=0,j<4,j++)
C[i][j]=A[i][j]+B[i][j];

/***********************************************
	C = A*B
***********************************************/
for(i=0; i<n; i++)
for(j=0; j<n; j++)
{
C[i][j] = 0;
for(k=0; k<n; k++)
C[i][j] += A[i][k]*B[k][j];
}

/***********************************************
	NORMA DE FROBENIOUS
***********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

///Function Prototype
float frobenious (int matrix[3][2], int i, int j);

///Main function
int main() {
	int matrix[3][2]={{1,2},{3,4},{5,6}};
    printf("frobenious rule: %f", frobenious(matrix, 2, 1));
	return 0;
}

float frobenious (int matrix[3][2], int i_max, int j_max){
    float result=0;
    int i,j;
    for (i=0; i<=i_max; i++)
        for (j=0; j<=j_max; j++)
            result += matrix[i][j]*matrix[i][j];
    return sqrt (result);;
}