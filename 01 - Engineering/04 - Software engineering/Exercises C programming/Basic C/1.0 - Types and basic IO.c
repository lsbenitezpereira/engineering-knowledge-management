/* ---------------------------------------------- */
/* FINANCE DEBT */
int main{
	value_original = input ("Enter with your original debt: ");
	days = input ("How many days did you delay the payment: ");
	value_new = value_original * (1 + 0.02 + days*0.1);
	disp (value_new, 'Your debt is now: ');
	return 0;
}

/* ---------------------------------------------- */
/* SECOND DEGREE EQUATION */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    float a, b, c;      // Coeficients
    float x1, x2, delt;  // Roots and delt
    printf("SECOND DEGREE ROOT CALCULATOR \n Enter with the coeficients A, B and C\n");
    scanf("%f %f %f", &a, %b, &c);
    delt = sqrt (b*b -4*a*c);
    x1 = (-b + delt)/(2*a);
    x2 = (-b - delt)/(2*a);
    printf("root 1 = %F \n root 2 = %f", x1, x2);
    return 0;
}

/* ---------------------------------------------- */
/* COMPLEX CONVERTER */
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>

int main()
{
    double mod, ang;
    double complex z;
    printf("POLAR TO RENTANGULAR CONVERTER \n\n");
    printf("Module: ");
    scanf("%lf", &mod);
    printf("\nAngle (in degrees): ");
    scanf("%lf", &ang);

    z=mod*cexp((ang*M_PI/180)*I);

    printf("\nRentangular form: %.3f %+.3f", creal(z), cimag(z));
    return 0;
}

