/****************************************************************************************************
 Title : 		area calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date :			22/05/2018
 Description :  
****************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void funcao_generica(float, float *, float *);

int main(void){
    float raio=3, area, comprimento;
    funcao_generica(raio, &area, &comprimento);
    printf("Area=%.2f PI \t Comprimento=%.2f PI", area/M_PI, comprimento/M_PI);
    return 0;
}

void funcao_generica(float raio, float *area, float *comprimento){
    *area=M_PI*raio*raio;
    *comprimento=2*M_PI*raio;
}

/****************************************************************************************************
 Title : 		mean calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date :			22/05/2018
 Description :  Resistor association calculator
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define END -32767

/// Function prototypes
void excluder (short int *, short int, short int);

/// Main function
int main(void) {
    int i;
    short int vect[] = {-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, END};
    printf("Original vector: ");
    for (i=0; vect[i]!=END; i++){
        printf("%d ", vect[i]);
    }
    printf("\n");

	excluder (&vect[0], -1, 1);
	printf("The new vector:  ");
    for (i=0; vect[i]!=END; i++){
        printf("%d ", vect[i]);
    }
    printf("\n\n");
	system ("pause");
	return 0;
}

void excluder (short int *vector, short int limit_inf, short int limit_sup){
    clock_t ini, fini;
    ini = clock();

    int i_write = 0, i_read;
    for (i_read = 0; vector[i_read]!=END; i_read++){
        if (vector[i_read]<limit_inf || vector[i_read]>limit_sup){
            vector[i_write] = vector[i_read];
            i_write++;
        }
    }
    vector[i_write] = END;

    fini = ini - clock();
    printf("\nExecution time: %f\n", fini/CLOCKS_PER_SEC);
}

/****************************************************************************************************
 Title : 		standard deviation calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date :			22/05/2018
 Description :  Receive a vector and calcules the standard deviation 
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

/// User defines 
#define END -32767

/// Function prototypes
float mean (float *);
float deviation (float *);

/// Main function
int main(void) {
    float vect[] = {1,1,1,2,-1, 5, 6, END};
    printf("%f", deviation (&vect[0]));
    system ("pause");
	return 0;
}

/// Used defined functions 
 float mean (float *vector){
     int i;
     float sum=0;
     for (i=0; vector[i]!=END; i++){
        sum += vector[i];
     }
     sum/=(i);
     return sum;
 }

 float deviation (float *vector){
     int i;
     float sum=0;
     float mean_value = mean(vector);
     for (i=0; vector[i]!=END; i++){
        sum += (vector[i]-mean_value)*(vector[i]-mean_value);
     }
     return (sqrt(sum/(i-1)));
 }

 /********************************************************
 Title  		point swaper
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description 	Swap two point in R3
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>

struct point {
    int x;
    int y;
    int z;
};

void swap_points (struct point *A, struct point *B){
    struct point temp;
    temp=*A;
    *A=*B;
    *B=temp;
}

int main (void){
    struct point A = {1,2,3}, B = {2,2,2};
    swap_points (&A, &B);
    printf ("Point A: (%d, %d, %d)\nPoint B: (%d,%d,%d)\n", A.x, A.y, A.z, B.x, B.y, B.z);
    return 0;
}
