/* ---------------------------------------------- */
/* BIGGER NUMBER CALCULATOR */
#include <stdio.h>
#include <stdlib.h>

int main(){
    printf("BIGGER NUMBER TESTER \n");

    float a, b, c;
    while (1){
        printf("\nSay first number: ");
        scanf("%f", &a);
        printf("Say second number: ");
        scanf("%f", &b);
        printf("Say third number: ");
        scanf("%f", &c);
        if (a>b){
            if(a>c){
                printf("The first number (%.2f) is the bigger", a);
            }
        } else if (b>c){
            printf("The second number (%.2f) is the bigger", b);
        } else{
            printf("The third number (%.2f) is the bigger", c);
        }
        printf("\n\n---------------\n\n");
    }
    return 0;
}



/* ------------------------- */
/*   TRAINING CALCULATOR     */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    printf("TRAINING TIME \n");

    unsigned char age, time;
    char sex;
    while (1){
        printf("\nYour age: ");
        scanf("%d", &age);
        if (age<=0 || age>120) age = 0;
        fflush(stdin);
        printf("Your sex (M/F): ");
        sex = getchar();
        if (sex == 'M' || sex=='m') sex='M';
        else sex='F';

        if (age>0 && age<=30){
            if (sex == 'M') time=30;
            if (sex == 'F') time=20;
        } else if (age>30 && age<=45){
            if (sex == 'M') time=30;
            if (sex == 'F') time=20;
        } else {
            time = 0;
        }

        printf("\nBest training time: %d min", time);

        printf("\n\n---------------\n\n");
    }
    return 0;
}
