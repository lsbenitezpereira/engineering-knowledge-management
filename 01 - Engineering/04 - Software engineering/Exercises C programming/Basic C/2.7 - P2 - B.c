/********************************************************
 Title          Exercise 1
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description    '\0' = void user
                '\n' = end of BD
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <stdbool.h>
#include <locale.h>

#define ATIVO 1
#define PREBLOQUEADO 0
#define BLOQUEADO -1

#define BD_PATH "bd.txt"
#define BD_SIZE 101

struct user{
    char login[20];
    char senha[20];
    int estado;
};

void admin_menu (struct user *usuarios);
void incluir (struct user *usuarios);
void excluir (struct user *usuarios);
void bloquear (struct user *usuarios);
void desbloquear (struct user *usuarios);
void imprimir (struct user *usuarios);
void gravar (struct user *usuarios);

int main (void){
    ///Main variables
    int i;
    struct user usuarios[BD_SIZE];
    struct user temp;
    FILE *fp;

    //strcpy(usuarios[0].login, "leonardo");
    //strcpy(usuarios[0].senha, "123");
    setlocale (LC_ALL,"portuguese");
    printf ("Bem vindo\n");
    ///Opening the file
    fp = fopen (BD_PATH, "r+w");
    if (fp == NULL){printf("banco de dados zuado"); return 0;}

    for (i=0; ; i++){
        if (feof(fp)) break;
        fflush (stdin);
        fscanf(fp, "%s", usuarios[i].login);
        fscanf(fp, "%s", usuarios[i].senha);
        fscanf(fp, "%d", &usuarios[i].estado);
        if (usuarios[i].login[0]=='\0') break;  // first null user, so it's the end of the DataBase
    }
    usuarios[i].login[0] = '\n';

    ///Login
    printf("\nDigite seu login:");
    fflush(stdin);
    scanf("%s", temp.login);
    printf("Digite sua senha:");
    fflush(stdin);
    scanf("%s", temp.senha);
    if (!strcmp(temp.login, usuarios[0].login) && !strcmp(temp.senha, usuarios[0].senha)){
		admin_menu (&usuarios[0]);
		rewind(fp);
		for (i=0; usuarios[i].login[0]!='\n'; i++) if(usuarios[i].login[0]!='\0'){ fprintf(fp, "%s %s %d\n", usuarios[i].login, usuarios[i].senha, usuarios[i].estado);printf("\nusuario valido");}else{printf("\nusuario inválido");}
        //if(usuarios[i].login[0]!='\0') fprintf(fp, "%s %s %d", usuarios[i].login, usuarios[i].senha, usuarios[i].estado);
        fputc('\0', fp);
	}
    else printf ("você nao tem acesso a este modulo");

    fclose (fp);
    return 0;
}

void admin_menu (struct user *usuarios){
    bool running = true;
    printf("\nBem vindo, admin");
    while(running){
        printf("\n<1> Incluir usuário");
        printf("\n<2> Excluir usuário");
        printf("\n<3> Bloquear usuário");
        printf("\n<4> Desbloquear usuário");
        printf("\n<5> Imprimir usuários");
        printf("\n<6> Sair e gravar modificações\n");
        switch (getche()){
        case '1':
            printf("\n\n---------------------");
            incluir(usuarios);
            printf("\n---------------------\n");
            break;
        case '2':
            printf("\n\n---------------------");
            excluir(usuarios);
            printf("\n---------------------\n");
            break;
        case '3':
            printf("\n\n---------------------");
            bloquear(usuarios);
            printf("\n---------------------\n");
            break;
        case '4':
            printf("\n\n---------------------");
            desbloquear(usuarios);
            printf("\n---------------------\n");
            break;
        case '5':
            printf("\n\n---------------------------------------------");
            imprimir(usuarios);
            printf("\n---------------------------------------------\n");
            break;
        default:
            printf("\n\n---------------------");
            running = false;
            break;
        }
    }
}


void incluir (struct user *usuarios){
    int i;
    for (i=0;(usuarios+i)->login[0]!='\0'; i++) if ((usuarios+i)->login[0]=='\n'){(usuarios+i+1)->login[0]='\n';break;};
    printf("\nQual o login desse camadarada?");
    fflush(stdin);
    gets((usuarios+i)->login);
    printf("\nQual a senha desse camadarada?");
    gets((usuarios+i)->senha);
    printf("o usuario está ativo (1), pré-bloqueado (0) ou bloqueado(-1)?");
    fflush(stdin);
    scanf("%d", &((usuarios+i)->estado));

}
void excluir (struct user *usuarios){
    int i;
    printf("\nQual usuário voce deseja excluir?");
    fflush(stdin);
    scanf("%d", &i);
    (usuarios+i)->login[0]='\0';
}
void bloquear (struct user *usuarios){
    int i;
    printf("\nQual usuário voce deseja bloquear?");
    fflush(stdin);
    scanf("%d", &i);
    (usuarios+i)->estado = BLOQUEADO;
}
void desbloquear (struct user *usuarios){
    int i;
    printf("\nQual usuário voce deseja desbloquear?");
    fflush(stdin);
    scanf("%d", &i);
    (usuarios+i)->estado = ATIVO;
}
void imprimir (struct user *usuarios){
    int i;
    printf("\n|Nº   |LOGIN     |SENHA     |ESTADO         |");
    printf("\n|-------------------------------------------|");
//    printf();
    for (i=0;;i++){
        //printf("%s", (usuarios+i)->login);
        if (usuarios[i].login[0]=='\n') break;
        if (usuarios[i].login[0]!='\0') printf("\n|%-5d|%-10s|%-10s|%-15s|", i, (usuarios+i)->login, (usuarios+i)->senha, ((usuarios+i)->estado==ATIVO)?("ativo"):(((usuarios+i)->estado==BLOQUEADO)?("Bloqueado"):("Pré-bloqueado")));
    }
}

/********************************************************
 Title			Question 2
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description	I didn't understand the exercise, that is didatic.
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>

int equation (float *x);

int main (void){
	float x[]={1,2,3,4,5,-2};

	equation (x);
	return 0;
}

int equation (float *x){
	int i, n;
	float s1, s2;
	for (i=0; x[i]>=-1; i++); n=i;
	printf("n=%d\n", n);
	if (i%2==0){printf ("meu merda"); return 0;}
	for (i=1, s1=0; i<=(n-2); i+=2) s1+=x[i];
	for (i=2, s2=0; i<=(n-3); i+=2) s2+=x[i];
	printf("%f", s1);
	printf ("the value of the equation is: %f", (x[n-1]-x[0]) * (4*s1+2*s2+x[n-1]) / (3*(n-1)));
	return 1;
}

/**********************************************
 Name        : Quesiton 3
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : 
 **********************************************/
 function a = equation (x, y)
     m=length (x);
     n=length (y);
     if m~=n then disp ("erro no programa");a=0;
     else
        a=(n*sum(x.*y) - sum(x)*sum(y))/(n*sum(x.*x) - sum(x)*sum(x));
     end
 endfunction
