/****************************************************************************************************
 Title : 		mean calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date  			24/04/2018
 Description : 	calculates the mean value between max and min values of an input (temperature)
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
int main(){
    float temp[30], t_max=0, t_min=100;
    int n=0;
    printf("Entre com as temperaturas medidas ou -50 para encerrar\n");
    while(n<29){
        printf("temperatura[%d] = ",n);
        scanf("%f",&temp[n]);
        if(temp[n]<=-50) break;
        if (temp[n] < t_min) t_min=temp[n];
        if (temp[n] > t_max) t_max=temp[n];
        n++;
    }
    //if (n==28) temp[29]=-50;
    printf("A temperatura media eh %.2f",(t_min+t_max)/2);
    return 0;
}

/****************************************************************************************************
 Title : 		moving mean calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date  			24/04/2018
 Description : 	calculates the moving mean value between the last 10 values of an input (temperature)
****************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
int main(){
    float temp[30], t_mean=0;
    int n=0, x;
    printf("Entre com as temperaturas medidas ou -50 para encerrar\n");
    while(n<29){
        printf("temperatura[%d] = ",n);
        scanf("%f",&temp[n]);
        if(temp[n]<=-50) break;
        for (x=n, t_mean=0
             ; x>=0&&x>(n-10); x--)t_mean=t_mean + temp[x];
        printf("Moving average (last 10)= %f\n\n", t_mean/10);
        //media = media + temp[n];
        n++;
    }
    //if (n==28) temp[29]=-50;
    printf("\nbyebye\n");
    return 0;
}
