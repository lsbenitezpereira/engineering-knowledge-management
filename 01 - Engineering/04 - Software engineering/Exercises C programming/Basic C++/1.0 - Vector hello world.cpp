#include <iostream>
#include <algorithm>    // std::sort & std::min_element & std::max_element
#include <vector>
#include <sstream>

using namespace std;

bool increasing (int i,int j) { return (i<j); }
bool decreasing (int i,int j) { return (i>j); }

int main()
{
    std::vector<int> buff;

    /* Hardwired initialization */
    //buff = {3,7,2,9,5,8,6};

    /* Sequential initialization */
    //for (int i=0; i<10; i++){
    //    buff.push_back(i);
    //}

    /* User input initialization */
    std::string line;
    int number;

    std::cout << "Enter numbers separated by spaces: ";
    std::getline(std::cin, line);
    std::istringstream stream(line);
    while (stream >> number){
        buff.push_back(number);
    }
    cout << endl;

    cout << "Original array: ";
    for (vector<int>::iterator vi=buff.begin(); vi!=buff.end(); vi++){
        cout << *vi << ' ';
    }
    cout << endl;


    cout << "\nSorted array: ";
    std::sort (buff.begin(), buff.end(), increasing);
    for (vector<int>::iterator vi=buff.begin(); vi!=buff.end(); vi++){
        cout << *vi << ' ';
    }
    cout << endl;


    cout << "\nReverse sorted array: ";
    std::sort (buff.begin(), buff.end(), decreasing);
    for (vector<int>::iterator vi=buff.begin(); vi!=buff.end(); vi++){
        cout << *vi << ' ';
    }
    cout << endl;


    cout << "Minimun element: " << *std::min_element (buff.begin(),buff.end()) << endl;
    cout << "Maximun element: " << *std::max_element (buff.begin(),buff.end()) << endl;


    return 0;
}
