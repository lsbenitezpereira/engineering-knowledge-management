/****************************************************
 * C++ project
 * Author: Leonardo Benitez
 * VS code tips 
 *   Build: Ctrl+Shift+B (with g++)
 *   Run: ./main
 *   Debug: F5
 *   Details in: https://code.visualstudio.com/docs/cpp/config-linux
****************************************************/

#include <iostream>
#include <math.h> /* sqrt */

using namespace std;

class Coordinate
{
private:
  int x;
  int y;

public:
  Coordinate(int x, int y)
  {
    this->x = x;
    this->y = y;
  }

  int getX()
  {
    return x;
  }
  int getY()
  {
    return y;
  }

  Coordinate operator+(const Coordinate &p)
  {
    Coordinate result(this->x + p.x, this->y + p.y);
    return result;
  }

  Coordinate operator-(const Coordinate &p)
  {
    Coordinate result(this->x - p.x, this->y - p.y);
    return result;
  }

  float operator*(const Coordinate &p){
    /* DOT product operation */
    float result = (this->x)*p.x + (this->y)*p.y;
    return result;
  }

  float normP2 (){
    return sqrt(x*x + y*y);
  }
  //int distanceTo(Coordinate){
  // }
};

int main()
{
  Coordinate p1 = Coordinate(1, 10);
  Coordinate p2 = Coordinate(2, 20);

  Coordinate p3 = p1 + p2;
  cout << "Hello world:" << p1*p2 << endl;
  return 0;
}
