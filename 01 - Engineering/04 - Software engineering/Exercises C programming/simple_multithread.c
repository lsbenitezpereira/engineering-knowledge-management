/*
To compile and run the code:
gcc main.c -pthread -o main && ./main
*/
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>

int value = 0;
void *runner(void *param); /* the thread */
int main(int argc, char *argv[])
{
    pid_t pid;
    pthread_t tid;
    pthread_attr_t attr;
    pid = fork();
    if (pid == 0) { /* child process */
    pthread_attr_init(&attr);
    pthread_create(&tid,&attr,runner,NULL);
    pthread_join(tid,NULL);
    printf("CHILD: value = %d\n",value); /* out: 5*/
    }
    else if (pid > 0) { /* parent process */
    wait(NULL);
    printf("PARENT: value = %d\n",value); /* out: 0 */
    }
}
void *runner(void *param) {
    value = 5;
    pthread_exit(0);
}
