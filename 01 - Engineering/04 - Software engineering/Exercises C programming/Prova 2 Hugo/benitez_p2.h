#include <vector>
using namespace std;


class Funcao{
    protected:
        Funcao *_f;

    public:
        virtual double operator()(double x) = 0;
        static double integrar(Funcao *f, double x0, double x1, double step);

        typedef vector<Funcao *> FuncaoVector;
};

class FuncaoAgregada: public Funcao{
    private:
        FuncaoVector _vec;
    public:
        FuncaoAgregada();
        void agrega(Funcao* f);

        double operator()(double x);
};

class Constante: public Funcao{
    private:
        double _value;

    public:
        Constante(double val);
        Constante();

        double getValue();
        double operator()(double x);
};

class Escalar: public Funcao{
    private:
        double _value;

    public:
        Escalar(double val, Funcao* f);
        Escalar(Funcao* f);
        Escalar(double val);
        Escalar();

        double getValue();
        double operator()(double x);
};

class Seno: public Funcao{
    private:
        double _value;

    public:
        Seno(double val, Funcao* f);
        Seno(Funcao* f);
        Seno(double val);
        Seno();

        double getValue();
        double operator()(double x);
};

class Cosseno: public Funcao{
    private:
        double _value;

    public:
        Cosseno(double val, Funcao* f);
        Cosseno(Funcao* f);
        Cosseno(double val);
        Cosseno();

        double getValue();
        double operator()(double x);
};

class Potencial: public Funcao{
    private:
        double _value;

    public:
        Potencial(double val, Funcao* f);
        Potencial(Funcao* f);
        Potencial(double val);
        Potencial();

        double getValue();
        double operator()(double x);
};

class Exponencial: public Funcao{
    private:
        double _value;

    public:
        Exponencial(double val, Funcao* f);
        Exponencial(Funcao* f);
        Exponencial(double val);
        Exponencial();

        double getValue();
        double operator()(double x);
};

void teste();