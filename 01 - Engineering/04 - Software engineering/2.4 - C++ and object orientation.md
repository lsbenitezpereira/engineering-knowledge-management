> “Funcionar depende da criatividade do usuário” Benitez

[TOC]

# OO model

* Method of design
* the hierarchy of classes is united via inheritance relationships
* **Abstraction**
  * focar nas acracterística essenciais 
* **Encapsulation**
  * compartimentalizing, defining boundaries to implementatons
  * Hide internal implementations 
  * “If a developer must consider the implementation of a component in order to use it, the value of encapsulation is lost.” Evans
  * Tip: design large-grained classes that address concepts our clients are already working with, they can understand and question the design as it grows, and they can cooperate in the design of test cases. My ophthalmology collaborators don't care about stacks; they do care about Legendre polynomial shape descriptions of corneas. Small encapsulations yield small benefits
* **Modularity**
  * the system can be decomposed into a set of cohesive and loosely-coupled modules
* **Hierarchy**
  * ordering of abstractions
  * classe = “é um”
  * Objeto = “é parte de”
  * Possuem herança (ver mais adiante)
* **Typing**
  * Enforcement of an class such that objects of a different class are not iterchangeble
  * Formas bem definidas
* **Concurrency**
  * Allow several object to work at the same time 
  * property that distinguishes an active object from an inactive 
  * Very important concept to parellel programming
* **Persistence**
  * propriedade de transcender o tempo
  * o objeto continua a existir quando seu criador deixou de existir 

## Classes

* ?

* **Objects**

  * Or *instance [of a class]*

  * Entidade individual, real ou abstrata

  * similar objects are defined by a class

  * <u>Every objects has</u>
    * State: properties (static) and values (dinamic)
    * Behavior: methods to act and react
    * Identify: unique not null identification (like memory address)
  
* **interface of a class**

  * public: can be accessed by anyone

  * private: cannot be accessed by anyone (other than the class itself)

  * protected: can be accesed by sub classes

  * Applied to properties and methods

  * Design tip: “The key to distinguishing a process that ought to be made explicit from one that should be hidden is simple: Is this something the domain experts talk about, or is it just part of the mechanism of the computer program?” Evans

    ![image-20200730005053220](Images - 2.0 - C++ and object orientation/image-20200730005053220.png)

  * <u>cross class interface visibilities</u>

    * some/all methods/properties are public jsut amogn two classes
    * C++ friend classes
    * Java package visibility

* **Properties**

  * :keyboard: C++: You can access variables directly, without `this`
  * It may be neccessary if you overwrite the variable name in the current scope

* **Methods**

  * service to: modify (set), select (get), interate, construct (create and initialize the object) or destroy (himself)
  * Usual categories of methods: getter (return property), setter (sets a property), helper (usually private), etc
  * <u>constructor</u>
    * same name as the class
    * you can have several constructors, with different prototypes
    * every class have a default constructor, that doesnt receive any parameter
    * you dont specify the return
    * you can pass arguments just as a normal function
    * :keyboard: C++: Example: `class Shape{public: }`
    * delegated constructor
      * to set default values
      * constructor without parameters that calls the normal constructor
  * <u>destructor</u>
    * :keyboard: C++: `~Rectangle`
    * usages:  free memory of allocated objects, among others
  * <u>Métodos estáticos</u>
    * podem ser chamados sem instanciar o objeto (:keyboard: C++: com operador `::`)
    * só podem acessar variáveis estáticas
    * no matter how many objects of the class are created, there is only one copy of the static member.

* **Singleton classes**

  * See more in *Design Patterns*
  * técnica para limitar o numero de instancias de uma classe
  * bota o construtor como privado
  * a instancia é armazeanda em uma variável estática, nicializada globalmente como null
  * iniciatiza a instancia (operador new) com uma função estatica (sugestã de nome: getInstanceOf)
  * Para expandir o singleton para outro numero alem de 1, a variável estática é um array e a função estática recebe um ID
  * code example: hugo repo file *HeloOO - TheRing.cpp*

* **Mixin classes**

  *  independently capture the unique properties
  *  extend classes and
     objects with new functionality without using direct inheritance
  *  merging functionality between existing
     things and new things.
  *  are meant to be mixed together with other classes to give some properties

* **Abstract class**

  * just interface, no implementation
  * is incomplete
  * cant be instantiated
  * :keyboard: C++:in a purely abstract class, the method will be defined like `virtual void foo()=0`
  * Java have the explicit concept of Interfaec inheritance, where only the function definition is inherited; you can only inherit from one class, but you can inherit from many Interfaces

* **Class Inheritance versus Interface Inheritance**

  * An object's class defines how the object is implemented. The class defines the object's internal state and the implementation of its operations. In contrast, an object's type only refers to its interface—the set of requests to which it can respond. An object can have many types, and objects of different classes can have the same type.
  * Languages like C++ and Eiffel use classes to specify both an object's type and its implementation
  * It's easy to confuse these two concepts, because many languages don't make the distinction explicit. 

### Polymorfism

* the exact property/method are defined in execution time
* childs override father
* To indicate in the UML Class Diagram, rewrite the method name
* :keyboard: C++ - Indicate to the compiler that the method is polymorfic: `virtual`
* gera um overhead
* you can write programs that expect an object with a particular interface, knowing that any object that has the correct interface will accept the request

### Namespace

* Each class defines a namespace
* isola as variáveis
* :keyboard: C++: ativa um namespace: `using namespace <name>`
* namespace std: ==biblioteca iostream==
* :keyboard: C++:Acessar uma variável em outro namespace: `<namespace>::<variable>`
* If parent and child classes have same named method, parent name and scope resolution operator(::) is used.

* 

## Relationships

* Generalização/Especialização: “é um(a)"

* We’ll write here about relationships among classes, but you can also formalize some thing about relationships among objects

  ![image-20230123220934368](Images - 2.0 - C++ and object orientation/image-20230123220934368.png)

### Association

* Or *links*
* How a client consumes information/behavior from a provider
* No dependency between classes
* Visibility: scope and access of the objects on each side of a
  link
* Synchronization: sequential (normal), guarded or protected (both in multi thread)
* Semantic Dependencies: directional, bydirectional, etc
* multiplicity: One-to-one, One-to-many, Many-to-many
* <u>Unidirectional association: ?</u>
* <u>Bidirectional association: ?</u> [good explanation](https://stackoverflow.com/questions/35198017/characteristics-of-bidirectional-relationships-in-uml)
* <u>Aggregation</u>: especial type of association com conotação parte-de-um-todo (ex: triangulo e linha)
* <u>Composição</u>: especial type of aggregation where the part does not exist wihtout the whole. Se pai deixou de existir, então filho deixa também (ex: casa e quarto)

### Inheritance

* passagem de propriedades e métodos

* One class depend on the other

* O filho nao deixa de ser o que o pai era

* (-) higher coupling

* (-) lower encapsulation (because exposes a subclass to details of its parent's implementation)

* (+) less code repetition

* (+) clear meaning for types

* superclasses (or *base class*): classes higher in the tree, from which a subclass (child) inherits

* subclasses (or *derivated class*): classes lower in the tree

* the constructor of the superclass is also executed

* you can specify which superclass’ constructor will be called, for instance in C++:

  ```C++
  class Rectangle: public Shape {
   Public:
     Retangle: Shape('blue'){}
  }
  ```

* **Types of inheritance**

  * single: one father per child
  * multiple: a class has more than one superclass above it in the class tree. Possui problemas com conflitos e redundancias (ex: diamond problem)
  * hybrid: ?
  * hierarchical: ?
  * multilevel: ?

* **Alternatives to inheritance**

  * <u>Object composition</u>
    * new functionality is obtained by assembling or composing objects to get more complex functionality

  * <u>Delegation</u>
    * extreme of object composition
    * two objects are involved in handling a request: a receiving object delegates operations to its
      delegate
    * For example, instead of making class Window a subclass of Rectangle (because windows happen to be rectangular), the Window class might reuse the behavior of Rectangle by keeping a Rectangle instance variable and delegating Rectangle-specific behavior to it. In other words, instead of a Window being a Rectangle, it would have a Rectangle.

# UML

* Notation to object oriented modeling
* baseada em diagramas graficos
* largely language-independent
* é mais fácil gerar uma UML a partir do código do que o contrário, pois o nível de abstração da UML é maior (e portanto algumas informações de baixo nível se perdem)
* Cada diagrama possui definições bem claras, mas sempre dá pra adicionar notas/comentários com explicações adicionais 
* [Excelent videoclass](https://www.youtube.com/watch?v=UI6lqHOVHic&list=PLUoebdZqEHTwbYD8oo6Wr81Xb7uCAh_oz)
* **Perfil**
  * extensões e varições e estereotipos da forma de fazer UMLs
  * SysML
  * MARTE
  * …
* **Tools**
  * Astah (recebi recomendação): http://astah.net/com-announcement
  * argoUML. free
  * umbrella. free
  * papyrus
    * free
    * pegada de gerar codigo
  * IMB rational
  * Together

## Diagramas estruturais

* Descrição estática das estruturas

* definir bem as interfaces entre os objetos (getter e setter) é mais importante do que as propriedades 

* **Diagrama de classes**
  
  * estrutura e relações das classes
  
  * Retangulo com nome, atributos e operações
  
    ![image-20200729222017988](Images - 2.0 - C++ and object orientation/image-20200729222017988.png)
  
  * <visibility><atributeName>:<type>
  
  * <operationName>(<parameters>):<returnType>
  
  * cada atributo e operação possui as visubilidades pública (+), protegida (#) ou privada (-). Default: atributos privados e operações públicas
  
  * operações estáticas: sublinhadas
  
  * classe abstrada: nome em itálico
  
  * A multiplicidade (na forma de numeros) é  escrita em cada ponta do relação
  
  * <u>Aggregation</u>
  
    * the objects have identical lifetime
    * the owner is responsible for the owned objects
    
    ![image-20200328225232797](Images - C++ and object orientation/image-20200328225232797.png)
    
  
* 
  
* **Diagrama de componentes**
  
  *  component: cluster of classes that are themselves cohesive but are loosely coupled relative to other clusters
  * as partes internas dos componentes são escondidas
  
* **Diagrama de objetos**

  * para um certo caso de uso
  * representa objeto que são instaniados

* **Diagrama de artefatos**
  
  * artefato = pedaço de código/documento/diagrama/etc. Produtos gerados a partir do trabalho de engenharia de software
  
* Diagrama de pacotes

* Diagrama de estrutura composta

* Diagrama de implamentação

## Diagramas comportamentais

* **Diagrama de casos de uso**
  * Situações onde os atores interagem com o sistema
  * a vezes é mais textual do que gráfica
  * A use case is not a description of a single functional requirement but  rather a description of something that provides significant value to the actor invoking it, in the form of scenarios. 
  * Good structure to describe a use case:
      * Use case name
      * Actor
      * Goal
      * Preconditions
      * Main flow
  * <u>Diagram Elements</u>
    * Our system: represented as big rectangle
    * Primary actor: initastes the use of the system. Placed in the left
    * Secondary actor: reacts to the primary actor. Placed in the right
    * Use case: task, action. circle inside system
  * <u>Relationships</u>
    * association: to actor from task. Solid line
    * include: Casos podem incluir ou excluir outros casos. Isso é repreentado por uma seta pontilhada escrita \<\<include|exclude\>\>
    * extend: when a “super” use case happens substimes when a “sub” use case is executed . Dashed arrow from super to sub written \<\<extend\>\>
    * generalization
  
* **Diagrama de atividades**
  * permite operações de fork e join de atividades
  
  * bom pra vizualizar paralelismos e precedencias
  
  * Dica: uma boa coisa é decompor cada caso de uso em um diagrama de atividades
  
  * you an represent paameters and outputs names (with their datatypes) with pins ([source](https://medium.com/@joshuaudayagiri/uml-activity-diagram-36aea144793b)):
  
  * ![img](https://miro.medium.com/v2/resize:fit:370/1*bg9bGagUyRf_kZvEiFl-YA.png)
  
  * You can group the activities in lanes to represent the actors performing them:
  
    ![img](https://www.smartdraw.com/activity-diagram/img/activity-diagram-swimlane.jpg)
  
* **Diagrama de transições** 

## Diagramas de interação

* Como os objetos conversam
* **Diagrama de sequência**
  * pode ser feita para a lógica inteira (usando operadores condicionais e loops) ou para apenas um caso de uso 
  * actor: figurinha de pessoa
  * object: quadrados
  * colunas = objects lifeline
  * linhas = chamadas e interações
  * reply is a dashed line
  * activation box: rectangle in the lifeline, showing in which period of time the object is active
* **Diagrama de interatividade**
* **Diagrama de colaboração**
  * muuito similar aos diagrama de sequência
* **Communication Diagram**: how the objects interact

## Outras notações

* BPMN

# C++

* Fortemente tipada
* NOT garbage collected
* 
* struct: class with Properties default visualization public
* Constructor method: same name as the class
* Curiosity: International Obfuscated C Code Contest, https://en.wikipedia.org/wiki/International_Obfuscated_C_Code_Contest
* 
## Input/Output

* Have fucking a lot of ways

![click on an element for detailed information](Images - 2.0 - C++ and object orientation/iostream.gif)

* **IOstream library**
  
  * print: `count << “string” << endl`
  
  * Insert operator `<<`: put the data that follows it into the stream that precedes it; ==para fazer o “untuple” do array é só encadear esse operator várias vezes?==
  
  * extraction operator `>>`: read the stream into the variable tha suceed it
  
  * space, tab, or new-line is used to separate two consecutive input operations
  
  * To read a string with space, use `getline (cin, mystr);`
  
  * An string can be used as stream if included `#include <sstream>`
  
  * Example of sstream to preprocess user input:
  
    ```c++
    #include <iostream>
    #include <string>
    #include <sstream>
    using namespace std;
    
    int main ()
    {
      string mystr;
      float price=0;
      int quantity=0;
    
      cout << "Enter price: ";
      getline (cin,mystr);
      stringstream(mystr) >> price;
      cout << "Enter quantity: ";
      getline (cin,mystr);
      stringstream(mystr) >> quantity;
      cout << "Total price: " << price*quantity << endl;
      return 0;
    }
    ```

* **IOmanip**
  * manipulate how the values are shown
  * Ex: `cout << setprecision(10) << myFloat <<endl`
* IOS
  
  * ==?==

## Data types

* **Fundamental data types**
  * Character: char, char16_t, char32_t, wchar_t (largest possible)
  * Integer: char, int, short int, long int, long long int
  * Floating-point: float, double, long double
  * bool
  * void (type) and null (ptr)
* **Literal constants**
  * Character
  * Integer
    * Prefix: 0=octal, 0x=hex
    * Sufix: u=unsigned, l=long, ll=long long
    * Default type: int
  * Floating-point
    * Sufix: f=float, l=long double
    * Default type: double
    * can be written as `6.02e23` (6.02 x 10^23)
  * char and string
    * ‘’ = character
    * “” = string
    * can be written multiline with \
    * Default type: char
    * Char prefix: u=16, U=32, L=w
    * String prefix: u8=UTF8, R=raw ()
  * bool
* **Compound data types**
  * Arrays
  * String
  * Pointer
  * Struct
  * aliases
    * different name by which a type can be identified
    * Old C sintax: `typedef existing_type new_type_name ;`
    * C++ sintax: `using new_type_name = existing_type ;`
  * Unions
  * enum

## Data Structures

* Or *Containers* (its official name)
* They are already implemented in Standard Library, so we can consider them as “advanced datatypes”
* **Iterator**
  * Forma padronizada de acesso
  * ==quais tipos tem um método iterador?==
* **Algorithm**
  * Funções genéricas para interagir com containers
  * Todo o acesso é feito por itarators
* **Types of containers**
  * <u>Sequence containers</u>
    * array
    * vector
    * deque
    * forward_list
    * list
  * <u>Container adaptors</u>
    * stack
    * queue
    * priority_queue
  * <u>Associative containers</u>
    * set
    * multiset
    * map
    * multimap
  * <u>Unordered associative containers</u>
    * unordered_set
  * <u>unordered_multiset</u>
    * unordered_map
    * unordered_multimap
* **Array**
  * Fixed size
  * Similar the basic data type, but with OP functions
  * :book: [reference](http://www.cplusplus.com/reference/array/array/)
* **Vector**
  * `#include <vector>`
  * tamanho variável
  * gerenciamento automático da memória
  * vetor de ints: `vector <int> nome`
  * Inner implementation: dynamically allocated array
  * `.push_back(vector)`: add a copy of the parameter to the vector
* **List**
  * dont have direct access
  * `.push_back`
  * `.push_front`
* **String**
  * can be resized
* **Deque**
  * double ended queue
  * deque<int> d;
  * d.push_back(<value>)
  * d.push_front(<value>)
* **Set**
  * Inner implementation: árvore binária baçanceada
* **Map**
  * Coleção de dados chave->valor
  * Similar à hashtable
  * Inner implementation: árvore binária baçanceada
* **Others**
  * stream?
    * have operators << and >>
    * `.str()`

## OO

### Templates

* Metaparâmetros para classes
* permite setar algumas coisas (ex: tipo de dados) on the fly na instanciação da classe
* A classe mesmo só é definida em tempo de compilação
* Os metaparâmetros podem ser opcionais

### Classes

* Properties default visualization: private

* The constructor have the same name as the class

* The destructor is ~<name>

* **Objetos**
  
  * Operadores
    * Igualdade: cópia 
  
* **Herança**

  * Access specifier (public, protected or private):

    ![image-20200730005318687](Images - 2.0 - C++ and object orientation/image-20200730005318687.png)

  * Contrutores e destrutores não são herdados

  * Overloadings não são herdados

  ![image-20200730004423041](Images - 2.0 - C++ and object orientation/image-20200730004423041.png)

### Overloading

* **Function overloading**
  * declaração com o mesmo nome no mesmo escopo, mas argumetos diferentes e e implementações distintas 
* **Operator overloading**
  * mesmo operador atuando de forma diferente para cada classe
  * definimos o operador da nossa classe junto com a própria classe
  * define o que um operador significa para aquela classe
  * precisa retornar um objeto do seu mesmo tipo
  * os operandos são passados passados como ponteiros
  * Os operandos não podem ser modificados pela função
  * O operador de asignment `=` já é implementado por padrão (pode ser modificado)
  * Retorno `friend`: retorna outra coisa que não a sua classe, conseuquentemente podendo acessar atributos privados da classe amiga. Quebrando a divisão estritia entre classe 

## Dynamic allocation

* `pointer = new type`

* `pointer = new type [number_of_elements]`

* if allocation fails, an `bad_alloc` exception is thrown  

* Other way of check allocation error:

  ```c++
  int * foo;
  foo = new (nothrow) int [5];
  if (foo == nullptr) {
    // error assigning memory. Take measures.
  }
  ```

* `delete pointer;` releases the memory of a single element allocated using new

* `delete[] pointer;`: releases the memory allocated for arrays of elements using new and a size in brackets []

	## Data persistence

* the entire objects becomes persistent
* declared on creation
* instead of `new T()`, one would call `new (db) T()` to create a persistent object
* provide support for starting a transaction, and for committing it or rolling it back
* Query language: Iterators provide support for simple selection queries.

## Exception handling

* `try {...} catch (const char* exp) {...} `
* `throw “message”`

## Memory considerations

* Dont have garbage collect
  
* Escrever o metodo dentro da definição da classe: fica como inline (e o compilador via obedecer o programador)
  
* **Variaveis**
  
  * globais: armazenado no segmento de dados
  * Locais estáticas: stack
  * Locais dinâmicas: heap
  * Globais (estáticas ou ==dinâmicas==): segmento de dados. Inicializada
  
* **static declaration**
  
  * `Obj obj;`
  
* **Dinamic declaration**
  
  * ==retorna um ponteiro==
  
    ``
    Obj* obj_ptr;
    obj_ptr = new Obj;
    ``
  
  * O operator new é equivalente ao malloc do C
  
  * Acesso a funções e variáveis feito com `->`
  
  * A função destrutora é chamada com `delete obj_ptr`

## Files

* The `fstream` library allows us to work with files.

## Others

* **Lambda expressions**
  
  * `[capture](parameters)->return-type {body}`
  
* **for_each()**
  
  * similar to `map` in other languages
  
  * https://en.cppreference.com/w/cpp/algorithm/for_each
  
  * `begin()` and `end()`  will take the right member for you with the call  context in mind. For example in a const member function, if the  container is a member too, begin(vw) will return vw.cbegin() and end(vw) will return vw.cend() that are new too.
  
    Using begin(vw) and  end(vw) makes the code more generic as you don't have to specify wich  type of access you need. You can also easily extend any loop written  using begin() and end() by writing their specialization for your own  types.
  
* **Random**
  * `srand(time(NULL))`: seed, already with a good one
  * `rand()%n`: random between 0 and n
  
* **Vcpkg**
  * manage C and C++ libraries
  * `CMake projects should use: "-DCMAKE_TOOLCHAIN_FILE <path>"`
  
* **Boost**

  * set of libraries for the C++
  * book: https://www.packtpub.com/product/learning-boost-c-libraries/9781783551217
  * book: https://theboostcpplibraries.com/introduction
  * <u>Boost.Build</u>
    * or *B2*
    * build library, high-level build system
    * same as bjam?
    * Configuration file: Jamfile.jam
    * https://boostorg.github.io/build/tutorial.html
    * https://boostorg.github.io/build/manual/master/index.html
    * Boost.jam: interpreted, procedural language
    * To invoke B2, type b2 on the command line

* * 
  
* **gtest**

  * unit testing library 

###  Templates

* parameterized types

### GSL

* GNU Scientific Library
* [seems](https://community.st.com/s/question/0D50X0000ADBe5i/can-i-use-gnu-scientific-library-with-stm32) to compile even in stm32; maybe with some [modifications](https://e2e.ti.com/support/tools/ccs/f/81/t/522979?Using-the-GNU-Scientific-Library-with-Code-Composer-Studio-for-ARM-Cortex-M4-processor))
* **My instalation**
  * version 2.6
  * compiled and installed with make
  * installed to `/usr/local/lib`
  * file `libgsl.a`?
  * If you ever happen to want to link against installed libraries
    in a given directory, LIBDIR, you must either use libtool, and
    specify the full pathname of the library, or use the `-LLIBDIR'
    flag during linking and do at least one of the following:
       - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
         during execution
       - add LIBDIR to the `LD_RUN_PATH' environment variable
         during linking
       - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
       - have your system administrator add LIBDIR to `/etc/ld.so.conf'