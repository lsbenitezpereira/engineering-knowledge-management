[TOC]

# **Conceituação**

-   Formata a informação entregue pelo HTML

-   CSS Aural → formata a leitura de tela (para deficientes)

-   **Estrutura básica (exemplo)**

    -   Esse código é uma *regra*:
    
        ```
        Div { Background-color: red }
        ```
    
    -   Div é o *seletor*
    
    -   Background-color é a *propriedade*
    
    -   Red é o *valor*

-   **Onde escrever**

    -   Inline, no head ou em um arquivo separado:

![](./Images/2.0 - CSS/media/image3.png){width="4.864583333333333in"
height="0.17708333333333334in"}

![](./Images/2.0 - CSS/media/image2.png){width="2.1041666666666665in"
height="1.0in"}

![](./Images/2.0 - CSS/media/image1.png){width="4.052083333333333in"
height="0.16666666666666666in"}

-   **Herança** → A maioria das propriedades do pai passam pro filho

-   **Precedência** → Caso uma propriedade for modificada duas vezes de
    forma diferente (*ex:* uma por herança e outra diretamente), fica
    valendo a que for mais específica

## Seletores

-   A quem serão aplicadas as regras

-   \#nome → seleciona uma ID (usada uma única vez na página)

-   .nome → seleciona uma class (usada várias vezes)

-   **Encadeando seletores**

    -   Div p {} → Só os "p" dentro de uma "div"

    -   Div\#navbar → Só as "div" com a id "navbar"

    -   Div\#navbar h1 → Só os "h1" dentro de uma "div" com id "navbar"

    -   .mensagem.obs → Só os elementos com a classe "mensagem" E "obs"

    -   H1 + p → Só os "p" quem vêm depois de um "h1"

    -   Strong, p, \#navbar → agrupa seletores (pega todos eles)

    -   E \> F → Só os E que são filhos diretos de F

-   **Pseudo-classes**

    -   Não são feitas pelo programador, e sim pelo navegador

    -   São escritos na forma de elemento:pseudo-classe

    -   :hover → quando o mouse em cima

    -   :first-child → Só os que são o primeiro filho do pai

    -   :nth-child(2) → Só os que forem o segundo filho do pai

-   **Pseudo-elemento**

    -   Cria um elemento que não existe

    -   São escritos na forma de elemento::pseudo-elemento

    -   ::first-letter → Seleciona a primeiro letra desse elemento

    -   ::after → insere um conteúdo antes do elemento

## Menu

-   margin → Aumenta a "distância entre itens".

-   block → Itens que podem "flutuar".

    -   ocupa toda largura de onde ele é colocado

    -   não deixa outros elementos ao seu lado

    -   ocupa o máximo que é permitido a ele

-   position → Posição dos itens.

-   none → Esconde do usuário um nome, mas não do código.

-   hover → Efeito para quando mover o mouse em cima do menu.

-   transition → Efeito de tempo sob um hover.

-   underline → Sublinha textos

-   border-bottom → Usada para formar uma "linha de cabeçalho", tendo
    como referência alguma margem.

-   width →Usada para delimitar o tamanho do conteúdo.

-   margin → Parâmetro para alinhar com a margem.

-   box-shadow: Utilizado para dar um sombreamento na margem.

-   clear both → Usado para adicionar rodapés.

## Posicionamento

-   Padding → Define o posicionamento de um item dentro de um "box".

-   **Posição Relativa X Posição Absoluta** →Na primeira, os itens podem
    "flutuar" apenas dentro de um bloco, na outra "flutua" na página
    toda.

-   **Position**

    -   Absolute, relative, fixed, etc

-   **Float**

    -   Faz um elemento flutuar para fora da sua posição original

    -   Não pode ter posicionamento absoluto

    -   O resto da pagina irá "contornar" esse elemento

    -   Sua altura fica zuada

    -   clearfix → corrige a altura

    -   [[https://edsonjunior.com/entendendo-float-clear-clearfix/]{.underline}](https://edsonjunior.com/entendendo-float-clear-clearfix/)

    -   https://befused.com/css/container-floated-elements

-   **Margin e padding**

## Criação de tabelas

-   ?

## Funcionalidades avançadas

-   **Gradiente**

    -   Bem mais rápido que carregar uma imagem

    -   Nem todos os browsers aceitam ainda

    -   *Exemplo: tentando o máximo de compatibilidade possível*

![](./Images/2.0 - CSS/media/image7.png){width="4.2479604111986005in"
height="2.5677088801399823in"}

-   **Columns →** Divide o texto em várias columas

-   **Border →** arredonda as bordas

-   **Background →** fundo. Podemos definir mais de um fundo se
    sobrepondo juntos

-   **Transform 2d**

    -   Altera a forma como o elemento é exibido

    -   Cada browser precisa do seu respectivo prefixo

    -   Scale → redimensiona

    -   Skew → muda os ângulos só de um eixo ("deita" a imagem)

    -   rotate → vira o objeto

    -   Translation → move o objeto

-   **Transições**

    -   Suaviza as mudanças nas propriedades

    -   *Exemplo:* um objeto muda de cor quando o mouse passa por cima

![](./Images/2.0 - CSS/media/image5.png){width="3.426509186351706in"
height="1.1835290901137359in"}

-   **Animation**

    -   Dá pra fazer várias coisas legais com CSS, mas nem todos os
        
    > browser suportam
    
    -   Com a regra *Keyframe* podemos dividir a animação em vários
        
        > pedaços e definir o que acontece em cada um

-   **Layout baseado em módulos →** deixa a diagramação organizada pacas

-   **Paged mídia →** organiza a página para impressão

-   **\@Font face**

    -   Para utilizar fontes fora do sistema padrão

    -   Declaramos a fonte no começo e depois podemos usar ela
        
> normalmente

-   Nem todos os navegadores aceitam todos os formatos de fonte
    
-   *Exemplo*
    
    -   Declaramos a Helveticaneue
    
        -   Ela será primeiro buscada localmente e, caso não existir,
            
            > buscada no url

![](./Images/2.0 - CSS/media/image6.png){width="3.973384733158355in"
height="0.889349300087489in"}

-   **Presentation-level →** ?
-   OBS: Caso precise de uma fonte diferente: [[Fontes]{.underline}](https://fonts.google.com/?selection.family=Finger+Paint)

# Bibliotecas de Estilos

* **FontAwesome -**	
  * uma biblioteca de *ícones* (paga, porém tem bastante conteúdo gratuito) Simples mas elegante.
  * https://fontawesome.com/

## Boostrap
* Em 2010 surgia o Twitter BluePrint
* Uma Biblioteca cheia de estilos e classes que facilitam o
* desenvolvimento das telas.
* Existe na Biblioteca além de CSS, Javascript também.
* A w3schools disponibiliza diversos tutoriais sobre o bootstrap que é um
framework mt bom: https://www.w3schools.com/bootstrap/default.asp
* História do Bootstrap: https://youtu.be/KCuogbn1uYs
* **Sistema de Grid**
  -   Exibe o conteúdo por linhas (rows), cada uma com 12 divisões

  -   Todo row deve estar dentro de um container

  -   Responsivo, se adaptando a 4 grandes classes de dispositivos

  -   Xs → extra small → até 768px

  -   Sm → small → maiores que 768px

  -   Md → medium → maiores que 998px

  -   Lg → large → maiores que 1200px

  -   *Exemplo*

      -   Para dispositivos md, o aside tem 4 e o article 8

      -   Para dispositivos lg, o aside terá 10 e o article 2

![](./Images/2.1 - Bootstrap/media/image3.png){width="3.2604166666666665in"
height="1.9270833333333333in"}

-   **Container**

    -   Onde colocar as coisas

    -   Fixo → largura definida (960px)

    -   Fluido → 100% da largura

    -   -offset-x → adiciona, antes do container, x divisões vazias



### Tipografia básica

-   Alinhamento do texto:

-   **Listas**

    -   Unstyled → tira todo o estilo

    -   Inline → um do lado do outro

> ![](./Images/2.1 - Bootstrap/media/image2.png){width="1.4583333333333333in"
> height="0.4583333333333333in"}

-   **Tabela**

    -   Dividida em thead e tbody

    -   Linha → tag tr

    -   Coluna → tag td (body) e th (head)

    -   [Customizações nativas]{.underline}

        -   As características se somam (herança não-excludente)

        -   Striped → uma colorida e outra não

        -   Condensed → toda apertada

        -   Hover → destaca com mouse em cima

        -   Bordered → com borda

![](./Images/2.1 - Bootstrap/media/image1.png){width="3.5416666666666665in"
height="0.17708333333333334in"}

-   **Botões**

    -   Class btn → botão básico

    -   Class block → torna o botão "fluído"