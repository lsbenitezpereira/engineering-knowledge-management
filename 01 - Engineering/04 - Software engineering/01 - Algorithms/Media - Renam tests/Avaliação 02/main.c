/*
 ============================================================================
 Name        : main.c
 Author      : Renan Augusto Starke
 Version     :
 Copyright   : Instituto Federal de Santa Catarina
 Description : prova grafos
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "grafo.h"
#include "fila.h"



int main(void) {
	grafo_t *g;

	/* Cria um grafo com 13 vértices */
	g = cria_grafo(13);

	/* Cria as arestas: grafo não direcionado */
    cria_adjacencia_nao_dir (g, 0, 5);
    cria_adjacencia_nao_dir (g, 5, 2);
    cria_adjacencia_nao_dir (g, 2, 0);
    cria_adjacencia_nao_dir (g, 0, 1);
    cria_adjacencia_nao_dir (g, 1, 2);
    cria_adjacencia_nao_dir (g, 1, 3);
    cria_adjacencia_nao_dir (g, 2, 3);
    cria_adjacencia_nao_dir (g, 3, 4);
    cria_adjacencia_nao_dir (g, 4, 5);

    cria_adjacencia_nao_dir (g, 6, 7);
    cria_adjacencia_nao_dir (g, 7, 8);

    cria_adjacencia_nao_dir (g, 9, 10);
    cria_adjacencia_nao_dir (g, 10, 11);
    cria_adjacencia_nao_dir (g, 11, 9);


	/* Inferência dos componentes conexos */
	componentes_conexos(g);

	/* Exibição dos componentes conexos (vértice por vértice)*/
	mostra_componentes_conexos (g);

	/* Exportação  do grafo */
	exportar_grafo_dot("grafo_dist.dot", g);

	/* Libere a memória */
	libera_grafo (g);


	return EXIT_SUCCESS;
}
