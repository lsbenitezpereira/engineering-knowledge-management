#include <stdio.h>
#include <stdlib.h>

#include "fila.h"

struct filas {
    int cabeca;     /*!< Índice do vetor que representa a cabeça da fila. */
    int cauda;      /*!< Índice do vetor que representa a cauda da fila. */
    int tamanho;    /*!< Tamanho atual da fila. */
    int tamanho_max; /*!< Tamanho máximo: utilizado pelo alocador dinâmico. */
    int *dados;     /*!< Vetor alocado dinamicamente que mantém os dados da fila. */
};

/**
  * @brief  Cria uma nova fila de números inteiros
  * @param	tamanho: tamanho da fila desejada
  *
  * @retval fila_t: ponteiro para uma nova fila
  */
fila_t* cria_fila(int tamanho)
{
    fila_t *p = NULL;

    //aloca memória
    p = (fila_t*)malloc(sizeof(fila_t));

    //variaveis de controle
    p->cabeca = 0;
    p->cauda = tamanho-1;
    p->tamanho = 0;
    p->tamanho_max = tamanho;

    p->dados = (int*) malloc(sizeof(int) * tamanho);

    return p;
}

/**
  * @brief  Libera os dados dinâmicos da fila
  * @param	fila: fila que será liberada
  *
  * @retval Nenhum
  */
void libera_fila(fila_t* fila)
{
    free(fila->dados);
    free(fila);
}

/**
  * @brief Enfileira um novo inteiro. 
  * @param dado: inteiro a ser adicionado no topo da fila
  * @param fila: fila criada que receberá o dado.
  *
  * @retval Nenhum
  */
void enqueue(int dado, fila_t* fila)
{
	int cauda = fila->cauda;

	if (fila->tamanho < fila->tamanho_max) {
		cauda++;
		if (cauda >= fila->tamanho_max)
			cauda = 0;

		fila->dados[cauda] = dado;
		fila->tamanho++;
		fila->cauda = cauda;


	}
	else
		fprintf(stderr, "enqueue: fila em overflow\n");
}

/**
  * @brief Retira da fila um inteiro. 
  * @param dado: inteiro a ser removido do topo da fila
  * @param fila: fila criada que receberá o dado.
  *
  * @retval Nenhum 
  */
int dequeue(fila_t* fila)
{
	int dado = 0;
	int cabeca = fila->cabeca;

	if (fila->tamanho > 0){
		dado = fila->dados[cabeca];
		cabeca++;

		if (cabeca >= fila->tamanho_max)
			cabeca = 0;

		fila->cabeca = cabeca;
		fila->tamanho--;

	}else
		fprintf(stderr, "dequeue: fila vazia\n");


    return dado;
}

/**
  * @brief Retorna se a fila esta vazia.
  * @param fila: fila criada.
  *
  * @retval Valor > 0 se fila vazia. Valor 0 caso contrário.
  */
int fila_vazia(fila_t *fila){
	return !fila->tamanho;
}
