/*
 * grafo.h
 *
 *  Created on: Nov 18, 2016
 *      Author: Renan Augusto Starke
 */

#ifndef GRAFO_H_
#define GRAFO_H_

#define TRUE 1
#define FALSE 0

typedef struct vertices vertice_t;
typedef struct arestas aresta_t;
typedef struct grafos grafo_t;

grafo_t *cria_grafo(int vertices);
void libera_grafo (grafo_t *g);

int cria_adjacencia(grafo_t *g, int u, int v);
int cria_adjacencia_nao_dir(grafo_t *g, int u, int v);

int rem_adjacencia(grafo_t *g, int u, int v);
int adjacente(grafo_t *g, int u, int v);

void componentes_conexos(grafo_t *grafo);
void busca_largura (grafo_t* g, int vertice, int id_grupo);
void mostra_componentes_conexos (grafo_t* g);
void exportar_grafo_dot(const char *filename, grafo_t *grafo);

#endif /* GRAFO_H_ */
