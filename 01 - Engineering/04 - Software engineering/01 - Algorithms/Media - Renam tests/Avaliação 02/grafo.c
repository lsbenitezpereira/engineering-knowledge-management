/*
 * grafo.c
 *
 *  Created on: Nov 18, 2016
 *      Author: Renan Augusto Starke
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "grafo.h"
#include "fila.h"

//#define DEBUG_GRAPH

struct vertices {
	int id;         /*!< Identificação numérica do vértice  */
    int grupo;      /*!< Grupo ao qual ele pertence (componente conexo)  */
    int pai;        /*!< Antecessor (após a busca)  */
    /* Mais informações, se necessário */

};

struct arestas {
	int adj;        /*!< Valor booleando. Verdadeiro representa uma adjacência entre dois vértices  */

    /* Mais informações, se necessário */
};

struct grafos{
	int n_vertices;        /*!< Número de vértices do grafo  */
	vertice_t *vertices;   /*!< Vetor de ponteiros: conjunto V */
	aresta_t **matriz_adj;	/* Matriz de adjacência: conjunto E */
};


/**
  * @brief  Verifica a existência de componentes conexos
  * @param	grafo: ponteiro para o grafo a ser varrido
  *
  * @retval void
  */
void componentes_conexos(grafo_t *grafo){
    int i;
    int id_grupo = 0;

    for (i=0; i < grafo->n_vertices; i++){
        grafo->vertices[i].grupo = -1;
        grafo->vertices[i].pai = -1;
    }

    for (i=0; i < grafo->n_vertices; i++){
        if (grafo->vertices[i].grupo == -1){
            busca_largura(grafo, i, id_grupo);
            id_grupo++;
        }
    }
}

/**
  * @brief  Modificação da busca em largura, adaptada para a prova
  * @param	grafo: ponteiro para o grafo a ser varrido
  * @param	vertice: ponto inicial da busca
  * @param	id_grupo: componente conexo
  *
  * @retval void
  */
void busca_largura (grafo_t* g, int vertice, int id_grupo){
    int v, u;
    fila_t* Q = cria_fila(g->n_vertices);   

    #ifdef DEBUG_GRAPH
    printf ("buscando...\n");
    #endif // DEBUG_GRAPH

    g->vertices[vertice].grupo = id_grupo;
    enqueue (vertice, Q);
    while (!fila_vazia(Q)){
        u = dequeue (Q);
        for (v=0; v < g->n_vertices; v++){
            if (adjacente(g, v, u) && g->vertices[v].grupo==-1){
                g->vertices[v].grupo = id_grupo;
                g->vertices[v].pai = u;
                enqueue (v, Q);
            }
        }
     }
     libera_fila (Q);
}


/**
  * @brief  Printa na saída padrão os vértices e os componentes conexos
  * @param	grafo: ponteiro para o grafo a ser varrido
  *
  * @retval void
  */
void mostra_componentes_conexos (grafo_t* g){
    int i;

    printf ("---------------------------\n");
    printf ("|  v  |  grupo | antecessor |\n");
    for (i=0; i < g->n_vertices; i++)
        printf("| % .2d |   % .2d  |     % .2d    |\n", i, g->vertices[i].grupo, g->vertices[i].pai);
    printf ("---------------------------\n");
}


/**
  * @brief  Cria uma novo grafo estático
  * @param	vertices: quantidade de vértices
  *
  * @retval grafo_t: ponteiro para um novo grafo
  */
grafo_t *cria_grafo(int vertices)
{
	int i;
	aresta_t **matriz_adj;
	/* Aloca estrutura do grafo */
    grafo_t *g = malloc(sizeof(grafo_t));

	if (g == NULL){
		perror("cria_grafo (g)");
		exit(EXIT_FAILURE);
	}

	/* Guarda número total de vértices */
	g->n_vertices = vertices;
    /* Aloca vértices */
	g->vertices = malloc(vertices * sizeof(vertice_t));

	if (g->vertices == NULL){
		perror("cria_grafo (vertices)");
		exit(EXIT_FAILURE);
	}

	/* Zera vetor de vértices */
	memset(g->vertices, 0, vertices * sizeof(vertice_t));

    /* Aloca 1a dimensão da matriz de adjacência */
	matriz_adj = malloc(vertices * sizeof(aresta_t *));

	if (matriz_adj == NULL){
		perror("cria_grafo (matriz_adj)");
		exit(EXIT_FAILURE);
	}

	 /* Aloca 2a dimensão da matriz de adjacência */
	for ( i = 0; i < vertices; i++ )
	{
		matriz_adj[i] = calloc(vertices, sizeof(aresta_t));

		if (matriz_adj[i] == NULL){
			perror("cria_grafo (matriz_adj[i])");
			exit(EXIT_FAILURE);
		}
	}

	g->matriz_adj = matriz_adj;

	return g;
}

/**
  * @brief  Libera a memória utilizada pelo grafo
  * @param	Nenhum
  *
  * @retval Nenhum
  */
void libera_grafo (grafo_t *g){
	int i;

	if (g == NULL){
		perror("libera_grafo");
		exit(EXIT_FAILURE);
	}

	for (i=0; i < g->n_vertices; i++)
		free(g->matriz_adj[i]);

	free(g->matriz_adj);
	free(g->vertices);
	free(g);
}

/**
  * @brief  Cria adjacência entre vértices u e v
  * @param	u: índice do vértice u
  * @param  v: índice do vértice v
  *
  * @retval int: verdadeiro se adjacência for criada
  */
int cria_adjacencia(grafo_t *g, int u, int v){

	if (g == NULL){
		return FALSE;
	}

	if (u > g->n_vertices || v > g->n_vertices )
		return FALSE;

	g->matriz_adj[u][v].adj = TRUE;
	return TRUE;
}


/**
  * @brief  Cria adjacência entre vértices u e v e v e u
  * @param	u: índice do vértice u
  * @param  v: índice do vértice v
  *
  * @retval int: verdadeiro se adjacência for criada
  */
int cria_adjacencia_nao_dir(grafo_t *g, int u, int v){

	if (g == NULL){
		return FALSE;
	}

	if (u > g->n_vertices || v > g->n_vertices )
		return FALSE;

	g->matriz_adj[u][v].adj = TRUE;
	g->matriz_adj[v][u].adj = TRUE;

	return TRUE;
}

/**
  * @brief  Remove adjacência entre vértices u e v
  * @param	u: índice do vértice u
  * @param  v: índice do vértice v
  *
  * @retval int: verdadeiro se adjacência for removida
  */
int rem_adjacencia(grafo_t *g, int u, int v){

	if (g == NULL){
		return FALSE;
	}

	if (u > g->n_vertices || v > g->n_vertices)
		return FALSE;

	g->matriz_adj[u][v].adj = FALSE;

	return TRUE;
}

/**
  * @brief  Retorna adjacência entre vértices u e v
  * @param	u: índice do vértice u
  * @param  v: índice do vértice v
  *
  * @retval int: verdadeiro se u for adjacente a v
  */
int adjacente(grafo_t *g, int u, int v){

	if (u > g->n_vertices  || v > g->n_vertices )
		return FALSE;

	return ((g->matriz_adj[u][v].adj));
}


/**
  * @brief  Exporta o grafo em formato dot
  * @param	filename: nome do arquivo
  * @param  grafo: grafo a ser exportado
  *
  * @retval Nenhum
  */

void exportar_grafo_dot(const char *filename, grafo_t *grafo){
	FILE* file;
    int i=0, j=0;

	if (filename == NULL || grafo == NULL){
		fprintf(stderr, "exportar_grafp_dot: ponteiros invalidos\n");
		exit(EXIT_FAILURE);
	}

	file = fopen(filename, "w");
	if (file == NULL){
		perror("exportar_grafp_dot:");
		exit(EXIT_FAILURE);
	}

	fprintf(file, "graph {\n");

	/* Exporta as strings dos vértices */
	for (i=0; i < grafo->n_vertices; i++){
		 fprintf(file, "\t%d [label=\"%d\"];\n", i, i);
	}

    for( i = 0; i < grafo->n_vertices; i++ )
        for(j = i; j < grafo->n_vertices; j++)	// Exporta adjacencia na diagonal superior
            if( (grafo->matriz_adj[i][j].adj) == TRUE)
                fprintf(file, "\t%d -- %d [label=%d];\n", i, j, 0);    //[label="d0_1=1*27(4)"style=solid]

    fprintf(file, "}\n");
	fclose(file);
}
