#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define SIZE 100

typedef struct Pearson{
    char name [10];
    int age;
} pearson_t;

void swap (pearson_t* a, pearson_t* b){
    pearson_t c;
    strcpy (c.name, a->name);
    c.age = a->age;

    strcpy (a->name, b->name);
    a->age = b-> age;

    strcpy (b->name, c.name);
    b->age = c.age;
}

/* 1 if first if bigger, 0 if equal and -1 if seconds is bigger */
int pearsonComp (pearson_t* a, pearson_t* b){
    if (a->age > b->age)
        return 1;
    else if (a->age == b->age)
        return 0;
    else
        return -1;
}


int main(){
    int i, j;
    pearson_t* vec = malloc (SIZE*sizeof(pearson_t));
    srand (time(NULL));


    /* Init and order */
    for (i=0; i<SIZE; i++){
        vec[i].age = rand()%(60);
        strcpy (vec[i].name, "benitez");
    }
    for (i=1; i<SIZE; i++){
        for (j=i; j>0 && pearsonComp (&vec[j-1], &vec[j])>0 ; j--)
            swap(&vec[j], &vec[j-1]);
    }

    printf("last 100 elements: \n");
    for (i=SIZE-100; i<SIZE; i++){
        printf (" %d\t| %s\n", vec[i].age, vec[i].name);
    }



    return 0;
}
