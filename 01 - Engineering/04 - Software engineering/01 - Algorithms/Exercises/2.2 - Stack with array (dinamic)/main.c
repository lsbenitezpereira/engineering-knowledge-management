#include <stdio.h>
#include <stdlib.h>

#include "libraries/stackAD.h"

int main()
{
    int i;
    pilha_t* stack;

    stack = cria_pilha(3);
    for (i=0; i<50; i++){
        push(stack, i);
    }
    while (stackSize(stack)){
        printf("data: %d\n", pop(stack));
    }
    return 0;
}
