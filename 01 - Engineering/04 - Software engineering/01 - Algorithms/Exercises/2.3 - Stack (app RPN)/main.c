/**********************************************************************
 Title          Reverse Polish Notation calculator
 Language 		C
 Author			Leonardo Benitez
 Date  			03/09/2018
 Description
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "libraries/stackAD.h"

int main(){
    //char input [] = "1.5 5 5 *+"; //result 26.5
    char input [] = "15 7 1 1 + - / 3 * 2 1 1 + + -";
    char buffer[40];
    char ch;
    int i, j;
    float a, b;
    pilha_t* stack = cria_pilha(10);

    for (ch='x', i=0, j=0; ch=input[i], ch!='\0'; i++){
       // printf("caracter lido: %c\n", ch);
        if ((ch >= 48 && ch <= 57) || ch=='.'){ //case number
            buffer[j]=ch;
            j++;
        } else if (ch=='+' || ch=='-' || ch=='*' || ch=='/'){
            printf("operador lido: %c\n", ch);
            a = pop (stack);
            b = pop (stack);
            if (ch=='+'){
                push(stack, b+a);
            } else if (ch=='-'){
                push(stack, b-a);
            } else if (ch=='*'){
                push(stack, b*a);
            } else if (ch=='/'){
                push(stack, b/a);
            }
            //printf("operacao realizada com %f e %f\n",b,a);
        }else if (ch == ' ' && j!=0){                  //case divider
            buffer[j]='\0';
            j=0;
            printf("numero: %.2f\n", atof(buffer));
            push(stack, atof(buffer));
        }
    }
    printf("result: %f", pop(stack));
    return 0;
}
