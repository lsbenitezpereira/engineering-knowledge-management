#ifndef APP_INCLUDED
#define APP_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "libraries/queue.h"

typedef struct StockElement stock_element_t;
typedef struct Stock stock_t;


/**
  * @brief
  * @param
  * @retval
  */
stock_t* newStock (void);


/**
  * @brief
  * @param
  * @retval
  */
void readStock(stock_t* stock, char* file);

/**
  * @brief
  * @param
  * @retval
  */
void printStock(stock_t* stock);

/**
  * @brief
  * @param
  * @retval
  */
void freeStock(stock_t* stock);

#endif // APP_INCLUDED
