#include <stdio.h>
#include <stdlib.h>

#include "libraries/lista_enc.h"

void printList (lista_enc_t* list, int direction);

int main(){
    lista_duplamente_enc_t* list;
    int a = 666, b= 777, c=888, aa=555;

    list = criar_lista_enc();

    add_cauda(list, criar_no(&a));
    add_cauda(list, criar_no(&b));
    add_cauda(list, criar_no(&c));

    printList(list, 0);
    remove_cabeca(list);
    printList(list, 0);
    remove_cauda(list);
    printList(list, 1);
    //add_cabe�a(list, criar_no(&aa));
    //printList(list, 1);
    return 0;
}

void printList (lista_enc_t* list, int direction){  //0=foward
    no_t* element;
    element = (direction==0) ? (obter_cabeca(list)) : (obter_cauda(list));
    //element = obter_cauda(list);
    printf("\n");
    while(element){
        printf("element: %d\n", *(int*)obter_dado(element));    //TODO: cast necess�rio?
        element = (direction==0) ? (obter_proximo(element)) : (obter_anterior(element));
    }
}
