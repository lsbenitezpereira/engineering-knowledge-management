/******************************************************************
 Title			winterGames library
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	Read a .csv file with the winner of the last winter games.
                The alocation of the vector is dinamic, and the alocation of the string "name" in
                each element of the struct is dinamic too.
				Line example: 1,Norway (NOR),13,13,10,36
 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "winterGames.h"
#include "lista_enc.h"
#include "no.h"

#define TAM_BUFFER 90   //max size of the line

struct Nations {
    int position;
    char *name;
    int gold;
    int silver;
    int bronze;
    int total;
};

/******************************************************************
Function:   readFile
Parameters: file name; stream used
Return:     address of the array of nations; Stream (by reference)
******************************************************************/
lista_enc_t* readFile(char* file){
    ///Local Variables
    char buffer [TAM_BUFFER];
    char name_buffer [TAM_BUFFER];
    FILE* fp;
    lista_enc_t* list = NULL;
    no_t* element = NULL;
    nations_t *nation;

    ///Open file
    fp = fopen (file, "r");
    if (fp==NULL){
        perror ("Error in Main");
        exit (-1);
    }

    ///create a new list
    list = criar_lista_enc();//recebe fun��o "limpar dados"

    ///read the file
    while (fgets(buffer, TAM_BUFFER, fp)!=NULL){
        nation = malloc (sizeof(nations_t));    //temp malloc
        if (sscanf(buffer, "%d,%90[^,],%d,%d,%d,%d", &(nation->position), name_buffer, &(nation->gold), &(nation->silver), &(nation->bronze), &(nation->total)) != 6){
            printf("Unstructured file, aborting...");
            exit (-1);
        }
        nation->name = malloc (strlen(name_buffer)+1);  //dinamic allocated string "name"
        strcpy(nation->name, name_buffer);

        element = criar_no(nation);
        add_cauda(list, element);
		//printf("read: %4d| %36s | %4d | %4d | %4d | %4d |\n", nation->position, nation->name, nation->gold, nation->silver, nation->bronze, nation->total);
    }

    ///close the file
    if (fclose(fp)!=0){//isso tamb�m some
        perror ("Error closing file");
        exit(-1);
    }
    return list;
}


/******************************************************************
Brief:      Liberado os dados de uma estrutura do tipo nations
Parameters: ponteiro para o inicio do dado a ser liberado
Return:     void
*****************************************************************/
static void _libera_dado(void *nation){
    nations_t* _nation = nation;
    free (_nation->name);
    free(_nation);
}

/******************************************************************
Brief:      Fun��o necess�ria para encerrar o programa corretamente
Parameters: lista a ser encerrada;
Return:
*****************************************************************/
void closeFile(lista_enc_t* list){
    libera_lista(list, _libera_dado);
}

/******************************************************************
Function:   printNations
Parameters: pointer to the list structure
Return:     void
Comment:    I assumed that the max string have 36 character.
            Otherwise the table will be unformatted (just visually)
*****************************************************************/
void printNations (lista_enc_t* list){
    no_t *meu_no = obter_cabeca(list);
    nations_t* temp;
    printf("Pos.| %36s | gold | sil. | bro. | tot. |\n", "Nation");
    while(meu_no){
        temp = obter_dado(meu_no);
        printf("%4d| %36s | %4d | %4d | %4d | %4d |\n", temp->position, temp->name, temp->gold, temp->silver, temp->bronze, temp->total);
        meu_no = obter_proximo(meu_no);
    }
    printf("\n\n");
}

