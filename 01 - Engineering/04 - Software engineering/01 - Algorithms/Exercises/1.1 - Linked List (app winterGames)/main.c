/******************************************************************
 Title			Fie exercise 2
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	test of the winterGames l
 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "winterGames.h"

int main (void){
    lista_enc_t* nations1,* nations2;

    nations1 = readFile("winterGames1.csv");
    printNations(nations1);
    closeFile(nations1);


    nations2 = readFile("winterGames2.csv");
    printNations(nations2);
    closeFile(nations2);

    return 0;
}

