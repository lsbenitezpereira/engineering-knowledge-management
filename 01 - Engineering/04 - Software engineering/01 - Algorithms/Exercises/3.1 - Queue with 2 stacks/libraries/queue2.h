/**********************************************************************
 Title			Queue using two stacks
 Language 		C
 Author			Leonardo Benitez
 Date  			13/09/2018
 Description
**********************************************************************/

#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

//#define DEBUG

typedef struct Queue queue_t;


/**
  * @brief Creates a queue
  * @param Void
  * @retval Pointer to the queue
  */
queue_t* newQueue (void);


/**
  * @brief  Enqueue data
  * @param  Pointer to the queue, abstract data to be enqueued
  * @retval Void
  */
void enqueue (queue_t* queue, void* data);


/**
  * @brief  Dequeue data
  * @param  Pointer to the queue
  * @retval Pointer to the data
  */
void* dequeue (queue_t* queue);


/**
  * @brief  Return the number of elements in the queue
  * @param  Pointer to the queue
  * @retval Size
  */
int queueSize (queue_t* queue);


void exportQueue (queue_t* queue, char* fileName);

/**
  * @brief
  * @param
  * @retval
  */
void freeQueue (queue_t* queue);

#endif // QUEUE_H_INCLUDED
