#include "stack.h"

struct Stack{
    lista_enc_t* list;
};

/**********************************************************************/
stack_t* newStack(void){
    stack_t* stack = malloc(sizeof(stack_t));
    stack->list = criar_lista_enc();
    return stack;
}

/**********************************************************************/
void stackPush(stack_t* stack, void* data){
    no_t* element = criar_no(data);
    add_cabeca(stack->list, element);
    #ifdef DEBUG
    printf("Data added to the queue\n");
    #endif // DEBUG
}

/**********************************************************************/
void* stackPop(stack_t* stack){
    if (isStackEmpty(stack)){
        #ifdef DEBUG
        printf("Stack underflow: the list is already empty\n");
        #endif // DEBUG
        return NULL;
    }
    //void* headData = obter_dado(obter_cabeca(stack->list));
    //remove_cabeca(stack->list);
    void* headData = remove_cabeca(stack->list);
    #ifdef DEBUG
    printf("sucessful pop\n");
    #endif // DEBUG
    return headData;
}

/**********************************************************************/
void* stackReturnTop(stack_t* stack){
    if (isStackEmpty(stack)){
        #ifdef DEBUG
        printf("Stack underflow: the list is already empty\n");
        #endif // DEBUG
        return NULL;
    }
    void* headData = obter_dado(obter_cabeca(stack->list));
    //remove_cabeca(stack->list);
    //void* headData = remove_cabeca(stack->list);
    #ifdef DEBUG
    printf("Top element returned\n");
    #endif // DEBUG
    return headData;
}

/**********************************************************************/
int stackSize(stack_t* stack){
    return obter_tamanho(stack->list);
}

/**********************************************************************/
int isStackEmpty(stack_t* stack){
    return (obter_tamanho(stack->list)>0)?0:1;
}

/**********************************************************************/
void freeStack(stack_t* stack){
    libera_lista(stack->list);
    free(stack);
}



