#include "heap.h"

#define DEBUG_HEAP

struct Heap {
	int id;                /*!< Identificação numérica da árvore */
	int size;
    int* vertex;            /*!< Array de vértices  */
};

heap_t* heap_new (int id, int size){
    int i;
    heap_t* h;

    h = malloc (sizeof(heap_t));
    if (h == NULL)	{
		perror("heap_new:");
		exit(EXIT_FAILURE);
	}
	h->vertex = malloc (sizeof(int)*size);
	if (h->vertex == NULL)	{
		perror("heap_new:");
		exit(EXIT_FAILURE);
	}

	h->id = id;
	h->size = size;
	for (i=0; i<size; i++){
        h->vertex[i] = INFINITE;
	}
	#ifdef DEBUG_HEAP
	printf ("Heap %d created\n", id);
	#endif // DEBUG_HEAP
	return h;
}

void heap_set_size (heap_t* heap, int size){
    if (heap == NULL || size<0){
		perror("set_size:");
		exit(EXIT_FAILURE);
    }
    heap->size = size;
}

int heap_get_size (heap_t* heap){
    if (heap == NULL){
		perror("get_size:");
		exit(EXIT_FAILURE);
    }
    #ifdef DEBUG_HEAP
	printf ("heap_size: %d\n", heap->size);
	#endif // DEBUG_HEAP
    return heap->size;
}

void heap_set_vertex (heap_t* heap, int vertex, int value){
	if (heap == NULL)	{
        fprintf(stderr,"heap inválido!");
        exit(EXIT_FAILURE);
	}
	heap->vertex[vertex] = value;
}

int heap_get_vertex (heap_t* heap, int vertex){
    if (heap == NULL || vertex >= heap->size)	{
		fprintf(stderr,"heap_get_vertex: heap ou vértice inválido!");
		exit(EXIT_FAILURE);
	}

	return heap->vertex[vertex];
}

int heap_get_father (heap_t* heap, int vertex){
    if (heap == NULL || vertex >= heap->size)	{
		fprintf(stderr,"heap_get_father: heap ou vértice inválido!");
		exit(EXIT_FAILURE);
	}
	return (int)floor (((double)vertex-1)/2);
}

int heap_get_left_child (heap_t* heap, int vertex){
    if (heap == NULL){// || vertex >= heap->size)	{
		fprintf(stderr,"heap_get_left_child: heap ou vértice inválido!");
		exit(EXIT_FAILURE);
	}
	return 2*vertex +1;
}

int heap_get_right_child (heap_t* heap, int vertex){
    if (heap == NULL){// || vertex >= heap->size)	{
		fprintf(stderr,"heap_get_right_child: heap ou vértice inválido!");
		exit(EXIT_FAILURE);
	}
	return 2*vertex +2;
}
/**
  * @brief  Exporta em formato DOT uma árvore.
  * @param  filename: nome do arquivo DOT gerado.
  * @param  arvore: ponteiro válido de uma árvore.
  *
  * @retval Nenhum
  */
void heap_export_dot (const char *filename, heap_t* heap){
	FILE *file;
	int i;

	if (filename == NULL || heap == NULL){
		#ifdef DEBUG_HEAP
		fprintf(stderr, "exportar_tree_dot: ponteiros invalidos\n");
		#endif // DEBUG_HEAP
		exit(EXIT_FAILURE);
	}

	file = fopen(filename, "w");
	if (file == NULL){
		#ifdef DEBUG
		perror("exportar_grafp_dot:");
		#endif // DEBUG
		exit(EXIT_FAILURE);
	}

    fprintf(file, "graph {\n");

    ///Print vertex
	for (i=0; i<heap->size; i++){
        if (heap_get_vertex(heap, i) != INFINITE)
            fprintf(file, "%d [label=%d];\n", i, heap_get_vertex(heap, i));
	}
	fprintf(file, "\n");
	///Print edges
	for (i=0; i<heap->size; i++){//TODO: preciso verificar se o filho está dentro do range do meu heap, ou posso só varrer metade do heap?
        if (heap_get_left_child(heap, i)<heap->size && heap_get_vertex(heap, heap_get_left_child(heap, i))!=INFINITE)
            fprintf(file, "%d -- %d;\n", i, heap_get_left_child(heap, i));
        if (heap_get_right_child(heap, i)<heap->size && heap_get_vertex(heap, heap_get_right_child(heap, i))!=INFINITE)
            fprintf(file, "%d -- %d;\n", i, heap_get_right_child(heap, i));
	}
	fprintf(file, "}");

}
//manda o vertex para a sua posição correta no heap
void heap_max_heapify (heap_t* heap, int vertex){
    if (heap == NULL){
        fprintf(stderr,"max_heapify: heap inválido!");
        exit(EXIT_FAILURE);
	}
    int left = heap_get_left_child(heap, vertex);
    int right = heap_get_right_child(heap, vertex);
    int bigger;
    int temp;

    if(left < heap->size && heap_get_vertex(heap, left)!=INFINITE && heap_get_vertex(heap, left)>heap_get_vertex(heap, vertex))
        bigger = left;
    else
        bigger = vertex;

    if(right < heap->size && heap_get_vertex(heap, left)!=INFINITE && heap_get_vertex(heap, right)>heap_get_vertex(heap, bigger))
        bigger = right;

    if (bigger != vertex){
        temp = heap_get_vertex(heap, vertex);
        heap_set_vertex(heap, vertex, heap_get_vertex(heap, bigger));
        heap_set_vertex(heap, bigger, temp);
        heap_max_heapify(heap, bigger);
    }
}

//recebe um vetor desordenado e constrói um heap a partir dele
void heap_from_vector (heap_t* heap, int* array, int size){
    int i;

    for (i=0; i<size; i++){
        heap_set_vertex(heap, i, array[i]);
    }

    for (i = 0; i<(size)/2 - 1; i++)
        heap_max_heapify(heap, i);
}

int heap_pop_max (heap_t* heap){
//    int i;
    int max;

    max = heap->vertex[0];
/* remoe o root. desloca todo o array para a esquerda e faz o maxheapfy pra a primeira metade (controi o heap novamente)*/
    return max;
}

static void swap (int* a, int* b){
    int c;
    c = *a;
    *a = *b;
    *b = c;
}

void heapSort (int* array, int size){
    int i;
    heap_t* heap;

    heap = heap_new(666, size);
    heap_from_vector (heap, &array[0], size);

    for (i=size; i>=1; i--){
        swap(&array[0], &array[i-1]);
        heap_set_size(heap, heap_get_size(heap)-1);
        heap_max_heapify(heap, 0);
    }
    swap(&array[0], &array[size-1]);
}
//TODO: pop maximum value. Insert and remove. is_heap_empty. heap_free
