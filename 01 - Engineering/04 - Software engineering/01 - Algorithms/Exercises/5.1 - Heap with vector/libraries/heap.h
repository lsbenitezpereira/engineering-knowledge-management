#ifndef HEAP_H_
#define HEAP_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include "vertice.h"
#include "lista_enc.h"
#include "heap.h"
#include "fila.h"

#define FALSE 0
#define TRUE 1
#define INFINITE INT_MAX
#define DEBUG_HEAP

typedef struct Heap heap_t;

heap_t* heap_new (int id, int size);

void heap_set_vertex (heap_t* heap, int vertex, int value);

int heap_get_vertex (heap_t* heap, int vertex);

void heap_max_heapify (heap_t*, int vertex);

void heap_export_dot (const char *filename, heap_t* heap);

int heap_get_father (heap_t* heap, int vertex);

void heap_from_vector (heap_t* heap, int* array, int size);
void heapSort (int* array, int size);

#endif /* HEAP_H_ */
