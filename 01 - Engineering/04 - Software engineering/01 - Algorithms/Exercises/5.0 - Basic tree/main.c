#include <stdio.h>
#include <stdlib.h>
#include "libraries/arvore.h"

int main()
{
    arvore_t* tree;
    int i;

    ///Plant tree
    tree = cria_arvore(666);
    for (i=1; i<=8; i++){
        arvore_adicionar_vertice_id (tree, i);
        vertice_set_chave (arvore_procura_vertice(tree, i), 100+i);
    }
    for (i=1; i<=3; i++){
        arvore_adiciona_filhos(tree, arvore_procura_vertice(tree, i), 2*i, 2*i+1);
    }
    arvore_adiciona_filhos(tree, arvore_procura_vertice(tree, 7), -1, 8);
    arvore_set_raiz(tree, arvore_procura_vertice(tree, 1));

    ///Escale tree
    arvore_busca_preordem(tree, arvore_get_raiz(tree));
    printf ("\n");
    arvore_busca_emordem(tree, arvore_get_raiz(tree));
    printf ("\n");
    arvore_busca_posordem(tree, arvore_get_raiz(tree));
    printf ("\n");
    arvore_busca_largura(tree, arvore_get_raiz(tree));
    printf ("\n");

    ///Cut tree
    arvore_exportar_grafo_dot("tree_log.txt", tree);
    libera_arvore(tree);
    printf("Yhaaaaa!!! (western-style scream) \n");
    return 0;
}
