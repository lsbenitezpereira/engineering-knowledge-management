/*
	grafo unidirecional
*/
#include "grafo.h"

struct vertices {
	int id;
	int dist;
	int pai;
	int visitado;
	/* mais informacoes, se necessario */
};

struct arestas {
	int adj;
	/* mais informacoes, se necessario */
};

struct grafos{
	int n_vertices;
	vertice_t *vertices;
	aresta_t **matriz_adj;	/* Matriz de adjacencia */
};


grafo_t *cria_grafo(int vertices)
{
	int i;
	aresta_t **matriz_adj;
	grafo_t *g = malloc(sizeof(grafo_t));

	if (g == NULL){
		perror("cria_grafo (g)");
		exit(EXIT_FAILURE);
	}

	g->n_vertices = vertices;
	g->vertices = malloc(vertices * sizeof(vertice_t));

	if (g->vertices == NULL){
		perror("cria_grafo (vertices)");
		exit(EXIT_FAILURE);
	}

	memset(g->vertices, 0, vertices * sizeof(vertice_t));

	matriz_adj = malloc(vertices * sizeof(aresta_t *));

	if (matriz_adj == NULL){
		perror("cria_grafo (matriz_adj)");
		exit(EXIT_FAILURE);
	}

	for ( i = 0; i < vertices; i++ )
	{
		matriz_adj[i] = calloc(vertices, sizeof(vertice_t));

		if (matriz_adj[i] == NULL){
			perror("cria_grafo (matriz_adj[i])");
			exit(EXIT_FAILURE);
		}
	}

	g->matriz_adj = matriz_adj;

	return g;
}

void libera_grafo (grafo_t *g){
	int i;

	if (g == NULL){
		perror("libera_grafo");
		exit(EXIT_FAILURE);
	}

	for (i=0; i < g->n_vertices; i++)
		free(g->matriz_adj[i]);

	free(g->matriz_adj);
	free(g->vertices);
	free(g);
}

int cria_adjacencia(grafo_t *g, int u, int v){

	if (g == NULL){
		return FALSE;
	}

	if (u > g->n_vertices || v > g->n_vertices )
		return FALSE;

	g->matriz_adj[u][v].adj = TRUE;
	g->matriz_adj[v][u].adj = TRUE;

	return TRUE;
}

int rem_adjacencia(grafo_t *g, int u, int v){

	if (g == NULL){
		return FALSE;
	}

	if (u > g->n_vertices || v > g->n_vertices)
		return FALSE;

	g->matriz_adj[u][v].adj = FALSE;
	g->matriz_adj[v][u].adj = FALSE;

	return TRUE;
}

int adjacente(grafo_t *g, int u, int v){

	if (u > MAX_VERTICES || v > MAX_VERTICES)
		return FALSE;

	return ((g->matriz_adj[u][v].adj));
}

void exportaDot (grafo_t* g){
    FILE* fp = fopen ("log.txt", "w");
    if (fp == NULL) {
        printf ("Erro ao abrir o arquivo.\n");
        exit(1);
    }

    fprintf(fp, "graph G {\n");
    int i, j;
    for (i=0; i < g->n_vertices; i++){
        for (j=0; j < g->n_vertices && j<=i; j++){
            if (adjacente(g,i,j)){
                fprintf(fp, "%d -- %d\n", i, j);
            }
        }
    }
    fprintf(fp, "}");
    fclose (fp);
}

void printNodes (grafo_t* g){
    int i;

    printf("PRINTING NODES\n");
    for (i=0; i < g->n_vertices; i++)
        printf("vertice: % d\tdist: % d\tpai: % d \tvisitado: % d\n", i, g->vertices[i].dist, g->vertices[i].pai, g->vertices[i].visitado);
}

void buscaLargura (grafo_t* g, int s){
    int v, u;
    fila_t* queue = cria_fila(10);

    for (v=0; v < g->n_vertices; v++){
        g->vertices[v].dist = -1;
        g->vertices[v].pai = -1;
    }

    g->vertices[s].dist = 0;
    enqueue (queue, s);

    while (queueSize(queue)!=0){
        u = dequeue(queue);
        for (v=0; v < g->n_vertices; v++){
            if (adjacente(g, v, u) && g->vertices[v].dist==-1){
                g->vertices[v].dist = g->vertices[u].dist + 1;
                g->vertices[v].pai = u;
                enqueue(queue, v);
            }
        }
    }
}

void buscaProfundidade(grafo_t* g, int s){
    int v, u;
    pilha_t* stack = cria_pilha(10);

    for (v=0; v < g->n_vertices; v++){
        g->vertices[v].visitado = 0;
    }

    push (stack, s);

    while (stackSize(stack)!=0){
        u = pop(stack);

        if (g->vertices[u].visitado == 0){
            g->vertices[u].visitado = 1;
            for (v=0; v < g->n_vertices; v++){
                if (adjacente(g, v, u)){
                    push (stack, v);
                }
            }
        }
    }
}




