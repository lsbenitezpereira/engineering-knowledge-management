/*
 * grafo.h
 *
 *  Created on: Nov 18, 2016
 *      Author: xtarke
 */

#ifndef GRAFO_H_
#define GRAFO_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "queueAD.h"
#include "stackAD.h"

#define MAX_VERTICES 50
#define TRUE 1
#define FALSE 0

typedef struct vertices vertice_t;
typedef struct arestas aresta_t;
typedef struct grafos grafo_t;

grafo_t *cria_grafo(int vertices);
void libera_grafo (grafo_t *g);
int cria_adjacencia(grafo_t *g, int u, int v);
int rem_adjacencia(grafo_t *g, int u, int v);
int adjacente(grafo_t *g, int u, int v);
void exportaDot (grafo_t* g);
void printNodes (grafo_t* g);
void buscaLargura (grafo_t* g, int s);
void buscaProfundidade(grafo_t* g, int s);

#endif /* GRAFO_H_ */
