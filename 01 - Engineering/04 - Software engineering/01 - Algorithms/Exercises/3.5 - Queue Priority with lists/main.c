#include <stdio.h>
#include <stdlib.h>
#include "libraries/queueP.h"

int main(){
    char a='a';
    char b='b';
    char c='c';
    char d='d';
    char e='e';
    queue_t* queue;

    queue = newQueue();
    enqueue(queue, &a, 4);
    enqueue(queue, &b, 4);
    enqueue(queue, &c, 1);
    enqueue(queue, &d, 3);
    enqueue(queue, &e, 2);

    while (queueSize(queue)){
        printf("dequeued: %c\n", *(char*)dequeue(queue));
    }

    return 0;
}
