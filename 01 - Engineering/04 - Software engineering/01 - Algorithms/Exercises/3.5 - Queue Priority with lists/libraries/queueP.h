#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

/**********************************************************************
 Title			Queue with priorities, using linked list
 Language 		C
 Author			Leonardo Benitez
 Date  			03/09/2018
 Description		Funciona 100%?
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "lista_enc.h"
#include "no.h"

//#define DEBUG_QP

typedef struct Queue queue_t;
typedef struct Member member_t;

/**
  * @brief  Creates a new queue (using a single linked list)
  * @param  void
  * @retval Queue pointer
  */
queue_t* newQueue(void);


/**
  * @brief  Enqueue a generic element (add to tail)
  * @param  Queue pointer, data to be enqueued, priority of the data
  * @retval void
  */
void enqueue(queue_t* queue, void* data, int priority);


/**
  * @brief  Remove and return the first element
  * @param  Queue pointer
  * @retval Data pointer
  */
void* dequeue(queue_t* queue);


/**
  * @brief  Return the first element (do not remove)
  * @param  Queue pointer
  * @retval Data pointer
  */
void* queueHead(queue_t* queue);


/**
  * @brief  Return the size of the queue
  * @param  Queue pointer
  * @retval size
  */
int queueSize(queue_t* queue);


/**
  * @brief  Return if the queue is empty
  * @param  Queue pointer
  * @retval 1 if empty
  */
int isQueueEmpty(queue_t* queue);


/**
  * @brief  Free the allocated data (abort if the queue is not empty)
  * @param  Queue pointer
  * @retval void
  */
void freeQueue(queue_t* queue);
#endif // QUEUE_H_INCLUDED
