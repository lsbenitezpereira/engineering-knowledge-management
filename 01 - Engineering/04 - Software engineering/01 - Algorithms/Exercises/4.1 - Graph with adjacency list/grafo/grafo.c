/*
 * grafo.c
 *
 *  Created on: Jul 5, 2016
 *      Author: Renan Augusto Starke
 */
#include "grafo.h"
#define FALSE 0
#define TRUE 1
#define INFINITO INT_MAX

struct grafos {
	int id;                    /*!< Identificação numérica do grafo  */
	lista_enc_t *vertices;     /*!< Lista encadeada dos vértices: conjunto V  */
};

/*********************************************************************************/
grafo_t *cria_grafo(int id)
{
	grafo_t *p = NULL;

	p = (grafo_t*) malloc(sizeof(grafo_t));

	if (p == NULL)	{
		perror("cria_grafo:");
		exit(EXIT_FAILURE);
	}

	p->id = id;
	p->vertices = cria_lista_enc();

	return p;
}

/*********************************************************************************/
vertice_t* grafo_adicionar_vertice(grafo_t *grafo, int id)
{
	vertice_t *vertice;
	no_t *no;

#ifdef DEBUG_GRAFO
	printf("grafo_adicionar_vertice: %d\n", id);
#endif

	if (grafo == NULL)	{
			fprintf(stderr,"grafo_adicionar_vertice: grafo invalido!");
			exit(EXIT_FAILURE);
	}

	if (procura_vertice(grafo, id) != NULL) {
		fprintf(stderr,"grafo_adicionar_vertice: vertice duplicado!\n");
		exit(EXIT_FAILURE);
	}

	vertice = cria_vertice(id);
	no = cria_no(vertice);

	add_cauda(grafo->vertices, no);

	return vertice;
}

/*********************************************************************************/
void adiciona_adjacentes(grafo_t *grafo, vertice_t *vertice, int n, ...)
{
	va_list argumentos;
	vertice_t *sucessor;
	arestas_t *aresta;

	int id_sucessor;
	int peso;
        int x;

	/* Initializing arguments to store all values after num */
	va_start (argumentos, n);

	for (x = 0; x < n; x=x+2 )
	{
		id_sucessor = va_arg(argumentos, int);
		peso = va_arg(argumentos, int);

		sucessor = procura_vertice(grafo, id_sucessor);

		if (sucessor == NULL) {
			fprintf(stderr, "adiciona_adjacentes: sucessor nao encontrado no grafo\n");
			exit(EXIT_FAILURE);
		}

		aresta = cria_aresta(vertice, sucessor,peso);
		adiciona_aresta(vertice, aresta);

#ifdef DEBUG_GRAFO
		printf("\tvertice: %d\n", vertice_get_id(vertice));
		printf("\tsucessor: %d\n", id_sucessor);
		printf("\tpeso: %d\n", peso);
#endif

	}

	va_end (argumentos);
}

/*********************************************************************************/
vertice_t* procura_vertice(grafo_t *grafo, int id)
{
	no_t *no_lista;
	vertice_t *vertice;
	int meu_id;

	if (grafo == NULL)	{
		fprintf(stderr,"procura_vertice: grafo invalido!");
		exit(EXIT_FAILURE);
	}

	if (lista_vazia(grafo->vertices) == TRUE)
		return FALSE;

	no_lista = obter_cabeca(grafo->vertices);

	while (no_lista)
	{
		//obtem o endereco da lista
		vertice = obter_dado(no_lista);

		//obterm o id do vertice
		meu_id = vertice_get_id(vertice);

		if (meu_id == id) {
			return vertice;
		}

		no_lista = obtem_proximo(no_lista);
	}

	return NULL;
}

/*********************************************************************************/
void bfs(grafo_t *grafo, vertice_t* inicial) //TODO: nao esta refletidnp a menor distancia, é assim mesmo?
{
    fila_t* Q = cria_fila();
    no_t* n;
    vertice_t* v, *u;
    arestas_t* aresta;

    ///Inicializando
    #ifdef DEBUG_GRAFO
    printf("\nIniciando busca em largura...\n");
    #endif // DEBUG_GRAFO
    n = obter_cabeca(grafo->vertices);
    while (n){
        v=obter_dado(n);
        vertice_set_pai(v, NULL);
        vertice_set_dist (v, INFINITO);
        #ifdef DEBUG_GRAFO
        printf("Inicilizando no %.2d \tDist:%.2d \tPai:%p\n",
        #endif // DEBUG_GRAFO
                vertice_get_id(v),
                vertice_get_dist(v),
                vertice_get_pai(v)
                );
        n = obtem_proximo(n);
    }

    vertice_set_dist(inicial, 0);
    enqueue (inicial, Q);

    ///Busca
    while (!fila_vazia(Q)){
        u = dequeue (Q);
        n = obter_cabeca(vertice_get_arestas(u));

        while(n){
           aresta = obter_dado(n);
           v = aresta_get_destino(aresta);
           if (vertice_get_dist(v)==INFINITO){
                enqueue (v, Q);
                vertice_set_pai(v, u);
                vertice_set_dist(v, vertice_get_dist(u) + aresta_get_peso(aresta));
                #ifdef DEBUG_GRAFO
                printf("Visitou no %.2d \t\tDist:%.2d \tPai:%p\n",
                #endif // DEBUG_GRAFO
                        vertice_get_id(v),
                        vertice_get_dist(v),
                        vertice_get_pai(v)
                        );
           }
           n = obtem_proximo(n);
        }
    }
}

/*********************************************************************************/
void dfs(grafo_t *grafo, vertice_t* inicial)
{
    pilha_t* S = cria_pilha();
    no_t* n;
    vertice_t* v, *u;
    arestas_t* aresta;

     ///Inicializando
    #ifdef DEBUG_GRAFO
    printf("\nIniciando busca em profundidade...\n");
    #endif // DEBUG_GRAFO
    n = obter_cabeca(grafo->vertices);
    while (n){
        v=obter_dado(n);
        vertice_set_dist (v, 0);
        #ifdef DEBUG_GRAFO
        printf("Inicilizando no %.2d \tDist: %d\n",
        #endif // DEBUG_GRAFO
                vertice_get_id(v),
                vertice_get_dist(v)
                );
        n = obtem_proximo(n);
    }

    vertice_set_dist(inicial, 0);
    push (inicial, S);

    ///Busca
    while (!pilha_vazia(S)){
        u = pop (S);
        n = obter_cabeca(vertice_get_arestas(u));

        while(n){
           aresta = obter_dado(n);
           v = aresta_get_destino(aresta);
           if (vertice_get_dist(v)==0){
                push (v, S);
                vertice_set_dist(v, 1);
                #ifdef DEBUG_GRAFO
                printf("Visitou no %.2d \t\tDist:%d\n",
                #endif // DEBUG_GRAFO
                        vertice_get_id(v),
                        vertice_get_dist(v)
                        );
           }
           n = obtem_proximo(n);
        }
    }
}

/*********************************************************************************/
int busca_vertice(lista_enc_t *lista, vertice_t *vertice_procurado){
    no_t* no_temp;
    no_temp = obter_cabeca(lista);
    while (no_temp){
        if (obter_dado(no_temp) == vertice_procurado) return 1;
        no_temp = obtem_proximo(no_temp);
    }
    return 0;
}

/*********************************************************************************/
void caminho_minimo (grafo_t *grafo, vertice_t* inicial, vertice_t* final){
    vertice_t* u;
    bfs (grafo, final);
    u = inicial;
    while (u){
        printf("%d\t", vertice_get_id(u));
        u = vertice_get_pai(u);
    }
}

/*********************************************************************************/
pilha_t* Dijkstra(grafo_t *grafo, vertice_t *fonte, vertice_t *destino){
    return NULL;
};//TODO: dijkstra

/*********************************************************************************/
no_t *busca_menos_distante(lista_enc_t *Q){
    no_t* no_menor = obter_cabeca(Q);
    no_t* no_temp = no_menor;
    while (no_temp){
        if (vertice_get_dist(obter_dado(no_temp)) < vertice_get_dist(obter_dado(no_menor))){
            no_menor = no_temp;
        }
        no_temp = obtem_proximo(no_temp);
    }
    return no_menor;
}

/*********************************************************************************/
lista_enc_t* componentes_conexos(grafo_t *grafo){
    return NULL;
} //TODO: é pra fazer o que??

/*********************************************************************************/
void exportar_grafo_dot(const char *filename, grafo_t *grafo)
{
	FILE *file;

	no_t *no_vert;
	no_t *no_arest;
	vertice_t *vertice;
	vertice_t *adjacente;
	arestas_t *aresta;
	arestas_t *contra_aresta;
	lista_enc_t *lista_arestas;

	int peso;

	if (filename == NULL || grafo == NULL){
		#ifdef DEBUG_GRAFO
		fprintf(stderr, "exportar_grafp_dot: ponteiros invalidos\n");
		#endif // DEBUG_GRAFO
		exit(EXIT_FAILURE);
	}

	file = fopen(filename, "w");

	if (file == NULL){
		#ifdef DEBUG_GRAFO
		perror("exportar_grafp_dot:");
		#endif // DEBUG_GRAFO
		exit(EXIT_FAILURE);
	}

    fprintf(file, "graph {\n");

	///Lista de vertices
	no_vert = obter_cabeca(grafo->vertices);
	while (no_vert){
        vertice = obter_dado(no_vert);
        #ifdef DEBUG_GRAFO
        fprintf(file, "%d [label=\"Id:%d \\nDist:%d \\nPai:%d\"];\n",
                vertice_get_id(vertice),
                vertice_get_id(vertice),
                vertice_get_dist(vertice),
                vertice_get_pai(vertice)?vertice_get_id(vertice_get_pai(vertice)):0
                );
        #endif // DEBUG_GRAFO
        no_vert = obtem_proximo(no_vert);
	}

	///Lista de arestas
	//obtem todos os nos da lista
	no_vert = obter_cabeca(grafo->vertices);
	while (no_vert){
		vertice = obter_dado(no_vert);

		//obtem todos as arestas
		lista_arestas = vertice_get_arestas(vertice);

		no_arest = obter_cabeca(lista_arestas);
		while (no_arest) {
			aresta = obter_dado(no_arest);

			//ignora caso já exportada
			if (aresta_get_status(aresta) == EXPORTADA) {
				no_arest = obtem_proximo(no_arest);
				continue;
			}

			//marca como exportada esta aresta
			aresta_set_status(aresta, EXPORTADA);
			adjacente = aresta_get_destino(aresta);

			//marca contra-aresta também como exporta no caso de grafo não direcionados
			contra_aresta = procurar_adjacente(adjacente, vertice);
			aresta_set_status(contra_aresta, EXPORTADA);

			//obtem peso
			peso = aresta_get_peso(aresta);

			fprintf(file, "\t%d -- %d [label = %d];\n",
					vertice_get_id(vertice),
					vertice_get_id(adjacente),
					peso);

			no_arest = obtem_proximo(no_arest);
		}
		no_vert = obtem_proximo(no_vert);
	}
	fprintf(file, "}\n");
	fclose(file);
}

/*********************************************************************************/
void libera_grafo (grafo_t *grafo){
	no_t *no_vert;
	no_t *no_arest;
	no_t *no_liberado;
	vertice_t *vertice;
	arestas_t *aresta;
	lista_enc_t *lista_arestas;

	if (grafo == NULL) {
		fprintf(stderr, "libera_grafo: grafo invalido\n");
		exit(EXIT_FAILURE);
	}

	//varre todos os vertices
	no_vert = obter_cabeca(grafo->vertices);
	while (no_vert){
		vertice = obter_dado(no_vert);

		//libera todas as arestas
		lista_arestas = vertice_get_arestas(vertice);
		no_arest = obter_cabeca(lista_arestas);
		while (no_arest){
			aresta = obter_dado(no_arest);

			//libera aresta
			free(aresta);

			//libera no da lsita
			no_liberado = no_arest;
			no_arest = obtem_proximo(no_arest);
			free(no_liberado);
		}

		//libera lista de arestas e vertice
		free(lista_arestas);
		free(vertice);

		//libera no da lista
		no_liberado = no_vert;
		no_vert = obtem_proximo(no_vert);
		free(no_liberado);
	}

	//libera grafo e vertice
	free(grafo->vertices);
	free(grafo);
}
