/*
 * grafo.h
 *
 *  Created on: Jul 5, 2016
 *      Author: Renan Augusto Starke
 */

#ifndef GRAFO_GRAFO_H_
#define GRAFO_GRAFO_H_


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include "vertice.h"
#include "../lista_enc/lista_enc.h"
#include "../lista_enc/no.h"
#include "../fila/fila.h"
#include "../pilha/pilha.h"

#define DEBUG_GRAFO

typedef struct grafos grafo_t;

/**
  * @brief  Cria uma novo grafo
  * @param	id: Identifica��o num�rica do grafo
  *
  * @retval grafo_t: ponteiro para um novo grafo
  */
grafo_t *cria_grafo(int id);


/**
  * @brief  Adicionar um v�rtice no grafo (conjunto V)
  * @param	grafo: ponteiro do grafo que se deseja adicionar um v�rtice
  * @param  id: identifica��o da v�rtice
  *
  * @retval vertice_t: ponteiro do v�rtice criado e adicionada no grafo
  */
vertice_t* grafo_adicionar_vertice(grafo_t *grafo, int id);


/**
  * @brief  Cria adjac�ncias (arestas).
  * @param	grafo: ponteiro do grafo que cont�m o v�rtice (V pertence a G)
  * @param  vertice: v�rtice fonte da(s) adjac�ncias
  * @param  n: n�mero de par�metros ap�s n
  * @param  ...: pares ordenados dos vertices destino e peso da aresta: (id vertice destino, peso aresta)
  *
  * @retval Nenhum
  *
  * Ex: adicionar uma aresta para o vertice 2 e 3 com respectivos pesos 9 e 15
  * adiciona_adjacentes(grafo, vertice, 4(n), 2, 9, 3, 15);
  */
void adiciona_adjacentes(grafo_t *grafo, vertice_t *vertice, int n, ...);


/**
  * @brief  Procura um v�rtice com id espec�fico no grafo
  * @param	grafo: ponteiro do grafo que se deseja busca o v�rtice
  * @param  id: identifica��o da aresta
  *
  * @retval vertice_t: ponteiro do v�rtice. NULL se n�o encontrado
  */
vertice_t* procura_vertice(grafo_t *grafo, int id);


/**
  * @brief  Busca em largura
  * @param	grafo: ponteiro do grafo que se deseja executar a busca
  * @param  inicial: ponteiro do v�rtice inicial (fonte) da busca
  *
  * @retval Nenhum: V�rtices s�o marcados internamente
  */
void bfs(grafo_t *grafo, vertice_t* inicial);


/**
  * @brief  Busca em profundidade
  * @param	grafo: ponteiro do grafo que se deseja executar a busca
  * @param  inicial: ponteiro do v�rtice inicial (fonte) da busca
  *
  * @retval Nenhum: V�rtices s�o marcados internamente
  */
void dfs(grafo_t *grafo, vertice_t* inicial);


/**
  * @brief  Retorna TRUE se vertice_procurado estiver no conjunto Q
  * @param
  *
  * @retval
  */
int busca_vertice(lista_enc_t *lista, vertice_t *vertice_procurado);


/**
  * @brief  Busca o menor caminho entre dois vertices (deconsiderando o peso das arestas)
  * @param  grafo: ponteiro do grafo que se deseja executar a busca
  * @param  inicial: vertice inicial da busca
  * @param  final: vertice final da busca
  * @retval Nenhum: o caminho � exibido na sa�da padr�o
  */
void caminho_minimo (grafo_t *grafo, vertice_t* inicial, vertice_t* final);


/**
  * @brief  Menor camingo entre todos os nos
  * @param
  * @param
  *
  * @retval retorna um pilha do caminho entre fonte e destino
  */
pilha_t* Dijkstra(grafo_t *grafo, vertice_t *fonte, vertice_t *destino);


/**
  * @brief  Procura o vertice com a menor distancia dentre uma lista de vertices
  * @param	Q: lista encadeada dos vertices a procurar
  *
  * @retval No com a menor dist�ncia
  */
no_t *busca_menos_distante(lista_enc_t *Q);


/**
  * @brief
  * @param
  *
  * @retval
  */
lista_enc_t* componentes_conexos(grafo_t *grafo);


/**
  * @brief  Exporta o grafo em formato dot.
  * @param	filename: nome do arquivo dot gerado
  * @param  grafo: ponteiro do grafo a ser exportado
  *
  * @retval Nenhum
  */
void exportar_grafo_dot(const char *filename, grafo_t *grafo);


/**
  * @brief  Libera a mem�ria utilizada pelo grafo
  * @param  grafo: ponteiro do grafo a ser exportado
  *
  * @retval Nenhum
  */
void libera_grafo (grafo_t *grafo);

#endif /* GRAFO_GRAFO_H_ */
