#include <stdio.h>
#include <stdlib.h>
#include "libraries/queue.h"

int main(void){
    fila_t* queue = cria_fila(5);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    enqueue(queue, 666);
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    printf("data: %d\n", dequeue(queue));
    return 0;
}
