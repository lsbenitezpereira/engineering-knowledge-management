#include <stdio.h>
#include <stdlib.h>

#include "no.h"
#include "lista_enc.h"
#include "stack.h"

#define DEBUG
struct Stack{
    lista_enc_t* list;
};

stack_t* newStack(void){
    stack_t* stack = malloc(sizeof(stack_t));
    stack->list = criar_lista_enc();
    return stack;
}

void stackPush(void* data, stack_t* stack){
    no_t* element = criar_no(data);
    add_cabeca(stack->list, element);
}

void* stackPop(stack_t* stack){
    if (isStackEmpty(stack)){
        #ifdef DEBUG
        printf("Stack underflow: the list is already empty\n");
        #endif // DEBUG
        return NULL;
    }
    void* headData = obter_dado(obter_cabeca(stack->list));
    remove_cabeca(stack->list);
    //free node
    //removeCabeca deve retornar o endere�o do dado
    #ifdef DEBUG
    printf("sucessful pop\n");
    #endif // DEBUG
    return headData;
}

int stackSize(stack_t* stack){
    return obter_tamanho(stack->list);
}

int isStackEmpty(stack_t* stack){
    return (obter_tamanho(stack->list))?0:1;
}



