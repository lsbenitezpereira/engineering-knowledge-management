#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

typedef struct Stack stack_t;

stack_t* newStack(void);
void stackPush(void* data, stack_t* stack);
void* stackPop(stack_t* stack);
int stackSize(stack_t* stack);
int isStackEmpty(stack_t* stack);

#endif // STACK_H_INCLUDED
