#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    int a=5;
    int b=6;
    int c=5;
    int* data;

    stack_t* stack1;
    stack1 = newStack();
    printf("stack size is: %d\n", stackSize(stack1));
    stackPush(&a, stack1);
    printf("stack size is: %d\n", stackSize(stack1));
    stackPush(&b, stack1);
    printf("stack size is: %d\n", stackSize(stack1));
    data=stackPop(stack1);
    printf("Poped %d, stack size now is: %d\n", (*data), stackSize(stack1));
    data=stackPop(stack1);
    printf("Poped %d, stack size now is: %d\n", (*data), stackSize(stack1));

    if (!isStackEmpty(stack1)){
        data=stackPop(stack1);
        printf("Poped %d, stack size now is: %d\n", (*data), stackSize(stack1));
    }

    //data=stackPop(stack1);
    //(data)?printf("Poped %d, stack size now is: %d\n", (*data), stackSize(stack1)) : printf("list empty\n");
    //TODO: estou tendo que fazer essa compara��o por que a fun��o stackPop retorna NULL quando a lista est� vazia. Como melhorar?
    //sim, sempre tem que fazer uma condificonal antes de ler o dado
    return 0;
}
