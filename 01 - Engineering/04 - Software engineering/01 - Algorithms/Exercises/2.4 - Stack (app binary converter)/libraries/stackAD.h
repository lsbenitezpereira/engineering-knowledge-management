#ifndef PILHA_H_INCLUDED
#define PILHA_H_INCLUDED

/**********************************************************************
 Title
 Language 		C
 Author			Renan Stark a and Leonardo Benitez
 Date  			03/09/2018
 Description
**********************************************************************/

#include <stdlib.h>
#include <stdio.h>

#define DEBUG

typedef struct pilhas pilha_t;

/**
  * @brief  Cria uma nova pilha com <size> inteiros
  * @param	size
  *
  * @retval pilha_t: ponteiro para uma nova pilha
  */
pilha_t* cria_pilha (int size);


/**
  * @brief  Empilha um novo inteiro.
  * @param dado: inteiro a ser adicionado no topo da pilha
  * @param pilha: pilha criada que receber� o dado.
  *
  * @retval Nenhum
  */
void push(pilha_t *pilha, char dado);


/**
  * @brief Desempilha um inteiro.
  * @param pilha: pilha criada que retornar� o dado.
  *
  * @retval int: valor inteiro do �ltimo dado empilhado.
  */
char pop(pilha_t *pilha);


/**
  * @brief Desempilha um inteiro.
  * @param pilha: pilha criada que retornar� o dado.
  *
  * @retval int: valor inteiro do �ltimo dado empilhado.
  */
int stackSize (pilha_t* stack);

#endif // PILHA_H_INCLUDED
