#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libraries/quickSort.h"
#include "libraries/mergeSort.h"

#define SIZE 100000
#define REPETIONS 3

//#define DO_BUBLE
//#define DO_QUICK
//#define DO_INSERTION
//#define D0_INSERTION_B
#define DO_MERGE
//#define DO_SELECTION_STRAIGHT
//#define DO_SELECTION_HEAP

void swap (int* a, int* b){
    int c;
    c = *a;
    *a = *b;
    *b = c;
}


int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}



int main(void){
    int* vec;
    int i, j, r;
    int temp, left, mid, right; //used in insertion binary
    int max; //used in selection sort

    clock_t buble_start=0, buble_end=0, buble_total = 0;
    clock_t quick_start=0, quick_end=0, quick_total = 0;
    clock_t insert_start=0, insert_end=0, insert_total = 0;
    clock_t insert_b_start=0, insert_b_end=0, insert_b_total = 0;
    clock_t merge_start=0, merge_end=0, merge_total = 0;
    clock_t sel_s_start=0, sel_s_end=0, sel_s_total = 0;
    clock_t sel_h_start=0, sel_h_end=0, sel_h_total = 0;

    vec = malloc (SIZE*sizeof(int));
    int* tempVec = malloc (SIZE*sizeof(int)); //used in Merge
    srand (time(NULL));

    for (r=0; r<REPETIONS; r++){
        #ifdef DO_BUBLE
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        buble_start = clock();
        for (i=SIZE; i>1; i--)
            for (j=0; j<i-1; j++)
                if (vec[j] > vec[j+1])
                    swap (&vec[j], &vec[j+1]);
        buble_end = clock();
        #endif // DO_BUBLE

        #ifdef DO_QUICK
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        quick_start = clock();
//        quickSort(&vec[0], 0, SIZE);
        qsort (vec, SIZE, sizeof(int), cmpfunc);
        quick_end = clock();
        #endif // DO_QUICK

        #ifdef DO_INSERTION
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        insert_start = clock();
        for (i=1; i<SIZE; i++){
            for (j=i; j>0 && vec[j-1]>vec[j]; j--)
                swap(&vec[j], &vec[j-1]);
        }
        insert_end = clock();
        #endif // DO_INSERTION

        #ifdef DO_INSERTION_B
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        insert_b_start = clock();

        for (i=0; i<SIZE; i++){
            temp = vec[i];
            left = 0;
            right = i;

            /* Binary search */
            while (left < right){
            mid = (left+right)/2;
            if (temp >= vec[mid])
                left = mid + 1;
            else
                right = mid;
            }

            /* insertion? */
            for (j=i; j>left; j--)
                swap (&vec[j], &vec[j-1]);

        }
        insert_b_end = clock();
        #endif

        #ifdef DO_MERGE
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        merge_start = clock();
        mergeSort (&vec[0], &tempVec[0], 0, SIZE-1);
        merge_end = clock();
        #endif

        #ifdef DO_SELECTION_STRAIGHT
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        sel_s_start = clock();
        for (i=SIZE; i>1; i--){
            max=0;
            for (j=1; j<i; j++){
                if (vec[j]>vec[max])
                    max = j;
            }
            swap (&vec[i-1], &vec[max]);
        }
        sel_s_end = clock();
        #endif // DO_SELECTION_STRAIGHT

        #ifdef DO_SELECTION_HEAP
        for (i=0; i<SIZE; i++) vec [i] = rand()%(SIZE/10);
        sel_h_start = clock();
        for (i=SIZE; i>1; i--){
            max=0;
            for (j=1; j<i; j++){
                if (vec[j]>vec[max])
                    max = j;
            }
            swap (&vec[i-1], &vec[max]);
        }
        sel_h_end = clock();
        #endif // DO_SELECTION_HEAP

        buble_total += buble_end-buble_start;
        quick_total += quick_end-quick_start;
        insert_total += insert_end-insert_start;
        insert_b_total += insert_b_end-insert_b_start;
        merge_total += merge_end-merge_start;
        sel_s_total += sel_s_end-sel_s_start;
        sel_h_total += sel_h_end-sel_h_start;
    }

    /* Result and analysis*/
    printf("last 100 elements: \n");
    for (i=SIZE-100; i<SIZE; i++){
        printf (" %d\n", vec[i]);
    }
    printf ("\n|------------------------");
    printf ("\n| Algorithm    | Time");
    printf ("\n|------------------------");
    printf ("\n| Buble        | %f", (double)buble_total/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Quick        | %f", (double)(quick_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Insertion    | %f", (double)(insert_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Insertion B  | %f", (double)(insert_b_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Merge        | %f", (double)(merge_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Selection S. | %f", (double)(sel_s_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n| Selection H. | %f", (double)(sel_h_total)/(REPETIONS*CLOCKS_PER_SEC));
    printf ("\n|------------------------");

    free (vec);
    return 0;
}
