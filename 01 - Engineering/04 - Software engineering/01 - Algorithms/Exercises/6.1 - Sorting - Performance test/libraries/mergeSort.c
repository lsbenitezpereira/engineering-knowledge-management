static void _merge (int* array, int* tempArray, int left, int mid, int right);

void mergeSort (int* array, int* tempArray, int left, int right){
    int mid;
    if (left < right){
        mid = (left+right)/2;
        mergeSort(&array[0], &tempArray[0], left, mid);
        mergeSort(&array[0], &tempArray[0], mid+1, right);
        _merge (&array[0], &tempArray[0], left, mid, right);
    }
}

static void _merge (int* array, int* tempArray, int left, int mid, int right){
    int i, j, k;

    i = left;
    j = left;
    k = mid+1;

    while (j<=mid && k<=right){
        if (array[j] < array[k])
            tempArray[i++] = array[j++];
        else
            tempArray[i++] = array [k++];
    }

    while (j <= mid){
        tempArray[i++] = array[j++];
    }
    for (i=left; i<k; i++){
        array[i] = tempArray[i];
    }
}
