static int _particion (int* A, int left, int right);
static int _medianThree (int* array, int left, int right);
static void _swap (int* a, int* b);

void quickSort (int* A, int left, int right){
    int p;
    if (left < right){
        p = _particion(A, left, right);
        quickSort (A, left, p);
        quickSort (A, p+1, right);
    }
}

/**********************************************/
static int _particion (int* A, int left, int right){
    int med = _medianThree (A, left, right);
    int pivot = A[med];
    int i = left - 1;
    int j = right + 1;

    for(;;){
        do {
            i++;
        } while (A[i] < pivot);

        do {
            j--;
        } while (A[j] > pivot);

        if (i>=j)
            return j;
        _swap (&A[i], &A[j]);
    }
}

/**********************************************/
static int _medianThree (int* array, int left, int right){
    int median = (left + right)/2;

    if (array[right] < array [left])
        _swap (&left, &right);
    if (array[median] < array[left])
        _swap (&median, &left);
    if (array[right] < array [median])
        _swap (&right, &median);

    return median;
}

/**********************************************/
static void _swap (int* a, int* b){
    int c;
    c = *a;
    *a = *b;
    *b = c;
}
