# Performance test

* Hardware: acer aspire 5 (i5 7200U 2.5Ghz, 8GB DDR4)

* Tested to 100000 element

* Mean of 20 iterations

* Result:

  ![1543574292757](1543574292757.png)

* insertion binary: 1.8s
* Merge: 10ms
* HeapSort?