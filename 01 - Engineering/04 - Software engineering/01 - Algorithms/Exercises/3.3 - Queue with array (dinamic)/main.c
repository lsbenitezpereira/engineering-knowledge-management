#include <stdio.h>
#include <stdlib.h>

#include "libraries/queueAD.h"

int main()
{
    int i;
    fila_t* queue;
    queue = cria_fila(10);

    for (i=1000; i<1050; i++){
        enqueue(queue, i);
        printf("dequeued: %d\t\tused size: %d\n", dequeue(queue), queueSize(queue));
    }
/*  for (i=1000; i<1100; i++){
        enqueue(queue, i);
        //printf("dequeued: %d\t\tused size: %d\n", dequeue(queue), queueSize(queue));
    }


    while (queueSize(queue)){
        printf("data: %d\t\t", dequeue(queue));
        printf("queueSize: %d\n", queueSize(queue));
    }*/
    libera_fila(queue);

    return 0;
}
