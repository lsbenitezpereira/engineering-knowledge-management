[TOC]

# String matching

* Let *m* be the length of the pattern, *n* be the length of the searchable text
* Basic aproach
  * Or "Brute-force" or "Naive" algorithm
  * element by element, compare
  * if equal, compare the next
  * problem: you have to check twice if the begining of the pattern is represent but not complete
  * $O(mn)$ (worst case)
* Knuth-Morris-Pratt KMP
  * O(m+n)
  * intuition
    * does the begining (prefix) of the pattern repeat itself across the pattern?
    * Can be do backtrack do avoid repeting to check the same part of the pattern?
  * algorithm
    * j = infex in pattern
    * i = index in string
    * if not match, i++
    * if match, j++ and i++
    * in case of mismach
      * j back to the last repetion (and here is the difference, because we dont move i back)
      * if j==0, i++
      * perceba que podemos aproveitar uma possível repetição no padrão para não ter que voltar i várias casas

## regular expression searching

* Pattern is contituted by symbols

# Fuzzy matching

Paper sobre fuzzy match em geral: [https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjM79vx7tDuAhWESzABHeZKBywQFjABegQIBRAC&url=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fuploads%2F2003%2F01%2Fbm_sigmod03.pdf&usg=AOvVaw0e09VdUNNOz8DRjyFNRTre](https://www.google.com/url?sa=t&rct=j&q&esrc=s&source=web&cd&cad=rja&uact=8&ved=2ahUKEwjM79vx7tDuAhWESzABHeZKBywQFjABegQIBRAC&url=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fuploads%2F2003%2F01%2Fbm_sigmod03.pdf&usg=AOvVaw0e09VdUNNOz8DRjyFNRTre&authuser=0)

Post sobre fuzzy match no contexto de custumer identification: https://www.datarobot.com/blog/using-fuzzy-matching-plus-artificial-intelligence-to-identify-duplicate-customers/



Fuzzy matching com locallity sensitive hashing; Algumas implementações:
https://github.com/embr/lsh
https://github.com/ekzhu/datasketch



* Common problems
  * Mispelling
  * different languages
  * abbreviations
  * Pseudonyms and Alternative naming
  * repetitons: two different entities may have equal values, without nothing being wrong



## Type of inputs

* **Phonetic**
  * records based on vocal input
  * Matching algorthm must handle phonetically similar cases
  * pytohn library: [phonetics](https://pypi.org/project/phonetics/); converts to a canonical representation
* **Pure string**
  * [tutorial](https://www.datacamp.com/community/tutorials/fuzzy-string-python)
* **Structured data**
  * several parameters representing the entity 
  * basic approach: Neirest Neighbors
  * Negative Matching approach: provides additional exclusion rules, usually handcrafted
  * <u>Type of field</u>
    * muttable (with different frequencies) (time dependent or not)
    * Immutable
    * multi valued; voting strategies may be specially useful for them 
  * <u>Implementations</u>
    * https://github.com/dedupeio/dedupe
      https://github.com/facebookresearch/faiss (only for int features)
      https://pixelogik.github.io/NearPy/

## Consolidation strategy

* Hard merge
  * consolidate together multiple records into one single record
  * (-) information loss
* Soft merge: creates an association between the different records that, when read, can be grouped.





# Check for balanced parentheses

https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-python/

# Geração de números aleatórios

- **true random**
  - measures some physical phenomenon that is expected to be random and then compensates for possible biases in the measurement process
  - Sources: atmospheric noise, thermal noise, cosmic background radiation, radioactive decay, valanche noise in Zener diodes, clock drift…
  - are usually blocking: they are rate-limited until enough entropy is harvested to meet the demand.
  - Linux provide one in: `/dev/random`
- **pseudorandom**
  - numbers that look random, but are actually deterministic, and can be reproduced if the state of the PRNG is known
  - usually require an initial value
  - usually the requence repeat after some time
  - <u>Linear congruential generator</u>: ${\displaystyle X_{n+1}=\left(aX_{n}+c\right){\bmod {m}}}$
  - <u>Middle Square Weyl Sequence</u>: ?
  - <u>Liner-Feedback Shift Register</u>
    - very easy to be implemented in C
- **Sampling from specific distributions**
  - <u>Smirnov transform</u>
    - or *Inverse transform sampling*
    - To obtain samples from an arbitrary distribution using a uniform number generator
    - uses the distribution's CDF (cumulative distribution function)
    - (-) requires inverting the CDF (may be expensive for continuous distributions)
    - (-) requires a uniform random number generation
    - intuition: if x is uniformly distributed and CDF is the cumulative distribution function of u, then $F^{-1}(x)$ has the same distribution as u
    - Discrete algorithm: Sort the samples based on their weights; Compute the cumulative sum of the samples; select a random number between 0 and 1; return the corresponding item in the array above
  - <u>Rejection sampling algorithm</u>
    - To obtain samples from an arbitrary distribution using a uniform number generator
    - (-) requires a uniform random number generation
    - Only for discrete?
    - We can use the "importance sampling principle" to sample from a gaussian instead of a uniform
    - Algorithm: A new sample is generated from the uniform; with a probability proportional to its weight, return this sample; else, repeat

# Association problem

* Or *stable matching problem*, *Stable marriage problem*, or *Weapon-target assignment problem*, or *Job-room scheduling*, or *linear assignment*

* Goal is to find the best assignment

* Finding the best matching between two equally sized sets of elements given an ordering of preferences for each element

* If you subtract or multiply all costs, that doesn't change the optimal solution

* for framing this as a maximization problems, just subtract all costs by the maximum cost (C = max(C) - C)

* ==move to optimization or operations research?==

* **Axhaustive search**

  * Runtime n!

* **Hungarian algorithm**

  * Or *Munkres*

  * Runtime O(n^3)

  * proven to be optimal

  * 

  * [as described here](https://en.wikipedia.org/wiki/Hungarian_algorithm#Matrix_interpretation)

  * element in the i-th row and j-th column represents the cost of assigning the j-th job to the i-th worker.

  * All zeros in the matrix must be covered by marking as few rows and/or columns as possible

  * can't be in the same row or column

  * n = rank of the matrix

  *  

  * step 1

  * for every row, subtract the min element of that row (actually if you can finish step 2 here, you don't need to do the col subtraction?)

  * for every col, subtract the min element of that col

  *  

  * Step 2:

  * try to cover all zeros with the minimum number of lines (vertical or horizontal)

  * If the number of lines==n, then go to step 4

  * If the number of lines<n, then go to step 3

  * if you are in doubt if you know how to best cover the zeros (aka if you are dumb), do 2B. You can imagine 2B and 2C as a separate function/sub-algorithm

  * 

  * Step 2B

  * For each row, try to assign an arbitrary zero

  * assignments can't be in the same row nor same column 

  * To indicate an assignment, we write an asterisk `*` and cross/highlight the entire row and column to show is it already "occupied"

  * 

  *  

  * Step 2C

  * look at the non-covered columns

  * in each column, try to mark with a prime `'` one zero (following the rule below), highligh that row

  * If the zero is on the same row as a starred zero: cover the corresponding row, and uncover the column of the starred zero. Then continue Finding a non-covered zero and prime it.

  * else: crazy logic with forming a path (alternating * and '); then for all elements of the path, star primed zeros and unstar starred zeros

  * ...?

  * Keep looping on this step until the following condition is met: if there is no non-covered zero in a column, go to step 3

  *  

  * Step 3

  * find the lowest uncovered value

  * Subtract this from uncovered elements (or every unmarked element, if you are dumb and followed the 2B and 2C?) and add it to every element covered by two lines

  * Go back to step 2.

  * 

  * Step 4

  * Transformations are done, assignments should be visually obvious

  * if they are not: start by the line with the minimum number of zeros, assignt it, proceed to the next line with fewer zeros

  * .

  * I think this is a similar but slightly different algorithm>

    ![image-20241123224236004](./Images - Other algorithms/image-20241123224236004.png)

* * 
  * 

* **Jonker-Vilgenant**

  * Also n^3, but in practice is faster
  * return the same result as Hungarian

* **Gale-Shapley algorithm**

  * ?

* **Variation - stable roommates problem**

  * One single group

* **Applications**

  * Object tracking
  * Optimal transport (positioning objects on a grid)
  * Task assignment in project management
  * Job scheduling in manufacturing systems
  * Vehicle routing for logistics
  * Resource allocation in network systems
