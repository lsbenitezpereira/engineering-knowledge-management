[TOC]

General libraries 
==================

-   Linked code to make our life easier
-   The compiler just include in the object file the functions that you
    are really using: the `#include` is just to allow the compiler to
    verify the data types
-   https://en.cppreference.com/w/c/header

-   **Std lib**
-   To Operational System
    
-   System ("pause") → good to run the output executable
    
-   Exit (int) → terminates the program
    
-   int atoi (const char \*str) →convert string to integer
    
-   float atof
    
-   **Math**

    -   The trigonometrc functions are not defined by default in the
        library, so you need to include the \#math extension\#, that can
        be done including the linker option -lm

    -   PI constant → M\_PI

-   **Locale**

    -   Put everything in Portuguese → setlocale
        (LC\_ALL,\"portuguese\");

-   **Conio **

-   **Complex**

    -   Absolute value → cabs (variable)

    -   Argument (in rads) → carg (variable)

    -   Imaginary part → cimag (variable)

    -   Real part → creal (variable)

    -   Conjugate → conj (variable)

-   **Assert**

    -   Allow to print thing in debug mode

## Profiling

* Not STD
* Valgrind
* Callgrind: uses runtime instrumentation via the Valgrind framework for its cache simulation and call-graph generation;  
* visualization tool: KCachegrind

## unistd

* sleep(<time>)

# Standard IO (main functions)

-   To use peripherals

-   **Printf**

    -   Printf ("text and format", variables);

    -   Send to screen

    -   *To string, see separate topic *

-   **Fflush(stdin)**

    -   Clean the standard buffer (usually keyboard)

-   **Scanf**

    -   Pcarf ("format", address of variable);

    -   Read keyboard buffer

    -   Wait for space to finish

    -   Return the number of variables that he read

    -   Be careful with buffer overrun, always limit the user input.

    -   *Example: read string with %\<max\_size\>s*

    -   *To more about string, see separate topic *

-   **Perror ("\<string\>")**

    -   print the last error, after the string

-   **Types specifiers**

    -   \%d → integer

    -   \%f → real

    -   \%c → character

    -   \%s → string (will stop when find a space)

    -   \%x → hexadecimal (to unsigned int)

![](Images - 1.1 - C Standard Library/image1.png){width="3.1640627734033244in"
height="1.6875in"}

-   **Formatters **

    -   String formatters accept regular expressions.

    -   Limit the decimal houses → printf ("*%.2f*

    -   Read string with space → scanf ("%\<max\_len\>\[\^\\n\]

    -   Read string alphabetical → scanf ("%\<max\_len\>\[a-z A-Z\]

    -   Print character % → printf ("10%% of something

    -   Ignore the read → scanf ("%\*d");

![](Images - 1.1 - C Standard Library/image2.png){width="3.729663167104112in"
height="2.28125in"}

## Reading 1 character

-   **Getchar**

    -   Wait for enter (just read after enter)

    -   With eco

    -   The \\n will stay in buffer, so you need to clear the buffer

-   **Scanf →** same as getchar?

-   **Getch**

    -   Don't wait for enter (read instantly)

    -   Without eco

    -   Need to include conio.h

    -   Receive directly what you write (don't read the buffer)

    -   Getche → with eco

## Reading strings

-   **Scanf**

    -   Read string until space (the rest stay in buffer)

    -   The second parameter don't need & (don't receive an address)

    -   Always clear the buffer before using scanf (ffflush(stdin);)

    -   To read until enter → scanf ("%\[\^\\n\]s", string);

    -   Limitating the read → scanf (\"%\[a-z A-Z\]s\", string);

-   **Gets**

    -   Read string until enter

    -   Parameter: pointer to the string

    -   Return the same pointer to the string

    -   Careful: that is a unsafe function, cause don't limit the buffer
        size

    -   *Example:*

$$\ \ \ char\ str\lbrack 50\rbrack;$$

$$\ \ \ gets(str);$$

# File IO

-   The text file (ASCII) is bigger than the binary equivalent

-   You can open how many files you want

-   Everything is defined in the standard library stdio

-   **Stream**

    -   Continuos flux of data

    -   Buffer that abstract the hardware

    -   The data go/comes to/from the stream when the OS is available

    -   The stream can be of text or of binaries

-   **Defaults IOs**

    -   Stdin → default input

    -   Stdout → default output

    -   Stderr → error output

-   **Opening mode**

    -   r (Read) → the file must exist
    -   w (write) → something that already exist: overwrite
    
    -   w+r → read and write
    -   a (append) → \#acrescenta dados no final do arquivo.
    
    -   binary files: must add a "b" (rb, wb, wb+rb)
    -   The distinction between binary and text is important only on Windows, where the default text mode will change line endings from CR+LF into C newlines; Linux treat binary and text basically as the same, only the functions that manipulate the file will behave differently
-   **File Path**
    -   Use double bar \\\\
        
    -   Relative → from where the executable is
        
    -   Absolute → from "C:"

-   **File handler**

    -   A pointer to the file
-   The pointer must be of the type FILE (a structure type)
    -   é apenas um inteiro, que é gerenciado pelo SO

-   **EOF**

    -   Last "character" of the file

    -   It doesn't really exist in the file, is just a return value of
        the stdio lib
        ([discussion](https://stackoverflow.com/questions/3061135/can-we-write-an-eof-character-ourselves))

    -   Setted by fgets, fgetc

## Main functions

-   **fopen ("file\_path", "mode")**

    -   Opening

    -   Return the file handler

    -   If the file can't be open, the return handler is NULL

-   **fclose (FILE \*\<fp\>)**

    -   Closing

    -   Return 0 if success

    -   Guaranties that the stream buffer is empty

-   **Fseek (FILE \*\<fp\>, int \<shift\>, int \<origin\>);**

    -   Displace the cursor from a relative point

    -   Return zero if everything is OK

    -   Shift → number in bytes of displacement

    -   Origin

        -   Beginning point of displacement.

        -   SEEK\_CUR → from current position

        -   SEEK\_SET → from the begin of the file

        -   SEEK\_ END → from the end of the file

-   **Fsetpos (FILE \*\<fp\>, fpos\_t \*\<pos\>)**

    -   Set cursor to a specific point

-   **Rewind **

    -   Set the cursor to zero

    -   Clear the indication of EOF and errrors

    -   Forget any virtual character putted by ungetc

## Text file (.txt)

-   Made by ASCII characters (1 byte)

-   Easy to a human read and manipulate

-   The access is sequential

-   When changing from the read operation to the write, be careful with
    the cursor: find the EOF or overwrite the content

-   **Read Functions**

    -   fscanf (FILE \*, "formatter", &variable)

        -   Read formatted string

    -   feof (FILE \*) → have found EOF in last read?

    -   fgetc (FILE \*) → Read a character

    -   fgets (char \*\<buffer\>, int \<size\>, FILE \*\<handler\>)

        -   Read a string

        -   Read until '\\n', EOF or \<size\>-1 characters

        -   Best way to read a file line by line:

$$while\ (fgets(buffer,\ TAM\_ BUFFER,\ fp)\ ! = \ NULL)\{//do\ something\}$$

-   *Example:*

fgets (name, sizeof (name), fp);

-   Ungetc

    -   Put virtually the character back

    -   Does not modify the file, just the buffer

<!-- -->

-   **Write functions**

    -   fprintf (FILE \*, "string and formater", variables) → Print
        formatted string

    -   fputc (char, FILE \*) → Print a character

    -   fputs (char \*, FILE \*) → Print a string

## Binary file (.dat, .bin)

-   the information is stored just as in the memory

-   The access can be random

-   When reading lotsa files, you don't need to look for EOF. When
    reading just one character, you need to.

-   To see a binary file, its needed a hexadecimal editor

-   **Functions**

    -   Read a binary

        -   Fread (void \*\<buffer\>, size\_t \<numbytes\>, size\_t
            \<count\>, FILE \*fp)

        -   Buffer → vector to where the data goes

        -   Numbytes → size (in bytes) of each element

        -   Count → max numbers of elements to be read

        -   Return the number of read characters

    -   Write a binary

        -   Fwrite (const void \*\<buffer\>, size\_t \<numbytes\>,
            size\_t \<count\>, FILE \*\<fp\>);
-   Buffer → vector from where the data comes
        -   Numbytes → size (in bytes) of each element
-   Count → number of elements to be written
        -   Return number of written elements
-   Will write the binary value in big endian

## SO processes

* **Popen**
  * from stdio
  * opens a process by creating a pipe
* **executing commands on shell**
  * execvp(char *command, char *params[])

# String manipulation

## String.h

-   char\* strcpy (char\* \<destination\>, char\* \<source\> )

    -   Copy a source string into destination string

-   Strcat → concatenate

-   Length → return length, not including the last index '\\0'

-   Strcmp → compare two strings (return 0 if equal)

## STDIO functions

-   Sprintf → print formatted to string (return number of characters,
    without \\0)

-   **Special operators**

    -   \# → transform to string

    -   \#\# → concat

## Reading 1 character

-   **Getchar**

    -   Wait for enter (just read after enter)

    -   With eco

    -   The \\n will stay in buffer, so you need to clear the buffer

-   **Scanf →** same as getchar?

-   **Getch**

    -   Don't wait for enter (read instantly)

    -   Without eco

    -   Need to include conio.h

    -   Receive directly what you write (don't read the buffer)

    -   Getche → with eco

## Reading strings

-   **Scanf**

    -   Read string until space (the rest stay in buffer)

    -   The second parameter don't need & (don't receive an address)

    -   Always clear the buffer before using scanf (ffflush(stdin);)

    -   To read until enter → scanf ("%\[\^\\n\]s", string);

    -   Limitating the read → scanf (\"%\[a-z A-Z\]s\", string);

-   **Gets**

    -   Read string until enter

    -   Parameter: pointer to the string

    -   Return the same pointer to the string

    -   *Example:*

$$\ \ \ char\ str\lbrack 50\rbrack;$$

$$\ \ \ gets(str);$$