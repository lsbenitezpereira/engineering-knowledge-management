[TOC]

# Conceituação

* Coletar, organizar, descrever, analisar e interpretar dados 

* Parâmetro: descrição numérica de uma característica populacional

* Estatística: descrição numérica de uma característica amostral 

  > “While the classical mathematical tools, exemplified by the calculus and differ-
  > ential equations, are applicable to problems that involve only a very small number
  > of components that are related in perfectly predictable ways, the applicability of
  > statistical tools is exactly the opposite: they require a very large number of
  > components and a high degree of unpredictability (randomness). These two types
  > of mathematical tools are thus complementary. When one of them excels, the other
  > totally fails.” George J. Klir

## Técnicas de amostragem

* temos que garantir que a nossa amostra é repreentativa (mesma distribuição estatística dos dados originais)

* população: coleção de todos os resultados de interesse

* Amostra: subgrupo analisado da população

* censo: dados de uma população inteira 

* A validade da nossa inferência futura dependera diretamente do método de amostragem e do tamanho da amostra

* **Tipos de amostra**

  * aleatória simples
    * sorteio lotérico
    * utilizamos algoritmos de aleatoriedade para escolher
    * pode ser com ou sem reposição dos dados (tira ou não) 
  * Proporcional estratificada
    * A amostra mantêm a mesma distribuição da população original (mesma proporção de grupos diferentes)
    * garante a balanceamento dos dados entre os atributos 
  * estratificada oversampled
    * os grupos são selecioados em proporções maiores que na população original, garantindo que cada grupo tenha amostras suficientes para gerar uma estatística válida
  * Amostragem por agrupamento
    * selecionamos *todos* os elementos de um ou mais grupos (localidade geográficas, características físicas, etc).
    * Os grupos precisam ter semelhantes (ou pelo menos sem diferenças que influenciem na pesquisa), senão a amostra será tendendiosa 
  * Amostragem sistemática
    * com os dados já organizados/ordenados, selecionamos segundo um padrão definido 
  * Amostragem por conveniência
    * escolher os membros disponíveis da população.

* **Tamanho da amostra**

  * Rigor estatístico: Quão deve ser o tamanho da amostra para que ela seja
    representativa do conjunto total (asism como a margem de erro,
    etc)
  * Amostragem progressiva: vamos aumentando seu tamanho com a necessidade

* **Reposição**

  * Se podemos escolher o mesmo membro da população mais de uma vez
  * Amostra com reposição: pode 
  * Amostra sem reposição: não pode (o elemento é retirado do universo de escolha)

  


## Pesquisa
* Devemos sempre ter claro o que queremos observar e quais os instrumentos de coleta
* Uma pesquisa precisa ter controle, aleatorização e replicação
* **Tipos de pesquisa**
  * experimental: faremos testes específicos. Tentamos fazer com que apenas as variáveis de interesse variem. 
  * observacional: quando não é possível induzir experimentos nem mudar as condições existentes. Raramente é possível manter variáveis constantes.
  * Simulada: baseada em modelos físicos/matemáticos
  * Levantamento: investigação das características de uma população (entrevistas, análises de mercado, etc)
* **Etapas da pesquisa**
  * Identificar as variáveis de interesse e a população do estudo (foco)
  * Planejamento (definição da amostragem, forma de coleta de dados, etc)
  * Coleta de dados
  * Descrição estatística dos dados
  * Interpretação e inferência
  * Identificar erros e melhoramentos futuros 
* **Fenônemos de pesquisa** 
  * Efeito hawtorn: a população muda de comportamenta por saber que estão participando de uma pesquisa
  * Variável confounding
    * quando não se pode saber a diferença entre os efeitos de diferentes fatores em uma variável 
    * Esses fatores possuem correlação com a variável, porém não é possivel determinar se há uma relação de causa-consequência ou a magnitude dessa relação
  * Efeito placebo
  	* O sujeito reage favoravelmente ao placebo (tratamento medicamentoso falso)
  	* técnica cega: o sujeito não sabe se é placebo ou não
  	* técnica dupla-cega: nem o sujeito nem o pesquisador que administrará a droga sabem 
* **Planejamento por blocos**
  * Dividimos nossa amostra em blocos segundo uma certa característica, para estratificar nossa amostra
  * dividimos novamente em grupos (esses sim serão analisados, os blocos são apenas para organização), sendo que os sujeitos podem ser atribuidos aos grupos de forma aleatória ou não
* **selection bias**
  * This kind of bias occurs when the sample group being studied differs from the target population in some relevant way
  * we can recover from bias by collecting data on the right set of deconfounders and controlling for them


# Estatística descritiva

* Focada na análise de um conjunto de dados, definindo métricas para analisálos (media, mediana, devio padrão, coveriância, etc)

* **momento**

  * é uma medida foda

  * seus casos particulares (ou instâncias) são medidas “famosas”

  * definição:
    $$
    definição
    $$

  * k=1	→ primeiro momento central = Zero

  * k=2	→ segundo momento central = Variância 

  * k=3	→ terceiro momento central = obliquidade

  * k=4	→ quarto momento central = curtose da escala

  * Podemos dividir pelo desvio padrão elevado a K para tornar a medida independente 

* **Outras anotações**

  * Dicas gerais:

  ![1554385366338](Images - 1.0 - Estatística/1554385366338.png)
  
  ![1554385383603](Images - 1.0 - Estatística/1554385383603.png)

## **Medidas de localidade**

* Indicam a tendência central/coisa-do-genero

* Rol: Organização dos dados em ordem crescente ou decrescente  

* **média**

  * Or *mean*, or *average*

  * muito sensível a outliers

  * Média populacional ($\mu$): toda a população

  * Média amostral ($\bar{x}$): só a amostra 

  * aritimética simples (soma os valores e divide pelo número de valores):
    $$
    \bar{X}=\frac{\sum_{i=1}^n x_i}{n}
    $$

  * Podemos aproximar a média a partir de um histograma de frequência, aproximando que todos os pontos de uma categoria estão no seu ponto médio e fazendo a média ponderada, ou seja, $\bar{x} = \frac{\sum x f}{\sum f}$. Quando menor o tamanho das classes e quanto maior o número de exemplos, mais esse aproximação é verídica 

  * Aritimética ponderada (pesos diferentes):
    ![img](1.0 - Estatística - Images/1544733481583.png)

  * média geométrica:

    ![img](1.0 - Estatística - Images/1544733523794.png)

  * média harmônica: Se um conjunto de números não-idênticos é submetido a um "espalhamento"
    que preserve a média, ou seja, dois ou mais elementos do conjunto são
    "afastados" do outro, deixando a média aritmética inalterada, a média
    harmônica sempre diminui.
  $$
  definição??
  $$

* **moda**

  * Valor que mais se repete
  * Amodal → sem moda (todos diferentes)
  * Bimodal→ uma única moda
  * trimodal→ três valores que aparecem mais

* **Mediana**

  * Valor central do rol ($n/2$ arredondado para cima)
  * menos sensível a outliers
  * Se for par: média aritmética simples dos dois valores centrais 

* **fractis**

  * numero que separa o rol em partes iguais (ou aproximadamente iguais)
  * Se a divisão não for exata, tiramos a média aritmética dos valores

* **quartil**

  *  $Q_1, Q_2, Q_3$

  *  often used to check whether a distribution is symmetric

  *  interquartile range (IQR): $Q_3 - Q_1$ (measure the spread)

  *  geralmente são considerados outliers  dados que estiverem 1,5 IQR acima de $Q_3$ (ou abaixo de $Q_1$), ou seja, se:

     ![image-20200430185844437](Images - 1.0 - Estatística/image-20200430185844437.png)

     > k=1.5
     
  * Algortithms to calculate approximate quartiles/percentiles cummulatively:  forward decay, t-digest, or HdrHistogram

  * Do not average quartiles/percentiles!

  * <u>Decil</u>: divide em 10 partes iguais

  * <u>percentil</u>: porção percentual do rol (também chamado de decil); p50, p75, p99, etc



## Medidas de espalhamento

* **Amplitude**

  * Máximo - mínimo

* **Variância  ($\sigma^2$)**

  * Quão distante os valores estão da média

  * A média da diferença entre cada valor e a média aritmética simples.

  * Covariancia consigo mesmo (ver em *Descrição de dados multivaridos*)

  * Definição:
    $$
    \sigma^2=\frac{\sum(x_i - \bar{x})^2}{n}
    $$

  * <u>Correção de Bessel</u>

    * Denominador $n-1$
    * Dá uma melhor estimativa quando trabalhamos com dados amostrais, ao invés de populacionais
    * Ideia instintiva: se consideramos só uma amostra, a variância tende a ficar menor. Assim, diminuímos a denominador para aumentar a variância
    * Se for utilizada para variância, deve ser utilizado também para o desvio padrão (e outras medidas que derivem disso)

* **Desvio padrão ($\sigma$)**

  * A variância distorce um pouco os valores (por estar ao quadrado)  

  * Medida de o quanto um objeto típico de desvia da média

  * O desvio padrão “concerta” isso
    $$
    \sigma=\sqrt{\frac{\sum(x_i - \bar{x})^2}{n-1}}
    $$

    > já considerando a correção de Bessel
    
  * Teorema de chebychen
    
    * Pelos menos $1-1/k^2$ (percentual) dos dados está sempre a uma distância de no máximo k devios padrões da média	
    * Válido para qualquer distribuição

* **Erro padrão ($\bar \sigma$)**

  * Ou *desvio padrao da média* ou *standard error of the mean (SEM)*

  * how far the sample mean of the data is likely to be from the true population mean (considering it a gaussian)

  * SEM is always smaller than the SD

  * SEM is the SD of the theoretical distribution of the sample means (the sampling distribution)

  * Standard Deviation is a descriptive statistic, whereas the standard error is an inferential statistic
    $$
    \bar \sigma = \frac{\sigma}{\sqrt n} = \sqrt{\frac{\sum(x_i - \bar{x})^2}{n(n-1)}}
    $$
    

* **Desvio de um objeto (d)**

  * or *deviation*

  * diferença entre seu valor e a média da amostra 

  * A soma dos desvios será necessariamente zero
  $$
    dx_i=x_i - \mu_x
  $$
    > $\mu_s=$ mean of the series

* **Escore padrão de um objeto (z)**

  *  quantos desvios padrão uma entrada $x$ esta da média $\mu$

  *  normalization of the deviation

  *  Adimensional (ou podemos considerar o “desvio padrão” como unidade)
    $$
    z=\frac{x-\mu}{\sigma}
    $$

  * para uma gaussiana, 95% dos dados possuem $-2<z<2$

  * ==The distributions of the standard scores of an random variable has mean 0 and variance 1?==

* **Coefficient of variation**

  * how much that variation if significant (compared to the mean)

  * To compare variability between groups
    $$
    σ/μ
    $$
    
  
* 

* **Amplitude**

  * Diferença entre o maior e o menor valor

## **Medidas de distribuição**

* descrevem como os dados estão espalhados 
* **obliquidade**

  * ou *skweness*

  * mede a assimetria 

  * zero: simétrica

  * positiva: cauda mais longa à direita

  * negativa: cauda mais longa à esquerda

  * Critério de bowley

    * coeficiente quartil de assimetria 

    * Só leva em conta os 50% centrais 

    * Entendimento: O quanto o primeiro e o terceiro quartil se afastam da média. O que se afastar mais “ganha”. Normalizamos o resultado

    * Definição: 
      $$
      A_s = \frac{Q_3+Q_1-2M_d}{Q_3-Q_1}
      $$

      > Md = mediana
      >

  * Critério de Kelley

    * faz pelo 1º e 9º decil
      $$
      A_s = \frac{P_{90}+P_{10}-2M_d}{P_{90}-P_{10}}
      $$

  * Critério de Pearson 1

    * Baseado na moda

    $$
    A_s=\frac{\bar{x}-M_o}{\sigma}
    $$

    > $\mu$ = média
    >
    > $M_o$ = moda
    >
    > $\sigma$ = desvio padrão

  * Critério de Pearson 2

    * Baseado na médiana
    * Instinctive interpretation: extreme values have more effect on the mean than the median
    * less vulnerable to the effect of outliers.
    $$
    A_s=\frac{3(\bar{x}-M_d)}{\sigma}
    $$
* **curtose (k)**

  * ou kurtosis

  * achatamento da distribuição (gordinha ou magrinha)

  * Podemos aplicar uma correção na formula para que seu valor base seja zero

  * zero	→ mesmo achatamento de uma distribuição normal (mesocúrtica)

  * positiva	→ mais alta e concentrada (leptocúrtica)

  * negativa	→ mais achatada (platicúrtica)

  * ==curtose da gaussiana = 0,263?==

  * Calculamos pela diferença de amplitude entre quartils e centis
    $$
    k=\frac{Q_3-Q_1}{2(P_{90}-P_{10})}
    $$
    

## Descrição de dados multivariados

* Possuem mais de um atributo de entrada

* Podemos usar medidas univariadas para analisar cada atributo individualmente

* **covariancia**

  * grau em que dois atributos variam juntos 

  * indicates how closely two variables are related	

  * joint variability of two random variables

  * Only measure linear relationships  

  	![1551840871243](1.0 - Estatística - Images/1551840871243.png)

  * próximo de zero	→ relacinamento não linear

  * positivo	→ diretamente proporcionais

  * negativo	→ inversamente 

  * afetada pela dimensão (unidades, que geralmente são diferentes) dos atributos 

  * Variancia ($\sigma^2$) = covariancia com sido mesmo 

  * Covariance matrix

    * Or *auto-covariance matrix* (becaue a group of random variables can be put togueder in one single matrix)
    * Podemos montar uma matriz para analisar a relação entre todos os atributos (tomados 2 a 2)
    * Geralmente visualizamos com um heatmap
    * diagonal da matrix → variancia dos atributos 
    * The inverse of the covariance matrix is called the precision matrix
    * Properties
      * positive semi-definite
      * symmetric

* **correlação (p)**

  * also called *Pearson Correlation*

  * corrige (normaliza) a distorção da covariancia em relação à dimensão

  * also only measure linear relationships  

  * Dimentionless

  * valor entre -1 e 1
    $$
    p=\frac{1}{n}\sum{\frac{x_i-\mu_x}{\sigma_x}\frac{y_i-\mu_y}{\sigma_y}}
    $$
    ![1551841116685](1.0 - Estatística - Images/1551841116685.png)

  * Também podemos fazer uma matriz de correlação para analisar todas as variáveis com todas as variáveis

  * :keyboard:Python pandas:

    ```
    corr=df.corr(method='person')
    sns.heatmap(corr, annot=True, cmap='Reds')
    ```
    
  * Correlation vs causation:

    <img src="Images - 1.0 - Estatística/aeDQ02B_460s.jpg" alt="Reality" style="zoom:50%;" />

* **Spearman coefficient**

  * Transform all values to their percentile ranks, then compute Pearson’s correlation for the ranks.
  * mitigates the effect of outliers and skewed distributions
  * measures monotonic association
  * better metric for non-gaussian distributions

* **Kendall's Tau**

  * does not take into account the difference between ranks — only directional agreement
  * more appropriate for discrete data

* **KSG mutual information estimator**

  * Describes causation

* **Maximal Information Coefficient**

  * Describes causation

* 

* 

* 

* **==Redundância?==**

* **==Correspondência?==**

# Distribuição estatística

* Ou *distribuição de frequência*

* Falaremos aqui de, a partir de dados, analisar a distribuição teórica que gerou os dados. A análise dessa *distribuição teórica* propriamente dita será feita mais a frente, sob uma ótica probabilística

* Contagem da quantidade de objetos de entrada pertencentes a cada classe (bin)

* Geralmente determina-se as classes em intervalos regulares sem sobreposição

* Classes grandes suavizam ruídos e facilitam a análise, mas podem descartar dados importantes

* **Descrição da classe**

  * Largura de classe = amplitude da classe

  * Fronteiras da classe: numeros que separam as classes sem formar lacunas entre elas

  * Limites da classe: maior e menor numero que pode pertencer a classe 

  * ponto médio da classe

    * ou *marca de classe* 

    ![1550794346078](Images - 1.0 - Estatística/1550794346078.png)

  * Frequencia relativa da classe

    * porção da amostra pertencente a uma classe 

    * A soma da frequência relativa de todas as classe deverá ser 100%

      ![1550794405026](Images - 1.0 - Estatística/1550794405026.png)

  * Frequencia acumulada da classe

    * somatório da classe e das anteriores
      $$
      FA(c)=\sum_{i=1}^cFR
      $$

## Formas de distribuição

* **Uniforme**
  * Ou *retangular*
  * Todas as entradas com a mesma frequencia 
* **Simétrica**
  * Se for simétrica e unimodal, média e mediana e moda serão iguais 
* **Assimétrica à esquerda**
  * Ou *negativamente assimétrica*
  * Média menor que a mediana 
* **Assimétrica à direita**
  * Ou *positivamente assimétrica*
  * Média maior que a mediana

## Visualização de distribuições

* **Histograma de frequência**

  * maps from values to integer counters

  * gráfico de visualização, representando as classes por barras verticais

  * a escala vertical pode ser o numero de membros de cada classe ou sua frequencia relativa 

  * Exemplo:

    ![1550794910202](Images - 1.0 - Estatística/1550794910202.png)

* **Histograma de frequência relativa**

  * ou *Histograma de frequência normalizado*
  * os valores vão de zero a 1
  * maps from values (integer) to probabilities (float point)

* **Diagrama de ramos e folhas**

  * para cada classe, ao invés de “barras gráficas”, vamos “empilhando” os valores pertencentes aquela classe
  * dá pra fazer uma variação bizarra agrupando pelos primeiros dígitos comuns de cada classe.

* **Poligono de frequencia**

  * versão contínua do histograma

  * interpola linearmente os pontos médios das classes

  * Exemplo:

    ![1550795060434](Images - 1.0 - Estatística/1550795060434.png)

* **Gráfico de frequencia acumulada** 

  * ou *ogiva*

  * interpolação linear da frequência acumulada, representadas em sua fronteira superior

    ![1550795293193](Images - 1.0 - Estatística/1550795293193.png)

## Curva de frequência

* Se, em uma distribuição de frequência, aumentarmos o numero e amostras e diminuirmos o tamanho das classes, então nos aproximaremos de uma curva contínua
* a mediana corta a distribuição em áreas iguais (área sob a curva = nº de amostras)
* a moda corta a distribuição no seu ponto máximo (maior valor = maior nº de acontecimento = maior probabilidade) 
* a média não representa nada em especial, pois pende para a “cauda” mais longa

# Calculos estatisticos incrementais

* ==qual o nome certo?==

* calcular estatisticas de forma incremental, sem manter todos os valores em memória

* **Média**

  * :book: RL barto, pg 54

  * considere Q sendo a média e R o ultimo valor

    ![image-20201230150255681](Images - 1.0 - Estatística/image-20201230150255681.png)

* **exponential recency-weighted mean**

* * for non stationary mean

  *  the step-size parameter α 2 (0, 1] is constant.

  * Q is a weighted average of past rewards (farther -> less important)

  * weight decays exponentially

    ![image-20201230150153988](Images - 1.0 - Estatística/image-20201230150153988.png)

# Causal statistics

*  Or *causal inference*

*  demonstrating that X directly influences Y, independently of other factors

*  Identification of the cause or causes of a phenomenon, by establishing covariation of cause and effect, a time-order relationship with the cause preceding the effect, and the elimination of plausible alternative cause

*   

*  X causes Y if P(Y | do(X)) > P(Y)

*  you cannot draw causal conclusions without some causal hypotheses/model

*  **kinds of causation**

   *  <u>necessary causation</u>
      *  Or *but-for* causation
      *  The Model Penal Code expresses it as follows: “Conduct is the cause of a result when it is an antecedent but for which the result in question would not have occurred.”
      *  probability of necessity (PN): $P(Y_{X = 0} = 0 | X = 1, Y = 1)$; the probability that X = 1 is a necessary cause of Y = 1

   *  <u>sufficient causation</u>
      *  probability of sufficiency (PS): $P(Y_{X = 1} = 1 | X = 0, Y = 0)$

   *  <u>necessary-and-sufficient causation</u>

*  **Hill’s criteria**

   *  set of guidelines to define causation
   *  informal/subjective
   *  Every one of these criteria has demonstrable exceptions, although collectively they have a compelling commonsense value
   *  Some of them may be missing for a particular case
   *   
   *  consistency (many studies, in different populations, show similar results)
   *  strength of association (including the dose-response effect)
   *  specificity of the association (a particular agent should have a particular effect and not a long litany of effects)
   *  temporal relationship (the effect should follow the cause)
   *  coherence (scientific plausibility and consistency with other types of evidence)

*  **dose-response effect**

   *  if action A causes an effect B, then a larger dose of A causes a stronger response B

*  

*  

*  **causal analysis**

   *  assuming the relationships (arrows), find weights
   *  we must have some understanding of the process that produces the data

*  **causal discovery**

   *  find the cause of Y from scratch
   *  Basic idea 1: fitting different reasonable structures, and select one using goodness of fit tests

   ![image-20250103135150271](./Images - 1.0 - Estatística/image-20250103135150271.png)

*  

*  **Levels of causal understanding**

   *  <u>seeing</u>
      *  or *observing*, or *associating*
      *  detection of regularities in our environment

   *  <u>doing</u>
      *  or *intervining*
      *  predicting the effects of deliberate alterations of the environment
      *  involves not just seeing but changing what is

   *  <u>imagining</u>
      *  or *counterfactual reasoning*
      *  why it works and what to do when it doesn’t
      *  “What if I had done…?” and “Why?”
      *  involve comparing the observed world to a counterfactual world
      *  need a model of the underlying causal process, a “theory”

*  **Path analysis**

  *  invented by geneticist Sewall Wright in the 1920s
  *  assumes that all the relationships between any two variables in the path diagram are linear
  *  you can equate paths similarly 2 ythevenin?

*   **structural equation modeling (SEM)**

  *  Much used in sociology
  *  once we proclaim an equation to be “structural”, we commit to writing equations that mirror our belief about who listens to whom in the world
  *  only linear
  *  can be estimated from observational data by linear regression

*  **simultaneous equation models**

  *  Much used in economics
  *  Purely algebraic

*  **structural causal model**

  *  represent counterfactuas as missing values
  *  not necessarily linear

*  **Causal diagrams**

  *  to express what we know
  *  Bayesian network in which every arrow signifies a direct causal relation
  *  statements like “There is no path connecting D and L” translates to a statistical statement, “D and L are independent”
  *  omitted arrows actually convey more significant assumptions than those that are present. An omitted arrow restricts the causal effect to zero, while a present arrow remains totally agnostic about the magnitude of the effect

*  **Do-operator**

  *  Or *intervention operator*
  *  signifies that we are dealing with an intervention rather than a passive observation; classical statistics has nothing remotely similar to this operator
  *  $do(A)$ means “once we hold A constant”, physically preventing A from varying and disabling any incoming arrow/influence
  *  erase all the arrows pointing to A (including the ones we don’t know about or cannot measure)
  *  <u>interventional probability</u>: P(Y | do(X))
  *  If we can rephrase a $do$-based equation into an equation without $do$, then we can use observational data

*  **Do-calculus**

  *  ?
  *  makes it possible to calculate the ideal adjustment method to any particular causal diagram
  *  <u>Rules/axioms</u>
    *  Insertion/deletion of observations
    *  Action/observation exchange
    *  Insertion/deletion of actions
    *   
    *  The rules are complete; that is, all identifiable intervention distributions can
      be computed by an iterative application of these three rules

  *  <u>Shpitser algorithm</u>
    *  decides if a solution exists
    *  polynomial time

  *  <u>Elias Bareinboim algorithm</u>
    *  2012
    *  whether we can find a Z such that we can transform P(Y | do(X)) into an expression in which the variable Z, but not X, is subjected to a do-operator.

*  **Rubin causal model**

  *  Used for potential outcomes / conterfactuals
  *  $Y_{X = x}(u)$: the value that Y would have taken for individual u, had X been assigned the value x

*  **Baron-Kenny method**

  *  1986
  *  set of principles for detecting and evaluating mediation in a system of equations
  *  The essential principles are, first, that the variables are all related by linear equations, which are estimated by fitting them to the data. Second, direct and indirect effects are computed by fitting two equations to the data: one with the mediator included and one with the mediator excluded
  *  Significant change in the coefficients when the mediator is introduced is taken as evidence of mediation

*  

*  

*  **Confounding**

  * Or *background factors*, or *lurking third variable*
  * When a variable that influences both the dependent variable and independent variable
  * Example: assess the effectiveness of drug *X* (independent variable), from population data in which drug usage was a patient's choice. The data shows that gender (*Z*, confounder variable) influences a patient's choice of drug as well as their chances of recovery (*Y*, outcome, dependant variable)
  * anything that leads to the discrepancy $P(Y | X) ≠ P(Y | do(X)).$
  * <u>Classification by source</u>
    * Operational: when a measure designed to assess a particular construct inadvertently measures something else as well
    * Procedural: when the researcher mistakenly allows another variable to change along with the manipulated independent variable
    * Person: when two or more groups of units are analyzed together (e.g., workers from different occupations), despite varying according to one or more other (observed or unobserved) characteristics (e.g., gender)

*  **Mediators**
  * Variables that lie on the causal path between X and Y
  * Similar to confounding, 

*  **Colliders**
  * Variables affected by both X and Y
  * Should not be adjusted for
  * e.g., fitness is influenced by diet and genetics
  * If a variable is a collider or a confounder is sometimes a matter of perspective
  * if there is feedback, then a collider may be at the same time a confounder

*  **Counterfactual reasoning**

  *  deals with what-ifs

*  **transportability**

  *  when results of an study are transportable/translatable from one setting to another (like different locations and under different conditions)
  *  <u>Elias Bareinboim criterion</u>
    *  Strongly based on causal diagrams
    *  represent the salient features of the data-generating process with a causal diagram, marked with locations of potential disparities
    *  if you can perform a valid sequence of do-operations that transforms the target quantity into another expression in which any factor involving S is free of do-operators, then the estimate is transportable
    *  Elias Bareinboim algorithm: can automatically determine for you whether the effect you are seeking is transportable

*  

*  

*  **Causal models**

  *  allows us to predict the result of an intervention from intervention-free data
  *  receives a query and can say whether  it can answer it
  *  if it can, then data is required to compute the actual answer you can calculate the degree of fitness between model and data
  *  Equivalence of causal models
    *  Two models are called probabilistically equivalent if they entail the same obs. distributions
    *  interventionally equivalent if they entail the same obs. and int. distributions
    *  counterfactually equivalent if they entail the same obs., int., and counterf. distributions

*  **causality and generalization**

  * Arguibly, models that learn causal relationships can generalize better
  * "Any agent capable of adapting to a sufficiently large set of distributional shifts must have learned a causal model" ([source](https://www.semanticscholar.org/paper/Robust-agents-learn-causal-world-models-Richens-Everitt/99a1060a6aa6cad15b37ac6566b20ba992b16b84))

*  **Classification of methods**

   * Experimental: assigments are designed to match the analysis. Requires manipulating the data collection
   * Quasi-experimental (or *observational data*): random assignment (thus can be done after data is collected). Subjects choose their own treatment

*  **Software libraries**

   *  <u>DoWhy</u>
      *  Python
      *  By microsoft
      *  part of the PyWhy set of libraries
      *  https://github.com/py-why/dowhy
      *  [paper](https://arxiv.org/abs/2011.04216)


## Experimental methods

* making an event happen (Interviing) means that you emancipate it from all other influences and subject it to one and only one influence—that which enforces its happening

* 

* **Randomized Controlled Trials**

  * By randomizing participants to exposure groups, confounders are distributed evenly, eliminating their bias
  * allows us to control for confounders that we cannot observe
  * must have samples for every possible combination of confounders
  * Control group: especific case to RCT, in which one single control group is used?

* **Difference-in-Differences (DiD)**: Compares changes in outcomes over time between treated and untreated groups. Only when we have a control group?

* **case-control study**

  * compares “cases” (people with a disease) to controls
  * (-) It is retrospective
  * (-) several possible sources of bias
  * Example: study people known to have cancer and look backward to discover why; tell us the probability that a cancer patient is a smoker instead of the probability that a smoker will get cancer

* 

* **Experimental tricks**

  * Blinding: prevent study participants, caregivers, or outcome assessors from knowing which intervention was received. May be used togehter with others

  * Placebo: inert subtance used in the control group

  * <u>problem of noncompliance</u>

    * when subjects randomized to receive a drug don’t actually take it

    * We can analyze the results by obtaining a range of estimated for the possible effect of X on Y: 

    * best-case scenario (in which all the noncompliers would have improved) and worst-case scenario (where the confounding goes completely against the drug)

      ![image-20250102220513419](./Images - 1.0 - Estatística/image-20250102220513419.png)

* **Evaluation metrics**
  * <u>effect of treatment on the treated (ETT)</u>
    * used to evaluate whether people who gain access to a treatment are those who would benefit most from it
  * <u>average causal effect (ACE)</u>
    * conventional measure of a treatment’s effectiveness
    * you can get from a RCT
  * <u>controlled direct effect (CDE)</u>
    * For models of treatment X, an outcome Y, and a mediator M
    * $CDE(0) = P(Y = 1 | do(X = 1), do(M = 0)) – P(Y = 1 | do(X = 0), do(M = 0))$, where the “0” in CDE(0) indicates that we forced the mediator to take on the value zero
  * <u>natural direct effect (NDE)</u>
    * $NDE = P(Y_{M = M_0} = 1 | do(X = 1)) – P(Y_{M = M_0} = 1 | do(X = 0))$
    * can estimate it from observational data
  * <u>natural indirect effect (NIE)</u>
    * can estimate it from observational data
    * $NIE = P(Y_{M = M_1} = 1 | do(X = 0))–P(Y_{M = M_0} = 1 | do(X = 0))$
    *  
    * $NIE = Σ_m [P(M = m | X = 1)–P(M = m | X = 0)] × × P(Y = 1 | X = 0, M = m)$
    * makes no linear assumptions... or just this second equation form? What exactly cahnges in this second form?

## Quasi-experimental methods

* Mainly focused in fixing / account for confounding/colliders/mediators without having to performm experiments

* It's always some kind of correction/adjustment/controlling/conditioning over the confounder variable

* information about the effects of actions or interventions is simply not available in raw data, unless it is collected by controlled experimental manipulation

* prediction of interventions (what if) can be made from quasi experiments only if we have  a causal model

* **When to control for what**

  * sometimes, you can control for too much. Sometimes you end up controlling for the thing you’re trying to measure.

  * if you have identified a sufficient set of deconfounders in your diagram, gathered data on them, and properly adjusted for them, then you have every right to say that you have computed the causal effect X -> Y

  * If we have data on a sufficient set of deconfounders, it does not matter if we ignore some or even all of the confounders

  * you should only control for Z if you have a “strong prior reason” to believe that it is not affected by X

  * explain-away effect: controlling so much that you can't conclude anything?

  *  

  * In a chain junction, $A \rightarrow B \rightarrow C$, controlling for B prevents information about A from getting to C. Conditioning on a mediator is incorrect if we want to estimate the total effect of A on C

  * in a fork or confounding junction, $A \leftarrow B \rightarrow C$, controlling for B prevents information about A from getting to C or vice versa

  * in a collider, $A \rightarrow B \leftarrow C$, The variables A and C start out independent, so that information about A tells you nothing about C. But if you control for B, then information starts flowing through the “pipe” (explain-away effect). Conditioning on a collider creates a spurious association

  * Controlling for descendants (or proxies) of a variable is like “partially” controlling for the variable itself. Controlling for a descendant of a mediator partly closes the pipe; controlling for a descendant of a collider partly opens the pipe

  * Example: no control needed. Controlling for B alone is wrong. It’s correct to control for B if you also control for A or C

    ![image-20250102212011877](./Images - 1.0 - Estatística/image-20250102212011877.png)

* 

* **Stratification**

  * split data into subgroups (e.g., by age group) to analyze outcomes separately within each group
  * For models of action X affecting Y under confounder Z
  * For instance, if the confounding variable Z is age, we compare the treatment and control groups in every age group separately. We can then take an average of the effects, weighting each age group according to its percentage in the target population
  * measure the average causal effect of an intervention by first estimating its effect at each level/stratum/bucket of Z
  * We then compute a weighted average of those strata, where each stratum is weighted according to its prevalence in the population
  * 
  * <u>numerical values</u>
    * have to be bucketed/separated/binned into a finite and manageable number of categories
    * regressing Y on X for each level of Z and computing the weighted average of the regression coefficients
    * see shortcut using linear regression
    * does this have less asumptions than direclty doing linear regression, or they are exactly equivalent?

* **linear regression**

  * train a regression model to separates the influences

  * For models of action X affecting Y under confounder Z

  * we can do linear regression instead of weighted-stratfication if and only if we have a reasonable causal diagram where correcting for Z satisfy the back-door criterion; in the regression $Y = aX + bZ + c$, the coeficient $a$ will automatically adjust the observed trend of Y on X to account for the confounder Z. If Z is the only confounder, then $a$ is the causal effect of X on Y

  * <u>Example</u>

    * Because Z and X are unconfounded, the causal effect of Z on X (that is, $a$) can be estimated from the slope $r_{XZ}$ of the regression line of X on Z

    * the slope of the regression line of Z on Y ($r_{ZY}$) will equal the causal effect on the direct path $Z -> X -> Y$, which is the product of the path coefficients $ab$

    * $b = r_{ZY}/r_{ZX}$

    * No data from U is needed

    * Z is also called instrumental variable

    * this necessarily assumes assumption of linearity and monotonicity

      ![image-20250102215125301](./Images - 1.0 - Estatística/image-20250102215125301.png)

* **non linear regression**

  * In theory should work... but math gurantees are harder

* **perturbation analysis**

  * ?

* **Sensitivity analysis**

  * Instead of drawing inferences by assuming the absence of certain causal relationships in the model, the analyst challenges such assumptions and evaluates how strong alternative relationships must be in order to explain the observed data
  * The quantitative result is then submitted to a judgment of plausibility

* **back-door criterion**

  * unambiguously identifies which variables in a causal diagram are deconfounders

  * test for confounding

  * checks if we are properly controlling all variables

  * Strongly based on causal diagrams

  * (+) works for non-linear relationships

  * For models of action X affecting Y and confounder Z

  * If we can measure variables all Zs that block all the back-door paths, we can control for Z to obtain the needed effect

  *  

  * the Test  passes if no backdoor path is present

  * <u>back-door path</u>: is any path from X to Y that starts with an arrow pointing into X;  allow spurious correlation between X and Y

  * X and Y will be deconfounded if we block every back-door path

  * to deconfound two variables X and Y, we need only block every noncausal path between them without blocking or perturbing any causal paths

    ![image-20250102214449136](./Images - 1.0 - Estatística/image-20250102214449136.png)

*  **front-door adjustment**

  * It differs from the back-door adjustment in that we adjust for two variables instead of one

  * for models X -> Z -> Y, where X and Y and confounded by an unobservable U

    ![image-20250103142310712](./Images - 1.0 - Estatística/image-20250103142310712.png)

  * allows us to control for confounders that we cannot observe

  * ==criterion (once we know what to control, just stratify for it) or adjustment (some specific trick)?==

    ![image-20250102214347196](./Images - 1.0 - Estatística/image-20250102214347196.png)

  * 