# Requires OS level dependency tkinter
# In ubuntu:
# apt install python3.10-tk

# Most of the code was copied from the function ppg_example()
# https://github.com/godamartonaron/GODA_pyPPG/blob/main/pyPPG/example.py

# Run this code with:
# ./.venv/bin/python3.10 task3.py

from pyPPG import PPG, Fiducials
from pyPPG.datahandling import load_data
import pyPPG.fiducials as FP

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import filtfilt, tf2zpk, freqz, cheby2, firwin


def plot_pole_zero(b, a, title: str = 'Pole-Zero Plot') -> None:
    z, p, k = tf2zpk(b, a)

    plt.figure(figsize=(8, 8))
    plt.scatter(np.real(z), np.imag(z), marker='o', color='blue', label='Zeros')
    plt.scatter(np.real(p), np.imag(p), marker='x', color='red', label='Poles')

    plt.axhline(0, color='black', linewidth=0.5, linestyle='--')
    plt.axvline(0, color='black', linewidth=0.5, linestyle='--')

    plt.title(title)
    plt.xlabel('Real Part')
    plt.ylabel('Imaginary Part')
    plt.grid()
    plt.legend()
    plt.show()


def plot_bode(b, a, fs, title: str = 'Bode Plot') -> None:
    nyq_rate = fs/2
    w, h = freqz(b, a, worN=8000)
    #plt.plot((w/np.pi)*nyq_rate, np.log10(abs(h)), linewidth=2)

    # Magnitude plot
    plt.subplot(2, 1, 1)
    f = (w/np.pi)*nyq_rate
    plt.semilogx(f, np.log10(abs(h)), color='blue')
    plt.title(f'{title} - Magnitude Response')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Magnitude [dB]')
    plt.grid(True, which='both', linestyle='--', linewidth=0.5)

    # Phase plot
    plt.subplot(2, 1, 2)
    plt.semilogx(f, np.angle(h, deg=True), color='red')
    plt.title('Phase Response')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Phase [degrees]')
    plt.grid(True, which='both', linestyle='--', linewidth=0.5)
    
    # Display the plot
    plt.tight_layout()
    plt.show()

def detect_and_plot_peaks(signal, title='PPG Peaks'):
    signal.vpg = np.gradient(signal.ppg)
    signal.apg = np.gradient(signal.vpg)
    signal.jpg = np.gradient(signal.apg)
    s = PPG(s=signal, check_ppg_len=check_ppg_len)

    fpex = FP.FpCollection(s=s)

    corr_on = ['on', 'dn', 'dp', 'v', 'w', 'f']
    correction.loc[0, corr_on] = True
    signal.correction=correction

    # Extract fiducial points
    fiducials = fpex.get_fiducials(s=s)
    fp = Fiducials(fp=fiducials)
    #print(fiducials)
    #plot_fiducials(s=s, fp=fp, savefig=savefig, show_fig=show_fig, print_flag=print_flag, use_tk=use_tk)
    # copied from https://github.com/godamartonaron/GODA_pyPPG/blob/main/pyPPG/datahandling.py#

    marker=[]
    marker_size=60
    marker = ['o', 's', 's','o', 'o', 's', 'o', 'o', 's', 'o', 's', 'o', 's', 'o', 's']
    color = ['r', 'b', 'g','m', 'r', 'b', 'g', 'r', 'b', 'g', 'm', 'c', 'k', 'r', 'b']
    fid_names = ('sp', 'on', 'dn','dp')
    str_sig = 0
    end_sig = len(s.ppg)
    len_sig=end_sig-str_sig
    step_small = 1
    if len_sig/s.fs<10:
        step_big = step_small * 1
    else:
        step_big = step_small * 5
    len_sig_sec=int(len_sig / s.fs)
    if len_sig_sec<1:
        len_sig_sec=1

    major_ticks_names = range(0,len_sig_sec,step_big)
    len_ticks_names=len(major_ticks_names)
    major_diff=len_sig/len_ticks_names
    major_ticks = np.arange(str_sig, end_sig, major_diff)
    major_ticks = major_ticks[0:len_ticks_names]
    plt.plot(s.ppg, 'k', label=None)
    for n in fid_names:
        ind = fid_names.index(n)
        plt_num = 1
        plt.subplot(111)
        plt.title(title, fontsize=20)
        ax = plt.subplot(1, 1, plt_num)
        tmp_pnt=eval("fp." + n + ".values")
        tmp_pnt=tmp_pnt[~np.isnan(tmp_pnt)].astype(int)
        tmp_sig=eval("s.ppg")
        plt.scatter(tmp_pnt, tmp_sig[tmp_pnt], s=marker_size,linewidth=2, marker = marker[ind], facecolors='none', color=color[ind], label=n)
    plt.xlabel('Time [s]', fontsize=20)
    plt.xticks(major_ticks, major_ticks_names, fontsize=20)
    if show_fig:
        plt.show()


data_path=""
fs=0
start_sig=0
end_sig=-1
fiducials=pd.DataFrame()
process_type="both"
channel="Pleth"
filtering=True
fL=0.5000001
fH=12
order=4
sm_wins={'ppg':50,'vpg':10,'apg':10,'jpg':10}
correction=pd.DataFrame()
plotfig=True
savingfolder="temp_dir"
savefig=False
show_fig=True
savingformat="both"
print_flag=True
use_tk=False
check_ppg_len=True
saved_fiducials=""
savedata=True
points_to_plot = 2000
signal = load_data(data_path="PPG.mat", fs=fs, start_sig=start_sig, end_sig=end_sig, channel=channel, use_tk=True, print_flag=print_flag)


############################################################
# Using his filters, but running the code myself
# https://github.com/godamartonaron/GODA_pyPPG/blob/main/pyPPG/preproc.py#L6
s=signal
b, a = cheby2(order, 20, [fL,fH], 'bandpass', fs=s.fs)
print('a', a)
print('b', b)
ppg_cb2 = filtfilt(b, a, s.v)
plot_pole_zero(b, a, title='PyPPG IIR filter')
plot_bode(b, a, s.fs, title='PyPPG IIR filter')

win = round(s.fs * sm_wins['ppg']/1000)
b = 1 / win * np.ones(win)
a = 1
ppg = filtfilt(b, a, ppg_cb2)
filtered_iir = ppg
filtered_iir = filtered_iir[:points_to_plot-740]

############################################################
# Our own filter
nyquist = s.fs / 2  # Nyquist frequency
low_cutoff = 0.5 / nyquist
high_cutoff = 12.0 / nyquist
numtaps = 2000
b = firwin(numtaps, [low_cutoff, high_cutoff], pass_zero=False)
a = 1  # For FIR filters, a = 1
ppg_cb2 = filtfilt(b, a, s.v)
plot_pole_zero(b, a, title='Our own FIR filter')
plot_bode(b, a, s.fs, title='Our own FIR filter')

win = round(s.fs * sm_wins['ppg']/1000)
b = 1 / win * np.ones(win)
a = 1
ppg = filtfilt(b, a, ppg_cb2)
filtered_fir = ppg
filtered_fir = filtered_fir[:points_to_plot-740]

############################################################
# Signal in time domain
plt.plot(signal.v[:points_to_plot], label='Original signal')
plt.plot(filtered_iir[:points_to_plot], label='Filtered with the PyPPG IIR filter')
plt.plot(filtered_fir[:points_to_plot], label='Filtered with our own FIR filter')
plt.legend()
plt.show()

############################################################
# peak detection

signal.ppg = filtered_iir
detect_and_plot_peaks(signal, 'PPG Peaks with PyPPG IIR filter')


signal.ppg = filtered_fir
detect_and_plot_peaks(signal, 'PPG Peaks with our own FIR filter')
