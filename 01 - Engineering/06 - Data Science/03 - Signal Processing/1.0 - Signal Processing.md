[TOC]

# Conceptualization

* ==this is highly complamentary with the materials about “Discrete Signals and Systems”==
* **Time series**
  * consecutive measurements
  * the observed object may be changing over time
  * See more in *Artificial Intelligence - Sequence Processing*
  * 
  * We'll usually assume measurements are equally spaced over time
  * We'll usually assume that the current measurement depends on the previous measurements
* **Useful links**
  * [curious blog with DSP stuff](http://www.windytan.com/)

## Programming techniques

* Usually DSP code is written in C, so we have a lot of flexibility to optimize code
* principal operação: MAC (multiçlicação e acumulação); acesso mutimateno a memoria de programas e de dados; operações aritiméticas com saturação ao invéz de overflow; instruction for trigonometrics or complex numbers; Circular buffers (allow efficient programming of delay lines and other data structures required in digital signal processing, and are commonly used in digital filters and Fourier transforms); Multiply and accumulate types of operations are generally subject to a great loss of numerical precision. This can be mitigated by using a temporary variable of higher precision to store values in the course of the operation, and then typecasting this variable back to the original precision after the operation is completed.
* 
* 
* 
* o processamento é feito amostra-por-amostra (tempo real) ou batch (buffer ping pong?)

## Processors for DSP

* Digital Signal Processors (DSP, too): processors specific for signal processing
* They are just microprocessor with some specific features for signal processing
* There not a line dividing DSPs and other processors, nor a minimum set of features to be considered a DSP
* Usually only used for high performance applications like: radar, telecomunicações, aviação, etc
* Usual features
  * provide assembly instruction for common DSP operations and programing patterns, like multiply-and-accumulate
* Other characteristics
  * geralmente não pssuem acessórios como: I2C, wifi, etc
  * the program usually runs in a single loop (without interruption) processing one stream of data
  * usually don't have many configuration options like low power modes, etc
  * expensive
* Some DSPs
  * ADSP-BF609 (fixed point)
  * TMS320C5535 (fixed point)
  * ADSP-21160 (does SIMD; have floating point in hardware?)
* **Other "in between" or application specific processors**
  * <u>microcntrolador com instruções de DSP</u>: ?; examplo: cortex M4
  * <u>Real time microcontrollers</u>: ?
  * <u>Embedded Vision Engines (EVEs)</u>
    - coprocessador
    - memoria propria, conjunto de instruções proprio, etc
    - instruções em hardware para operações como: convolução 2d?
  * <u>ASIC</u>
    - /eisic/
    - silício customizado
  * <u>System on a board for DSP</u>
    - beaglebone AI: https://beagleboard.org/ai; usa o AM5729 da texas

## Image processing

* image = signal (instead of the Compute Science aproach where signal = data structure )
* Motivation of Frequency Analysis to images: Salvador Dali’s paiting “Gala Contemplating the Mediterranean Sea which at Twenty Meters Becomes the Portrait of Abraham Lincoln” 
* **Decomposing an image: Basis set**
  * B is a subset of vector space V
  * B is a basic set of V if and only if B is a base of V (remember linear algebra: linear independet set that spans V )
* **Fourier decomposition**
  * provides an orthonormal basis for images
  * Natural imagem usually have similar power spectrum, and the big difference comes from the phase

## Sensor Fusion

* combining sensor data derived from disparate sources so that the resulting information has less uncertainty than would be possible if these sources were used individually
* either multiple sensors (of the same type or of different types), or multiple measurements with the same sensor
* [curiosity - Sensor fusion in humans - McGurk effect](https://www.youtube.com/watch?v=FkIfX5QioMI)
* **Classification by where the processing happens**
  * <u>Centralized fusion</u>
    * the clients simply forward all of the data to a central location, and some entity at the central location is responsible for correlating and fusing the data

  * <u>Decentralized</u>
    * the clients take full responsibility for fusing the data
    * Every sensor makes a decision and the decisions are aggregated
    * (+) paralelizable
    * (-) less accurate
    * may have some feedback from a central analyzer

* **Classification by what is performed**
  * (these levels are not mutually exclusive, and just roughly sequential in complexity / applcation-specificity)
  * Level 0 – Data alignment (sub object): fusion of the raw data
  * Level 1 – Entity Assessment (e.g signal/feature/object): tracking and object detection/recognition/identification
  * Level 2- Situation assessment: the assessment is done regarding to the complete processes
  * Level 3 – Impact assessment: We examine the impact of the decision (application specific)
  * Level 4- - Process assessment (or resource management)– on a process level (sensor management) a feedback loop in the system, background knowledge is needed; when more measurements re needed, go back and collect more data;
  * Level 5- User refinement – on the user level, the user can interact with the data; show to the user and let him decide (or ask for help)

* **Biological levels of fusion**
  * level 1: simple averaging
  * level 2: calibration?
  * level 3: oppression of one of the senses
  * level 4: completely independent senses, no fusion; does not happen in animals (nor any other known animal)

# Discrite Filters

* For frequency-selective filters (e.g., lowpass filters) it is generally desirable to have a phase response that is a linear function of frequency
  
* Linear time-invariant system designed for a specific job of frequency selection or frequency discrimination
  
* Basic elements:

  ![image-20210321195001095](Images - 1.0 - Signal Processing/image-20210321195001095.png)

* The computational complexity of a digital filter structure can be given by the total number of multiplications and the total number of 2-input additions that are required per output point
  
* complex multiplications are expensive, at least 2~3 time more

* 

* filtros seletores de frequencia (passa alta, baixa, rejeia, etc) vs filtros de equalização?

* 

* 

* 

* filtro FIR costuma ser muito maior, em impementação, ao seu equivalente IIR

* FIR sofre menos com quantização do que IIR

* filtros com tempo morto na resposta ao impulso não costumam ser algo positivo

* filtro (um sistema) instável: bibo, bota uma entrada e a saída satura

* frequencia central do filtro: $w_c=2\pi \frac{f_c}{f_s}$

* lembre que a interpretação do espectro discret depende da frequencia de amostragem

* **other form the LP**

  * passa faixa: ideal_lp(wc2,M) - ideal_lp(wc1,M)
  * rejeita faixa: ideal_lp(wc1,M) + ideal_lp(pi,M) - ideal_lp(wc2,M)
  * passa alta: ideal_lp(pi,M) - ideal_lp(wc,M)

* **Efeitos de quantização**

  * geralmente analisado via simulação

  * Casar polos e zeros diminui os erros… wtf?

  * :keyboard: matlab coeff: quantiza na forma sinal-magnitude

  * example (proakis pg 288):
  
    ![image-20210328105731643](Images - 1.0 - Signal Processing/image-20210328105731643.png)
  
* **Other annotations**

  * What is the result of having complex coeficients in the filter? you can have an assimetric filter, but what is the advantaeg of t
  * os coeficientes de um filtro também são chamados de "taps", pois seu diagrama de blocos +- parece com torneiras
  * pyFDA is a GUI based tool in Python / Qt for analysing and designing discrete time filters.
  * Online tool for filter design: https://www.micromodeler.com/dsp/

## Phase considerations
* The phase delay gives the *time delay* in seconds experienced by each sinusoidal component of the input signal

* equalizador de fase: filtro após um sistema para corrigir as distorções de fase desse sistema

* Filter delay that is constant over all frequencies can be easily  compensated for by shifting the signal in time. FIR filters usually have constant delay

* [practical matlab example](https://www.mathworks.com/help/signal/ug/practical-introduction-to-digital-filtering.html;jsessionid=4e60fd5c9745eca206ff244b58d8#zmw57dd0e3504)

* **group delay**

  * $D(w)$
  * derivate of fase
  * ⌨️ matlab `grpdelay`: plots group delay vs frequency
  
  $$
  {D(\omega) = - \frac{d}{d\omega} \Theta(\omega).}
  $$
  
  
  
* **constant phase filters**: impossible?

* **Linear phase filters**

  * phase is a straight line not passing by the origin

  * linear phase = constant group delay

  * That characteristic is usually desirable

  * Se a fase não for linear, então sinais com varias frequencias (quase todos) vão ser  "misturados" no tempo ao passar pelo filtro

  * (+) design problem contains only real arithmetic and not complex arithmetic

  * (+) provide no delay distortion and only a fixed amount of delay

  * (+) less operations in the implementation

  * Linear phase transfer function:

    ![image-20210402144857677](Images - 1.0 - Signal Processing/image-20210402144857677.png)

    > a =  constant phase delay

  * linear fase can only be implemented with FIR filters

  * Type-1: Symmetrical impulse response, M odd 

  * Type-2: Symmetrical impulse response, M even; cannot be used for highpass or bandstop filters

  * Type-3: Antisymmetric impulse response, M odd; cannot be used for lowpass filter or a highpass filter

  * Type-4: Antisymmetric impulse response, M even; 

  * 



## FIR filters

* Or *nonrecursive* or *moving average filters*

* finite-duration impulse response 

* jsut feedfoward

*  always stable

*  impulse response decay to zero in $t<\infin$

* the output sequence has exactly the same length as the input x(n) sequence.

* there are (M − 1) poles at the origin and (M − 1) zeros located somewhere in the z-plane.

* (+) can be designed to have a linear-phase response ( generally desirable)

* (-) Costuma precisar de uma ordem muito maior para chegar no mesmo resultado do IIR

![Choosing a Filter Response Type](Images - 1.0 - Signal Processing/df0436.gif)

* **Implementation**
  * sempre estável
  * matlab: all the same, with a=1
  * forma direta
    * ?
    * a resposta ao impulso (no tempo) é igual aos coeficientes do filtro
  * forma cascata
    * ?
  * forma de fase linear
    * ?
  * lattice
    * é a mais comum
    * (-) não consegue implementar filtros de fase linear
    * :keyboard: matlab (proakis) apply filter: `dir2latc(b)`
    * :keyboard: matlab (proakis) conversion from direct: `latcfilt(K,x)`
  * Frequency sampling form
    * dir2fs(b)
* **Design**
  * we will design primarily multiband lowpass, highpass, bandpass, and bandstop filters
  * 
  * um filtro perfeito é não causal e infinito, portanto não implementável
  * Hilbert transformers: ?; all-pass filter that imparts a 90º phase shift on the input signal.
  * Hilbert differentiator: ?
  * THe outcome of this process is a difference equation, or a system function H(z), or an impulse response h(n)
  * M=comprimento e número de coeficientes (e de multiplicações na forma direta I); M-1 = ordem e número de zeros do filtro
  * :keyboard: matlab: `h = fir1(N,wc)` projeta o filtro com ua combinação de técnicas; designs an N th-order (N = M − 1) lowpass FIR
  * :keyboard: matlab: `h = fir2(N,f,m)`,  to design arbitrary shaped magnitude response FIR filters
  * <u>window design technique</u>
  * choose a proper ideal frequency-selective filter
    * :keyboard: matlab: `ideal_lp(wc, M)`, for low pass
    * podemos descobrir o tamanho da janela M pela largura de transição desejada, `M = ceil(6.6*pi/tr_width) + 1`
    * and then to truncate (or *window*) its impulse response to obtain a linear-phase and causal FIR filter
    * :keyboard: matlab: `freqz_m(h, [1])`, already does a good analysis
    * gera sempre um filtro com fase linear?
  * <u>frequency sampling technique</u> 

## IIR filters

* infinite-duration impulse response

* Or *autoregressive moving average (ARMA) filter* or *recursive*

* have feedback (and feedfoward)

* possuem polos fora da origem e zeros

* podem ser instáveis (saturarem)

* fase não linear (mas podemos colocar um passa tudo junto, tal que compense a fase do nosso filtro)

  ![image-20210328115038631](Images - 1.0 - Signal Processing/image-20210328115038631.png)	

  ![Help Online - Origin Help - Algorithms (IIR Filters)](Images - 1.0 - Signal Processing/IIR_Filter_1.png)

  ![image-20210321210351559](Images - 1.0 - Signal Processing/image-20210321210351559.png)

* O tamanho do filtro não está relacionado à sua seletividade: alguns filtros muito seletivos são simples
  
* **Implementation structures**
  
  * pode ser instável
  * <u>Direct form</u>
    * from the difference equation; moving average part and the recursive part (or equivalently, the numerator and denominator parts)
    * form 1: ?
    * form 2: ? (+) use less memory; premissa: os blocos podem ser trocados de ordem (sistema linear); (-) possui problemas com implementação em ponto fixo, pois pode saturar e o sistema deixa de ser linear (portanto a premissa dessa implementação) não é mais válida
    * (-) muito sensível a qualquer variação nos parâmetros?
    * :keyboard: matlab (proakis) aplica filtro: `y = filter(b,a,x)`
  * <u>forma cascata</u>
    * produto de frações parciais de segunda ordem (eq de segunda ordem no denominador, eq de segunda ordem no numerador)
    * (-) menos sensiível a quantização do que a forma direta
    * only practical way to implement
    * cada termo é chamado de seção biquadrada
    * [This tutorial have good tips about in which sequence apply the biquad](http://www.iowahills.com/A4IIRBilinearTransform.html)
    * os ganhos devem ser distribuidos entre todos os termos
    * :keyboard: matlab (proakis) aplica filtro: `casfiltr(b,a)`
    * :keyboard: matlab (proakis) converision from direct: dir2cas(b,a)
  * <u>forma paralela</u>
    * somatorio de frações parciais de segunda ordem
    * :keyboard: matlab (proakis) aplica filtro: `parfiltr(b,a)`
    * :keyboard: matlab (proakis) conversion from direct: `dir2par(b,a)`
  * <u>Forma lattice</u>
    * (-) Requerem o dobro do número de multiplicações que a forma
      direta
    * (+) baixa sensibilidade à quantização dos parâmetros k.
    * :keyboard: matlab (proakis) apply filter: `ladrfilt(a,b)`
    * :keyboard: matlab (proakis) conversion from direct: `dir2ladr`
  
* **design**
  * começamos por um fltro analógico (o “protótipo”), sempre um passa baixa
  
  * geralmente primeiro se passa para a frequencia, e depois se muda o tipo de filtro
  
  * não conseguimos controar qual será a fase
  
  * amostramos a resposta em frequenia: $h_d[n] = h_a(nT)$; invariancia ao impulso??
  
  * N=ordem e número de polos; Número de multiplicações em seções de 2ª ordem = 2N+1 (mínimo)
  
  * :keyboard: matlab example (Butterworth Bandpass Filter):
  
    ```matlab
    [N,wn] = buttord(wp/pi,ws/pi,Rp,As);
    [b,a] = butter(N,wn);
    Ordem=length(roots(a))
    ```
  
  * <u>Relembrando estruturas analogicas</u>
    * Filtros elípticos possuem a melhor desempenho da resposta em magnitude, mas com a fase altamente não-linear.
    * Já os filtros Butterworth estão na outra ponta, com uma fase quase linear, mas exigindo um alto N para uma banda de transição estreita.
    * Então os filtros Chebyshev representam um bom “meio-termo” entre os butterworht and eliptico. 
    * IIR chebchev costuma precisar de uma ordem menor, para uma mesma resposta
    * IIR bessel é bem suave, tipo o butterworth, mas geralmente dá uma ordem maior
    * `[z,p,k]=buttap(N)` dá o filtro analógico na forma direta, e com `[C,B,A] = sdir2cas(b,a)` podemos converter para forma cascata (que nos é mais útil)
    
  * <u>transformação bilinear</u>
    * procedimento para compenar a distorção
    * aplica uma função arcotangente para mapear/wrap o espectro infinito (em frequencia) para uma faixa finita (-1 a 1); tranformação não linear
    * MATLAB já calcula pelo método bilinear
    
  * <u>fazendo as transformações</u>
  
* * * matlab: zmapping

## Notch

* used to eliminate an arbitrary frequency
* Rejeita faixa muito estreito
* Formalmente é um tipo de IIR, mas geralmente estudamos separadamente
* para a tarefa de eliminar um uma faixa estreita de frequência, o Notch precisa de um ordem muito menor do que um IIR tradicional precisaria
* (-) fase é pior 
*  
*  See the paper "Narrowband Notch Filter Using Feedback Structure" to an enhanced implementation of notch (digital);

## Adaptative filtering

* adjustable coefficients that can be optimized to minimize some measure
* the FIR filter is by far the most practical and widely used, mainly because it is stable
* the direct form and the lattice form are the ones often used
* **least-mean-square (LMS) algorithm**
  * to adaptively adjust the coefficients of an FIR filter.



# Envelope detection

* General problem:

  ![image-20241008110105171](./Images - 1.0 - Signal Processing/image-20241008110105171.png)

* **Square law envelop detector**

  * Take the square of the signal

  * because of the additive nature of [1 + cos(4𝜋𝑓𝑐𝑡)] the carrier and modulating signals are easily separable with a linear LPF

  * sqrt to scale back down

    ![image-20241008110118784](./Images - 1.0 - Signal Processing/image-20241008110118784.png)

* **Half wave envelop detector**

  * The negative values of the signal are set to zero (thresholding, half-wave rectification)

  * The high-frequency oscillation is filtered out by a low-pass filter.

  * 

    ![image-20241008110111775](./Images - 1.0 - Signal Processing/image-20241008110111775.png)

* **Energy based**

  * ?

    ![image-20241008110253419](./Images - 1.0 - Signal Processing/image-20241008110253419.png)

* **FIR hilbert transformer**

  * Same as *Complex square-law detector*?

  * About the Hilbert Transform itself, see in *mathematics*

  * 

    ![image-20241008110309802](./Images - 1.0 - Signal Processing/image-20241008110309802.png)

# Souce separation

* Given many signals that were mixed, separate what is what
* Example: cocktail party problem
* <u>Formalism as linear combination</u>
  * N components X were mixed to N channels of recordings Y using a mixing matrix $A_{N \times N}$
  * Y = AX
  * Assumes we have as many recordings as sources
  * Our goal is to find $\tilde A$ such that $\tilde X = \tilde A^{-1} Y$
* Methods
  * PCA
  * ICA
  * see both in ML unsup

# Métodos para sinais estocásticos

* **Sources of uncertainty in measurements**
  *  inherent uncertainty (Heisenberg)
  *  uncertainty from complexity: a too complex process which we do not want or can not explain
  * de facto uncertain process (the underlying process is stochastic. like a dice)

## Discrete state

* **Stochastic time series of discrete states**
  * measurement = signal + noise
  * generalization of deterministic time series
  * <u>Representing a time series as generated by a sequence of hidden states</u>
    * measurement = state + noise
    * state changes over time (not necessarily at the same speed as the measurements)
    * 
    * $E(f(X_0, X_1, ..., X_n) | Y_0, Y_1, ... , Y_k)$
    * Given $n$ measurements/observation and $k$ hidden states (or number of observations used in the calculation)
    * E = estimation process (or the process that generates the observations?)
    * X = observations
    * Y = hidden states
    * Prediction – estimating future data points based on observations; n>k
    * Filtering – Estimating the current datapoint based on the observations; k=n
    * Smoothing – Decreasing noise on the observations by decreasing the number of datapoints; n<k
    *  
    * (another notation)
    * $y(t) = \phi(x_t) + e_2(t)$
    * y = observations
    * e = additive noise
    * phi = observation function
    * x = hidden state
    * 
  * <u>Markov processes</u>
    * Great way of representing how the state transitions happen
    * models how the time series is generated
    * ==this formlism is already for hidden markov models, since we handle the observations and not the states directly, right?==
    * See more in *Stochastic processes*
  * <u>relative frequency matrix</u>
    * table that displays the frequency or count of occurrences of different hypotheses/states (rows) based on specific observations/evidences (columns)
    * summarises the relationship between different hypotheses and their corresponding evidence
    * Each cell in the matrix contains the count of how many times a given hypothesis Hi was observed when the corresponding evidence Ei occurred
    *  
    * Requires you to have performed the same number of measurements for each state (the sum of the values in each row in the same)
    * If you can not control the state, then before assembling the RelativeFrequencyMatrix you should normalize/correct such that the condition above is true
  * <u>decision matrix (D)</u>
    * Or *Bayes risk matrix*
    * given the observation (evidence), determine the probability of being in each hidden state (hypothesis)
    * P(H|E)
    * usually used to estimate denoise or forecast the data
    * all the rows sum up to one
    * can be obtained from the relative frequency matrix by dividing each column by the sum of the column, then transpose
    * :keyboard: python: `decision_matrix = (relative_frequency_matrix/np.sum(relative_frequency_matrix,0)).T` (uniform priors)
    * :keyboard: python: `decision_matrix = (relative_frequency_matrix*priors/np.sum(relative_frequency_matrix*priors,0)).T` (priors as column vector)
    * :keyboard: python: `decision_matrix = (observation_matrix/np.sum(observation_matrix,0)).T` (uniform priors)
    * :keyboard: python: `decision_matrix = (observation_matrix*priors/np.sum(observation_matrix*priors,0)).T` (priors as column vector)
    * P(Z|X)P(X) = observation matrix multiplied with our assumption about the hidden state = this would be the joint distribution matrix P(X,Z)
  * <u>observation matrix</u>
    * Or *likelihood matrix*
    * given hidden state (hypotheses), determine the probability of obtaining each observation (evidence)
    * can be obtained from the relative frequency matrix by dividing each row by the sum of the row
    * P(E|H)
    * usually used to generate/simulate the series
    * all the rows sum up to one
    * `observation_matrix = relative_frequency_matrix/np.sum(relative_frequency_matrix,1)`
    * joint distribution of the observation matrix and P: `observation_matrix*priors` (priors as column vector)
  * <u>observation probability</u>
    * vector
    * for each column (observation/evidence), sum the column and divide by overall sum (matrix-wide)]
    * for each observation, determine the probability of obtaining that observation
    * P(E)
* **Estimating the discrete state of a stochastic time series out of observations**
  * $P_{(t)} = someCrazyAlgorithm\left(\bold P, \bold O\right)$
  * $O_{(t)}$ = observations
  * $P_{(t)}$: smoothed/filtered outputs
  * most algorithms assume that the phenomena is markovian and that we know the state transition matrix S
  * we usually initialization P(0) as uniform, or maybe as the limit of the random process
  * <u>forward algorithm</u>
    * supposes a markovian process, and our knowledge of the state trnasition
    * you 
    * $P_{(t+1)} = P_{(t)} \cdot S *O_{(t)} \cdot D$ (==should th eindex of O also be t+1?==)
    * 
    * (-)very depandant on the initial conditions
  * <u>backward algorithm</u>
    * Same as forwards, but starting from the end and predicting all the way to the begining
    * Assumes the "prior" at the end of the process, instead of the beginign (as it was in the forwards)
    * you will usually assume that the final value is the stationary value of the process (which is very reasonable)
    * only works if you already have all the measurements before processing (not causal; therefore not markovian in an strict sense)
    * you need to convert the StateTransitionMatrix (that usually goes from $t-1$ to $t$) to something that goes from $t$ to $t-1$. ==Just normalize column-wise?==
    * (+) the end probability is easier to estimate (you can guess the steady state of the markov process)
    * Variation - foward-backward algorithm: generate a better prior with backward, then that prior is used to initialize the foward algorithm; multiply the probabilities of the backward and forwards series
  * <u>viterbi</u>
    * at each step, when calculating the probabilities of each one, takes only the highest probability
    * follows always the maximum likelihood trajectory
    * 
    * $P_{(t+1)} = \max (P_{(t)} \cdot S) * (O_{(t)} \cdot D)$
    * Phrased this way, the "max" operation is a little bit weird, and it's computed as (considering a 2d state $P=[P_1, P_2]$): $[max(P_1S_{(1,1)}, P_2S_{(2,1)}), max(P_1S_{(1,2)}, P_2S_{(2,2)})]$
    * $[max(P_{was1}S_{(was1,is1)}, P_{was2}S_{(was2,is1)}), max(P_{was1}S_{(was1,is2)}, P_{was2}S_{(was2,is2)})]$
    * at each timestep, P is a column matrix with the probabilty of ending up in each state
    * the fact that P holds probabilities is only relevant for the next timestamp; after that the least probably path isn't used anymore, thus it's irrelevant to remember the intermediate probabilityes
    * Instead of D you can actually use the ObservationMatrix, since the results will have the same ranking and probabilities will be normalized anymore
    * (+) computationaly eficient
    * (+-) leads to higher confidences (because it eliminates low probability paths)
    * (-) since the probability of most states will converge to zero, this may lead to numerical instability; to decrease that, most implemenatations store the log-probabilities
  * <u>baum algorithm</u>
    * calculates viterbi for all possible observatiin, then obtain the most probable hidden states?
    * observation sequence must be known
    * create a lookup table that goes from possible observations to the hidden states...?

## Continuous state

* **Stochastic time-series of continuous states**
  * See in *Stochastic Processes*
  * S: state
  * A = StMat = transition matrix
  * $S_{(t+1)}=A*State_{(t)}$
  * <u>Measurement matrix, B or C or OM</u>
    * equivalent to the discrete-state observation matrix
    * maps from the hidden states to the observation (==to the observation or to the measurment? the first refers to what is directly observed, 2 dimensions, and the second refers to the estimation of the state, 4 dimensions... I think...==)
    * O(t) = P(t) * OM
  * 
  * <u>Adding noise</u>
    * We'll add noise in both the state transition (like "random staggerings" in how the system evolve) and in our observation (if we are deadling with a hidden process) 
  
* **Estimating the continuous state of a stochastic time series out of observations**
  * <u>Alpha and beta filter</u>
    * or *tradeoff parameters*
    * Simplified alternative to kalman filter
    * just applying the state transition matrix, plus weighting how much do you trust the observation vs trust your state estimation
    * each dimension of the observation have a different value
    * "tradeoff" vector, with the number of states
    * for 4 dimensions models (like position and speed at 2d), we usually call those "alpha" and "beta"
    * specially useful for steady state processes...?
  * <u>kalman</u>
    * See in specific section


### Kalman Filter

* Or *linear quadratic estimation*

* Linear recursive filter to dynamic models

* predictive filter

* For markov processes (continos state) with lienar observation and linear state-transition (vanilla)

* minimizes L2 error

* can be used for prediction

* (-) only gaussian hipothesis

* proved optimal for markovian, linear transition, linear observation and ad

* **How it works**

  * $N_o$: estimationn of the oise in the observation. Also a matrix, with the dimension of the observations

  * $N_t$: estimation of the noise in the transition. Also a matrix, with the dimension of the hidden state

  * 

  * S = our estimation of the state; vector; sometimes denoted as $\eta_1$?

  * Observation(t): ?; sometimes denoted as $\eta_2$?

  * <u>gain G</u>

    * Or *K*, or *H*
    * how mcuh we trust the observation
    * updated automatically (if the measurements aren't very noisy, we trust the measurements more)
    * high gain = trust more recent measurements
    * $G=Cov*B* inv(B*Cov*B'+N_o)$
    * the term $Cov*B$ is often called "cross covariance"
    * the term $B*Cov*B'+N_o$ is often called "total variance"
  
  * <u>covariance matrix (Cov)</u>

    * noise across the state dimensions
    * describes the uncertainty
    * if state has n dimensions, then the Cov will be n x n matrix
    * if variables are independent, Cov is diagonal
    * it is updated over time, since our uncertainty changes (for example, when the object moves to the left, the uncertainty across the horizontal direction increases)
  
  * <u>Updates over time</u>
  
    * first update to Cov: $Cov=A*Cov*A'+N_t$
  
    * (this serves as a "increase of the uncertainty": the more time advances, the less certain we tend to be about the initial covarience)
  
    * (predicts how the uncertainty will be in the next step)
  
    * recalculate gain with this new Cov
  
    * 
  
    * estimation update: $S(t+1)=A*S(t)+G*(Observation(t)-B*A*S(t))$
  
    * (the A*S(t) on the right side corresponds to, based solely on the model, imagining where you probably are now; a priori prediction)
  
    * (the second term in the sum is like a weighted error: what we observe - what we expect)
  
    * 
  
    * second update to Cov: $Cov=(I-G*B)*Cov$
  
    * (this acts as a "decrease of the undertainty": the more the measurements are trustworty, the smaller will be G, and the smaller will become Cov)
  
    * (reconsider what the uncertainty is, given the measurement we)
  
    * We may also change A (the process itself is non-stationary)
  
    * matlab example:
  
      ```matlab
      % given
      A=[1 0 1 0; 0 1 0 1; 0 0 1 0; 0 0 0 1]  % classical kinematics
      COV = [4 0 0 0; 0 4 0 0; 0 0 2 0; 0 0 7 0]
      B=[1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]  %fully observable
      NOISE_OBSERVATION=[2 0 0 0; 0 2 0 0; 0 0 2 0; 0 0 0 2]
      
      S_0 = [50; 60; 10; -4] % initial state
      observation_0 = [21.5185; 44.6126; 0; 0] % first observation
      observation_1 = [30; 40; 1; -15] % second observation
      
      % first update
      COV = A*COV*A' + NOISE_OBSERVATION
      G = COV*B*(B*COV*B' + NOISE_OBSERVATION)^-1
      S_1 = A*S_0 + G*(observation_0 - B*A*S_0)
      COV = (eye(4) - G*B)*COV
      
      % second update
      COV = A*COV*A' + NOISE_OBSERVATION
      G = COV*B*(B*COV*B' + NOISE_OBSERVATION)^-1
      S_2 = A*S_1 + G*(observation_1 - B*A*S_1)
      ```
  
* **How it works (notation 2)**

  * X = hidden state

  * prediction $X_t$ at time $t$ is a linear function of the past measurement $X_{t-1}$

  * X is ==sampled== from a gaussian process, representing the uncertainty

  * +gaussian noise

  * The parameters of X are updated over time

    ![image-20200726235716595](Images - 1.0 - Signal Processing/image-20200726235716595.png)

  * $u_t^+$ (best guess) is the weighted average between the measurement and the prediction

* **Extended kalman filter**

  * for non linear prediction
  * (-) strongly dependant on how good the initial estimation is
  * need to know the derivate of the transition function (either analytically or some approximation)
  * approximate the state transition of the update by the linearized version of the state transition functions
  *  
  * every time we have a new measurement, we need to update the state estimate, but that requires linearization now so it becomes a matrix multiplication
  * $A_{xt}$: used instead of the A matrix; linearized version of the non-linear state transitions functions $f$ for an specific position
  * $B_{st}$: used instead of B; linearized version of the observation functions $g$
  * the linearization is just the derivate at position xt... how is this done matricially? jacobian?

* **unscented kalman filter (UKF)**

  * improves the EKF

  * could be considered like a simplification of particle filter, where our few particles are carefully chosen in the begining to be sigma points

  * based on the 'unscented transformation'

  * (-) stiil assumes a gaussian noise

  * (+) avoids the need to calculate the Jacobian (thus any linearization)

  * <u>unscented transformation</u>

    * ==move to Math?==

    * used to estimate a gaussian process after it is multiplied by a non-linear function

    * sigma points are a way to estimate the parameters of a distribution

    * get several sampling points (Sigma points) around the mean (which in the kalman context, will be the current state estimate) based on its covariance, used just for probing the non-linear function

    * in the kalman context, these sigma points are like "pseudo states"

    * fit the gaussian to the points transformed by the non-linear function, you just attribute different weights $w_i$ to each point $i$, such that the weighted sum of the points is equal to the mean of the distribution; sum of the weights should be one; obtaining these weights is actually a crazy stuff (==TODO==)

    * X = set of sigmas points; one in the center + 2 per dimension: 3 points for a 1d process, 5 for a 2d process, etc

    * the points are not always chosen to be the std in the statistical sense, but some empiral (given by parameter $\lambda$) so we can use more "spread" estimation for transformations that are more non-linear

    * Matlab example: obtaining the 5 sigma points used to track a 2d gaussian (the non linear transformation is not included)
  
      ```matlab
      MEAN = [5 5]
      COV = [0.8 0.3; 0.2 0.7]
      LAMBDA = 0.5
      
      % Calculate the "spread" in each of the two directions
      N = size(COV, 2)
      [V, D] = eig(COV);
      COV_sqrt = V * sqrt(D) * V^-1
      
      % Obtain the sigma points
      X0=MEAN
      X1=MEAN + sqrt(N+LAMBDA) * COV_sqrt(:, 1)'
      X2=MEAN - sqrt(N+LAMBDA) * COV_sqrt(:, 1)'
      X3=MEAN + sqrt(N+LAMBDA) * COV_sqrt(:, 2)'
      X4=MEAN - sqrt(N+LAMBDA) * COV_sqrt(:, 2)'
      ```

    * after the non linear equation is applied to those points, $X = f(X)$, we estimate the fitting a gaussian:

    * $\mu = \sum_i w_i X_i$

    * $COV = \sum_i (X_i - \mu) w_i(X_i - \mu)^T$

  * 

  * 

  * Start by choosing the initial positions of the points X (sigma points)

  * <u>Updates over time</u>

    * make new measurement
    * Update the state estimate using the sigma method to estimate the new state covariance
    * the non linear state transition equation is applied to those points, $X = f(X)$
    * instead of doing the state update with $A_{st}$ (linearization), we do based on the sigma points
    * the non-linear function used for the unscented transformation is the set of state-transfer functions
    * both the estimated covariance and the estimated state is passed through the uscented transformation
    * ==TODO: how exactly==


### Particle Filter

* ==Same as Bayes filter?==

* Similar to Kalman filter, but to arbitrary distributions

* state transitions and observations can be non-linear and have multiple modes

* special case of Sequential Monte Carlo methods

* Special case of Recursive Bayesian estimation

* Form of genetic algorithm with proportional selection

* (-) more much computational expensive than the kalman filter

* 

* sample random “particles” to represent the distributuins

* after sampling, add some noise (difusion) to compensate non-perfect representation

* the only step that can't be paralelized is the resampling step

* **How it works**

  * $\xi^i$: current state of particle i (often called $S^i$)
  * $\hat \xi^i$: ?
  * ==the system as a whole don't have a state as kalman's had, but just a set of particles?==
  * $\varphi$: State Transition Function (a non-linear version of A)
  * $w_i$: weight of particle $i$
  * $e_1$: noise; different from each particle, drawn from...? Aids with more exploration (instead of pure exploitation)
  * Observation is often called $y$ or $z$
  * $\eta(U_i)$: maps uniform random variables $U_i$ to particle indices j...?
  * $r(⋅)$: computes the likelihood of the observation given the particle's state.
  * $\psi$: observation function (non linear version of B)?
  * 
  * Start with a uniform belief
  * draw N particles to mimic the current state estimation 
  * <u>Updates over time</u>
    * Iteration: propagate the particle using the system's dynamics
    * run the state transition for each particle
    * $\xi_{t+1}^i = \varphi\left(\hat{\xi}_t^i, e_1^i(t+1)\right)$
    * 
    * Make a new observation (one observation per particle?)
    * Error calculation: $L_t^i = r\left(Observation(t) - \psi(\xi_t^i)\right)$
    * (difference between the real distribution and the distribution predicted by the particles)
    * 
    * the error is used to calculate the weights (just a gaussian?)
    * $w = \alpha p(z|x)$ ????
    * $w_k = (1/\sqrt{2*\pi*variance})*e^{-error^2/2*var}$
    * normalize the weights (divide each weight by the sum of all weights)
    * 
    * Resampling (aka Bootstrap filter): Resampling the possible states based on the given weights
    * Every possible explanation will be selected with a probability equal to the previously calculated weight
    * sample more those particles with higher weights
    * probability of selecting particule $i$ is its likelihood: $P\left(\eta(U_i) = j\right) = L_t^j$
    * the exact resampling algorithm varies, it's usually something like: repeat each particle by something proportional to its weight (you will end up with much more particles than N), then sample uniformly from that distribution. This general approach is called "deck of cards principle"
    * $\hat{\xi}_t^i := \xi_t^{\eta(U_i)}$
    * Add new particles to set: $S_t = S_t \cup \{<x^i_t, w^i_t >\}$...? Isn't this some variation that invovled merging?

* **How it works - Notation 2**

  ![image-20200809204441510](Images - 1.0 - Signal Processing/image-20200809204441510.png)

* Application: localization in robotics, when the map in completely know

* * 

# Localization and navegation

* Also much used to tracking, target representation, etc
* This is a lot related to robotics, but all the algorithmic side is registered here
* Mostly vision-based, but inertial and sensors are also common
* About the capture of images, see *Instrumentação eletrônica*
* About the capture and semantic of GPS, see in *Computer Networks*
* ==should this be here?==
* ==should this include kalman and particle filter?==
* :book: Vetterli, last chapter

# Sensor-specific considerations

* **Lidar**
  * By establishing a consistent integration time, the points collected sequentially can be grouped into distinct lidar time frames
  * usually collect about 200k points per second
  * <u>NRCS lidar</u>
    * (-) the scanning laser beams are unable to densely cover the entire FoV of the sensor within
    * a data collection window (the edges are often empty)
    * example: Livox Avia

























* 







































