clc
Wp = 0.2*pi; Ws = 0.3*pi; Rp = 7; As = 16;
Ripple = 10 ^ (-Rp/20); Attn = 10 ^ (-As/20);
[b,a] = afd_butt(Wp,Ws,Rp,As)
[C,B,A] = sdir2cas(b,a)
[db,mag,pha,w] = freqs_m(b,a,0.5*pi);
%[ha,x,t] = impulse(b,a);
