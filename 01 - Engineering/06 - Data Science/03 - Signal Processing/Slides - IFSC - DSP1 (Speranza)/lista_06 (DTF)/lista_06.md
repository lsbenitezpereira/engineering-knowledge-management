

## P5.1
Usaremos a implementação em Matlab:
```matlab
function [Xk] = dfs(xn,N)
    % Computes Discrete Fourier Series Coefficients
    % Xk = DFS coeff. array over 0 <= k <= N-1
    % xn = One period of periodic signal over 0 <= n <= N-1
    % N = Fundamental period of xn
    % 
    % Example:
    %    xn = [0,1,2,3]; N = 4; Xk = dfs(xn,N)
    n = [0:1:N-1]; % row vector for n
    k = [0:1:N-1]; % row vecor for k
    WN = exp(-j*2*pi/N);  % Wn factor
    nk = n'*k; % creates a N by N matrix of nk values
    WNnk = WN .^ nk; % DFS matrix
    Xk = xn * WNnk; % row vector for DFS coefficientss
    
    subplot(2,1,1); 
    stem(abs(Xk));
    grid;
    title('Magnitude Part');
    
    subplot(2,1,2);
    stem(angle(Xk));
    grid;
    title('Angle Part');
endfunction
```
### 1)
```matlab
>> xn = [4,1,-1,1]; N = 4; Xk = dfs(xn,N)
Xk =

   5.0000 + 0.0000i   5.0000 + 0.0000i   1.0000 - 0.0000i   5.0000 + 0.0000i
```
![image-20210313193221800](Images - lista_06/image-20210313193221800.png)

### 2)

```matlab
>> xn = [2, 0, 0, 0, −1, 0, 0, 0]; N = 8; Xk = dfs(xn,N)

Xk =

 Columns 1 through 6:

   1.0000 + 0.0000i   3.0000 + 0.0000i   1.0000 - 0.0000i   3.0000 + 0.0000i   1.0000 - 0.0000i   3.0000 + 0.0000i

 Columns 7 and 8:

   1.0000 - 0.0000i   3.0000 + 0.0000i
```


![image-20210313193234233](Images - lista_06/image-20210313193234233.png)
## P5.3


### 1)
```matlab
xn = [5*sin(0.1*pi*[0:19]),zeros(1,20)];
N = length(xn);
Xk = dfs(xn,N)
```
![image-20210313150631513](Images - lista_06/image-20210313150631513.png)

### 2)
```matlab
xn = [5*sin(0.1*pi*[0:19]),zeros(1,60)];
N = length(xn);
Xk = dfs(xn,N)
```
![image-20210313150919272](Images - lista_06/image-20210313150919272.png)

acho que o grafico acima foi feito com o numero errado de pontos!!

### 3)

O zero padding tem o efeito de “interpolar na frequência”, de forma a obtermos um gráfico com maior resolução

diferença apenas na densidade de pontos (densidade espectral), não na resolução!

 

## P5.10

Modificamos a função de `dft` para adicionar um zero padding (melhorando a resolução), plotar o eixo x na escala de multiplos de $\pi$ e interpolar a curva. 
Dessa forma, obtemos uma aproximação de dtft a partir de seus valores amostrados (dft).

De forma geral, adotamos o período $N$ grande de forma a aumentarmos a resolução espectral da dft.

```matlab
function [Xk] = dtft_from_dft(xn)
    N = length(xn);
    paddings = 20;

    xn = [xn,zeros(1,paddings*N)];
    N = (paddings+1)*N;

    n = [0:1:N-1]; % row vector for n
    k = [0:1:N-1]; % row vecor for k
    WN = exp(-j*2*pi/N);  % Wn factor
    nk = n'*k; % creates a N by N matrix of nk values
    WNnk = WN .^ nk; % DFS matrix
    Xk = xn * WNnk; % row vector for DFS coefficients

    subplot(2,1,1); 
    plot(n*2/N, abs(Xk));
    grid;
    title('Magnitude Part');
    xlabel('frequency in pi units'); 
    
    subplot(2,1,2);
    plot(n*2/N, angle(Xk));
    grid;
    title('Angle Part');
    xlabel('frequency in pi units'); 
endfunction
```

### 1)

`n=[0:50]; xn = 0.6.^abs(n); dtft_from_dft(xn)`

![image-20210313160406780](Images - lista_06/image-20210313160406780.png)

### 2)

`>> n=[0:20]; xn = n.*(0.6.^n); dtft_from_dft(xn)`

![image-20210313160505075](Images - lista_06/image-20210313160505075.png)

### 3)

`>> n=[0:50]; xn = cos(0.5*pi*n) + j*sin(0.5*pi*n); dtft_from_dft(xn)`

![image-20210313160548304](Images - lista_06/image-20210313160548304.png)

### 4)

`>> xn = [1,2,3,4,3,2,1]; dtft_from_dft(xn)`

![image-20210313160652913](Images - lista_06/image-20210313160652913.png)

## P5.33

## 1) 
```matlab
cconv([2,1,1,2], [1,-1, -1, 1], 4) % 0  -2   0   2
cconv([2,1,1,2], [1,-1, -1, 1], 7) % 2.0  -1.0  -2.0   2.0  -2.0  -1.0   2.0
cconv([2,1,1,2], [1,-1, -1, 1], 8) % 2.0  -1.0  -2.0   2.0  -2.0  -1.0   2.0   0.0
```
![image-20210313193154985](Images - lista_06/image-20210313193154985.png)

## 2)

![image-20210313193048637](Images - lista_06/image-20210313193048637.png)

$x_3(n) = x_2(n) * x_1(n)$
$x_3(n) = [+2,  -1,  -2,   +2,  -2,  -1,   +2]$
`conv([2,1,1,2], [1,-1, -1, 1])`

## 3)

7

## 4) 

A saída a convolução linear possui tamanho  $m+n-1$.

Se fizermos a convolução linear com saída de mesmo tamanho ($N=7$), obteremos o mesmo resultado.



## P5.38	

A função possui período de 50 amostras, portanto o teremos o menor *spectrum leakage* se amostramos com N múltiplo desse valor.

Conforme vemos:

![image-20210313173854637](Images - lista_06/image-20210313173854637.png)

```matlab
n=[0:(50)]
stem(2*sin(4*pi*0.01*n) + 5*cos(8*pi*0.01*n))
```

### 1) 

Para $N=50$, temos:

![image-20210313174010541](Images - lista_06/image-20210313174010541.png)

### 2)

 Para $N=99$ (o valor mais próximo de um múltiplo de 50), temos:

![image-20210313174101747](Images - lista_06/image-20210313174101747.png)