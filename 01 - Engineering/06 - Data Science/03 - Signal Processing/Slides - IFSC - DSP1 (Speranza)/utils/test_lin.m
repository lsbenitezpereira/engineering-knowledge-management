function r = test_lin(f)
  % Teste numerico se um sistema é linear
  % Teste realizado com uma matrix x com size(x)=1x100
  % Exemplos de uso:
  %   function [y, yn] = foo(x, n) y=2n; yn=n; endfunction
  %   test_lin(@foo)

  n=0:100;
  x1=rand(1,length(n));
  x2=sqrt(10)*randn(1,length(n));

  [y1, y1n] = f(x1, n);
  [y2, y2n] = f(x2, n);
  [y, yn] = f(x1+x2, n);

  diff = sum (abs(y - (y1 + y2)));

  if (diff < 1e-5)
      disp(' **** Sistema é linear ***')
      r=1
  else
      disp (' **** Sistema não é linear ***');
      r=0
  end
endfunction