function r = test_time_inv(f)
  % Teste numerico se um sistema é invariante no tempo
  % Teste realizado com uma matrix x com size(x)=1x100
  % Exemplos de uso:
  %   function [y, yn] = foo(x, n) y=2n; yn=n; endfunction
  %   test_time_inv(@foo)

  nx1=0:100;
  x1=sqrt(10)*randn(1,length(nx1));

  [y, ny] =f(x1, nx1);
  [y1,ny1]=sigshift(y,ny,1); %y[n-1]

  [x2,nx2]=sigshift(x1,nx1,1); %x[n-1]
  [y2,ny2]= f(x2, nx2)
 

  [diff,ndiff]=sigadd(y1,ny1,-y2,ny2);
  diff=sum(abs(diff));

  if (diff < 1e-5)
      disp(' **** Sistema eh Invariante no Tempo ***')
      r=1
  else
      disp (' **** Sistema nao eh Invariante no Tempo ***');
      r=0
end