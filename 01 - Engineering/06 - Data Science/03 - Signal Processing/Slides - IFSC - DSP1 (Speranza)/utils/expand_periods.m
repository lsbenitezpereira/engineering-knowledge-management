function y = expand_periods(x, n)
  % x = fundamental discrete sequence
  % n = numeber of periods to repeat
  y = ((x'*ones(1,n))(:))'
endfunction
