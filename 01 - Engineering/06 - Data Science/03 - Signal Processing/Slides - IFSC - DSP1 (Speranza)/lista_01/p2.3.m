0;
#1
#x = [-2,-1,0,1,2]
#expand_periods(x, 5)
#stem(x)

#2
[x1,n] = stepseq(0, 0, 19)
[x2,_] = stepseq(20, 0, 19)
x3 = exp(0.1*n)
x4 = x3.*(x1 - x2)

x1 = expand_periods(x1, 3)
x2 = expand_periods(x2, 3)
x3 = expand_periods(x3, 3)
x4 = expand_periods(x4, 3)


subplot(4, 1, 1)
stem(x1)

subplot(4, 1, 2)
stem(x2)

subplot(4, 1, 3)
stem(x3)

subplot(4, 1, 4)
stem(x4)


#3
[x1,n] = stepseq(0, 0, 9)
[x2,_] = stepseq(10, 0, 9)
x3 = sin(0.1*pi*n)
x4 = x3.*(x1 - x2)

x1 = expand_periods(x1, 4)
x2 = expand_periods(x2, 4)
x3 = expand_periods(x3, 4)
x4 = expand_periods(x4, 4)


subplot(4, 1, 1)
stem(x1)

subplot(4, 1, 2)
stem(x2)

subplot(4, 1, 3)
stem(x3)

subplot(4, 1, 4)
stem(x4)

#4
x1 = [1,2,3]
x1 = expand_periods(x1, 8)

x2 = [1,2,3,4]
x2 = expand_periods(x2, 6)

x3 = x1+x2

subplot(3, 1, 1)
stem(x1)

subplot(3, 1, 2)
stem(x2)

subplot(3, 1, 3)
stem(x3)





