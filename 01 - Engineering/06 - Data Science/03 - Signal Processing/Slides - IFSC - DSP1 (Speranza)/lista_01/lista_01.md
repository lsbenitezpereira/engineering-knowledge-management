# P2.3

```matlab
function y = expand_periods(x, n)
  % x = fundamental discrete sequence
  % n = number of periods to repeat
  y = ((x'*ones(1,n))(:))'
endfunction

function [x,n] = stepseq(n0,n1,n2)
% Generates x(n) = u(n-n0); n1 <= n <= n2
% ------------------------------------------
% [x,n] = stepseq(n0,n1,n2)
%
n = [n1:n2]; x = [(n-n0) >= 0];
```



## 1)

```matlab
x = [-2,-1,0,1,2]
x = expand_periods(x, 5)
stem(x)
```

![image-20201121165356947](Images - lista_01/image-20201121165356947.png)

## 2)

```matlab
[x1,n] = stepseq(0, 0, 19)
[x2,_] = stepseq(20, 0, 19)
x3 = exp(0.1*n)
x4 = x3.*(x1 - x2)

x1 = expand_periods(x1, 3)
x2 = expand_periods(x2, 3)
x3 = expand_periods(x3, 3)
x4 = expand_periods(x4, 3)


subplot(4, 1, 1)
stem(x1)

subplot(4, 1, 2)
stem(x2)

subplot(4, 1, 3)
stem(x3)

subplot(4, 1, 4)
stem(x4)
```

![image-20201121101937323](Images - lista_01/image-20201121101937323.png)

## 3) 

```matlab
[x1,n] = stepseq(0, 0, 9)
[x2,_] = stepseq(10, 0, 9)
x3 = sin(0.1*pi*n)
x4 = x3.*(x1 - x2)

x1 = expand_periods(x1, 4)
x2 = expand_periods(x2, 4)
x3 = expand_periods(x3, 4)
x4 = expand_periods(x4, 4)


subplot(4, 1, 1)
stem(x1)

subplot(4, 1, 2)
stem(x2)

subplot(4, 1, 3)
stem(x3)

subplot(4, 1, 4)
stem(x4)
```

![image-20201121102330630](Images - lista_01/image-20201121102330630.png)

## 4)

Dada duas sequencias periódicas $f_1$ e $f_2$ com período fundamental $T_1$ e $T_2$, o perído de $f_1 + f_2$ será o mínimo multiplo comum entre $T_1$ e $T_2$.

Para esse probema, o período será 12.

```
x1 = [1,2,3]
x1 = expand_periods(x1, 8)

x2 = [1,2,3,4]
x2 = expand_periods(x2, 6)

x3 = x1+x2

subplot(3, 1, 1)
stem(x1)

subplot(3, 1, 2)
stem(x2)

subplot(3, 1, 3)
stem(x3)
```



![image-20201121103148063](Images - lista_01/image-20201121103148063.png)

# P2.4

```matlab
x = [2, 4, -3, 1, -5, 4, 7]
n = [-3, -2, -1, 0, 1, 2, 3]
```

## 1)

![image-20201121161300761](Images - lista_01/image-20201121161300761.png)

```matlab
# Function definition

[x1a, n1a] =  sigshift(x, n, 3)
x1a = 2*x1a

[x1b, n1b] =  sigshift(x, n, -4)
x1b = 3*x1b

n1c = n
x1c = -x

[x1, n1] = sigadd(x1a, n1a, x1b, n1b)
[x1, n1] = sigadd(x1, n1, x1c, n1c)

# Plot

ax1 = subplot(4, 1, 1)
stem(n1a, x1a)

ax2 = subplot(4, 1, 2)
stem(n1b, x1b)

ax3 = subplot(4, 1, 3)
stem(n1c, x1c)

ax4 = subplot(4, 1, 4)
stem(n1, x1)

linkaxes([ax1,ax2,ax3,ax4],'x')

```



## 2)

![image-20201121161826607](Images - lista_01/image-20201121161826607.png)

```matlab
[x2a, n2a] =  sigshift(x, n, -4)
x1a = 4*x1a

[x2b, n2b] =  sigshift(x, n, -5)
x2b = 5*x2b

n2c = n
x2c = 2*x

[x2, n2] = sigadd(x2a, n2a, x2b, n2b)
[x2, n2] = sigadd(x2, n2, x2c, n2c)

# Plot
ax2 = subplot(4, 1, 1)
stem(n2a, x2a)

ax2 = subplot(4, 1, 2)
stem(n2b, x2b)

ax3 = subplot(4, 1, 3)
stem(n2c, x2c)

ax4 = subplot(4, 1, 4)
stem(n2, x2)

linkaxes([ax1,ax2,ax3,ax4],'x')
```

## 3)

![image-20201121163151509](Images - lista_01/image-20201121163151509.png)

```matlab
[x3a, n3a] =  sigshift(x, n, -3)

[x3b, n3b] =  sigshift(x, n, 2)

[x3c, n3c] =  sigshift(x, n, 1)
[x3c, n3c] = sigfold(x3c, n3c)

[x3d, n3d] =  sigshift(x, n, -1)

[x3a, n3a] = sigmult(x3a, n3a, x3b, n3b)
[x3c, n3c] = sigmult(x3c, n3c, x3d, n3d)
[x3, n3] = sigadd(x3a, n3a, x3c, n3c)

stem(n3, x3)
```

## 4)

![image-20201121164946651](Images - lista_01/image-20201121164946651.png)

```matlab
x4a = 2*exp(0.5*n);
[x4b, n4b] = sigmult(x4a, n, x, n);

x4c = cos(0.1*pi*n);
[x4d, n4d] = sigshift(x,n,-2);
[x4e, n4e] = sigmult(x4c, n, x4d, n4d);

x4f = zeros(1, 21);
n4f = [-10:10];

[x4, n4] = sigadd(x4b, n4b, x4e, n4e)
[x4, n4] = sigadd(x4, n4, x4f, n4f)

stem(n4, x4)
```



# P2.5

## 1)

Para que a função $\cos(w_0n)$ seja períódica, deve haver $N \in \Z$ tal que:
$$
\cos(w_0n) = \cos(w_0(n+N))
$$
Como $\cos(n)=\cos(z+2K\pi), k\in\Z$, a função será períódica se e somente se:
$$
w_0(n+N)+2K\pi=w_0n \\

w_0 = \frac{2K\pi}{N} \\

f_0 = \frac{K}{N}, \forall K,N\in\Z
$$

## 2)

O função possui período de $\frac{2 \pi}{0.1*\pi} = 20$, conforme vemos:

![image-20201121155427579](Images - lista_01/image-20201121155427579.png)

```matlab
n = [-100:100]
y = exp(j*0.1*pi*n)

subplot(2,1,1)
stem(n, real(y))
title('Real part')

subplot(2,1,2)
stem(n, imag(y))
title('Imaginary part')
```

K e N determinarão a frequência e o número de oscilações da senoide por período

## 3)

A sequência $cos(0.1*n)$ não é periódica pois não existe $K, N \in \Z$ tal que $\frac{0.1}{2\pi}=\frac{K}{N}$ , conforme vemos no gráfico:

![image-20201121155934680](Images - lista_01/image-20201121155934680.png)