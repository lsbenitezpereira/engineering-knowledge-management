function [Xk] = dfs(xn,N)
    % Computes Discrete Fourier Series Coefficients
    % Xk = DFS coeff. array over 0 <= k <= N-1
    % xn = One period of periodic signal over 0 <= n <= N-1
    % N = Fundamental period of xn
    % 
    % Example:
    %    xn = [0,1,2,3]; N = 4; Xk = dfs(xn,N)
    n = [0:1:N-1]; % row vector for n
    k = [0:1:N-1]; % row vecor for k
    WN = exp(-j*2*pi/N);  % Wn factor
    nk = n'*k; % creates a N by N matrix of nk values
    WNnk = WN .^ nk; % DFS matrix
    Xk = xn * WNnk; % row vector for DFS coefficientss
    
    subplot(2,1,1); 
    stem(abs(Xk));
    grid;
    title('Magnitude Part');
    
    subplot(2,1,2);
    stem(angle(Xk));
    grid;
    title('Angle Part');
endfunction