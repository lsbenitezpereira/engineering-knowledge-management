## P4.3
### 1)

![image-20210308000358136](Images - lista_05/image-20210308000358136.png)

```matlab
b = [0 -8 0 -1.5 0 -1/16];
a = [1 0 3/16 0 3/256 0 1/(256*16)];
[delta,n1] = impseq(0,0,9);
xb1 = filter(b,a,delta);

[u,n2] = stepseq(0,0,9);
xb2 = (((n2-3).*((1/4).^(n2-2))).*cos((pi/2)*(n2-1))).*u;
disp(max(abs(xb1-xb2)))
```

### 2)

![image-20210308000419679](Images - lista_05/image-20210308000419679.png)

```matlab
b = [3 (3*sin(0.3*pi)-2.25*cos(0.3*pi))]; 
a = [1 -1.5*cos(0.3*pi) 0.5625];
[delta,n1] = impseq(0,0,7); 
xb1 = filter(b,a,delta); 

[u,n2] = stepseq(0,0, 7);
xb2 = 3*(((0.75).^n2).*cos(0.3*pi*n2)).*u+4*(((0.75).^n2).*sin(0.3*pi*n2)).*u ;
error = max(abs(xb1-xb2));
error
```

### 3)

![image-20210308000446950](Images - lista_05/image-20210308000446950.png)

```matlab
b = [0 sin(pi/3) (0.81-0.9*sin(pi/3)) -(1.62+sin(pi/3)) +(0.9*sin(pi/3)+2.43) -1.62 0.81];
a = [1 -2.9 4.8 -4.7 2.8 -0.9]; 
[delta,n1] = impseq(0,0,9);
xb1 = filter(b,a,delta);

[u2,n2] = stepseq(0,0,9); [u3,n3] = stepseq(2,0,9);
xb2 = (n2.*sin(pi/3*n2)).*u2+((0.9).^n3).*u3; 
error = max(abs(xb1-xb2))

```

### 4)

![image-20210308000613454](Images - lista_05/image-20210308000613454.png)

### 5)

![image-20210308000629652](Images - lista_05/image-20210308000629652.png)

## P4.11

utilizou-se a expensão por frações particiais implementada no Octave

### 1)

```matlab
b1 = [1,-1,-4,4]; a1 = [1,-11/4,13/8,-1/4];
[R,p,k] = residuez(b1,a1)
```

![image-20210308000933505](Images - lista_05/image-20210308000933505.png)

### 2)

```matlab
b2 = [1,1,-4,4]; a2 = [1,-11/4,13/8,-1/4];
[R,p,k] = residuez(b2,a2)
```

![image-20210308000920318](Images - lista_05/image-20210308000920318.png)

### 3)

```matlab
b3 = [1,-3,4,1]; a3 = [1,-4,1,-0.16];
[R,p,k] = residuez(b3,a3)
```

![image-20210308000950011](Images - lista_05/image-20210308000950011.png)

### 4)

```matlab
b4 = [0,0,1]; a4 = [1,2,1.25,0.25];
[R,p,k] = residuez(b4,a4)
```

![image-20210308001024573](Images - lista_05/image-20210308001024573.png)

### 5)

```matlab
b5 = [0,0,0,1]; a5 = conv([1,0,-0.25],[1,0,-0.25]);
[R,p,k] = residuez(b5,a5)
```

![image-20210308001037648](Images - lista_05/image-20210308001037648.png)

## P4.15

Para todos os item, usaremos $X(z)=ZT\left\{\left(\frac{1}{4}\right)^2u(n)\right\} = \frac{1}{1-0.25 ^{-1}}$
### 1)

![image-20210308001118072](Images - lista_05/image-20210308001118072.png)

### 2)

![image-20210308001131936](Images - lista_05/image-20210308001131936.png)

### 3) 

Não consegui



## P4.18

Para todos os item, usaremos $X(z)=ZT\left\{  2*0.9^n u(n)  \right\} = \frac{2}{1- 0.9z^{-1}}$

### 1)

![image-20210308001150913](Images - lista_05/image-20210308001150913.png)

### 2)

![image-20210308001337466](Images - lista_05/image-20210308001337466.png)

### 3)

![image-20210308001404184](Images - lista_05/image-20210308001404184.png)

### 4) 

![image-20210308001422573](Images - lista_05/image-20210308001422573.png)