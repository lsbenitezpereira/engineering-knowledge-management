function deq = Quant_RSM(x,n)
% deq = Quant_RSM(x,n)
%   Quantization using Rounding in sign-magnitude format

xm = abs(x);
B = min(max(0,fix(log2(xm+eps)+1)));
deq = xm./(2.^B);
deq = round(deq.*(2.^n));
deq = sign(x).*deq.*(2.^(B-n));
