function [y] = QFixRS(x,B)
%  [y] = QFixRO(x,B)
%      B-bit Quantization -> Fix-point -> Rounding -> Saturation
%      y is the quantized array (same dim as x) with values in [-,1)
%      x can be a scalar, vector, or a matrix
%      B is the number of fractional bits

x = min(x,1-2^(-B)); x = max(-1,x);

xm = abs(x);
y = round(xm.*(2^B));    % Rounding to N bits
y = sign(x).*y*(2^(-B)); % B+1 bit representation