function [y] = QFixRO(x,B)
%  [y] = QFixRO(x,B)
%      B-bit Quantization -> Fix-point -> Rounding -> Overflow (2'C)
%      y is the quantized array (same dim as x) with values in [-,1)
%      x can be a scalar, vector, or a matrix
%      B is the number of fractional bits

if (x > -1) & (x < 1)
    x = x;
else
    x = rem(x,1)-sign(x);
end
xm = abs(x);
y = round(xm.*(2^B));    % Rounding to N bits
y = sign(x).*y*(2^(-B)); % B+1 bit representation