function deq = Quant_TSM(x,n)
% deq = Quant_TSM(x,n)
%   Quantization using Truncation in sign-magnitude format

xm = abs(x);
B = min(max(0,fix(log2(xm+eps)+1)));
deq = xm./(2.^B);
deq = fix(deq.*(2.^n));
deq = sign(x).*deq.*(2.^(B-n));