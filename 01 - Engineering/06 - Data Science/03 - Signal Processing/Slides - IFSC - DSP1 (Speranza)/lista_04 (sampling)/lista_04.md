# P3.19

Utilizaremos a função escrita para exercícios anteriores:

```matlab
function numerical_dtft(x, n)
    % example:
    %  n = [-10:10];
    %  x=0.6.^n;
    %  numerical_dtft(x,n)
    k = 0:500; w = (pi/500)*k;
    X = x * (exp(-j*pi/500)) .^ (n'*k);
    magX = abs(X); angX = angle(X);
    realX = real(X); imagX = imag(X);
    subplot(2,2,1); plot(k/500,magX);grid
    xlabel('frequency in pi units'); title('Magnitude Part')
    subplot(2,2,3); plot(k/500,angX/pi);grid
    xlabel('frequency in pi units'); title('Angle Part')
    subplot(2,2,2); plot(k/500,realX);grid
    xlabel('frequency in pi units'); title('Real Part')
    subplot(2,2,4); plot(k/500,imagX);grid
    xlabel('frequency in pi units'); title('Imaginary Part')
endfunction
```

## 1)

```matlab
Ts = 0.0001;
n = -10:1:10;
x = sin(1000*pi*n*Ts);
numerical_dtft(x,n)
```

![image-20210207185813956](Images - lista_04/image-20210207185813956.png)

## 2)

```matlab
Ts = 0.001;
n = -10:1:10;
x = sin(1000*pi*n*Ts);
numerical_dtft(x,n)
```

![image-20210207185851899](Images - lista_04/image-20210207185851899.png)

## 3)

```matlab
Ts = 0.00001;
n = -10:1:10;
x = sin(1000*pi*n*Ts);
numerical_dtft(x,n)
```

![image-20210207185917778](Images - lista_04/image-20210207185917778.png)



# P3.20

## 1)

O período fundamental é $N=8$, ou seja, o sinal se repete a cada 8 amostras.

```matlab
Ts = 1/8000;
n = 0:1:20; 
x = 10*sin(10000*pi*n*Ts);
stem(n,x)
```

![image-20210211225120997](Images - lista_04/image-20210211225120997.png)

## 2)

Para as questões 2 e 3, utilizaremos uma frequência de amostragem muito acima da frequência de nyquist, permitindo visualizarmos o formato da onda de saída.

```matlab
Ts = 1/200000;
n = 0:1:200; 
x = 10*sin(10000*pi*n*Ts);
%stem(n,x)

h = (-0.9).^n;
[y,ny]= conv_m(x,n,h,n);
%stem(ny, y)

%Zero order reconstruction
stairs(ny,y);
```

![image-20210211225439101](Images - lista_04/image-20210211225439101.png)

Vemos que a amplitude é de $5.34$

## 3)

De forma similar ao problema anterior, obtivemos que a amplitude de $y_a(t)$ será $2.6$

![image-20210211225939451](Images - lista_04/image-20210211225939451.png)

## 4)

Escolhendo as frequências 4k e 1k, obtemos:

```matlab
Ts = 1/8000;
n = 0:1:200; 

% Primeiro sinal
x = 10*sin(4000*pi*n*Ts);
h = (-0.9).^n;
[y,ny]= conv_m(x,n,h,n);
subplot(211)
stairs(y(100:150));

% Segundo sinal
subplot(212)
x = 10*sin(1000*pi*n*Ts);
[y,ny]= conv_m(x,n,h,n);
stairs(y(100:150));
```

![image-20210211235448459](Images - lista_04/image-20210211235448459.png)

## 5)

Filtro passa baixa, com frequência de corte igual à metade da frequência de amostragem.

Ou seja, $f_c=4KHz$

# P3.21

## 1)

```matlab
Ts=0.01;
n = 0:Ts:1;
x=cos(20*pi*n);
stem(n,x)
```
![image-20210207190732390](Images - lista_04/image-20210207190732390.png)

```matlab
Ts=0.05; %frequência de nyquist
n = 0:Ts:1;
x=cos(20*pi*n);
stem(n,x)
```
![image-20210207190821204](Images - lista_04/image-20210207190821204.png)

```matlab
Ts=0.1;
n = 0:Ts:1;
x=cos(20*pi*n);
stem(n,x)
```

![image-20210207190923628](Images - lista_04/image-20210207190923628.png)

## 2)

Para cada sinal, usou-se:

```matlab
Dt = 0.001;
t = 0:Dt:1;
xa = x * sinc((1/Ts)*(ones(length(n),1)*t-n'*ones(1,length(t))));
plot(t, xa)
```
Resultando, para o primeiro sinal ($T_s=0.01$):

![image-20210207191250576](Images - lista_04/image-20210207191250576.png)

Segundo sinal ($T_s=0.05$):

![image-20210207191315669](Images - lista_04/image-20210207191315669.png)


Terceiro sinal ($T_s=0.1$):

![image-20210207191340101](Images - lista_04/image-20210207191340101.png)

## 3)

Para cada sinal, usou-se:

```matlab
Dt = 0.001;
t = 0:Dt:1;
xa = spline(n,x,t);
plot(t, xa)
```
Resultando, para o primeiro sinal ($T_s=0.01$):

![image-20210207191606062](Images - lista_04/image-20210207191606062.png)


Segundo sinal ($T_s=0.05$):

![image-20210207191622169](Images - lista_04/image-20210207191622169.png)

Terceiro sinal ($T_s=0.1$):

![image-20210207191636886](Images - lista_04/image-20210207191636886.png)

## 4)

Quando amostrou-se à uma frequência duas vezes acima da frequência de Nyquist foi possível reconstruir a onda com bastante fidelidade.

Quando amostramos exatamente na frequência de Nyquist, o sinal reconstruido possui a frequência certa, mas possui mais distorções no seu formato. Se a onda amostrada fosse uma onda quadrada ou triangular, por exemplo, teríamos coletados as mesma amostras e a reconstrução seria menos satisfatória.

Quando amostrados na metade da frequência de Nyquist o sinal reconstruido já não possui nem frequência nem formato corretos, sendo o efeito ainda mais sobressalente na reconstrução com spline.

