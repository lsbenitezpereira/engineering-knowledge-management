# P3.3

Função utilizada para os plots:

```matlab
function plot_dftf(f)
  % Plot dos diagramas de F(w)
  % Teste realizado com uma matrix w com size(x)=1x501
  % Exemplos de uso:
  %   function [Y] = foo(w) Y = exp(j*w) ./ (exp(j*w) - 0.5); endfunction
  %   plot_dtft(@foo)
  
  w = [0:1:500]*pi/500; % [0, pi] axis divided into 501 points.
  X = f(w);
  magX = abs(X); angX = angle(X); realX = real(X); imagX = imag(X);
  magX = abs(X); angX = angle(X); realX = real(X); imagX = imag(X);
  subplot(2,2,1); plot(w/pi,magX); grid
  xlabel('frequency in pi units'); title('Magnitude Part'); ylabel('Magnitude')
  subplot(2,2,3); plot(w/pi,angX); grid
  xlabel('frequency in pi units'); title('Angle Part'); ylabel('Radians')
  subplot(2,2,2); plot(w/pi,realX); grid
  xlabel('frequency in pi units'); title('Real Part'); ylabel('Real')
  subplot(2,2,4); plot(w/pi,imagX); grid
  xlabel('frequency in pi units'); title('Imaginary Part'); ylabel('Imaginary')
endfunction
```



## 1)

![image-20201219192307758](Images - lista_03/image-20201219192307758.png)

```matlab
function [Y] = foo(w) Y=2./(1-0.5*exp(-j*w)) + 8./exp(-2*j*w) + 4./exp(-j*w) ; endfunction
plot_dtft(@foo)
```

![image-20201212114859002](Images - lista_03/image-20201212114859002.png)

## 2)

![image-20201219192322510](Images - lista_03/image-20201219192322510.png)

A equação acima pode ser escrita na forma vetorial, com as linhas representando os diferentes termos de $n$ e as colunas representando os diferentes valores de $\omega$. implementação em matlab:

```matlab
function [Y] = foo(w)  
  ns = [-10:10];
  Ys = 0.6.^(abs(ns)').*exp(-j*w.*ns');
  Y = sum(Ys);
endfunction
plot_dtft(@foo)
```

![image-20201212131904601](Images - lista_03/image-20201212131904601.png)

Conforme podemos observar, a parte imaginária (e portado a fase) são zero, pois as componentes complexas se anulam.

## 3)

![image-20201219192342105](Images - lista_03/image-20201219192342105.png)

```matlab
function [Y] = foo(w) 
	Y = (90*exp(j*w))./(10*exp(j*w)-9).^2;
	Y = Y - 1000*exp(3*j*w)./243;
	Y = Y - 200*exp(2*j*w)./81;
	Y = Y - 10*exp(1*j*w)./9;
	endfunction
plot_dtft(@foo)
```

![image-20201212140046039](Images - lista_03/image-20201212140046039.png)

## 4)

![image-20201219192413990](Images - lista_03/image-20201219192413990.png)

```matlab
function [Y] = foo(w) 
	Y = (16*exp(-j*w))./((5*exp(j*w)-4).^2)
	Y = Y + (4*exp(-j*w))./(exp(j*w)-0.8)
endfunction
plot_dtft(@foo)

```

![image-20201212144645005](Images - lista_03/image-20201212144645005.png)

## 5)

<img src="Images - lista_03/image-20201219192453691.png" alt="image-20201219192453691" style="zoom:80%;" />

```matlab
function [Y] = foo(w) 
	Y = 2./(1 + 0.7*exp(-j*(w-0.25*pi)));
	Y = Y + 2./(1 + 0.7*exp(-j*(w+0.25*pi)));
endfunction
plot_dtft(@foo)
```

![image-20201219192811515](Images - lista_03/image-20201219192811515.png)

# P3.11

```matlab
function numerical_dtft(x, n)
    % example:
    %   n = [-10:10];
    %   x=0.6.^n;
    %   numerical_dtft(x,n)
    k = 0:500; w = (pi/500)*k;
    X = x * (exp(-j*pi/500)) .^ (n'*k);
    magX = abs(X); angX = angle(X);
    realX = real(X); imagX = imag(X);
    subplot(2,2,1); plot(k/500,magX);grid
    xlabel('frequency in pi units'); title('Magnitude Part')
    subplot(2,2,3); plot(k/500,angX/pi);grid
    xlabel('frequency in pi units'); title('Angle Part')
    subplot(2,2,2); plot(k/500,realX);grid
    xlabel('frequency in pi units'); title('Real Part')
    subplot(2,2,4); plot(k/500,imagX);grid
    xlabel('frequency in pi units'); title('Imaginary Part')
end function
```
## 1)

![image-20201219192557814](Images - lista_03/image-20201219192557814.png)

Resolução numérica:
```matlab
n = [-1000:1000];
x = 0.9.^abs(n);
numerical_dtft(x,n)
```

![image-20201212173821055](Images - lista_03/image-20201212173821055.png)

## 2)

![image-20201219192626502](Images - lista_03/image-20201219192626502.png)

Resolução numérica:
```matlab
n = [-20:19];
x = sinc(0.2*n);
numerical_dtft(x,n)
```

![image-20201212173849256](Images - lista_03/image-20201212173849256.png)

## 3)

![image-20201219192637356](Images - lista_03/image-20201219192637356.png)

Resolução numérica:
```matlab
n = [0:39];
x = sinc(0.2*n);
numerical_dtft(x,n)
```

![image-20201212173913176](Images - lista_03/image-20201212173913176.png)

## 4)

![image-20201219192647707](Images - lista_03/image-20201219192647707.png)

Resolução numérica:
```matlab
n = [0:1000];
x = 0.5.^n + 0.4.^n;
numerical_dtft(x,n)
```

![image-20201212173931827](Images - lista_03/image-20201212173931827.png)

## 5)

<img src="Images - lista_03/image-20201219192526676.png" alt="image-20201219192526676" style="zoom:80%;" />

Resolução numérica:
```matlab
n = [-1000:1000];
x = 0.5.^abs(n).*cos(0.1*pi*n);
numerical_dtft(x,n)
```

![image-20201212173958509](Images - lista_03/image-20201212173958509.png)

# P3.13

Para cada função $H_i(\omega)$ do item $i$ da questão 3.11, será plotada a função:
$$
y_i(n) = 3*\left|H_i(0.5\pi)\right|*\cos(0.5\pi*n+\pi/3+\angle H_i(0.5\pi))\\
	   + 2*\left|H_i(0.3\pi)\right|*\cos(0.3\pi*n+\angle H_i(0.3\pi))
$$

Os ângulos e fases podem ser obtidos avaliando analiticamente a função de transferência encontrada ou utilizando a DTFT numérica, para a qual escrevemos a função:

```matlab
function plot_3_13(x, n)
    X1 = x * (exp(-j*pi/500)) .^ (n'*250);
    magX1 = abs(X1)
    angX1 = angle(X1);

    X2 = x * (exp(-j*pi/500)) .^ (n'*150);
    magX2 = abs(X2)
    angX2 = angle(X2);

	n = [-15:0.1:15];
	y = 3*magX1*cos(0.5*pi*n + pi/3 + angX1);
	y = y + 2*magX2*cos(0.3*pi*n + angX2);
	
	stem(n, y);
	title('Output sequence');
	xlabel('1/10 fraction of n');
	grid;
endfunction
```

## 1)

```matlab
n = [-1000:1000];
x = 0.9.^abs(n);
plot_3_13(x,n)
```

![image-20201212181315284](Images - lista_03/image-20201212181315284.png)

## 2)

```matlab
n = [-20:19];
x = sinc(0.2*n);
plot_3_13(x,n)
```

![image-20201212181331779](Images - lista_03/image-20201212181331779.png)

## 3)

```matlab
n = [0:39];
x = sinc(0.2*n);
plot_3_13(x,n)
```

![image-20201212181415838](Images - lista_03/image-20201212181415838.png)

## 4)

```matlab
n = [0:1000];
x = 0.5.^n + 0.4.^n;
plot_3_13(x,n)
```

![image-20201212181436632](Images - lista_03/image-20201212181436632.png)

## 5)

```matlab
n = [-1000:1000];
x = 0.5.^abs(n).*cos(0.1*pi*n);
plot_3_13(x,n)
```

![image-20201212181453699](Images - lista_03/image-20201212181453699.png)

# P3.18

<img src="Images - lista_03/image-20201219215437831.png" alt="image-20201219215437831" style="zoom:50%;" />

Se quisermos plotar a resposta em frequência do filtro, podemos fazer-lo com:

```matlab
b = [1,0,1,0,1,0,1];    					% filter coefficient array b
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	% filter coefficient array a
m = 0:length(b)-1; l = 0:length(a)-1;    	% index arrays m and l
K = 500; k = 0:1:K;    						% index array k for frequencies
w = pi*k/K;    								% [0, pi] axis divided into 501 points.
num = b * exp(-j*m'*w);    					% Numerator calculations
den = a * exp(-j*l'*w);    					% Denominator calculations
H = num ./ den;    							% Frequency response
magH = abs(H); angH = angle(H);    			% mag and phase responses

subplot(2,1,1); plot(w/pi,magH); grid; axis([0,1,0,2])
xlabel('frequency in pi units'); ylabel('|H|');
title('Magnitude Response');
subplot(2,1,2); plot(w/pi,angH/pi); grid
xlabel('frequency in pi units'); ylabel('Phase in pi Radians');
title('Phase Response');
```



## 1)

```matlab
b = [1,0,1,0,1,0,1];    					
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	

n=[0:200];
x= 5 + 10*cos(pi.*n);
subplot(2,1,1);
y = filter(b,a,x);
stem(n,y);
title('Complete response');
ylim ([-10 25]);
grid;

%%%%%%%%%
m = 0:length(b)-1; l = 0:length(a)-1;
w=1*pi;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y = 10*magH*cos(1*pi*n + angH);


m = 0:length(b)-1; l = 0:length(a)-1;
w=0;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y = y+ 5*magH


subplot(2,1,2);
stem(n, y);
title('Steady state response');
ylim ([-10 25]);
grid;
```

![image-20201219220106904](Images - lista_03/image-20201219220106904.png)

## 2)

```matlab
b = [1,0,1,0,1,0,1];    					
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	

n=[0:200];
x=1 + cos(0.5*pi*n + pi/2);
subplot(2,1,1);
y = filter(b,a,x);
stem(n,y);
title('Complete response');
ylim ([-1 2]);
grid;

%%%%%%%%%
m = 0:length(b)-1; l = 0:length(a)-1;
w=0.5*pi;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y1 = 1*magH*cos(0.5*pi*n + pi/2 + angH);

m = 0:length(b)-1; l = 0:length(a)-1;
w=0;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y2 = 1*magH

y=y1+y2
subplot(2,1,2);
stem(n, y);
title('Steady state response');
ylim ([-1 2]);
grid;
```
![image-20201219213359918](Images - lista_03/image-20201219213359918.png)

## 3)

```matlab
b = [1,0,1,0,1,0,1];    					
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	

n=[0:200];
x=2*sin(n*pi/4) + 3*cos(n*3*pi/4);
subplot(2,1,1);
y = filter(b,a,x);
stem(n,y);
title('Complete response');
ylim ([-3 3]);
grid;

%%%%%%%%%
m = 0:length(b)-1; l = 0:length(a)-1;
w=pi/4;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y1 = 2*magH*sin(pi/4*n + angH);

m = 0:length(b)-1; l = 0:length(a)-1;
w=3*pi/4;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response
magH = abs(H)
angH = angle(H)
y2 = 3*magH*cos(3*pi/4*n + angH);

y=y1+y2
subplot(2,1,2);
stem(n, y);
title('Steady state response');
ylim ([-3 3]);
grid;
```

![image-20201219213919593](Images - lista_03/image-20201219213919593.png)

## 4)

```matlab
b = [1,0,1,0,1,0,1];    					
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	

n=[0:200];
x = 0;
for k=[0:5]
	x = x + (k + 1)*cos(n*pi*k/4)
end
subplot(2,1,1);
y = filter(b,a,x);
stem(n,y);
title('Complete response');
ylim ([-20 20]);
grid;

%%%%%%%%%
y = 0
for k=[0:5]
    m = 0:length(b)-1; l = 0:length(a)-1;
    w=pi*k/4;
    num = b * exp(-j*m'*w);
    den = a * exp(-j*l'*w);
    H = num ./ den;    	% Frequency response
    magH = abs(H)
    angH = angle(H)
    y = y + (k + 1)*magH*cos(pi*k/4*n + angH);
end
subplot(2,1,2);
stem(n, y);
title('Steady state response');
ylim ([-20 20]);
grid;
```

![image-20201219214921050](Images - lista_03/image-20201219214921050.png)

## 5)

```matlab
b = [1,0,1,0,1,0,1];    					
a = [1,0,0.81^1,0,0.81^2,0,0.81^3];		  	

n=[0:200];
x=cos(pi.*n);
subplot(2,1,1);
y = filter(b,a,x);
stem(n,y);
title('Complete response');
ylim ([-2 2]);
grid;

%%%%%%%%%
m = 0:length(b)-1; l = 0:length(a)-1;
w=1*pi;
num = b * exp(-j*m'*w);
den = a * exp(-j*l'*w);
H = num ./ den;    	% Frequency response

magH = abs(H)
angH = angle(H)

subplot(2,1,2);
y = 1*magH*cos(1*pi*n + angH);
stem(n, y);
title('Steady state response');
ylim ([-2 2]);
grid;
```

![image-20201219213433657](Images - lista_03/image-20201219213433657.png)