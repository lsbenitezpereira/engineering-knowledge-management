function x = TensC2Sm(y,n)
%
% y = TensC2Sm(y,n)
% --------------
% Decimal equivalent of 
%  n-digit 10's-Complement format integer to Sign-Magnitude format conversion
%
%    y: integer between       0 <= y <= 10^n-1  (10's-complement)
%    x: integer between -10^n/2 <= x <  10^n/2  (sign-magnitude)

% if any((y < 0) | (y > 10^n-1)) 
%     error('Numbers must satisfy 0 <= y <= 10^n-1')
% end
% 
x = (10^n)/2-y;
sb = (sign(x) <= 0); % sign-bit (0 if x>0, 1 if x<=0));
x = (1-sb).*y + sb.*(y-10^n);