function x = TwosC2Sm(y,b)
%
% x = TwosC2Sm(y,b)
% ------------
% Decimal equivalent of 
%  b-bit Two's-Complement format integer to Sign-Magnitude format conversion
%
%    y: integer between        0 <= y <= 2^b-1   (2's-complement)
%    x: integer between -2^(b-1) <= x <  2^(b-1) (sign-magnitude)

% if any((y < 0) | (y > 10^b-1)) 
%     error('Numbers must satisfy 0 <= y <= 10^b-1')
% end
% 
x = 2^(b-1)-y;
sb = (sign(x) <= 0); % sign-bit (0 if x>0, 1 if x<=0));
x = (1-sb).*y + sb.*(y-2^b);