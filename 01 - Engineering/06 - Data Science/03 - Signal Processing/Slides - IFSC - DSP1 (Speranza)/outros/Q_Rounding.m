function xq = Q_Rounding(x,B)
%
% xq = Q_Rounding(x,B)
% ----------------------
%   Binary equivalent xq of rounding of x to B fraction bits
%
xq = round(x*2^B)/2^B;