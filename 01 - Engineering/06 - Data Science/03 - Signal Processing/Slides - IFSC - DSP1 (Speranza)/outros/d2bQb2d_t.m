function deq = d2dt(x,n)


B = min(max(0,fix(log2(x+eps)+1)));
deq = x./(2.^B);
deq = fix(deq.*(2.^n));
deq = deq.*(2.^(B-n));
