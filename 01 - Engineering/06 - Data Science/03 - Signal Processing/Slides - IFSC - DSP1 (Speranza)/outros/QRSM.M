function [y,L,B] = QRSM(x,N)
%  [y,L,B] = QRSM(x,N)
%      Quantization using N-bit Rounding in sign-magnitude format
%      x can be a scalar, vector, or a matrix
%      y is the quantized array (same dim as x)
%      L is the number of integer bits
%      B is the number of fractional bits

xm = abs(x);
 L = max(max(0,fix(log2(xm(:)+eps)+1))); % Integer bits
if (L > N)
     errmsg = [' *** N must be at least ',num2str(L),' ***'];
     error(errmsg);
end 
B = N-L;                            % Fractional bits
y = xm./(2^L); y = round(y.*(2^N)); % Rounding to N bits
y = sign(x).*y*(2^(-B));            % L+B+1 bit representation