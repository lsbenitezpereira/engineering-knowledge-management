clear;
fs = 8000;
ws = 0.4586*pi%2*pi*0/fs;
wp = 0.6*pi%2*pi*1000/fs;
Rp = 1; % Passband ripple in dB
As = 15%45; % Stopband attenuation in dB
[N,wn] = cheb1ord(wp/pi,ws/pi,Rp,As);
[b,a] = cheby1(N,Rp,wn,'high');
[b0,B,A] = dir2cas(b,a)


%%% continuação...
subplot(2,1,1)
zplane(b,a)

subplot(2,1,2)
N=8
[bahat,L1,B1] = qcoeff([b;a],N);
zplane(bahat(1, :), bahat(2, :))