# P2.11

```matlab
function r = test_lin(f)
  %Teste numerico se um sistema é linear
  %Teste realizado com uma matrix x com size(x)=1x100
  %Exemplos de uso:
  %  function y = foo(x) y=round(x); endfunction
  %  function y = foo(x) u=stepseq(0,0,100); y=x.*u; endfunction
  %  test_lin(@foo)

  n=0:100;
  x1=rand(1,length(n));
  x2=sqrt(10)*randn(1,length(n));

  [y1, y1n] = f(x1, n);
  [y2, y2n] = f(x2, n);
  [y, yn] = f(x1+x2, n);

  diff = sum (abs(y - (y1 + y2)));

  if (diff < 1e-5)
      disp(' **** Sistema é linear ***')
      r=1
  else
      disp (' **** Sistema não é linear ***');
      r=0
  end
endfunction
```

## A)

```matlab
function [y, yn] = foo(x, n)
  % x(n)u(n)
  u = stepseq(0,0,100);
  y = x.*u;
  yn = n;
end
```

![image-20201127115218408](Images - lista_02/image-20201127115218408.png)

## B)

```matlab
function [y, yn] = foo(x, n)
  % x(n) + n x(n + 1)
  y = x + n.*sigshift(x,n,-1);
  yn = n
end
```

![image-20201127115227305](Images - lista_02/image-20201127115227305.png)

## C)

```matlab
function [y, yn] = foo(x, n)
  % x(n) + 1/2*x(n − 2) −1/3* x(n − 3)x(2n)
  [y1, y1n] = dnsample(x,n,2);
  [y2, y2n] = sigshift(x,n,3);
  y2 = -1/3*y2;
  [y3, y3n] = sigshift(x,n,2);
  y3 = 1/2*y3;
  
  [y, yn] = sigmult(y1, y1n, y2, y2n);
  [y, yn] = sigadd(y, yn, y3, y3n);
  [y, yn] = sigadd(y, yn, x, n);
end
```

![image-20201127115248902](Images - lista_02/image-20201127115248902.png)

## D)

```matlab
function [y, yn] = foo(x, n)
  %sum_{k=-inf}^{n+5} 2x(k)
    max = size(x)(2)
  temp = zeros(1,max)

  for i=1:max-5
    temp(i) = 2*sum(x(1:i+5))
  endfor
  s = 2*sum(x)
  for i=max-5:max
    temp(i) = s
  endfor
  y = temp
  yn = n
end
```

![image-20201127115300718](Images - lista_02/image-20201127115300718.png)

## E)

```matlab
function [y, yn] = foo(x, n)
  % x(2n)
  [y, yn] = dnsample(x,n,2)
end
```

![image-20201127115312746](Images - lista_02/image-20201127115312746.png)

## F)

```matlab
function [y, yn] = foo(x, n)
  y = round(x)
  yn = n
end
test_time_inv(@foo)
```

![image-20201127115322369](Images - lista_02/image-20201127115322369.png)

# P2.12

Utilizou-se as funções acima para testar cada item da questão, usando a função de testes:

```matlab
function r = test_time_inv(f)
  % Teste numerico se um sistema é invariante no tempo
  % Teste realizado com uma matrix x com size(x)=1x100
  % Exemplos de uso:
  %   function [y, yn] = foo(x, n) y=2n; yn=n; endfunction
  %   test_time_inv(@foo)

  nx1=0:100;
  x1=sqrt(10)*randn(1,length(nx1));

  [y, ny] =f(x1, nx1);
  [y1,ny1]=sigshift(y,ny,1); %y[n-1]

  [x2,nx2]=sigshift(x1,nx1,1); %x[n-1]
  [y2,ny2]= f(x2, nx2)
 

  [diff,ndiff]=sigadd(y1,ny1,-y2,ny2);
  diff=sum(abs(diff));

  if (diff < 1e-5)
      disp(' **** Sistema eh Invariante no Tempo ***')
      r=1
  else
      disp (' **** Sistema nao eh Invariante no Tempo ***');
      r=0
end
```

 ![image-20201127115343836](Images - lista_02/image-20201127115343836.png)

# P2.15

## A)

```matlab
x = [2 -4 5 3 -1 -2 6]
nx = [-3 -2 -1 0 1 2 3]
h = [1 -1 1 -1 1]
nh = [-1 0 1 2 3]
[y,ny] = conv_m(x,nx,h,nh)
stem(ny, y)
```

![image-20201127160831422](Images - lista_02/image-20201127160831422.png)

![image-20201127165325872](Images - lista_02/image-20201127165325872.png)

## B)

```matlab
1;
x =[1 1 0 1 1]
nx = [-2 -1 0 1 2]
h = [1 -2 -3 4]
nh = [-3 -2 -1 0]

[y,ny] = conv_m(x,nx,h,nh)
stem(ny, y)
```

![image-20201127161734823](Images - lista_02/image-20201127161734823.png)

![image-20201127165338992](Images - lista_02/image-20201127165338992.png)

## C)

```matlab
[x1, nx1] = stepseq(-1, -10, 10)
[x2, nx2] = stepseq(4, -10, 10)
x = (1/4).^-nx1.*(x1-x2)

[h1, nh1] = stepseq(0, -10, 10)
[h2, nh2] = stepseq(5, -10, 10)
h = h1 - h2

[y,ny] = conv_m(x,nx1,h,nh1)
stem(ny, y)
```



![image-20201127165121386](Images - lista_02/image-20201127165121386.png)

<img src="Images - lista_02/image-20201127165408324.png" alt="image-20201127165408324" style="zoom:37%;" />

## D)

```matlab
1;
[x1, nx1] = stepseq(0, -10, 10)
[x2, nx2] = stepseq(6, -10, 10)
x = (1/4).*nx1.*(x1-x2)

[h1, nh1] = stepseq(-2, -10, 10)
[h2, nh2] = stepseq(3, -10, 10)
h = 2*(h1 - h2)

[y,ny] = conv_m(x,nx1,h,nh1)
stem(ny, y)
```

![image-20201128102242849](Images - lista_02/image-20201128102242849.png)

![image-20201128145749386](Images - lista_02/image-20201128145749386.png)

# P2.19

O sistema
$$
y(n) − 0.5y(n − 1) + 0.25y(n − 2) = x(n) + 2x(n − 1) + x(n − 3)
$$
pode ser representado como:
$$
\sum_{k=0}^2 a_k y(n-m) = \sum_{m=0}^3 b_m x(n-m) \\
\text{sendo que:} \\
a_k = [1, -0.5, 0.25]\\
b_m = [1, 2, 0, 1]
$$

## A)

```matlab
a = [1, -0.5, 0.25]
b = [1, 2, 0, 1]
n = 100
h = impz(b,a,n);
stem(h)
title('Impulse Response'); xlabel('n'); ylabel('h(n)')
```

![image-20201128123652117](Images - lista_02/image-20201128123652117.png)

## B)

Determine the stability of the system from this impulse response

A resposta é limitada e, portanto, BIBO estável

Simulando por um período maior e verificando o resultado: 

```matlab
n = 10000
h = impz(b,a,n);
sum(h)
%ans = 1.1111
```

## C)

```matlab
a = [1, -0.5, 0.25]
b = [1, 2, 0, 1]
n = [0:200]
x = 5 + 3*cos(0.2*pi.*n) + 4*sin(0.6*pi.*n)
y = filter(b, a, x)
subplot(2,1,1)
stem(n,x)
title('Input function'); xlabel('n'); ylabel('x(n)')
subplot(2,1,2)
stem(n,y)
title('System response'); xlabel('n'); ylabel('y(n)')	
```

![image-20201128145604704](Images - lista_02/image-20201128145604704.png)

