import numpy as np
import matplotlib.pyplot as plt
from IPython.display import (Audio, display, clear_output)
from typing import List, Tuple

from scipy.signal import lfilter


################
## Audio generation

def tonegen(freq:float, duration:float, amplitude:float=1, fs:float=8000, play:bool=True) -> np.array:
    '''
    gerar um tom senoidal.

    Parâmetros de entrada: frequência (Hz), duração (s), amplitude (0 a 1) e frequência de amostragem (Hz).
    '''
    t = np.linspace(0., duration, int(fs * duration))
    x = np.sin(freq * 2. * np.pi * t)
    if play:
        plt.stem(x)
        display(Audio(x, rate=fs, autoplay=True)) # para mostrar o widget
    return x*amplitude


def dtmf_gen(key:str, duration:float, amplitude:float, fs:float, play:bool) -> np.array:
    key2freq:List[str, Tuple(int, int)] = {
        '1': (697, 1209),
        '2': (697, 1336),
        '3': (697, 1477),
        'A': (697, 1633),
        '4': (770, 1209),
        '5': (770, 1336),
        '6': (770, 1477),
        'B': (770, 1633),
        '7': (852, 1209),
        '8': (852, 1336),
        '9': (852, 1477),
        'C': (852, 1633),
        '0': (941, 1336),
        '*': (941, 1209),
        '#': (941, 1477),
        'D': (941, 1633),
    }
    freq0, freq1 = key2freq[key]
    tone0 = tonegen(freq0, duration, amplitude, fs, play=False)
    tone1 = tonegen(freq1, duration, amplitude, fs, play=False)
    return tone0+tone1

def dtmf_gen_seq(
    seq:list, duration:float, interval:float, amplitude:float=1, fs:float=8000, play:bool=True
) -> np.array:
    '''
    recebe um vetor com uma sequência de dígitos e gera tons DTMF correspondentes
    '''
    out = np.array([])
    tone = dtmf_gen(seq[0], duration, amplitude, fs, play=False)
    out = np.concatenate((out, tone))
    for k in seq[1:]:
        inter = np.zeros(int(fs * interval))
        tone = dtmf_gen(k, duration, amplitude, fs, play=False)
        out = np.concatenate((out, inter, tone))
        
    if play:
        display(Audio(out, rate=fs, autoplay=True))
    return out


#################
## Audio processing
def halfsp(signal:np.array) -> np.array:
    '''
    remover uma em cada duas amostras de um sinal, criando um novo sinal que tem somente as amostras de índice ímpar do sinal original.
    '''
    return signal[::2]

def doublesp(signal:np.array) -> np.array:
    '''
     criar um novo sinal que insere uma nova amostra entre cada duas amostras. Essa nova amostra é a média das duas amostras vizinhas.
    '''
    out = np.zeros(signal.shape[0]*2-1)
    last_sample = signal[0]
    out[0] = last_sample
    for idx, current_sample in enumerate(signal[1:]):
        idx = idx+1 #correction because of enumerate starts in 0
        out[idx*2] = current_sample
        out[idx*2-1] = (current_sample + last_sample)/2
        last_sample = current_sample
    return out


    '''
    gera um modelo discreto de um sinal de eco $s_e(t)=\alpha s(t-T)$, onde $\alpha$ é o fator de atenuação, $T$, o atraso (em segundos) e $s(t)$, o sinal original. Assuma que $0 \leq \alpha \leq 1$ e que $T \geq 0$. O sinal resultante, com eco, é $r(t)=s(t)+s_e(t)$.
    '''
    delay_samples = int(fs*delay)
    se = alpha*np.concatenate((np.array([0]*delay_samples), signal))
    r = signal + se[:-delay_samples]
    if play:
        display(Audio(r, rate=fs, autoplay=True)) 

    return r

def echo(signal:np.array, fs:float, alpha:float, delay:float, play:bool=True) -> np.array:
    '''
    gera um modelo discreto de um sinal de eco $s_e(t)=\alpha s(t-T)$, onde $\alpha$ é o fator de atenuação, $T$, o atraso (em segundos) e $s(t)$, o sinal original. Assuma que $0 \leq \alpha \leq 1$ e que $T \geq 0$. O sinal resultante, com eco, é $r(t)=s(t)+s_e(t)$.
    '''
    delay_samples = int(fs*delay)
    se = alpha*np.concatenate((np.array([0]*delay_samples), signal))
    r = signal + se[:-delay_samples]
    if play:
        display(Audio(r, rate=fs, autoplay=True)) 

    return r


def moving_average(x:np.array, n=2) -> np.array:
    y = lfilter([1/n]*n, [1, 0], x)
    return y

##############
## Filters

def plot_lp(w:np.array, h:np.array, wc:float=None):
    '''
    w in radians/s
    '''
    w = w/(2*np.pi)
    plt.subplots(1,2,figsize=(12,4))

    plt.subplot(1,2,1)
    plt.plot(w, np.abs(h))
    plt.title('Magnitude')
    plt.xlabel('Frequency (Hz)')
    if wc:
        plt.vlines(wc, 0, 1/np.sqrt(2), 'red', 'dashed')
        plt.hlines(1/np.sqrt(2), 0, wc, 'red', 'dashed')

    plt.subplot(1,2,2)
    plt.plot(w, np.angle(h)*180/np.pi)
    plt.title('Phase')
    plt.xlabel('Frequency (Hz)')
    if wc:
        plt.vlines(wc, -45, -90, 'red', 'dashed')
        plt.hlines(-45, 0, wc, 'red', 'dashed')

##############
## Otehrs
from  matplotlib import patches
from matplotlib.figure import Figure
from matplotlib import rcParams
    
def zplane(b,a,filename=None):
    '''
    Plot the complex z-plane given a transfer function.
    # Copyright (c) 2011 Christopher Felton
    #
    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU Lesser General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.
    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU Lesser General Public License for more details.
    #
    # You should have received a copy of the GNU Lesser General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.
    #

    # The following is derived from the slides presented by
    # Alexander Kain for CS506/606 "Special Topics: Speech Signal Processing"
    # CSLU / OHSU, Spring Term 2011.
    # https://www.dsprelated.com/showcode/244.php
    '''

    # get a figure/plot
    ax = plt.subplot(111)

    # create the unit circle
    uc = patches.Circle((0,0), radius=1, fill=False,
                        color='black', ls='dashed')
    ax.add_patch(uc)

    # The coefficients are less than 1, normalize the coeficients
    if np.max(b) > 1:
        kn = np.max(b)
        b = b/float(kn)
    else:
        kn = 1

    if np.max(a) > 1:
        kd = np.max(a)
        a = a/float(kd)
    else:
        kd = 1
        
    # Get the poles and zeros
    p = np.roots(a)
    z = np.roots(b)
    k = kn/float(kd)
    
    # Plot the zeros and set marker properties    
    t1 = plt.plot(z.real, z.imag, 'go', ms=10)
    plt.setp( t1, markersize=10.0, markeredgewidth=1.0,
              markeredgecolor='k', markerfacecolor='g')

    # Plot the poles and set marker properties
    t2 = plt.plot(p.real, p.imag, 'rx', ms=10)
    plt.setp( t2, markersize=12.0, markeredgewidth=3.0,
              markeredgecolor='r', markerfacecolor='r')

    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    # set the ticks
    r = 1.5; plt.axis('scaled'); plt.axis([-r, r, -r, r])
    ticks = [-1, -.5, .5, 1]; plt.xticks(ticks); plt.yticks(ticks)

    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)
    

    return z, p, k

from scipy.fft import fft, fftfreq
def plot_fft(x:np.array, fs:int, verbose=1):
    N = x.shape[0]
    T = 1/fs
    f = fft(x)
    xf = fftfreq(N, T)
    
    plt.plot(xf[0:N//2], 2.0/N * np.abs(f[0:N//2]))
    plt.grid()
    
    if verbose:
        print(f'Resolução espectral: {(fs/2)/(N/2)} Hz')
        
        
def fft_highlight_freq(x:np.array, fs:int, N:int, freq:float) -> None:
    f = fft(x,N)
    f_resolution = (fs/2)/(N/2)
    i = int(freq/f_resolution)
    v = np.abs(f[i])
    
    plt.stem(np.abs(f))
    plt.stem([i], [v], 'red', markerfmt='ro')
    plt.title(f'Highlight $f={freq}$ Hz at $N={N}$ (index ${i}$)')
    print(f'{v} at {i}')