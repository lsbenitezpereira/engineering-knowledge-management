import numpy as np
import matplotlib.pyplot as plt
from IPython.display import (Audio, display, clear_output)

import time
import unittest
from scipy.io import wavfile

import sys
sys.path.append('..')
from dsp_utils import *


from scipy.signal import freqz
########
class TestTone(unittest.TestCase):
    '''
    Utils: 
        assertEqual
        assertNotEqual
        assertTrue
        assertInt
    
    '''
    def test_frequency(self):
        x = tonegen(100, 1/100, fs=2*100, play=False)
        self.assertEqual(len(x), 2)
        x = tonegen(100, 10/100, fs=2*100, play=False)
        self.assertEqual(len(x), 2*10)
    def test_amplitude_1(self):
        x = tonegen(330, 1, amplitude=1, fs=8000, play=False)
        self.assertAlmostEqual(max(x), 1, delta=0.1)
        self.assertAlmostEqual(min(x), -1, delta=0.1)
    def test_amplitude_10(self):
        x = tonegen(330, 1, amplitude=10, fs=8000, play=False)
        self.assertAlmostEqual(max(x), 10, delta=0.1)
        self.assertAlmostEqual(min(x), -10, delta=0.1)
    def test_play(self):
        x = tonegen(440, 1, amplitude=10, fs=8000, play=True)
        self.assertNotEqual(type(x), None)          
unittest.main(TestTone(), argv=[''], verbosity=2, exit=False)

class TestDTMF(unittest.TestCase):
    def test_len(self):
        seq = ['9', '8', '1', 'A']
        x = dtmf_gen_seq(seq, 0.1, 40e-3, 0.7, play=False)
        self.assertEqual(len(x), int(len(seq)*0.1*8000 + (len(seq)-1)*40e-3*8000))
    def test_play(self):
        seq = ['9', '8', '1', 'A']
        x = dtmf_gen_seq(seq, 0.3, 0.1, 1, play=True)
        self.assertNotEqual(type(x), None)
unittest.main(TestDTMF(), argv=[''], verbosity=2, exit=False)







class TestHalf(unittest.TestCase):
    def test_basic(self):
        x1=np.array([7, 3, 9, 1, 0, 4])
        x2 = halfsp(x1)
        self.assertTrue((x2 == [7, 9, 0]).all())
unittest.main(TestHalf(), argv=[''], verbosity=2, exit=False)

class TestDouble(unittest.TestCase):
    def test_basic(self):
        x1=np.array([7, 3, 9, 1, 0, 4])
        x2 = doublesp(x1)
        self.assertTrue((np.abs(x2 - np.array([7, 5, 3, 6, 9, 5, 1, 0.5, 0, 2, 4]))<0.1).all())
    def test_small(self):
        x1=np.array([1])
        x2 = doublesp(x1)
        self.assertTrue((np.abs(x2 - np.array([1]))<0.1).all())
    def test_negative(self):
        x1=np.array([1, -1, 1, -1])
        x2 = doublesp(x1)
        self.assertTrue((np.abs(x2 - np.array([1, 0, -1, 0, 1, 0, -1]))<0.1).all())
unittest.main(TestDouble(), argv=[''], verbosity=2, exit=False)



class TestEcho(unittest.TestCase):
    def test_len(self):
        fs, x = wavfile.read('../02 - Audio basics/assets/my_voice.wav')
        x_new = echo(x, fs, 0.65, 0.250, play=False)
        self.assertEqual(x_new.shape[0], x.shape[0])

    def test_impulse(self):
        x = np.array([1,0,0,0])
        x_new = echo(x, 1, 0.5, 1, play=False)
        self.assertTrue((np.abs(x_new - np.array([1,0.5, 0, 0]))<0.1).all())
        
    def test_play(self):
        fs, x = wavfile.read('../02 - Audio basics/assets/my_voice.wav')
        x_new = echo(x, fs, 0.65, 0.250)
        self.assertNotEqual(type(x_new), None)
unittest.main(TestEcho(), argv=[''], verbosity=2, exit=False)


### Filters
class TestMA(unittest.TestCase):
    '''
    Utils: 
        assertEqual
        assertNotEqual
        assertTrue
        assertInt
    
    '''
    def test_basic(self):
        y = moving_average([10,3,-3,3,8,1,-2])
        self.assertTrue((np.abs(y - np.array([5, 6.5, 0, 0, 5.5, 4.5, -0.5]))<0.01).all())
        
    def test_edges(self):
        y = moving_average([0,0,0])
        self.assertTrue((np.abs(y - np.array([0,0,0]))<0.01).all())
        
        y = moving_average([1, 0, 1])
        self.assertTrue((np.abs(y - np.array([0.5,0.5,0.5]))<0.01).all())         
unittest.main(TestMA(), argv=[''], verbosity=2, exit=False)




############
## Others
class FrequencyUtils(unittest.TestCase):
    def test_zplane(self):
        b = np.array([0.5, 0.5])
        a = np.array([1, 0])
        zplane(b,a)
        
    def test_plot_lp(self):
        b = [0.1325, 0.1325]
        a = [1.0000, -0.7350]
        w, h = freqz(b, a, fs=22050)
        plot_lp(w*2*np.pi,h, 1063)
        
    def test_fft_highlight(self):
        fs = 4000
        x = tonegen(400, 1, fs=fs, play=False)
        fft_highlight_freq(x, fs=fs, N=100, freq=400)
unittest.main(FrequencyUtils(), argv=[''], verbosity=2, exit=False)

