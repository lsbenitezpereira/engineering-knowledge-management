#include <stm32f4xx.h>
#include <arm_math.h>
#include <stm32f4_discovery.h>
#include <stm32f4_discovery_accelerometer.h>
//#include <wolfson_pi_audio.h>
#include <diag/Trace.h>
#include <dwt.h>

int main(int argc, char* argv[])
{
	UNUSED(argc);
	UNUSED(argv);

	uint32_t zcycleCount;

	// Initialise the HAL Library; it must be the first
	// instruction to be executed in the main program.
	HAL_Init();

	DWT_Enable();

	trace_printf("Init OK\n");

	volatile int16_t ai16=5;
	volatile int16_t bi16=105;
	int16_t ci16;

	volatile float32_t af32=5;
	volatile float32_t bf32=105;
	float32_t cf32;

	volatile float64_t af64=5;
	volatile float64_t bf64=105;
	float64_t cf64;

	while (1) {
		// Add your code here.
			DWT_Reset();

			zcycleCount = DWT_GetValue();
			trace_printf("int cycle: %d\n", DWT_GetValue() - zcycleCount);

			zcycleCount = DWT_GetValue();
			//Aqui soma dos numeros int16
			ci16=ai16+bi16;
			//Final da soma dos numeros

			// problema do trace_printf seguinte � que a subtra��o
			// parece fazer parte do c�lculo.
			// Melhor usar diretamente o registrador
			//https://mcuoneclipse.com/2017/01/30/cycle-counting-on-arm-cortex-m-with-dwt/
			trace_printf("int cycle: %d\n", DWT_GetValue() - zcycleCount);
			trace_printf("int16 c=%d\n", ci16);


			zcycleCount = DWT_GetValue();
			//Aqui soma dos numeros FLOAT 32
			cf32=af32+bf32;
			//Final da soma dos numeros
			trace_printf("float 32 cycle: %d\n", DWT_GetValue() - zcycleCount);
			trace_printf("float 32 c=%f\n", cf32);


			zcycleCount = DWT_GetValue();
			//Aqui soma dos numeros FLOAT 64
			cf64=af64+bf64;
			//Final da soma dos numeros
			trace_printf("float 64 cycle: %d\n", DWT_GetValue() - zcycleCount);
			trace_printf("float 64 c=%f\n", cf64);


		}

	return 0;
}

