#include <stm32f4xx.h>
#include <arm_math.h>
#include <stm32f4_discovery.h>
#include <stm32f4_discovery_accelerometer.h>
#include <wolfson_pi_audio.h>
#include <diag/Trace.h>
#include <tests.h>
#include <dwt.h>
#include "filter.h"
//#include "math_helper.h"

#define NUM_TAPS   6
#define BLOCK_SIZE (WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE)/4

int16_t TxBuffer[WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE];
int16_t RxBuffer[WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE];

__IO BUFFER_StateTypeDef buffer_offset = BUFFER_OFFSET_NONE;

__IO uint8_t Volume = 70;

uint32_t AcceleroTicks;
int16_t AcceleroAxis[3];


int main(int argc, char* argv[])
{
	UNUSED(argc);
	UNUSED(argv);

	uint32_t k;

	float32_t inputF32Buffer[BLOCK_SIZE];
	float32_t outputF32Buffer[BLOCK_SIZE];


#ifdef OS_USE_SEMIHOSTING
	//Semihosting example
#endif

	// Initialise the HAL Library; it must be the first
	// instruction to be executed in the main program.
	HAL_Init();

	DWT_Enable();

#ifdef OS_USE_SEMIHOSTING
	//Semihosting example
#endif

	WOLFSON_PI_AUDIO_Init((INPUT_DEVICE_LINE_IN << 8) | OUTPUT_DEVICE_BOTH, 80, AUDIO_FREQUENCY_48K);

	WOLFSON_PI_AUDIO_SetInputMode(INPUT_DEVICE_LINE_IN);

	WOLFSON_PI_AUDIO_SetMute(AUDIO_MUTE_ON);

	WOLFSON_PI_AUDIO_Play(TxBuffer, RxBuffer, WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE);

	WOLFSON_PI_AUDIO_SetVolume(Volume);

	BSP_ACCELERO_Init();

	TEST_Init();

	float inputF32s[32];
	float outputF32s[32];
	float goldRef[32];

	arm_rfft_fast_instance_f32 S;

	arm_status status;
	status=arm_rfft_fast_init_f32(&S, 32);
	if (status==ARM_MATH_ARGUMENT_ERROR) {
		trace_printf("PROBLEM at arm_rfft_fast_init_f32.\n");
		while(1);
	}

	trace_printf("End of initialization.\n");

	while (1) {
		//=============sinal 1===========
		for (k=0; k<32; k++) {
			inputF32s[k]=1; // sinal constante. Saída FFT = 32 no nível DC; 0 em todos os outros
		}
		goldRef[0]=32;
		for (k=1; k<32; k++) {
			goldRef[k]=0; // sinal constante. Saída FFT = 32 no nível DC; 0 em todos os outros
		}
		//=============fim sinal 1===========

		arm_rfft_fast_f32(&S, inputF32s, outputF32s, 0);
		for (k=0; k<32; k++) {
			if (fabs(outputF32s[k] - goldRef[k]) > 0.00001) { //comparacao de floats
				trace_printf("PROBLEM at gold1, array index %d\n", k);
				while(1);
			}
		}
		trace_printf("PASS gold1\n");

		//=============sinal 2===========
		for (k=0; k<32; k++) {
			inputF32s[k]=1+sin(2*M_PI*(1/32.0)*k); // senoide + nivel DC - Matlab/Octave: x=1+sin(2*pi*1/32*[0:31])
		}
		for (k=0; k<32; k++) {
			goldRef[k]=0; //Saída FFT = 32 no nível DC; -16 na parte imag. da primeira harmonica; resto tudo zero
		}
		goldRef[0]=32;
		goldRef[3]=-16;
		//https://www.keil.com/pack/doc/CMSIS/DSP/html/group__RealFFT.html
	    //The FFT of a real N-point sequence has even symmetry in the frequency domain.
		//The second half of the data equals the conjugate of the first half flipped in frequency.
		//Looking at the data, we see that we can uniquely represent the FFT using only N/2 complex numbers.
		//These are packed into the output array in alternating real and imaginary components:
	    //X = { real[0], imag[0], real[1], imag[1], real[2], imag[2] ... real[(N/2)-1], imag[(N/2)-1 }
		//=============fim sinal 2===========
		arm_rfft_fast_f32(&S, inputF32s, outputF32s, 0);
		for (k=0; k<32; k++) {
			if (fabs(outputF32s[k] - goldRef[k]) > 0.00001) { //comparacao de floats
				trace_printf("PROBLEM at gold2, array index %d\n", k);
				while(1);
			}
		}
		trace_printf("PASS gold2\n");



		trace_printf("End of processing.\n");
		/*
		// Add your code here.
		if(buffer_offset == BUFFER_OFFSET_HALF)
		{
			for(i=0, k=0; i<(WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE/2); i++) {
				if(i%2) {
					inputF32Buffer[k] = (float32_t)(RxBuffer[i]/32768.0);//convert to float LEFT
					k++;
				}
				else {
					TxBuffer[i] = RxBuffer[i];//   RIGHT (canal de baixo no OcenAudio)
				}
			}

			for(i=0, k=0; i<(WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE/2); i++) {
				if(i%2)	{
					TxBuffer[i] = (int16_t)(outputF32Buffer[k]*32768);//back to 1.15
					k++;
				}
			}

		buffer_offset = BUFFER_OFFSET_NONE;
		}

		if(buffer_offset == BUFFER_OFFSET_FULL)
		{
			for(i=(WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE/2), k=0; i<WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE; i++) {
				if(i%2) {
					inputF32Buffer[k] = (float32_t)(RxBuffer[i]/32768.0);//convert to float
					k++;
				}
				else {
					TxBuffer[i] = RxBuffer[i];//pass-through(int16_t)0.3*32768;//
				}
			}


			for(i=(WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE/2), k=0; i<WOLFSON_PI_AUDIO_TXRX_BUFFER_SIZE; i++) {
				if(i%2)	{
					TxBuffer[i] = (int16_t)(outputF32Buffer[k]*32768.0);//back to 1.15
					k++;
				}
			}
			buffer_offset = BUFFER_OFFSET_NONE;
		}
		TEST_Main();
		*/
	}
	return 0;
}

/*--------------------------------
Callbacks implementation:
--------------------------------------------------------*/

/**
  * @brief  Manages the DMA full Transfer complete event.
  */
void WOLFSON_PI_AUDIO_TransferComplete_CallBack(void)
{
	buffer_offset = BUFFER_OFFSET_FULL;
}

/**
  * @brief  Manages the DMA Half Transfer complete event.
  */
void WOLFSON_PI_AUDIO_HalfTransfer_CallBack(void)
{
	  buffer_offset = BUFFER_OFFSET_HALF;
}

/**
  * @brief  Manages the DMA FIFO error interrupt.
  * @param  None
  * @retval None
  */
void WOLFSON_PI_AUDIO_OUT_Error_CallBack(void)
{
  /* Stop the program with an infinite loop */
  while (1);
}
