# Title:	Exercice 01
# Author:	Leonardo Benitez
# Brief:	Dimention-wise array multiplication


	## Array 1
	addi $t0, $zero, 3
	addi $t1, $zero, 2
	addi $t2, $zero, 1
	addi $t3, $zero, 7
	addi $t4, $zero, 5
	addi $t5, $zero, 9
	addi $t6, $zero, 4
	addi $t7, $zero, 8
	
	sw $t0, 0 ($gp)
	sw $t1, 4 ($gp)
	sw $t2, 8 ($gp)
	sw $t3, 12 ($gp)
	sw $t4, 16 ($gp)
	sw $t5, 20 ($gp)
	sw $t6, 24 ($gp)
	sw $t7, 28 ($gp)
	
	## Array 2
	addi $t0, $zero, 5
	addi $t1, $zero, 8
	addi $t2, $zero, 9
	addi $t3, $zero, 1
	addi $t4, $zero, 3
	addi $t5, $zero, 4
	addi $t6, $zero, 7
	addi $t7, $zero, 6
	
	sw $t0, 32 ($gp)
	sw $t1, 36 ($gp)
	sw $t2, 40 ($gp)
	sw $t3, 44 ($gp)
	sw $t4, 48 ($gp)
	sw $t5, 52 ($gp)
	sw $t6, 56 ($gp)
	sw $t7, 60 ($gp)

	# Init
	addi $s0, $zero, 0	# k
	addi $s1, $zero, 2	# N
	addi $s2, $zero, 0	# Result
	addi $s3, $gp, 0	# array 1 addr
	addi $s4, $gp, 32	# array 2 addr
	
	# Processing
for:	bge	$s0, $s1, end 	#for(k=0; k<N; k++)	# 7 cycles (translated into 2 instructions)
	lw	$t0, ($s3)	# x[k]			# 5 cycles
	lw	$t1, ($s4)	# y[k]			# 5 cycles
	mul	$t2, $t0, $t1				# 4 cycles
	add	$s2, $s2, $t2	#result += x[k]*y[k]	# 4 cycles
	addi	$s3, $s3, 4				# 4 cycles
	addi	$s4, $s4, 4				# 4 cycles
	addi	$s0, $s0, 1	#k++			# 4 cycles
	j	for					# 3 cycles
end:

	# Number of cycles (considering a MIPS multicicle): n*(1*3 + 5*4 + 2*5 + 1*7) + 7
	# Number of instructions: n*10 + 2
	# For N=2, we'll have 22 instructions in 87 cycles
	# For N=8, we'll have 82 instructions in 327 instructions
