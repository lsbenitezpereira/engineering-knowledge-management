# Introdução

Realizaremos aqui a análise dos dados InsideAirBnb no Rio de Janeiro [1], que apresenta as características de 35 mil hospedagens disponíveis na cidade, compiladas no ano de 2020.

Especificamente, focaremos em analisar a influências da diversas variáveis no **preço da hospedagem**, e interpretar os resultados à luz das teorias de marketing apresentados ao longo do curso Marketing Analytics [2], ministrado pelos professores Thiago Marques (USP/IBGE) e Marcos Severo (UFG).

Diviremos este trabalho em 3 etapas:

1) Enriquecer os dados fornecidos com outras fontes de dados e/ou modelos de machine learning pré-treinados, preparando o dataset para as próximas análises;

2) Calcular e avaliar as principais medidas de estatística descritiva do dataset, em especial a correlação das variáveis com o preço;

3) Utilizar métodos de detecção de outliers - hospedagens muito diferentes das demais - e avaliar se a ”normalidade” de uma hospedagem influencia no seu preço.

Este documento esta organizado conforme segue:

[TOC]

# Desenvolvimento

## Enriquecimento dos dados

### Localização

Para incluirmos na análise fatores demográficos do bairro onde o imóvel está localizado, utilizamos a base de dados de Prefeitura do Rio de Janeiro (Data Rio). Enriquecemos com informações gerais sobre o bairro, escolhendo algumas características que julgamos que _poderiam_ influenciar no preço do imóvel.


A partir da Tabela 3132 [3], obteve-se a proporção de pessoas com 10 anos ou mais alfabetizadas:

![image-20210125232544260](Images - Relatório/image-20210125232544260.png)

A partir da Tabela 2974 [4], obteve-se a população total do bairro e a proporção de pessoas com menos de 30 anos em relação a população total (ou seja, quão jovem tendem a ser as pessoas que moram no bairro):

![image-20210125232354734](Images - Relatório/image-20210125232354734.png)

### Idioma

A coluna “name” é apenas texto em linguagem natural, não estruturada, e portanto muito difícil de ser analisada estatisticamente. Para incluir essa variável no presente trabalho, utilizou-se um modelo de machine learning pre-treinado para classificar **qual o idioma do texto**. O modelo usado foi o `TextCat`, disponível na biblioteca Natural Language Toolkit, que é baseado no método “N-Gram-Based Text Categorization”.

Por simplicidade, classificou-se o texto apenas nas categorias “português” ou “outra”, e foram identificadas 14.110 e 21.621 amostras correspondentes a cada uma das respectivas categorias. A rotina utilizada para o processo é conforme segue:

```python
tc = nltk.TextCat()
def guess_language(s):
    try:
        language = tc.guess_language(s)
        if language!='por': 
            language='other'
    except:
        language = 'other'
    return language

df_merged['name_lan'] = df_merged['name'].map(guess_language)
```

### Transformações nos dados

Para facilitar a utilização  computacional, a variável “last_review” foi transformada para um número inteiro com o número de dias transcorridos desde o ultimo review (em relação ao dia 26 de janeiro de 2021), utilizando a seguinte rotina:

```python
def process_days_ago(s, format='%Y-%m-%d'):
    try:
        result = (datetime.now() - datetime.strptime(s, format)).days
    except:
        result = np.NaN
    return result

df_merged['last_review_days_ago'] = df_merged['last_review'].map(process_days_ago)
```

![image-20210125233939562](Images - Relatório/image-20210125233939562.png)

> Histograma da variável transformada last_review_days_ago

A variável reviews_per_month continha valores indefinidos, NaN (Not a Number), quando a hospedagem não possuia nenhum review. Para facilitar a utilização dos dados, converteu-se esses valores para zero.

Quando treinando modelos de machine learning para complementar a análise (o que será feito na análise de importância e análise de outliers), serão aplicadas também a transformação `getDumies` nas variáveis categóricas (room_type e name_lan), visando facilitar o treinamento dos modelos. Essa transformação faz com que, por exemplo, “homem” e “mulher” seja convertida para duas variáveis booleanas “homem=falso ou verdadeiro” e “mulher=falso ou verdadeiro”.

Por fim, algumas variáveis não foram utilizadas na análise, são elas: neighbourhood_group, id, host_id. As variáveis latitude e longitude foram utilizadas apenas para fins de visualização, mas poderiam ser transformadas para um formato mais relevante (como a distância para a o centro da cidade, por exemplo).

## Medidas descritivas e correlações

Seguem algumas medidas descritivas do preço:

| Medida        | Valor  |
| ------------- | ------ |
| Mínimo        | R\$ 0,00      |
| Percentil 25% | R\$ 159,00    |
| Média         | R\$ 787,00    |
| Percentil 75% | R\$ 642,00    |
| Percentil 90% | R\$ 1498,00   |
| Máximo        | R\$ 132358,00 |

Podemos perceber que 50% dos dados estão contidos em uma faixa “razoável” de 155 a 642, mas a distribuição é fortemente enviesada para preços _muito_ mais altos do que a média. Podemos perceber também que alguns valores provavelmente são frutos de erros de digitação e erros na coleta de dados, como preços zerados ou preços exageradamente altos. 

--------------

Para observarmos a relação do preço com a localização, plotamos um mapa com cada ponto sendo uma hospedagem e a cor do ponto sendo correspondente ao preço do imóvel. Os imóveis com preços acima do percentil 90% (R$ 1492,00) foram removidos para facilitar a visualização. 

![image-20210126233551452](Images - Relatório/image-20210126233551452.png)

Apesar de haverem inúmeras exceções, vemos que _em geral_ os imóveis perto da praia possuem um preço mais elevado. Também podemos perceber que alguma regiões possuem alta densidade de imóveis, como nos arredores dos aeroportos ou das praias mais populares. Com exceção deste gráfico, as variáveis e latitude e longitude não foram mais utilizadas na análise.

-------------

Avaliamos a mediana de **preço quando segmentamos por diferentes tipos de imóveis** (variável room_type) e diferentes idiomas no nome (variável name_lan). Para ambas as variáveis, utilizamos o teste de hipótese não paramétrico (não assume uma distribuição gaussiana) Kruskal-Wallis H-test [5] para avaliar se as medianas são significativamente diferentes:

![image-20210126233742011](Images - Relatório/image-20210126233742011.png)

Percebemos que alugar o apartamento inteiro e escrever o título do anúncio em uma língua estrangeira são opções que maximizam o preço médio do imóvel. Fica a dica!!!

--------------------------

A correlação de Spearman é uma medida que indica se duas variáveis possuem uma relação monotônica. Ou seja se quando uma cresce a outra tende a crescer também (correlação entre 0 e 1) ou se quando uma cresce a outra tende a decrescer (correlação entre -1 e 0). Para os dados em estudo, a correlação das variáveis numéricas com o preço são, ordenados pela magnitude do módulo:

| Variável        | Correlação |
| ------------- | ------ |
| reviews_per_month        |        -0.380002 |
| number_of_reviews         |       -0.366709 |
| last_review_days_ago     |        -0.257248 |
| minimum_nights           |         0.179453 |
| literate                   |       0.165524 |
| calculated_host_listings_count |  -0.087265 |
| age_proportion           |         0.041635 |
| availability_365         |        -0.021811 |
| pop_total               |         -0.009059 |


Observamos que, quanto maior o preço, menos reviews o imóvel tende a ter, o que **vem ao encontro das teorias de marketing acerca da relação entre preço e consumo**. Para confirmarmos que essa relação é significativa, utilizamos o teste não paramétrico de Spearman e comprovamos a afirmação.

Entretanto, graficamente é possível perceber que essas relações são 1) não lineares, e 2) altamente “espalhadas”, longes de indicarem um relacionamento claro. Seguem os gráficos das 4 variáveis com maior correlação vs o preço:

![image-20210126234118568](Images - Relatório/image-20210126234118568.png)

Apesar de altamente informativa, a correlação de spearman não é adequada para avaliar influências que não são monotônicas. Para darmos mais robustez a nossa análise, avaliamos a relação das diferentes variáveis com o preço por um método alternativo: treinamos um modelo de aprendizado estatístico de Random Forest Regression e utilizamos o **calculo de Feature Importance** [7] (que retorna valores entre 0 e 1 de acordo com quão relevantes as variáveis foram na construção do modelo, sem diferenciar se as variáveis se reforçam ou de anulam) para avaliar os resultados anteriores.

| Variável | Correlação |
| ------------- | ------ |
| calculated_host_listings_count |   0.282523 |
| availability_365              |    0.206356 |
| minimum_nights      |              0.099057 |
| pop_total             |            0.078986 |
| literate                |          0.070970 |
| age_proportion           |         0.068784 |
| reviews_per_month      |           0.062511 |
| last_review_days_ago       |       0.046386 |
| name_lan_int                |      0.034456 |
| number_of_reviews          |       0.025001 |
| room_type Entire home/apt    |     0.018320 |
| room_type Private room       |     0.002941 |
| room_type Shared room     |        0.002637 |
| room_type Hotel room       |       0.001070 |

Observamos aqui um fenômeno interessante: **a análise de importância concorda com alguns aspectos com as análises anteriores, mas não completamente.**

Quanto ao tipo de hospedagem, por exemplo, a “Entire home/apt” apareceu como uma variável mais importante que as demais, coincidindo com o fato que esta categoria também apresenta o maior preço médio, mas “Hotel room” aparece como sendo uma variável menos relevante.

Em relação à correlação de Spearman, as variáveis relacionadas à localização (população total, proporção de jovens e proporção de alfabetizados) aparecem como sendo mais importantes, enquanto as variáveis relacionadas aos reviews (número de reviews, reviews por mês e data do último review) apareceram abaixo. Esses dois grupos de modificações aparecem realçados abaixo:

![image-20210127211439172](Images - Relatório/image-20210127211439172.png)

## Detecção de outliers

Conforme percebemos na sessão anterior, existem alguns valores zerados ou exageradamente altos no dataset. As técnicas de detecção de outliers permitem identificar automaticamente essas amostras anormais, que podem então ser removidas ou então analisadas de forma especial.

Todas as variáveis foram incluídas na análise, incluindo o preço. Ou seja, levamos em consideração apenas os dados aqui apresentados, e não outros fatores como fotos do imóvel ou a descrição completa do anúncio

Utilizamos a técnica de Isolation Forest [8] para identificar outliers, que identificiou 5525 amostras como outliers. Para validar se essas amostram devem de fato serem tratadas separadamente, faz-se necessário a análise manual por parte de um especialista na área, o que não será feito no presente trabalho. Esse método fornece também um “escore de normalizade”, onde valores maiores indicam inliers (amostras normais). Traçando gráficos de diversas variáveis vs escore de normalidade, percebemos que este escore possui uma **relação muito interessante com o número de reviews**:

![image-20210127212412793](Images - Relatório/image-20210127212412793.png)

Vemos que quanto mais “normal” é o imóvel, menos reviews ele tende a receber. Podemos interpretar esse resultado sob a lógica de que “o diferente chama a atenção”, se destaca, especialmente no contexto de marketing. Como vimos anteriormente, os reviews recebidos são uma da variáveis que mais influenciam o preço.

# Conclusão

Neste trabalho conseguimos identificar quais variáveis mais mais contribuem para a composição do preço, e como o preço influencia no número de reviews que o imóvel recebe. Essas informações podem ser utilizadas para tomar decisões bem informadas por parte de um locador de imóveis, identificando estratégias ótimas para maximizar o seu lucro.

Vale ressaltar que nem todas as análises convergiram para as mesmas respostas: a análise de importância com Random Forest apresentou resultados um pouco diferentes das análises anteriores, o que deve ser analisado por um especialista na área de marketing para identificar quais das análises melhor explica a realidade. 

Por fim, ressaltamos uma característica profundamente desafiadora da área de Marketing Analytics: a sua interdisciplinaridade. Ao longo desta breve análise passamos por conceitos de demografia, linguagem, inteligência artificial, entre vários outros. Diante desta miríade de áreas de conhecimento, o papel do analista é humildemente navegar no caos, aceitando a incompletude de sua análise e o seu constante processo de aprimoramento.

# Referências

[1] Inside AirBnB. Disponíveis em http://insideairbnb.com/rio-de-janeiro/

[2] Curso de Marketing Analytics. Disponivel em https://www.youtube.com/watch?v=lRTR7XHwOe4&list=PLjdDBZW3EmXdnMDPMSChpslsw5sxDZA7L

[3] Data Rio (2019b). Tabela 3132. Prefeitura do Rio de Janeiro. Disponível em [https://www.data.rio/datasets/percentual-de-pessoas-de-10-anos-ou-mais-de-idade-total-e-com-renda-por-condi%C3%A7%C3%A3o-de-alfabetiza%C3%A7%C3%A3o-segundo-regi%C3%B5es-administrativas-ra-e-bairros-no-munic%C3%ADpio-do-rio-de-janeiro-2010](https://www.data.rio/datasets/percentual-de-pessoas-de-10-anos-ou-mais-de-idade-total-e-com-renda-por-condição-de-alfabetização-segundo-regiões-administrativas-ra-e-bairros-no-município-do-rio-de-janeiro-2010).

[4] Data Rio (2019a). Tabela 2974. Prefeitura do Rio de Janeiro. Disponível em [https://www.data.rio/datasets/popula%C3%A7%C3%A3o-residente-por-grupos-de-idade-e-sexo-segundo-as-%C3%A1reas-de-planejamento-ap-regi%C3%B5es-administrativas-ra-e-bairros-em-2000-2010-](https://www.data.rio/datasets/população-residente-por-grupos-de-idade-e-sexo-segundo-as-áreas-de-planejamento-ap-regiões-administrativas-ra-e-bairros-em-2000-2010-)

[5] SciPy. Teste H de Kruskal-Wallis. Disponível em: https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kruskal.html

[6] SciPy. Teste de Spearman rank-order correlation. Disponível em: https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.spearmanr.html

[7] Machine Learning Mastery. How to Calculate Feature Importance With Python	. Disponível em: https://machinelearningmastery.com/calculate-feature-importance-with-python/

[8] Tony Liu et al. Isolation-Based Anomaly Detection. Disponível em https://dl.acm.org/doi/10.1145/2133360.2133363