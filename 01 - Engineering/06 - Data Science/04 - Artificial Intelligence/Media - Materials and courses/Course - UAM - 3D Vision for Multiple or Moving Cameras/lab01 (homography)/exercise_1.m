clc; clear; close all;
img = imread('Hopper.JPG');

xy_origin = [41 484 586 72; 124 30 299 414];
xy_target = [32 632 632 32; 25 25 468 468];

% Compute the transformation matrix using the matching points
H_1 = maketform('projective', xy_origin', xy_target');
transformed_img_1 = imtransform(img, H_1);

H_2 = homography_solve_vmmc(xy_origin, xy_target);
transformed_img_2 = imtransform(img, maketform('projective', H_2'));

% Display the images in a 1x3 subplot
figure;

subplot(1, 3, 1);
imshow(img);
title('Original Image');

subplot(1, 3, 2);
imshow(transformed_img_1);
title('Transformed Image by method 1');
disp('H_1 matrix:'); disp(H_1.tdata.T);

subplot(1, 3, 3);
imshow(transformed_img_2);
title('Transformed Image by method 2');
disp('H_2 matrix:'); disp(H_2);

disp('The two matrices are proportional by '); disp(mean(mean(H_1.tdata.T ./ H_2')))