clc; clear; close all;
img = imread('Hopper.JPG');
img_target = zeros(size(img), 'like', img);

xy_origin = get_user_points_vmmc(img);  % Usage: click with left, left, left, right
xy_target = get_user_points_vmmc(img_target);

H = maketform('projective', xy_origin', xy_target');
img_transformed = imtransform(img, H);

figure;
subplot(1, 2, 1);
imshow(img);
title('Original Image');

subplot(1, 2, 2);
imshow(img_transformed);
title('Transformed Image');

disp('H matrix:'); disp(H.tdata.T);