clc; clear; close all;
img = imread('perspective_pattern.bmp');
img_reference = imread('reference_pattern.bmp');

xy_origin = [18 475 599 41; 59 11 420 446];
xy_target = get_user_points_vmmc(img_reference);  % Usage: click with left, left, left, right; on the 4 thick corners
H = maketform('projective', xy_origin', xy_target');
img_transformed = imtransform(img,H,'XData',[1 size(img_reference,2)], 'YData',[1 size(img_reference,1)]);

figure;
subplot(1, 3, 1);
imshow(img);
title('Original Image');

subplot(1, 3, 2);
imshow(img_reference);
title('Reference Image');

subplot(1, 3, 3);
imshow(img_transformed);
title('Transformed Image');

error = get_error_energy_vmmc(img_reference, img_transformed);
disp('Error:'); disp(error);

