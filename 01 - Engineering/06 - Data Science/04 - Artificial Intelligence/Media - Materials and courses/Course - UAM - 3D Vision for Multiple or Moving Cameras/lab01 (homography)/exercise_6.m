clc; clear; close all;
images = {
    imread('mountain1.jpg'),
    imread('mountain2.jpg'),
    imread('mountain3.jpg'),
    imread('mountain4.jpg')
};

th = 0.1;

img_final = images{1};
for i = 2:4
    H = homography_auto_vmmc_2(img_final, images{i}, th);
    [img_final, stitched_mask, im1, im2] = stitch_vmmc(img_final, images{i}, maketform('projective', H'));
end
imshow(img_final);