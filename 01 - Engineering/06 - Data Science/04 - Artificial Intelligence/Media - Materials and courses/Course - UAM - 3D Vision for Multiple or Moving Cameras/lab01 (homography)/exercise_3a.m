clc; clear; close all;

n=12;
img = imread('perspective_pattern.bmp');
img_reference = imread('reference_pattern.bmp');

fprintf('Click on the %d thick corners of the image\n', n);
xy_origin = get_user_points_vmmc(img);  % Usage: click with left, left, left, right
xy_target = get_user_points_vmmc(img_reference);



H = fitgeotrans(xy_origin', xy_target', 'projective');
img_transformed = imtransform(img, maketform('projective', H.T), 'XData', [1 size(img_reference,2)], 'YData', [1 size(img_reference,1)]);

figure;
subplot(1, 3, 1);
imshow(img);
title('Original Image');

subplot(1, 3, 2);
imshow(img_reference);
title('Reference Image');

subplot(1, 3, 3);
imshow(img_transformed);
title(sprintf('Transformed Image (n = %d)', n));

error = get_error_energy_vmmc(img_reference, img_transformed);
fprintf('Error for %d points: %f\n', n, error);
disp('Selected points in the original image:');
disp(xy_origin);
disp('Selected points in the reference image:');
disp(xy_target);
