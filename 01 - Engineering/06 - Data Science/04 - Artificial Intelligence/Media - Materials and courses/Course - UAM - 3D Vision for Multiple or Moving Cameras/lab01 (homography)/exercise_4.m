clc; clear; close all;

% Load images
img = imread('hopper_vanishing.bmp');
img_reference = imread('hopper_reference.bmp');

[h, w, ~] = size(img_reference);
%xy_origin = get_user_points_vmmc(img);  % Manually select 4 points in the image, the corners
%xy_target = get_user_points_vmmc(img_reference);
xy_origin = [252, 371, 417, 202; 183, 159, 331, 272];
xy_target = [27, 635, 633, 30; 22, 22, 470, 474]; 

% Compute homography 
H = fitgeotrans(xy_origin', xy_target', 'projective');
img_transformed = imtransform(img, maketform('projective', H.T), 'XData', [1 size(img_reference,2)], 'YData', [1 size(img_reference,1)]);
H = homography_solve_vmmc(xy_origin, xy_target);  % For some reason the rest of the code only works with the H found by this method...

% Find the line at infinity
l_inf = H' * [0; 0; 1];  % Mapping the ideal line [0;0;1] to original space

% Convert to affine form
x = linspace(1, w, 100); % Generate x-coordinates
y = (-l_inf(1) * x - l_inf(3)) / l_inf(2); % Solve for y


% Display results
figure;
subplot(1, 4, 1);
imshow(img);
title('Original Image');

subplot(1, 4, 2);
imshow(img_reference);
title('Reference Image');

subplot(1, 4, 3);
imshow(img_transformed);
title('Transformed Image');

subplot(1, 4, 4);
imshow(img);
hold on;
plot(x, y, 'r', 'LineWidth', 2); % Red line
title('Vanishing Line in the Original Image');
hold off;

disp('Homography matrix:');
disp(H);
disp('Selected points in the original image:');
disp(xy_origin);
disp('Selected points in the reference image:');
disp(xy_target);
fprintf('Equation of the vanishing line in the original image:\n');
fprintf('%.4fx + %.4fy + %.4f = 0\n', l_inf(1), l_inf(2), l_inf(3));