clc; clear; close all;

img_a1 = imread('pan_demo_1.png');
img_a2 = imread('pan_demo_2.png');
img_b1 = imread('mountain1.jpg');
img_b2 = imread('mountain2.jpg');

for th = [0.05, 0.1, 0.5, 1]
    disp(['Threshold = ', num2str(th)]);
    H_a = homography_auto_vmmc_2(img_a2, img_a1, th);
    H_b = homography_auto_vmmc_2(img_b1, img_b2, th);

    [stitched_image_a, stitched_mask_a, im1_a, im2_a] = stitch_vmmc(img_a1, img_a2, maketform('projective', H_a'));
    [stitched_image_b, stitched_mask_b, im1_b, im2_b] = stitch_vmmc(img_b2, img_b1, maketform('projective', H_b'));

    figure;
    subplot(2, 1, 1);
    imshow(stitched_image_a);
    title('Stitched Image A');

    subplot(2, 1, 2);
    imshow(stitched_image_b);
    title('Stitched Image B');

    sgtitle(['Stitched Images with Threshold = ', num2str(th)]);

    disp('---------------------------------------------------------')
end
