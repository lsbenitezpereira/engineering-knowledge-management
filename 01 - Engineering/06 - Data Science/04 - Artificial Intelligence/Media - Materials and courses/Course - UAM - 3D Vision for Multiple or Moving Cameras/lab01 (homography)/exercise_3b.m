clc; clear; close all;

n=6;
img = imread('Hopper.JPG');
img_reference = imread('hopper_reference.bmp');

fprintf('Click on the %d thick corners of the image\n', n);
xy_origin = get_user_points_vmmc(img);
xy_target = get_user_points_vmmc(img_reference);

H_manual = fitgeotrans(xy_origin', xy_target', 'projective');
img_transformed_manual = imtransform(img, maketform('projective', H_manual.T), 'XData', [1 size(img_reference,2)], 'YData', [1 size(img_reference,1)]);

H_auto = homography_auto_vmmc(img, img_reference);
img_transformed_auto = imtransform(img, maketform('projective', H_auto'), 'XData', [1 size(img_reference,2)], 'YData', [1 size(img_reference,1)]);

figure;
subplot(1, 4, 1);
imshow(img);
title('Original Image');

subplot(1, 4, 2);
imshow(img_reference);
title('Reference Image');

subplot(1, 4, 3);
imshow(img_transformed_manual);
title(sprintf('Transformed Image (manually with n = %d)', n));

subplot(1, 4, 4);
imshow(img_transformed_auto);
title('Transformed Image (automatically)');

error = get_error_energy_vmmc(img_reference, img_transformed_manual);
fprintf('Error for manually selected %d points: %f\n', n, error);

error = get_error_energy_vmmc(img_reference, img_transformed_auto);
fprintf('Error for automatically selected: %f\n', error);

disp('---------------------------------------------------------')
disp('Running automated rotaiton 5 times')

for i = 1:5
    H_auto = homography_auto_vmmc(img, img_reference);
    img_transformed_auto = imtransform(img, maketform('projective', H_auto'), 'XData', [1 size(img_reference,2)], 'YData', [1 size(img_reference,1)]);
    error = get_error_energy_vmmc(img_reference, img_transformed_auto);
    disp('Error:'); disp(error);
end
