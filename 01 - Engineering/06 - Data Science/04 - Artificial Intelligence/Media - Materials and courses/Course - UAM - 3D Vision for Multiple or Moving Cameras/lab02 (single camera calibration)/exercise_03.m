clc; clear; close all;
Np = 9;
widhtP = 800;
display = false;

[coords, img_reference] = get_real_points_checkerboard_vmmc(Np, widhtP, display);
H_all = {};
for j = 1:6
    img = imread(sprintf('MobileCamera_Data/calibration_%d.jpg', j));
    xy_origin = get_user_points_vmmc(img);
    xy_target = coords';

    assert(isequal(size(xy_origin), size(xy_target)), 'The selected points and target points must have the same shape.');
    H = homography_solve_vmmc(xy_target, xy_origin);
    [H, rperr] = homography_refine_vmmc(xy_target, xy_origin, H);
    assert(all(size(H) == [3, 3]), 'Homography matrix H must be 3x3.');
    H_all{j} = H;
    
    if display
        img_transformed = imtransform(img_reference, maketform('projective', H'));

        figure;
        subplot(1, 2, 1);
        imshow(img);
        title('Original Image');

        subplot(1, 2, 2);
        imshow(img_transformed);
        title('Transformed Image');
    end

    disp('Image:'); disp(j);
    disp('Selected points in the image:');
    disp(xy_origin);
    disp('Refined homography matrix:');
    disp(H);
    disp('Reprojection error:');
    disp(rperr);
    disp('------------------------------------------------------')
end

disp('===============================================================')
for i = 3:6
    A = internal_parameters_solve_vmmc(H_all(1:i));
    theta = acot(-A(1,2)/A(1,1));
    disp(sprintf('Estimated internal parameters with %d images', i));
    disp(sprintf('scale factor for the U axis: %.4f', A(1, 1)));
    disp(sprintf('scale factor for the V axis: %.4f', A(2, 2)*abs(sin(theta))));
    disp(sprintf('u0 coordinate of the principal point: %.4f', A(1, 3)));
    disp(sprintf('v0 coordinate of the principal point: %.4f', A(2, 3)));
    disp(sprintf('angle between the image axes: %.4f degrees', theta*180/pi));
    disp('A: '); disp(A);
    disp('-----------------------------------------------------------')
end
