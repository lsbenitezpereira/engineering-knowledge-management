clc; clear; close all;
Np = 9;
widhtP = 320;
display = false;

[coords, img_reference] = get_real_points_checkerboard_vmmc(Np, widhtP, display);
H_all = {};
for j = 1:5
    img = imread(sprintf('FixedCamera_Data/PatternImage_Orientation_%d.bmp', j));
    xy_origin = get_user_points_vmmc(img);
    xy_target = coords';

    assert(isequal(size(xy_origin), size(xy_target)), 'The selected points and target points must have the same shape.');
    H = homography_solve_vmmc(xy_target, xy_origin);
    [H, rperr] = homography_refine_vmmc(xy_target, xy_origin, H);
    H_all{j} = H;
    img_transformed = imtransform(img_reference, maketform('projective', H'));

    if display
        figure;
        subplot(1, 2, 1);
        imshow(img);
        title('Original Image');

        subplot(1, 2, 2);
        imshow(img_transformed);
        title('Transformed Image');
    end

    disp('Image:'); disp(j);
    disp('Selected points in the image:');
    disp(xy_origin);
    disp('Refined homography matrix:');
    disp(H);
    disp('Reprojection error:');
    disp(rperr);
    disp('------------------------------------------------------')
end
