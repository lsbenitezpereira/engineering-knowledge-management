close all;

% Define the homography matrices
% Use the variable H_all and A from the exercise_02, or hardcode the values here

for i = 1:5
      [R T] = external_parameters_solve_vmmc(A, H_all(i));
      Rt = [R{1} T{1}; zeros(1, 3) 1];
      disp(sprintf('Estimated external parameters from image %d', i));
      disp('[R t]: '); disp(Rt);
      disp('-----------------------------------------------------------')
end