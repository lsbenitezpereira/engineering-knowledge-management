% Given 2 cameras with known positions (with respect to a common origin), convert the second to be with respect to the first
%clc; clear; close all;
T_1 = [
    -0059.7;
    0598.7;
    -3198.5;
]
T_2 = [
    -0059.7;
    0598.7;
    3198.5;
]

R_1 = [
    -0.6696,  0.0777, 0.7387;
    0.0235,  -0.9918, 0.1256;
    0.7424,   0.1014, 0.6623;
];

R_2 = [
    -0.6023,  0.0862, 0.7936;
    0.0235,  -0.9918, 0.1256;
    0.7979,   0.0942, 0.5954;
];

T_2_new = T_2 - T_1;
R_2_new = R_1'*R_2;
disp('Camera 2 with respect to Camera 1:');
fprintf('Displacement in X-axis: %.2f meters \n', T_2_new(1)/1000);
fprintf('Displacement in Y-axis: %.2f meters \n', T_2_new(2)/1000);
fprintf('Displacement in Z-axis: %.2f meters \n', T_2_new(3)/1000);
fprintf('Yaw (ψ, Rotation about Z-axis): %.2f degrees\n', atan2d(R_2_new(2,1), R_2_new(1,1)));
fprintf('Pitch (θ, Rotation about Y-axis): %.2f degrees\n', asind(-R_2_new(3,1)));
fprintf('Roll (φ, Rotation about X-axis): %.2f degrees\n', atan2d(R_2_new(3,2), R_2_new(3,3)));