% Obtain the external parameters for an unknown position, given analready calibrated camera
clc; clear; close all;
Np = 9;
widhtP = 800;
display = false;

A = [
    2.9750, 0.0370, 1.9986;
         0, 2.9904, 1.4946;
         0,      0, 0.0010;
];

% Homography from reference to camera
img = imread('MobileCamera_Data/unknown_position_1.jpg');
[coords, img_reference] = get_real_points_checkerboard_vmmc(Np, widhtP, display);
xy_origin = get_user_points_vmmc(img);
xy_target = coords';
H = homography_solve_vmmc(xy_target, xy_origin);
[H, rperr] = homography_refine_vmmc(xy_target, xy_origin, H);

% Obtain external parameters
[R T] = external_parameters_solve_vmmc(A, {H});
disp('R: '); disp(R{1});
disp('T: '); disp(T{1});

yaw = atan2d(R{1}(2,1), R{1}(1,1));
pitch = asind(-R{1}(3,1));
roll = atan2d(R{1}(3,2), R{1}(3,3));
dsplacement_x = T{1}(1);
dsplacement_y = T{1}(2);
dsplacement_z = T{1}(3);
fprintf('Yaw (ψ, Rotation about Z-axis): %.2f degrees\n', yaw);
fprintf('Pitch (θ, Rotation about Y-axis): %.2f degrees\n', pitch);
fprintf('Roll (φ, Rotation about X-axis): %.2f degrees\n', roll);
fprintf('Displacement in X-axis: %.2f meters \n', dsplacement_x/1000);
fprintf('Displacement in Y-axis: %.2f meters \n', dsplacement_y/1000);
fprintf('Displacement in Z-axis: %.2f meters \n', dsplacement_z/1000);