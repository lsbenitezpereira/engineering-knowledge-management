close all;

% Define the homography matrices
% Use the variable H_all from the exercise_01, or hardcode the values here
% H1 = [-0.0036, 0.0006, 0.7950;
%       -0.0002, -0.0030, 0.6066;
%       -0.0000, 0.0000, -0.0043]';
% 
% H2 = [0.0026, -0.0003, -0.9439;
%       0.0002, 0.0021, -0.3301;
%       -0.0000, -0.0000, 0.0042]';
% 
% H3 = [0.0023, 0.0002, -0.9764;
%       0.0001, 0.0016, -0.2160;
%       0.0000, 0.0000, 0.0018]';
% 
% H4 = [-0.0025, -0.0002, 0.8964;
%       0.0008, -0.0032, 0.4432;
%       0.0000, -0.0000, -0.0031]';
% 
% H5 = [0.0027, 0.0003, -0.8036;
%       0.0001, 0.0037, -0.5951;
%       -0.0000, -0.0000, 0.0054]';
% 
% H_all = {H1, H2, H3, H4, H5};

% Ground truth
a = 1828;
b = 1828;
u0 = 512;
v0 = 280;

for i = 3:5
      A = internal_parameters_solve_vmmc(H_all(1:i));
      theta = acot(-A(1,2)/A(1,1));
      disp(sprintf('Estimated internal parameters with %d images', i));
      disp(sprintf('Error for scale factor for the U axis: %.4f', abs(A(1, 1) - a)));
      disp(sprintf('Error for scale factor for the V axis: %.4f', abs(A(2, 2)*abs(sin(theta)) - b)));
      disp(sprintf('Error for u0 coordinate of the principal point: %.4f', abs(A(1, 3) - u0)));
      disp(sprintf('Error for v0 coordinate of the principal point: %.4f', abs(A(2, 3) - v0)));
      disp(sprintf('Error for angle between the image axes: %.4f degrees', abs(90 - theta*180/pi)));
      %disp('theta: '); disp(theta);
      disp('A: '); disp(A);
      disp('-----------------------------------------------------------')
end

disp(sprintf('Final scale factor for the U axis: %.4f', A(1, 1)));
disp(sprintf('Final scale factor for the V axis: %.4f', A(2, 2)*abs(sin(theta))));
disp(sprintf('Final u0 coordinate of the principal point: %.4f', A(1, 3)));
disp(sprintf('Final v0 coordinate of the principal point: %.4f', A(2, 3)));
disp(sprintf('Final angle between the image axes: %.4f degrees', theta*180/pi));