from sklearn.preprocessing import OrdinalEncoder

def fit_encoder(column):
    encoder = OrdinalEncoder(dtype=int)
    encoder.fit(column)
    return encoder


def encode_data(data, feature_names, encoders=None):
  if encoders:
    for i in range(len(feature_names)):
        data[feature_names[i]] = encoders[i].transform(data[feature_names[i]].values.reshape(-1, 1))

  else:
    encoders = []
    for feature_name in feature_names:
        feature_data = data[feature_name].values.reshape(-1, 1)
        encoder = fit_encoder(feature_data)
        encoders.append(encoder)
        data[feature_name] = encoder.transform(feature_data)

  return data, encoders
