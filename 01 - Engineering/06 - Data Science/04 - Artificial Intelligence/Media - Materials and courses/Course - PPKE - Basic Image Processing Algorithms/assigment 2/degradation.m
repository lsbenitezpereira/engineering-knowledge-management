% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [y, h, n] = degradation(x)
    h = fspecial('motion', 21, 11);
    n = sqrt(0.001)*randn(size(x));
    y = imfilter(x, h, 'replicate', 'same', 'conv') + n;
end
