% Powered by LLMs
% Compatible with Octave 5.2

% Main script to be used to iteratively select patches from images
close all;
clear;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over images
input_data = load('input/input_data.mat');
patch_counter = 1;
patch_counter = 58;
for i = 1 : size(input_data.input_data, 2)
    if i <= 3
        continue;
    end

    fprintf('Reading image %d...\n', i);
    y = imread(input_data.input_data{i}.degraded_img_path);
    original_size = size(y);
    y = im2double(y);

    len = input_data.input_data{i}.len;
    theta = input_data.input_data{i}.theta;
    h = fspecial('motion', double(len), double(theta));
    
    y_blurred_edges = blur_edge(y, 180, 20);

    x_tilde = restoration_rl(y_blurred_edges, h, 0.08);
    x_tilde = imresize(x_tilde, 0.2);

    desired_mean = 0.6 * max(x_tilde(:));
    desired_contrast = 0.2 * max(x_tilde(:));
    Amax = 3;
    p = 0.15;
    x_tilde_filtered = wallis_operator(x_tilde, desired_contrast, desired_mean, p, Amax, 5);

    x_tilde_filtered_hsv = rgb2hsv(x_tilde_filtered);
    x_tilde_filtered_hsv(:, :, 2) = min(1, x_tilde_filtered_hsv(:, :, 2)*1.3);
    x_tilde_filtered = hsv2rgb(x_tilde_filtered_hsv);

    x_tilde_filtered = im2uint8(x_tilde_filtered);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Loop for interactive patch selection
    while true
        disp('Draw a rectangle to select a patch or double-click to confirm.');
        imshow(x_tilde_filtered);
        drawnow;

        rect = getrect; % Returns [x, y, width, height]
        patch = imcrop(x_tilde_filtered, rect);
        category = questdlg('Select the category for this patch:', 'Patch Category', 'Car', 'Tree', 'Grass', 'Car');
        if isempty(category)
            disp('No patch selected. Continuing...');
            continue;
        end

        imwrite(patch, fullfile('./training_textures', lower(category), sprintf('%d.png', patch_counter)));
        fprintf('Saved patch %d\n', patch_counter);

        close;
        patch_counter = patch_counter + 1;

        choice = questdlg('Do you want to select more patches?', 'Continue Selecting?', 'Yes', 'No', 'Yes');
        if strcmp(choice, 'No')
            break;
        end
    end
end