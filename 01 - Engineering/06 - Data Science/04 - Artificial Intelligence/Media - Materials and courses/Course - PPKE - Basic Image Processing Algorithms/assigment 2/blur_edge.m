% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function y_edgetapered = blur_edge(y, hsize, sigma)
    % Works for both grayscale and color images
    if nargin < 2
        hsize = 60;
    end
    if nargin < 3
        sigma = 10;
    end

    if size(y, 3) == 1
        num_channels = 1;
    elseif size(y, 3) == 3
        num_channels = 3;
    else
        error('Input must be a grayscale or color image');
    end

    % Initialize the output image
    y_edgetapered = zeros(size(y));

    % Process each channel separately
    for c = 1:num_channels
        h = fspecial('gaussian', hsize, sigma);
        OTF = psf2otf(h, size(y(:,:,c)));

        blurred_img = abs(ifft2(OTF .* fft2(y(:,:,c))));
        wm = ones(70, 70);
        wm = padarray(wm, [1 1], 0, 'both');
        wm = imresize(wm, size(y(:,:,c)), 'bilinear');
        wm = cast(wm, class(y));
        blurred_img = cast(blurred_img, class(y));
        y_edgetapered(:,:,c) = wm .* y(:,:,c) + (1 - wm) .* blurred_img;
    end
end
