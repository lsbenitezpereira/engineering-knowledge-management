% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function y = wallis_operator(x, sigma_d, x_d, p, Amax, window_size)
    % Applies the Wallis operator to an image to adjust local contrast.
    % 
    % Inputs:
    %   x           - Input image (color or grayscale, type double, range [0, 1] or [0, 255]).
    %   sigma_d     - Desired local contrast (scalar).
    %   x_d         - Desired mean value (scalar).
    %   p           - Weighting factor for mean compensation (scalar between 0 and 1).
    %   Amax        - Maximum adjustment factor for local contrast (scalar).
    %   window_size - Size of the local neighborhood (scalar, e.g., 3, 5, etc.).
    %
    % Outputs:
    %   y           - Output image after applying the Wallis operator.

    % Ensure the input image is in the range [0, 255] for processing
    if max(x(:)) <= 1
        x = x * 255;
    end
    
    % Check if the input is a color image (3 channels)
    if size(x, 3) == 3
        % Process each color channel separately
        y = zeros(size(x));
        for c = 1:3
            y(:, :, c) = process_channel(x(:, :, c), sigma_d, x_d, p, Amax, window_size);
        end
    else
        % Process a single-channel grayscale image
        y = process_channel(x, sigma_d, x_d, p, Amax, window_size);
    end
    
    % Clip the output to a valid range
    y = max(min(y, 255), 0);
end

function y_channel = process_channel(x, sigma_d, x_d, p, Amax, window_size)
    % Helper function to process a single channel with the Wallis operator.
    
    % Compute the local mean and variance using sliding window
    local_mean = colfilt(x, [window_size, window_size], 'sliding', @mean);
    local_variance = colfilt(x, [window_size, window_size], 'sliding', @var);
    local_std = sqrt(local_variance);
    
    % Initialize the output channel
    [rows, cols] = size(x);
    y_channel = zeros(rows, cols);
    
    % Loop through each pixel
    for i = 1:rows
        for j = 1:cols
            % Local mean and local std at the current pixel
            mean_local = local_mean(i, j);
            std_local = local_std(i, j);
            
            % Adjustment factor for contrast (clipping to Amax)
            A = min(Amax * sigma_d / (Amax * std_local + sigma_d), Amax);
            
            % Compute the Wallis operator for the current pixel
            y_channel(i, j) = A * (x(i, j) - mean_local) + ...
                              p * x_d + (1 - p) * mean_local;
        end
    end
end
