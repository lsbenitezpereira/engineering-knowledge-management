% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function MODEL = laws_training_phase(T_cell)
    MODEL = zeros(length(T_cell), 27); % Updated to 27 columns (9 per channel)
    for n = 1:9
        for k = 1:length(T_cell)
            texture = T_cell{k};
            if size(texture, 3) == 3
                hsvImage = rgb2hsv(texture);
                R_channel = stretch_lin(hsvImage(:, :, 1) + 0.5 * hsvImage(:, :, 3));
                G_channel = stretch_lin(hsvImage(:, :, 2));
                B_channel = stretch_lin(hsvImage(:, :, 3));
            else
                R_channel = stretch_lin(texture);
                G_channel = stretch_lin(texture);
                B_channel = stretch_lin(texture);
            end

            R_conv = conv2(R_channel, laws_kernel(n), 'same');
            G_conv = conv2(G_channel, laws_kernel(n), 'same');
            B_conv = conv2(B_channel, laws_kernel(n), 'same');

            R_feature = sum(sum(R_conv .* R_conv)) / (size(texture, 1) * size(texture, 2));
            G_feature = sum(sum(G_conv .* G_conv)) / (size(texture, 1) * size(texture, 2));
            B_feature = sum(sum(B_conv .* B_conv)) / (size(texture, 1) * size(texture, 2));

            MODEL(k, (n - 1) * 3 + 1) = R_feature;
            MODEL(k, (n - 1) * 3 + 2) = G_feature;
            MODEL(k, (n - 1) * 3 + 3) = B_feature;
        end
    end
end