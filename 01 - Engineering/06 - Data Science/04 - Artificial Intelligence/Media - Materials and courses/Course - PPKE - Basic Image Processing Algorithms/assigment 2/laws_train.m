% Powered by LLMs
% Compatible with Octave 5.2

% Main script to be used to train the Laws model
% Assumes multiple samples for each class
% Assumes the classes are in separate folders: car, tree, grass
% These images should have been selected beforehand and received the same preprocessing as at inference time (use script `laws_select_data.m`)
% The resulting model and labels is printed and should be hardcoded in your program for inference
close all;
clear;
clc;

folder = 'training_textures';
classes = {'car', 'tree', 'grass'};
T_cell = {};
labels = [];
for i = 1:length(classes)
    class = classes{i};
    filePattern = fullfile(folder, class, '*.png');
    files = dir(filePattern);
    
    for k = 1:length(files)
        % Read
        baseFileName = files(k).name;
        fullFileName = fullfile(folder, class, baseFileName);
        fprintf(1, 'Now reading %s\n', fullFileName);
        
        % Append to texture bank
        T_cell{end+1} = imread(fullFileName);
        labels = [labels, i];
    end
end


MODEL = laws_training_phase(T_cell);

%%%%%%%%%
% Save
%disp(MODEL);
%disp(labels);
fileID = fopen('model.txt', 'w');

fprintf(fileID, 'MODEL = [\n');
[rows, cols] = size(MODEL);
for i = 1:rows
    fprintf(fileID, '    ');
    fprintf(fileID, '%g ', MODEL(i, 1:cols-1)); % Write all but the last column
    fprintf(fileID, '%g;\n', MODEL(i, cols));   % Add the last column with a semicolon
end
fprintf(fileID, '];\n');

fprintf(fileID, 'labels = [\n');
[rows, cols] = size(labels);
for i = 1:rows
    fprintf(fileID, '    ');
    fprintf(fileID, '%g ', labels(i, 1:cols-1)); % Write all but the last column
    fprintf(fileID, '%g;\n', labels(i, cols));   % Add the last column with a semicolon
end
fprintf(fileID, '];\n');

fclose(fileID);