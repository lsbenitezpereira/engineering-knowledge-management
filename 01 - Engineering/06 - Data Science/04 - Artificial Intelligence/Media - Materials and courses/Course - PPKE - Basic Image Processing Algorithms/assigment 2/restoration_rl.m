% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function u = restoration_rl(y, h, threshold, maxNumIterations)
    % Stops automatically when the RMSE improvement (comparing current result with the original image) between two consecutive iterations is less than the threshold
    if nargin < 4
        maxNumIterations = 60; % Default maximum number of iterations
    end
    
    y = im2double(y);
    h = h / sum(h(:), "all");
    H = psf2otf(h, size(y));
    Y = fft2(y);
    u = y;
    prevRMSE = inf;

    for k = 1:maxNumIterations
        % Perform Richardson-Lucy iteration
        U = fft2(u);
        division = ifft2(U .* H);
        r = y ./ max(division, 1e-6);
        r = ifft2(fft2(r) .* conj(H));
        u = u .* r;

        % Compute RMSE in the frequency domain
        Y_reblur = U .* H;             % Re-blurred image in frequency domain
        rmse = sqrt(sum(abs(Y(:) - Y_reblur(:)).^2) / numel(Y));
        
        % Check stopping criterion
        %fprintf('    %f\n', abs(prevRMSE - rmse));
        if abs(prevRMSE - rmse) < threshold
            %fprintf('    Converged at iteration %d with RMSE difference %.6f\n', k, abs(prevRMSE - rmse));
            break;
        end
        
        if mod(k, 10) == 0
            fprintf('    RL iteration %d: RMSE Difference = %.6f\n', k, abs(prevRMSE - rmse));
        end

        prevRMSE = rmse;
    end

    u = abs(u);
end
