% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function ClassMap = laws_recognition_phase(I, MODEL)
    if size(I, 3) == 3
        hsvImage = rgb2hsv(I);
        R_channel = stretch_lin(hsvImage(:, :, 1) + 0.5 * hsvImage(:, :, 3));
        G_channel = stretch_lin(hsvImage(:, :, 2));
        B_channel = stretch_lin(hsvImage(:, :, 3));
    else
        R_channel = stretch_lin(I);
        G_channel = stretch_lin(I);
        B_channel = stretch_lin(I);
    end

    BB = zeros(size(I, 1), size(I, 2), 27); % Unified feature map for all channels
    for k = 1:9
        R_conv = conv2(R_channel, laws_kernel(k), 'same');
        G_conv = conv2(G_channel, laws_kernel(k), 'same');
        B_conv = conv2(B_channel, laws_kernel(k), 'same');

        norm = ones(15) / (15 * 15);
        BB(:, :, (k - 1) * 3 + 1) = conv2(R_conv .* R_conv, norm, 'same');
        BB(:, :, (k - 1) * 3 + 2) = conv2(G_conv .* G_conv, norm, 'same');
        BB(:, :, (k - 1) * 3 + 3) = conv2(B_conv .* B_conv, norm, 'same');
    end

    ClassMap = zeros(size(I, 1), size(I, 2)); % Correct output size
    for x = 1:size(I, 1)
        for y = 1:size(I, 2)
            min_dist = inf;
            min_class = 0;
            for n = 1:size(MODEL, 1)
                diff = squeeze(BB(x, y, :))';
                dist = sum(abs(diff - MODEL(n, :)));
                if dist < min_dist
                    min_dist = dist;
                    min_class = n;
                end
            end
            ClassMap(x, y) = min_class;
        end
    end
end
