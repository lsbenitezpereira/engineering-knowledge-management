% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [x_tilde] = restoration_wiener(y, h, n)
    Y = fft2(y);
    N = fft2(n);
    OTF = psf2otf(h, size(y));

    % Power spectra
    PNN = N .* conj(N);
    PYY = Y .* conj(Y);
    PXX = PYY; % We don't know the matrix X, so we approximate P_XX with P_YY.

    R = conj(OTF) ./ (abs(OTF).^2 + PNN ./ PXX);
    x_tilde = abs(ifft2(Y .* R));

end
