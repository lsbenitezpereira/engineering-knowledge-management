% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [x_tilde] = restoration_CLS(y, h, alpha, c)
    Y = fft2(y);
    OTF = psf2otf(h, size(y));
    C = psf2otf(c, size(y));
    R = conj(OTF) ./ (abs(OTF).^2 + alpha * (abs(C).^2));
    x_tilde = abs(ifft2(Y .* R));
end
