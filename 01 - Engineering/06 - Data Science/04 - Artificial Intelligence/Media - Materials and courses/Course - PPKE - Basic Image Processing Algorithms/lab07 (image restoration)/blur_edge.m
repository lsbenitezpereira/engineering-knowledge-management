% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function y_edgetapered = blur_edge(y)
    %at the center preserve the original; at the boundaries use the blurred one)
    %The same image with blurred boundary should be returned
    h = fspecial('gaussian', 60, 10);
    OTF = psf2otf(h, size(y));

    blurred_img = abs(ifft2(OTF .* fft2(y)));
    wm = ones(70, 70);
    wm = padarray(wm, [1 1], 0, 'both');
    wm = imresize(wm, size(y), 'bilinear');
    y_edgetapered = wm .* y + (1-wm) .* blurred_img;
end
