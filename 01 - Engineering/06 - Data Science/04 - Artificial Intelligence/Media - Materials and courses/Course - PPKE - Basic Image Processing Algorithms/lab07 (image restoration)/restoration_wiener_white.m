% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [x_tilde] = restoration_wiener_white(y, h, var_n)
    Y = fft2(y);
    OTF = psf2otf(h, size(y));

    NSPR = var_n / var(y(:));
    R = conj(OTF) ./ (abs(OTF).^2 + NSPR);
    x_tilde = abs(ifft2(Y .* R));
end
