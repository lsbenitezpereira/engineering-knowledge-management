% Powered by LLMs
function [H] = my_hough(input_img)
  % Assumes the matrix contains {0,1} values only
  dim_x = size(input_img, 1);
  dim_y = size(input_img, 2);
  r_max = ceil(sqrt(dim_x^2 + dim_y^2));
  H = zeros(2*r_max+1, 180);
  % For every non-zero pixel...
  for i = 1:dim_x
    for j = 1:dim_y
      if input_img(i, j) == 1
        % Draw its sinusoid
        for theta = 1:180
          radius = round(i*cosd(theta) + j*sind(theta));
          H(radius + r_max + 1, theta) = H(radius + r_max + 1, theta) + 1;
        end
      end
    end
  end
end





