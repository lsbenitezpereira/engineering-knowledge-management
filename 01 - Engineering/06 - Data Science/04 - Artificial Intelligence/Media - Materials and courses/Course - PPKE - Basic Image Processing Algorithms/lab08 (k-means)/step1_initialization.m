% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [LUT, M] = step1_initialization(S, k)
    m = size(S, 1);
    n = size(S, 2);

    LUT = zeros(1, m);
    M = zeros(k, n);

    indices = 1 : floor(m/k) : (m-k+1);
    %disp(indices);
    M = S(indices, :);
end
