% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function M = step3_update(S, k, LUT, M)
    m = size(S, 1);
    n = size(S, 2);
    for j = 1:k
        cluster_points = S(LUT == j, :);
        if ~isempty(cluster_points)
            M(j, :) = mean(cluster_points, 1);
        end
    end
end
