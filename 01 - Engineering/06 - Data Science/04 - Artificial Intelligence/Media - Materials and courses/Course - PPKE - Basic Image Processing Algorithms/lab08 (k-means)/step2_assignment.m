% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function LUT = step2_assignment(S, k, LUT, M)
    m = size(S, 1);
    n = size(S, 2);
    %fprintf('Number of clusters (k): %d\n', k);
    for i = 1:m
        distances = sum((M - S(i, :)).^2, 2);
        %disp(distances);
        %disp('--------------------------')
        [~, minIndex] = min(distances);
        LUT(i) = minIndex;
    end
end
