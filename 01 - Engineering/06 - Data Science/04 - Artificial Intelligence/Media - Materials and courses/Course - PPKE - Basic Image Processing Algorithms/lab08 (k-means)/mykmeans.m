% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [LUT, M, iter] = mykmeans(S, k)
    m = size(S, 1);
    n = size(S, 2);
    iter_max = 100;
    epsilon = 0.02;

    [LUT, M] = step1_initialization(S, k);
    M_old = -inf(k, n);
    iter = 0;

    %disp('----------------------------------------')
    while (iter<iter_max) & (sum(sum((M - M_old).^2))>epsilon)
        LUT = step2_assignment(S, k, LUT, M);
        M_old = M;
        M = step3_update(S, k, LUT, M);
        iter = iter + 1;
        %disp(iter);
        %disp(sum((M - M_old).^2, 2))
    end
end
