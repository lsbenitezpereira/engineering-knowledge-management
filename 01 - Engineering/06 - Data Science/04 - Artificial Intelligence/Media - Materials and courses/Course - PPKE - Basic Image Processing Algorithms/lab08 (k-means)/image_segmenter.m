% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function [O] = image_segmenter(I, k)
    [x_dim, y_dim, c_dim] = size(I);
    S = reshape(I, x_dim*y_dim, c_dim);
    m = size(S, 1);
    n = size(S, 2);

    [LUT, M, iter] = mykmeans(S, k);
    %disp('K MEANS DONE')
    S_replaced = M(LUT, :);
    assert(isequal(size(S_replaced), size(S)), 'S_replaced does not have the same size as M');
    O = reshape(S_replaced, x_dim, y_dim, c_dim);
end
