% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function H = laws_kernel(k)
    if ~isnumeric(k) || k < 1 || k > 9
        error('Input argument k must be a number between 1 and 9.');
    end
    LES = [1 1 1; 2 0 -2; 1 -1 1] .* [1/6 1/2 1/2; 1/6 1/2 1/2; 1/6 1/2 1/2] ;  % Each base filter in a column
    first_matrix = LES(:, ceil(int32(k+1) / 3));  % I'm not sure if these equations are right, but they work up to k=9
    second_matrix = LES(:, mod((floor(k)-1), 3) + 1);
    H=first_matrix*second_matrix';
end
