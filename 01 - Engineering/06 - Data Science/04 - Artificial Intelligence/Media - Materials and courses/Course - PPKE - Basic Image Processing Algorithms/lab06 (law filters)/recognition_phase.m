% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function ClassMap = recognition_phase(I, MODEL)
    % Computing the sum of absolute differences for every class
    BB = zeros(size(I, 1), size(I, 2), 9);
    for k = 1:9
        B = conv2(I, laws_kernel(k), 'same');
        norm = ones(15)/(15*15);
        BB(:,:, k) = conv2(B.*B, norm, 'same');
        %disp(size(BB))
    end

    % find the n where the value of sumAbsDiff is the smallest
    % MODEL_{n, k}: scalar result of applying filter $n$ on training image $k$
    ClassMap = zeros(size(I));
    for x = 1:size(I, 1)
        for y = 1:size(I, 2)
            min_dist = inf;
            min_class = 0;
            for n = 1:size(MODEL, 1)
                dist = sum(abs(squeeze(BB(x, y, :))' - squeeze(MODEL(n, :))));
                assert(isscalar(dist), 'dist is not a scalar');
                if dist < min_dist
                    min_dist = dist;
                    min_class = n;
                end
            end
            %disp(min_class)
            ClassMap(x, y) = min_class;
        end
    end
    assert(isequal(size(ClassMap), size(I)), 'ClassMap does not have the same size as I');
    assert(all(ClassMap(:) > 0), 'ClassMap still contains unset classes');
end
