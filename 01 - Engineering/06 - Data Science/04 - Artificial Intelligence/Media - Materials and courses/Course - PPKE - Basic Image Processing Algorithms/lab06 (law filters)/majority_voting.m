% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function OUT = majority_voting(IN, w_dia)
    % w_dia = window diameter
    OUT = zeros(size(IN));
    for x = 1:w_dia:size(IN, 1)
        for y = 1:w_dia:size(IN, 2)
            x_1 = max(1, x);
            x_2 = min(size(IN, 1), x + w_dia-1);
            y_1 = max(1, y);
            y_2 = min(size(IN, 2), y + w_dia-1);
            window = IN(x_1:x_2, y_1:y_2);
            majority_value = mode(window(:));
            OUT(x_1:x_2, y_1:y_2) = majority_value;
        end
    end
end
