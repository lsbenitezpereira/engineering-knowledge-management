% Powered by LLMs
% Compatible with Octave 5.2 on Ubuntu 20.04
function MODEL = training_phase(T_cell)
    %MODEL_{n, k}: scalar result of applying filter $n$ on training image $k$
    MODEL = zeros(length(T_cell), 9);
    for n = 1:9
        for k = 1:length(T_cell)
            %disp(size(T_cell{k}))
            %disp(size(laws_kernel(n)))
            conv = conv2(T_cell{k}, laws_kernel(n), 'same');
            MODEL(k, n) = sum(sum(conv.*conv)) / (size(T_cell{k}, 1) * size(T_cell{k}, 2));
        end
    end

end
