function mrf=gmrf_doMMD(mrf)

         cmap = load('MRF_colormap.mat'); % the colormap
            h = mrf.imagesize(1);         % height of the image
            w = mrf.imagesize(2);         % width of the image
         cnum = mrf.classnum;             % number of classes
         beta = mrf.Beta;                 % value of parameter beta
    DeltaUmin = mrf.DeltaUmin;            % value of minimal necessary energy change
            T = mrf.T0;                   % temperature at the begining
            c = mrf.c;                    % the c constant parameter
     

           cycle = 0;
    summa_deltaE = 2 * DeltaUmin; % the first iteration is guaranteed

    while summa_deltaE > DeltaUmin 
        
        % ====================================== %
        %                                        %
        %    Please, put your implementation     %
        %             BELOW THIS LINE            %
        %                                        %
        % ====================================== %
        summa_deltaE = 0;
        cycle = cycle + 1;
        for y = 1:h
            for x = 1:w
                current_class = mrf.classmask(y, x);
                %%%%%%%%%%%%%%%%%
                % neighbohood (not including center pixel)
                offsets = [-1, 0; 1, 0; 0, -1; 0, 1; -1, -1; -1, 1; 1, -1; 1, 1];
                neighbors = [];
                
                for k = 1:size(offsets, 1)
                    ny = y + offsets(k, 1);
                    nx = x + offsets(k, 2);
                    
                    if ny >= 1 && ny <= h && nx >= 1 && nx <= w
                        neighbors = [neighbors, mrf.classmask(ny, nx)];
                    end
                end
                assert(ismember(length(neighbors), [3, 5, 8]), 'Neighborhood must have 3, 5, or 8 elements');

                %%%%%%%%%%%%%%%%%
                % Compute current probs
                actual_posterior_prob = mrf.logProbs{current_class}(y, x);
                actual_prior_prob = 0;
                for neighbor_class = neighbors
                    if neighbor_class == current_class
                        actual_prior_prob = actual_prior_prob - beta;
                    else
                        actual_prior_prob = actual_prior_prob + beta;
                    end
                end


                %%%%%%%%%%%%%%%%%
                % Try out with a new class
                % If it improves, take it
                % If it doesn't, take it with a probability given by T
                new_class = current_class;
                while new_class == current_class
                    new_class = randi(cnum);
                end

                new_posterior_prob = mrf.logProbs{new_class}(y, x);
                new_prior_prob = 0;
                for neighbor_class = neighbors
                    if neighbor_class == new_class
                        new_prior_prob = new_prior_prob - beta;
                    else
                        new_prior_prob = new_prior_prob + beta;
                    end
                end

                U_act = actual_posterior_prob + actual_prior_prob;
                U_new = new_posterior_prob + new_prior_prob;
                dU = U_new - U_act;

                if dU < 0 || (dU > 0 && rand < exp(-dU / T))
                    summa_deltaE = summa_deltaE + abs(dU);
                    mrf.classmask(y, x) = new_class;
                end
            end
        end

        T = T * c;

        % ====================================== %
        %                                        %
        %    Please, put your implementation     %
        %             ABOVE THIS LINE            %
        %                                        %
        % ====================================== %    
        
        imshow(uint8(255*reshape(cmap.color(mrf.classmask,:), h, w, 3)));
        %fprintf('Iteration #%i\n', cycle);
        title(['Class map in cycle ', num2str(cycle)]);
        drawnow;
    end
end
