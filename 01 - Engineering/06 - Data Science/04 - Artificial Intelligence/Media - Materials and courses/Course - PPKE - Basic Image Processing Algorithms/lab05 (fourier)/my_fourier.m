function [F] = my_fourier(I)
  F = zeros(size(I));
  rows = size(I, 1);
  cols = size(I, 2);
  
  % Iterate over pixels in frequency domain
  for k1 = 0:rows-1
    for k2 = 0:cols-1
      % Iterate over pixels in the space domain
      s = 0;
      for n1 = 0:rows-1
        for n2 = 0:cols-1
          e1 = exp(-1i * (2 * pi * k1 * n1 / rows));
          e2 = exp(-1i * (2 * pi * k2 * n2 / cols));
          s = s + I(n1+1, n2+1) * e1 * e2;
        end
      end
      F(k1+1, k2+1) = s;
    end
  end
end
