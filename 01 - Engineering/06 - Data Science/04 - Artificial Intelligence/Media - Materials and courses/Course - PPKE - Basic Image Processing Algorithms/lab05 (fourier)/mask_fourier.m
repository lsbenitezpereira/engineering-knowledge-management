function newF = mask_fourier(F, x, y, r)
    F = fftshift(F);
    for i = 1:length(x)
        x_cur = round(x(i));
        y_cur = round(y(i));

        x_1 = max([1, x_cur-r]);
        x_2 = min([size(F, 1), x_cur+r]);
        y_1 = max([1, y_cur-r]);
        y_2 = min([size(F, 2), y_cur+r]);
        F(x_1:x_2, y_1:y_2) = 0+0i;
    end

    newF=ifftshift(F);
end
