% Powered by LLMs
% Compatible with Octave 5.2
function [angle] = identify_document_rotation(input_img)
    % Return the angle in degrees that the document is rotated clockwise.

    % Input preprocessing
    if size(input_img, 3) ~= 1
        error('Input image must be a single-channel image.');
    end

    if ~islogical(input_img)
        error('Input image must be a binary image.');
    end
    
    % Detect the strongest line in the log-magnitude FFT
    F = fft2(double(input_img));
    F_shifted = fftshift(F);
    log_magnitude = log(1 + abs(F_shifted));

    log_magnitude_edges = edge(mat2gray(log_magnitude), 'canny', [0.1 0.6]);
    [H, theta, rho] = hough(log_magnitude_edges);
    peaks = houghpeaks(H, 1);
    angle = theta(peaks(2));

    %%%%%%%%%%%
    % DEBUG
    %line_rho = rho(peaks(1));
    %theta_rad = deg2rad(angle);
    %cos_theta = cos(theta_rad);
    %sin_theta = sin(theta_rad);
    %line_length = 1000;
    %x0 = cos_theta * line_rho;
    %y0 = sin_theta * line_rho;
    %x1 = x0 + line_length * (-sin_theta); % Extend line in one direction
    %y1 = y0 + line_length * cos_theta;
    %x2 = x0 - line_length * (-sin_theta); % Extend line in the other direction
    %y2 = y0 - line_length * cos_theta;
    %figure;
    %imshow(log_magnitude_edges);
    %hold on;
    %plot([x1 x2], [y1 y2], 'LineWidth', 2, 'Color', 'red');
    %hold off;
end