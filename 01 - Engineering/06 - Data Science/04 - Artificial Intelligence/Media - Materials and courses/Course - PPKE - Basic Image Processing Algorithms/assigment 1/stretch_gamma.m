% Powered by LLMs
% Compatible with Octave 5.2
function output_img = stretch_gamma(input_img, gamma)
  if size(input_img, 3) ~= 1
    error('Input image must be a single-channel image.');
  end

  img_normalized = double(input_img);
  img_normalized = img_normalized - min(img_normalized(:)); % Shift to make the minimum value 0
  img_normalized = img_normalized / max(img_normalized(:)); % Normalize to the range [0, 1]
  img_gamma_corrected = img_normalized .^ gamma;
  output_img = stretch_lin(img_gamma_corrected);
end
