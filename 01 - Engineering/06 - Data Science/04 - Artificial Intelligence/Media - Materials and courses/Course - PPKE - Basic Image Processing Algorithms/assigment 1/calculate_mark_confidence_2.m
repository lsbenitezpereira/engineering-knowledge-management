% Powered by LLMs
% Compatible with Octave 5.2
% This code is NOT used
% I experimented using HOG to clasify the marks, but it didn't work as well as the simpler hough-based approach

function hog = hog_compute(image, dimension)
  if size(image, 3) == 3
    image = rgb2gray(image);
  end

  [gx, gy] = imgradientxy(image);
  [magnitude, orientation] = imgradient(gx, gy);
  orientation = mod(orientation, 180);  % Convert to range [0, 180]

  bin_edges = linspace(0, 180, dimension + 1);
  hog = zeros(1, dimension);
  for i = 1:dimension    
    bin_mask = orientation >= bin_edges(i) & orientation < bin_edges(i + 1);  % Pixels whose orientation falls in the current bin
    hog(i) = sum(magnitude(bin_mask));
  end

  hog = hog / norm(hog);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is how you would score it
% You need to hardcode in the inferecence application some "reference hogs"
% hog_dimension = 20;
% reference_hogs = [
%     0.1586    0.1995    0.1718    0.1898    0.2682    0.3282    0.2142    0.2320    0.2032    0.1818    0.1843    0.2742    0.3845 0.2328    0.2218    0.1498    0.1968    0.2089    0.1527    0.1649;
%     0.1634    0.2214    0.2551    0.3465    0.2354    0.1436    0.1465    0.2043    0.2001    0.1601    0.1367    0.2042    0.1828 0.1914    0.2856    0.3754    0.2728    0.1761    0.2094    0.1791;
%     0.2188    0.1546    0.1219    0.1414    0.1265    0.1617    0.1495    0.1612    0.2113    0.3431    0.3756    0.2669    0.1743 0.1531    0.1296    0.1572    0.1893    0.2333    0.3627    0.3334;
%     0.2635    0.2650    0.2023    0.2006    0.2391    0.2064    0.2171    0.2042    0.2081    0.2312    0.2064    0.2745    0.2263 0.2450    0.2749    0.1822    0.2145    0.1973    0.1965    0.1796;
%     0.1682    0.2405    0.1795    0.1827    0.2880    0.3659    0.2923    0.1712    0.1943    0.2138    0.2287    0.1696    0.1954 0.2755    0.2939    0.1635    0.1565    0.1999    0.2072    0.1326;
% ];
% assert (size(reference_hogs, 2) == hog_dimension, 'Invalid reference HOG size.');
% patch_hog = hog_compute(patches{j}, hog_dimension);
% distances = sqrt(sum((patch_hog - reference_hogs).^2, 2));  % Euclidean distance to each reference HOG


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is how you would "train" it (calculate the reference)
%
%radius = 40;
%margin = 10;
%hog_dimension = 20;
%
%hogs = zeros(5, hog_dimension);
%
%I = imread('input/1.jpg');
%x1 = 897;
%y1 = 647;
%%imshow(I);
%patch = I(x1:x1+radius*2+margin*2, y1:y1+radius*2+margin*2);
%%imshow(patch);
%hogs(1, :) = hog_compute(patch, hog_dimension);
%
%
%I = imread('input/14.jpg');
%x1 = 360;
%y1 = 888;
%%imshow(I);
%patch = I(x1:x1+radius*2+margin*2, y1:y1+radius*2+margin*2);
%hogs(2, :) = hog_compute(patch, hog_dimension);
%%imshow(patch);
%
%I = imread('input/3.jpg');
%x1 = 1100;
%y1 = 675;
%%imshow(I);
%patch = I(x1:x1+radius*2+margin*2, y1:y1+radius*2+margin*2);
%hogs(3, :) = hog_compute(patch, hog_dimension);
%%imshow(patch);
%
%
%I = imread('input/6.jpg');
%x1 = 375;
%y1 = 1066;
%%imshow(I);
%patch = I(x1:x1+radius*2+margin*2, y1:y1+radius*2+margin*2);
%hogs(4, :) = hog_compute(patch, hog_dimension);
%%imshow(patch);
%
%I = imread('input/8.jpg');
%x1 = 273;
%y1 = 661;
%%imshow(I);
%patch = I(x1:x1+radius*2+margin*2, y1:y1+radius*2+margin*2);
%hogs(5, :) = hog_compute(patch, hog_dimension);
%%imshow(patch);
%
%disp(hogs);
%%disp(mean(hogs, 1););
%