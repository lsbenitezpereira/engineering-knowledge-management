% Powered by LLMs
% Compatible with Octave 5.2
function output_img = stretch_lin(input_img)
  input_img = double(input_img);
  vmin = min(min(input_img));
  vmax = max(max(input_img));
  
  output_img = uint8(floor(255/(vmax-vmin)*(input_img-vmin)));
end
