% Powered by LLMs
% Compatible with Octave 5.2
function [digitized_data] = processor(img_input, verbose)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Input preprocessing
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    num_questions = 4;
    num_options = 2;
    radius = 40;
    margin = 10;
    
    if nargin < 2
        verbose = false;
    end

    if size(img_input, 3) ~= 3
        warning('Input image is not a color image.');
    end
    img_gray = rgb2gray(img_input);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Enhance contrast
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gamma = 3;
    img_stretch = stretch_gamma(img_gray, gamma);

    % DEBUG
    %subplot(2, 2, 1);
    %imshow(img_gray);
    %title('Original image');
    %subplot(2, 2, 2);
    %imshow(img_stretch);
    %title(['Gamma corrected (gamma = ', num2str(gamma), ')']);
    %subplot(2, 2, 3);
    %imhist(img_gray);
    %title('Original histogram');
    %subplot(2, 2, 4);
    %imhist(img_stretch);
    %title('Gamma corrected histogram');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Rotation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    edges = edge(img_stretch, 'Canny');
    angle = identify_document_rotation(edges);
    rotated = imrotate(edges, angle, 'bilinear', 'loose');
    rotated_original = imrotate(img_gray, angle, 'bilinear', 'loose');

    if verbose
        figure;
        subplot(1, 2, 1);
        imshow(img_input);
        title('Original input image');
        
        subplot(1, 2, 2);
        imshow(rotated);
        title('Straightened edge image');
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Circle detection
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    rotated_processed = rotated;
    rotated_processed(:, 1:floor(1/2*size(rotated_processed, 2))) = 0; % Set the left side to black
    %rotated_processed = rotated_processed .* (rand(size(rotated_processed)) > 0.5);  % sample pixels for computational performance
    assert(isequal(size(rotated_processed), size(rotated)));
    %tic;
    H = my_hough_circle(rotated_processed, radius);
    %toc;
    H_processed = uint8(255*H/max(max(H)));
    
    % Increase brightness around the marks
    % gaussian centered around the brightest marks
    [m, n] = size(H_processed);
    [~, x_vect] = non_max_sup_2d(H_processed, num_questions, radius);
    sigma = 0.5 * n;
    x = 1:n;
    gaussian_curve = exp(-(x - mean(x_vect)).^2 / (2 * sigma^2));
    brightness_mask = repmat(gaussian_curve, m, 1);
    H_processed = H_processed .* uint8(brightness_mask);

    % Increase sharpness
    %H_processed = diff(H_processed, 1, 2);  %derivative in the horizontal direction
    H_processed = imsharpen(H, 'Radius', 5, 'Amount', 1.5);
    H_processed = uint8(255*H_processed/max(max(H_processed)));

    [y_vect, x_vect] = non_max_sup_2d(H_processed, num_questions*num_options, radius);

    if verbose
        figure;
        subplot(1, 2, 1);
        imshow(rotated_original);
        hold on;
        viscircles([x_vect' y_vect'], radius * ones(length(x_vect), 1), 'EdgeColor', 'r', 'LineWidth', 2);
        hold off;
        title('Straightened original image with circles');

        subplot(1, 2, 2);
        imshow(H_processed);
        title('Hough space');
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Mark detection
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    digitized_data = zeros(num_questions, num_options);
    if verbose
        figure;
    end
    for i = 1:num_questions
        % Find the circles of this row (by y)
        [sorted_y, sort_index] = sort(y_vect);
        sorted_x = x_vect(sort_index);
        sorted_y = sorted_y(1:num_options);
        sorted_x = sorted_x(1:num_options);

        % remove the circles of this row
        x_vect(sort_index(1:num_options)) = [];
        y_vect(sort_index(1:num_options)) = [];

        % Order the options (by x)
        [sorted_x, sort_index] = sort(sorted_x);
        sorted_y = sorted_y(sort_index);
        
        patches = cell(1, num_options);
        patches_confidence = cell(1, num_options);
        for j = 1:num_options
            y_start = max([1, sorted_y(j) - radius - margin]);
            y_end = min([size(rotated_original, 1), sorted_y(j) + radius + margin]);
            x_start = max([1, sorted_x(j) - radius - margin]);
            x_end = min([size(rotated_original, 2), sorted_x(j) + radius + margin]);
            patches{j} = rotated_original(y_start:y_end, x_start:x_end);
            patches_confidence{j} = calculate_mark_confidence(patches{j}, radius);            
        end
        [mark_confidence, mark_index] = max(cell2mat(patches_confidence));  % most probable among the options
        if mark_confidence >= 0.1
            digitized_data(i, mark_index) = 1;
            %disp(['chosen mark: ', num2str(mark_index)]);
        end

        %disp(digitized_data);
        if verbose
            for j = 1:num_options
                subplot(num_questions, num_options, (i-1)*num_options + j);
                imshow(patches{j});
                title(['Patch ', num2str((i-1)*num_options + j), ' Confidence: ', num2str(patches_confidence{j})]);
                if digitized_data(i, j) == 1
                    title('True');
                else
                    title('False');
                end
            end
        end
    end
    assert(isempty(x_vect) && isempty(y_vect), 'x_vect and y_vect should be empty at this point.');
end