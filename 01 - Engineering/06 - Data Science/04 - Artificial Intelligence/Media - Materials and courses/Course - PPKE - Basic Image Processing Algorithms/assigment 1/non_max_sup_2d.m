% Powered by LLMs
% Compatible with Octave 5.2
function [r_vect, t_vect] = non_max_sup_2d(A, k, p)
  % @param A: input 2d matrix
  % @param k: number of maxima points, whose neighboring regions should be suppressed
  % @param p: the radius of the region around a maximum to be suppressed
  % @return r_vect: row indices of the maxima
  % @return t_vect: column indices of the maxima
  r_vect = [];
  t_vect = [];

  while k > 0
    % Find the index of the maximum value
    [maxValue, linearIndex] = max(A(:));
    [x_n, y_n] = ind2sub(size(A), linearIndex);
    r_vect = [r_vect, x_n];
    t_vect = [t_vect, y_n];
    
    % Suppress the region around the maximum
    x_1 = max([1, x_n-p]);
    x_2 = min([size(A, 1), x_n+p]);
    y_1 = max([1, y_n-p]);
    y_2 = min([size(A, 2), y_n+p]);
    A(x_1:x_2, y_1:y_2) = 0;
    k = k - 1;
  end
end



