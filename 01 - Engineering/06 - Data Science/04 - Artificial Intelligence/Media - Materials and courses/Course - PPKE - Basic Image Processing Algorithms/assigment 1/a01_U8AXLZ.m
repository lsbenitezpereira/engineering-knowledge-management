% Powered by LLMs
% Compatible with Octave 5.2
close all;
clear;
clc;

folder = 'input';
filePattern = fullfile(folder, '*.jpg');
jpegFiles = dir(filePattern);
verbose = true;
num_questions = 4;
num_options = 2;
processed_images = 0;
digitized_data_sum = [0 0; 0 0; 0 0; 0 0];

for k = 1:length(jpegFiles)
    % Read
    baseFileName = jpegFiles(k).name;
    fullFileName = fullfile(folder, baseFileName);
    %fprintf(1, 'Now reading %s\n', fullFileName);
    
    % Process
    imageArray = imread(fullFileName);
    digitized_data = processor(imageArray, verbose);
    assert(isequal(size(digitized_data), [num_questions, num_options]), 'Invalid digitized data size.');
    assert(all(sum(digitized_data ~= 0, 2) <= 1), 'Each question must have at most one answer.');
    digitized_data_sum = digitized_data_sum + digitized_data;
    processed_images = processed_images + 1;
    input('Press Enter to continue to the next image...');
    close all;
end

% Summarize
fprintf('----------------------------------------\n');
fprintf('Processed %d images.\n', processed_images);
for i = 1:num_questions
    fprintf('Question %d: A=%d, B=%d\n', i, digitized_data_sum(i, 1), digitized_data_sum(i, 2));
end
