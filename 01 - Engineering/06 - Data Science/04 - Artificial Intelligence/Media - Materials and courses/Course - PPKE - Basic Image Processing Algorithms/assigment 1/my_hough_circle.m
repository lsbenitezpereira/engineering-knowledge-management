% Powered by LLMs
% Compatible with Octave 5.2
function [H] = my_hough_circle(input_img, radius)
  assert(all(input_img(:) == 0 | input_img(:) == 1), 'Input image must contain only 0 and 1 values');
  
  [dim_x, dim_y] = size(input_img);  % Image dimensions
  
  % Accumulator for circle centers (a, b)
  H = zeros(dim_x, dim_y);
  
  % Find all non-zero (edge) pixels in the image
  [non_zero_x, non_zero_y] = find(input_img == 1);
  
  % Precompute cos and sin values for theta = 1:360
  theta = 1:360;
  cos_theta = cosd(theta);
  sin_theta = sind(theta);
  
  % For every non-zero pixel...
  for idx = 1:length(non_zero_x)
    %fprintf('Processing pixel %d/%d\n', idx, length(non_zero_x));
    i = non_zero_x(idx);
    j = non_zero_y(idx);
    
    % Compute the corresponding (a, b) for all thetas in one go
    a = round(i - radius * cos_theta);
    b = round(j - radius * sin_theta);
    
    % Ensure (a, b) are within image bounds
    valid_indices = (a > 0 & a <= dim_x & b > 0 & b <= dim_y);
    a = a(valid_indices);
    b = b(valid_indices);
    
    % Accumulate votes
    for k = 1:length(a)
      H(a(k), b(k)) = H(a(k), b(k)) + 1;
    end
  end
end
