function confidence = calculate_mark_confidence(image, radius)
    [rows, cols] = size(image);
    if size(image, 3) == 3
        image = rgb2gray(image);
    end

    [X, Y] = meshgrid(1:cols, 1:rows);
    distances = sqrt((X - cols/2).^2 + (Y - rows/2).^2);
    image(distances > radius*0.7) = 255; % Set to white
    confidence = 0;
    %%%%%%%%%%%
    image_processed = image;
    image_processed = imsharpen(image_processed, 'Radius', 15, 'Amount', 1.5);
    if min(image_processed(:)) > 254
        return;
    end
    image_processed = edge(image_processed, 'Canny', [0.4 0.5]);

    % keep just the two biggest connected components
    CC = bwconncomp(image_processed);
    numPixels = cellfun(@numel, CC.PixelIdxList);
    [~, idx] = sort(numPixels, 'descend');
    if length(idx) > 2
        for i = 3:length(idx)
            image_processed(CC.PixelIdxList{idx(i)}) = 0;
        end
    end


    [H, theta, rho] = hough(image_processed);
    %%%%%%%%%figure; title('Hough');  imshow(uint8(255*H/max(max(H))));

    P = houghpeaks(H, 4, 'Threshold', ceil(0.55 * max(H(:))));
    lines = houghlines(image_processed, theta, rho, P);
    %fprintf('Number of peaks: %d\n', length(lines));

    if length(lines) >= 2
        % if there are at least two lines above the confidence threshold
        confidence = confidence + 0.3;

        % if any 2 lines form an angle between 80 and 100 degrees, add 0.5 to the confidence
        for i = 1:length(lines)
            for j = i+1:length(lines)
                angle = abs(lines(i).theta - lines(j).theta);
                if angle > 80 && angle < 100 && confidence < 0.5
                    confidence = confidence + 0.5;
                    %fprintf('Right angle: %d\n', angle);
                end
            end
        end
    end

    %10% desempate
    average_brightness = mean(image_processed(:));
    if average_brightness > 0.015 && average_brightness < 0.03 &&  mean(image(:)) > 243
        %fprintf('Right brightness\n');
        confidence = confidence + 0.15;
    end

    % basicamente só pra desempate
    % 10% of the confidence is given by average brightness
    % higher brighness (more edges) -> higher confidence
    confidence = confidence + 0.05 * average_brightness;

    %figure;
    %subplot(1, 3, 1);
    %imshow(image);
    %title('Original Image');
%
    %subplot(1, 3, 2);
    %imshow(image_processed);
    %title('Processed Image');
%
    %subplot(1, 3, 3);
    %imshow(image_processed);
    %hold on;
    %for k = 1:length(lines)
    %    xy = [lines(k).point1; lines(k).point2];
    %    plot(xy(:,1), xy(:,2), 'LineWidth', 2, 'Color', 'green');
    %end
    %title(['Processed Image with Hough Lines. Brightness: ', num2str(average_brightness)]);
    %hold off;
%
    %fprintf('Confidence: %f\n', confidence);
    %fprintf('Average brightness: %f\n', average_brightness);
    %fprintf('------------------\n');
    %

  
end