% Powered by ChatGPT
function D = ord_dit_matrix(n)
    Dn = [0];
    for i = 1:n
        Dn = [4*Dn+0, 4*Dn+2; 4*Dn+3, 4*Dn+1];
    end
    D = Dn/(4^n);
end