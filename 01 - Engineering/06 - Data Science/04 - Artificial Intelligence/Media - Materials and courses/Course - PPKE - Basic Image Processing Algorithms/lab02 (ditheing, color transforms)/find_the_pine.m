% Powered by ChatGPT
function pine = find_the_pine(I)
  I_hsv = rgb2hsv(I);
  pine = I_hsv(:,:, 1)<0.15;
end