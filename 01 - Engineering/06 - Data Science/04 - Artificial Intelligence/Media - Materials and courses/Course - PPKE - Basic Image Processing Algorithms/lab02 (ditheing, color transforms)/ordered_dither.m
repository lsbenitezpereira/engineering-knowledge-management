% Powered by ChatGPT
function B = ordered_dither(I, D)
    % replicate D to create a matrix which has the same size as I.
    [mI, nI] = size(I);
    [mD, nD] = size(D);
    map = repmat(D, ceil(mI / mD), ceil(nI / nD));
    map = map(1:mI, 1:nI);
    
    % Threshold
    B = I>map;
end
