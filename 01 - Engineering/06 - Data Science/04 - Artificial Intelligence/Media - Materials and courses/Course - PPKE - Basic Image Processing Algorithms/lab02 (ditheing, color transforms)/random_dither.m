
function B = random_dither(A)
    threshold = 0.75;
    noise = rand(size(A))*threshold;
    output = A + noise;
    B = output > threshold;
end
