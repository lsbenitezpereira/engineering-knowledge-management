% Powered by LLMs
function output_img = myconv(input_img, kernel)
  % Assumes the kernel size is odd
  radius_x = floor(size(kernel, 1)/2);
  radius_y = floor(size(kernel, 2)/2);
  
  input_img_padded = padarray(input_img, [radius_x, radius_y], 0);
  kernel_rotated = rot90(kernel, 2);
  output_img = zeros(size(input_img));
  for i = 1:size(input_img, 1)
    for j = 1:size(input_img, 2)
      output_img(i, j) = sum(sum(kernel_rotated .* input_img_padded(i:i+2*radius_x, j:j+2*radius_y)));
    end
  end    
end
