% Powered by LLMs
function output_img = stretch_log(input_img, c)
  input_img = double(input_img); % Ensure it's in double format for transformation
  input_img = input_img - min(input_img(:)); % Shift to make the minimum value 0
  input_img = input_img / max(input_img(:)); % Normalize to the range [0, 1]
  input_img_trans = c*log(input_img+1);
  output_img = stretch_lin(input_img_trans);
end
