% Powered by LLMs
function output_img = stretch_lin(input_img)
  input_img = double(input_img);
  vmin = min(min(input_img));
  vmax = max(max(input_img));
  
  % Takes about 2.8s
  %tic;
  %output_img = arrayfun(@(pixel) (
  %  uint8(floor(255/(vmax-vmin)*(pixel-vmin)))
  %), input_img);
  %toc;
  
  % takes about 0.003s
  %tic;
  output_img = uint8(floor(255/(vmax-vmin)*(input_img-vmin)));
  %toc;
end
