% Powered by LLMs
function hist_vector = calc_hist_vector(input_img)
  % Assumes the pixel values are grayscale and uint8
  hist_vector = zeros(1, 256);
  for i = 1:size(input_img, 1)
    for j = 1:size(input_img, 2)
      value = input_img(i, j);
      hist_vector(value+1) = hist_vector(value+1) + 1;
    end
  end
  assert(sum(hist_vector), size(input_img, 1)*size(input_img, 2));
end
