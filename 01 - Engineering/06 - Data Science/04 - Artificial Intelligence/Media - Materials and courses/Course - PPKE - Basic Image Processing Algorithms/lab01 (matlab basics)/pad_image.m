function [PAD] = pad_image(varargin)
  if (length(varargin) == 0)
    error('First argument is mandatory')
  end
  IMG = varargin{1};
  if (length(varargin) >= 2)
    border_size = varargin{2};
  else
    border_size = 10;
  end
  % display(border_size);

  if (ndims(IMG) == 3)
    warning('Expected grayscale image. Converting...');
    IMG = rgb2gray(IMG);
  end

  [rows, cols] = size(IMG);
  PAD = zeros([border_size + rows + border_size, border_size + cols + border_size], 'uint8');
  PAD(border_size + 1:end - border_size, border_size + 1:end - border_size) = IMG;
end