clear; close all;
I = imread('input/AlfredoBorba_TuscanLandscape.jpg');
imshow(I)
if (ndims(I) == 3)
  I = rgb2gray(I);
end
imshow(I)
imwrite(I, 'output/AlfredoBorba_TuscanLandscape_GRAY.jpg')