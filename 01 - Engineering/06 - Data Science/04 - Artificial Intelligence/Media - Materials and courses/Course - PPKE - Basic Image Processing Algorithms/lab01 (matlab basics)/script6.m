clear; close all;
IMG = imread('input/AlfredoBorba_TuscanLandscape.jpg');
IMG = rgb2gray(IMG);

row = 150;
figure(1);
subplot(211); imshow(IMG); hold on; plot([1 size(IMG, 2)], [row row], 'r', 'LineWidth', 2); hold off;
subplot(212); plot(IMG(row, :), 'r'); xlim([1, size(IMG2)]);