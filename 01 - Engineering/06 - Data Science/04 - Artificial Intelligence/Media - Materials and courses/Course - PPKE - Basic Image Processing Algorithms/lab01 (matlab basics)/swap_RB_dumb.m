function [BGR] = swap_RB_dumb(RGB)
  BGR = zeros(size(RGB), 'uint8');
  [rows, cols, channels] = size(RGB);
  for i = 1:rows
      for j = 1:cols
        BGR(i, j, 1) = RGB(i, j, 3);
        BGR(i, j, 2) = RGB(i, j, 2);
        BGR(i, j, 3) = RGB(i, j, 1);
      end
  end
end