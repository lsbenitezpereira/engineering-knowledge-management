function [TH] = mythreshold(IMG, level)
  if (ndims(IMG) == 3)
    warning('Expected grayscale image. Converting...');
    IMG = rgb2gray(IMG);
  end
  TH = uint8(255*(IMG > level));;
end