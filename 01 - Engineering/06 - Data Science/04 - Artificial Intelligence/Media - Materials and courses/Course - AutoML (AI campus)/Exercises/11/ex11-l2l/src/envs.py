import numpy as np
from matplotlib import pyplot as plt
import seaborn as sb
import logging

from gym import spaces, Env

sb.set_style('darkgrid')


class Sigmoid(Env):
    """
    Sigmoid reward with multiple actions
    """

    def _sig(self, x):
        """ Simple sigmoid """
        return 1 / (1 + np.exp(-self.slope * (x - self.shift)))

    def __init__(self,
                 n_steps: int = 10,
                 n_actions: int = 3,
                 seed: int = 0,
                 noise: bool = False,
                 render: bool = False) -> None:
        super().__init__()
        self.n_steps = n_steps
        self.action_space = spaces.Discrete(n_actions)
        self.rng = np.random.RandomState(seed)
        self._c_step = 0
        self.slope = 1
        self.shift = n_steps / 2
        self.reward_range = (0, 1)
        self._c_step = 0
        self.noise = noise
        self.observation_space = spaces.Box(low=np.array([0, self.n_steps / 2 - 5 * self.n_steps / 2,
                                                          -2, -1]),
                                            high=np.array([self.n_steps, self.n_steps / 2 + 5 * self.n_steps / 2,
                                                           2, n_actions]))
        self.logger = logging.getLogger(self.__str__())
        self._prev_state = None
        self._prev_act = None
        self.render_ = render
        if self.render_:
            plt.ion()
        self.fig = None
        self.ax = None
        self.scatter = None
        self.legend = None

    def _get_reward(self, action):
        raise NotImplementedError

    def step(self, action: int):
        """
        Advance on step forward
        :param action: int
        :return: next state, reward, done, misc
        """
        r = self._get_reward(action)
        prev_state = self._prev_state
        next_state = self.compute_state()

        self.logger.debug("i: (s, a, r, s') / %d: (%s, %d, %5.2f, %2s)", self._c_step - 1, str(prev_state),
                          action, r, str(next_state))
        self._c_step += 1
        self._prev_state = next_state
        self._prev_act = action
        return tuple(np.array(next_state, dtype=int)), r, self._c_step > self.n_steps, {}

    def compute_state(self):
        remaining_budget = self.n_steps - self._c_step
        next_state = [remaining_budget]
        return next_state

    def _reset_render_plot(self):
        plt.cla()
        plt.close('all')
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        x = np.linspace(0, self.n_steps, 100)
        line, = self.ax.plot(x, self._sig(x), label='ground truth')
        self.fig.canvas.draw()
        self.scatter = self.ax.scatter([], [], c='k', marker='x', s=10, label=r'$\pi(s_t)$')
        self.legend = plt.legend()
        plt.pause(.2)

    def reset(self, prepare_plot: bool = False):
        """
        Reset the environment.
        Always needed before starting a new episode.
        :return: Start state
        """
        if self.noise:
            self.shift = self.rng.normal(self.n_steps / 2, self.n_steps / 4)
            self.slope = self.rng.choice([-1, 1]) * self.rng.uniform() * 2  # negative slope
        self._c_step = 0
        self._prev_state = None
        self._prev_act = None
        next_state = self.compute_state()
        self.logger.debug("i: (s, a, r, s') / %d: (%2d, %d, %5.2f, %2d)", -1, -1, -1, -1, -1)

        # For rendering purposes only
        if prepare_plot and self.render_:
            self._reset_render_plot()
        return tuple(np.array(next_state, dtype=int))

    def render(self):
        if not self.render_:
            return
        if self._prev_state:
            self.scatter.set_offsets(np.c_[self._c_step, self._prev_act / (self.action_space.n - 1)])
            self.legend.get_texts()[1].set_text(r'$\pi(s_{' + str(self._c_step) + '})$')
        self.fig.canvas.draw()
        plt.pause(.2)


class SigmoidChangingSlope(Sigmoid):
    """
    Sigmoid reward with multiple actions
    """

    def compute_state(self):
        # TODO
        raise NotImplementedError

    def reset(self, prepare_plot: bool = False):
        """
        Reset the environment.
        Always needed before starting a new episode.
        :return: Start state
        """
        # TODO adjust the slope
        self.slope = None
        self._c_step = 0
        next_state = self.compute_state()
        self._prev_act = None
        self._prev_state = None
        self.logger.debug("i: (s, a, r, s') / %d: (%2d, %d, %5.2f, %2d)", -1, -1, -1, -1, -1)

        # For rendering purposes only
        if prepare_plot and self.render_:
            self._reset_render_plot()
        return tuple(np.array(next_state, dtype=int))


class SigmoidChangingSlopeAndInflection(Sigmoid):
    """
    Sigmoid reward with multiple actions
    """

    def compute_state(self):
        raise NotImplementedError

    def reset(self, prepare_plot: bool = False):
        """
        Reset the environment.
        Always needed before starting a new episode.
        :return: Start state
        """
        # TODO adjust slope and shift
        self.slope = None
        self.shift = None
        self._c_step = 0
        next_state = self.compute_state()
        self._prev_act = None
        self._prev_state = None
        self.logger.debug("i: (s, a, r, s') / %d: (%2d, %d, %5.2f, %2d)", -1, -1, -1, -1, -1)

        # For rendering purposes only
        if prepare_plot and self.render_:
            self._reset_render_plot()
        return tuple(np.array(next_state, dtype=int))
