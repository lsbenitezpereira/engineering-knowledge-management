import unittest
from collections import defaultdict
import logging
import numpy as np

from src.main import q_learning
from src.envs import Sigmoid, SigmoidChangingSlope, SigmoidChangingSlopeAndInflection


class TestQ(unittest.TestCase):

    def setUp(self):  # This Method is executed once before each test
        logging.basicConfig(level=logging.DEBUG)

    def test_q_learning_simple_state(self):
        env = Sigmoid(render=False)
        q_func, train_stats, test_stats = q_learning(env,
                                                     1000,
                                                     discount_factor=.9,
                                                     alpha=.5,
                                                     epsilon=1.,
                                                     epsilon_decay='linear',
                                                     decay_starts=0,
                                                     eval_every=999999)

        actions = []
        current_state = env.reset()
        done = False
        while not done:
            q_vals = q_func[current_state]
            action = np.argmax(q_vals)  # type: int
            actions.append(action)
            current_state, _, done, _ = env.step(action)
        for i in range(1, len(actions)):
            self.assertTrue(actions[i] >= actions[i - 1])

    def test_q_learning_slope_change(self):
        env = SigmoidChangingSlope(render=False, seed=42)
        q_func, train_stats, test_stats = q_learning(env,
                                                     250,
                                                     discount_factor=.9,
                                                     alpha=.5,
                                                     epsilon=1.,
                                                     epsilon_decay='linear',
                                                     decay_starts=0,
                                                     eval_every=999999)
        optimal_pol_acts = [0, 0, 0, 0,
                            1, 1, 1,
                            2, 2, 2, 2]
        episodes_actions = []
        episodes_slopes = []
        for i in range(20):  # We can quickly check for the optimal policies here
            actions = []
            current_state = env.reset()
            done = False
            while not done:
                q_vals = q_func[current_state]
                action = np.argmax(q_vals)  # type: int
                actions.append(action)
                current_state, _, done, _ = env.step(action)
            episodes_actions.append(actions)
            episodes_slopes.append(env.slope)
            if episodes_slopes[-1] == 1:
                self.assertListEqual(optimal_pol_acts, actions)
            elif episodes_actions[-1] == -1:
                self.assertListEqual(optimal_pol_acts[::-1], actions)
            self.assertTrue(episodes_slopes[-1] in [-1, 1])

    def test_q_learning_slope_change_and_inflection(self):
        env = SigmoidChangingSlopeAndInflection(render=False, seed=42)
        q_func, train_stats, test_stats = q_learning(env,
                                                     5000,
                                                     discount_factor=.9,
                                                     alpha=.5,
                                                     epsilon=1.,
                                                     epsilon_decay='linear',
                                                     decay_starts=0,
                                                     eval_every=999999)
        for i in range(20):
            prev_action = -1
            not_allowed = []
            actions = []
            current_state = env.reset()
            done = False
            while not done:
                q_vals = q_func[current_state]
                action = np.argmax(q_vals)  # type: int

                # simply check that we are either strictly increasing or strictly
                # decreasing by never playing actions again after switching to a new action
                self.assertTrue(action not in not_allowed)
                if action != prev_action:
                    not_allowed.append(prev_action)
                    prev_action = action
                actions.append(action)
                current_state, _, done, _ = env.step(action)


if __name__ == '__main__':
    unittest.main()
