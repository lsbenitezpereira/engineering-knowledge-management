import unittest
from collections import defaultdict
import logging
import numpy as np

from src.main import td_update


class TestTD(unittest.TestCase):

    def setUp(self):  # This Method is executed once before each test
        logging.basicConfig(level=logging.DEBUG)

    def test_td(self):
        q = defaultdict(lambda: np.zeros(2))
        # simple test if full value gets propagated with gamma=1 and alpha = 1
        update_val = td_update(q, 0, 0, 10, 1, 1., 1)
        q[0][0] = update_val
        self.assertEqual(10, update_val)

        # tests if gamma and learning rate properly influence update
        update_val = td_update(q, 1, 1, 10, 0, .9, .5)
        q[1][1] = update_val
        self.assertEqual(9.5, update_val)
        update_val = td_update(q, 0, 0, 10, 1, .9, .5)
        q[0][0] = update_val
        self.assertEqual(14.275, update_val)

        # test if alpha=0 stops learning
        update_val = td_update(q, 1, 1, 10, 0, .9, 0)
        q[1][1] = update_val
        self.assertEqual(9.5, update_val)

        # test if gamma=0 allows for continued learning without expected future rewards
        update_val = td_update(q, 1, 1, 10, 0, 0., .5)
        q[1][1] = update_val
        self.assertEqual(9.75, update_val)


if __name__ == '__main__':
    unittest.main()
