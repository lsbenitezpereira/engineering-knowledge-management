import argparse
import logging
from collections import defaultdict
import numpy as np
import datetime
import pickle
import os
from tqdm import tqdm


from src.envs import Sigmoid, SigmoidChangingSlope, SigmoidChangingSlopeAndInflection


def make_epsilon_greedy_policy(Q: defaultdict, epsilon: float, nA: int) -> callable:
    """
    Creates an epsilon-greedy policy based on a given Q-function and epsilon.
    I.e. create weight vector from which actions get sampled.

    :param Q: tabular state-action lookup function
    :param epsilon: exploration factor
    :param nA: size of action space to consider for this policy
    """

    def policy_fn(observation):
        policy = np.ones(nA) * epsilon / nA
        best_action = np.random.choice(np.argwhere(  # random choice for tie-breaking only
            Q[observation] == np.amax(Q[observation])
        ).flatten())
        policy[best_action] += (1 - epsilon)
        return policy

    return policy_fn


def get_decay_schedule(start_val: float, decay_start: int, num_episodes: int, type_: str):
    """
    Create epsilon decay schedule

    :param start_val: Start decay from this value (i.e. 1)
    :param decay_start: number of iterations to start epsilon decay after
    :param num_episodes: Total number of episodes to decay over
    :param type_: Which strategy to use. Implemented choices: 'const', 'log', 'linear'
    :return:
    """
    if type_ == 'const':
        return np.array([start_val for _ in range(num_episodes)])
    elif type_ == 'log':
        return np.hstack([[start_val for _ in range(decay_start)],
                          np.logspace(np.log10(start_val), np.log10(0.000001), (num_episodes - decay_start))])
    elif type_ == 'linear':
        return np.hstack([[start_val for _ in range(decay_start)],
                          np.linspace(start_val, 0, (num_episodes - decay_start))])
    else:
        raise NotImplementedError


def td_update(q: defaultdict, state: list, action: int, reward: float, next_state: int, gamma: float, alpha: float):
    """
    Simple TD update

    :param q: Q-function given as default dict. Maps from state to action values
    :param state: list of integer values that numerically describe the current state of the environment
    :param action: the action that was played in the current state
    :param reward: the associated reward value for playing action in state
    :param next_state: the successor state that is visited after playing action in state
    :param gamma: the discounting factor
    :param alpha: the learning rate

    :return: The new value that can be assigned to q[state][action]
    """
    # TD update TODO
    raise NotImplementedError


def q_learning(
        environment,
        num_episodes: int,
        discount_factor: float = 1.0,
        alpha: float = 0.5,
        epsilon: float = 0.1,
        epsilon_decay: str = 'const',
        decay_starts: int = 0,
        eval_every: int = 1,
        render_eval: bool = True):
    """
    Vanilla tabular Q-learning algorithm
    :param environment: which environment to use
    :param num_episodes: number of episodes to train
    :param discount_factor: discount factor used in TD updates
    :param alpha: learning rate used in TD updates
    :param epsilon: exploration fraction (either constant or starting value for schedule)
    :param epsilon_decay: determine type of exploration (constant, linear/exponential decay schedule)
    :param decay_starts: After how many episodes epsilon decay starts
    :param eval_every: Number of episodes between evaluations
    :param render_eval: Flag to activate/deactivate rendering of evaluation runs
    :return: training and evaluation statistics (i.e. rewards and episode lengths)
    """
    assert 0 <= discount_factor <= 1, 'Lambda should be in [0, 1]'
    assert 0 <= epsilon <= 1, 'epsilon has to be in [0, 1]'
    assert alpha > 0, 'Learning rate has to be positive'
    # The action-value function.
    # dict that maps state -> (action -> action-value).
    Q = defaultdict(lambda: np.zeros(environment.action_space.n))

    # Keeps track of episode lengths and rewards
    rewards = []
    lens = []
    test_rewards = []
    test_lens = []

    epsilon_schedule = get_decay_schedule(epsilon, decay_starts, num_episodes, epsilon_decay)
    for i_episode in tqdm(range(num_episodes + 1)):
        # print('#' * 100)
        epsilon = epsilon_schedule[min(i_episode, num_episodes - 1)]
        # The policy we're following
        policy = make_epsilon_greedy_policy(Q, epsilon, environment.action_space.n)
        policy_state = environment.reset()
        episode_length, cummulative_reward = 0, 0
        while True:  # roll out episode
            policy_action = np.random.choice(list(range(environment.action_space.n)), p=policy(policy_state))
            s_, policy_reward, policy_done, _ = environment.step(policy_action)
            cummulative_reward += policy_reward
            episode_length += 1

            Q[policy_state][policy_action] = td_update(Q, policy_state, policy_action,
                                                       policy_reward, s_, discount_factor, alpha)

            if policy_done:
                break
            policy_state = s_
        rewards.append(cummulative_reward)
        lens.append(episode_length)

        # evaluation with greedy policy
        test_steps = 0
        if i_episode % eval_every == 0:
            policy_state = environment.reset(prepare_plot=True)
            episode_length, cummulative_reward = 0, 0
            if render_eval:
                environment.render()
            while True:  # roll out episode
                policy_action = np.random.choice(np.flatnonzero(Q[policy_state] == Q[policy_state].max()))
                # environment.total_steps -= 1  # don't count evaluation steps
                s_, policy_reward, policy_done, _ = environment.step(policy_action)
                test_steps += 1
                if render_eval:
                    environment.render()
                s_ = s_
                cummulative_reward += policy_reward
                episode_length += 1
                if policy_done:
                    break
                policy_state = s_
            test_rewards.append(cummulative_reward)
            test_lens.append(episode_length)
            test_steps_list.append(test_steps)
            # print('Done %4d/%4d episodes' % (i_episode, num_episodes))
    return Q, (rewards, lens), (test_rewards, test_lens)


def zeroOne(stringput):
    """
    Helper to keep input arguments in [0, 1]
    """
    val = float(stringput)
    if val < 0 or val > 1.:
        raise argparse.ArgumentTypeError("%r is not in [0, 1]", stringput)
    return val


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Tabular Q-learning example')
    parser.add_argument('-n', '--n-eps', dest='neps',
                        default=10000,
                        help='Number of episodes to roll out.',
                        type=int)
    parser.add_argument('--epsilon_decay',
                        choices=['linear', 'log', 'const'],
                        default='const',
                        help='How to decay epsilon from the given starting point to 0 or constant')
    parser.add_argument('--decay_starts',
                        type=int,
                        default=0,
                        help='How long to keep epsilon constant before decaying. '
                             'Only active if epsilon_decay log or linear')
    parser.add_argument('-r', '--repetitions',
                        default=1,
                        help='Number of repeated learning runs.',
                        type=int)
    parser.add_argument('-d', '--discount-factor', dest='df',
                        default=.99,
                        help='Discount factor',
                        type=zeroOne)
    parser.add_argument('--exercise', choices=['A', 'B', 'C'], default='A', type=str.upper, help='Which state to use')
    parser.add_argument('--render', action='store_true', help='Render env or not')
    parser.add_argument('--eval-every', type=int, default=10, help='Run one evaluation episode after this many training'
                                                                   ' episodes')
    parser.add_argument('-l', '--learning-rate', dest='lr',
                        default=.125,
                        help='Learning rate',
                        type=float)
    parser.add_argument('-e', '--epsilon',
                        default=0.01,
                        help='Epsilon for the epsilon-greedy method to follow',
                        type=zeroOne)
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Use debug output')
    parser.add_argument('-s', '--seed',
                        default=0,
                        type=int)

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    np.random.seed(args.seed)
    q_func = None
    # tabular Q
    ds = datetime.date.strftime(datetime.datetime.now(), "%Y_%m_%d_%H_%M_%S_%f")
    random_agent = False
    folder = 'tabular_' + ds
    if not os.path.exists(folder):
        os.mkdir(folder)
        os.chdir(folder)
    for r in range(args.repetitions):
        logging.info('Round %d/%d', r + 1, args.repetitions)
        if args.exercise == 'C':
            env = SigmoidChangingSlopeAndInflection(render=args.render)
        elif args.exercise == 'B':
            env = SigmoidChangingSlope(render=args.render)
        else:
            env = Sigmoid(render=args.render)
        q_func, train_stats, test_stats = q_learning(env,
                                                     args.neps,
                                                     discount_factor=args.df,
                                                     alpha=args.lr,
                                                     epsilon=args.epsilon,
                                                     epsilon_decay=args.epsilon_decay,
                                                     decay_starts=args.decay_starts,
                                                     eval_every=args.eval_every)
        fn = '%04d_%s-greedy-results-%s-%d_eps-%d_reps-seed_%d.pkl' % (r, str(args.epsilon), 'sig', args.neps,
                                                                       args.repetitions, args.seed)
        with open(fn.replace('results', 'q_func'), 'wb') as fh:  # TODO modify s.t. your Q function can be stored
            pickle.dump(dict(q_func), fh)
        with open(fn.replace('results', 'trueReward'), 'wb') as fh:
            pickle.dump(train_stats[0], fh)

        # Some post training Q-examples
        if r == args.repetitions - 1:
            logging.info('Example Q-function following greedy-policy:')
            for i in range(10):  # More examples only necessary for noisy environment
                print('#' * 120)
                current_state = env.reset()
                done = False
                while not done:
                    q_vals = q_func[current_state]
                    action = np.argmax(q_vals)  # type: int
                    print(current_state, q_vals, action)
                    current_state, _, done, _ = env.step(action)
