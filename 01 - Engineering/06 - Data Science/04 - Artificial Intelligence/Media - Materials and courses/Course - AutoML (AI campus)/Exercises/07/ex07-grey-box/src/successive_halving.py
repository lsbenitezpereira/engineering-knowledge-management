import numpy as np

import sys
from os.path import dirname, abspath
sys.path.append(dirname(dirname(abspath(__file__))))

from src.data.fcnet_benchmark import FCNetProteinStructureBenchmark  # noqa: E402
from src.utils import plot_grey_box_optimization  # noqa: E402


def successive_halving(problem, n_models, min_budget_per_model, max_budget_per_model, eta, random_seed):
    """
    The successive halving algorithm, called as subroutine in hyperband.
    :param problem: An instance of problem
    :param n_models: int;  The number of configs to evaluate
    :param min_budget_per_model: int
    :param max_budget_per_model: int
    :param eta: float
    :param random_seed: int
    :return:
    """
    np.random.seed(random_seed)
    configs_dict = {i: {'config': problem.get_configuration_space().sample_configuration(),
                        'f_evals': {}} for i in range(n_models)}

    configs_to_eval = list(range(n_models))
    b = np.int(min_budget_per_model)
    while b <= max_budget_per_model:
        for config_id in configs_to_eval:
            configs_dict[config_id]['f_evals'][b] = problem.objective_function(configs_dict[config_id]['config'],
                                                                               budget=b)
        # Number of configs to proceed
        num_configs_to_proceed = np.floor(len(configs_to_eval) / eta).astype(np.int)

        eval_configs_curr_budget = dict(
            filter(lambda config_id_dict: config_id_dict[0] in configs_to_eval, configs_dict.items()))

        # Select the config to evaluate on the higher budget.
        configs_to_eval = list(
            dict(sorted(eval_configs_curr_budget.items(),
                        key=lambda config_id_dict: config_id_dict[1]['f_evals'][b][0])).keys())[:num_configs_to_proceed]
        b = np.round(b * eta).astype(np.int)
    return configs_dict


if __name__ == '__main__':
    problem = FCNetProteinStructureBenchmark(data_dir="src/data/fcnet_tabular_benchmarks/")
    configs_dict = successive_halving(problem=problem, n_models=40, eta=2, random_seed=0, max_budget_per_model=100,
                                      min_budget_per_model=10)
    plot_grey_box_optimization([configs_dict], min_budget_per_model=10)
