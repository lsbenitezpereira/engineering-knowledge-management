function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
n = size(X)(2)
% You need to return the following variables correctly 

y_hat = arrayfun(@(z) 1/(1+e^(-z)), [X*theta])
J = (1/m)*sum([-y.*log(y_hat)-(1-y).*log(1-y_hat)]) + lambda/(2*m)*sum([theta(2:n).^2]);
grad = (1/m)*X'*[y_hat-y] + [0; lambda/m*theta(2:n)]


% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta






% =============================================================

end
