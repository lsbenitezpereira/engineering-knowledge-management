% Gradient Direction
1;
function result = select_gdir(gmag, gdir, mag_min, angle_low, angle_high)
    % TODO Find and return pixels that fall within the desired mag and angle range
    [h, w] = size(gmag);
    result = zeros(h,w);
    % Thi can be done using element-wise operators, you dump fucker
    for i=[1:h]
      for j=[1:w]
        if gmag(i,j)>mag_min && gdir(i,j)>angle_low && gdir(i,j)<angle_high
          result(i,j)=1;
        endif  
      endfor
    endfor  
endfunction

pkg load image;

%% Load and convert image to double type, range [0, 1] for convenience
img = double(imread('bird_small.png')) / 255.; 
img = img(:,:,1); %if the image is color
imshow(img); % assumes [0, 1] range for double images

%% Compute x, y gradients
[gx gy] = imgradientxy(img, 'sobel'); % Note: gx, gy are not normalized

%% Obtain gradient magnitude and direction
[gmag gdir] = imgradient(gx, gy);
imshow(gmag / (4 * sqrt(2))); % mag = sqrt(gx^2 + gy^2), so [0, (4 * sqrt(2))]
imshow((gdir + 180.0) / 360.0); % angle in degrees [-180, 180]

%% Find pixels with desired gradient direction
my_grad = select_gdir(gmag, gdir, 1, 30, 60); % 45 +/- 15
%imshow(my_grad);  % NOTE: enable after you've implemented select_gdir
