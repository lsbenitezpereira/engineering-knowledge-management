% Find template 1D
% NOTE: Function definition must be the very first piece of code here!
function index = find_template_1D(t, s)
  n = size(t)(2);
  g = [];
  for x=1:(size(s)(2)-n+1)
    g(x) = dot(t, s(x:(x+n-1))); % I forget to normalize
  endfor
  % or it can manually be done with normxcorr2(t,s)
  [~, index] = max(g)
endfunction

pkg load image; % AFTER function definition

% Test code:
s = [-1 0 0 1 1 1 0 -1 -1 0 1 0 0 -1];
t = [1 1 0];
%disp('Signal:'), disp([1:size(s, 2); s]);
%disp('Template:'), disp([1:size(t, 2); t]);

index = find_template_1D(t, s);
disp('Index:'), disp(index);