[TOC]

# Genetic algorthms

* stochastic hill-climbing search in which a large population of states is maintained

* a type of evolutionary algorithms (registered separatly because the size and importance)

* combine an uphill tendency with random exploration and exchange of information among parallel search threads

* “survival of the fittest”

* The solution is Fitness-Oriented

* Can be used with other IA algorithms 

* Aplica variações aleatórias às soluções do problema, selecionando algumas (cuja probabilidade de escolha é proporcional à sua adequação como solução) e aplicando novamente as variações.

* **Utilization**

  * Seach in very big spaces, so we decide do to randon choises and “big jumps” to find a good solution
  * work better if adjacent bits are related (because the crossover will not just be aleatory)
  * (+) awnsers with high diversity
  * (-) Usually slower to converge
  * (-) not suitable for expensive problems (like training large neural networks)

* **Adaptatives GAs**

  * the values of crossover (pc) and mutation (pm) decrese with time (like simulated anealing)
  * higher in the begining, lower in the end
  * improves the convergence 

* **How it works**

  * the “genes” (characteristics of the population) must be parametrized into a vector of boleans/integers/etcs
  * each individual is rated by the fitness function
  * the probability of being chosen for reproducing is directly proportional to the
    fitness score (==whats the difference?==)
  * For each pair to be mated, a crossover point is chosen randomly from the positions in the string (the crossover is done in blocks, thus raising the level of granularity at which the search operates)
  * each location is subject to random mutation with a small independent
    probability

  ![1550357044590](Artificial%20Intelligence%20-%20Images/1550357044590.png)

  ![1550541642725](Artificial%20Intelligence%20-%20Images/1550541642725.png)

* **Aging mechanism**

  * Discarding the oldest individual

	* works as regularization
	
* **Variations**

  * genetic programming: The programs are represented in the form of expression trees

  

# GA and NN

## To design NN

See more in *AutoML - Neural Architecture Search*

change number of layers, etc

https://blog.coast.ai/lets-evolve-a-neural-network-with-a-genetic-algorithm-code-included-8809bece164

## To train

alternative to backpropagation

A generation of networks is created with random weights and biases

http://www2.cs.uregina.ca/~dbd/cs831/notes/neural-networks/neuroevolution/

https://www.youtube.com/watch?v=r8KWciNmEGw&fbclid=IwAR3Jk32esCvVz41uf88BO-dRMNun2G3uNgzVljJ-ghwIe766GCS6iETzFWI