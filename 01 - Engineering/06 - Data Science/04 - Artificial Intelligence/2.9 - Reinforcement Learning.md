[TOC]

# Conceituação

* Deixa o algoritmo fazendo tentativas e dá “recompesas/punições” quando ele acerta

* learn by exploration

* in its purest form, reinforcement learning requires no labeled training examples

* [Great tutorial with codes (university of Washington)](https://www.youtube.com/playlist?list=PLjy4p-07OYzsPuX4_D7ZzRe1jpEx1mZEB)

* [Another great tutorial with code (sentdex)](https://www.youtube.com/watch?v=yMk_XtIEzH8&list=PLQVvvaa0QuDezJFIOU5wDdfy4e9vdnx-7)

* [Course from DeepMind (youtube)](https://www.youtube.com/watch?v=2pWv7GOvuf0&list=PLqYmG7hTraZDM-OYHWgPebj2MfCFzFObQ) [(slides)](https://www.davidsilver.uk/teaching/)

* [OpenAI’s course](https://spinningup.openai.com/en/latest/index.html)

* (+) allows to learn by interacting with the enviorment (but is very different to learn from zero, you still need a lot of prior data)

* three aspects—sensation, action, and goal

* Fundamental assumption: goals can be described by maximization of expected cumulative results 

* RL is very general, encompassing all problems that involve making a sequence of decisions

  ![image-20201227144838235](Images - 2.9 - Reinforcement Learning/image-20201227144838235.png)

* **Comparison with other techniques**
  * If the space of policies is sufficiently small, or can be structured so that good policies are common or easy to find—or if a lot of time is available for the search—then evolutionary
    methods can be effective. In addition, evolutionary methods have advantages on problems
    in which the learning agent cannot sense the complete state of its environment.
  * Because almost all of the conventional methods [of control and dynamic programming] require complete knowledge
    of the system to be controlled, it feels a little unnatural to say that they are part of
    reinforcement learning.
  * Although one
    might be tempted to think of reinforcement learning as a kind of unsupervised learning
    because it does not rely on examples of correct behavior, reinforcement learning is trying
    to maximize a reward signal instead of trying to find hidden structure.

* **k -armed bandit problem**
  * well known learning problem	
  * nonassociative (there is no need to associate different actions with different situations)
  * purely selectional (?)
  * so named by analogy to a slot machine
  * <u>how it works</u>
    * action = push one of the k buttons (or arms)
    * each button have have an independent reward function; the function is hidden to you
    * the reward is a gaussian random variable whose mean was sampled from a gaussian distibution (mean 0, std 1)
    * the goal is to find the optimal button to push (or arm to pull)
    * in the basic problem, the dstributions are stationary
    * [great explanation](https://www.youtube.com/watch?v=e3L4VocZnnQ)
  * <u>How to evaluate</u>
    * For any learning method, we can measure its performance
      and behavior as it improves with experience over $n$ time steps when applied to one of
      the bandit problems. This makes up one run
    * $n=k^k$?? or doesnt matter?
    * repeat several runs, and you get the learning algorithm’s average behavior.
    * reget: total maximum possible reward - total obtained reward
    * zero regret strategy: an algorithhm that, as $t\rightarrow\infty$, $\frac{reget}{t} \rightarrow 0$

* **Contextual bandid problems**

  * ban one of the places?
  * have state?

* **Teaching process**

  * how to organize training to maximize it
  * example: easy simulation -> hard simulation -> fine tunning on real scenario
  * gradual feedback is easier to learn than sparse reward
  * the more complex and unpredictable the environment, the less successful are the attempts to transfer what is learned in simulation to the real world. 

* **Exploration**

  * Gather more information
  * do something we believe that is not optimal, but may end up being optimal
  * you can explore in the state space, or in the parameter space (like in policy gradent methods)
  * Simpler approach: sometimes take a random action (state space exploration)
  * Approach 2: if your expectation is probabilistic, sometimes decide the action considering the maximum possible output (optimistic choice) (state space exploration); UCB?; if you quantify the uncertainty about the rewards, you can be optimistic only to the state where you are more uncertain (thus balancing exploration and exploitation)

* 

* 

* 

* **Model full techniques**

  * if we have a model (a markov process, a ODE, etc) we can solve it
  * to MDP (see in *Statistics*), optimal solutions can be computed with dinamic programming
  * usually RL problems are defined as MDP or similar

* **Model free techniques**

  * if we dont, we must learn about the environment or map directly from state to action 

* **Model free prediction**

  * given a policy ($v_\pi$), learn what will be the reward

  * ==should I unify prediction and control, separing by MC and TD, as Barto does?==

  * <u>monte carlo learning</u>

    * only for episodic tasks
    * predict expected reward by mean on random sampled returns
    * the mean is computed incrementally (see in *Statistics*)
    * (+) dont need to explore all space, only the important states
    * high variance, low bias

  *  <u>temporal-difference learning</u>

     *  update estimates based in part on other learned estimates, without waiting for a final outcome (they bootstrap)
        
     *  minimize the difference between the current and the previous iteration’s outputs
        
     * 
     
     *  “learning a guess from a guess.” Richard Sutton
        
     * “learning a guess from a better guess.”  Mitchell

     *  like Dinamic programming techniques and bellman equation, it uses the expected reward of the next state to estimate the current expected reward
        $$
        {\displaystyle V(s)\leftarrow V(s)+\alpha (\overbrace {r+\gamma V(s')} ^{\text{The TD target}}-V(s))}
        $$
     
        > where $s$ and $s′$ are the old and new states
     
     *  learns better in environment modelled by makov processes
     
     *  (+) usually learns faster than Monte Carlo methods
     
     *  (-) very sentitive to initial values
     
     *  low variance, high bias
     
     *  variation TD-Lambda: perform a lookahead of more than one step (modifies the learnign equation)

* **Model-free control**

   *  learn a policy

   *  doesnt learn how the word works, like how the transitions happens 

   *  usually, when we refer to “learning”, whe are referring to learning a policy (thus, control)

   *  as we dont have a model, we must learn direcly to map “input -> expected reward” (Q function, action function)

   *  on policy: learn a policy, following that policy; “Learn by doing”

   *  off policy: learn a policy, following another policy; “Learn by looking”

   *  the expected reward 

      ![image-20210103150107721](Images - 2.9 - Reinforcement Learning/image-20210103150107721.png)

   *  <u>monte carlo learning</u>

      *  ?
      *  GLIE?

   *  <u>temporal-difference learning</u>

      *  ?
      

* **Conventions**

  * random variables, denoted with capital letters, and their instantiations, denoted in lower case
  * lower case for value functions (e.g., v_\pi ) and restrict capitals to their tabular estimates (e.g., Qt (s, a))

## Fundamentals

* The agent choose actions that maximize the expected reward

* Reward are given by what actions the agent perform in the environment

* 

* <img src="Images - 2.0 - Machine Learning/1567533493969.png" alt="1567533493969" style="zoom: 80%;" />

  > [Fonte](introtodeeplearning.com)

  ![image-20201225224721449](Images - 2.9 - Reinforcement Learning/image-20201225224721449.png)

  > [Fonte](https://www.youtube.com/watch?v=GOsUHlr4DKE)

* Usually, It is possible to visualize the game your agent is playing

* **Agent**

  * entity in the environment
  * model free agents: maps directly, without having an explicit model of the envorinment
  * may or not have a model of the environment
  * may or not have a transition function (predict the next state)
  * policy: agent’s way of behaving at a given time 
  * value function: expected reward, desirability of states after taking into
    account the states that are likely to follow and the rewards available in those states; the only purpose of estimating values is to
    achieve more reward
  * critic: evaluate performance; relate policy and value;

* **Environment**

  * simulator, world
  * is always in a specific state that is changed by the actions of the agent
  * fully observable: agent know all states
  * Markov Decision Process: ==when an agent takes actions with fully observability?==
  * <u>About the design of environmnets</u>
    * the boundary between agent and environment is typically not the same
      as the physical boundary of a robot’s or animal’s body. Usually, the boundary is drawn
      closer to the agent than that. For example, the motors and mechanical linkages of a robot
      and its sensing hardware should usually be considered parts of the environment rather
      than parts of the agent. anything that cannot be changed arbitrarily by the agent is usually considered to be outside of it and thus part of its environment

* **Actions** - Steps that can be performed by the agent to alter the environment  

* **Step** - A step occurs each time that the agent performs an action and potentially changes the environment state. 

* **Episode**

  * A chain of steps that ultimately culminates in the environment entering a terminal state
  * one “run” of the game
  * task: ?
  * episodic task: tasks that end within an episode; usually all RL tasks are one
  * continuing tasks: does not break naturally into identifiable episodes, but goes on continually without limit; usually all control tasks are one

* **Epoch** - A training iteration of the agent that contains some number of episodes. 

* **State**

  * captures all the necessary information, including history
  * See also *Control Systems - State Space Modelling*
  * ideally, is a markov state (see in *Probability*)

* **Terminal State** -  A state in which further actions do not make sense.  In many environments, a terminal state occurs when the agent has won/lost; ends an episode

* 


## Learning and planning

* plan = lookahead of actions 

* you can only plan if you know the effect of your actions 

* 

* **Model-based learning**

  * refeers to both pre known (like in control systems) or learned

  * we’ll focus here in learning models 

    ![image-20210124203339137](Images - 2.9 - Reinforcement Learning/image-20210124203339137.png)

  * We’ll usually try to model de worls as a markov process

  * in the RL context, an “state transition model” is something good to learn

* **Sample based**

  * use the model only to draw sample of “action -> transition”, then use a model-free method to learn “state->action”
  * <u>dyna algorithm</u>
    * uses both learned and simulated experience

## Techniques



### Q-learning

* Model free control with TD learning

* off policy

* building a table that suggests an action for every possible state

* if state is continous, create bins

* if the actions are continous, workarounds are necessary (also bins?)

* (-)  if the state space is large, the Q-table can become prohibitively large

* model free 

* **Reward**

  * “You get want you incentivize, not whatyou intend”

  * we can look only to the imediate next reward, or considering all rewards afterwards

  * if we consider infinite and without decay, it’s the same as monte-carlo learning?

  * Total discounted reward (one step look-ahead): 
    $$
    R_t = r_{t+1} + \gamma Q(t+1, a)
    $$
    
    > $\gamma$ = discount factor

  * <u>n step look-ahead</u>

    * Total reward:

    ![1567533540708](Images - 2.9 - Reinforcement Learning/1567533540708.png)

    * Discounted total reward (future is not so important)

    ![1567533652101](Images - 2.9 - Reinforcement Learning/1567533652101.png)

    * we will usually consider, in the next equations, only 1 step look-ahead (lambda=0)
    * you can also decay or not the importance over time of future rewards
  * this propagation of future reward is the Eligibility Trace?
  
  <img src="Images - 2.9 - Reinforcement Learning/image-20210103161735922.png" alt="image-20210103161735922" style="zoom:67%;" />
  
  * <u>Metrics</u>
  
  * evaluate a policy given the total reward it brings
    * most common: average reward
  
* **Q function**

  * is the mapping function (input -> expected reward), a table

  * expected reward (total future reward) if perform action $a$ in state $s$
  
  * $Q(s, a) = Expected (R_t)$
  
  * Usually the initialization is random
  
  * Optimize Q with gradient-descent-like update (comes from the Bellman equation, when formulating the learning proble as a Dynamic Programming problem):
    
    ![image-20210103154735773](Images - 2.9 - Reinforcement Learning/image-20210103154735773.png)
    $$
    Q^{new}(s_{t},a_{t}) \leftarrow \underbrace{Q(s_{t},a_{t})}_{\text{old value}} + \underbrace{\alpha}_{\text{learning rate}} \cdot  \overbrace{\bigg( \underbrace{\underbrace{r_{t}}_{\text{reward}} + \underbrace{\gamma}_{\text{discount factor}} \cdot \underbrace{\max_{a}Q(s_{t+1}, a)}_{\text{estimate of optimal future value}}}_{\text{new value (temporal difference target)}} - \underbrace{Q(s_{t},a_{t})}_{\text{old value}} \bigg) }^{\text{temporal difference}}
    $$
    
  * We update Q independent of the policy being followed (off policy)
  
  * lambda (λ) - The discount factor is the percentage of future reward that the algorithm should consider in this update.
  
* **Policy function ($\pi(s)$)**

  *  infer the best action to take at its state, s 

  *  maps state->action
  
  *  <u>Optimal action policy</u>
  
     *  just choose the action with higher expected reward
  
     *  greedy policy
  
        ![1567533899506](Images - 2.9 - Reinforcement Learning/1567533899506.png)
  
  *  <u>exploit/explore policy</u>
  
     *  sometimes choose the best Q, sometimes goes random
     *  epsilon: randomness factor? how much of your action is determined by the Q table, and how much by randomness? usually is decresed over time, like simulated aneeling; 
     *  “balancing between unfocused exploration and focused exploitation has been hypothesized to be a general property of adaptive and intelligent systems” Mitchell
     
     

### Deep Q networks

* funtion approximation using Neural Networks

* maps directly state->actions (array of expect reward for each action)?

* model free 

* (+) better handle large state spaces, because the Q mapping is indirect

* (+) can handle raw data input (which is prohibitive in Q learning because of the table size)

* (+) can take actions never seen before, “interpolating” past experiences

  ![Structure of the neural network used for the Deep Q-learning Network... |  Download Scientific Diagram](Images - 2.9 - Reinforcement Learning/Structure-of-the-neural-network-used-for-the-Deep-Q-learning-Network-implementation-with.png)

* input: state of the system at a given time (in atari is the pixels of the current screen—plus three prior screens)

* outputs: one value for each possible action. 

* Replay buffer: keeps track of data collected from the environment; examples presented in random order; stabilized the learning by decorrelation the trajectories; batch learning is easier to the NN to learn

* data collection policy?

* ==backpropagate for every output?==

* Q target? Fixed Q iteration?

* ==The difference between the new set of values and the previous set of values is considered the “error” of the network?==

* **Famous agent’s architecture**

  * DQN
    * good for boolean or discrite action space
  * DDPG
    * Deep Deterministic Policy Gradients
    * good for continous action space
  * REINFORCE
  * TD3
  * PPO
  * SAC
  
* **actor-critic network**

  * to instead of mapping to expected reward and then take the argmax, map directly to actions
  * ==is optional, like another way of learning==
  * can learn non determistic policies, which may be desired
  * also works well for continous action spaces 
  * (+) better convergence
  * (+) may be more lightweight 
  * (-) may be an overkill to simple problems, and sometimes worse 

### Sarsa

* Model free control with TD learning

* on policy

* Q function update:

  ![image-20210103154003366](Images - 2.9 - Reinforcement Learning/image-20210103154003366.png)

* The name sarsa comes form the fact that the update is done with actions $ (S_{t}, A_{t}, R_{t+1}, S_{t+1}, A_{t+1} )$, that is, on policy

### Gradient Q-learning

* ?
* both on policy and off policy

### Least Square Q-Learning

* ?


### Value Function Approximation

* Estimates the Q function by a function, instead of a table
* both for prediction and control
* a tabular method is a special case of a linear combination of features
* **Batch methods**
  * take several experieces, learn afterwards 
  * each action is revised several times 
  * quite similar to supervised learning
  * the examples are presented in random order
* **Incremental methods**
  * take action, see result, learn, take action again
  * you learn only once to each action, which is not much efficient
* **Methods**
  * Neural Networks 
  * decision trees
  * …

## Applications

* [RL in enterprise applications](https://www.youtube.com/watch?v=GOsUHlr4DKE)
* https://www.youtube.com/watch?v=ClDjWyHRLks
* [Modulação de Inversores por Deep Q Learning](https://www.youtube.com/watch?v=fOcpNLEl14Q)
* Coisas da Boston Dynamics, tipo: https://www.youtube.com/watch?v=fn3KWM1kuAw&list=RDCMUC7vVhkEfw4nOGp8TyDk7RcQ&start_radio=1
* **Games**
  * Atari games have become popular benchmarks for AI systems, particularly reinforcement learning
  * "Games are just our development platform.… It’s the fastest way to develop these AI algorithms and test them, but ultimately we want to use them so they apply to real-world problems and have a huge impact on things like healthcare and science. The whole point is that it’s general AI—it’s learning how to do things [based on] its own experience and its own data." Demis Hassabis
  * there are many games humans play that are even more challenging for AI than Go. One striking example is Marcus'  charades, which requires sophisticated visual, linguistic, and social understanding far beyond the abilities of any current AI system. 
* **Games - DeepMind case**
  * first presented this work in 2013 at an international machine-learning conference. 
  * DeepMind’s deep Q-learning programs turned out to be better players than the human tester on more than half the games.
  * Less than a year later, Google announced that it was acquiring DeepMind for £440 million
  * a year after the Lee Sedol match, DeepMind developed a version of the program that was both simpler than and superior to the previous versions. This newer version is called AlphaGo Zero because, unlike its predecessor, it started off with “zero” knowledge of Go besides the rules
  * Even the most general version, AlphaZero, is not a single system that learned to play Go, chess, and shogi. Each game has its own separate convolutional neural network that must be trained from scratch for its particular game. 
  * Atari game-playing programs were actually better examples of “learning without human guidance” than AlphaGo, because unlike the latter they were not provided with the rules of their game 
  * <u>how it works</u>
    * AlphaGo Fan and AlphaGo Lee both used an intricate mix of deep Q-learning, “Monte Carlo tree search,” supervised learning, 
    * Similar to the way Deep Blue’s evaluation function worked, Monte Carlo tree search is used to assign a score to each possible move from a given board position. 
    * With Monte Carlo tree search, Black looks ahead at only a minuscule fraction of the possible sequences that could arise from each move, 
    * More specifically, in order to choose a move from its current position, Black “imagines” (that is, simulates) several possible ways the game could play out, 
    * during a roll-out the program chooses moves probabilistically based on any scores that those moves might have from previous rounds of Monte Carlo tree search. 
    * AlphaGo’s Monte Carlo tree search performed close to two thousand roll-outs per turn. 
    * The DeepMind group realized that they could improve their system by complementing Monte Carlo tree search with a deep convolutional neural network. 
    * before you start the Monte Carlo process of performing roll-outs from that position, the ConvNet is whispering in your ear which of the possible moves from your current position are probably the best ones. 
    * assign a rough value to all possible moves from the current position. 
    * the ConvNet will play the role of the program’s “intuition,”
* **Robotics**
  * learn policies to map raw video images to robot’s actions
  * [stacking blocks](https://youtu.be/GOsUHlr4DKE?t=991)
* **navigation**

## Others

* prior information can be incorporated into
  reinforcement learning in a variety of ways that can be critical for efficient learning (e.g.,
  see Sections 9.5, 17.4, and 13.1)





# Code

* **RL-Glue**: software package for reinforcement-learning experiments
* **OpenGym**: see in *physics engines*
## C++ libraires

* **smarties**
  * https://github.com/cselab/smarties
  * (+) minimal dependencies
  *  uses pybind11 for seamless compatibility with python
  * It handles on its own multiple agents per environment, partial observability via rnn, conv net preprocessing, and so o
  * integrates with OpenAI gym
  * :book:!!!???
* **RLlib**
  * https://github.com/HerveFrezza-Buet/RLlib
  * [paper](https://www.jmlr.org/papers/volume14/frezza-buet13a/frezza-buet13a.pdf)
  * seams quite basic 
  * ++
  * (+) minimal dependencies (just GSL)
  * :book:!!!???
  * may require doxygen (if you want docuemntation), which required Flex (`sudo apt-get install flex`) and Bison (`sudo apt-get install bison`)
  * some examples require: ImageMagick, ffmpeg
* **AI-Toolbox**
  * focused on RL, 
  * [paper](https://www.jmlr.org/papers/v21/18-402.html)
  * [code](https://github.com/Svalorzen/AI-Toolbox)
  * have bidding for python
  * not many dependencies
  * (+) very complete
  * (+) currently updated (2020)
* **OpenSpiel**
  * [paper](https://arxiv.org/abs/1908.09453)
  * [code](https://github.com/deepmind/open_spiel)
  * have bidding for python (is their focus, in fact) and swift
* **Arcade Learning Environment **
  * https://github.com/mgbellemare/Arcade-Learning-Environment
  * for experimentation with atari games
* **cytonRL**
  * requires cuda
  * [paper](https://arxiv.org/abs/1804.05834)
* **RL-CPP**
  * https://github.com/navneet-nmk/Pytorch-RL-CPP
* uses pytorch as backend
  * Arcade Learning Environment as “frontend”
* 
  * not being maintained (last commit july 2019)
* **CppRl**
  * uses pytorch as backend
  * https://github.com/Omegastick/pytorch-cpp-rl
  * production-ready
  * :book:!!!
  * precisa necessariamente do OpenGym? Ou pode ser treinado isoladamente?
  * I tryed, but had problems with the OpenGym

## Python libraries

* ?

## Other languages

* **torch-twrl**
  * for reinforcement learning
  * Lua/Torch
