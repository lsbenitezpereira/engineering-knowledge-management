Questão 1:

a) Escolhestes o método aberto de pior convergência.

Os métodos de Newton Raphson e da secante convergem.

b) Parece ok

c) A função bissecção que foi deixada no SIGAA funciona perfeitamente (testei hoje novamente na versão 6.1.0)

Questão 2:

a) Gauss seidel convege, só que com a tolerância default qua usastes, ele leva 55 iterações. Como usate o numero de iterações default (50),  ela diz que não convergiu.

Isso ocorre porque na segundo linha a diagonal não é dominante.

--> A=[-5/6 1/3 1/2; 1/3 5/12 0; 1/2 -1 -2/3];

--> b=[-4;0;0];

--> [x,i]=gauss_seidel(A,b,1.1,0.0001,60)

x = 

 

  31.999879

 -25.599906

  62.399773

 i = 

 

  55.

b) Foi solicitada a norma soma das colunas, mas tudo bem a de Frobenius serve.

c) Esta correto

 

Questão 3:

 

Tudo certo, só faltou a unidade das variáveis na letra a! (mg/l)

 

**NOTA: 8**