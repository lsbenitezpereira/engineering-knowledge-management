## A

![img](Images - Exercicio 03/ex3a.jpeg)



## B

Usando a ferramenta [Desmos](https://www.desmos.com/calculator):

![image-20200923105519939](Images - Exercicio 03/image-20200923105519939.png)

## C

Comandos:

```matlab
fun = '[16*x(1)-cos(x(2)-2*x(1)); 16*x(2)+0.75*sin(-x(2)-3*x(1))]'
jac = '[16-2*sin(x(2)-2*x(1)) sin(x(2)-2*x(1)); -2.25*cos(-x(2)-3*x(1)) 16-0.75*cos(-x(2)-3*x(1))]'

[x,iter]=newraph_n(fun,jac,0.001,50)
Entre com as aprox. inicial x0 = [0.1; 0.01]
```

Resultado:

```matlab
 iter  = 3.

 x  = 
   0.0620866
   0.0090993
```



