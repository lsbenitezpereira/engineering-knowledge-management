## A

Utilizando os pontos extremos para calcular as derivadas, obtemos:
$$
f_1^`=\frac{20-16}{20-15} = 0.8
$$

$$
f_2^`=\frac{90-60}{50-40} = 3
$$

Resolvendo pelo scilab:

```matlab
[y, b, c, d, ye] = splines([15, 25, 30, 50],[16, 34, 40, 90],35, 'c', [0.8 3])
--> ye 
  ye = 48.580529
```

## B

Gráfico interativo disponível em https://www.desmos.com/calculator/5wnmqn2crh:

![image-20200930120012465](Images - Exercicio 04/image-20200930120012465.png)