## A

![](Images - Exercicio 01/WhatsApp Image 2020-09-30 at 11.07.09.jpeg)

## B

Utilizando a solução encontrada acima, obtemos que:
$$
y(0.5)=4.0723
$$
Dessa forma, o erro relativo percentual será:
$$
\text{erro }= \left | \frac{4.0723 - 4.2767}{4.0723} \right | \times 100\% = 5.0192\%
$$
