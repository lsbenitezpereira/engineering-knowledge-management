## A

![image-20200930095150598](Images - Exercicio 03/WhatsApp Image 2020-09-30 at 09.31.38.jpeg)

## B

Não encontrei no SIGAA a função fornecida pelo professor, e acredito que o objetivo da questão não é escrevermos a função agora. Dessa forma, utilizei a ferramenta online https://www.emathhelp.net/calculators/calculus-2/simpsons-rule-calculator/, obtendo o resultado:
$$
\text {integral_numerica} = 2.71372533333333
$$
## C

Calculando analiticamente a integral, obtemos:

<img src="Images - Exercicio 03/WhatsApp Image 2020-09-30 at 11.42.40.jpeg" alt="WhatsApp Image 2020-09-30 at 11.42.40" style="zoom:40%;" />
$$
\text{integral_analitica} = 2.674182336331968
$$
Dessa forma, temos que:
$$
\text{erro_verdadeiro} = |\text {integral_numerica} - \text {integral_analítica}| = 0.039542997
$$

$$
\text{erro_relativo_percentual} = \left | \frac{\text{integral_analitica} - \text {integral_numerica}}{\text{integral_analitica}} \right | \times 100\% \\

 = 1.47869486924\%
$$

