[TOC]

# Computational algebra/calculus

* I’ll register here both numerical and simbolical

## Maxima

* Computational algebra/calculus
* Mainly simbolical
* `:` to set values ('a : 3;')
* `:=` to define functions ('f(x) := x^2;').
* **La Place**
  * ilt(1/(s+1), s, t)
  * laplace(%e^(-5*t), t, s)
* **Solvers**
  * solve(<eq>, <variable>); is not too powerful; there are many kinds of equations it cannot solve
  * findroot(??)
* **partial fraction expantion**
  * `partfrac ( 1/(x^2*(x^2 + 1)), x);`
  
  * it doesnt separe complex factor, but you can user [this](https://themaximalist.org/2016/08/18/partial-fraction-decomposition-using-complex-factors/) implementation:
  
    ```matlab
    factorC(_f,_z):=block(
    [s,n,m,fp,j],
    fp:1,
    /* This commented code was meant to use the
    more robust solver to_poly_solve, but 
    I couldn't understand how to handle multiplicities
    ss:args(to_poly_solve(_f,_z)),
    s:create_list(ss[k][1],k,1,length(ss)),*/
    s:solve(_f,_z),
    m:multiplicities,
    n:length(s),
    for j:1 thru n do 
     if lhs(s[j])#0 
     then fp:fp*(_z-(rhs(s[j])))^m[j],
     fp:fp*divide(_f,fp)[1],
    fp
    );
    
    partfracC(_f,_z):=block(
    [d,fd],
    d:denom(_f),
    fd:factorC(d,_z),
    partfrac(1/fd,_z)
    );
    
    partfracC(1/s^3+1, s)
    ```
  
* **Others**
  * taylor(cos(x), x, 0, 5);
  * divide polimials: `divide(<poli1>, <poli2>)`
* GUI
  * wxmaxima
  * xmaxima

## Smath

* Para planilhas de calculo visuais 

## SymPy

* alternative to systems such as Mathematica or Maple
* Implicit multiplication (like 3x or 3 x) is not allowed in Python, and thus not allowed in SymPy. To multiply 3 and x, you must type 3*x with the *.
* `=` is variable assignment
* `==` represents exact structural equality testing
* There is a separate object, called `Eq`, which can be used to create symbolic equalities
* use ** for exponentiation
* <u>Evaluation</u>
  * To evaluate a numerical expression into a floating point number, use evalf
  * can evaluate floating point expressions to arbitrary precision
  * lambdify: evaluate an expression at many points
* <u>Substitution</u>
  * replaces all instances of something in an expression with something else
  * Ex: Evaluating an expression at a point, `expr.subs(x, 0)`
  * Ex: Replacing a subexpression with another subexpression, `expr = expr.subs(y, x**y)`

# Graphing softwares

* Geogebra
* Desmos