function [raiz, iter]=simples(funcao, x0, es) 
    // Cálculo das raizes por iteração de ponto fixo simples  
    // function [raiz,iter]=simples(funcao, x0, es)
    // onde raiz é a raiz procurada de funcao
    // iter é o no de iterações realizadas para o erro especificado
    // funcao é a função de entrada literal em x
    // x0 é a condição inicial
    // es é o criterio de parada que é opcional
    // Exemplo de chamada:
    // exec('path\ simples.sci',-1)
    // fun ='sin(sqrt(x))'
    // [raiz,iter]=simples(fun, 0.1, 0.1)
    //i = 0; maxi = 50; x = x0; ea=100;
    i = 0; maxi = 50; x = x0; ea=100;
    // se es não foi estabelecido usa 0.0001%
    if argn(2) < 3 then
        es = 0.0001;
    end
    printf("Iter\tErro aprox.%%\tRaiz\n");
    // inicio do processo iterativo
    while ea > es & i < maxi do
        xi = evstr(funcao);
        i= i+1;
        if xi ~=0 then // xr_novo não pode ser zero
            ea = abs((xi - x)/xi)*100;
        end
        printf("%d\t%f\t%f\n",i,ea,xi);
        x = xi;
    end
    if i == maxi then
        Raiz = 'divergiu';
    else
        raiz = xi;
    end
    iter = i;
endfunction
