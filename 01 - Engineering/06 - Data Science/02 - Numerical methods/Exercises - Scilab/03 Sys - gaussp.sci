function x=gaussp(A,b)
    // Eliminação de Gauss Ingênua
    // function x=gaussi(A,b)
    // onde x é o vetor solução
    //      b é o vetor coluna de estimulos
    //      A é a matriz de coeficientes
   [m,n]=size(A);
   if m~=n then
       error('A matriz A deve ser quadrada');
   end
   n=length(b)
   if m~=n then
       error('Dimensão incorreta de b');
   end
   A=[A b];
   nb = n+1;
   //Eliminação Progressiva
   for i=1:n-1
       // pivotamento parcial
       // encontrar o maior coeficiente da coluna abaixo do pivo
       [maior,k]=max(abs(A(i:n,i)));
       l=i + k - 1;
       if l~=i then
           A([l,i],:)=A([i,l],:);
       end
       for j=i+1:n
           A(j,i) = A(j,i)/A(i,i);
       // A(j,i) que seriam eliminadas são usadas para armazenar os multiplicadores
           //for k=i+1:nb
               //A(j,k)=A(j,k)-A(j,i)*A(i,k);
           //end
           A(j,i+1:nb)=A(j,i+1:nb)-A(j,i)*A(i,i+1:nb);
           disp(A);
       end       
   end
   x = zeros(n,1);
   x(n)=A(n,nb)/A(n,n);
   for i=n-1:-1:1
       //for j=i+1:n
       //    x(i)=x(i)+A(i,j)*x(j);
       //end
       //x(i)=(A(i,nb)-x(i))/A(i,i);
       x(i)=(A(i,nb)- A(i,i+1:n)*x(i+1:n))/A(i,i)
   end
endfunction
