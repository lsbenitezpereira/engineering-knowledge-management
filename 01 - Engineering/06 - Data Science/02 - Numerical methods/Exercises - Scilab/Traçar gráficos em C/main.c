#include <stdio.h>
#include <locale.h>
#include <math.h>
#define ISCR 60
#define JSCR 21
#define BRANCO ' '
#define ZERO '-'
#define YY '|'
#define XX '-'
#define FF '.'

/*********************************************************************
   Para uso iterativo no terminal. Produz um gr�fico simples de fun
   para o intervalo x1, x2. Permite plotar outro gr�fico at� a satis-
   fa��o do usu�rio.
   Adaptado de:
   PRESS, William H. Press et alii. Numerical recipes in C : the art
   of scientific computing.Cambridge University Press, 1992
*********************************************************************/

typedef float (*ptr)(float);
float fun(float);

int main()

{
    int jz,j,i;
    float ysml,ybig,x2,x1,x,dyj,dx,y[ISCR+1];
    ptr fx = fun;
    char scr[ISCR+1][JSCR+1];
    setlocale(LC_CTYPE,"");
    while(1)
    {
        printf("\nEntre com x1 e x2 separados por espa�o\n");
        printf("\nx1 = x2 para parar:\n");
        scanf("%f %f",&x1,&x2);
        if (x1 == x2)
            break;
        //Constr�i o eixo y
        for (j=1; j<=JSCR; j++)
            scr[1][j]=scr[ISCR][j]=YY;
        for (i=2; i<=(ISCR-1); i++)
        {
            // constr�i o eixo x
            scr[i][1]=scr[i][JSCR]=XX;
            // Tela em branco
            for (j=2; j<=(JSCR-1); j++)
                scr[i][j]=BRANCO;
        }
        dx=(x2-x1)/(ISCR-1);
        x=x1;
        ysml=ybig=0.0; // limites incluir� o zero
        for (i=1; i<=ISCR; i++)
        {
            y[i]=(*fx)(x); // calcula a fun��o em intervalos iguais
            if (y[i] < ysml) ysml=y[i]; // encontra o menor e
            if (y[i] > ybig) ybig=y[i]; // maior valor
            x += dx;
        }
        if (ybig == ysml)
            ybig=ysml+1.0;   // Cuida poara separar a parte superior e inferior.
        dyj=(JSCR-1)/(ybig-ysml);
        jz=1-(int) (ysml*dyj); //Observa qual linha corresponde a 0.
        for (i=1; i<=ISCR; i++) // Coloca um indicador na altura da fun��o e 0
        {
            scr[i][jz]=ZERO;
            j=1+(int) ((y[i]-ysml)*dyj);
            scr[i][j]=FF;
        }
        printf(" %10.3f ",ybig);
        for (i=1; i<=ISCR; i++)
            printf("%c",scr[i][JSCR]);
        printf("\n");
        for (j=(JSCR-1); j>=2; j--)
        {
            printf("%12s"," ");
            for (i=1; i<=ISCR; i++)
                printf("%c",scr[i][j]);  // Imprime
            printf("\n");
        }
        printf(" %10.3f ",ysml);

        for (i=1; i<=ISCR; i++)
            printf("%c",scr[i][1]);
        printf("\n");
        printf("%4s %10.3f %48s %10.3f\n"," ",x1," ",x2);
    }
    return 0;
}
float fun(float x)
{
    // sua fun��o aqui
    return x*x;
}
