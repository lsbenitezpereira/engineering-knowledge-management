function grafico(fun, a, b)
// Apresentação de o gráfico de uma função
// 
// onde fun é a função de entrada literal em x
//      a é o limite inferior do intervalo de busca
//      b é o limite superior do intervalo de busca 
x = linspace(a,b,100);
ylabel("$ \scalebox{3}{\ \ x_2  \ $");
xlabel("$ \scalebox{3}{\ \ x_1  \ $");
//xlabel(' t ', 'fontsize', 5);
//ylabel(' y ', 'fontsize', 5,'rotation', 0);
f = evstr(fun)
plot2d(x,f,style=[color('blue4')]);
p = get("hdl");
p.children.thickness = 2; 
c=get("current_axes")
c.labels_font_size=4;
xgrid;
endfunction
