/home/benitez/Software/Scilab-x86_64.AppImage

# Exec and test

## Basics

**Basic - serie_mclaurin**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Basic - serie_mclaurin.sci')

[fx,term]=series('(-1)^(n+2)*x^(2*n)/factorial(2*n)', %pi/3, 3)



**Basic - grafico**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Basic - grafico.sci')

grafico('x^3', -1, 1)

## Root finding

**Root - bissecao**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/02 Root - bissecao.sci')

[raiz,iter]=bissecao('log(x) + x', 0.1, 2, 0.1)

```
bissecao('10-x-10^(-11)*(%e^(x/0.025248)-1)', 0.6, 0.8)
```

**Root - falsapos**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Root - falsapos.sci')

[raiz,iter]=falsapos('log(x) + x', 0.1, 2, 0.1)



**Root - IteracaoPontoFixoSimples**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/02 Root - IteracaoPontoFixoSimples.sci')

[raiz,iter]=simples('sin(sqrt(x))', 0.1, 0.1)



**Root - new_raphson**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/02 Root - new_raphson.sci')

[raiz,iter]=new_raphson('log(x) + x', '(1/x) + 1', 0.0001,50)



**Root - sec_mod**
exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Root - sec_mod.sci')

[raiz,iter]=sec_mod('log(x) + x', 1d-6, 0.0001,50)

## Linear systems

**Gauss naive**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Sys - gaussi.sci')

gaussi([10 2 -1; -3 -5 2; 1 1 6], [27; -61.5; -21.5])



**Gauss pivoting**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/03 Sys - gaussp.sci')

gaussp([10 2 -1; -3 -5 2; 1 1 6], [27; -61.5; -21.5])



**LU pivoting**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/Sys - fatoraLU.sci')

fatoralu([1 -6 -1; -3 -1 6; -8 1 -2], [-38; -34; -40])



**Gauss Seidel**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/03 Sys - gauss_seidel.sci')

A=[0.8 -0.4 0; -0.4 0.8 -0.4; 0 -0.4 0.8]

b = [41; 25; 105]

gauss_seidel(A,b)



**Newton-raphson**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/03 Sys - newraph_n.sci')

fun = '[(x(1))^2+x(1)*x(2)-10 ; x(2)+3*x(1)*x(2)^2-57]'
jac = '[2*x(1)+x(2) x(1); 3*x(2)^2 1+6*x(1)*x(2)]'
[x,iter]=newraph_n(fun,jac,es,maxi)


## EDO

**Euler**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/04 EDO - euler.sci')

`euler('3*y(i)-5*x(i)-2', 0.5, 0.7, 0.1, 1.2)`



**Range Kutta 4º ordem**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/03 - Computação/08 - Data Science/02 - Numerical methods/Exercises - Scilab/04 EDO - rk4.sci')

`rk4('3*ya-5*ta-2', 0.5, 0.7, 0.1, 1.2)x`

## Interpolação

**Newton**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/06 - Interpolação - pi_Newt.sci')



**La Grange**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/06 - Interpolação - pi_Lagr.sci')



**Spline (apenas natural, otimizado)**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/07 - Interpolação spline_natural.sci')

splines([3, 4.5, 7, 9],[2.5, 1, 2.5, 0.5],5, ‘n’) = splines_natural([3, 4.5, 7, 9],[2.5, 1, 2.5, 0.5],5)

(retornará ye=1.1028897)

**Spline**

exec('/home/benitez/Desktop/engineering-knowledge-management/01 - Specific Subjects/06 - Data Science/02 - Numerical methods/Exercises - Scilab/07 - Interpolação spline.sci')

[y, b, c, d, ye] = splines([3, 4.5, 7, 9],[2.5, 1, 2.5, 0.5],5, ‘c’, [-1.4 -1.5])

(retornará ye=1.1011594)