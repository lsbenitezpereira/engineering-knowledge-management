# Spline cúbicas

Resolva os exercícios propostos na vídeo aula:

## 1)

 A partir do conjunto de dados :

Determine splines cúbicos naturais que façam o ajuste dos dados;

Determine o valor interpolado de y em x =12,7.

Trace um gráfico com os pontos do conjunto de dados 

`A = [1 0 0 0 0; 3 14 4 0 0; 0 4 14 3 0; 0 0 3 14 4; 0 0 0 0 1]`

`b = [0; -13/4; -11/4; 5/4; 0] `

`c = A\b`

![image-20200809030311925](Images - 07 - Interpolação spline - Homeword A/image-20200809030311925.png)

Gráfico interativo disponível em https://www.desmos.com/calculator/vnkslvbznl

![image-20200808174215241](Images - 07 - Interpolação spline - Homeword A/image-20200808174215241.png)

## 2)

 Ajuste splines cúbicos não é nó aos mesmos dados usados no exemplo 3. Apresente a estimativa do valor de y em x = 5.

`A = [2.5 -4 1.5 0; 1.5 8 2.5 0; 0 2.5 2.25 2; 0 2 -4.5 2.5]`

`b = [0; 4.8; -4.8; 0]`

`c = A\b `

 ![image-20200809030252902](Images - 07 - Interpolação spline - Homeword A/image-20200809030252902.png)

## 3)

 Um braço de um robô deve passar nos instantes t0, t1, t2, t3, t4 e t5 por posições pré-definidas θ(t0), θ(t1), θ(t2), θ(t3), θ(t4) e  θ(t5), onde θ(t) é o ângulo (em radianos) que o braço do robô faz com o  plano XOY.

Com base nos dados da tabela, aproxime a trajetória do robô por uma spline cúbica amarrada. Indique também uma aproximação da posição do  robô no instante 𝑡 = 1.5 𝑠.

 Calcule uma aproximação à velocidade do robô no instante 𝑡 = 1.5 s.

`A = [4 2 0 0; 2 6 1 0; 0 1 6 2; 0 0 2 4]`

`b = [0.375; 0.375; -0.15; -0.9]`

`c = A\b`

![image-20200809030236732](Images - 07 - Interpolação spline - Homeword A/image-20200809030236732.png)