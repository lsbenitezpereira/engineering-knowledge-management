/**********************************************
 Name        : 
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : 
 **********************************************/
 function a = equation (x, y)
     m=size (x);
     n=size (y);
     if m~=n then disp ("erro no programa");a=0;
     else
        a=(n*sum(x.*y) - sum(x)*sum(y))/(n*sum(x.*x) - sum(x)*sum(x));
     end
 endfunction
