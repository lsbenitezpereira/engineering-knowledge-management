function[y, b, c, d, ye] = splines(x, y, xe, tipo, vetor)
    //onde o y(a), b, c, d são vetores com coeficientes das (n-1) cubicas naturais
    //ye é o valor da variável dependente em xe
    //x é o vetor dos pontos amostrados da variável independente
    //y é o vetor dos pontos amostrados da variável dependente
    //tipo = ‘n’, spline natural
    //tipo = ‘c’, clamped (nesse caso é necessário passar através de vetor as derivadas primeiras nas extremidades)
    //tipo = ‘k’, not-a-knot
    //vetor é uma informação necessária apenas no tipo clamped (derivada no primeiro e ultimo ponto)

    if argn(2) < 5 then
        vetor = [0 0];
    end
    n = length(x);
    if n ~= length(y) then
        error("vetores x e y com dimenções diferentes");
    end
    //cálculo das diferenças finitas
    for i = 1:n-1
        h(i) = x(i+1)-x(i);
        ddf(i) = (y(i+1)-y(i))/h(i);
    end

    A = zeros(n, n);      //monta A
    f = zeros(n,1);       //monta f
    //Gerar a parte interna da matriz A e do vetor coluna f (não depende do tipo)
    for i = 2:n-1
        for j = 2:n-1
            if i == j then
                A(i,j) = 2*(h(i-1)+h(i));
            elseif j == i+1 then
                A(i,j) = h(j-1);
            elseif i == j+1 then
                A(i,j) = h(i-1);
            end            
        end
        f(i) = 3*(ddf(i)-ddf(i-1));
    end
    A(2,1) = h(1);
    A(n-1, n) = h(n-1);

    //Gerar a parte restante da matriz A e do vetor coluna f (depende do tipo)
    if tipo=='n' then
        f(1) = 0;
        A(1,1) = 1;
        A(n,n) = 1;
	f(n) = 0;
    elseif tipo=='c' then
        f(1) = 3*ddf(1)-3*vetor(1);
        A(1,1) = 2*h(1);
        A(1,2) = h(1);
        A(n,n) = 2*h(n-1);
        A(n,n-1) = h(n-1);
	f(n) = 3*vetor(2) - 3*ddf(n-1);
    elseif tipo=='k' then
        f(1) = 0;
        A(1,1) = h(2);
        A(1,2) = -h(1)-h(2);
        A(1,3) = h(1);
        A(n,n) = h(n-2);
        A(n,n-1) = -h(n-2) -h(n-1);
        A(n,n-2) = h(n-1);
	f(n) = 0;
    end

    c = zeros (n, 1);
    c = A\f;
    for i=1:n-1
        d(i) = (c(i+1)-c(i))/(3*h(i));
        b(i) = ddf(i)- (h(i)/3)*(2*c(i)+c(i+1));
        //descobrindo em qual segmento está o ponto a ser interpolado
        if (xe > x(i)) & (xe < x(i+1)) then
            j = i;
        end
    end
    k = xe - x(j);
    ye = y(j) + b(j)*k + c(j)*k^2 + d(j)*k^3;
    //construindo as splines
    for j=1:n-1
        xx = linspace(x(j), x(j+1), 1000);
        k = xx-x(j);
        yy = y(j) + b(j)*k + c(j)*k .^2 + d(j)*k .^3;
        //plot2d(xx, yy);
    end
    //ylabel("Splines");
    //xlabel("X");
    //xgrid;
endfunction
