 function a = equation (x, y)
     m=length (x);
     n=length (y);
     if m~=n then disp ("erro no programa");a=0;
     else
        a=(n*sum(x.*y) - sum(x)*sum(y))/(n*sum(x.*x) - sum(x)*sum(x));
     end
 endfunction
 
 x=[1 2 3];
 y=[4 5 6];
 
 a = equation (x, y)
 
