/*----------------------*/
/*     PRINT PIRAMID	*/
while(1) do
    m=input("Say a number:");
    i=1;
    j=1;
    while i<=m do 
        while j <= m do
            if j == i | j == (m+1)-i then printf("*");
                else printf(" ");
            end
            j=j+1;
        end
        printf("\n");
        i=i+1;
        j=1;
    end
    printf("\n\n------------------------------------\n\n");
end


/*-----------------------*/
/* PRINT REVERSE PIRAMID */
    m=input("Say a number:");
    i=1;
    j=1;
    while i<=m do 
        while j <= (m+1)-i do
            printf("*");
            j=j+1;
        end
        printf("\n");
        i=i+1;
        j=1;
    end
    printf("\n\n------------------------------------\n\n");

	
	
/*----------------------*/
/*       DIAMOND        */	
    m=input("Say a number:");
    i=1;
    j=1;
    n=(m-1)/2
    while i<=m do 
        while j<=m do
            if j<=n | j>(m-n) then printf(" "); else printf("*"); end
            j=j+1;
        end
        printf("\n");
        if i<(m+1)/2 then n=n-1; else n=n+1; end
        i=i+1;
        j=1;
    end
	
/**********************************************
 Name        : Fibonacci printer
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Print the fibonacci sequence until de x_maxº number
 **********************************************/

	x_max=15;       
	n_current = 1;
	n_last = 1;
	/// Main loop
	for x=0:1: x_max
        printf("%d\n", n_current);
        n_current = n_current + n_last;
        n_last = n_current - n_last;
	end


/**********************************************
 Name        : Sum and mean calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Keep calculating sum, mean and total entries while the entry is positive 
 **********************************************/
 
x_sum =0;
x_number = 0;
// Main loop
while 1 then
    x_temp=input("Say a number: ");
    //scanf("%f", &x_temp);
    if x_temp<0 then
        break;
    else
        x_sum=x_sum+x_temp;
        x_number=x_number+1;
    end
end
// End program
printf("\n\n-------------\n\n");
printf("Total = %f \nMean = %f \nNumbers = %d\n\n", x_sum, x_sum/x_number, x_number);


/**********************************************
 Name        : Hand multiplication calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Receive the integers A and B e multiply then in the ROOT way 
 **********************************************/

    /// Global variables
    result=0;
	/// Main
    a=input("Say a number: ");
    b=input("Say another number: ");
    for x=0:1:(b-1)
        result=result+a;
    end
	/// End program
	printf("Result = %d\n\n", result);

	
/**********************************************
 Name        : Hand divider calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Receive the integers A and B e divide then in the ROOT way
 **********************************************/

    result=0;

	/// Main
    a=input("Say a number: ");
    b=input("Say another number: ");
    x=0;
    while b*x<a then
        x=x+1;
    end
	/// End program
	printf("\nResult = %d\n\n", x);



