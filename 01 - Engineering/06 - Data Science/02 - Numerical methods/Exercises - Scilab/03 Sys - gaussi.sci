function x = gaussi(A,b)
    // Eliminação de Gauss Ingênua
    // function x = gaussi(A, b)
    // onde x vetor solução
    //      A é a matriz de coeficientes
    //      b é 
    // exec('path\gaussi.sci',-1)
    // x=gaussi(A, b)
    // 
    [m,n] = size(A);
    if  m~=n then
        error('A deve ser uma matriz quadrada.');
    end
    [m,nb] = size(b);
    if  m~=n then
        error('Vetor b com número incorreto de linhas.');
    end
    nb = n + 1;
    A = [A b];
    // eliminação progressiva
    for i = 1:n-1
        for j = i+1:n 
            A(j,i) = A(j,i)/A(i,i);
            //for k = i+1:nb
            //   A(j,k)= A(j,k)-A(j,i)*A(i,k);
            //end
            A(j,i+1:nb)= A(j,i+1:nb)-A(j,i)*A(i,i+1:nb);
            disp(A)
        end
    end
    // substituição regressiva
    x = zeros(n,1);
    x(n)= A(n,nb)/A(n,n);
    for i=n-1:-1:1
        //for j=i+1:n
        //    x(i)=x(i)+A(i,j)*x(j);
        //end
        //x(i)=(A(i,nb)-x(i))/A(i,i);
        // para resolver a equação em uma unica linha usa-se um for implicito para o somatorio
        x(i)=(A(i,nb)-A(i,(i+1):n)*x((i+1):n))/A(i,i);
    end
endfunction
