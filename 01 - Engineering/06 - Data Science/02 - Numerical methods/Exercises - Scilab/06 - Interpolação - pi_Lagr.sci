function  ye = pi_Lagr (x,y,xe)
    //onde
    //vetor x conhecido
    //y vetor conhecido (pontos)
    //xe onde quero estimar y
    n = length (x);
    if length(y)~= n then
        error ("vetores com tamanho diferente");
    end
    soma = 0;
    for i = 1 : n
        produto  = y(i);
        for j=1:n
            if i~=j
                produto = produto*(xe-x(j))/(x(i)-x(j));
            end
        end
        soma = soma +produto;      
    end
    ye = soma;
endfunction
