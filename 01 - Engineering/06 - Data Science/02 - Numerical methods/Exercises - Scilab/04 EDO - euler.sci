function [y,x]=euler(fun,xi,xf,h,y0)
// Res. de sist. EDs por Euler
// function [y,x]=rk4(fun,ti,tf,h,y0)
// onde y é o vetor solução para a variavel depentente
//      x é o vetor da variável independente
//      xi é o valor inicial da variável independente
//      xf é o instante final
//      h é o tamanho do passo de cálculo
//      y0 valor inicial da variável dependente
    x(1) = xi; i = 1; xa = xi; y(1)=y0;
        // inicio do processo iterativo
    while xa < xf do
        ya = y(i);
        phi = evstr(fun);
        disp(phi, 'Phi: ') 
        y(i+1)=y(i)+phi*h;
        xa = xa + h;
        x(i+1)=xa;
        i=i+1;
    end 
endfunction
